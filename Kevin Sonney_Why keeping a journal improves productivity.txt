<p>In previous years, this annual series covered individual apps. This year, we are looking at all-in-one solutions in addition to strategies to help in 2021. Welcome to day 10 of 21 Days of Productivity in 2021.</p>
<p>When I was in primary school in the days before the commercial internet, teachers would often give my class an assignment to keep a journal. Sometimes it was targeted at something particular, like a specifically formatted list of bugs and descriptions or a weekly news article summary for a civics class.</p>
<p>People have been keeping journals for centuries. They are a handy way of storing information. They come in many forms, like the Italian <a href="https://en.wikipedia.org/wiki/Zibaldone" target="_blank">Zibaldone</a>, <a href="https://en.wikipedia.org/wiki/Commonplace_book" target="_blank">Commonplace Books</a>, or a diary of events that logs what got done today.</p>
<p><article class="align-center media media--type-image media--view-mode-wysiwyg" title="Notebook folders"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/styles/medium/public/pictures/day10-image1.png?itok=lqNpr-FH" width="156" height="220" alt="Notebook folders" title="Notebook folders" class="image-style-medium" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter">(Kevin Sonney, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</p>
</div>
      
  </article></p>
<p>Why should we keep a journal of some sort? The first reason is so that we aren't keeping everything in our heads. Not many of us have an <a href="https://en.wikipedia.org/wiki/Eidetic_memory" target="_blank">Eidetic memory</a>, and maintaining a running log or set of notes makes it easier to reference something we did before. Journals are also easier to share since they can be copy/pasted in chat, email, and so on. "Knowledge is Power. Knowledge shared is Power Multiplied," as <a href="https://en.wikipedia.org/wiki/Robert_Boyce" target="_blank">Robert Boyce</a> famously said, and the sharing of knowledge is an intrinsic part of Open Source.</p>
<p><article class="align-center media media--type-image media--view-mode-wysiwyg" title="Today's journal"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/styles/medium/public/pictures/day10-image2.png?itok=Gr_DysDU" width="220" height="104" alt="Today's journal" title="Today's journal" class="image-style-medium" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter">Today's journal (Kevin Sonney, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</p>
</div>
      
  </article></p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on productivity</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/downloads/collaboration-tools-ebook">5 open source collaboration tools</a></li>
<li><a href="https://opensource.com/downloads/organization-tools">6 open source tools for staying organized</a></li>
<li><a href="https://opensource.com/downloads/desktop-tools">7 open source desktop tools</a></li>
<li><a href="https://opensource.com/tags/tools">The latest on open source tools</a></li>
</ul></div>
</div>
</div>
</div>
<p>One of the critical points when journaling events is to make it fast, simple, and easy. The easiest way is to open a document, add a line with the current date and the note, and save.</p>
<p>Several programs or add-ons are available to make this easier. <a href="https://help.gnome.org/users/gnote/unstable/addin-noteoftheday.html.en" target="_blank">The GNote Note of the Day Plugin</a> automatically creates a note titled with the date and can be used to store content for just that day.</p>
<p>Emacs Org has a hotkey combination to "capture" things and put them into a document. Combined with the <a href="https://github.com/bastibe/org-journal" target="_blank">org-journal</a> add-on, this will create entries in a document for the day it was created.</p>
<p>The KNotes component of Kontact automatically adds the date and time to new notes.</p>
<p><article class="align-center media media--type-image media--view-mode-wysiwyg" title="Finding a note"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/styles/medium/public/pictures/day10-image3.png?itok=LFLFov5c" width="220" height="21" alt="Finding a note" title="Finding a note" class="image-style-medium" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>Finding a note (Kevin Sonney, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</p>
</div>
      
  </article></p>
<p>Keeping a journal or record of things is a handy way of keeping track of what and how something was done. And it can be useful for more than just "I did this" - it can also include a list of books read, foods eaten, places visited, and a whole host of information that is often useful in the future.</p>
