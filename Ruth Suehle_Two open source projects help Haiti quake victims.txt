<h3>Ushahidi</h3>
<p>Ushahidi means "testimony" in Swahili. Ushahidi.com was originally created by citizen journalists to map violence and peace efforts in Kenya after the 2008 elections. The Ushahidi Engine is a mashup of user-generated reports with Google Maps. It is now used around the world from its roots in Kenya to Atlanta, GA to India.</p>
<p>Now haiti.ushahidi.com is being used to track the status of the earthquake aftermath in Haiti. The map shows the situation in several categories:</p>
<ul><li>Emergency (includes collapsed structures, fires, trapped people, and contaminated water) </li>
<li>Threats (structures at risk and looting) </li>
<li>Vital lines (water shortages, blocked roads, power outages) </li>
<li>Response (includes health services, food and shelter, and search and rescue) </li>
<li>Persons news (deaths and missing persons) </li>
</ul><p>Cell tower availability is spotty, but people in Haiti can text 4636 (International:447624802524) on Digicel with their location and reports of emergencies or missing persons.</p>
<p>The Haiti Ushahidi site has aggregated a <a href="http://haiti.ushahidi.com/help">list of ways to help</a>. The greater Ushahidi project also needs help--they're still looking for developers. <a href="http://www.ushahidi.com/join">Learn how to join the project</a> if you'd like to get involved.</p>
<p> </p>
<h3>CrisisCommons::Haiti</h3>
<h2><img src="https://opensource.com/sites/default/files/resize/images/life/crisiscommons-520x289.jpg" alt="Photo by Leslie Jenkins" title="CrisisCamp in Washington, DC" class="mceItem" height="289" width="520" /></h2>
<p><a href="http://www.flickr.com/photos/lesliejenkins/4284656284/"><em>CrisisCamp photo by Leslie Jenkins</em></a></p>
<p><a href="http://www.crisiscommons.org">CrisisCommons</a> describes itself as "an international network of professionals drawn together by a call to service. [They] create the technological tools and resources for responders to use in mitigating disasters and crises around the world." Everything CrisisCommons creates is open source.</p>
<p>CrisisCommons::Haiti is one of its sub-projects working to help Haiti by organizing developers, volunteers, agencies, and responders.  On Jan. 16, more than 200 volunteers gathered in Washington, DC, in addition to groups in Silicon Valley, Los Angeles, Brooklyn, London, and Boulder/Denver through CrisisCommons. These groups called CrisisCamps collaborated on technology projects for Haiti relief efforts--mapping, data, technical help for other agencies, etc. <a href="http://www.serve.gov/stories_detail.asp?tbl_servestories_id=326">Read more about CrisisCamp.</a></p>
<p><em>Learn more about CrisisCommons::Haiti</em></p>
<ul><li><a href="http://haiti.crisiscommons.org/">Read the blog</a> </li>
<li><a href="http://haiti.crisiscommons.org/topics/projects/">See the projects list</a> </li>
<li><a href="http://wiki.crisiscommons.org/index.php?title=Haiti/2010_Earthquake">Visit their wiki</a> </li>
<li><a href="http://search.twitter.com/search?q=%23cchaiti">See tweets tagged #cchaiti</a></li>
</ul><p><em>Join a CrisisCamp Haiti on </em><em>Saturday, Jan. 23, </em></p>
<p><strong> </strong></p>
<ul><li> <a href="http://www.eventbrite.com/event/541831633">Boston</a></li>
<li> <a href="http://www.eventbrite.com/event/539521724">Boulder/Denver</a></li>
<li> <a href="http://crisiscampbrooklyn.eventbrite.com/">Brooklyn</a></li>
<li> <a href="http://crisiscamphaitila2.eventbrite.com/">Los Angeles</a></li>
<li> <a href="http://crisiscampmiami.eventbrite.com/">Miami</a></li>
<li> <a href="http://crisiscamphaitipdx.eventbrite.com/">Portland</a></li>
<li> <a href="http://crisiscamphaitisiliconvalley.eventbrite.com/">Silicon Valley</a></li>
<li> <a href="http://crisiscamphaitiwdc.eventbrite.com/">Washington, DC</a></li>
</ul>