<p>Ah, so many file formats—especially audio and video ones—can make for fun times if you get a file with an extension you don't recognize, if your media player doesn't play a file in that format, or if you want to use an open format.</p>
<p>So, what can a Linux user do? Turn to one of the many open source media conversion tools for the Linux desktop, of course. Let's take a look at four of them.</p>
<h2>Gnac</h2>
<p><article class="align-center media media--type-image media--view-mode-full" title="Gnac"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/gnac.png" width="502" height="533" alt="Gnac" title="Gnac" /></div>
      
  </article></p>
<p><a href="http://gnac.sourceforge.net" target="_blank">Gnac</a> is one of my favorite audio converters and has been for years. It's easy to use, it's powerful, and it does one thing well—as any top-notch utility should.</p>
<p>How easy? You click a toolbar button to add one or more files to convert, choose a format to convert to, and then click <b>Convert</b>. The conversions are quick, and they're clean.</p>
<p>How powerful? Gnac can handle all the audio formats that the <a href="http://www.gstreamer.net/" target="_blank">GStreamer</a> multimedia framework supports. Out of the box, you can convert between Ogg, FLAC, AAC, MP3, WAV, and SPX. You can also change the conversion options for each format or add new ones.</p>
<h2>SoundConverter</h2>
<p><article class="align-center media media--type-image media--view-mode-full" title="SoundConverter"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/soundconverter.png" width="506" height="452" alt="SoundConverter" title="SoundConverter" /></div>
      
  </article></p>
<p>If simplicity with a few extra features is your thing, then give <a href="http://soundconverter.org/" target="_blank">SoundConverter</a> a look. As its name states, SoundConverter works its magic only on audio files. Like Gnac, it can read the formats that GStreamer supports, and it can spit out Ogg Vorbis, MP3, FLAC, WAV, AAC, and Opus files.</p>
<p>Load individual files or an entire folder by either clicking <b>Add File</b> or dragging and dropping it into the SoundConverter window. Click <b>Convert</b>, and the software powers through the conversion. It's fast, too—I've converted a folder containing a couple dozen files in about a minute.</p>
<p>SoundConverter has options for setting the quality of your converted files. You can change the way files are named (for example, include a track number or album name in the title) and create subfolders for the converted files.</p>
<h2>WinFF</h2>
<p><article class="align-center media media--type-image media--view-mode-full" title="WinFF"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/winff.png" width="596" height="532" alt="WinFF" title="WinFF" /></div>
      
  </article></p>
<p><a href="https://www.biggmatt.com/winff/" target="_blank">WinFF</a>, on its own, isn't a converter. It's a graphical frontend to FFmpeg, which <a href="https://opensource.com/article/17/6/ffmpeg-convert-media-file-formats">Tim Nugent looked at</a> for Opensource.com. While WinFF doesn't have all the flexibility of FFmpeg, it makes FFmpeg easier to use and gets the job done quickly and fairly easily.</p>
<p>Although it's not the prettiest application out there, WinFF doesn't need to be. It's more than usable. You can choose what formats to convert to from a dropdown list and select several presets. On top of that, you can specify options like bitrates and frame rates, the number of audio channels to use, and even the size at which to crop videos.</p>
<p>The conversions, especially video, take a bit of time, but the results are generally quite good. Once in a while, the conversion gets a bit mangled—but not often enough to be a concern. And, as I said earlier, using WinFF can save me a bit of time.</p>
<h2>Miro Video Converter</h2>
<p><article class="align-center media media--type-image media--view-mode-full" title="Miro Video Converter"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/miro-main-window.png" width="506" height="660" alt="Miro Video Converter" title="Miro Video Converter" /></div>
      
  </article></p>
<p>Not all video files are created equally. Some are in proprietary formats. Others look great on a monitor or TV screen but aren't optimized for a mobile device. That's where <a href="http://www.mirovideoconverter.com/" target="_blank">Miro Video Converter</a> comes to the rescue.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Audio and Video</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/article/18/4/new-state-video-editing-linux?intcmp=7016000000127cYAAQ">The current state of Linux video editing</a></li>
<li><a href="https://opensource.com/article/18/2/open-source-audio-visual-production-tools?intcmp=7016000000127cYAAQ">23 open source audio-visual production tools</a></li>
<li><a href="https://opensource.com/article/17/8/linux-guitar-amp?intcmp=7016000000127cYAAQ">How to make a low-cost guitar amp with Linux</a></li>
<li><a href="https://opensource.com/downloads/blender-hotkey-cheat-sheet?intcmp=7016000000127cYAAQ">Blender Cheat Sheet</a></li>
<li><a href="https://opensource.com/tags/audio-and-music?intcmp=7016000000127cYAAQ">Latest audio and music articles</a></li>
<li><a href="https://opensource.com/tags/video-editing?intcmp=7016000000127cYAAQ">Latest video editing articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>Miro Video Converter has a heavy emphasis on mobile. It can convert video that you can play on Android phones, Apple devices, the PlayStation Portable, and the Kindle Fire. It will convert most common video formats to MP4, <a href="https://en.wikipedia.org/wiki/WebM" target="_blank">WebM</a>, and <a href="https://en.wikipedia.org/wiki/Ogg_theora" target="_blank">Ogg Theora</a>. You can find a full list of supported devices and formats <a href="http://www.mirovideoconverter.com/" target="_blank">on Miro's website</a>.</p>
<p>To use it, either drag and drop a file into the window or select the file that you want to convert. Then, click the Format menu to choose the format for the conversion. You can also click the Apple, Android, or Other menus to choose a device for which you want to convert the file. Miro Video Converter resizes the video for the device's screen resolution.</p>
<hr /><p>Do you have a favorite Linux media conversion application? Feel free to share it by leaving a comment.</p>
