<p id="mistakes-from-my-first-open-source-contribution-part-4">Previously, I explained <a href="https://opensource.com/article/19/10/first-open-source-contribution-fork-clone">the importance of forking repositories</a>. Once I finished the actual "writing the code" part of making my first open source pull request, I felt excellent. It seemed like the hard part was finally over. What’s more, I felt great about the code that I wrote.</p>
<p>One thing that I decided to do (which turned out to be an excellent choice) was to use <a href="https://opensource.com/article/19/10/test-driven-development-best-practices">test-driven development</a> (TDD) to write the code. Using TDD was helpful because it gave me a place to start, and a way to know if what I was doing actually worked. Because my background was in building web apps, I rarely ran into the problem of writing code that didn’t have a tangible, visible output. The test-first approach helped me make the leap into working on a tool where you can’t evaluate your progress manually. The fact that I had written a clear test also helped me ultimately get my pull request accepted. The reviewer highlighted the test in his comments on my code.</p>
<p>Another thing I felt great about was that I had accomplished the whole thing in around 20 lines of code. I know from experience that shorter pull requests are much easier to review. Such short pieces generally take less time, and the reviewer can concentrate on only the small number of lines that were changed. I hoped that this would increase my chances that one of the maintainers would look at my work and feel confident in it.</p>
<p>Much to my surprise, when I finally pushed my branch to GitHub, the diff was showing that I had changed multiple lines of code. I ran into trouble here because I had become too comfortable with my usual development setup. Because I typically work on a single project, I barely think about some of the tools I have working in the background to make my life easier. The culprit here was <a href="https://prettier.io/" target="_blank"><code>prettier</code></a>, a code formatter that automatically fixes all of my minor spacing and syntax discrepancies when I save an edited file. In my usual workflow, this tool is extremely helpful. Most of the developers I work with have <code>prettier</code> installed, so all of the code that we write obeys the same style rules.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Great Content</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=7016000000127cYAAQ">Free online course: RHEL technical overview</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=7016000000127cYAAQ">Learn Advanced Linux Commands</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Download Cheat Sheets</a></li>
<li><a href="https://opensource.com/alternatives?intcmp=7016000000127cYAAQ">Find an Open Source Alternative</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=7016000000127cYAAQ">Read Top Linux Content</a></li>
<li><a href="https://opensource.com/resources?intcmp=7016000000127cYAAQ">Check out open source resources</a></li>
</ul></div>
</div>
</div>
</div>
<p>In this new project, however, style rules had fallen by the wayside. The project did, in fact, contain an eslint config stating that single quotes should be used instead of double-quotes. However, the developers who were contributing to the project ignored this rule and used both single- and double-quotes. Unlike human beings, <code>prettier</code> never ignores the rules. While I was working, it took the initiative to turn every double quote in every file I changed to a single quote, causing hundreds of unintentional changes.</p>
<p>I tried for a few minutes to remove these changes, but because they had been continually happening as I worked, they were embedded in all of my commits. Then the type-B in me took over and decided to leave the changes in. "Maybe this is not a big deal," I thought. "They said they wanted single quotes, after all."</p>
<p>My mistake was including these unrelated changes in my PR. While I was technically right that this wasn’t a "big deal," the maintainer who reviewed my code asked me to revert the changes. My initial instinct, that keeping my pull request small and to the point, was correct.</p>
<p>The lesson here is that you should keep your changes as minimal and to-the-point as possible. Be mindful of any tools you have that might apply to your normal workflow, but aren’t as useful if you are working on a new project.</p>
<p class="rteindent1"><strong>Free idea:</strong> If you are looking for a way to get an open source PR in without writing any code, pick a project that doesn’t adhere to its style guide, run <code>prettier</code> on it, and make the result your whole pull request. It’s not guaranteed that every project community will appreciate this, but it’s worth a shot.</p>
