<p>Over the last five years, a massive shift in how applications get deployed has occurred. It’s gone from self-hosted infrastructure to the world of the cloud and Kubernetes clusters. This change in deployment practices brought a lot of new things to the world of developers, including <a href="https://opensource.com/resources/what-are-linux-containers">containers</a>, <a href="https://www.redhat.com/architect/what-is-rosa?intcmp=7013a000002qLH8AAM" target="_blank">cloud provider configuration</a>, <a href="https://www.redhat.com/en/topics/containers/what-is-kubernetes?intcmp=7013a000002qLH8AAM" target="_blank">container orchestration</a>, and more. There’s been a shift away from coding monoliths towards cloud-native applications consisting of multiple microservices.</p>
<p>While application deployment has advanced, the workflows and tooling for development have largely remained stagnant. They didn’t adapt completely or feel “native” to this brave new world of cloud-native applications. This can mean an unpleasant developer experience, involving a massive loss in developer productivity.</p>
<p>But there’s a better way. What if you could seamlessly integrate Kubernetes and unlimited cloud resources with your favorite local development tools?</p>
<h2>The current state of cloud-native development</h2>
<p>Imagine that you’re building a cloud-native application that includes a Postgres database in a <a href="https://www.redhat.com/en/technologies/cloud-computing/openshift/aws?intcmp=7013a000002qLH8AAM" target="_blank">managed application platform</a>, a data set, and three different microservices.</p>
<p>Normally, this would involve the following steps:</p>
<ol type="1"><li>Open a ticket to get your IT team to provision a DB in your corporate AWS account.</li>
<li>Go through documentation to find where to get a copy of last week’s DB dump from your staging environment (you are not using prod data in dev, right?)</li>
<li>Figure out how to install and run service one on your local machine</li>
<li>Figure out how to install and run service two on your local machine</li>
<li>Figure out how to install and run service three on your local machine</li>
</ol><p>And that’s just to get started. Once you’ve made your code changes, you then have to go through these steps to test them in a realistic environment:</p>
<ol type="1"><li>Create a Git branch</li>
<li>Commit your changes</li>
<li>Figure out a meaningful commit message</li>
<li>Push your changes</li>
<li>Wait your turn in the CI queue</li>
<li>CI builds your artifacts</li>
<li>CI deploys your application</li>
<li>You finally validate your changes</li>
</ol><p>I’ve worked with teams where this process takes anything from a few minutes to several hours. But as a developer, waiting even a few minutes to see whether my code works was a terrible experience. It was slow, frustrating, and made me dread making complex changes.</p>
<h2>Simplify your cloud-native development workflow with Crossplane and Okteto</h2>
<p><a href="https://github.com/crossplane/crossplane" target="_blank">Crossplane</a> is an open source project that connects your Kubernetes cluster to external, non-Kubernetes resources and allows platform teams to build a custom Kubernetes API to consume those resources. This enables you to do something like <code>kubectl apply -f db.yaml</code> to create a database in any cloud provider. And this enables your DevOps or IT team to give you access to cloud infra without having to create accounts, distribute passwords, or manually limit what you can or can’t do. It's self-service heaven.</p>
<p>The <a href="https://github.com/okteto/okteto" target="_blank">Okteto CLI</a> is an open source tool that enables you to build, develop, and debug cloud native applications directly in any Kubernetes cluster. Instead of writing code, building, and then deploying in Kubernetes to see your changes, you simply run <code>okteto up</code>, and your code changes are synchronized in real time. At the same time, your application is hot-reloaded in the container. It’s a fast inner loop for cloud-native applications.</p>
<p>On their own, each of these tools is very useful, and I recommend you try them both. The Crossplane and Okteto projects enable you to build a great developer experience for you and your team, making building cloud-native applications easier, faster, and joyful.</p>
<p>Here’s the example I mentioned in the previous section, but instead of a traditional setup, imagine you’re using Crossplane and Okteto:</p>
<ol type="1"><li>You type <code>okteto up</code></li>
<li>Okteto deploys your services in Kubernetes while Crossplane provisions your database (and data!)</li>
<li>Okteto synchronizes your code changes and enables hot-reloading in all your services</li>
</ol><p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">More on Kubernetes</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://www.redhat.com/en/topics/containers/what-is-kubernetes?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="What is Kubernetes?">What is Kubernetes?</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/services/training/do080-deploying-containerized-applications-technical-overview?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Free online course: Containers, Kubernetes and Red Hat OpenShift technical over…">Free online course: Containers, Kubernetes and Red Hat OpenShift technical over…</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/engage/openshift-storage-testdrive-20170718?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Test drive OpenShift hands-on">Test drive OpenShift hands-on</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/resources/managing-containers-kubernetes-openshift-technology-detail?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="An introduction to enterprise Kubernetes">An introduction to enterprise Kubernetes</a></div>
              <div class="field__item"><a href="https://enterprisersproject.com/article/2017/10/how-explain-kubernetes-plain-english?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="How to explain Kubernetes in plain terms">How to explain Kubernetes in plain terms</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/kubernetes-raspberry-pi?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="eBook: Running Kubernetes on your Raspberry Pi homelab">eBook: Running Kubernetes on your Raspberry Pi homelab</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/kubernetes-cheat-sheet?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Kubernetes cheat sheet">Kubernetes cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/kubernetes-sysadmin?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="eBook: A guide to Kubernetes for SREs and sysadmins">eBook: A guide to Kubernetes for SREs and sysadmins</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Register for your free Red Hat account">Register for your free Red Hat account</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/kubernetes?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Latest Kubernetes articles">Latest Kubernetes articles</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<p>At this point, you have a live environment in Kubernetes, just for you. You saved a ton of time by not having to go through IT, figuring out local dependencies, and remembering the commands needed to run each service. And because everything is defined as code, it means that everyone in your team can get their environment in exactly the same way. No degree in cloud infrastructure required.</p>
<p>But there’s one more thing. Every time you make a code change, Okteto automatically refreshes your services without requiring you to commit code. There’s no waiting for artifacts to build, no redeploying your application, or going through lengthy CI queues. You can write code, save the file, and see your changes running live in Kubernetes in less than a second.</p>
<p>How’s that for a fast cloud-native development experience?</p>
<h2>Get into the cloud</h2>
<p>If you’re building applications meant to run in Kubernetes, why are you not developing in Kubernetes?</p>
<p>Using Crossplane and Okteto together gives your team a fast cloud-native development workflow. By introducing Crossplane and Okteto into your team:</p>
<ul><li>Everyone on your team can spin up a fully-configured environment by running a single command</li>
<li>Your cloud development environment spans Kubernetes-based workloads, as well as cloud services</li>
<li>Your team can share a single Kubernetes cluster instead of having to spin up one cluster on every developer machine, CI pipeline, and so on</li>
<li>Your development environment looks a lot like your production environment</li>
<li>You don’t have to train every developer on Kubernetes, containers, <a href="https://opensource.com/article/22/3/cloud-services-providers" target="_blank">cloud providers</a>, and so on.</li>
</ul><p>Just type <code>okteto up</code>, and you’re developing within seconds!</p>
