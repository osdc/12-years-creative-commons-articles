<p>Given the interest in my earlier article about <a title="Organic Software: A Software Freedom Scorecard" href="http://webmink.com/essays/scorecard/">a scorecard</a> for open source and my own rough-and-ready <a href="https://opensource.com/life/11/2/open-rule-governance-benchmark">benchmark proposal</a>, I’d be interested in seeing how well the benchmark works at rating a variety of open source projects. If you’re familiar enough with a project to be willing to have your name associated with rating it, please complete the table below in the same style as my own <a href="http://webmink.com/2011/02/04/rating-openjdk-governance/">evaluation of OpenJDK</a>. Cut and paste into an e-mail and send the  completed table to <a href="mailto:ratings@webmink.com&amp;subject=%22Governance%20Benchmark%20For%20My%20Project">me</a>. After review, I’ll publish all  valid good-faith submissions on my blog.</p>
<table border="2" cellspacing="3" rules="all"><thead><tr><th scope="col">Rule</th>
<th scope="col">Data</th>
<th scope="col">Evaluation</th>
<th scope="col" align="center">Score</th>
</tr></thead><tbody><tr><td><em>Instructions </em></td>
<td><em>Provide sample extracts from public sources supporting your evaluation, together with links </em></td>
<td><em>Read <a title="The Open-By-Rule Benchmark" href="http://webmink.com/essays/open-by-rule/">the Benchmark</a>. Evaluate as objectively as possible, and conclude with a rationale for the score you are giving. Score -1 for a rule where the governance implementation detracts from open-by-rule; score 0 for implementations that have an overall neutral effect; score +1 for implementations that contribute positively to an open-by-rule community. “Open/Meritocratic/Oligarchy” scores between -3 and +3, evaluating for each word. I’ll review your submission before publication, so don’t worry too much <img src="http://s2.wp.com/wp-includes/images/smilies/icon_smile.gif?m=1256404518g" alt=":-)" class="wp-smiley mceItem" /></em></td>
<td> </td>
</tr><tr><td><em>Open, Meritocratic Oligarchy</em></td>
<td> </td>
<td> </td>
<td align="center">+/-3</td>
</tr><tr><td><em>Modern license</em></td>
<td> </td>
<td> </td>
<td align="center">+/-1</td>
</tr><tr><td><em>Copyright aggregation</em></td>
<td> </td>
<td> </td>
<td align="center">+/-1</td>
</tr><tr><td><em>Trademark policy</em></td>
<td> </td>
<td> </td>
<td align="center">+/-1</td>
</tr><tr><td><em>Roadmap</em></td>
<td> </td>
<td> </td>
<td align="center">+/-1</td>
</tr><tr><td><em>Multiple co-developers</em></td>
<td> </td>
<td> </td>
<td align="center">+/-1</td>
</tr><tr><td><em>Forking feasible</em></td>
<td> </td>
<td> </td>
<td align="center">+/-1</td>
</tr><tr><td><em>Transparency</em></td>
<td> </td>
<td> </td>
<td align="center">+/-1</td>
</tr></tbody><tfoot><tr><td> </td>
<td> </td>
<td><strong>Summary (scale -10 to +10)</strong></td>
<td align="center"><strong>+/-3</strong></td>
</tr></tfoot></table><table border="1" cellspacing="3" rules="all"><tbody><tr><td>Project name</td>
<td width="300"> </td>
</tr><tr><td>Project URL</td>
<td> </td>
</tr><tr><td>Your name</td>
</tr></tbody></table>