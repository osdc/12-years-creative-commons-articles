<p><em>Automation is a hot topic right now. In my day job as a site reliability engineer (SRE), part of my remit is to automate as many repeating tasks as possible. But how many of us do that in our daily, not-work, lives? This year, I am focused on automating away the toil so that we can focus on the things that are important.</em></p>
<p>While automating everything, I ran into some difficulty with remote sites. I'm not a networking person so I started to look at my options. After researching the various virtual private networks (VPN), hardware endpoints, firewall rules, and everything that goes into supporting multiple remote sites, I was confused, grumpy, and frustrated with the complexity of it all.</p>
<p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">Automation with open source</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://www.redhat.com/en/engage/automated-enterprise-ebook-20171115?intcmp=701f2000000h4RcAAI" data-analytics-category="resource list" data-analytics-text="Download now: The automated enterprise eBook">Download now: The automated enterprise eBook</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/services/training/do007-ansible-essentials-simplicity-automation-technical-overview?intcmp=701f2000000h4RcAAI" data-analytics-category="resource list" data-analytics-text="Free online course: Ansible essentials">Free online course: Ansible essentials</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/ansible-cheat-sheet?intcmp=701f2000000h4RcAAI" data-analytics-category="resource list" data-analytics-text="Ansible cheat sheet">Ansible cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/home-automation-ebook?intcmp=701f2000000h4RcAAI" data-analytics-category="resource list" data-analytics-text="eBook: A practical guide to home automation using open source tools">eBook: A practical guide to home automation using open source tools</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/ansible-quickstart?intcmp=701f2000000h4RcAAI" data-analytics-category="resource list" data-analytics-text="A quickstart guide to Ansible">A quickstart guide to Ansible</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/engage/automation-at-edge-20220727?intcmp=701f2000000h4RcAAI" data-analytics-category="resource list" data-analytics-text="eBook: 7 examples of automation on the edge">eBook: 7 examples of automation on the edge</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/topics/automation/understanding-ansible-vs-terraform-puppet-chef-and-salt?intcmp=701f2000000h4RcAAI" data-analytics-category="resource list" data-analytics-text="Understanding Ansible, Terraform, Puppet, Chef, and Salt">Understanding Ansible, Terraform, Puppet, Chef, and Salt</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/engage/automate-redhat-enterprise-20221220?intcmp=701f2000000h4RcAAI" data-analytics-category="resource list" data-analytics-text="eBook: Automate RHEL">eBook: Automate RHEL</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Register for your free Red Hat account">Register for your free Red Hat account</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/automation?intcmp=701f2000000h4RcAAI" data-analytics-category="resource list" data-analytics-text="More articles about open source automation">More articles about open source automation</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<p>Then I found <a href="https://github.com/zerotier" target="_blank">ZeroTier</a>. ZeroTier is an encrypted virtual network backbone, allowing multiple machines to communicate as if they were on a single network. The code is all open source, and you can self-host the controller or use the <a href="https://www.zerotier.com/pricing" target="_blank">ZeroTierOne</a> service with either free or paid plans. I'm using their free plan right now, and it is robust, solid, and very consistent.</p>
<p>Because I'm using the web service, I'm not going to go into detail about running the controller and root services. ZeroTier has a complete reference on how to do that in their <a href="https://docs.zerotier.com" target="_blank">documentation</a>, and it's very good.</p>
<p>After creating my own virtual network in the web user interface, the client installation is almost trivial. ZeroTier has packages for APT, RPM, FreeBSD, and many other platforms, so getting the first node online takes little effort.</p>
<p>Once installed, the client connects to the controller service and generates a unique ID for the node. On Linux, you use the <code>zerotier-cli</code> command to join a network, using the <code>zerotier-cli join NETWORKID</code> command.</p>
<pre>
<code class="language-bash">$ sudo zerotier-cli info
200 info 469584783a 1.x.x ONLINE
</code></pre><p>You can also use <code>zerotier-cli</code> to get a listing of connected and available nodes, change network settings, and leave networks.</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-04/SecondDay02-2.png" width="1984" height="268" alt="Image of Setting up a New Node" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Kevin Sonney, CC BY-SA 4.0)</p>
</div>
      
  </article><p>After joining a network, you do have to approve access for the node, either through the web console or by making a call to the application programming interface (API). Both methods are documented on the ZeroTier site. After you have two nodes connected, connecting to each other — no matter where you are or what side of any firewalls you may be on — is exactly what you would expect if you were in the same building on the same network. One of my primary use cases is for <a href="https://opensource.com/article/22/5/remote-home-assistant" target="_blank">remote access to my Home Assistant setup</a> without needing to open up firewall ports or expose it to the internet (more on my Home Assistant setup and related services later).</p>
<p>One thing I did set up myself is a <a href="https://github.com/zerotier/zeronsd" target="_blank">Beta ZeroNDS Service</a> for internal DNS. This saved me a lot of complexity for managing my own name service or having to create public records for all my private hosts and IP addresses. I found the instructions to be very straight forward, and was able to have a DNS server for my private network up in about 5 minutes. Each client has to allow Zerotier to set the DNS, which is very simple in the GUI clients. To enable it for use on Linux clients, use:</p>
<pre>
<code class="language-bash">$ sudo zerotier-cli setNETWORKID allowDNS=1</code></pre><p>No other updates are needed as you add and remove hosts, and it "just works."</p>
<pre>
<code class="language-bash">$ sudo zerotier-cli info
200 info 469584845a 1.x.y ONLINE
$ sudo zerotier-cli join
93afae596398153a 200 join OK
$ sudo zerotier-cli peers
200 peers
&lt;ztaddr&gt; &lt;ver&gt; &lt;role&gt; &lt;lat&gt; &lt;link&gt; &lt;TX&gt; &lt;RX&gt; &lt;path&gt;
61d294b9cb - PLANET 112 DIRECT 7946 2812 50.7.73.34/9993
62f865ae71 - PLANET 264 DIRECT 7946 2681 50.7.76.38/9993
778cde7190 - PLANET 61 DIRECT 2944 2901 103.195.13.66/9993
93afae5963 1.x LEAF 77 DIRECT 2945 2886 35.188.31.177/41848
992fcf1db7 - PLANET RECT 79124 DI47 2813 195. 181.173.159/9993
</code></pre><p>I've barely scratched the surface of the features here. ZeroTier also allows for bridging between ZeroTier networks, advanced routing rules, and a whole lot more. They even have a <a href="https://github.com/zerotier/terraform-provider-zerotier" target="_blank">Terraform provider</a> and a listing of <a href="https://github.com/zerotier/awesome-zerotier" target="_blank">Awesome Zerotier Things</a>. As of today, I'm using ZeroTier to connect machines across four physical sites, three of which are behind NAT firewalls. Zerotier is simple to set up, and almost completely painless to manage.</p>
