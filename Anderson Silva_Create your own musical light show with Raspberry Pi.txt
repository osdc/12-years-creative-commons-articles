<p>Last Thanksgiving, I took some time off from work and was looking for a fun project to work on during my downtime. I decided to <a href="http://anderson.the-silvas.com/2014/11/26/having-fun-with-the-raspberry-pi-b/" target="_blank">check out</a> Raspberry Pi. After a quick search on Amazon, I ordered the <a href="http://www.amazon.com/CanaKit-Raspberry-Ultimate-Starter-Components/dp/B00G1PNG54/ref=sr_1_1">CanaKit Raspberry Pi B+ Ultimate Starter Kit</a>.</p>
<p><article class="media media--type-image media--view-mode-full"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/image1.jpg" width="520" height="520" /></div>
      
  </article></p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on Raspberry Pi</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/resources/what-raspberry-pi?src=raspberry_pi_resource_menu1&amp;intcmp=701f2000000h4RcAAI">What is Raspberry Pi?</a></li>
<li><a href="https://opensource.com/downloads/raspberry-pi-guide?src=raspberry_pi_resource_menu2&amp;intcmp=701f2000000h4RcAAI">eBook: Guide to Raspberry Pi</a></li>
<li><a href="https://opensource.com/downloads/getting-started-raspberry-pi-cheat-sheet?src=raspberry_pi_resource_menu3&amp;intcmp=701f2000000h4RcAAI">Getting started with Raspberry Pi cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/kubernetes-raspberry-pi?src=raspberry_pi_resource_menu6&amp;intcmp=701f2000000h4RcAAI">eBook: Running Kubernetes on your Raspberry Pi</a></li>
<li><a href="https://www.redhat.com/en/resources/data-intensive-applications-hybrid-cloud-blueprint-detail?src=raspberry_pi_resource_menu7&amp;intcmp=701f2000000h4RcAAI">Whitepaper: Data-intensive intelligent applications in a hybrid cloud blueprint</a></li>
<li><a href="https://www.redhat.com/en/topics/edge-computing?src=raspberry_pi_resource_menu5&amp;intcmp=701f2000000h4RcAAI">Understanding edge computing</a></li>
<li><a href="https://opensource.com/tags/raspberry-pi?src=raspberry_pi_resource_menu4&amp;intcmp=701f2000000h4RcAAI">Our latest on Raspberry Pi </a></li>
</ul></div>
</div>
</div>
</div>

<ul></ul><p>The kit arrived the Friday before Thanksgiving, and I wasted no time getting to work. I didn't need another computer running Linux at home, so my interest in the Raspberry Pi focused on what its hardware could offer that my laptops couldn't.</p>
<p>When I unboxed the kit and saw all the LEDs and wires that came with it, the first thing that came to mind was <a href="https://www.youtube.com/watch?v=D4PaB4TenH4" target="_blank">Osborne Family Spectacle of Dancing Lights</a> at Disney's Hollywood Studios. I had no idea if I'd be able to get the Pi to light up any of those LEDs, but if I could just understand how to make software interact with hardware at this level, my investment would be worth it.</p>
<p>First, being completely biased to anything Fedora or Red Hat-based, I decided to use <a href="http://www.raspberrypi.org/introducing-noobs/" target="_blank">NOOBS</a> to install <a href="http://pidora.ca/" target="_blank">Pidora</a>. But as cool as the Fedora spinoff was, I quickly realized it wasn't the Linux distribution I needed to make my Christmas light show.</p>
<p>I went with the Raspberry Pi's recommendation and installed <a href="http://www.raspbian.org/" target="_blank">Raspbian</a>. It quickly proved itself to be the better distribution for the job for a few reasons:</p>
<ol><li><a href="http://elinux.org/RPi_raspi-config" target="_blank">raspi_config</a> allowed me to do quite a bit of configuration changes to the Raspberry Pi right off the bat.</li>
<li>Raspbian's out-of-the-box needed about 50 percent less memory than Pidora, which is HUGE for a little piece of hardware that has 512 MB!</li>
<li>It came with <a href="http://www.wolfram.com/raspberry-pi/" target="_blank">Mathematica</a>, <a href="http://www.raspberrypi.org/learning/getting-started-with-scratch/" target="_blank">Scratch</a>, <a href="http://sonic-pi.net/" target="_blank">Sonic Pi</a>, and the Python libraries I needed to do some coding with the GPIO pins.</li>
<li>The Raspberry Pi <a href="http://www.amazon.com/Raspberry-User-Guide-Eben-Upton/dp/1118921666/" target="_blank">book</a> (and foundation) recommend Raspbian, so I had two places to look for guidance and examples.</li>
</ol><p><article class="media media--type-image media--view-mode-full"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/image2.jpg" width="520" height="347" /></div>
      
  </article></p>
<p>By Friday night, I had the Pi connected to my TV and Wi-Fi. With Raspbian installed, I ran an: <code>apt-get update &amp;&amp; apt-get upgrade</code> to update the OS and was ready to dive in to all the extras that came with the kit.</p>
<p>I had learned a little about electricity in high school and college, but never really had a chance to apply that knowledge anywhere else. Terms like "breadboard," "jumper wires," "resistors," "current," and "voltage" weren't foreign to me, but I had very little hands-on experience with them. So, what did I do? Googled them!</p>
<p>First, I used <a href="https://www.youtube.com/watch?v=k9jcHB9tWko" target="_blank">this video</a> to learn about the breadboard, LEDs, and resistors. I then applied that concept to the CanaKit. Here's a video with a quick video I made of how I put it all together:</p>
<p><iframe allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/DzzdyzDdMKU" width="520"></iframe></p>
<p>Now that I had the lights working, the next step was making the LEDs "listen to music" and blink accordingly?</p>
<p>With another Google search and I found an awesome open source project called: <a href="https://bitbucket.org/togiles/lightshowpi/wiki/Home">LightshowPi</a>. I quickly realized that the idea of an Osborne Family Spectacle of Dancing Lights of my own wasn't such a silly idea after all. After joining the LightshowPi <a href="https://plus.google.com/communities/101789596301454731630" target="_blank">Google+ Community</a>, I read up on a few tips and tricks for setting up my very own light show.</p>
<p><article class="media media--type-image media--view-mode-full"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/image3.jpg" width="520" height="293" /></div>
      
  </article></p>
<p>I went ahead and ordered the next part of the puzzle: a <a href="http://www.sainsmart.com/8-channel-5v-solid-state-relay-module-board-omron-ssr-4-pic-arm-avr-dsp-arduino.html">Sainsmart eight-channel 5V solid state relay (SSR) module board</a>. It took a couple of weeks to arrive at my doorstep, but when it did I went to the store right away to buy the rest of the materials I needed for my light show.</p>
<p><iframe allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/wZ-lfn3oZiU" width="520"></iframe></p>
<p>Once I tested the connection with the board, it was time for me hook up the Christmas lights I was going to put on my Christmas tree. The final result was simply one of the most satisfyingly geeky things I have ever done. For most of December, I left my Raspberry Pi connected to my Christmas tree. With a simple SSH client on my phone, I could easily start/stop the show any time I wanted.</p>
<p>Here's the final result:</p>
<p><iframe allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/u2wVxuOD8ww" width="520"></iframe></p>
<p>Below is a comprehensive set of steps you can take to make your own:</p>
<ol><li>Watch the breadboard <a href="https://www.youtube.com/watch?v=k9jcHB9tWko">tutorial</a>.</li>
<li>Watch my canakit <a href="https://www.youtube.com/watch?v=DzzdyzDdMKU">tutorial</a>.</li>
<li>Once you are comfortable with that, look at the <a href="https://bitbucket.org/togiles/lightshowpi/wiki/Home">LightshowPi</a> project. Learn how to use it. <a href="https://www.youtube.com/watch?v=ZzF9u41EgCU">Here's an example</a> of the LEDs being controlled by LightshowPi.</li>
<li>Join LightshowPi's <a href="https://plus.google.com/communities/101789596301454731630">Google+ community</a>, read up the projects and photos there.</li>
<li>Once you've done all that and understand what's going, order the <a href="http://www.sainsmart.com/8-channel-5v-solid-state-relay-module-board-omron-ssr-4-pic-arm-avr-dsp-arduino.html">Sainsmart eight-channel 5V solid state relay module board</a>. (mine took two weeks to arrive)</li>
<li>Watch <a href="https://www.youtube.com/watch?v=Z2B67hybdAA">this video</a> on how to connect your Raspberry Pi to the SSR board.</li>
<li>Purchase six polarized extension cords at Target: five for the light string and one for the power source. I only used five of the eight channels on the SSR board.</li>
<li>Following this <a href="https://plus.google.com/photos/100867431155529957394/albums/6085653216891072897">step-by-step guide</a>, cut the wires and daisy chain the power source into the relay, and that's it!</li>
</ol><p>Note: Make sure you have something to cut the wires with, a small screwdriver to tighten the SSR board slots, electrical tape, and a jackknife.</p>
<div class="series-wide">Open<br />
Hardware<br />
Connection</div>
<p>
<em>This article is part of the <a href="https://opensource.com/tags/open-hardware-column" target="_blank" title="Open Hardware Connection column">Open Hardware Connection column</a> coordinated by Rikki Endsley. Share your stories about the growing open hardware community and the fantastic projects coming from makers and tinkers around the world by contacting us at <a href="mailto:open@opensource.com">open@opensource.com</a></em>.</p>
