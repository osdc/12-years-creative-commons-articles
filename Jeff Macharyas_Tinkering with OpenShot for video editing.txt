<p>Although I love using open source software, I work for an organization that relies on Adobe. Most of what I do is created in Adobe, but whenever I get a chance to branch out, I turn to open source first to supplement and enhance my work. I had an opportunity recently to do just that.</p>
<p>I was tasked with creating some graphics for a reunion of a university's IT helpdesk crew. Part of the project consisted of video interviews that people recorded with their smartphones. I had to put the videos together into some sort of creative package and upload it to a YouTube channel.</p>
<p>Because I have access to the entire Adobe suite, I fiddled around with Premiere and After Effects. For what I needed to do, they were too complicated. So, I went in search of open source video editors that would work better for me and my simple project. I first turned to Opensource.com to see what others have recommended: I tried a few (<a href="https://opensource.com/search/apachesolr_search/blender">Blender</a>, for example), then hit upon <a href="https://github.com/OpenShot" target="_blank">OpenShot Video Editor</a> and <a href="http://www.openshot.org/download/" target="_blank">downloaded it for Mac</a>.</p>
<h2>Getting started with OpenShot</h2>
<p>It installed quickly and the interface was simple to understand. I was on my way. I created some text-graphic cards in Photoshop for each person's intro. I had to convert the final layer to a smart object, then export it as an SVG file. After that, I simply clicked the green plus sign in OpenShot and added the SVG files to my projects.</p>
<p class="rtecenter">
<article class="media media--type-image media--view-mode-full" title="Editing video in OpenShot"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/openshot-video.png" width="700" height="394" alt="Editing video in OpenShot" title="Editing video in OpenShot" /></div>
      
  </article></p>
<p>Then, I added the video clips, which were recorded as MP4 files, and I had everything I needed.</p>
<p>Once I dragged the elements to the timeline, I added some simple fade transitions between the cards and video clips. Because I had several interviewees, I saved each project with a new name, deleted the previous clips and name cards, and added the new assets. I had to move things around on the timeline to get them to match up since each clip was a different length, but that was easy.</p>
<p>Now it was time to view my creations! I exported the videos as MP4 files and opened one in Google Chrome. Video played great. No sound. Tried it in Firefox. Same result. Tried it in Safari and Opera: video and sound played. What to do?</p>
<p>I opened the MP4 file with the open source video player <a href="http://www.videolan.org/index.html" target="_blank">VLC</a>, then used the Convert/Stream function in VLC.</p>
<p class="rtecenter">
<article class="media media--type-image media--view-mode-full" title="Encapsulating video as webm"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/save-as-webm.png" width="700" height="439" alt="Encapsulating video as webm" title="Encapsulating video as webm" /></div>
      
  </article></p>
<p>I chose <strong>WebM</strong> as the encapsulation. This also made the file much smaller. I tried it in all four browsers, and it worked just fine. Problem solved!</p>
<p>Uploading the final videos to <a href="https://www.youtube.com/user/SLUInfotech" target="_blank">YouTube</a> was much simpler. I had no issues with that.</p>
<p>It's important to remember that open source tools do not have to be used to the exclusion of closed source tools or vice versa. Depending on the project, they can both contribute. It's just a matter of finding the right tools and applying them to your particular situation.</p>
