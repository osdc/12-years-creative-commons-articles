<p><a href="https://opensource.com/article/22/3/open-source-collaboration-carbonio" target="_blank">Carbonio Community Edition (Carbonio CE)</a> is an open source no-cost email and collaboration platform by Zextras. It provides privacy for organizations seeking digital sovereignty by using on-premises self-hosted servers. Using self-hosted servers offers a deeper level of control over infrastructure and data. However, it requires more attention to server configurations and infrastructure management to guarantee data sovereignty. Tasks done by system administrators play an important role in this matter. This makes administrative tasks a crucial part of achieving digital sovereignty, therefore, an administrative console dedicated to such tasks becomes extremely valuable to facilitate sysadmins' everyday jobs.</p>
<p>This is why Zextras launched the first release of its own admin panel for Carbonio CE on October 2022. For Carbonio CE system administrators, it is the first step toward the creation of an all-inclusive admin console.</p>
<p>In this article, I go into detail about the Carbonio CE Admin Panel and take a deeper look into what it can accomplish.</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-11/carbonio_admin_panel.webp" width="2560" height="1463" alt="Image of the Carbonio admin panel." /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Arman Khosravi, CC BY-SA 4.0)</p>
</div>
      
  </article><h2>What is the Carbonio CE Admin Panel?</h2>
<p>The Carbonio CE Admin Panel is designed to assist the Carbonio CE system administrators with the most repetitive and frequent tasks such as user management and domain configuration. It is a browser-based application that runs on a particular port and is available for system administrators to use in production environments as soon as Carbonio CE is installed.</p>
<p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">More for sysadmins</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://www.redhat.com/sysadmin/?intcmp=7013a000002CxqpAAC" data-analytics-category="resource list" data-analytics-text="Enable Sysadmin blog">Enable Sysadmin blog</a></div>
              <div class="field__item"><a href="https://go.redhat.com/automated-enterprise-ebook-20180920?intcmp=7013a000002CxqpAAC" data-analytics-category="resource list" data-analytics-text="The Automated Enterprise: A guide to managing IT with automation">The Automated Enterprise: A guide to managing IT with automation</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/ansible-quickstart?intcmp=7013a000002CxqpAAC" data-analytics-category="resource list" data-analytics-text="eBook: Ansible automation for Sysadmins">eBook: Ansible automation for Sysadmins</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/engage/system-administrator-guide-s-202107300146?intcmp=7013a000002CxqpAAC" data-analytics-category="resource list" data-analytics-text="Tales from the field: A system administrator's guide to IT automation">Tales from the field: A system administrator's guide to IT automation</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/kubernetes-sysadmin?intcmp=7013a000002CxqpAAC" data-analytics-category="resource list" data-analytics-text="eBook: A guide to Kubernetes for SREs and sysadmins">eBook: A guide to Kubernetes for SREs and sysadmins</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Register for your free Red Hat account">Register for your free Red Hat account</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/sysadmin?intcmp=7013a000002CxqpAAC" data-analytics-category="resource list" data-analytics-text="Latest sysadmin articles">Latest sysadmin articles</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<h2 id="why-do-you-need-the-admin-panel">Why do you need the admin panel?</h2>
<p>Everything done in Carbonio CE Admin Panel can be done through the command-line interface as well. This raises the question: why might system administrators prefer using the admin panel rather than the command-line interface?</p>
<p>Using the admin panel has its own obvious advantages such as:</p>
<ul><li>
<p>Making repetitive activities much easier to perform</p>
</li>
<li>
<p>Saving system administrators' time monitoring servers</p>
</li>
<li>
<p>Providing a much easier learning process for junior system administrators</p>
</li>
</ul><p>Even though using the admin panel makes administrative tasks easier to perform, there is more to using this native user interface for Carboino CE. In essence, the Carbonio CE Admin Panel gives you the ability to monitor and manage your organization server from a single centralized location. Even when you're far away, you may still access your admin panel to check the status of servers and carry out various administrative activities.</p>
<h2 id="creating-and-managing-user-accounts">Creating and managing user accounts</h2>
<p>Managing users has always been one of the most, if not the most, performed action by sysadmins. Therefore it should be an essential part of every administrative GUI available for system administrators. Suppose you, as the system administrator of the company have received some request by users to edit some information on their account. For instance, giving them access to some features, or your company has hired new employees, or some employees have left the company. All these scenarios require a sysadmin to manage user accounts frequently.</p>
<p>Using the Carbonio CE Admin Panel you can simply go to <strong>Domains &gt; select a domain &gt; Accounts</strong> and select any account to modify, or press the<strong> + </strong>button to add a new account.</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-11/accounts_carbonio_admin_panel.webp" width="2560" height="1464" alt="Image of the accounts Carbonio adminpanel." /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Arman Khosravi, CC BY-SA 4.0)</p>
</div>
      
  </article><h2 id="creating-and-managing-mailing-lists">Creating and managing mailing lists</h2>
<p>Besides creating user accounts, a system administrator is often required to create different mailing lists that reflect the organizational structure of the company. Using mailing lists, users can simply send emails to a group of users by inserting the list address instead of every user address one by one.</p>
<p>Creating mailing lists in Carbonio CE is extremely easy using the admin panel. You need to go to <strong>Domains &gt; select a domain &gt; Mailing List &gt; press the + button</strong>. You can now use the wizard that opens up to create a mailing list.<br />
 </p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-11/mailing_list_carbonio_admin_panel.webp" width="2560" height="1464" alt="Image of the Carbonio admin panel mailing list." /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Arman Khosravi, CC BY-SA 4.0)</p>
</div>
      
  </article><p>The essential steps to follow are:</p>
<ul><li>
<p>Insert the name</p>
</li>
<li>
<p>Insert the mailing list address</p>
</li>
<li>
<p>Press <strong>NEXT</strong></p>
</li>
<li>
<p>Insert the members</p>
</li>
<li>
<p>Press <strong>CREATE</strong>.</p>
</li>
</ul><p>You can follow the same path to edit mail lists created before.</p>
<h2 id="creating-and-managing-domains">Creating and managing domains</h2>
<p>Managing domains is another task frequently done by system administrators. Similar to accounts, creating a domain is very easy in the Carbonio Admin Panel. You only need to go to <strong>Domains &gt; select a domain &gt; under the details</strong> and find different entries to monitor the status of the domain. To create a new domain simply click on the <strong>CREATE</strong> button on the top bar and select <strong>Create New Domain</strong> and insert the necessary information such as:</p>
<ul><li>
<p>Domain name</p>
</li>
<li>
<p>Maximum number of accounts and maximum email quota</p>
</li>
<li>
<p>Mail server where the domain is hosted</p>
</li>
</ul><article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-11/domains_carbonio_admin_panel.webp" width="2560" height="1464" alt="Image of the Carbonio admin domains panel." /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Arman Khosravi, CC BY-SA 4.0)</p>
</div>
      
  </article><h2 id="creating-and-managing-mailstore-servers">Creating and managing mailstore servers</h2>
<p>The Carbonio CE Admin Panel allows system administrators to manage different servers present in the infrastructure and provide them with different tools to configure them. To monitor a new mailstore server you can go to <strong>Mailstores &gt; Servers List</strong> and find all the available mailstore servers in your infrastructure in a list (when just one server is installed, only one server is shown in this area).</p>
<p>Under <strong>Server Details</strong>, you can select any of the available servers in the list and select <strong>Data Volumes</strong> to show more details of the storage volumes attached to it. While multiple volumes can be mounted simultaneously, only one primary volume, one secondary volume, and one index volume can be designated as active. You can add new volumes using the <strong>NEW VOLUME +</strong> button in the same section. You can also change the volume properties simply by clicking on them to open their detail window.</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-11/volumes_carbonio_admin_panel.webp" width="2560" height="1464" alt="Image of the Carbonio admin panel volumes." /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Arman Khosravi, CC BY-SA 4.0)</p>
</div>
      
  </article><h2 id="creating-and-managing-classes-of-service">Creating and managing classes of service</h2>
<p>Another scenario that can be facilitated with the help of the admin panel is creating classes of service (COS). After the installation, a system administrator might need to create different classes (groups) and assign different properties to them. This way, later in the process each user or a group of users can be easily nominated to a class of service in order to have access to the features and properties assigned to that specific COS.</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-11/cos_features_carbonio_admin_panel.webp" width="2560" height="1464" alt="Image of the Carbonio admin panel COS features." /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Arman Khosravi, CC BY-SA 4.0)</p>
</div>
      
  </article><p>To create a COS simply click on the <strong>CREATE</strong> button and select <strong>Create New COS</strong> or alternatively go to <strong>COS</strong> on the left panel and click on <strong>CREATE NEW COS +</strong>. You can then insert the name of the COS and define the different services available to this specific class.</p>
<p>To edit a COS, go to <strong>COS</strong> on the left panel and select a COS from the dropdown menu at top of the list.</p>
<p>You can define settings like quotas, the mail servers that can host accounts from this COS, or enable features for this COS. You can also define features for general features like Mail, Calendar, and Contacts. Additional customizable options include Tagging, Out of Office Reply, Distribution List Folders, and so on.<br />
 </p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-11/cos_preferences_carbonio_admin_panel.webp" width="2560" height="1464" alt="Image of the Carbonio admin panel classes of service preferences." /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Arman Khosravi, CC BY-SA 4.0)</p>
</div>
      
  </article><h2 id="conclusion">Conclusion</h2>
<p>In this article, you saw a few scenarios in which the Carbonio CE Admin Panel saves you time and effort. The admin panel is an evolution of classical administrative tools in a new and centralized interface that gives the possibility of accessing different functionalities and monitoring tools from the same location.</p>
