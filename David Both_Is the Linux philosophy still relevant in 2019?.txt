<p>In August 2018, I published <a href="http://www.both.org/?page_id=903" target="_blank"><em>The Linux Philosophy for SysAdmins</em></a>. It seems to be selling fairly well, and I started thinking about whether the original Linux philosophy (or my own version of it for system administrators) is still relevant.</p>
<p>In Chapter 1, I said:</p>
<blockquote><p>"The Unix Philosophy is an important part of what makes <a href="https://en.wikipedia.org/wiki/Unix" target="_blank">Unix</a> unique and powerful. Much has been written about the Unix Philosophy. And the Linux philosophy is essentially the same as the Unix philosophy because of its direct line of descent from Unix.</p>
<p>"The original Unix Philosophy was intended primarily for the system developers. In fact, the developers of Unix, led by <a href="https://en.wikipedia.org/wiki/Ken_Thompson" target="_blank">Ken Thompson</a> and <a href="https://en.wikipedia.org/wiki/Dennis_Ritchie" target="_blank">Dennis Ritchie</a>, designed Unix in a way that made sense to them, creating rules, guidelines, and procedural methods, then designing them into the structure of the operating system. That worked well for system developers and that also—partly, at least—worked for SysAdmins (System Administrators). That collection of guidance from the originators of the Unix operating system was codified in the excellent book, <em>The Unix Philosophy</em>, by Mike Gancarz, and then later updated by Mr. Gancarz as <a href="https://www.elsevier.com/books/linux-and-the-unix-philosophy/gancarz/978-1-55558-273-9" target="_blank"><em>Linux and the Unix Philosophy</em></a>.</p>
<p>"Another fine book, <em><a href="http://www.catb.org/esr/writings/taoup/" target="_blank">The Art of Unix Programming</a></em>, by Eric S. Raymond, provides the author's philosophical view of programming in a Unix environment. It is also somewhat of a history of the development of Unix as it was experienced and recalled by the author. This book is also available <a href="http://www.catb.org/esr/writings/taoup/html/" target="_blank">in its entirety</a> at no charge on the Internet."</p>
</blockquote>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Linux resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux commands cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Free online course: RHEL Technical Overview</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux networking cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">SELinux cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux common commands cheat sheet</a></li>
<li><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">What are Linux containers?</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Our latest Linux articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>The philosophy outlined in these books was critical to the original design of Unix and its modern descendant, Linux. That groundbreaking design and its creative implementation made it possible for us to have the amazing open source operating system we have today. Without the concept of data streams, the use of pipes to modify and transform those data streams, the idea that "everything is a file," and so much more, we would be reduced to struggling with a command line even less powerful than the old IBM or MS-DOS. Even DOS used pipes but never provided powerful utilities like the <a href="http://www.linux-databook.info/?page_id=5352" target="_blank">GNU Core Utilities</a> that we take for granted today and give us access to the most basic of system functions.</p>
<p>The more I thought about this, the more I realized that many Linux users and sysadmins have never even heard about the Linux philosophy. So I started wondering whether the Linux philosophy, in whatever form you like it, is still relevant. I decided to ask you what you think.</p>
<p>Is the Linux philosophy still relevant in 2019?</p>
<p>I'm also interested in hearing what else you think about the Linux philosophy's relevance in 2019. Please share your feedback in the comments below.</p>
