<p>When I use the Linux command line, whether I'm writing a new program on my desktop computer or managing a website on my web server, I often need to process text files. Linux provides powerful tools that I leverage to get my work done. I frequently use <code>sed</code>, an editor that can modify text according to a pattern.</p>
<p><code>sed</code> stands for <em>stream editor</em>, and it edits text in a file and prints the results. One way to use <code>sed</code> is to identify several occurrences of one string in a file and replace them with a different string. You can use <code>sed</code> to process text files to a seemingly endless degree, but I'd like to share a few ways I use <code>sed</code> to help me manage files.</p>
<h2>Search and replace text in a file on Linux</h2>
<p>To use <code>sed</code>, you need to use a <em>regular expression</em>. A regular expression is a set of special characters that define a pattern. My most frequent example of using <code>sed</code> is replacing text in a file. The syntax for replacing text looks like this: <code>s/originaltext/newtext/</code>. The <code>s</code> tells sed to perform text replacement or swap occurrences of text. Provide the original text and new text between slashes.</p>
<p>This syntax will only replace the first occurrence of <code>originaltext</code> on each line. To replace every occurrence, even if the original text appears more than once on a line, append <code>g</code> to the end of the expression. Here is an example: <code>s/originaltext/newtext/g</code>.</p>
<p>To use this with <code>sed</code>, specify this regular expression with the <code>-e</code> option:</p>
<pre>
<code class="language-bash">$ sed -e 's/originaltext/newtext/g'</code></pre><p>For example, let's say I have a Makefile for a program called <strong>game</strong>, which simulates Conway's Game of Life:</p>
<pre>
<code class="language-text">.PHONY: all run clean

all: game

game: game.o
	$(CC) $(CFLAGS) -o game game.o $(LDFLAGS)

run: game
	./game

clean:
	$(RM) *~
	$(RM) *.o
	$(RM) game</code></pre><p>The name <strong>game</strong> isn't very descriptive, so I might choose to rename it <strong>life</strong>. Renaming the <code>game.c</code> source file to <code>life.c</code> is easy enough, but now I need to modify the Makefile to use the new name. I can use <code>sed</code> to change every occurrence of <strong>game</strong> to <strong>life</strong>:</p>
<pre>
<code class="language-bash">$ sed -e 's/game/life/g' Makefile
.PHONY: all run clean

all: life

life: life.o
	$(CC) $(CFLAGS) -o life life.o $(LDFLAGS)

run: life
	./life

clean:
	$(RM) *~
	$(RM) *.o
	$(RM) life</code></pre><p>This prints the <code>sed</code> output to the screen, which is a good way to check if the text replacement will do what you want. To make these changes to the Makefile, first, make a backup of the file, then run <code>sed</code> and save the output to the original filename:</p>
<pre>
<code class="language-bash">$ cp Makefile Makefile.old
$ sed -e 's/game/life/g' Makefile.old &gt; Makefile</code></pre><p>If you are confident that your changes are exactly what you want, use the <code>-i</code> or <code>--in-place</code> option to edit the file in place. However, I recommend adding a backup filename suffix like <code>--in-place=.old</code> to save a copy of the original file in case you need to restore it later. It looks like this:</p>
<pre>
<code class="language-bash">$ sed --in-place=.old -e 's/game/life/g' Makefile
$ ls Makefile*
Makefile  Makefile.old</code></pre><p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">More Linux resources</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux commands cheat sheet">Linux commands cheat sheet</a></div>
              <div class="field__item"><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Advanced Linux commands cheat sheet">Advanced Linux commands cheat sheet</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Free online course: RHEL technical overview">Free online course: RHEL technical overview</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux networking cheat sheet">Linux networking cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="SELinux cheat sheet">SELinux cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux common commands cheat sheet">Linux common commands cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="What are Linux containers?">What are Linux containers?</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Register for your free Red Hat account">Register for your free Red Hat account</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Our latest Linux articles">Our latest Linux articles</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<h2>Quoting files with sed on Linux</h2>
<p>You can use other features of regular expressions to match specific instances of text. For example, you might need to replace text that occurs at the start of a line. With <code>sed</code>, you can match the beginning of a line with <strong>^</strong>, the caret character.</p>
<p>One way I use "start of line" in replacing text is when I need to quote a file in an email. Let's say I want to share my Makefile in an email, but I don't want to include it as a file attachment. Instead, I prefer to "quote" the file in the body of an email, using <strong>&gt;</strong> before each line. I can use the following <code>sed</code> command to print out an edited version to my terminal, which I can copy and paste into a new email:</p>
<pre>
<code class="language-bash">$ sed -e 's/^/&gt;/' Makefile
&gt;.PHONY: all run clean
&gt;
&gt;all: life
&gt;
&gt;life: life.o
&gt;	$(CC) $(CFLAGS) -o life life.o $(LDFLAGS)
&gt;
&gt;run: life
&gt;	./life
&gt;
&gt;clean:
&gt;	$(RM) *~
&gt;	$(RM) *.o
&gt;	$(RM) life</code></pre><p>The <code>s/^/&gt;/</code> regular expression matches the start of each line (<strong>^</strong>) and places a <strong>&gt;</strong> there. Effectively, this starts each line with the <strong>&gt;</strong> symbol.</p>
<p>The tabs might not show up correctly in an email, but I can replace all tabs in the Makefile with a few spaces by adding another regular expression:</p>
<pre>
<code class="language-bash">$ sed -e 's/^/&gt;/' -e 's/\t/  /g' Makefile
&gt;.PHONY: all run clean
&gt;
&gt;all: life
&gt;
&gt;life: life.o
&gt;  $(CC) $(CFLAGS) -o life life.o $(LDFLAGS)
&gt;
&gt;run: life
&gt;  ./life
&gt;
&gt;clean:
&gt;  $(RM) *~
&gt;  $(RM) *.o
&gt;  $(RM) life</code></pre><p>The <code>\t</code> indicates a literal tab, so <code>s/\t/ /g </code>tells sed to replace all tabs in the input with two spaces in the output.</p>
<p>If you need to apply lots of edits to files, you can save your <code>-e</code> commands in a file and use <code>-f</code> to tell <code>sed</code> to use that file as a "script." This approach is especially useful if you need to make the same edits frequently. I might have prepared the Makefile for quoting in email using a script file called <code>quotemail.sed</code>:</p>
<pre>
<code class="language-bash">$ cat quotemail.sed
s/^/&gt;/
s/\t/  /g
$ sed -f quotemail.sed Makefile
&gt;.PHONY: all run clean
&gt;
&gt;all: life
&gt;
&gt;life: life.o
&gt;  $(CC) $(CFLAGS) -o life life.o $(LDFLAGS)
&gt;
&gt;run: life
&gt;  ./life
&gt;
&gt;clean:
&gt;  $(RM) *~
&gt;  $(RM) *.o
&gt;  $(RM) life</code></pre><h2>Learn to work with sed on Linux</h2>
<p><code>sed</code> is a great tool to keep in your Linux command-line toolkit. Explore the <code>sed</code> manual page and learn more about how to use it. Type <code>man sed</code> at the command line to get complete documentation about the different command line options and how to use <code>sed</code> to process text files.</p>
