<p>There are a lot of great open source tools out there for storing secrets in Git. It can be hard to determine the right one for you and your organization—it depends on your use cases and requirements. To help you compare and choose, we'll look at four of the most popular open source tools for secrets management and see how they stack up against each other:</p>
<ul><li><a href="https://github.com/AGWA/git-crypt" target="_blank">Git-crypt</a></li>
<li><a href="https://github.com/StackExchange/blackbox" target="_blank">BlackBox</a></li>
<li><a href="https://github.com/mozilla/sops" target="_blank">SOPS</a></li>
<li><a href="https://github.com/elasticdog/transcrypt" target="_blank">Transcrypt</a></li>
</ul><p>We won't review larger solutions like HashiCorp Vault. A production-ready Vault can be a rather large hurdle, especially if your organization is just getting started with secrets management. The tools above are easy to use and set up quickly.</p>
<h2 id="encryption-types">Encryption types</h2>
<p>These secrets management tools use GNU Privacy Guard (<a href="https://www.gnupg.org/" target="_blank">GPG</a>), symmetric key encryption, and/or cloud key services.</p>
<ul><li>GPG-based tools require users to create a GPG keypair. The public key is used to encrypt and is shared with other users, while the private key is used to decrypt and is known by only one user.</li>
<li>Symmetric key tools are password-based and work when given the correct password.</li>
<li>Cloud key services—Amazon Key Management Service (KMS), Google Cloud KMS, and Azure Key Vault-based tools—allow integration with services from cloud providers.</li>
</ul><p>The encryption types these secrets management tools use are:</p>
<table border="1" cellpadding="1" cellspacing="1"><thead><tr><th scope="row"> </th>
<th scope="col" style="text-align: center;">GPG</th>
<th scope="col" style="text-align: center;">Symmetric key</th>
<th scope="col" style="text-align: center;">Amazon KMS</th>
<th scope="col" style="text-align: center;">Google KMS</th>
<th scope="col" style="text-align: center;">Azure Key Vault</th>
</tr></thead><tbody><tr><th scope="row">Git-crypt</th>
<td style="text-align: center;">X</td>
<td style="text-align: center;">X</td>
<td style="text-align: center;"> </td>
<td style="text-align: center;"> </td>
<td style="text-align: center;"> </td>
</tr><tr><th scope="row">BlackBox</th>
<td style="text-align: center;">X</td>
<td style="text-align: center;"> </td>
<td style="text-align: center;"> </td>
<td style="text-align: center;"> </td>
<td style="text-align: center;"> </td>
</tr><tr><th scope="row">SOPS</th>
<td style="text-align: center;">X</td>
<td style="text-align: center;"> </td>
<td style="text-align: center;">X</td>
<td style="text-align: center;">X</td>
<td style="text-align: center;">X</td>
</tr><tr><th scope="row">Transcrypt</th>
<td style="text-align: center;"> </td>
<td style="text-align: center;">X</td>
<td style="text-align: center;"> </td>
<td style="text-align: center;"> </td>
<td style="text-align: center;"> </td>
</tr></tbody></table><p>As you can see, Git-crypt and SOPS use more than one encryption basis. This means Git-crypt can achieve encryption by using GPG <em>OR</em> a symmetric key, and SOPS can use GPG <em>OR</em> one of the cloud services.</p>
<h2 id="goals">Goals</h2>
<p>These tools have similar goals:</p>
<table border="1" cellpadding="1" cellspacing="1"><thead><tr><th scope="row"> </th>
<th scope="col" style="text-align: center;">Transparency with Git</th>
<th scope="col" style="text-align: center;">Whole-file encryption</th>
<th scope="col" style="text-align: center;">Partial-file encryption</th>
</tr></thead><tbody><tr><th scope="row">Git-crypt</th>
<td style="text-align: center;">X</td>
<td style="text-align: center;">X</td>
<td style="text-align: center;"> </td>
</tr><tr><th scope="row">BlackBox</th>
<td style="text-align: center;">X</td>
<td style="text-align: center;">X</td>
<td style="text-align: center;"> </td>
</tr><tr><th scope="row">SOPS</th>
<td style="text-align: center;"> </td>
<td style="text-align: center;">X</td>
<td style="text-align: center;">X</td>
</tr><tr><th scope="row">Transcrypt</th>
<td style="text-align: center;">X</td>
<td style="text-align: center;">X</td>
<td style="text-align: center;"> </td>
</tr></tbody></table><p>All but SOPS are transparent with Git, which means they have built-in mechanisms to ensure that files in source control are encrypted without much effort from users. They help prevent a <strong>git push</strong> from accidentally pushing plaintext secrets to Git.</p>
<p>At this point, you might be wondering, "Why is SOPS here if it doesn't encrypt transparently with Git? Isn't this a post about Git encryption tools?" The reason is because of how it handles key-value-based files, such as YAML and JSON. When encrypting these types of files, SOPS will leave the keys unencrypted but will encrypt the values. There are often cases, especially in configuration management, where these types of files need to be encrypted in Git, but it would also be convenient to see what kind of information they contain. While SOPS does not provide native Git transparency, tools like <a href="https://github.com/awslabs/git-secrets" target="_blank">git-secrets</a> can be used alongside SOPS to help ensure plaintext secrets are not pushed to Git.</p>
<p>Finally, all of these tools support whole-file encryption, in which secrets files are encrypted in their entirety.</p>
<h2 id="workflows-and-differences">Workflows and differences</h2>
<p>None of these tools are particularly difficult to use, but they all have quirks and operational challenges to consider.</p>
<h3 id="gpg">GPG</h3>
<p>The basic workflow for a GPG-based tool is:</p>
<ol><li>Initialize the repository with the encryption tool</li>
<li>Create GPG keys for users that are allowed to manage secret files</li>
<li>Add the corresponding public keys to the tool</li>
<li>Designate the files that should be treated as "secret"</li>
<li>Encrypt the files using the tool</li>
<li>Repeat steps 2, 3, and 5 for each new user that is granted permission</li>
<li>To revoke a user, remove the user and repeat step 5</li>
</ol><p>In theory, this workflow is simple. One operational issue is GPG key maintenance. Your team will need to back up its GPG keys to prevent a lock-out scenario if GPG keys are accidentally deleted. If you are using the tool for automation, you will also need to consider who will be responsible for creating and maintaining keys for that service. Additionally, if you need to add, remove, or rotate a key, you'll need to re-encrypt each file for the changes to take effect.</p>
<p>Advantages and disadvantages of the GPG-based tools include:</p>
<ul><li>Git-crypt cannot remove GPG users natively, which means step 7 above is not easy to do. There are workarounds available, but it's not a built-in feature.</li>
<li>Git-crypt will transparently perform step 5 above on a <strong>git push</strong>—even when new users are added.</li>
<li>BlackBox provides a <strong>blackbox_update_all_files</strong> command, which can perform step 5 by re-encrypting all secret files in one command. This comes in handy in key rotation and adding/removing GPG keys, where all files need to be re-encrypted.</li>
<li>SOPS makes key rotation and adding/removing GPG keys the most difficult, as it requires each file to be manually re-encrypted.</li>
<li>BlackBox has a <strong>blackbox_list_admins</strong> command that returns the email address that corresponds with registered users' GPG keys. This makes it easier to discern who has access to the secrets versus trying to map plain GPG fingerprints.</li>
</ul><h3 id="cloud-key-services">Cloud key services</h3>
<p>Here is a sample workflow using SOPS with Amazon KMS:</p>
<ol><li>Create identity and access management (IAM) entities</li>
<li>Create KMS master key</li>
<li>Grant IAM entities access to the master key</li>
<li>Add the master key to each secret file with SOPS and encrypt the file (adding keys and encrypting is usually a one-step process with SOPS)</li>
<li>Re-encrypt when adding or removing master keys</li>
</ol><p>Of these four tools, SOPS is the only one that allows users to configure encryption with a cloud-based key service. Cloud key services ease much of the operational burden that GPG-based solutions carry. Take Amazon KMS, for example: The master key is added to SOPS and access to secret files is controlled through IAM policies. Adding and removing users is as simple as granting or revoking permission with IAM, meaning secret files do not need to be re-encrypted when changing permissions, since nothing changed from SOPS's perspective. This solution does have its own set of operational challenges, however. Each member of the team must have an AWS account before they can access secret files. Also, admins must configure and maintain the IAM policies and KMS master key(s).</p>
<h3 id="symmetric-key-encryption">Symmetric key encryption</h3>
<p>The workflow for symmetric key-based tools is probably the simplest:</p>
<ol><li>Initialize the repository with the encryption tool</li>
<li>Designate files that should be treated as "secret"</li>
<li><strong>git push</strong> to transparently encrypt the files</li>
<li>Share the symmetric key with other users who need access</li>
<li>Rotate the key each time a user is revoked access</li>
</ol><p>Git-crypt and Transcrypt both provide a complex password as the symmetric key. The operational challenges are to find a secure way to share the symmetric key and to be sure to rotate the key each time a user is removed.</p>
<p>Here are a few differences between Git-crypt and Transcrypt, our symmetric key-compatible tools:</p>
<ul><li>Git-crypt is compatible with both GPG and symmetric key encryption</li>
<li>Git-crypt doesn't support symmetric key rotation, so you can't complete step 5 if you use it with a symmetric key</li>
<li>Transcrypt provides a convenient <strong>--rekey</strong> command for key rotation</li>
</ul><h2 id="other-features">Other features</h2>
<p>Other features and characteristics of the tools include:</p>
<table border="1" cellpadding="1" cellspacing="1"><thead><tr><th scope="row"> </th>
<th scope="col" style="text-align: center;">Editor-in-place</th>
<th scope="col" style="text-align: center;">Auditing</th>
<th scope="col" style="text-align: center;">Repo-level permission</th>
<th scope="col" style="text-align: center;">File-level permission </th>
</tr></thead><tbody><tr><th scope="row">Git-crypt</th>
<td style="text-align: center;"> </td>
<td style="text-align: center;"> </td>
<td style="text-align: center;">X</td>
<td style="text-align: center;"> </td>
</tr><tr><th scope="row">BlackBox</th>
<td style="text-align: center;">X</td>
<td style="text-align: center;"> </td>
<td style="text-align: center;">X</td>
<td style="text-align: center;"> </td>
</tr><tr><th scope="row">SOPS</th>
<td style="text-align: center;">X</td>
<td style="text-align: center;">X</td>
<td style="text-align: center;"> </td>
<td style="text-align: center;">X</td>
</tr><tr><th scope="row">Transcrypt</th>
<td style="text-align: center;"> </td>
<td style="text-align: center;"> </td>
<td style="text-align: center;">X</td>
<td style="text-align: center;"> </td>
</tr></tbody></table><p>Both BlackBox and SOPS feature an "editor-in-place" tool, which decrypts the file and opens a text editor specified by the <strong>$EDITOR</strong> environment variable. This enables the user to make in-place edits to the file before it is saved and re-encrypted, so users can modify secret files without requiring them to be "decrypted in place" first.</p>
<p>SOPS is the only tool that provides an auditing feature. This feature tracks and monitors SOPS usage by forwarding events to a database. It requires a certain amount of setup, so check out SOPS's <a href="https://github.com/mozilla/sops/blob/master/README.rst#auditing" target="_blank">README</a> for more information.</p>
<p>Git-crypt, BlackBox, and Transcrypt handle access at the repo level, meaning that if you can view one decrypted file, you can view them all. Depending on your use case, this is either a feature or a misfeature. SOPS handles permissions at the file level, meaning just because users can view one file, they can't necessarily view other files in the repo.</p>
<h2 id="thanks-for-reading">For more information</h2>
<p>Hopefully, this high-level overview of four open source secrets management tools will help you make an educated decision about the right tool for you. For more information on the tools, please check out their GitHub pages (linked at the top of this article).</p>
