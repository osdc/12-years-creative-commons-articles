<p>2016 has been quite a year for licensing and technology ownership cases, and we had some interesting articles indeed.</p>
<p>One topic that has the attention of many in the open source world is that of GPL enforcement, and so when the GPL enforcement action in <em>Hellwig v. VMware</em> <a href="https://opensource.com/law/16/8/gpl-enforcement-action-hellwig-v-vmware">was dismissed</a> many took an interest.</p>
<p>Patent trolls continue to concern many of our readers, and collective action efforts to fight them have gained traction; one such effort was the <a href="https://opensource.com/node/31061">EFF asked universities not to sell to patent trolls</a> and to make a Public Interest Patent Pledge.</p>
<p>Articles on various types of open source licensing were also popular, including <a href="https://opensource.com/law/16/1/case-educating-judges-open-source-licensing">why we should educate judges on open source licensing</a>, a <a href="https://opensource.com/law/16/8/which-type-open-source-license-do-you-prefer">poll on readers' open source license preference</a>, and why <a href="https://opensource.com/law/16/11/licenses-are-shared-resources">open source licenses themselves are shared resources</a>.</p>
<p>Looking ahead to 2017, patent reform appears to be increasingly unlikely, so we will all need to continue to think about other ways to act to protect open source software.</p>
<h2>Top open source in law articles in 2016:</h2>
<ol><li><a href="https://opensource.com/law/16/4/why-lawyers-need-understand-technical-terminology-used-copyright-licenses">What is a URI? Understanding license terminology for compliance</a></li>
<li><a href="https://opensource.com/law/16/9/eff-asks-universities-not-sell-patents-trolls">EFF asks universities not to sell to patent trolls</a></li>
<li><a href="https://opensource.com/law/16/10/distributing-encryption-software-may-break-law">Distributing encryption software may break the law</a></li>
<li><a href="https://opensource.com/law/16/8/gpl-enforcement-action-hellwig-v-vmware">GPL enforcement action in Hellwig v. VMware dismissed, with an appeal expected</a></li>
<li><a href="https://opensource.com/law/16/1/case-educating-judges-open-source-licensing">The case for educating judges on open source licensing</a></li>
<li><a href="https://opensource.com/law/16/6/outcome-google-v-oracle-good-open-source">Oracle v. Google: What it means for software developers</a></li>
<li><a href="https://opensource.com/law/16/8/which-type-open-source-license-do-you-prefer">Which type of open source license do you prefer?</a></li>
<li><a href="https://opensource.com/law/16/11/licenses-are-shared-resources">Open source licenses are shared resources</a></li>
</ol>