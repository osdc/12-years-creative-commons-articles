<p>Rochester Institute of Technology is establishing <a href="https://www.rit.edu/research/open" target="_blank">Open@RIT</a>, an initiative dedicated to supporting all kinds of "open work," including—but not limited to—open source software, open data, open hardware, open educational resources, Creative Commons-licensed work, and open research.</p>
<p>The new open source programs office aims to determine and grow the footprint of RIT's impact on all things "open," leading to more collaboration, creation, and contribution, on and off campus.</p>
<p>Open work is non-proprietary—meaning it's licensed to be publicly accessible and anyone can modify or share, within the terms of the license. While the term "open source" originally came out of the software industry, it has since become a set of values that has applications in everything from science to media.</p>
<p>"It's important for RIT and all of academia to be involved in open work because we're engaged in this give and take of sharing information that can benefit us all," says <a href="https://www.rit.edu/directory/sxjics-stephen-jacobs" target="_blank">Stephen Jacobs</a>, director of <a href="https://www.rit.edu/research/open">Open@RIT</a>. "Open work can make the world a better, more collaborative place—and that has become especially important since the COVID-19 pandemic hit."</p>
<p>Many companies have an open source programs office (OSPO) to support and mentor employees and work with license compliance issues. However, RIT is one of the first universities to create an OSPO.</p>
<p><a href="https://www.rit.edu/research/open">Open@RIT</a> is an extension of Jacobs' 12 years of work with students to promote the practice of free and open source software (FOSS) development, currently embodied in <a href="https://fossrit.github.io/" target="_blank">FOSS@MAGIC</a>. The idea for the office developed from a whitepaper that Jacobs wrote in 2019. The research office sponsored a series of meetings with faculty and staff to discuss what an open programs office should include.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="RIT hackathon"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/foss-hackathon_0.jpg" width="675" height="450" alt="RIT hackathon" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>RIT hackathon held prior to 2020 (Image: Michael Owens)</sup></p>
</div>
      
  </article></p>
<p>"I knew we were at a moment in time where we could level up the positive impact that RIT already makes in open work and broaden it," said Jacobs, who is also a professor of <a href="https://www.rit.edu/computing/school-interactive-games-and-media" target="_blank">interactive games and media</a>. "What surprised me was seeing so many people from 37 units across campus who wanted to learn how to get involved and do more."</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Great Content</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=7016000000127cYAAQ">Free online course: RHEL technical overview</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=7016000000127cYAAQ">Learn Advanced Linux Commands</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Download Cheat Sheets</a></li>
<li><a href="https://opensource.com/alternatives?intcmp=7016000000127cYAAQ">Find an Open Source Alternative</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=7016000000127cYAAQ">Read Top Linux Content</a></li>
<li><a href="https://opensource.com/resources?intcmp=7016000000127cYAAQ">Check out open source resources</a></li>
</ul></div>
</div>
</div>
</div>
<p>Organizers are intentionally leaving the word "source" out of the <a href="https://www.rit.edu/research/open">Open@RIT</a> name because the initiative includes more than just open software. Members of the RIT community are contributing to open data sets, open science, open research, and more.</p>
<p>"I'm extremely excited about what the new <a href="https://www.rit.edu/research/open">Open@RIT</a> initiative could hold for our research community," said <a href="https://www.rit.edu/directory/rprsps-ryne-raffaelle" target="_blank">Ryne Raffaelle</a>, RIT vice president for research. "This initiative is designed to support engagement and interaction for our faculty, staff, and students in all university units that strive to engage in and extend their efforts in open work."</p>
<p>In the future, <a href="https://www.rit.edu/research/open">Open@RIT</a> hopes to teach novices how to get involved with open projects while getting support for those who are already engaged. The office will look at how faculty can get credit toward tenure and promotion for open work, help find research grants, and collaborate with open source program offices in industry.</p>
<p>The office will build on RIT's <a href="https://www.redhat.com/en/blog/how-first-minor-foss-grew-idea-fossmagic" target="_blank">history in open source</a>. The university began offering FOSS classes in 2009. In 2014, <a href="http://games.rit.edu/" target="_blank">RIT's School of Interactive Games and Media</a> began offering the country's first <a href="https://www.rit.edu/study/free-and-open-source-software-and-free-culture-minor" target="_blank">minor in FOSS and free culture</a>. RIT's <a href="https://opensource.com/article/19/12/humanitarian-startups-open-source" target="_blank">LibreCorps</a> initiative also helps students find internships and contribute to humanitarian, community, and educational projects with organizations including UNICEF and Red Hat.</p>
<p>On September 9, Jacobs discussed <a href="https://www.rit.edu/research/open">Open@RIT</a> at the Open Source Initiative's <a href="https://eventyay.com/e/8fa7fd14/schedule" target="_blank">State of the Source summit</a>.</p>
<hr /><p><em>This article was originally published on the <a href="https://www.rit.edu/news/rit-creates-openrit-university-wide-initiative-all-things-open" target="_blank">RIT News site</a> and is reused with permission.</em></p>
