<p>
  In this edition of our open source news roundup, we take a look at Linux for developers coming to Chrome OS, a tool that uses AR to assist surgeons, and more.
</p>
<h2>Linux for developers is coming to Chrome OS</h2>
<p>
  Developers will soon have the option to <a href="https://www.infoworld.com/article/3271132/linux/linux-for-developers-is-coming-to-chrome-os.html" target="_blank">run a Linux virtual machine within Chrome OS</a>. A selection of popular code editors will also be available, including Android Studio IDE. If desired, developers will also have the option to add projects to the Google Cloud Platform from the command line.
</p>
<h2>Augmented reality amplifies X-ray images to help surgeons</h2>
<p>A study published in the Journal of Medical Imaging <a href="https://www.healthdatamanagement.com/news/augmented-reality-amplifies-x-ray-images-to-help-surgeons" target="_blank">proposes the addition of augmented reality</a> to some minimally invasive surgeries.</p>
<p>Researchers proposed the use of 2-D X-ray images as 3-D virtual objects. They used the open source ARToolKit to create and calibrate markers and a guidance system. The accuracy of the AR system was comparable to the traditional techniques used while reducing the procedure time by almost 10 percent.</p>
<h2>Ed Department rolling out free digital textbook program</h2>
<p>
  The U.S. Department of Education plans to <a href="https://www.educationdive.com/news/ed-department-rolling-out-free-digital-textbook-program/523250/" target="_blank">expand access to free, open digital textbooks and materials</a> for colleges across the nation. $5 million in federal appropriations will support this expansion to open, peer-reviewed materials.
</p>
<p>
  Campuses will have the opportunity to submit ideas for expanded digital textbook access. A formal pilot program is planned to launch this summer. Projects with the greatest cost-savings potential will be considered for funding through this grant program.
</p>
<h3>In other news</h3>
<ul><li><a href="http://www.information-age.com/big-data-social-good-unicef-123471867/" target="_blank">Accelerating big data for social good with UNICEF</a></li>
<li><a href="https://techstartups.com/2018/05/10/genetic-blockchain-startup-dnatix-releases-first-blockchain-based-open-source-dna-compression-tool/" target="_blank">Genetic blockchain startup DNAtix releases its first blockchain-based open source DNA compression tool</a></li>
<li><a href="http://www.plasticsnews.com/article/20180510/NEWS/180519993/dell-wants-open-source-solution-to-ocean-debris" target="_blank">Dell wants 'open source' solution to ocean debris</a></li>
<li><a href="https://www.designnews.com/electronics-test/hifive-unleashed-expansion-board-opens-door-risc-v-pcs/183342268658749" target="_blank">HiFive-Unleashed Expansion Board Opens Door for RISC-V PCs</a></li>
<li><a href="http://blog.executivebiz.com/2018/05/cia-to-seek-partners-to-conduct-open-source-intell-big-data-experiments-on-aws-built-cloud/" target="_blank">CIA to Seek Partners to Conduct Open-Source Intel, Big Data Experiments on AWS-Built Cloud</a></li>
</ul><p>
  <em>Thanks, as always, to Opensource.com staff members and moderators for their help this week. Make sure to check out <a href="https://opensource.com/resources/conferences-and-events-monthly">our event calendar</a> to see what's happening next week in open source.</em>
</p>
