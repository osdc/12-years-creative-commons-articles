<p>Open source is ready to get to work, and in 2019, Opensource.com had many great articles about how organizations have adopted open source software or open methods to drive their business. As open source matures, we've seen open source not just replace proprietary software, but create entirely new business models.</p>
<p>Check out this list of five outstanding articles from Opensource.com in 2019 about running a business with open source.</p>
<h2 id="get-your-business-up-and-running-with-these-open-source-tools">Get your business up and running with these open source tools</h2>
<p>In <a href="https://opensource.com/article/19/9/business-creators-open-source-tools"><em>Get your business up and running with these open source tools</em></a>, I explain: "Yes, you really can operate a business using open source software." In this article, I review the key open source software tools that I use to run my company, including Inkscape, GIMP, LibreOffice, and Scribus.</p>
<h2 id="whats-your-favorite-open-source-bi-software">What's your favorite open source BI software?</h2>
<p>As Lauren Maffeo explains in <em><a href="https://opensource.com/article/19/8/favorite-open-source-bi-software">What's your favorite open source BI software?</a></em> "Open source business intelligence (BI) software helps users upload, visualize, and make decisions based on data that is pulled from several sources... BI involves turning data into insights that help your business make better decisions... Before choosing which open source BI tool to adopt, it's worth weighing the pros and cons of each tool against your business needs." The article's accompanying poll asks readers whether they prefer Pentaho, Logz.io, Cluvio, Qlikview, Sisense, or another BI application. Answer the poll to let us know which is your favorite and to see what other readers say.</p>
<h2 id="scrum-vs.-kanban-which-agile-framework-is-better">Scrum vs. kanban: Which agile framework is better?</h2>
<p>Because scrum and kanban both fall under the agile framework umbrella, many people confuse them or think they're the same thing. There are differences, however. In <a href="https://opensource.com/article/19/8/scrum-vs-kanban"><em>Scrum vs. kanban: Which agile framework is better?</em></a> Taz Brown explains the differences between scrum and kanban and helps you decide which one may be best for your team.</p>
<h2 id="what-is-small-scale-scrum">What is Small Scale Scrum?</h2>
<p>"Agile is fast becoming a mainstream way industries act, behave, and work as they look to improve efficiency, minimize costs, and empower staff. Most software developers naturally think, act, and work this way, and alignment towards agile software methodologies has gathered pace in recent years," write Agnieszka Gancarczyk and Leigh Griffin in <a href="https://opensource.com/article/19/1/what-small-scale-scrum"><em>What is Small Scale Scrum?</em></a> In this article, they explain how the scrum agile methodology can help small teams work more efficiently.</p>
<h2 id="what-does-devops-mean-to-you">What does DevOps mean to you?</h2>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More DevOps resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/resources/devops?intcmp=7013a00000263HlAAI">What is DevOps?</a></li>
<li><a href="https://opensource.com/downloads/devops-hiring-guide?intcmp=7013a00000263HlAAI">The ultimate DevOps hiring guide</a></li>
<li><a href="https://opensource.com/downloads/devops-monitoring-guide?intcmp=7013a00000263HlAAI">DevOps monitoring tools guide</a></li>
<li><a href="https://opensource.com/downloads/devsecops?intcmp=7013a00000263HlAAI">Getting started with DevSecOps</a></li>
<li><a href="https://enterprisersproject.com/cheat-sheet-devops-glossary?intcmp=7013a00000263HlAAI">Download the DevOps glossary</a></li>
<li><a href="https://www.ansible.com/resources/ebooks/ansible-for-devops?intcmp=7013a00000263HlAAI">eBook: Ansible for DevOps</a></li>
<li><a href="https://opensource.com/tags/devops?src=devops_resource_menu2">Latest DevOps articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>In <em><a href="https://opensource.com/article/19/1/what-does-devops-mean-you">What does DevOps mean to you?</a> </em>Girish Managoli offers one answer to this article's headline: "DevOps is a process of software development focusing on communication and collaboration to facilitate rapid application and product deployment." But there are a range of opinions and expectations around DevOps. To help explain DevOps and how to leverage it in organizations, Girish interviewed six experts to break down DevOps and the key practices and philosophies in making DevOps work for you.</p>
<h2 id="open-source-is-open-for-business">Open source is open for business</h2>
<p>As open source plays an increasingly important role in business, there's more to learn about the topic. What do you want to know about it in 2020? Please share your ideas for articles in the comments—or even share your own experiences with running a business on open source software by <a href="https://opensource.com/how-submit-article">writing an article for Opensource.com</a>.</p>
