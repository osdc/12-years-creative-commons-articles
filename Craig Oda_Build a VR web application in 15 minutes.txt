<p>In 15 minutes, you can develop a virtual reality application and run it in a web browser, on a VR headset, or with <a href="https://vr.google.com/daydream/" target="_blank">Google Daydream</a>. The key is <a href="https://aframe.io/" target="_blank">A-Frame</a>, an open source toolkit built by the <a href="https://mozvr.com/" target="_blank">Mozilla VR Team</a>.</p>
<h2>Test it</h2>
<p>Open <a href="https://theta360developers.github.io/360gallery/" target="_blank">this link</a> using Chrome or Firefox on your mobile phone.</p>
<p>Put your phone into <a href="https://vr.google.com/cardboard/" target="_blank">Google Cardboard</a> and stare at a menu square to switch the 360-degree scene.</p>
<p></p><center><img src="https://opensource.com/sites/default/files/resize/augmentedvr-700x482.png" width="700" height="482" /></center>
<h2>Fork it</h2>
<p>Fork the <a href="https://github.com/theta360developers/360gallery" target="_blank">sample repository from GitHub</a>. Change directory into the repo.</p>
<p></p><center><img src="https://opensource.com/sites/default/files/samplerepo.png" width="618" height="94" /></center>
<p>If you have 360-degree images, you can drop them into the img/ sub-directory. If you don't have 360-degree images, you can get started with the open source <a href="http://hugin.sourceforge.net/" target="_blank">Hugin</a> panorama photo stitcher. The boilerplate app includes <a href="http://theta360.guide/community-document/community.html" target="_blank">RICOH THETA media</a> I took at a meetup in San Francisco.</p>
<h2>Create thumbnails</h2>
<p>The menus in the headset are standard images that are 240x240 pixels. You can use <a href="https://www.gimp.org/" target="_blank">GIMP</a> to create the images. A-Frame handles the perspective shifts for you automatically.</p>
<p></p><center><img src="https://opensource.com/sites/default/files/vrthumbnails.png" width="572" height="310" /></center>
<h2>Edit code</h2>
<p>If you use the same image file names and overwrite 1.jpg in /img, you do not need to edit the code at all. If you want to extend the program or modify it with your own filenames, change the id and the src in index.html to match your files.</p>
<pre>
<code class="language-html">&lt;body&gt;
  &lt;a-scene&gt;
    &lt;a-assets&gt;
      &lt;img id="kieran" src="https://opensource.com/img/1.jpg"&gt;
      &lt;img id="kieran-thumb" crossorigin="anonymous" src="https://opensource.com/img/kieran-thumb.png"&gt;
      &lt;img id="christian-thumb" crossorigin="anonymous" src="https://opensource.com/img/christian-thumb.png"&gt;
      &lt;img id="eddie-thumb" crossorigin="anonymous" src="https://opensource.com/img/eddie-thumb.png"&gt;
      &lt;audio id="click-sound" crossorigin="anonymous" src="https://cdn.aframe.io/360-image-gallery-boilerplate/audio/click.ogg"&gt;&lt;/audio&gt;
      &lt;img id="christian" crossorigin="anonymous" src="https://opensource.com/img/2.jpg"&gt;
      &lt;img id="eddie" crossorigin="anonymous" src="https://opensource.com/img/4.jpg"&gt;</code></pre><p>Scroll down and edit the section for the menu links.</p>
<pre>
<code class="language-html">&lt;!-- 360-degree image. --&gt;
&lt;a-sky id="image-360" radius="10" src="#kieran"&gt;&lt;/a-sky&gt;

&lt;!-- Image links. --&gt;
&lt;a-entity id="links" layout="type: line; margin: 1.5" position="0 -1 -4"&gt;
  &lt;a-entity template="src: #link" data-src="#christian" data-thumb="#christian-thumb"&gt;&lt;/a-entity&gt;
  &lt;a-entity template="src: #link" data-src="#kieran" data-thumb="#kieran-thumb"&gt;&lt;/a-entity&gt;
  &lt;a-entity template="src: #link" data-src="#eddie" data-thumb="#eddie-thumb"&gt;&lt;/a-entity&gt;
&lt;/a-entity&gt;</code></pre><h2>Upload to GitHub pages</h2>
<p>Add and commit your changes:</p>
<pre>
<code class="language-bash">git add *
git commit -a -m ‘changed images'
git push</code></pre><p>Open your app on a mobile phone at <a href="http://username.github.io/360gallery">http://username.github.io/360gallery</a>.</p>
<h2>Next steps</h2>
<p>This is a brief taste of A-Frame to illustrate that WebVR is easy and accessible to web developers. Go to <a href="https://aframe.io/" target="_blank">aframe.io</a> to see more demos. Although the display of 360 images is not true VR, it is easy, fun, and accessible today. Using 360 images is also a great way to start to understand the basics of augmented reality.</p>
<p>Take your own pictures with a standard camera and stitch them together or buy or borrow a 360-degree camera. The camera I used supports 360-degree video files and live streaming.</p>
<h2>Troubleshooting</h2>
<p>The application won't run from a local file that you open in your browser. You must either run a local webserver like Apache2 or upload it to an external site like GitHub Pages for testing.</p>
<p>If you're using an Oculus Rift or HTC Vive, you may need to install Firefox Nightly or experimental Chromium builds. See the current status of your browser at <a href="https://iswebvrready.org/" target="_blank">Is WebVR Ready?</a></p>
<p>360-degree video works on desktop browsers. I've experienced some glitches on mobile devices. The technology is improving quickly.</p>
