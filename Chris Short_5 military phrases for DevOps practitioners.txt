<p>The U.S. military is known for quite a few things, including its excessive and fascinating use of acronyms, euphemisms, and colorful phrases to describe certain situations. But many of these terms are applicable to other areas of life, including many non-military jobs.</p>
<p>Following are five phrases from the armed forces that I think DevOps practitioners should adopt.</p>
<h2 id="good-idea-fairy">"Good Idea Fairy"</h2>
<p>According to the <a href="https://www.urbandictionary.com/define.php?term=good%20idea%20fairy" target="_blank">Urban Dictionary</a>, a Good Idea Fairy is "an evil mythical creature that whispers advice and ideas into the ears of military leadership causing hundreds [of] unnecessary changes and countless wasted man-hours every year."</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More DevOps resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/resources/devops?intcmp=7013a00000263HlAAI">What is DevOps?</a></li>
<li><a href="https://opensource.com/downloads/devops-hiring-guide?intcmp=7013a00000263HlAAI">The ultimate DevOps hiring guide</a></li>
<li><a href="https://opensource.com/downloads/devops-monitoring-guide?intcmp=7013a00000263HlAAI">DevOps monitoring tools guide</a></li>
<li><a href="https://opensource.com/downloads/devsecops?intcmp=7013a00000263HlAAI">Getting started with DevSecOps</a></li>
<li><a href="https://enterprisersproject.com/cheat-sheet-devops-glossary?intcmp=7013a00000263HlAAI">Download the DevOps glossary</a></li>
<li><a href="https://www.ansible.com/resources/ebooks/ansible-for-devops?intcmp=7013a00000263HlAAI">eBook: Ansible for DevOps</a></li>
<li><a href="https://opensource.com/tags/devops?src=devops_resource_menu2">Latest DevOps articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>I discussed the Good Idea Fairy in "<a href="https://chrisshort.net/what-the-military-taught-me-about-devops/" target="_blank">What the military taught me about DevOps</a>." The best example of the Good Idea Fairy in DevOps is when someone wants to use a new tool that offers no new functionality nor improvement. This happens more often than you realize. Sometimes new tools will result in new and improved functionality. But, if no one uses that functionality, what good is taking the time to implement it? DevOps is about getting the job done with the right tool now while being mindful of the future. If it's time to retool, do it. Otherwise, resist that "new DevOps tool" someone read about on Hacker News.</p>
<p>Another example is when leadership decides that your well-trained developers are going to code in a different language. Or your infrastructure is well-managed with Ansible and, for no reason, everything has to be ported over to Terraform. That's insane, yes, but I've seen it happen. The Good Idea Fairy is not your friend in DevOps (or in life for that matter).</p>
<h2 id="hurry-up-and-wait">"Hurry up and wait."</h2>
<p>The urge to rush to the airport so you can get through security and relax at the gate or in a lounge—that's "hurry up and wait." It's neither good nor bad, and it's sometimes the best thing to do. In the military, folks are quickly marshaled to a place so they can wait for the next thing to hurry up and do.</p>
<p>It's much better to get a function or system deployed early and have it lying in wait rather than trying to rush it out the door. A project finishing too early can be bad, too. The requirements can change, requiring rework, which is a DevOps anti-pattern. The trick is finding the balance between readiness and waiting.</p>
<p>Much like getting a group of people to a certain place at a certain time requires more time the more people there are, the same holds true for code. The more code you try to deploy at once, the longer it takes. Make sure you are breaking down work in progress to the smallest possible unit.</p>
<h2 id="embrace-the-suck">"Embrace the suck."</h2>
<p>"The suck" is frequently used by Marines to describe the U.S. Marine Corps. But I have seen its context expand to a ton of situations. "The suck" is any miserable situation that someone has to get through to get to a better situation (hopefully). In DevOps, "the suck" is going to be building that tedious, repetitive pipeline. Another good example is doing the investigative work to figure out how legacy systems got to their current state (bonus points for no documentation and no one that could have built the system still being employed).</p>
<p>Let's face it, DevOps isn't all rainbow, flowers, unicorns, and glory. There are going to be things that you will have to do in DevOps that you're not going to like. Embracing "the suck" reminds folks that there are better things ahead. Push through this now and move on to a better future.</p>
<h2 id="you-can-only-expect-what-you-inspect">"You can only expect what you inspect."</h2>
<p>"You can only expect what you inspect" often refers to a leader's expectations versus the inevitable reality. If there is an expectation that something is done a certain way, be sure to periodically check that it was done right (or at all). For DevOps, consistently check that outcomes are improving business metrics and providing value. Don't assume that processes are running when you think they are; monitor them. Don't assume users will use a tool in a certain way, observe feature usage. Assumptions are an enemy in DevOps organizations, so make sure to eliminate them through inspection.</p>
<h2 id="the-standard-you-walk-past-is-the-standard-you-accept.">"The standard you walk past is the standard you accept."</h2>
<p>David Morrison, a former chief of the Australia Army, made a prolific statement when addressing unacceptable behavior: "The standard you walk past is the standard you accept." This holds true in life and in DevOps. If your coding practices or culture do not live up to the standards your organization has set for itself, correct it on the spot. Think of the concept of <a href="https://itrevolution.com/kata/" target="_blank">Toyota's Andon Cord</a>. It signals an issue and calls for swarming so quality issues can be corrected before being rolled out into a finished vehicle. The same should hold true for the tooling and people in an organization. Do not allow the bar to be lowered. Keep the standards high.</p>
<p><strong>[See our related story, <a href="https://opensource.com/article/18/6/devops-blogs-newsletters-follow" target="_blank">16 blogs and newsletters to follow for DevOps practitioners</a>.]</strong></p>
