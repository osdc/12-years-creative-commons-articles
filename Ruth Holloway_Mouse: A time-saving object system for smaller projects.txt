<p>There are several great object systems for Perl, and <a href="https://metacpan.org/pod/Moose" target="_blank">Moose</a> is one of them. But Moose comes with a compile-time penalty that smaller applications may not be willing to pay, particularly for certain CGI or command-line scripts. Moose is <em>incredibly</em> feature-rich, and you may not need all of those features all the time.</p>
<p><a href="https://metacpan.org/pod/Mouse" target="_blank">Mouse</a> to the rescue! Mouse is a lightweight object system with a subset of Moose's features. The goal throughout its development has been to make it syntactically consistent with Moose so if you later need to switch to the heavier-duty Moose, you can just substitute 'Moose' for 'Mouse' everywhere in your codebase, and things should just work.</p>
<p>Let's take a quick look at Mouse's capabilities. For this discussion, here's a code snippet defining three classes:</p>
<pre><code class="language-perl">package Vehicle;
use Mouse;

has 'engine_type'    =&gt; ( is =&gt; 'rw', isa =&gt; 'Str' );
has 'num_of_wheels'  =&gt; ( is =&gt; 'rw', isa =&gt; 'Int' );
has 'max_passengers' =&gt; ( is =&gt; 'rw', isa =&gt; 'Int' );
has 'color'          =&gt; ( is =&gt; 'rw', isa =&gt; 'Str' );

sub repaint {
    my ($self, $new_color) = @_;
    $self-&gt;color($new_color);
}

__PACKAGE__-&gt;meta-&gt;make_immutable();

package Bicycle;
use Mouse;
extends 'Vehicle';

has '+engine_type'    =&gt; ( default =&gt; 'human' );
has '+num_of_wheels'  =&gt; ( default =&gt; '2' );
has '+max_passengers' =&gt; ( default =&gt; '1' );
has 'basket_capacity' =&gt; ( is =&gt; 'rw', isa =&gt; 'Int', default =&gt; '0' );

before 'color' =&gt; sub {
    my $self = shift;
    $self-&gt;basket_capacity(0);   # Take off the basket when we repaint
};

__PACKAGE__-&gt;meta-&gt;make_immutable();

package GoodsWagon;
use Mouse;
extends 'Vehicle';

has 'max_load_capacity' =&gt; ( is =&gt; 'rw', isa =&gt; 'Int' );
__PACKAGE__-&gt;meta-&gt;make_immutable();</code></pre><h2 id="somebasics">Some basics</h2>
<p>Regular Perl mongers will note that I did not <code>use strict; use warnings;</code> in this code, something that is almost always encouraged. Mouse imports those for you, so you don't have to.</p>
<p>Also, see the call to <code>meta-&gt;make_immutable()</code> at the bottom of each of the three packages? When you make a class immutable, you're telling Mouse (or Moose) that you're not going to be adding any more attributes, methods, or roles to the class. Doing this will speed things up at runtime, with a small cost when the class is first being loaded. It's recommended for most classes, so I included it here.</p>
<h2>Attributes</h2>
<p>Each of the three packages has a group of attributes, defined using the <code>has</code> keyword. Because all of them are <code>'rw'</code> in the <code>is</code> attribute parameter, Mouse will automatically create a reader and a writer for the attribute, visible to users of the object:</p>
<pre><code class="language-perl">use Bicycle;

my $bike = Bicycle-&gt;new( color =&gt; 'Purple' );

print $bike-&gt;engine_type;    # human
$bike-&gt;basket_capacity(4);   # install a basket, with capacity 4
$bike-&gt;repaint('Blue');      # Bicycle inherits this sub from Vehicle.
                             # We could also just $bike-&gt;color('Blue');</code></pre><p>In the Vehicle class, I'm defining some characteristics common to many types of vehicles, but not setting values for any of them. You could stop there, but in the application, we want to have some special sorts of vehicles with additional characteristics.</p>
<p>In the Bicycle class, we set defaults for <code>engine_type</code>, <code>num_of_wheels</code>, and <code>max_passengers</code> that are rational for a bicycle, and we also set a default for <code>basket_capacity</code> that indicates no basket is installed by default. Meanwhile, in GoodsWagon, we define the vehicle's load capacity, something of value to know about large commercial vehicles. In both of the subordinate classes, we inherit the attribute characteristics of the parent class, using the <code>extends</code> keyword.</p>
<p>There are a <em>lot</em> of things you can do here, like making an attribute required, define "builder" subroutines or methods to construct or calculate attributes, trigger other actions when an attribute is set, and much more.</p>
<h2>Subroutine inheritance</h2>
<p>As you can see in the example above, classes that <code>extend</code> others inherit their subroutines. You can override the subroutine by writing it into the child class, but in many cases, there may be an easier way: <code>before</code>, <code>after</code>, and <code>around</code>. These subroutines are executed before the subroutine or attribute named in the case of <code>before</code>, afterward in the case of <code>after</code>, and <em>both before and after</em> in the case of <code>around</code>. I've included a <code>before</code> in the Bicycle class, which removes the instance's basket capacity before you change the color (you wouldn't want to repaint your bicycle with the basket still mounted, now, would you?). Cleverly, you could also use an <code>after</code> for other tasks you might want to do, such as lubricating the chain.</p>
<h2>Roles</h2>
<p>Finally, let's look at roles. Roles are class definitions in Mouse that describe something another class may <em>do</em> or have as a common characteristic. Here's one, which I'm snagging and enhancing <a href="https://metacpan.org/pod/Moose::Manual::Roles" target="_blank">from the Moose documentation</a>:</p>
<pre><code class="language-perl">package Breakable;

use Mouse::Role;

has 'is_broken' =&gt; ( is =&gt; 'rw', isa =&gt; 'Bool' );

sub break {
    my $self=shift;
    $self-&gt;is_broken(1);
}

sub fix {
    my $self=shift;
    $self-&gt;is_broken(0);
}</code></pre><p>We've added an attribute here that could be used across Vehicle and all of its child types, and (since it's pretty generic) for other types of machines or other objects completely unrelated to vehicles. If we add <code>with 'Breakable';</code> to the Vehicle class above, then we can do something like this:</p>
<pre><code class="language-perl">use GoodsWagon;

my $wagon = GoodsWagon-&gt;new( color =&gt; 'Red', max_load_capacity =&gt; '4000' );

print $wagon-&gt;is_broken ? 'Broken down!' : 'Running fine!';   # Running fine!
$wagon-&gt;break;
print $wagon-&gt;is_broken ? 'Broken down!' : 'Running fine!';   # Broken down!
$wagon-&gt;fix;</code></pre><p>Roles are useful when you want to have a behavior that is common across more than one tree of classes. If Vehicles were the only things that you wanted to make breakable in this way, then you'd just put that functionality in the Vehicle class.</p>
<p>You have a lot of choices for object management systems in Perl. Choosing Mouse for small projects can speed them up over running Moose, and if the project grows, you can easily switch to Moose for its much larger feature set. It's easy to get up to speed quickly with Mouse; check out the <a href="https://metacpan.org/pod/Mouse" target="_blank">Mouse documentation</a> for more details.</p>
