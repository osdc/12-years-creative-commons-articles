<p>﻿Professional design software like Photoshop is common, and yet it’s also expensive (and subscription based). What do you do if you're a designer on a tight budget? Photoshop used to be an unquestioned requirement for design work, but as open source alternatives started to arise, people realized that good art made with free tools is indistinguishable from art made with expensive proprietary ones.</p>
<p>There are quite of few free or open source alternatives, so it's helpful to try each one out and compare each to what your specific needs are. Any time you change tooling, there's some adjustment you have to make, so jump right in but don't panic when you have to re-learn tasks that you thought could be done only one way in the past. In many cases, there are online tutorials for these free programs that can help you master them quickly.</p>
<h2>Five open source or no cost tools for graphic designers</h2>
<p>These free tools will ease the financial concerns of designers still operating on a college budget.</p>
<h3>GIMP<em> (open source, no cost)</em></h3>
<p><em></em></p>
<article class="media media--type-image media--view-mode-full" title="GNU Image Manipulation Program 2.10"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/gimp-2.10_0.jpg" width="675" height="360" alt="GNU Image Manipulation Program 2.10" title="GNU Image Manipulation Program 2.10" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>GNU Image Manipulation Program 2.10</p>
</div>
      
  </article><p></p>
<p>The most well-known alternative to Photoshop is the <a href="http://www.gimp.org/" target="_blank" title="Gimp">GNU Image Manipulation Program (GIMP)</a>. It is also the most feature-rich free program, allowing you flexibility that is closer to the more expensive options. Photoshop, of course, sets the standard for cutting-edge photo-editing features, but fortunately almost everything that can be done in Photoshop can be done in the GIMP. The GIMP is also a cross-platform program supported by a huge online community. It can also be easily installed with Windows installers.</p>
<h3>Krita<em> (open source, no cost)</em></h3>
<p><article class="media media--type-image media--view-mode-full" title="Krita 4.0, art by Galaad G"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/krita-2020-galaad-g.jpeg" width="675" height="405" alt="Krita 4.0, art by Galaad G" title="Krita 4.0, art by Galaad G" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>Krita 4.0, art by Galaad G</p>
</div>
      
  </article></p>
<p><a href="http://krita.org/" target="_blank" title="Krita">Krita</a> is great for those who want to sketch or paint, and has one of the best (if not the best) brush emulators in the business. If you are creating cartoons, textures, concept art, anime, digital paintings, Krita supports almost all graphics tablets from the moment of installation. It also comes with a fair number of useful features, and is geared toward paint emulation over photo manipulation. It is robust enough to do photo editing, although if you're used to Photoshop or GIMP, you'll have to change your workflow in some major ways. That can be a useful change of paradigm, though, so if you're curious about what exciting things you can do to images with a new set of tools, Krita might open up some exciting ideas. And if you're a digital painter, Krita is the obvious choice (and has been seen "in the wild" on several high profile projects).</p>
<h3>Pinta<em> (open source, no cost)</em></h3>
<p><em></em></p>
<article class="media media--type-image media--view-mode-full" title="Pinta"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/pinta-2020.jpg" width="675" height="358" alt="Pinta" title="Pinta" /></div>
      
  </article><p></p>
<p><a href="https://pinta-project.com/pintaproject/pinta/" target="_blank" title="GIMPshop">Pinta</a> is ostensibly a simplified paint program but a closer looks reveals that it's got many advanced features suitable to graphic design and photo editing. It has layers and history, adjustments for levels, hue, saturation, a curve editor, and many standard effects. As an added bonus, it has a pixel grid view for artists working in a retro 8-bit style (especially useful for video game art). It also features an add-in manager so you can download and install new plugins right from the Pinta interface. Compared to GIMP or Krita, Pinta is certainly a basic editing application, but it helps define the base level expectations of a modern image editor, and it's a powerful tool in its own right.</p>
<h3>Darktable<em> (open source, no cost)</em></h3>
<p><img alt="" src="https://opensource.com/sites/default/files/dt_lighttable.jpg" width="520" height="309" /></p>
<p><a href="https://opensource.com/life/16/4/how-use-darktable-digital-darkroom" target="_blank" title="Pixlr Editor">Darktable</a> is dedicated to enhancing photos using the same workflow you'd use when developing and processing film. For users new to dark room concepts, this can be a shift in how specific looks or effects are achieved. However, once you get used to dynamic adjustment filters and presets, Darktable will make it shockingly easy (and fun!) for you to sort through hundreds of photos to find and enhance the best of the best. Darktable is a must-have for any digital photographer.</p>
<h3><em>Shotwell (open source, no cost)</em></h3>
<p><article class="media media--type-image media--view-mode-full" title="Shotwell"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/shotwell-2020_0.jpg" width="675" height="380" alt="Shotwell" title="Shotwell" /></div>
      
  </article></p>
<p><a href="https://wiki.gnome.org/Apps/Shotwell" target="_blank">Shotwell</a> is admittedly a lot less like Photoshop than, say, Google Picasa. It's a simple desktop app by the creators of the GNOME desktop for Linux, and it doesn't pretend to be anything more than that. But sometimes that's all you need. Shotwell has only the most basic adjustment settings to enhance good photos and images, and excels at organization. If you're looking to quickly find the best shot out of a batch, enhance and resize it, then Shotwell may well be all you need.</p>
<hr /><p>There are many more excellent open source design tools, but this short list is a good start if you're new. Tell us your favorite open source design applications in the comments!</p>
<p>This article was originally published in 2012 and has been updated to include additional information.</p>
