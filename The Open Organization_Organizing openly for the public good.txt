<p>The open organization community at Opensource.com welcomed the second half of 2016 with a Twitter chat discussing "open organizations and non-profits." Voices old and new participated, and our chat was (as always) compelling. Read the recap below as you prepare for <a href="https://opensource.com/open-organization/resources/twitter-chats">the next #OpenOrgChat</a>.</p>
<h2>Q1: What makes a union between OpenOrg thinking and non-profit work so appealing? #OpenOrgChat</h2>
<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr" xml:lang="en">A1: Non-profits and OpenOrg thinking thrive on passion &amp; purpose! <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— David Egts (@davidegts) <a href="https://twitter.com/davidegts/status/756188959005347840">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">A1:Software Freedom issues are essential to everyone on a societal level, and it's orgs that are doing a lot of this work <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— karen sandler (@o0karen0o) <a href="https://twitter.com/o0karen0o/status/756189281509605376">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-conversation="none" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">A1 While we should partner with companies, and they can do good work, we can't rely on them to have our best interests at heart <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— karen sandler (@o0karen0o) <a href="https://twitter.com/o0karen0o/status/756189984906960896">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">A.1. <a href="https://twitter.com/hashtag/OpenOrgs?src=hash">#OpenOrgs</a> bring out the individual passion and collaborative mindset -- both critical to <a href="https://twitter.com/hashtag/NonProfit?src=hash">#NonProfit</a> organizations <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— E.G.Nadhan (@NadhanEG) <a href="https://twitter.com/NadhanEG/status/756189282734333953">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">A1 many <a href="https://twitter.com/hashtag/nonprofit?src=hash">#nonprofit</a> orgs are de-centralized w/ national &amp; state chapters -  open org makes collab easier <a href="https://twitter.com/hashtag/openorgchat?src=hash">#openorgchat</a> <a href="https://t.co/U5F3PrbS6e">https://t.co/U5F3PrbS6e</a></p>
<p>— JP Sherman (@jpsherman) <a href="https://twitter.com/jpsherman/status/756189368633655297">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">A1.  Employees in <a href="https://twitter.com/hashtag/Nonprofits?src=hash">#Nonprofits</a> and <a href="https://twitter.com/hashtag/OpenOrgs?src=hash">#OpenOrgs</a> do what they do because they want to do it -- not because they have to do it <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— E.G.Nadhan (@NadhanEG) <a href="https://twitter.com/NadhanEG/status/756189576889327617">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">RT <a href="https://twitter.com/jhibbets">@jhibbets</a>: A1: Open Organizations and NPO's share a very important component: They are both mission-driven organizations <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— jackieyeaney (@jackieyeaney) <a href="https://twitter.com/jackieyeaney/status/756190218890412032">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">A1: <a href="https://twitter.com/hashtag/openorg?src=hash">#openorg</a> thinking promotes volunteerism, which is the fuel for non-profits. <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— Guy Martin (@guyma) <a href="https://twitter.com/guyma/status/756190207494361089">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">AI: Charities can provide open orgs with opportunities when their missions align w/ the public interest. <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— Tony Sebro (@keynote2k) <a href="https://twitter.com/keynote2k/status/756190437354967040">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><h2>Q2: How does an organization's status as non-profit affect organizational culture? #OpenOrgChat</h2>
<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr" xml:lang="en">A2: The mission is the cultural motivator for non-profits &amp; its employees. <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— David Egts (@davidegts) <a href="https://twitter.com/davidegts/status/756190694017040384">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-conversation="none" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">.<a href="https://twitter.com/openorgbook">@openorgbook</a> A2 it sharpens focus on things like smart use of limited resources, and being lean and effective. <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— Thomas Cameron (@thomasdcameron) <a href="https://twitter.com/thomasdcameron/status/756190841673101314">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-conversation="none" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">.<a href="https://twitter.com/openorgbook">@openorgbook</a> A2 it makes folks really focus on "bang for the buck," since bucks are often scarce for non-profits. <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— Thomas Cameron (@thomasdcameron) <a href="https://twitter.com/thomasdcameron/status/756191030265774081">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">A2: In my experience, the culture for non-profits lends itself to more passion and buy-in  <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— Jason Hibbets (@jhibbets) <a href="https://twitter.com/jhibbets/status/756190931838181376">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">A2: Nonprofit can mean many things. <a href="https://twitter.com/conservancy">@conservancy</a> is a charity, like <a href="https://twitter.com/fsf">@fsf</a> or <a href="https://twitter.com/gnome">@gnome</a>. Our status aligns us w/public good. <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— karen sandler (@o0karen0o) <a href="https://twitter.com/o0karen0o/status/756191014826696705">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">Especially in <a href="https://twitter.com/hashtag/FOSS?src=hash">#FOSS</a> charities, staff is deeply committed and volunteers know they have the best chance of long-term engagement <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— karen sandler (@o0karen0o) <a href="https://twitter.com/o0karen0o/status/756191477370322944">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-conversation="none" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">.<a href="https://twitter.com/openorgbook">@openorgbook</a> A2 it makes folks really focus on "bang for the buck," since bucks are often scarce for non-profits. <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— Thomas Cameron (@thomasdcameron) <a href="https://twitter.com/thomasdcameron/status/756191030265774081">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">A2 many nonprofits try to maximize impact w/ less resources - with a community's shared direction, many more hands can help <a href="https://twitter.com/hashtag/openorgchat?src=hash">#openorgchat</a></p>
<p>— JP Sherman (@jpsherman) <a href="https://twitter.com/jpsherman/status/756191227079458816">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-conversation="none" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en"><a href="https://twitter.com/openorgbook">@openorgbook</a> People tend to work in NPOs for love of the work rather than for monetary reasons. Same orientation for OpenOrgs. <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— Leslie Hawthorn (@lhawthorn) <a href="https://twitter.com/lhawthorn/status/756191442884825088">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">True. Non-profit organd OpenOrg works best when community, contributors, investors are engagged most. <a href="https://twitter.com/hashtag/OpenOrgchat?src=hash">#OpenOrgchat</a> <a href="https://t.co/4dPBsLYOoO">https://t.co/4dPBsLYOoO</a></p>
<p>— Moin Shaikh (@moingshaikh) <a href="https://twitter.com/moingshaikh/status/756191555581456384">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">A2: NPO status helps center culture on the purpose but can make innovation slow <a href="https://twitter.com/hashtag/openorgchat?src=hash">#openorgchat</a></p>
<p>— jackieyeaney (@jackieyeaney) <a href="https://twitter.com/jackieyeaney/status/756191855570718720">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">A2. e.g., <a href="https://twitter.com/conservancy">@conservancy</a> maintains our corporate policies in a public DVCS. <a href="https://t.co/SWIu0P41ES">https://t.co/SWIu0P41ES</a> <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— Tony Sebro (@keynote2k) <a href="https://twitter.com/keynote2k/status/756192236472307712">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">A2: People tend to work in NPOs for love of the work rather than for monetary reasons. Same orientation for Open Orgs. <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— Leslie Hawthorn (@lhawthorn) <a href="https://twitter.com/lhawthorn/status/756192537023479808">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">A2: ive found it centered on reaching out to find volunteers to help collaborate, culture is very centered on bringing ppl in <a href="https://twitter.com/hashtag/openorgchat?src=hash">#openorgchat</a></p>
<p>— Máirín Duffy (@mairin) <a href="https://twitter.com/mairin/status/756192466638962688">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><h2>Q3: What can open organizations learn from successful non-profits? #OpenOrgChat</h2>
<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr" xml:lang="en">A3: How to build trust &amp; do inclusive decision making like <a href="https://twitter.com/carverc">@carverc</a> of <a href="https://twitter.com/UABIT">@UABIT</a> <a href="https://t.co/rtdolcDmtH">https://t.co/rtdolcDmtH</a> <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— David Egts (@davidegts) <a href="https://twitter.com/davidegts/status/756192656749920256">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-conversation="none" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">.<a href="https://twitter.com/openorgbook">@openorgbook</a> A3 How to harness that passion to make the mission successful. <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— Thomas Cameron (@thomasdcameron) <a href="https://twitter.com/thomasdcameron/status/756192744985366528">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">A3: Three answers: 1) How to scale 2) How to define a great mission 3) Creating a shared-purpose  <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— Jason Hibbets (@jhibbets) <a href="https://twitter.com/jhibbets/status/756192807413506048">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">A3: I think there's a maturity model: communications&gt;transparency&gt;self-organization&gt;evidence-based decisions&gt;meritocracy. <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— Patrick Masson (@massonpj) <a href="https://twitter.com/massonpj/status/756192942910541824">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">A3: Keeping external stakeholders regularly briefed on org goals is just as vital as maintaining internal transparency. <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— Leslie Hawthorn (@lhawthorn) <a href="https://twitter.com/lhawthorn/status/756192935662813184">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">A3: How to break down heirarchies &amp; implement meritocracy effectively. <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— Guy Martin (@guyma) <a href="https://twitter.com/guyma/status/756193595154018305">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">A3. You have to work hard to earn the next round of funding in <a href="https://twitter.com/hashtag/Nonprofits?src=hash">#Nonprofits</a> :: There is no steady revenue stream <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— E.G.Nadhan (@NadhanEG) <a href="https://twitter.com/NadhanEG/status/756193733578723328">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-conversation="none" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en"><a href="https://twitter.com/openorgbook">@openorgbook</a> mission-oriented scrappiness &amp; a genuine celebration of the team's wins made on behalf of the customers/users <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— David Ryan (@DaveDri) <a href="https://twitter.com/DaveDri/status/756194060373811201">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><h2>Q4: What are your most effective strategies for aligning passion and purpose? #OpenOrgChat</h2>
<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr" xml:lang="en">A4 Making sure that we focus on the most important issues, even if corporate funders aren't interested <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— karen sandler (@o0karen0o) <a href="https://twitter.com/o0karen0o/status/756194780686741504">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">A4: Ask people what they enjoy doing. Are they Happy? <a href="https://twitter.com/hashtag/openorgchat?src=hash">#openorgchat</a></p>
<p>— Jeff Mackanic (@mackanic) <a href="https://twitter.com/mackanic/status/756194770284937217">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">A4: Get folks together to explicitly discuss org values. Document visibly &amp; make them an integral part of org culture. <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— Leslie Hawthorn (@lhawthorn) <a href="https://twitter.com/lhawthorn/status/756195008546533376">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">A4: (cont'd) When tackling tough problems or controversial topics, focus conversation through the lens of *shared* org values. <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— Leslie Hawthorn (@lhawthorn) <a href="https://twitter.com/lhawthorn/status/756195336524357632">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">A4. Checkout the mission and vision for the <a href="https://twitter.com/hashtag/Nonprofit?src=hash">#Nonprofit</a> before jumping in :: Aligning passion and purpose :: <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— E.G.Nadhan (@NadhanEG) <a href="https://twitter.com/NadhanEG/status/756195081212887045">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">A4 1) Hiring well. 2) Acculturation. 3) Reinforcement of mission. 4) Public recognition of impact. 5) Garner feedback. <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— Thomas Cameron (@thomasdcameron) <a href="https://twitter.com/thomasdcameron/status/756195166680035333">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">A4 Trying to prioritize the time to talk about the work you're actually doing <a href="https://twitter.com/hashtag/openorgchat?src=hash">#openorgchat</a></p>
<p>— karen sandler (@o0karen0o) <a href="https://twitter.com/o0karen0o/status/756195445647474688">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">A4 Actively seek out other orgs who's values &amp; goals have natural overlaps with your goals - cross pollinate &amp; help each other <a href="https://twitter.com/hashtag/openorgchat?src=hash">#openorgchat</a></p>
<p>— JP Sherman (@jpsherman) <a href="https://twitter.com/jpsherman/status/756195639143309312">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">A4: make it fun, give people permission to ‘brag’ a little about how they are helping the mission. <a href="https://twitter.com/hashtag/openorgchat?src=hash">#openorgchat</a></p>
<p>— Guy Martin (@guyma) <a href="https://twitter.com/guyma/status/756196195526074368">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">Part of a trending <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a> :: Learnt a new word <a href="https://twitter.com/hashtag/acculturation?src=hash">#acculturation</a> :: What a Day !! this must be an  <a href="https://twitter.com/hashtag/OpenOrg?src=hash">#OpenOrg</a> !!! <a href="https://twitter.com/hashtag/RedHat?src=hash">#RedHat</a></p>
<p>— E.G.Nadhan (@NadhanEG) <a href="https://twitter.com/NadhanEG/status/756196674712854528">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><h2>Q5: What challenges do non-profits encounter when they try to open up (bet @lhawthorn has stories!)? #OpenOrgChat</h2>
<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr" xml:lang="en">A5: W/o a clear mission, passionate people may have well intended but widely diverging views. <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— David Egts (@davidegts) <a href="https://twitter.com/davidegts/status/756198223627313152">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">A5: comment threads on <a href="https://twitter.com/reddit">@reddit</a> , <a href="https://twitter.com/HackerNews">@HackerNews</a> and <a href="https://twitter.com/slashdot">@slashdot</a> j/k! <a href="https://twitter.com/hashtag/openorgchat?src=hash">#openorgchat</a></p>
<p>— karen sandler (@o0karen0o) <a href="https://twitter.com/o0karen0o/status/756198392888426496">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">A5: Individuals feel very insecure that people will see their failings when the org opens up. This is not NPO specific! <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— Leslie Hawthorn (@lhawthorn) <a href="https://twitter.com/lhawthorn/status/756198488363429888">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">A5 It's a potentially disruptive culture change (see Cathedral and the Bazaar). It can freak people out to break down walls. <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— Thomas Cameron (@thomasdcameron) <a href="https://twitter.com/thomasdcameron/status/756198574325510144">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">A5: Taking in critique can be very difficult when the org first opens up, especially when org members are driven by passion. <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— Leslie Hawthorn (@lhawthorn) <a href="https://twitter.com/lhawthorn/status/756198647071666176">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-conversation="none" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en"><a href="https://twitter.com/lhawthorn">@lhawthorn</a> so maaaaybe my joke about comment threads wasn't *that* much of a joke! <a href="https://twitter.com/hashtag/openorgchat?src=hash">#openorgchat</a></p>
<p>— karen sandler (@o0karen0o) <a href="https://twitter.com/o0karen0o/status/756198948470226944">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">A5: Lawyers are often taught to err on the side of nondisclosure by default. But that may not be what's best for the mission <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— Tony Sebro (@keynote2k) <a href="https://twitter.com/keynote2k/status/756200542217666560">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><h2>Q6: What books would you recommend to someone interested in OpenOrg thinking and non-profit work? #OpenOrgChat</h2>
<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr" xml:lang="en">A6: <a href="https://twitter.com/hashtag/TeamOfTeams?src=hash">#TeamOfTeams</a> by <a href="https://twitter.com/StanMcChrystal">@StanMcChrystal</a>. Bringing diverse teams together to work as one. <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— David Egts (@davidegts) <a href="https://twitter.com/davidegts/status/756199882478739456">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">A6: <a href="https://twitter.com/hashtag/Grunt?src=hash">#Grunt</a> by <a href="https://twitter.com/mary_roach">@mary_roach</a>. Military scientists support troop safety &amp; effectiveness via teamwork &amp; comms <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— David Egts (@davidegts) <a href="https://twitter.com/davidegts/status/756200029614960640">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">A6: The Power of a Positive No, in fact anything from Harvard Negotiation Series of publications. Useful for *everyone*! <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— Leslie Hawthorn (@lhawthorn) <a href="https://twitter.com/lhawthorn/status/756200057670627328">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-cards="hidden" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">humans are poor predictors of their own behavior, knowing that can help - here's a good book on that <a href="https://t.co/Rr8smJchty">https://t.co/Rr8smJchty</a> <a href="https://twitter.com/hashtag/openorgchat?src=hash">#openorgchat</a></p>
<p>— JP Sherman (@jpsherman) <a href="https://twitter.com/jpsherman/status/756200209848397824">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-conversation="none" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">A6: Facilitators teach Open Org principles to attendees throughout event.  If you can attend, do so! <a href="https://t.co/dtw02nUQkb">https://t.co/dtw02nUQkb</a> <a href="https://twitter.com/hashtag/openorgchat?src=hash">#openorgchat</a></p>
<p>— Leslie Hawthorn (@lhawthorn) <a href="https://twitter.com/lhawthorn/status/756201888241115136">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><h2>Q7: What unique value do non-profits bring to open source communities specifically? (we're looking at @o0karen0o!) #OpenOrgChat</h2>
<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr" xml:lang="en">A7 Nonprofits have a broader perspective in mind &amp; can think long term in a way that companies can't.  <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— karen sandler (@o0karen0o) <a href="https://twitter.com/o0karen0o/status/756200629568147456">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">A7  charities can't pivot away from the public good. You can vote with your money &amp; show that certain causes are important. <a href="https://twitter.com/hashtag/openorgchat?src=hash">#openorgchat</a></p>
<p>— karen sandler (@o0karen0o) <a href="https://twitter.com/o0karen0o/status/756200769184038912">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">We have to acknowledge the issues around ethical technology and the responsibility we have as its stewards. <a href="https://twitter.com/hashtag/openorgchat?src=hash">#openorgchat</a></p>
<p>— karen sandler (@o0karen0o) <a href="https://twitter.com/o0karen0o/status/756200886423195648">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">A7: Non-profits are an advocate for a cause &amp; gathering place for people with a common goal. <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— David Egts (@davidegts) <a href="https://twitter.com/davidegts/status/756200778881232896">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr" xml:lang="en">A7: Thinking specifically about foundations, they bring organization, structure, and stability to many open source projects <a href="https://twitter.com/hashtag/OpenOrgChat?src=hash">#OpenOrgChat</a></p>
<p>— Jason Hibbets (@jhibbets) <a href="https://twitter.com/jhibbets/status/756200898951516160">July 21, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script>