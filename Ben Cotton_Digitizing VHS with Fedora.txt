<p>Just before Christmas, I decided it was time for my kids to see one of my favorite movies: <em><a href="http://www.imdb.com/title/tt0104940/" target="_blank">The Muppet Christmas Carol</a></em>. I grabbed the tape (yes, tape) off the shelf and put it in the VCR (yes, VCR) and... nothing happened. Oh no!</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Linux resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux commands cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Free online course: RHEL Technical Overview</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux networking cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">SELinux cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux common commands cheat sheet</a></li>
<li><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">What are Linux containers?</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Our latest Linux articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>I have a dozen or so movies on VHS that we still watch. To be honest, I'm not that concerned about the commercial movies; those are easy enough to replace. But what about our home movies? My high school cross country team videos and my wife's marching band videos, among others—you won't find those on Netflix anytime soon. So I decided it was time to get serious about something I'd been meaning to do for a long time: Digitize my VHS tapes.</p>
<p>In this article, I'll describe how I set up my Fedora desktop to convert my VHS tapes into 1s and 0s. Previously, Don Watkins described <a href="https://opensource.com/life/15/6/VHS-conversion-MP4-Linux" target="_blank">a different setup for VHS conversion</a>.</p>
<h3>Step 1</h3>
<p>The first thing I needed was a video capture card. Years ago, I had one that I had used for a <a href="https://www.mythtv.org/" target="_blank">MythTV</a> setup. I knew the manufacturer was well-supported in Linux, so I just needed to find a model that had RCA input. I bought a used Hauppauge WinTV PVR-150 and installed it in an available PCI slot.</p>
<p>The kernel recognized the capture card. So far, so good.</p>
<h3>Step 2</h3>
<p>I opened <a href="http://www.videolan.org/vlc/index.html" target="_blank">VLC</a> and tried to view the input. No luck. After nosing around in <code>dmesg</code> output, I discovered that I didn't have the firmware for that card (v4l-cx2341x-enc, specifically). Fortunately, Fedora's ivtv-firmware package has the files I needed. After a <code>dnf install ivtv-firmware</code> and a reboot, I was off to the races.</p>
<p>Or not. VLC still wasn't showing me anything, but I could watch the video input from <a href="http://www.mplayerhq.hu/design7/news.html" target="_blank">mplayer</a>. More specifically, I could watch static. But, it was progress.</p>
<h3>Step 3</h3>
<p>The last step was to tell the card which input I wanted it to use. I installed the v4l-utils package, and after some trial and error found that <code>v4l2-ctl -i 2</code> set it to use the RCA input (instead of coax or S-video).</p>
<p>With that done, <code>mplayer /dev/video0</code> gave me the output from my VCR. Now to save it to a file. <code>mplayer -cache 8192 /dev/video0 -dumpstream -dumpfile my_video.mp4</code> was the command I needed. The <code>-cache 8192</code> helped suppress some occasional error messages about my computer being too slow (it's not, I promise).</p>
<h3>That's a wrap!</h3>
<p>That's all it took; a few steps and I was converting my VHS tapes to digital files. The only downside is that mplayer can't tell when the tape has reached the end, so it requires some careful babysitting to stop the capture. Once the files are done, I use <a href="https://kdenlive.org/" target="_blank">Kdenlive</a> to snip off the extra at the beginning and end.</p>
<p>Now you can digitize your own tapes before VCRs completely vanish from the face of the earth. Have suggestions for how to improve this workflow? Let us know in the comments below.</p>
