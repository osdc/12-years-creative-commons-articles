<p>A few days ago, I received an email from a reader of one of my books. Among other things, he said that he was having trouble getting to one of the websites I'd referenced in the book. I responded that I would check it out. Usually, something like this is due to a misprinted URL in the referring article or book, or it could be that I'd deleted or changed a page on my website.</p>
<p>That was not the case this time. When I clicked on the link to my website, I was faced with—horror of horrors—an online casino.</p>
<p>I thought this would turn out to be a simple case of DNS <a href="https://opensource.com/article/20/4/mitm-attacks" target="_blank">man-in-the-middle</a> or something similar. Certainly, nothing would be wrong on my own server.</p>
<h2 id="_finding_the_problem">Finding the problem</h2>
<p>I use Google Domains as my registrar. Before doing anything else, I checked to ensure that my IP addresses were correct. They were.</p>
<p>I logged into a remote Linux host that I have access to, and performed a traceroute with MTR (Matt's TraceRoute). That indicated that the route to my host was correct.</p>
<p>This did not look good.</p>
<p>Next, I looked at my <code>httpd.conf</code> and verified that it was correct. I did find a couple of non-related configuration issues, and fixed those, but they didn't affect the problem at hand. I isolated my network from the internet, and tried my website again. I have internal DNS that works for that. I still came up with the invasive website. That was proof positive that the problem was an infection of my own server.</p>
<p>None of that took long. I was just working under the assumption that the problem was elsewhere rather than on my own server. Silly me!</p>
<p>I finally looked at my server's WordPress installation. I was hoping that the database hadn't been infected. I could have recovered from anything by wiping it all out and restoring from backups, but I was hoping to avoid that if possible. Unfortunately, the <code>html</code> directory of my website had some noticeable, "extra" files and one new directory. The <code>html/wp-admin/admin.php</code> file had also been replaced.</p>
<p>I was fortunate to have multiple other websites that weren't infected, so it was easy to compare file dates and sizes. I also keep complete daily backups of my websites in order to recover from problems such as this.</p>
<h2 id="_fixing_the_problem">Fixing the problem</h2>
<p>The fix, in this case, was surprisingly easy. WordPress is quite easy to install, backup, move, reinstall, and restore. I started by deleting the obvious extra files and directory. I copied the known good files from my backups over the infected ones. I could have simply restored everything from the last known good backup and that would have worked as well. I compared the good backup with the recovered website and all looked good.</p>
<p>The database for the website was not affected in any way, which I verified with manual review of a data dump.</p>
<h2 id="_the_real_problem">The real problem</h2>
<p>After analyzing the problem, I realized that <em>I </em>was the root cause. My failure to ensure that WordPress was properly updated for this website allowed this to happen. I use separate instances of WordPress for each of my websites, so the others were not affected because they were being updated automatically.</p>
<p>A series of issues led to this failure of mine.</p>
<ol class="arabic"><li>
<p>The affected website had been set up with a different email address that I'd stopped using a few months ago. This prevented me from getting the usual notices that upgrades were available.</p>
</li>
<li>
<p>I'd also failed to configure that site for automatic updates from WordPress.</p>
</li>
<li>
<p>And I didn't bother to check to see whether the site was being updated.</p>
</li>
</ol><p>When it was attacked, the site was at least one full release level behind what was available. The ones that were kept up to date were not affected by this attack.</p>
<p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">More for sysadmins</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://www.redhat.com/sysadmin/?intcmp=7013a000002CxqpAAC" data-analytics-category="resource list" data-analytics-text="Enable Sysadmin blog">Enable Sysadmin blog</a></div>
              <div class="field__item"><a href="https://go.redhat.com/automated-enterprise-ebook-20180920?intcmp=7013a000002CxqpAAC" data-analytics-category="resource list" data-analytics-text="The Automated Enterprise: A guide to managing IT with automation">The Automated Enterprise: A guide to managing IT with automation</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/ansible-quickstart?intcmp=7013a000002CxqpAAC" data-analytics-category="resource list" data-analytics-text="eBook: Ansible automation for Sysadmins">eBook: Ansible automation for Sysadmins</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/engage/system-administrator-guide-s-202107300146?intcmp=7013a000002CxqpAAC" data-analytics-category="resource list" data-analytics-text="Tales from the field: A system administrator's guide to IT automation">Tales from the field: A system administrator's guide to IT automation</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/kubernetes-sysadmin?intcmp=7013a000002CxqpAAC" data-analytics-category="resource list" data-analytics-text="eBook: A guide to Kubernetes for SREs and sysadmins">eBook: A guide to Kubernetes for SREs and sysadmins</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Register for your free Red Hat account">Register for your free Red Hat account</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/sysadmin?intcmp=7013a000002CxqpAAC" data-analytics-category="resource list" data-analytics-text="Latest sysadmin articles">Latest sysadmin articles</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<h2 id="_what_i_learned">What I learned</h2>
<p>Having written many books and articles in which I discuss the necessity to keep systems updated with the latest versions of operating system and application software, I'm frankly embarrassed by this. However, it has been a good learning experience for me and a reminder that I must not become complacent. I almost didn't write this article! I didn't want to admit to being negligent with one of my own systems. And yet, I felt compelled to write about it in the hope that you learn from my experience.</p>
<p>So, as I have learned from painful experience, it is critical to keep our systems updated. It's one of the most vital steps in the continuing battle to prevent the computers under our care from being infected. The specific details of the infection I experienced are less important than the fact that there are always attacks taking place against our systems. Complacency is one of the attack vectors that crackers can count on to aid their efforts.</p>
