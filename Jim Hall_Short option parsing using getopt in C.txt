<p>Writing a C program to process files is easy when you already know what files you'll operate on and what actions to take. If you "hard code" the filename into your program, or if your program is coded to do things only one way, then your program will always know what to do.</p>
<p>But you can make your program much more flexible if it can respond to the user every time the program runs. Let your user tell your program what files to use or how to do things differently. And for that, you need to read the command line.</p>
<h2 id="reading-the-command-line">Reading the command line</h2>
<p>When you write a program in C, you might start with the declaration:</p>
<pre><code class="language-c">int main()</code></pre><p>That's the simplest way to start a C program. But if you add these standard parameters in the parentheses, your program can read the options given to it on the command line:</p>
<pre><code class="language-c">int main(int argc, char **argv)</code></pre><p>The <code>argc</code> variable is the argument count or the number of arguments on the command line. This will always be a number that's at least one.</p>
<p>The <code>argv</code> variable is a double pointer, an array of strings, that contains the arguments from the command line. The first entry in the array, <code>*argv[0]</code>, is always the name of the program. The other elements of the <code>**argv</code> array contain the rest of the command-line arguments.</p>
<p>I'll write a simple program to echo back the options given to it on the command line. This is similar to the Linux <code>echo</code> command, except it also prints the name of the program. It also prints each command-line option on its own line using the <code>puts</code> function:</p>
<pre><code class="language-c">#include &lt;stdio.h&gt;

int
main(int argc, char **argv)
{
  int i;

  printf("argc=%d\n", argc); /* debugging */

  for (i = 0; i &lt; argc; i++) {
    puts(argv[i]);
  }

  return 0;
}</code></pre><p>Compile this program and run it with some command-line options, and you'll see your command line printed back to you, each item on its own line:</p>
<pre><code class="language-c">$ ./echo this program can read the command line
argc=8
./echo
this
program
can
read
the
command
line</code></pre><p>This command line sets the program's <code>argc</code> to <code>8</code>, and the <code>**argv</code> array contains eight entries: the name of the program, plus the seven words the user entered. And as always in C programs, the array starts at zero, so the elements are numbered 0, 1, 2, 3, 4, 5, 6, 7. That's why you can process the command line with the <code>for</code> loop using the comparison <code>i &lt; argc</code>.</p>
<p>You can use this to write your own versions of the Linux <code>cat</code> or <code>cp</code> commands. The <code>cat</code> command's basic functionality displays the contents of one or more files. Here's a simple version of <code>cat</code> that reads the filenames from the command line:</p>
<pre><code class="language-c">#include &lt;stdio.h&gt;

void
copyfile(FILE *in, FILE *out)
{
  int ch;

  while ((ch = fgetc(in)) != EOF) {
    fputc(ch, out);
  }
}

int
main(int argc, char **argv)
{
  int i;
  FILE *fileptr;

  for (i = 1; i &lt; argc; i++) {
    fileptr = fopen(argv[i], "r");

    if (fileptr != NULL) {
      copyfile(fileptr, stdout);
      fclose(fileptr);
    }
  }

  return 0;
}</code></pre><p>This simple version of <code>cat</code> reads a list of filenames from the command line and displays the contents of each file to the standard output, one character at a time. For example, if I have one file called <code>hello.txt</code> that contains a few lines of text, I can display its contents with my own <code>cat</code> command:</p>
<pre><code class="language-c">$ ./cat hello.txt 
Hi there!
This is a sample text file.</code></pre><p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Programming and development</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ&amp;src=programming_resource_menu4">Red Hat Developers Blog</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Programming cheat sheets</a></li>
<li><a href="https://www.redhat.com/en/services/training/learning-subscription?intcmp=7016000000127cYAAQ">Try for free: Red Hat Learning Subscription</a></li>
<li><a href="https://opensource.com/downloads/bash-programming-guide?intcmp=7016000000127cYAAQ">eBook: An introduction to programming with Bash</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/bash-shell-cheat-sheet?intcmp=7016000000127cYAAQ">Bash Shell Scripting Cheat Sheet</a></li>
<li><a href="https://developers.redhat.com/e-books/modernizing-enterprise-java?intcmp=7016000000127cYAAQ">eBook: Modernizing Enterprise Java</a></li>
</ul></div>
</div>
</div>
</div>
<p>Using this sample program as a starting point, you can write your own versions of other Linux commands, such as the <code>cp</code> program, by reading only two filenames: one file to read from and another file to write the copy.</p>
<h2 id="reading-command-line-options">Reading command-line options</h2>
<p>Reading filenames and other text from the command line is great, but what if you want your program to change its behavior based on the <em>options</em> the user gives it? For example, the Linux <code>cat</code> command supports several command-line options, including:</p>
<ul><li><code>-b</code> Put line numbers next to non-blank lines</li>
<li><code>-E</code> Show the ends of lines as <code>$</code></li>
<li><code>-n</code><code> </code>Put line numbers next to all lines</li>
<li><code>-s</code> Suppress printing repeated blank lines</li>
<li><code>-T</code> Show tabs as <code>^I</code></li>
<li><code>-v</code><code> </code>Verbose; show non-printing characters using <code>^x</code> and <code>M-x</code> notation, except for new lines and tabs</li>
</ul><p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Programming and development</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ&amp;src=programming_resource_menu4">Red Hat Developers Blog</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Programming cheat sheets</a></li>
<li><a href="https://www.redhat.com/en/services/training/learning-subscription?intcmp=7016000000127cYAAQ">Try for free: Red Hat Learning Subscription</a></li>
<li><a href="https://opensource.com/downloads/bash-programming-guide?intcmp=7016000000127cYAAQ">eBook: An introduction to programming with Bash</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/bash-shell-cheat-sheet?intcmp=7016000000127cYAAQ">Bash Shell Scripting Cheat Sheet</a></li>
<li><a href="https://developers.redhat.com/e-books/modernizing-enterprise-java?intcmp=7016000000127cYAAQ">eBook: Modernizing Enterprise Java</a></li>
</ul></div>
</div>
</div>
</div>
<p>These <em>single-letter</em> options are called <em>short options</em>, and they always start with a single hyphen character. You usually see these short options written separately, such as <code>cat -E -n</code>, but you can also combine the short options into a single <em>option string</em> such as <code>cat -En</code>.</p>
<p>Fortunately, there's an easy way to read these from the command line. All Linux and Unix systems include a special C library called <code>getopt</code>, defined in the <code>unistd.h</code> header file. You can use <code>getopt</code> in your program to read these short options.</p>
<p>Unlike other Unix systems, <code>getopt</code> on Linux will always ensure your short options appear at the front of your command line. For example, say a user types <code>cat -E file -n</code>. The <code>-E</code> option is upfront, but the <code>-n</code> option is after the filename. But if you use Linux <code>getopt</code>, your program will always behave as though the user types <code>cat -E -n file</code>. That makes processing a breeze because <code>getopt</code> can parse the short options, leaving you a list of filenames on the command line that your program can read using the <code>**argv</code> array.</p>
<p>You use <code>getopt</code> like this:</p>
<pre><code class="language-c">       #include &lt;unistd.h&gt;

       int getopt(int argc, char **argv, char *optstring);</code></pre><p>The option string <code>optstring</code> contains a list of the valid option characters. If your program only allows the <code>-E</code> and <code>-n</code> options, you use "<code>En"</code> as your option string.</p>
<p>You usually use <code>getopt</code> in a loop to parse the command line for options. At each <code>getopt</code> call, the function returns the next short option it finds on the command line or the value <code>'?'</code> for any unrecognized short options. When <code>getopt</code> can't find any more short options, it returns <code>-1</code> and sets the global variable <code>optind</code> to the next element in <code>**argv</code> after all the short options.</p>
<p>Let's look at a simple example. This demo program isn't a full replacement of <code>cat</code> with all the options, but it can parse its command line. Every time it finds a valid command-line option, it prints a short message to indicate it was found. In your own programs, you might instead set a variable or take some other action that responds to that command-line option:</p>
<pre><code class="language-c">#include &lt;stdio.h&gt;
#include &lt;unistd.h&gt;

int
main(int argc, char **argv)
{
  int i;
  int option;

  /* parse short options */

  while ((option = getopt(argc, argv, "bEnsTv")) != -1) {
    switch (option) {
    case 'b':
      puts("Put line numbers next to non-blank lines");
      break;
    case 'E':
      puts("Show the ends of lines as $");
      break;
    case 'n':
      puts("Put line numbers next to all lines");
      break;
    case 's':
      puts("Suppress printing repeated blank lines");
      break;
    case 'T':
      puts("Show tabs as ^I");
      break;
    case 'v':
      puts("Verbose");
      break;
    default:                          /* '?' */
      puts("What's that??");
    }
  }

  /* print the rest of the command line */

  puts("------------------------------");

  for (i = optind; i &lt; argc; i++) {
    puts(argv[i]);
  }

  return 0;
}</code></pre><p>If you compile this program as <code>args</code>, you can try out different command lines to see how they parse the short options and always leave you with the rest of the command line. In the simplest case, with all the options up front, you get this:</p>
<pre><code class="language-c">$ ./args -b -T file1 file2
Put line numbers next to non-blank lines
Show tabs as ^I
------------------------------
file1
file2</code></pre><p>Now try the same command line but combine the two short options into a single option string:</p>
<pre><code class="language-c">$ ./args -bT file1 file2
Put line numbers next to non-blank lines
Show tabs as ^I
------------------------------
file1
file2</code></pre><p>If necessary, <code>getopt</code> can "reorder" the command line to deal with short options that are out of order:</p>
<pre><code class="language-c">$ ./args -E file1 file2 -T
Show the ends of lines as $
Show tabs as ^I
------------------------------
file1
file2</code></pre><p>If your user gives an incorrect short option, <code>getopt</code> prints a message:</p>
<pre><code class="language-c">$ ./args -s -an file1 file2
Suppress printing repeated blank lines
./args: invalid option -- 'a'
What's that??
Put line numbers next to all lines
------------------------------
file1
file2</code></pre><h2>Download the cheat sheet</h2>
<p><code>getopt</code> can do lots more than what I've shown. For example, short options can take their own options, such as <code>-s string</code> or <code>-f file</code>. You can also tell <code>getopt</code> to not display error messages when it finds unrecognized options. Read the <code>getopt(3)</code> manual page using <code>man 3 getopt</code> to learn more about what <code>getopt</code> can do for you.</p>
<p>If you're looking for gentle reminders on the syntax and structure of <code>getopt()</code> and <code>getopt_long()</code>, <a href="https://opensource.com/downloads/c-getopt-cheat-sheet">download my getopt cheat sheet</a>. One page demonstrates short options, and the other side demonstrates long options with minimum viable code and a listing of the global variables you need to know.</p>
