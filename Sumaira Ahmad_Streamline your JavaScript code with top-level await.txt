<p>JavaScript is a popular programming language that started out as a single-threaded, synchronous language, meaning that one step runs after another step without waiting to fetch any external resources or perform any lengthy calculation or process. If the script requires such a resource or calculation, this synchronous behavior results in an error. This blocks all other processes in the queue from running whether or not they were dependent on the step that produced the error.</p>
<p>But some time ago, JavaScript introduced a new feature that made it possible to wait only for code that requires an external resource to load or a lengthy process to complete while processing and rendering the rest of the code. This asynchronous behavior is achieved by using callbacks or promises, which work at the function level.</p>
<h2 id="what-are-callbacks-and-promises">What are callbacks and promises?</h2>
<p>I'll explain these concepts through the help of code. If you already know what callbacks and promises are, feel free to <a href="#toplevelawait">skip down</a> to the top-level await section and example application.</p>
<h3 id="callbacks">Callbacks</h3>
<p>In a callback, one function is passed to another function as an argument; therefore, the second argument in the <code>addEventListener</code> function below is a callback function. This callback will wait for the first argument to happen and—only then—execute the body of the second argument.</p>
<pre><code class="language-javascript">const x = document.getElementsByTagName('Button');
x[0].addEventListener('click',() =&gt;{alert("I was clicked")})</code></pre><p>This waiting behavior makes the code asynchronous. This is unlike synchronous code, where tasks are performed one after another without waiting for a resource to download or a lengthy calculation to process. Note, however, that not all callback functions are asynchronous.</p>
<h3 id="promises">Promises</h3>
<p>Promises are similar to callbacks in the sense that they attach a function to the returning object. However, there are differences between callbacks and promises. Promises are designed mainly for asynchronous functions. They only have one argument, and a <code>.then()</code> function is chained after the results once its only argument returns. Also, there can be multiple <code>.then()</code> and <code>catch()</code> functions attached to it.</p>
<pre><code class="language-javascript">fetch('www.xyz.com/api')
.then((res)=&gt;{let x = res.data; //do something with received data})
.catch((err)=&gt;{console.log(err)})</code></pre><p>Promises use an event queue and strictly follow the order in which the tasks are chained.</p>
<h3>Async/await</h3>
<p>Async/await is the syntactical modification of promises to avoid chaining. It makes the code a lot cleaner and easier to understand. The <code>await</code> keyword makes the code halt until the promise is resolved or rejected.</p>
<pre><code class="language-javascript">async function asyncwaitcode(){
  let getData = await axios('www.xyzdata.org/api')
  console.log(getData.data)
} </code></pre><h2><a id="toplevelawait" name="toplevelawait"></a>What is top-level await?</h2>
<p>All of the examples above make the steps in functional blocks asynchronous, and none works at the modular level.</p>
<p>However, asynchronous behavior can be achieved at the modular level. The purpose of using top-level await is to allow modules to asynchronously initialize their namespace before notifying the consumer that the module has finished evaluating.</p>
<p>The following example app uses top-level await to show how this works.</p>
<h3 id="about-the-app">About the app</h3>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Programming and development</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ&amp;src=programming_resource_menu4">Red Hat Developers Blog</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Programming cheat sheets</a></li>
<li><a href="https://www.redhat.com/en/services/training/learning-subscription?intcmp=7016000000127cYAAQ">Try for free: Red Hat Learning Subscription</a></li>
<li><a href="https://opensource.com/downloads/bash-programming-guide?intcmp=7016000000127cYAAQ">eBook: An introduction to programming with Bash</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/bash-shell-cheat-sheet?intcmp=7016000000127cYAAQ">Bash Shell Scripting Cheat Sheet</a></li>
<li><a href="https://developers.redhat.com/e-books/modernizing-enterprise-java?intcmp=7016000000127cYAAQ">eBook: Modernizing Enterprise Java</a></li>
</ul></div>
</div>
</div>
</div>
<p>This app will fetch the top news data from a news API and render it locally in the browser. This app also has search functionality so that users can get news data about a searched term. Before I start, there are few caveats to know:</p>
<ul><li>Top-level await is supported in node version 13.3 and above, and none of these versions has LTS (long-term support) at this time.</li>
<li>Top-level await is supported only in the <a href="https://en.wikipedia.org/wiki/ECMAScript" target="_blank">ECMAScript</a> module, and <a href="https://nodejs.org/" target="_blank">Node.js</a> and <a href="https://expressjs.com/" target="_blank">Express</a> use <a href="https://en.wikipedia.org/wiki/CommonJS" target="_blank">CommonJS</a> modules. However, support for the ECMAScript module has been added in Node.js. So, instead of using <code>require</code> to fetch the modules, I use <code>import</code> in the entire app. CommonJS does not support top-level await.</li>
<li>Top-level await doesn't work with named export; it only works with default exports.</li>
<li>In versions prior to Node.js 14.x, top-level await doesn't run out of the box. Instead, you have to use Google <a href="https://v8.dev/" target="_blank">V8 engine</a>'s <code>--harmony</code> top-level await flag to run it. However, it is fully supported in 14.x and above (i.e., it runs without flags).</li>
<li>Top-level await does not work with classic scripts and non-asynchronous functions.</li>
<li>Circular module dependencies could introduce a deadlock.</li>
</ul><h3 id="prerequisites">Prerequisites</h3>
<ul><li>Node.js version 13.3 or above</li>
<li>npm</li>
<li>API keys for the news APIs (see <a href="#newsAPI">below</a>)</li>
</ul><h3 id="make-the-app">Build the app</h3>
<ol><li>
<p>First, make a new directory named <code>toplevelawait</code>:</p>
<pre><code class="language-bash">$ mkdir toplevelawait</code></pre></li>
<li>
<p>Run <code>npm init</code> to create a <code>package.json</code> file:</p>
<pre><code class="language-bash">$ npm init</code></pre></li>
<li>
<p>Add <code>"type": "module"</code> to <code>package.json</code> to add support for ECMAScript modules:</p>
<pre><code class="language-text">"author": "",
"license": "ISC",
"type": "module",</code></pre></li>
<li>
<p>Make a <code>src</code> folder in the <code>toplevelawait</code> folder. Inside the <code>src</code> folder, make a <code>views</code> directory to save the embedded JavaScript (.ejs) files.</p>
<pre><code class="language-bash">$ mkdir -p src/views</code></pre></li>
<li>
<p>Make two files—<code>app.mjs</code> and <code>exp.mjs</code>—in the <code>src</code> folder. Note the file extension is <code>.mjs</code> and not just <code>.js</code>, which indicates that you are using ECMA modules.</p>
<pre><code class="language-bash">$ touch app.mjs
$ touch exp.mjs
$ ls -1 src
app.mjs
exp.mjs</code>
</pre></li>
<li>
<p>Next, add the dependencies <code>axios</code>, <code>ejs</code>, and <code>express</code>:</p>
<pre><code class="language-text">$ npm install axios ejs express --save</code></pre></li>
<li>
<p>Add the following in the <code>exp.mjs</code> file:</p>
<pre><code class="language-javascript">import express from "express"

export const exp = await express();</code></pre></li>
</ol><p>Note that this uses the <code>await</code> keyword without the <code>async</code> keyword. This wait is waiting for the express instance to initialize before exporting it to other modules. You can use this example to wait for the instances to initialize before moving to code that depends on the awaited resource.</p>
<p>If a module contains a top-level await, then this module's and the parent module's execution will halt until the promise is resolved. All the siblings will continue executing in the usual, synchronous way.</p>
<p>Note that module loading in Node.js is also synchronous, which means it doesn't wait for a resource to load. You will get an error. However, you can achieve asynchronous behavior by placing the <code>await</code> keyword in front of the resource that is loading or doing some processing.</p>
<h2 id="add-the-news-apis"><a id="newsAPI" name="newsAPI"></a>Add the news APIs</h2>
<p>This app uses two freely available news APIs to get data. Using two APIs supports fallback dependency behavior; if one API fails to retrieve the data, the other will get it. Both of these APIs use API keys:</p>
<ul><li><a href="https://newsapi.org/" target="_blank">News API</a></li>
<li><a href="https://gnews.io/" target="_blank">GNews API</a></li>
</ul><p>Insert the following code in the <code>app.mjs</code> file. This first section imports the axios and express instances initialized in <code>exp.js</code>. The next part sets the view engine to view .ejs files in a browser:</p>
<pre><code class="language-javascript">import { exp } from "./exp.mjs";
import axios from "axios"

exp.set("view engine","ejs");
// dependency fall back
let response = "";
let site = true;
try{
   response = await axios('https://newsapi.org/v2/top-headlines?country=us&amp;apiKey=your-api-key');  
 }
 catch{
  response = await axios("https://gnews.io/api/v3/top-news?token=your-api-key"); 
  site = false; 
 }
 // Get top news
exp.get('/',function(req,res){
  let response0 = response.data.articles  
  res.render('main.ejs',{response0: response0, site:site})
 })
 // search news
exp.get('/search', function(req,res){
  res.render("searchnews.ejs")   
})
exp.get('/result', async(req, res)=&gt;{
  let x = req.query.newtitlesearch;
  let response1 = {}
  let data = {}
  try{
    let url = 'https://newsapi.org/v2/everything?q='+x+'&amp;apiKey=your-api-key'
    response1 =  await axios(url);
  }
  catch{
    let url = 'https://gnews.io/api/v3/search?q='+x+'&amp;token=your-api-key'
    response1 =  await axios(url)
  }
  res.render('result.ejs', {response1: response1.data.articles, site: site})  
})
exp.listen(3000)</code></pre><p>The most important part is next: the <strong>try</strong> and <strong>catch</strong> block, which uses top-level await to wait for axios to get the data from the API and, if for any reason, axios fails to retrieve the data from the first API, the app uses the second API to get the data. Once it gets the data from the API, express renders it on the main page:</p>
<pre><code class="language-javascript">try{
   response = await axios('https://newsapi.org/v2/top-headlines?country=us&amp;apiKey=your-api-key');
  
 }
 catch{
  response = await axios("https://gnews.io/api/v3/top-news?token=your-api-key");
  
 }</code></pre><p>After that is another express route that takes users to a search form where they can search for news they are interested in:</p>
<pre><code class="language-javascript">// search news
exp.get('/search', function(req,res){
  res.render("../src/view/searchnews.ejs")  
})</code></pre><p>Finally, another express route displays results from the search:</p>
<pre><code class="language-javascript">exp.get('/result', async(req,res)=&gt;{
  let x = req.query.newtitlesearch;
  let response1 = {}
  let data = {}
  try{
    let url = 'https://newsapi.org/v2/everything?q='+x+'&amp;apiKey=your-api-key'
    response1 =  await axios(url);
  }
  catch{
    let url = 'https://gnews.io/api/v3/search?q='+x+'&amp;token=your-api-key'
    response1 =  await axios(url)
  }
  res.render('../src/view/result.ejs', {response1: response1.data.articles , site: site})  
})</code></pre><h2>Write the frontend pages</h2>
<p>The last part of the app writes the four .ejs HTML files for the frontend pages. Save these files in the <code>views</code> folder:</p>
<pre><code class="language-javascript">//header.ejs
&lt;!DOCTYPE html&gt;
&lt;head&gt;
    &lt;title&gt;newapiapp&lt;/title&gt;
    &lt;link type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" &gt;
&lt;/head&gt;

&lt;body&gt;  
    &lt;nav class="navbar navbar-default"&gt;
              &lt;div class="container-fluid"&gt;
                  &lt;div class="navbar-header"&gt;
                      &lt;a class="navbar-brand" href="#"&gt;News app&lt;/a&gt;
                  &lt;/div&gt; 
                  &lt;div class="collapse navbar-collapse"&gt;
                      &lt;ul class="nav navbar-nav navbar-right"&gt;
                             &lt;li&gt;&lt;a href="https://opensource.com/"&gt;Main&lt;/a&gt;&lt;/li&gt;                
                              &lt;li&gt;&lt;a href="https://opensource.com/search"&gt;Search&lt;/a&gt;&lt;/li&gt;      
                      &lt;/ul&gt;      
                  &lt;/div&gt;
              &lt;/div&gt;  
    &lt;/nav&gt;</code></pre><pre><code class="language-javascript">//main.ejs
&lt;%- include('header');%&gt;
&lt;%let rows = response0%&gt;
&lt;%let siterep = site%&gt;

&lt;div name "container"&gt;
  &lt;div class="row text-center" style="display:flex; flex-wrap:wrap"&gt;
    &lt;% for(let i = 0; i &lt; rows.length; i++){%&gt;
      &lt;div class="col-md-3 col-sm-6 "&gt;
                          &lt;div class="thumbnail" &gt;
                            &lt;a href="https://opensource.com/%3C%25-rows%5Bi%5D.url%20%25%3E"&gt;
                              &lt;img src = "&lt;%= siterep ? rows[i].urlToImage :  rows[i].url  %&gt;"&gt;
                            &lt;/a&gt;                            
                          &lt;/div&gt;
                          &lt;div&gt;&lt;%= rows[i].title%&gt;&lt;/div&gt;                 
                        &lt;/div&gt;    
    &lt;% } %&gt;
  &lt;/div&gt;  
&lt;/div&gt;</code></pre><pre><code class="language-javascript">//Searchnews.ejs 
&lt;%- include('header');%&gt;

  &lt;h1&gt;Search news &lt;/h1&gt;
  &lt;form action="https://opensource.com/result" method="Get"&gt;
      &lt;iput type ="text" placeholder="news title search" name="newtitlesearch"&gt;&lt;/input&gt;
        &lt;input type="submit" placeholder="submit"&gt;&lt;/input&gt;        
   &lt;/form&gt;</code></pre><pre><code class="language-javascript">//result.ejs
&lt;%- include('header');%&gt;
&lt;%let rows = response1%&gt;
&lt;%let siterep = site%&gt;

&lt;div name "container"&gt;
  &lt;div class="row text-center" style="display:flex; flex-wrap:wrap"&gt;

    &lt;% for(let i = 0; i &lt; rows.length; i++){%&gt;
      &lt;div class="col-md-3 col-sm-6 "&gt;
                          &lt;div class="thumbnail" &gt;
                            &lt;a href="https://opensource.com/%3C%25-rows%5Bi%5D.url%20%25%3E"&gt;
                              &lt;img src = "&lt;%= siterep ? rows[i].urlToImage :  rows[i].url  %&gt;"&gt;
                            &lt;/a&gt;                            
                          &lt;/div&gt;
                          &lt;div&gt;&lt;%= rows[i].title%&gt;&lt;/div&gt;                  
                        &lt;/div&gt;    
    &lt;% } %&gt;
  &lt;/div&gt;  
&lt;/div&gt;</code></pre><h2 id="run-the-app">Run the app</h2>
<p>Now the app is complete, and you can try it out.</p>
<p>If you are using a Node.js version from v13.3 to v14.0, run the program with:</p>
<pre><code class="language-bash">$ node --harmony-top-level-await app.js</code></pre><p>If you are using Node.js v14 and up, you don't need to use V8's <code>--harmony</code> flag:</p>
<pre><code class="language-text">$ node app.js</code></pre><p>If you have successfully built this app, congratulations! You have learned a new JavaScript feature.</p>
<p>You can find more uses of top-level await in the ECMAScript <a href="https://github.com/tc39/proposal-top-level-await" target="_blank">TC39 top-level await proposal</a>.</p>
<hr /><p><em>The code for this top-level await example using Node.js by Sumaira Ahmad is open source under a <a href="http://creativecommons.org/licenses/by/4.0/" target="_blank">CC BY 4.0</a> license.</em></p>
<p><gdiv></gdiv></p>
