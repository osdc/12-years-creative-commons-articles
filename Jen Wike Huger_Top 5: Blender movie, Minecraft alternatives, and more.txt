<p>In this week's Top 5, our best articles of the week are about a new Blender short film, alternatives to Minecraft, back to school with open source, impostor syndrome, and teaching student sys admins.</p>
<p><iframe width="520" height="315" src="https://www.youtube.com/embed/AgilUlRkHzA" frameborder="0" allowfullscreen=""></iframe></p>
<h2>Top 5 articles of the week</h2>
<p class="show-read-more"><strong>5. <a href="https://opensource.com/education/15/9/system-administration-homework" target="_blank">How to teach student sys admins</a></strong></p>
<p>Student sys admins need hands-on, practical experience with a wide variety of software. We look at how one professor trains future system administrators.</p>
<p class="show-read-more"><strong>4. <a href="https://opensource.com/business/15/9/tips-avoiding-impostor-syndrome" target="_blank">Tips for avoiding impostor syndrome</a></strong></p>
<p>At this year's Texas Linux Fest, Rackspace's Major Hayden gave a talk on dealing with impostor syndrome.</p>
<p class="show-read-more"><strong>3. <a href="https://opensource.com/education/15/8/5-back-school-open-source-programs-2015" target="_blank">Back to school: 5 open source programs for students and teachers</a></strong></p>
<p>Charlie Reisinger takes a look at a few affordable and stress-free open source software tools to help students and teachers make learning fun and stress-free.</p>
<p class="show-read-more"><strong>2. <a href="https://opensource.com/life/15/9/open-source-alternatives-minecraft" target="_blank">10 open source alternatives to Minecraft</a></strong></p>
<p>Explore the top ten free Minecraft alternatives, all released under open source licenses, reviewed and brought together into one handy collection.</p>
<p class="show-read-more"><strong>1. <a href="https://opensource.com/life/15/9/how-open-film-project-cosmos-laundromat-made-blender-better" target="_blank">How open film project Cosmos Laundromat made Blender better</a></strong></p>
<p>Cosmos Laundromat (or Project Gooseberry for those of us who have been following its production from the start) isn't just a 10-minute short film. It's also the Blender Institute's most ambitious project to date, serving as a pilot for the first fully free and open animated feature film.</p>
