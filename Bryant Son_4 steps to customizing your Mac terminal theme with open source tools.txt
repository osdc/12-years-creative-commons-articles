<p>Do you ever get bored with seeing the same old terminal window on your macOS computer? If so, add some bells and whistles to your view with the open source Oh My Zsh framework and Powerlevel10k theme.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Great Content</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=7016000000127cYAAQ">Free online course: RHEL technical overview</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=7016000000127cYAAQ">Learn Advanced Linux Commands</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Download Cheat Sheets</a></li>
<li><a href="https://opensource.com/alternatives?intcmp=7016000000127cYAAQ">Find an Open Source Alternative</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=7016000000127cYAAQ">Read Top Linux Content</a></li>
<li><a href="https://opensource.com/resources?intcmp=7016000000127cYAAQ">Check out open source resources</a></li>
</ul></div>
</div>
</div>
</div>
<p>This basic step-by-step walkthrough (including a video tutorial at the end) will get you started customizing your macOS terminal. If you're a Linux user, check out Seth Kenlon's guide to <a href="https://opensource.com/article/19/9/adding-plugins-zsh">Adding themes and plugins to Zsh</a> for in-depth guidance.</p>
<h2 id="step-1-install-oh-my-zsh">Step 1: Install Oh My Zsh</h2>
<p><a href="https://ohmyz.sh/" target="_blank">Oh My Zsh</a> is an open source, community-driven framework for managing your Z shell (Zsh) configuration.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Oh My Zsh"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/1_ohmyzsh.jpg" width="675" height="385" alt="Oh My Zsh" title="Oh My Zsh" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Bryant Son, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>Oh My Zsh is released under the MIT License. Install it with:</p>
<pre><code class="language-bash">$ sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"</code></pre><h2 id="step-2-install-powerlevel10k-fonts">Step 2: Install Powerlevel10k fonts</h2>
<p><article class="align-center media media--type-image media--view-mode-full" title="Powerlevel10k"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/2_powerlevel10k.jpg" width="675" height="379" alt="Powerlevel10k" title="Powerlevel10k" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Bryant Son, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>Powerlevel10k is an MIT-Licensed Zsh theme. Before installing Powerlevel10k, you will want to install custom fonts for your terminal.</p>
<p>Go to the <a href="https://github.com/romkatv/powerlevel10k" target="_blank">Powerlevel10 GitHub</a> page, and search for "fonts" in the README. The steps for installing the custom fonts will vary depending on your operating system; the video at the bottom of this page explains how to do it on macOS. It should be just a simple click–download–install series of operations.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Powerlevel10k fonts"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/3_downloadfonts.jpg" width="675" height="402" alt="Powerlevel10k fonts" title="Powerlevel10k fonts" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Bryant Son, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<h2 id="step-3-install-the-powerlevel10k-theme">Step 3: Install the Powerlevel10k theme</h2>
<p>Next, install Powerlevel10k by running:</p>
<pre><code class="language-bash">git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k</code></pre><p>After you finish, open a <code>~/.zshrc</code> configuration file with a text editor, such as <a href="https://opensource.com/resources/what-vim">Vim</a>, set the line <code>ZSH_THEME="powerlevel10k/powerlevel10k</code>, then save the file.</p>
<h2 id="step-4-finalize-your-powerlevel10k-setup">Step 4: Finalize your Powerlevel10k setup</h2>
<p>Open a new terminal, and you should see the Powerlevel10k configuration wizard. If not, run <code>p10k configure</code> to bring up the configuration wizard. If you installed the custom fonts in Step 2, the icons and symbols should display correctly. Change the default font to <strong>MeslowLG NF</strong> (see the video below for instructions).</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Powerlevel10k configuration"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/4_p10kconfiguration.jpg" width="675" height="338" alt="Powerlevel10k configuration" title="Powerlevel10k configuration" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Bryant Son, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>Once you complete the configuration, you should see a beautiful terminal.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Oh My Zsh/Powerlevel10k theme"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/5_finalresult.jpg" width="675" height="142" alt="Oh My Zsh/Powerlevel10k theme" title="Oh My Zsh/Powerlevel10k theme" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Bryant Son, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>If you want to see an interactive tutorial, please check out this video:</p>
<p class="rtecenter">
<iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/McaAsw_3C0w" title="YouTube video player" width="560"></iframe></p>
<p>That's it! You should be ready to enjoy your beautiful new terminal. Be sure to check out other Opensource.com articles for more tips and articles on using the shell, Linux administration, and more.</p>
