<p>Knowing how to read and write files can be useful for various purposes. In Rust, this task is done using the file system module (<a href="https://doc.rust-lang.org/std/fs/" rel="noopener" target="_blank">std::fs</a>) in the standard library. In this article, I'll give you an overview on how to use this module.</p>
<p>To demonstrate this task, I prepared example code which is also available on <a href="https://github.com/hANSIc99/rust_file_io" rel="noopener" target="_blank">GitHub</a>.</p>
<p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">Programming and development</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Red Hat Developers Blog">Red Hat Developers Blog</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Programming cheat sheets">Programming cheat sheets</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/services/training/learning-subscription?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Try for free: Red Hat Learning Subscription">Try for free: Red Hat Learning Subscription</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/bash-programming-guide?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="eBook: An introduction to programming with Bash">eBook: An introduction to programming with Bash</a></div>
              <div class="field__item"><a href="https://developers.redhat.com/cheat-sheets/bash-shell-cheat-sheet?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Bash shell scripting cheat sheet">Bash shell scripting cheat sheet</a></div>
              <div class="field__item"><a href="https://developers.redhat.com/e-books/modernizing-enterprise-java?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="eBook: Modernizing Enterprise Java">eBook: Modernizing Enterprise Java</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/building-applications?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="An open source developer's guide to building applications">An open source developer's guide to building applications</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Register for your free Red Hat account">Register for your free Red Hat account</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<h2 id="preperation">Preparation</h2>
<p>When using Rust, a function that fails returns the <a href="https://doc.rust-lang.org/std/result/enum.Result.html" rel="noopener" target="_blank">Result</a> type. The file system module in particular returns the specialized type <a href="https://doc.rust-lang.org/std/io/type.Result.html" rel="noopener" target="_blank">std::io::Result&lt;T, Error&gt;</a>. With this knowledge, you can return the same type from the <code>main()</code> function:</p>
<pre>
<code class="language-rust">fn  main() -&gt;  std::io::Result&lt;()&gt; {
/* ...code comes here... */
</code></pre><h2 id="writing">Writing Rust files</h2>
<p>Performing file I/O-operations in Rust is relatively easy. Writing to a file can be simplified to one line:</p>
<pre>
<code class="language-rust">use  std::fs;
fs::write("favorite_websites.txt", b"opensource.com")?;
Ok(())</code></pre><p>Using the error propagation operator <code>(?)</code>, the error information gets passed on to the calling function where the error can subsequently be handled. As <code>main()</code> is the only other function in the call stack, the error information gets passed on to the console output in case the write operation failed.</p>
<p>The syntax of the <a href="https://doc.rust-lang.org/std/fs/fn.write.html" rel="noopener" target="_blank">fs::write</a> function is quite forward. The first argument is the file path, which must be the type <a href="https://doc.rust-lang.org/std/path/struct.Path.html" rel="noopener" target="_blank">std::path::Path</a>. The second argument is the content, which is actually a slice of bytes (<code>[u8]</code>). Rust converts the arguments passed into the correct type. Luckily, these types are basically the only types dealt with in the following examples.</p>
<p>A more concise access of the write operation can be achieved using the file descriptor type <a href="https://doc.rust-lang.org/std/fs/struct.File.html" rel="noopener" target="_blank">std::fs::File</a>:</p>
<pre>
<code class="language-rust">let mut file = fs::File::create("favorite_websites.txt")?;
file.write_all(b"opensource.com\n")?;
Ok(())</code></pre><p>As the file type implements the <a href="https://doc.rust-lang.org/std/io/trait.Write.html" rel="noopener" target="_blank">Write</a> trait, it is possible to use the associated methods to write to the file. However, the <code>create</code> method can overwrite an already existing file.</p>
<p>To get even more control of the file descriptor, the type <a href="https://doc.rust-lang.org/std/fs/struct.OpenOptions.html#" rel="noopener" target="_blank">std::fs::OpenOptions</a> must be used. This provides opening modes similar to the ones in other languages:</p>
<pre>
<code class="language-rust">let mut file = fs::OpenOptions::new()
                            .append(true)
                            .open("favorite_websites.txt")?;
                            
file.write_all(b"sourceforge.net\n")?;</code></pre><h2 id="reading">Reading Rust files</h2>
<p>What applies to writing also applies to reading. Reading can also be done with a simple one-line of code:</p>
<pre>
<code class="language-rust">let websites = fs::read_to_string("favorite_websites.txt")?;</code></pre><p>The above line reads the content of the file and returns a string. In addition to reading a string, there is also the <a href="https://doc.rust-lang.org/std/fs/fn.read.html" rel="noopener" target="_blank">std::fs::read</a> function which reads the data into a vector of bytes if the file contains binary data.</p>
<p>The next example shows how to read the content of the file into memory and subsequently print it line by line to a console:</p>
<pre>
<code class="language-rust">let file = fs::File::open("favorite_websites.txt")?;
let lines = io::BufReader::new(file).lines();

for line in lines {
    if let Ok(_line) = line {
        println!("&gt;&gt;&gt; {}", _line);
    }
}</code></pre><h2 id="summary">Summary</h2>
<p>If you are already familiar with other programming languages, you may have noticed that there is <code>no close-</code>function (or something similar) that releases the file handle. In Rust, the file handle is released as soon as the related variable goes out of scope. To define the closing behavior, a scope <code>({ })</code> around the file representation can be applied. I recommend that you get familiar with <a href="https://doc.rust-lang.org/std/io/trait.Read.html">Read</a> and <a href="https://doc.rust-lang.org/std/io/trait.Write.html" rel="noopener" target="_blank">Write</a> trait as you can find this trait implemented in many other types.</p>
