<p>I was introduced to open source by my husband, a long-term Unix/Linux user. When he encouraged me to attend the Triangle Linux Users Group (<a href="https://trilug.org/" target="_blank">TriLUG</a>), a local meetup in the Raleigh, NC, area, I was quite intimidated, as I was one of a handful of women in attendance. But the more I learned about open source, the more intrigued I became.</p>
<p>As a quality engineer (QE), it was challenging to comprehend what impact I could have in open source. I had to expand my own understanding to realize that contributing to open source does not only mean code contributions and that QE brings a lot of value by providing feedback, testing, reporting, and verifying fixes. Any way someone can contribute to a project makes the project that much better.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Linux resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux commands cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Free online course: RHEL Technical Overview</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux networking cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">SELinux cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux common commands cheat sheet</a></li>
<li><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">What are Linux containers?</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Our latest Linux articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>As a beginner Fedora user, I went from being nervous about breaking it, to trying new features, to realizing that if I can break it, I can fix it, too. Writing a bug report for a project felt like I was part of a huge movement.</p>
<h2 id="becoming-an-open-source-contributor">Becoming an open source contributor</h2>
<p><a href="https://spacewalkproject.github.io/" target="_blank">Spacewalk</a> was the first project that I was part of from its creation. Spacewalk was <a href="https://www.redhat.com/en/technologies/management/satellite" target="_blank">Red Hat Satellite</a>'s upstream community project, and I was one of the first to ensure quality. Later, my focus shifted to the <a href="https://opensource.com/article/20/8/manage-repositories-pulp">Pulp project</a> (Pulp is a platform for managing content repositories), where I initially was the only QE working on the project. As the project took off, user adoption increased, and quality became of utmost importance.</p>
<p>The Pulp team practices the agile methodology for development and depends on continuous integration and delivery to do time-based releases. We integrated QE into the project, and test automation, implementing CI/CD systems, running tests, and reporting issues became the QE team's responsibility. We provide information about the existing test suites, which contain the automated tests, test cases that are to be automated, and documentation on how to contribute to the test suites.</p>
<p>Encouraging users to contribute to the testing efforts ensures that, as the software evolves, it doesn't break the functionalities users rely on. Understanding the open source <a href="https://www.researchgate.net/figure/The-general-onion-model-of-open-source-communities-vs-the-Linux-Kernel-onion-model_fig1_220725001" target="_blank">onion model</a> reinforced the significance of a healthy and sustainable community that includes users, bug reporters, QE, and developers.</p>
<h2 id="migrating-to-management">Migrating to management</h2>
<p>As an open source enthusiast, it was easy for me to transition my management style to the open management philosophy, which fosters <em>transparency</em>, <em>inclusivity</em>, <em>adaptability</em>, and <em>collaboration</em>. As an open manager, one of my primary goals is to engage and empower associates to be their best. It is easy to adopt this philosophy when you understand the <a href="https://opensource.com/open-source-way">open source values</a>.</p>
<ul><li>By being <em>transparent</em>, I help create the context for the team and the "why." This is a building block in creating trust.</li>
<li>Being consciously <em>inclusive</em> is another value that I regard highly. Making sure everyone in the team is included and everyone's voice is heard is extremely important for individual and organizational growth.</li>
<li>In an environment that is constantly evolving and where innovation is key, being nimble and <em>adaptable</em> is of utmost importance. Encouraging associates' growth mindset and continuous learning helps foster these traits.</li>
<li>For effective <em>collaboration</em>, I believe we need an environment where there is trust, open communication, and respect.</li>
</ul><p>By paying attention to these values, an open manager can create an environment that is inclusive, treats others with respect, and encourages everyone to support each other. These open management practices give me the framework and guidance to increase employee engagement.</p>
