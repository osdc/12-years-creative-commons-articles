<p>As part of my role as a principal communication strategist at an enterprise software company with an open source development model, I publish a regular update about open source community, market, and industry trends. Here are some of my and their favorite articles from that update.</p>
<h2 class="title is-size-3 is-size-1-desktop has-text-weight-normal"><a href="https://www.openfaas.com/blog/openfaas-functions-with-github-actions/" target="_blank">Build and deploy OpenFaaS functions: Run serverless functions anywhere you want</a></h2>
<blockquote><p>In this tutorial, I’ll show you how to build and deploy functions anywhere using GitHub Actions and multi-arch images that can run on a cloud instance, or on your Raspberry Pi homelab.</p>
</blockquote>
<p><strong>The impact</strong>: Aside from clearly showing how "serverless" serverless really is, this tutorial makes the magic of "it just runs" a lot more accessible. </p>
<h2 class="docs-title-input-label" style="pointer-events: auto; max-width: 660px;"><a href="https://docs.google.com/spreadsheets/d/191WWNpjJ2za6-nbG4ZoUMXMpUK8KlCIosvQB0f-oq3k/edit#gid=907731238" target="_blank"><span class="docs-title-input-label-inner" id="docs-title-input-label-inner">Kubernetes ingress controllers</span></a></h2>
<blockquote><p>A google spreadsheet that compares the functionality of multiple open source Kubernetes ingress controllers.</p>
</blockquote>
<p><strong>The impact</strong>: This is a for-the-community by-the-community resource leaving the marketing at the door.</p>
<h2><a href="https://superuser.openstack.org/articles/virtual-open-infrastructure-summit-recap/" target="_blank">Virtual Open Infrastructure Summit recap</a></h2>
<blockquote><div class="col col--2-of-3">
<div class="article-rider">
<p>Here’s a snapshot covering five days, over 100 sessions, two software releases, and a Foundation announcement backed by over 60 organizations.</p>
</div>
</div>
</blockquote>
<p><strong>The impact</strong>: As always, it is the use cases from the community that really show what is possible when a dedicated community works together to solve complex problems. CERN is an old OpenStack stalwart; Workday was a bit of a surprise for me (and makes using the tool a little bit easier to stomach now). </p>
<p><em>I hope you enjoyed this list and come back next week for more open source community, market, and industry trends.</em></p>
