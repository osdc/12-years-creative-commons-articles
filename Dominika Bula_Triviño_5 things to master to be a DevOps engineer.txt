<p>There's an increasing global demand for DevOps professionals, IT pros who are skilled in software development and operations. In fact, the Linux Foundation's <a href="https://www.linuxfoundation.org/publications/2018/06/open-source-jobs-report-2018/" target="_blank">Open Source Jobs Report</a> ranked DevOps as the most in-demand skill, and DevOps career opportunities are thriving worldwide.</p>
<p>The main focus of DevOps is bridging the gap between development and operations teams by reducing painful handoffs and increasing collaboration. This is not accomplished by making developers work on operations tasks nor by making system administrators work on development tasks. Instead, both of these roles are replaced by a single role, DevOps, that works on tasks within a cooperative team. As Dave Zwieback wrote in <a href="https://www.oreilly.com/library/view/devops-hiring/9781491908570/" target="_blank"><em>DevOps Hiring</em></a>, "organizations that have embraced DevOps need people who would naturally resist organization silos."</p>
<p>If you want to know more about DevOps, here are a few articles to prime your interests:</p>
<ul><li><a href="https://opensource.com/resources/devops">What is DevOps?</a></li>
<li><a href="https://opensource.com/article/18/11/how-non-engineer-got-devops">DevOps is for everyone</a></li>
<li><a href="https://opensource.com/article/18/1/getting-devops">How to get into DevOps</a></li>
</ul><h2 id="foundations-of-devops-engineering-1">5 foundations of DevOps engineering</h2>
<p>Working in a DevOps role requires more than the traditional skillsets you'd see in system administrators or developers. In <a href="https://books.google.com/books/about/The_Phoenix_Project.html?id=H6x-DwAAQBAJ&amp;printsec=frontcover&amp;source=kp_read_button&amp;redir_esc=y#v=onepage&amp;q&amp;f=false" target="_blank"><em>The Phoenix Project: A Novel About IT, DevOps, and Helping Your Business Win</em></a>, Gene Kim, Kevin Behr, and George Spafford introduce the Three Ways at the core of DevOps practice:</p>
<ol><li>
<p><strong>Lean flow</strong>, with emphasis on the entire system's performance, to accelerate the delivery of work from development to operations to customers</p>
</li>
<li>
<p><strong>Feedback loops</strong> to create safer work systems</p>
</li>
<li>
<p><strong>Continual learning and experimentation</strong> to foster a high-trust culture and a scientific approach to organizational improvement during daily activities</p>
</li>
</ol><p>From these come the five foundations of DevOps engineering, which you need to master to be successful in DevOps.</p>
<h3 id="lean-and-agile">1. Lean and agile</h3>
<div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More DevOps resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/resources/devops?intcmp=7013a00000263HlAAI">What is DevOps?</a></li>
<li><a href="https://opensource.com/downloads/devops-hiring-guide?intcmp=7013a00000263HlAAI">The ultimate DevOps hiring guide</a></li>
<li><a href="https://opensource.com/downloads/devops-monitoring-guide?intcmp=7013a00000263HlAAI">DevOps monitoring tools guide</a></li>
<li><a href="https://opensource.com/downloads/devsecops?intcmp=7013a00000263HlAAI">Getting started with DevSecOps</a></li>
<li><a href="https://enterprisersproject.com/cheat-sheet-devops-glossary?intcmp=7013a00000263HlAAI">Download the DevOps glossary</a></li>
<li><a href="https://www.ansible.com/resources/ebooks/ansible-for-devops?intcmp=7013a00000263HlAAI">eBook: Ansible for DevOps</a></li>
<li><a href="https://opensource.com/tags/devops?src=devops_resource_menu2">Latest DevOps articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>A DevOps engineer understands that the operational aspects of software development should be given the same priority as every other project-related task or feature. The best way to succeed in this environment is to foster a strong culture of collaboration between the development and operations teams. Learning more about lean, systems thinking, and agile development can be helpful with this collaboration. The concepts of the DevOps team and the System team are an integral part of the <a href="https://en.wikipedia.org/wiki/Scaled_agile_framework" target="_blank">Scaled Agile Framework (SAFe)</a>. Giving joint responsibilities to the System and DevOps teams also helps to build high-performance teams.</p>
<h3 id="continuous-integration-and-continuous-delivery">2. Continuous integration and continuous delivery</h3>
<p><a href="https://opensource.com/article/18/8/what-cicd">CI/CD</a> software engineering practices are a fundamental tool to enable agility. They enable software changes to reach production more frequently, which considerably increases the opportunities for customers to experience and provide feedback on incremental changes. This feedback is vital for fast-paced agile software delivery.</p>
<p><strong>Continuous integration:</strong> CI refers to the "<a href="https://fedoraproject.org/wiki/CI/Manifesto" target="_blank">build and test</a>" process workflow with the goal of ensuring that broken changes don't affect other developers or users. This is achieved by assembling a software change into a production-like system, then test-driving it like a user/customer (integration), and (continuously) repeating this process for every single change. CI enables teams to rapidly build software toward a moving target. Depending on the project, there may be several CI efforts. You might end up with different frameworks, implementations, or tests. It's good practice to come up with a basic set of rules for your team.</p>
<p><strong>Continuous delivery:</strong> CD defines the release process and the packaging and deployment workflow, where the key aspect is to prevent any broken changes from being delivered or released.</p>
<p>The best case is when every successful integration can reach production with minimum human intervention, in which case the full lifecycle is automated:</p>
<ul><li>Builds and infrastructure should be deterministic and repeatable</li>
<li>Release steps should be automated</li>
<li>Changes should be tested in their own environment</li>
</ul><p>These versions are usually called release candidates; at this point, the release has already been packaged, tested, and deployed in a test environment and only requires human intervention to decide if a particular release candidate will be pushed to final production.</p>
<p>Combining CI/CD practices produces transformational results because software changes reach production more frequently.</p>
<h3 id="devsecops">3. DevSecOps</h3>
<p>In the past, security was isolated to a specific team in a final stage of the development process. The cost to fix a vulnerability in post-production is far more expensive than fixing it during the design, requirement identification, and architecture stages.</p>
<p><a href="https://opensource.com/downloads/devsecops">DevSecOps</a> means that security must be integrated into the full cycle. Security must be present in the application and infrastructure from the very beginning; this can be achieved by <a href="https://www.devseccon.com/wp-content/uploads/2017/07/DevSecOps-whitepaper.pdf" target="_blank">automating security testing in a secure CD pipeline</a>.</p>
<p><strong>Automated security testing</strong> is a key to DevSecOps, since running manual security checks in the pipeline can be time-consuming. DevSecOps ensures the principle of "security by design" by automating security testing in code review, automated application testing, educating, and usage of secure design patterns.</p>
<p>The <strong>secure delivery pipeline</strong>, on the other hand, integrates six important components into the DevOps approach:</p>
<ul><li>Code analysis</li>
<li>Change management</li>
<li>Compliance monitoring</li>
<li>Thread investigation</li>
<li>Vulnerability assessment</li>
<li>Security training</li>
</ul><p>This will produce significant benefits if done properly; chiefly agility for security teams, early identification of vulnerabilities in the source code, cost reduction, and recovery speed.</p>
<p>For more information, download <a href="https://opensource.com/downloads/devsecops">The open source guide to DevOps security</a>.</p>
<h3 id="infrastructure-as-code">4. Infrastructure as code</h3>
<p>In a nutshell, <a href="https://docs.huihoo.com/redhat/summit/2017/C144009-Enable-Agility-With-Infrastructure-as-Code.pdf" target="_blank">infrastructure as code (IaC) makes DevOps possible</a>. It is an essential part of DevOps practices and plays an important role in conjunction with CD. One of its main benefits is the ability to reproduce an environment automatically based on a particular version of the source code, eliminating infrastructure administration and maintenance usually carried out by manual processes, which are potentially prone to errors and difficult to track.</p>
<p>The latter is the main reason IaC is often wrapped into the topic of automation (involving the automation of manual processes), but it goes beyond that. Experts often cite the following as best practices in IaC:</p>
<ul><li>The infrastructure is managed using the same versioning control as DevOps engineers use for source code, which helps considerably to keep track of changes. Hence, the IaC model generates the same infrastructure environment every time it's deployed, similar to how the same source code of an application generates the same binary.</li>
<li>All infrastructure changes can be tested in the form of unit, functional, and integration testing.</li>
<li>The source code describes the infrastructure state, so it avoids creating unnecessary documentation. This, in particular, enables collaboration between devs and ops.</li>
</ul><h3 id="monitoring-and-observability">5. Monitoring and observability</h3>
<p>Monitoring answers questions like "is the health of systems in good shape?" while observability digs deeper into the system's behavior for debugging purposes, especially when something doesn't work. <a href="https://opensource.com/article/18/8/now-available-open-source-guide-devops-monitoring-tools">The combination also plays an important role in DevOps</a>, making it easier to detect and resolve problems and maintain continuous improvement. It is the evolution of logging and metrics, where monitoring aims to collect logs and observability allows the information to be collected.</p>
<p>The special nature of DevOps practices, with continuous integration and delivery of new versions of infrastructure and application code, makes monitoring and observability a fundamental aspect of making systems more reliable, easier to manage, and troubleshoot.</p>
<h2 id="a-cultural-change">DevOps is a culture</h2>
<p>While there are many DevOps tools on the market and they can help you streamline the process of development and deployment, the baseline to becoming good DevOps engineer is to focus on the foundations. DevOps requires a <a href="https://opensource.com/article/18/8/visualizing-devops-essentials-mindset">change of mindset</a> from older practices; this can be tricky as DevOps isn't a specific framework or workflow—DevOps is a culture.</p>
<p>Or, as Jennifer Davis and Ryn Daniels said in <a href="http://shop.oreilly.com/product/0636920039846.do" target="_blank"><em>Effective DevOps: Building a Culture of Collaboration, Affinity, and Tooling at Scale</em></a>, "DevOps is a prescription for culture… DevOps is about finding ways to adopt and innovate social structure, culture, and technology together in order to work more effectively."</p>
<p>This change of mindset drives the digital revolution. Industry leaders combine agile and lean methodologies in DevOps to shorten cycle times to better address market needs for deploying software more quickly and efficiently.</p>
