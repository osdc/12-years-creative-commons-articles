<p dir="ltr">The problem: As mobile devices continue to proliferate, the Internet of Things keeps growing immensely, and more users and new data are pushed across telecom networks every day, network operators must invest in expanded facilities. The revenue from mobile applications is tied to number of devices/consumers not amount of data consumed. As time goes on, average revenue per user will remain flat or even decrease as  data demand will increase significantly over time.</p>
<!--break-->
<p dir="ltr">And these demands are elastic, they fluctuate. For example, during a sports event, or on election night, video streaming demand spikes for a few hours, then quickly dissipates.</p>
<h2 dir="ltr">Why can’t current network provider solutions satisfy these needs?</h2>
<p dir="ltr">Historically, proprietary hardware was used because it offered performance and capabilities that commodity hardware could not provide. Telecommunication service providers bought, and were locked into,expensive custom hardware and chipsets to run their networks.  Service providers would attempt to predict peak demand, and then purchase and provision sufficient infrastructure to handle this peak traffic load, which may only happen infrequently. Provisioning of new services involves the purchase and deployment of hardware, with a lead time of months before it was ready for production deployment. These hardware appliances, with short lifecyles, would quickly reach end of life, requiring the purchase and install cycle to be repeated with perhaps limited revenue benefit.</p>
<h2 dir="ltr">The NFV solution</h2>
<p dir="ltr">Today, Network Functions Virtualization (NFV) takes software applications that run on proprietary hardware and allows them to run on standard x86 servers. This allows  core network infrastructure to dynamically allocate network, compute and storage to satisfy workloads on-demand. It also allows you to  move these workloads to different servers, or even different data centers as needed, and to scale up or scale down without changing the underlying hardware. NFV provides you with a modern agile environment to respond to customers needs, get to market quickly with new services, and reduce both capital and operational expenditures.</p>
<table><tbody><tr><td>
<p><strong>The old approach</strong></p>
<ul><li>Expensive proprietary hardware</li>
<li>Physical installation required</li>
<li>Less competition means less innovation</li>
</ul></td>
<td>
<p><strong><span style="line-height: 1.5em; background-color: transparent;">The NFV approach</span></strong></p>
<ul><li><span style="line-height: 1.5em; background-color: transparent;">Indepedendent software vendors</span></li>
<li><span style="line-height: 1.5em; background-color: transparent;">Easily orchestrated install process</span></li>
<li><span style="line-height: 1.5em; background-color: transparent;">Commodity servers and hardware</span></li>
</ul></td>
</tr></tbody></table><h2 dir="ltr">The Benefits of NFV</h2>
<p>Network functions virtualization offers many benefits to providers of telecom services, including:</p>
<ul><li>Decoupling the software from the hardware allowing network functions to run as virtual machines on standard x86 hardware.</li>
<li>Exploiting economies of scale by consolidating many applications and networking appliances onto industry-standard high-volume servers, switches, routers, and storage.</li>
<li>Transforming network functions into elastic, pooled resources that can scale up or down as needed.</li>
<li>Speeding time to market by minimizing the cycle of innovation.</li>
<li>Targeting service introduction based on geography or customer sets.</li>
</ul><h2>Why develop NFV with open source?</h2>
<p dir="ltr">Without community collaboration, individual vendors independently implement NFV solutions, thus the goal of industry interoperability is difficult to achieve. This fragmentation dilutes resources and leads to divergence, from which users don’t benefit.</p>
<p dir="ltr">Open source communities and NFV can support each other in many ways.</p>
<ul><li><span style="font-size: 13.0080003738403px; line-height: 1.538em; background-color: transparent;">Open source projects ease support of NFV features, and solve major technical implementation challenges including obtaining high performance using industry standard servers.</span></li>
<li><span style="font-size: 13.0080003738403px; line-height: 1.538em; background-color: transparent;">Open source communities can create the software code or hardware necessary to implement NFV solutions based on common industry requirements.</span></li>
<li><span style="font-size: 13.0080003738403px; line-height: 1.538em; background-color: transparent;">An open source community shares the burden of integration, testing and validation of Network Function Virtualization solutions, which results in increased industry-wide interoperability, reliability and efficiency that can shorten time to market.</span></li>
<li><span style="font-size: 13.0080003738403px; line-height: 1.538em; background-color: transparent;">The community provides a common foundation for deployment of Virtual Network Functions. Thus, the customer risk in identifying, testing and deploying new functions that interoperate with each other is mitigated.</span></li>
<li>Openness enables a wide variety of ecosystems and encourages openness, which is open to software companies and academic institutions.</li>
</ul><p>The <a href="https://www.opnfv.org/" target="_blank" style="font-size: 13.0080003738403px; line-height: 1.538em; background-color: transparent;">Open Platform for NFV</a> (OPNFV) is a new open source project that will enable industry collaboration to advance NFV and ensure a consistent interface among virtual infrastructure platforms for the deployment of network functions. The OPNFV project provides an open reference platform for the deployment of Virtual Network Functions (VNF). OPNFV can benefit its members and community by creating a collaborative development of an open source platform to promote interoperable NFV solutions, and stimulating existing open source communities to create software code or hardware for NFV solutions based on common industry requirements.</p>
<p dir="ltr">The telecommunication industry is ripe for agility, modernization and standardization, all of which are best achieved using open source software.</p>
