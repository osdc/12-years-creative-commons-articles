<p>Gaming has traditionally been one of Linux's weak points. That has changed somewhat in recent years thanks to Steam, GOG, and other efforts to bring commercial games to multiple operating systems, but those games are often not open source. Sure, the games can be played on an open source operating system, but that is not good enough for an open source purist.</p>
<p>So, can someone who only uses free and open source software find games that are polished enough to present a solid gaming experience without compromising their open source ideals? Absolutely. While open source games are unlikely ever to rival some of the AAA commercial games developed with massive budgets, there are plenty of open source games, in many genres, that are fun to play and can be installed from the repositories of most major Linux distributions. Even if a particular game is not packaged for a particular distribution, it is usually easy to download the game from the project's website to install and play it.</p>
<p>This article looks at strategy and simulation games. I have already written about <a href="https://opensource.com/article/18/1/arcade-games-linux" target="_blank">arcade-style games</a>, <a href="https://opensource.com/article/18/3/card-board-games-linux" target="_blank">board &amp; card games</a>, <a href="https://opensource.com/article/18/6/puzzle-games-linux" target="_blank">puzzle games</a>, <a href="https://opensource.com/article/18/7/racing-flying-games-linux" target="_blank">racing &amp; flying games</a>, and <a href="https://opensource.com/article/18/8/role-playing-games-linux" target="_blank">role-playing games</a>.</p>
<h2>Freeciv</h2>
<p><article class="align-center media media--type-image media--view-mode-full" title="Freeciv screenshot.png"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/freeciv.png" width="1366" height="768" alt="Freeciv screenshot.png" title="Freeciv screenshot.png" /></div>
      
  </article></p>
<p><a href="http://www.freeciv.org/" target="_blank">Freeciv</a> is an open source version of the <a href="https://en.wikipedia.org/wiki/Civilization_(series)" target="_blank">Civilization series</a> of computer games. Gameplay is most similar to the earlier games in the Civilization series, and Freeciv even has options to use Civilization 1 and Civilization 2 rule sets. Freeciv involves building cities, exploring the world map, developing technologies, and competing with other civilizations trying to do the same. Victory conditions include defeating all the other civilizations, developing a space colony, or hitting deadline if neither of the first two conditions are met. The game can be played against AI opponents or other human players. Different tile-sets are available to change the look of the game's map.</p>
<p>To install Freeciv, run the following command:</p>
<ul><li>On Fedora: <code>dnf install freeciv</code></li>
<li>On Debian/Ubuntu: <code>apt install freeciv</code></li>
</ul><h2>MegaGlest</h2>
<p><article class="align-center media media--type-image media--view-mode-full" title="megaglest screenshot.png"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/megaglest.png" width="1366" height="768" alt="megaglest screenshot.png" title="megaglest screenshot.png" /></div>
      
  </article></p>
<p><a href="https://megaglest.org/" target="_blank">MegaGlest</a> is an open source real-time strategy game in the style of Blizzard Entertainment's <a href="https://en.wikipedia.org/wiki/Warcraft" target="_blank">Warcraft</a> and <a href="https://en.wikipedia.org/wiki/StarCraft" target="_blank">StarCraft</a> games. Players control one of several different factions, building structures and recruiting units to explore the map and battle their opponents. At the beginning of the match, a player can build only the most basic buildings and recruit the weakest units. To build and recruit better things, players must work their way up their factions technology tree by building structures and recruiting units that unlock more advanced options. Combat units will attack when enemy units come into range, but for optimal strategy, it is best to manage the battle directly by controlling the units. Simultaneously managing the construction of new structures, recruiting new units, and managing battles can be a challenge, but that is the point of a real-time strategy game. MegaGlest provides a nice variety of factions, so there are plenty of reasons to try new and different strategies.</p>
<p>To install MegaGlest, run the following command:</p>
<ul><li>On Fedora: <code>dnf install megaglest</code></li>
<li>On Debian/Ubuntu: <code>apt install megaglest</code></li>
</ul><h2>OpenTTD</h2>
<p><article class="align-center media media--type-image media--view-mode-full" title="openttd screenshot.png"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/openttd.png" width="1366" height="768" alt="openttd screenshot.png" title="openttd screenshot.png" /></div>
      
  </article></p>
<p><a href="https://www.openttd.org/" target="_blank">OpenTTD</a> (see also <a href="https://opensource.com/life/15/7/linux-game-review-openttd" target="_blank">our review</a>) is an open source implementation of <a href="https://en.wikipedia.org/wiki/Transport_Tycoon#Transport_Tycoon_Deluxe" target="_blank">Transport Tycoon Deluxe</a>. The object of the game is to create a transportation network and earn money, which allows the player to build an even bigger transportation network. The network can include boats, buses, trains, trucks, and planes. By default, gameplay takes place between 1950 and 2050, with players aiming to get the highest performance rating possible before time runs out. The performance rating is based on things like the amount of cargo delivered, the number of vehicles they have, and how much money they earned.</p>
<p>To install OpenTTD, run the following command:</p>
<ul><li>On Fedora: <code>dnf install openttd</code></li>
<li>On Debian/Ubuntu: <code>apt install openttd</code></li>
</ul><h2>The Battle for Wesnoth</h2>
<p><article class="align-center media media--type-image media--view-mode-full" title="the_battle_for_wesnoth screenshot.png"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/the_battle_for_wesnoth.png" width="1366" height="768" alt="the_battle_for_wesnoth screenshot.png" title="the_battle_for_wesnoth screenshot.png" /></div>
      
  </article></p>
<p><a href="https://www.wesnoth.org/" target="_blank">The Battle for Wesnoth</a> is one of the most polished open source games available. This turn-based strategy game has a fantasy setting. Play takes place on a hexagonal grid, where individual units battle each other for control. Each type of unit has unique strengths and weaknesses, which requires players to plan their attacks accordingly. There are many different campaigns available for The Battle for Wesnoth, each with different objectives and storylines. The Battle for Wesnoth also comes with a map editor for players interested in creating their own maps or campaigns.</p>
<p>To install The Battle for Wesnoth, run the following command:</p>
<ul><li>On Fedora: <code>dnf install wesnoth</code></li>
<li>On Debian/Ubuntu: <code>apt install wesnoth</code></li>
</ul><h2>UFO: Alien Invasion</h2>
<p><article class="align-center media media--type-image media--view-mode-full" title="ufo_alien_invasion screenshot.png"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/ufo_alien_invasion.png" width="1024" height="768" alt="ufo_alien_invasion screenshot.png" title="ufo_alien_invasion screenshot.png" /></div>
      
  </article></p>
<p><a href="https://ufoai.org/" target="_blank"><br /><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Great Content</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=7016000000127cYAAQ">Free online course: RHEL technical overview</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=7016000000127cYAAQ">Learn Advanced Linux Commands</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Download Cheat Sheets</a></li>
<li><a href="https://opensource.com/alternatives?intcmp=7016000000127cYAAQ">Find an Open Source Alternative</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=7016000000127cYAAQ">Read Top Linux Content</a></li>
<li><a href="https://opensource.com/resources?intcmp=7016000000127cYAAQ">Check out open source resources</a></li>
</ul></div>
</div>
</div>
</div>
<p>UFO: Alien Invasion</p></a> is an open source tactical strategy game inspired by the <a href="https://en.wikipedia.org/wiki/X-COM" target="_blank">X-COM series</a>. There are two distinct gameplay modes: geoscape and tactical. In geoscape mode, the player takes control of the big picture and deals with managing their bases, researching new technologies, and controlling overall strategy. In tactical mode, the player controls a squad of soldiers and directly confronts the alien invaders in a turn-based battle. Both modes provide different gameplay styles, but both require complex strategy and tactics.</p>
<p>To install UFO: Alien Invasion, run the following command:</p>
<ul><li>On Debian/Ubuntu: <code>apt install ufoai</code></li>
</ul><p>Unfortunately, UFO: Alien Invasion is not packaged for Fedora.</p>
<p>Did I miss one of your favorite open source strategy or simulation games? Share it in the comments below.</p>
