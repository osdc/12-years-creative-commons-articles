<p>Distributed databases are common for many reasons. They increase reliability, redundancy, and performance. Apache ShardingSphere is an open source framework that enables you to transform any database into a distributed database. Since the release of ShardingSphere 5.0.0, DistSQL (Distributed SQL) has provided dynamic management for the ShardingSphere ecosystem.</p>
<p>In this article, I demonstrate a data sharding scenario in which DistSQL's flexibility allows you to create a distributed database. At the same time, I show some syntax sugar to simplify operating procedures, allowing your potential users to choose their preferred syntax.</p>
<p>A series of DistSQL statements are run through practical cases to give you a complete set of practical DistSQL sharding management methods, which create and maintain distributed databases through dynamic management.</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-09/1sharding.png" width="852" height="550" alt="Diagram of database sharding management options" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Jiang Longtao, CC BY-SA 4.0)</p>
</div>
      
  </article><h2>What is sharding?</h2>
<p>In database terminology, <em>sharding</em> is the process of partitioning a table into separate entities. While the table data is directly related, it often exists on different physical database nodes or, at the very least, within separate logical partitions.</p>
<h2>Practical case example</h2>
<p>To follow along with this example, you must have these components in place, either in your lab or in your mind as you read this article:</p>
<ul><li>Two sharding tables: <strong>t_order</strong> and <strong>t_order_item.</strong></li>
<li>For both tables, database shards are carried out with the <strong>user_id</strong> field, and table shards with the <strong>order_id</strong> field.</li>
<li>The number of shards is two databases times three tables.</li>
</ul><article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-09/2shardingsphere.png" width="852" height="570" alt="Apache ShardingSphere databases" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Jiang Longtao, CC BY-SA 4.0)</p>
</div>
      
  </article><h2>Set up the environment</h2>
<p>1. Prepare a database (MySQL, MariaDB, PostgreSQL, or openGauss) instance for access. Create two new databases: <strong>demo_ds_0</strong> and <strong>demo_ds_1</strong>.</p>
<p>2. Deploy <a href="https://shardingsphere.apache.org/document/5.1.2/en/overview/" target="_blank">Apache ShardingSphere-Proxy 5.1.2</a> and <a href="https://zookeeper.apache.org/" target="_blank">Apache ZooKeeper</a>. ZooKeeper acts as a governance center and stores ShardingSphere metadata information.</p>
<p>3. Configure <code>server.yaml</code> in the Proxy conf directory as follows:</p>
<pre>
<code class="language-yaml">mode:
  type: Cluster
  repository:
    type: ZooKeeper
    props:
      namespace: governance_ds
      server-lists: localhost:2181 #ZooKeeper address
      retryIntervalMilliseconds: 500
      timeToLiveSeconds: 60
      maxRetries: 3
      operationTimeoutMilliseconds: 500
  overwrite: falserules:
  - !AUTHORITY
    users:
      - root@%:root</code></pre><p>4. Start ShardingSphere-Proxy and connect it to Proxy using a client, for example:</p>
<pre>
<code class="language-bash">$ mysql -h 127.0.0.1 -P 3307 -u root -p</code></pre><p>5. Create a distributed database:</p>
<pre>
<code class="language-text">CREATE DATABASE sharding_db;USE sharding_db;</code></pre><h3><strong>Add storage resources</strong></h3>
<p>Next, add storage resources corresponding to the database:</p>
<pre>
<code class="language-text">ADD RESOURCE ds_0 (
    HOST=127.0.0.1,
    PORT=3306,
    DB=demo_ds_0,
    USER=root,
    PASSWORD=123456
), ds_1(
    HOST=127.0.0.1,
    PORT=3306,
    DB=demo_ds_1,
    USER=root,
    PASSWORD=123456
);</code></pre><p>View the storage resources:</p>
<pre>
<code class="language-bash">mysql&gt; SHOW DATABASE RESOURCES\G;
******** 1. row ***************************
         name: ds_1
         type: MySQL
         host: 127.0.0.1
         port: 3306
           db: demo_ds_1
          -- Omit partial attributes
******** 2. row ***************************
         name: ds_0
         type: MySQL
         host: 127.0.0.1
         port: 3306
           db: demo_ds_0
          -- Omit partial attributes</code></pre><p>Adding the optional <code>\G</code> switch to the query statement makes the output format easy to read.</p>
<p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">Open source and data science</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://opensource.com/resources/data-science?intcmp=7013a000002CxqkAAC" data-analytics-category="resource list" data-analytics-text="What is data science?">What is data science?</a></div>
              <div class="field__item"><a href="https://opensource.com/resources/python?intcmp=7013a000002CxqkAAC" data-analytics-category="resource list" data-analytics-text="What is Python?">What is Python?</a></div>
              <div class="field__item"><a href="https://enterprisersproject.com/article/2022/9/data-scientist-day-life?intcmp=7013a000002CxqkAAC" data-analytics-category="resource list" data-analytics-text="Data scientist: A day in the life">Data scientist: A day in the life</a></div>
              <div class="field__item"><a href="https://developers.redhat.com/products/red-hat-openshift-data-science/overview?intcmp=7013a000002CxqkAAC" data-analytics-category="resource list" data-analytics-text="Try OpenShift Data Science">Try OpenShift Data Science</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/mariadb-mysql-cheat-sheet?intcmp=7013a000002CxqkAAC" data-analytics-category="resource list" data-analytics-text="MariaDB and MySQL cheat sheet">MariaDB and MySQL cheat sheet</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Register for your free Red Hat account">Register for your free Red Hat account</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/data-science?intcmp=7013a000002CxqkAAC" data-analytics-category="resource list" data-analytics-text="Latest data science articles">Latest data science articles</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<h2>Create sharding rules</h2>
<p>ShardingSphere's sharding rules support regular sharding and automatic sharding. Both sharding methods have the same effect. The difference is that the configuration of automatic sharding is more concise, while regular sharding is more flexible and independent.</p>
<p>Refer to the following links for more details on automatic sharding:</p>
<ul><li><a href="https://medium.com/nerd-for-tech/intro-to-distsql-an-open-source-more-powerful-sql-bada4099211?source=your_stories_page-------------------------------------" target="_blank">Intro to DistSQL-An Open Source and More Powerful SQL</a></li>
<li><a href="https://medium.com/geekculture/autotable-your-butler-like-sharding-configuration-tool-9a45dbb7e285" target="_blank">AutoTable: Your Butler-Like Sharding Configuration Tool</a></li>
</ul><p>Next, it's time to adopt regular sharding and use the <strong>INLINE</strong> expression algorithm to implement the sharding scenarios described in the requirements.</p>
<h2>Primary key generator</h2>
<p>The primary key generator creates a secure and unique primary key for a data table in a distributed scenario. For details, refer to the document <a href="https://shardingsphere.apache.org/document/current/en/features/sharding/concept/#distributed-primary-key" target="_blank">Distributed Primary Key</a><em>.</em></p>
<p>1. Create a primary key generator:</p>
<pre>
<code class="language-text">CREATE SHARDING KEY GENERATOR snowflake_key_generator (
TYPE(NAME=SNOWFLAKE)
);</code></pre><p>2. Query the primary key generator:</p>
<pre>
<code class="language-bash">mysql&gt; SHOW SHARDING KEY GENERATORS;
+-------------------------+-----------+-------+
| name                    | type      | props |
+-------------------------+-----------+-------+
| snowflake_key_generator | snowflake | {}    |
+-------------------------+-----------+-------+
1 row in set (0.01 sec)</code></pre><h2>Sharding algorithm</h2>
<p>1. Create a database sharding algorithm used by <strong>t_order</strong> and <strong>t_order_item</strong> in common:</p>
<pre>
<code class="language-text">-- Modulo 2 based on user_id in database sharding
CREATE SHARDING ALGORITHM database_inline (
TYPE(NAME=INLINE,PROPERTIES("algorithm-expression"="ds_${user_id % 2}"))
);</code></pre><p>2. Create different table shards algorithms for <strong>t_order</strong> and <strong>t_order_item:</strong></p>
<pre>
<code class="language-text">-- Modulo 3 based on order_id in table sharding
CREATE SHARDING ALGORITHM t_order_inline (
TYPE(NAME=INLINE,PROPERTIES("algorithm-expression"="t_order_${order_id % 3}"))
);
CREATE SHARDING ALGORITHM t_order_item_inline (
TYPE(NAME=INLINE,PROPERTIES("algorithm-expression"="t_order_item_${order_id % 3}"))
);</code></pre><p>3. Query the sharding algorithm:</p>
<pre>
<code class="language-bash">mysql&gt; SHOW SHARDING ALGORITHMS;
+---------------------+--------+---------------------------------------------------+
| name                | type   | props                                             |
+---------------------+--------+---------------------------------------------------+
| database_inline     | inline | algorithm-expression=ds_${user_id % 2}            |
| t_order_inline      | inline | algorithm-expression=t_order_${order_id % 3}      |
| t_order_item_inline | inline | algorithm-expression=t_order_item_${order_id % 3} |
+---------------------+--------+---------------------------------------------------+
3 rows in set (0.00 sec)</code></pre><h2>Create a default sharding strategy</h2>
<p>The <a href="https://shardingsphere.apache.org/document/5.1.2/en/features/sharding/concept/sharding/" target="_blank">sharding strategy</a> consists of a sharding key and sharding algorithm, which in this case is <strong>databaseStrategy</strong> and <strong>tableStrategy</strong>. Because <strong>t_order</strong> and <strong>t_order_item</strong> have the same database sharding field and sharding algorithm, create a default strategy to be used by all shard tables with no sharding strategy configured.</p>
<p>1. Create a default database sharding strategy:</p>
<pre>
<code class="language-text">CREATE DEFAULT SHARDING DATABASE STRATEGY (
TYPE=STANDARD,SHARDING_COLUMN=user_id,SHARDING_ALGORITHM=database_inline
);</code></pre><p>2. Query default strategy:</p>
<pre>
<code class="language-bash">mysql&gt; SHOW DEFAULT SHARDING STRATEGY\G;
*************************** 1. row ***************************
                    name: TABLE
                    type: NONE
         sharding_column:
 sharding_algorithm_name:
 sharding_algorithm_type:
sharding_algorithm_props:
*************************** 2. row ***************************
                    name: DATABASE
                    type: STANDARD
         sharding_column: user_id
 sharding_algorithm_name: database_inline
 sharding_algorithm_type: inline
sharding_algorithm_props: {algorithm-expression=ds_${user_id % 2}}
2 rows in set (0.00 sec)</code></pre><p>You have not configured the default table sharding strategy, so the default strategy of <strong>TABLE</strong> is <strong>NONE</strong>.</p>
<h2>Set sharding rules</h2>
<p>The primary key generator and sharding algorithm are both ready. Now you can create sharding rules. The method I demonstrate below is a little complicated and involves multiple steps. In the next section, I'll show you how to create sharding rules in just one step, but for now, witness how it's typically done.</p>
<p>First, define <strong>t_order</strong>:</p>
<pre>
<code class="language-text">CREATE SHARDING TABLE RULE t_order (
DATANODES("ds_${0..1}.t_order_${0..2}"),
TABLE_STRATEGY(TYPE=STANDARD,SHARDING_COLUMN=order_id,SHARDING_ALGORITHM=t_order_inline),
KEY_GENERATE_STRATEGY(COLUMN=order_id,KEY_GENERATOR=snowflake_key_generator)
);</code></pre><p>Here is an explanation of the values found above:</p>
<ul><li><strong>DATANODES</strong> specifies the data nodes of shard tables.</li>
<li><strong>TABLE_STRATEGY</strong> specifies the table strategy, among which <strong>SHARDING_ALGORITHM</strong> uses created sharding algorithm <strong>t_order_inline</strong>.</li>
<li><strong>KEY_GENERATE_STRATEGY</strong> specifies the primary key generation strategy of the table. Skip this configuration if primary key generation is not required.</li>
</ul><p>Next, define <strong>t_order_item</strong>:</p>
<pre>
<code class="language-text">CREATE SHARDING TABLE RULE t_order_item (
DATANODES("ds_${0..1}.t_order_item_${0..2}"),
TABLE_STRATEGY(TYPE=STANDARD,SHARDING_COLUMN=order_id,SHARDING_ALGORITHM=t_order_item_inline),
KEY_GENERATE_STRATEGY(COLUMN=order_item_id,KEY_GENERATOR=snowflake_key_generator)
);</code></pre><p>Query the sharding rules to verify what you've created:</p>
<pre>
<code class="language-bash">mysql&gt; SHOW SHARDING TABLE RULES\G;
************************** 1. row ***************************
                           table: t_order
               actual_data_nodes: ds_${0..1}.t_order_${0..2}
             actual_data_sources:
          database_strategy_type: STANDARD
        database_sharding_column: user_id
database_sharding_algorithm_type: inline
database_sharding_algorithm_props: algorithm-expression=ds_${user_id % 2}
              table_strategy_type: STANDARD
            table_sharding_column: order_id
    table_sharding_algorithm_type: inline
   table_sharding_algorithm_props: algorithm-expression=t_order_${order_id % 3}
              key_generate_column: order_id
               key_generator_type: snowflake
              key_generator_props:
*************************** 2. row ***************************
                            table: t_order_item
                actual_data_nodes: ds_${0..1}.t_order_item_${0..2}
              actual_data_sources:
           database_strategy_type: STANDARD
         database_sharding_column: user_id
 database_sharding_algorithm_type: inline
database_sharding_algorithm_props: algorithm-expression=ds_${user_id % 2}
              table_strategy_type: STANDARD
            table_sharding_column: order_id
    table_sharding_algorithm_type: inline
   table_sharding_algorithm_props: algorithm-expression=t_order_item_${order_id % 3}
              key_generate_column: order_item_id
               key_generator_type: snowflake
              key_generator_props:
2 rows in set (0.00 sec)</code></pre><p>This looks right so far. You have now configured the sharding rules for <strong>t_order</strong> and <strong>t_order_item</strong>.</p>
<p>You can skip the steps for creating the primary key generator, sharding algorithm, and default strategy, and complete the sharding rules in one step. Here's how to make it easier.</p>
<h2>Sharding rule syntax</h2>
<p>For instance, if you want to add a shard table called <strong>t_order_detail</strong>, you can create sharding rules as follows:</p>
<pre>
<code class="language-text">CREATE SHARDING TABLE RULE t_order_detail (
DATANODES("ds_${0..1}.t_order_detail_${0..1}"),
DATABASE_STRATEGY(TYPE=STANDARD,SHARDING_COLUMN=user_id,SHARDING_ALGORITHM(TYPE(NAME=INLINE,PROPERTIES("algorithm-expression"="ds_${user_id % 2}")))),
TABLE_STRATEGY(TYPE=STANDARD,SHARDING_COLUMN=order_id,SHARDING_ALGORITHM(TYPE(NAME=INLINE,PROPERTIES("algorithm-expression"="t_order_detail_${order_id % 3}")))),
KEY_GENERATE_STRATEGY(COLUMN=detail_id,TYPE(NAME=snowflake))
);</code></pre><p>This statement specifies a database sharding strategy, table strategy, and primary key generation strategy, but it doesn't use existing algorithms. The DistSQL engine automatically uses the input expression to create an algorithm for the sharding rules of <strong>t_order_detail</strong>.</p>
<p>Now there's a primary key generator:</p>
<pre>
<code class="language-bash">mysql&gt; SHOW SHARDING KEY GENERATORS;
+--------------------------+-----------+-------+
| name                     | type      | props |
+--------------------------+-----------+-------+
| snowflake_key_generator  | snowflake | {}    |
| t_order_detail_snowflake | snowflake | {}    |
+--------------------------+-----------+-------+
2 rows in set (0.00 sec)</code></pre><p>Display the sharding algorithm:</p>
<pre>
<code class="language-bash">mysql&gt; SHOW SHARDING ALGORITHMS;
+--------------------------------+--------+-----------------------------------------------------+
| name                           | type   | props                                               |
+--------------------------------+--------+-----------------------------------------------------+
| database_inline                | inline | algorithm-expression=ds_${user_id % 2}              |
| t_order_inline                 | inline | algorithm-expression=t_order_${order_id % 3}        |
| t_order_item_inline            | inline | algorithm-expression=t_order_item_${order_id % 3}   |
| t_order_detail_database_inline | inline | algorithm-expression=ds_${user_id % 2}              |
| t_order_detail_table_inline    | inline | algorithm-expression=t_order_detail_${order_id % 3} |
+--------------------------------+--------+-----------------------------------------------------+
5 rows in set (0.00 sec)</code></pre><p>And finally, the sharding rules:</p>
<pre>
<code class="language-bash">mysql&gt; SHOW SHARDING TABLE RULES\G;
*************************** 1. row ***************************
                            table: t_order
                actual_data_nodes: ds_${0..1}.t_order_${0..2}
              actual_data_sources:
           database_strategy_type: STANDARD
         database_sharding_column: user_id
 database_sharding_algorithm_type: inline
database_sharding_algorithm_props: algorithm-expression=ds_${user_id % 2}
              table_strategy_type: STANDARD
            table_sharding_column: order_id
    table_sharding_algorithm_type: inline
   table_sharding_algorithm_props: algorithm-expression=t_order_${order_id % 3}
              key_generate_column: order_id
               key_generator_type: snowflake
              key_generator_props:
*************************** 2. row ***************************
                            table: t_order_item
                actual_data_nodes: ds_${0..1}.t_order_item_${0..2}
              actual_data_sources:
           database_strategy_type: STANDARD
         database_sharding_column: user_id
 database_sharding_algorithm_type: inline
database_sharding_algorithm_props: algorithm-expression=ds_${user_id % 2}
              table_strategy_type: STANDARD
            table_sharding_column: order_id
    table_sharding_algorithm_type: inline
   table_sharding_algorithm_props: algorithm-expression=t_order_item_${order_id % 3}
              key_generate_column: order_item_id
               key_generator_type: snowflake
              key_generator_props:
*************************** 3. row ***************************
                            table: t_order_detail
                actual_data_nodes: ds_${0..1}.t_order_detail_${0..1}
              actual_data_sources:
           database_strategy_type: STANDARD
         database_sharding_column: user_id
 database_sharding_algorithm_type: inline
database_sharding_algorithm_props: algorithm-expression=ds_${user_id % 2}
              table_strategy_type: STANDARD
            table_sharding_column: order_id
    table_sharding_algorithm_type: inline
   table_sharding_algorithm_props: algorithm-expression=t_order_detail_${order_id % 3}
              key_generate_column: detail_id
               key_generator_type: snowflake
              key_generator_props:
3 rows in set (0.01 sec)</code></pre><p>In the <code>CREATE SHARDING TABLE RULE</code> statement, <strong>DATABASE_STRATEGY</strong>, <strong>TABLE_STRATEGY</strong>, and <strong>KEY_GENERATE_STRATEGY</strong> can reuse existing algorithms.</p>
<p>Alternatively, they can be defined quickly through syntax. The difference is that additional algorithm objects are created.</p>
<h2>Configuration and verification</h2>
<p>Once you have created the configuration verification rules, you can verify them in the following ways.</p>
<p>1. Check node distribution:</p>
<p>DistSQL provides <code>SHOW SHARDING TABLE NODES</code> for checking node distribution, and users can quickly learn the distribution of shard tables:</p>
<pre>
<code class="language-bash">mysql&gt; SHOW SHARDING TABLE NODES;
+----------------+------------------------------------------------------------------------------------------------------------------------------+
| name           | nodes                                                                                                                        |
+----------------+------------------------------------------------------------------------------------------------------------------------------+
| t_order        | ds_0.t_order_0, ds_0.t_order_1, ds_0.t_order_2, ds_1.t_order_0, ds_1.t_order_1, ds_1.t_order_2                               |
| t_order_item   | ds_0.t_order_item_0, ds_0.t_order_item_1, ds_0.t_order_item_2, ds_1.t_order_item_0, ds_1.t_order_item_1, ds_1.t_order_item_2 |
| t_order_detail | ds_0.t_order_detail_0, ds_0.t_order_detail_1, ds_1.t_order_detail_0, ds_1.t_order_detail_1                                   |
+----------------+------------------------------------------------------------------------------------------------------------------------------+
3 rows in set (0.01 sec)

mysql&gt; SHOW SHARDING TABLE NODES t_order_item;
+--------------+------------------------------------------------------------------------------------------------------------------------------+
| name         | nodes                                                                                                                        |
+--------------+------------------------------------------------------------------------------------------------------------------------------+
| t_order_item | ds_0.t_order_item_0, ds_0.t_order_item_1, ds_0.t_order_item_2, ds_1.t_order_item_0, ds_1.t_order_item_1, ds_1.t_order_item_2 |
+--------------+------------------------------------------------------------------------------------------------------------------------------+
1 row in set (0.00 sec)</code></pre><p>You can see that the node distribution of the shard table is consistent with what is described in the requirement.</p>
<h2>SQL preview</h2>
<p>Previewing SQL is also an easy way to verify configurations. Its syntax is <code>PREVIEW SQL</code>. First, make a query with no shard key, with all routes:</p>
<pre>
<code class="language-bash">mysql&gt; PREVIEW SELECT * FROM t_order;
+------------------+---------------------------------------------------------------------------------------------+
| data_source_name | actual_sql                                                                                  |
+------------------+---------------------------------------------------------------------------------------------+
| ds_0             | SELECT * FROM t_order_0 UNION ALL SELECT * FROM t_order_1 UNION ALL SELECT * FROM t_order_2 |
| ds_1             | SELECT * FROM t_order_0 UNION ALL SELECT * FROM t_order_1 UNION ALL SELECT * FROM t_order_2 |
+------------------+---------------------------------------------------------------------------------------------+
2 rows in set (0.13 sec)

mysql&gt; PREVIEW SELECT * FROM t_order_item;
+------------------+------------------------------------------------------------------------------------------------------------+
| data_source_name | actual_sql                                                                                                 |
+------------------+------------------------------------------------------------------------------------------------------------+
| ds_0             | SELECT * FROM t_order_item_0 UNION ALL SELECT * FROM t_order_item_1 UNION ALL SELECT * FROM t_order_item_2 |
| ds_1             | SELECT * FROM t_order_item_0 UNION ALL SELECT * FROM t_order_item_1 UNION ALL SELECT * FROM t_order_item_2 |
+------------------+------------------------------------------------------------------------------------------------------------+
2 rows in set (0.00 sec)</code></pre><p>Now specify <strong>user_id</strong> in a query with a single database route:</p>
<pre>
<code class="language-bash">mysql&gt; PREVIEW SELECT * FROM t_order WHERE user_id = 1;
+------------------+---------------------------------------------------------------------------------------------------------------------------------------------------+
| data_source_name | actual_sql                                                                                                                                        |
+------------------+---------------------------------------------------------------------------------------------------------------------------------------------------+
| ds_1             | SELECT * FROM t_order_0 WHERE user_id = 1 UNION ALL SELECT * FROM t_order_1 WHERE user_id = 1 UNION ALL SELECT * FROM t_order_2 WHERE user_id = 1 |
+------------------+---------------------------------------------------------------------------------------------------------------------------------------------------+
1 row in set (0.14 sec)

mysql&gt; PREVIEW SELECT * FROM t_order_item WHERE user_id = 2;
+------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| data_source_name | actual_sql                                                                                                                                                       |
+------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| ds_0             | SELECT * FROM t_order_item_0 WHERE user_id = 2 UNION ALL SELECT * FROM t_order_item_1 WHERE user_id = 2 UNION ALL SELECT * FROM t_order_item_2 WHERE user_id = 2 |
+------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------+
1 row in set (0.00 sec)</code></pre><p>Specify <strong>user_id</strong> and <strong>order_id</strong> with a single table route:</p>
<pre>
<code class="language-bash">mysql&gt; PREVIEW SELECT * FROM t_order WHERE user_id = 1 AND order_id = 1;
+------------------+------------------------------------------------------------+
| data_source_name | actual_sql                                                 |
+------------------+------------------------------------------------------------+
| ds_1             | SELECT * FROM t_order_1 WHERE user_id = 1 AND order_id = 1 |
+------------------+------------------------------------------------------------+
1 row in set (0.04 sec)

mysql&gt; PREVIEW SELECT * FROM t_order_item WHERE user_id = 2 AND order_id = 5;
+------------------+-----------------------------------------------------------------+
| data_source_name | actual_sql                                                      |
+------------------+-----------------------------------------------------------------+
| ds_0             | SELECT * FROM t_order_item_2 WHERE user_id = 2 AND order_id = 5 |
+------------------+-----------------------------------------------------------------+
1 row in set (0.01 sec)</code></pre><p>Single-table routes scan the fewest shard tables and offer the highest efficiency.</p>
<h2>Query unused resources</h2>
<p>During system maintenance, algorithms or storage resources that are no longer in use may need to be released, or resources that need to be released may have been referenced and cannot be deleted. DistSQL's <code>SHOW UNUSED RESOURCES</code> command can solve these problems:</p>
<pre>
<code class="language-bash">mysql&gt; ADD RESOURCE ds_2 (
    -&gt;     HOST=127.0.0.1,
    -&gt;     PORT=3306,
    -&gt;     DB=demo_ds_2,
    -&gt;     USER=root,
    -&gt;     PASSWORD=123456
    -&gt; );
Query OK, 0 rows affected (0.07 sec)

mysql&gt; SHOW UNUSED RESOURCES\G;
*************************** 1. row ***************************
                           name: ds_2
                           type: MySQL
                           host: 127.0.0.1
                           port: 3306
                             db: demo_ds_2
connection_timeout_milliseconds: 30000
      idle_timeout_milliseconds: 60000
      max_lifetime_milliseconds: 2100000
                  max_pool_size: 50
                  min_pool_size: 1
                      read_only: false
               other_attributes: {"dataSourceProperties":{"cacheServerConfiguration":"true","elideSetAutoCommits":"true","useServerPrepStmts":"true","cachePrepStmts":"true","useSSL":"false","rewriteBatchedStatements":"true","cacheResultSetMetadata":"false","useLocalSessionState":"true","maintainTimeStats":"false","prepStmtCacheSize":"200000","tinyInt1isBit":"false","prepStmtCacheSqlLimit":"2048","serverTimezone":"UTC","netTimeoutForStreamingResults":"0","zeroDateTimeBehavior":"round"},"healthCheckProperties":{},"initializationFailTimeout":1,"validationTimeout":5000,"leakDetectionThreshold":0,"poolName":"HikariPool-8","registerMbeans":false,"allowPoolSuspension":false,"autoCommit":true,"isolateInternalQueries":false}
1 row in set (0.03 sec)</code></pre><h3>Query unused primary key generator</h3>
<p>DistSQL can also display unused sharding key generators with the <code>SHOW UNUSED SHARDING KEY GENERATORS</code>:</p>
<pre>
<code class="language-bash">mysql&gt; SHOW SHARDING KEY GENERATORS;
+--------------------------+-----------+-------+
| name                     | type      | props |
+--------------------------+-----------+-------+
| snowflake_key_generator  | snowflake | {}    |
| t_order_detail_snowflake | snowflake | {}    |
+--------------------------+-----------+-------+
2 rows in set (0.00 sec)

mysql&gt; SHOW UNUSED SHARDING KEY GENERATORS;
Empty set (0.01 sec)

mysql&gt; CREATE SHARDING KEY GENERATOR useless (
    -&gt; TYPE(NAME=SNOWFLAKE)
    -&gt; );
Query OK, 0 rows affected (0.04 sec)

mysql&gt; SHOW UNUSED SHARDING KEY GENERATORS;
+---------+-----------+-------+
| name    | type      | props |
+---------+-----------+-------+
| useless | snowflake |       |
+---------+-----------+-------+
1 row in set (0.01 sec)</code></pre><h3>Query unused sharding algorithm</h3>
<p>DistSQL can reveal unused sharding algorithms with (you guessed it) the <code>SHOW UNUSED SHARDING ALGORITHMS</code> command:</p>
<pre>
<code class="language-bash">mysql&gt; SHOW SHARDING ALGORITHMS;
+--------------------------------+--------+-----------------------------------------------------+
| name                           | type   | props                                               |
+--------------------------------+--------+-----------------------------------------------------+
| database_inline                | inline | algorithm-expression=ds_${user_id % 2}              |
| t_order_inline                 | inline | algorithm-expression=t_order_${order_id % 3}        |
| t_order_item_inline            | inline | algorithm-expression=t_order_item_${order_id % 3}   |
| t_order_detail_database_inline | inline | algorithm-expression=ds_${user_id % 2}              |
| t_order_detail_table_inline    | inline | algorithm-expression=t_order_detail_${order_id % 3} |
+--------------------------------+--------+-----------------------------------------------------+
5 rows in set (0.00 sec)

mysql&gt; CREATE SHARDING ALGORITHM useless (
    -&gt; TYPE(NAME=INLINE,PROPERTIES("algorithm-expression"="ds_${user_id % 2}"))
    -&gt; );
Query OK, 0 rows affected (0.04 sec)

mysql&gt; SHOW UNUSED SHARDING ALGORITHMS;
+---------+--------+----------------------------------------+
| name    | type   | props                                  |
+---------+--------+----------------------------------------+
| useless | inline | algorithm-expression=ds_${user_id % 2} |
+---------+--------+----------------------------------------+
1 row in set (0.00 sec)</code></pre><h3>Query rules that use the target storage resources</h3>
<p>You can also see used resources within rules with <code>SHOW RULES USED RESOURCE</code>. All rules that use a resource can be queried, not limited to the sharding rule.</p>
<pre>
<code class="language-bash">mysql&gt; DROP RESOURCE ds_0;
ERROR 1101 (C1101): Resource [ds_0] is still used by [ShardingRule].

mysql&gt; SHOW RULES USED RESOURCE ds_0;
+----------+----------------+
| type     | name           |
+----------+----------------+
| sharding | t_order        |
| sharding | t_order_item   |
| sharding | t_order_detail |
+----------+----------------+
3 rows in set (0.00 sec)</code></pre><h3><strong>Query sharding rules that use the target primary key generator</strong></h3>
<p>You can find sharding rules using a key generator with <code>SHOW SHARDING TABLE RULES USED KEY GENERATOR</code>:</p>
<pre>
<code class="language-bash">mysql&gt; SHOW SHARDING KEY GENERATORS;
+--------------------------+-----------+-------+
| name                     | type      | props |
+--------------------------+-----------+-------+
| snowflake_key_generator  | snowflake | {}    |
| t_order_detail_snowflake | snowflake | {}    |
| useless                  | snowflake | {}    |
+--------------------------+-----------+-------+
3 rows in set (0.00 sec)

mysql&gt; DROP SHARDING KEY GENERATOR snowflake_key_generator;
ERROR 1121 (C1121): Sharding key generator `[snowflake_key_generator]` in database `sharding_db` are still in used.

mysql&gt; SHOW SHARDING TABLE RULES USED KEY GENERATOR snowflake_key_generator;
+-------+--------------+
| type  | name         |
+-------+--------------+
| table | t_order      |
| table | t_order_item |
+-------+--------------+
2 rows in set (0.00 sec)</code></pre><h3>Query sharding rules that use the target algorithm</h3>
<p>Show sharding rules using a target algorithm with <code>SHOW SHARDING TABLE RULES USED ALGORITHM</code>:</p>
<pre>
<code class="language-bash">mysql&gt; SHOW SHARDING ALGORITHMS;
+--------------------------------+--------+-----------------------------------------------------+
| name                           | type   | props                                               |
+--------------------------------+--------+-----------------------------------------------------+
| database_inline                | inline | algorithm-expression=ds_${user_id % 2}              |
| t_order_inline                 | inline | algorithm-expression=t_order_${order_id % 3}        |
| t_order_item_inline            | inline | algorithm-expression=t_order_item_${order_id % 3}   |
| t_order_detail_database_inline | inline | algorithm-expression=ds_${user_id % 2}              |
| t_order_detail_table_inline    | inline | algorithm-expression=t_order_detail_${order_id % 3} |
| useless                        | inline | algorithm-expression=ds_${user_id % 2}              |
+--------------------------------+--------+-----------------------------------------------------+
6 rows in set (0.00 sec)

mysql&gt; DROP SHARDING ALGORITHM t_order_detail_table_inline;
ERROR 1116 (C1116): Sharding algorithms `[t_order_detail_table_inline]` in database `sharding_db` are still in used.

mysql&gt; SHOW SHARDING TABLE RULES USED ALGORITHM t_order_detail_table_inline;
+-------+----------------+
| type  | name           |
+-------+----------------+
| table | t_order_detail |
+-------+----------------+
1 row in set (0.00 sec)</code></pre><h2>Make sharding better</h2>
<p>DistSQL provides a flexible syntax to help simplify operations. In addition to the <strong>INLINE</strong> algorithm, DistSQL supports standard sharding, compound sharding, HINT sharding, and custom sharding algorithms.</p>
<p>If you have any questions or suggestions about <a href="https://shardingsphere.apache.org/" target="_blank">Apache ShardingSphere</a>, please feel free to post them on <a href="https://github.com/apache/shardingsphere" target="_blank">ShardingSphereGitHub</a><u>.</u></p>
