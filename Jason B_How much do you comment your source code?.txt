<p>While it may be true that the best code is self-<a href="https://opensource.com/tags/documentation" target="_blank">documenting</a>, even the clearest written source code requires adequate documentation in order to be quickly parsed and understood by human readers.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Programming and development</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ&amp;src=programming_resource_menu4">Red Hat Developers Blog</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Programming cheat sheets</a></li>
<li><a href="https://www.redhat.com/en/services/training/learning-subscription?intcmp=7016000000127cYAAQ">Try for free: Red Hat Learning Subscription</a></li>
<li><a href="https://opensource.com/downloads/bash-programming-guide?intcmp=7016000000127cYAAQ">eBook: An introduction to programming with Bash</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/bash-shell-cheat-sheet?intcmp=7016000000127cYAAQ">Bash Shell Scripting Cheat Sheet</a></li>
<li><a href="https://developers.redhat.com/e-books/modernizing-enterprise-java?intcmp=7016000000127cYAAQ">eBook: Modernizing Enterprise Java</a></li>
</ul></div>
</div>
</div>
</div>
<p>In practice, though, where and how much to comment is much more up to the individual developer to decide than elements of the code itself. The compiler or interpreter doesn't generally care about your comments; after all, they're not for it, they're for us humans. And so while some organizations and projects will offer guidelines, commenting styles are much more open to interpretation.</p>
<p>In general, being clear and concise with your comments will be appreciated. But particular within open source, you don't have the luxury of assuming that other developers looking at your code are familiar with the context in which it was written. Everyone is new to the project at some point, and you can't count on them understanding the project's history, its quirks, or the libraries it uses. New contributors may not be native speakers of your language, either.</p>
<p>And so well-written and well-placed comments are an important part of your project's documentation. Are you making sure that your project's code is as accessible as it can be?</p>
<p>How often do you comment? Do you think you comment frequently enough? Let us know what you think!</p>
