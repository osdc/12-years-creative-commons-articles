<p id="what-makes-a-high-impact-team">What is the top requirement for high-impact teams? When I was recently asked this question, I started making a list.</p>
<ul><li>You need to know why you are doing what you are doing, and everyone on the team needs to know what that is.</li>
<li>You need to trust the people on the team. Trusting them is connected to personally caring for each member of your team.</li>
</ul><p>Assuming your team has a great purpose and people who trust and care for each other, will that guarantee a high impact? Maybe not.</p>
<p>From my perspective, the one thing that is essential on a high-impact team is for everyone to understand the workflow—from the beginning to the end. And not only to have a clear, shared understanding of all the steps but to have the workflow visible to all team members at all times.</p>
<h2 id="how-to-make-your-work-visible">How to make your work visible</h2>
<p>If you build software or do other work in the virtual world, it's not easy to make your workflow visible at each step. Here's a process I came up with some time ago when working with a team that was in charge of delivering a website for a luxury products company. The expectations for the user experience were incredibly high. Some would say ridiculously high.</p>
<p>We started to investigate our team's workflow by beginning at the end. On the upper far-right side of a large whiteboard, I wrote the word DONE in large letters and asked two questions:</p>
<ul><li><strong>How do we know that our work is done?</strong> The website is behaving as expected by the customer on all the defined platforms.</li>
<li><strong>What changed between now and before?</strong> We implemented a new feature or a fix to an existing feature, and the result of this implementation is that the website is behaving as expected by the customer on all the defined platforms.</li>
</ul><p>These questions gave us information about the steps that had to happen before considering a work item "done." We need to test the website on all of the platforms. And we need to implement a feature or a fix.</p>
<p>On the whiteboard, I wrote TEST to the left of DONE, and IMPLEMENT to the left of TEST.</p>
<p>In the whiteboard's upper-left corner, I added a new column called INBOX. I explained to the team that we would use cards on the whiteboard to represent all the work items we had to do. When a new card (i.e., work item) enters the workflow, we would place it in the INBOX column.</p>
<ul><li><strong>What needs to happen between the INBOX and the IMPLEMENT column?</strong> Before we begin working on the issue, we need to assess its severity. We need to understand the new feature and how it will affect our personas.</li>
</ul><p>I added an ASSESSED column between the INBOX and IMPLEMENT columns.</p>
<ul><li><strong>How do we know what we need to implement?</strong> There are two types of work: planned work and unplanned work.
<ul><li>Unplanned work is the fixes that are detected by the users, the customers, or our teams. We are committed to delivering the fixes in a defined timeframe according to the severity of the issue.</li>
<li>Planned work is the new features that are defined with the representatives from the customer teams.</li>
</ul></li>
</ul><p>To distinguish between the types of work, I added a horizontal line across the columns to create an area in each column for Fixes (unplanned) and Features (planned) work.</p>
<p>Here's what the whiteboard looks like:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="High-impact teams workflow"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/changing_your_team_from_the_inside.jpg" width="675" height="380" alt="High-impact teams workflow" title="High-impact teams workflow" /></div>
      
  </article></p>
<p>We identified another issue we needed to address: The list of defined platforms had changed regularly, so we decided to make that list visible on a wall in the team room and use it as a checklist for the implementation and test activities.</p>
<h2 id="how-it-works">How it works</h2>
<p>A card enters our workflow in the INBOX column:</p>
<ul><li>If the card describes an issue to fix, the team assess its severity and places the card in the ASSESSED column of the FIX swimlane. The card is timestamped and assigned a due date, then all the cards are sorted by due date and severity.</li>
<li>In the case of a feature to implement, the team evaluates what the ask means, discusses the implementation approach, and places the card in the ASSESSED column of the FEATURE swimlane. The customer and the team agree on the relative importance of the feature compared to the other features already in the column.</li>
<li>On the list of defined platforms posted on our team room wall, the team also discussed and displayed the assessment criteria for bugs and features on each platform.</li>
</ul><h2 id="what's-the-benefit">What's the benefit?</h2>
<p>This example walked you through how we identified our workflow and made the work visible. In the interest of brevity, I simplified what was a multi-day planning process.</p>
<p>The fundamental principles to make work visible are: Start with the desired end state, ask questions to understand what needs to happen to reach that state, and iterate to reach the beginning of the workflow. This must be a team activity because each team member will have his or her own view of the different steps.</p>
<p>The immediate benefit for the team is it is obvious what needs to be worked on next, so each team member can pick the next card knowing that he or she is doing the right thing for the project. The indirect benefit is that it shows where in the flow we are overinvesting–we can see it because the cards are pilling up in the column just to the right of our overinvestment. In this case, we must limit that space according to the capacity of the team and focus on keeping a continuous and even flow of cards.</p>
<h2 id="what-do-you-think">What do you think?</h2>
<p>By making the work visible, the team knows where to invest its energy to have the most impact. This is why I think making work visible is the top requirement for high-impact teams. What do you think is the #1 factor in a high-impact team? Please post your answer in the comments or tweet <a href="https://twitter.com/alexismonville?lang=en" target="_blank">@alexismonville</a> or <a href="https://twitter.com/opensourceway?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor" target="_blank">@opensourceway</a> with the hashtag <a href="https://twitter.com/hashtag/ChangingYourTeam?src=hash&amp;lang=en" target="_blank">#ChangingYourTeam</a>.</p>
<hr /><p><em>For more on building high-impact, sustainable teams, check out Alexis' book</em> <em><a href="http://alexis.monville.com/en/" target="_blank">Changing Your Team From The Inside</a>.</em></p>
