<p>Wget is a free utility to download files from the web. It gets data from the Internet and saves it to a file or displays it in your terminal. This is literally also what web browsers do, such as Firefox or Chromium, except by default, they <em>render</em> the information in a graphical window and usually require a user to be actively controlling them. The <code>wget</code> utility is designed to be non-interactive, meaning you can script or schedule <code>wget</code> to download files whether you're at your computer or not.</p>
<h2>Download a file with wget</h2>
<p>You can download a file with <code>wget</code> by providing a link to a specific URL. If you provide a URL that defaults to <code>index.html</code>, then the index page gets downloaded. By default, the file is downloaded into a file of the same name in your current working directory.</p>
<pre><code class="language-bash">$ wget http://example.com
--2021-09-20 17:23:47-- http://example.com/
Resolving example.com... 93.184.216.34, 2606:2800:220:1:248:1893:25c8:1946
Connecting to example.com|93.184.216.34|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 1256 (1.2K) [text/html]
Saving to: 'index.html'</code></pre><p>You can make <code>wget</code> send the data to standard out (<code>stdout</code>) instead by using the <code>--output-document</code> with a dash <code>-</code> character:</p>
<pre><code class="language-bash">$ wget http://example.com --output-document - | head -n4
&lt;!doctype html&gt;
&lt;html&gt;
&lt;head&gt;
   &lt;title&gt;Example Domain&lt;/title&gt;</code></pre><p>You can use the <code>--output-document</code> option (<code>-O</code> for short) to name your download whatever you want:</p>
<pre><code class="language-bash">$ wget http://example.com --output-document foo.html</code></pre><h2>Continue a partial download</h2>
<p>If you're downloading a very large file, you might find that you have to interrupt the download. With the <code>--continue</code> (<code>-c</code> for short), <code>wget</code> can determine where the download left off and continue the file transfer. That means the next time you download a 4 GB Linux distribution ISO you don't ever have to go back to the start when something goes wrong.</p>
<pre><code class="language-bash">$ wget --continue https://example.com/linux-distro.iso</code></pre><h2>Download a sequence of files</h2>
<p>If it's not one big file but several files that you need to download, <code>wget</code> can help you with that. Assuming you know the location and filename pattern of the files you want to download, you can use Bash syntax to specify the start and end points between a range of integers to represent a sequence of filenames:</p>
<pre><code class="language-bash">$ wget http://example.com/file_{1..4}.webp</code></pre><h2>Mirror a whole site</h2>
<p>You can download an entire site, including its directory structure, using the<code> --mirror</code> option. This option is the same as running <code>--recursive --level inf --timestamping --no-remove-listing</code>, which means it's infinitely recursive, so you're getting everything on the domain you specify. Depending on how old the website is, that could mean you're getting a lot more content than you realize.</p>
<p>If you're using <code>wget</code> to archive a site, then the options <code>--no-cookies --page-requisites --convert-links</code> are also useful to ensure that every page is fresh, complete, and that the site copy is more or less self-contained.</p>
<h2>Modify HTML headers</h2>
<p>Protocols used for data exchange have a lot of metadata embedded in the packets computers send to communicate. HTTP headers are components of the initial portion of data. When you browse a website, your browser sends HTTP request headers. Use the <code>--debug</code> option to see what header information <code>wget</code> sends with each request:</p>
<pre><code class="language-bash">$ wget --debug example.com
---request begin---
GET / HTTP/1.1
User-Agent: Wget/1.19.5 (linux-gnu)
Accept: */*
Accept-Encoding: identity
Host: example.com
Connection: Keep-Alive

---request end---</code></pre><p>You can modify your request header with the <code>--header</code> option. For instance, it's sometimes useful to mimic a specific browser, either for testing or to account for poorly coded sites that only work correctly for specific user agents.</p>
<p>To identify as Microsoft Edge running on Windows:</p>
<pre><code class="language-bash">$ wget --debug --header="User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36 Edg/91.0.864.59" http://example.com</code></pre><p>You can also masquerade as a specific mobile device:</p>
<pre><code class="language-bash">$ wget --debug \
--header="User-Agent: Mozilla/5.0 (iPhone; CPU iPhone OS 13_5_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Mobile/15E148 Safari/604.1" \
http://example.com</code></pre><h2>Viewing response headers</h2>
<p>In the same way header information is sent with browser requests, header information is also included in responses. You can see response headers with the <code>--debug</code> option:</p>
<pre><code class="language-bash">$ wget --debug example.com
[...]
---response begin---
HTTP/1.1 200 OK
Accept-Ranges: bytes
Age: 188102
Cache-Control: max-age=604800
Content-Type: text/html; charset=UTF-8
Etag: "3147526947"
Server: ECS (sab/574F)
Vary: Accept-Encoding
X-Cache: HIT
Content-Length: 1256

---response end---
200 OK
Registered socket 3 for persistent reuse.
URI content encoding = 'UTF-8'
Length: 1256 (1.2K) [text/html]
Saving to: 'index.html'</code></pre><h2>Responding to a 301 response</h2>
<p>A 200 response code means that everything has worked as expected. A 301 response, on the other hand, means that an URL has been moved permanently to a different location. It's a common way for a website admin to relocate content while leaving a "trail" so people visiting the old location can still find it. By default, <code>wget</code> follows redirects, and that's probably what you normally want it to do.</p>
<p>However, you can control what <code>wget</code> does when it encounters a 301 response with the <code>--max-redirect</code> option. You can set it to <code>0</code> to follow no redirects:</p>
<pre><code class="language-bash">$ wget --max-redirect 0 http://iana.org
--2021-09-21 11:01:35-- http://iana.org/
Resolving iana.org... 192.0.43.8, 2001:500:88:200::8
Connecting to iana.org|192.0.43.8|:80... connected.
HTTP request sent, awaiting response... 301 Moved Permanently
Location: https://www.iana.org/ [following]
0 redirections exceeded.</code></pre><p>Alternately, you can set it to some other number to control how many redirects <code>wget</code> follows.</p>
<h3>Expand a shortened URL</h3>
<p>The <code>--max-redirect</code> option is useful for looking at shortened URLs before actually visiting them. Shortened URLs can be useful for print media, in which users can't just copy and paste a long URL, or on social networks with character limits (this isn't as much of an issue on a modern and <a href="https://opensource.com/article/17/4/guide-to-mastodon" target="_blank">open source social network like Mastodon</a>). However, they can also be a little dangerous because their destination is, by nature, concealed. By combining the <code>--head</code> option to view just the HTTP headers, and the <code>--location</code> option to unravel the final destination of an URL, you can peek into a shortened URL without loading the full resource:</p>
<pre><code class="language-bash">$ wget --max-redirect 0 "https://bit.ly/2yDyS4T"
--2021-09-21 11:32:04-- https://bit.ly/2yDyS4T
Resolving bit.ly... 67.199.248.10, 67.199.248.11
Connecting to bit.ly|67.199.248.10|:443... connected.
HTTP request sent, awaiting response... 301 Moved Permanently
Location: http://example.com/ [following]
0 redirections exceeded.</code></pre><p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Linux resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux commands cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Free online course: RHEL Technical Overview</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux networking cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">SELinux cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux common commands cheat sheet</a></li>
<li><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">What are Linux containers?</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Our latest Linux articles</a></li>
</ul></div>
</div>
</div>
</div>

<p>The penultimate line of output, starting with <strong>Location</strong>, reveals the intended destination.</p>
<h2>Use wget</h2>
<p>Once you practice thinking about the process of exploring the web as a single command, <code>wget</code> becomes a fast and efficient way to pull information you need from the Internet without bothering with a graphical interface. To help you build it into your usual workflow, we've created a cheat sheet with common <code>wget</code> uses and syntax, including an overview of using it to query an API. <a href="https://opensource.com/downloads/linux-wget-cheat-sheet"><strong>Download the Linux <code>wget</code> cheat sheet here.</strong></a></p>
