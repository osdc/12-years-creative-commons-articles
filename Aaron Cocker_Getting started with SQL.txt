<p class="rtejustify">Building a database using SQL is simpler than most people think. In fact, you don't even need to be an experienced programmer to use SQL to create a database. In this article, I'll explain how to create a simple relational database management system (RDMS) using MySQL 5.6. Before I get started, I want to quickly thank <a href="http://sqlfiddle.com" target="_blank">SQL Fiddle</a>, which I used to run my script. It provides a useful sandbox for testing simple scripts.</p>
<p class="rtejustify">
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Programming and development</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ&amp;src=programming_resource_menu4">Red Hat Developers Blog</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Programming cheat sheets</a></li>
<li><a href="https://www.redhat.com/en/services/training/learning-subscription?intcmp=7016000000127cYAAQ">Try for free: Red Hat Learning Subscription</a></li>
<li><a href="https://opensource.com/downloads/bash-programming-guide?intcmp=7016000000127cYAAQ">eBook: An introduction to programming with Bash</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/bash-shell-cheat-sheet?intcmp=7016000000127cYAAQ">Bash Shell Scripting Cheat Sheet</a></li>
<li><a href="https://developers.redhat.com/e-books/modernizing-enterprise-java?intcmp=7016000000127cYAAQ">eBook: Modernizing Enterprise Java</a></li>
</ul></div>
</div>
</div>
</div>
<p>In this tutorial, I'll build a database that uses the simple schema shown in the entity relationship diagram (ERD) below. The database lists students and the course each is studying. I used two entities (i.e., tables) to keep things simple, with only a single relationship and dependency. The entities are called <code>dbo_students</code> and <code>dbo_courses</code>.</p>
<p class="rtejustify">
<article class="align-center media media--type-image media--view-mode-full" title="entity relationship diagram "><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/erd.png" width="700" height="287" alt="entity relationship diagram" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p class="rtejustify">The multiplicity of the database is <em>1-to-many</em>, as each course can contain many students, but each student can study only one course.</p>
<p class="rtejustify">A quick note on terminology:</p>
<ol><li class="rtejustify">A table is called an <em>entity</em>.</li>
<li class="rtejustify">A field is called an <em>attribute</em>.</li>
<li class="rtejustify">A record is called a <em>tuple</em>.</li>
<li class="rtejustify">The script used to construct the database is called a <em>schema</em>.</li>
</ol><h2 class="rtejustify">Constructing the schema</h2>
<p class="rtejustify">To construct the database, use the <code>CREATE TABLE &lt;table name&gt;</code> command, then define each field name and data type. This database uses <code>VARCHAR(n)</code> (string) and <code>INT(n)</code> (integer), where <em>n</em> refers to the number of values that can be stored. For example <code>INT(2)</code> could be 01.</p>
<p class="rtejustify">This is the code used to create the two tables:</p>
<pre>
<code class="language-sql">CREATE TABLE dbo_students
(
  student_id INT(2) AUTO_INCREMENT NOT NULL,
  student_name VARCHAR(50),
  course_studied INT(2),
  PRIMARY KEY (student_id)
);

CREATE TABLE dbo_courses
(
  course_id INT(2) AUTO_INCREMENT NOT NULL,
  course_name VARCHAR(30),
  PRIMARY KEY (course_id)
);
</code></pre><p class="rtejustify"><code>NOT NULL</code> means that the field cannot be empty, and <code>AUTO_INCREMENT</code> means that when a new tuple is added, the ID number will be auto-generated with 1 added to the previously stored ID number in order to enforce referential integrity across entities. <code>PRIMARY KEY</code> is the unique identifier attribute for each table. This means each tuple has its own distinct <em>identity</em>.</p>
<h2 class="rtejustify">Relationships as a constraint</h2>
<p class="rtejustify">As it stands, the two tables exist on their own with no connections or relationships. To connect them, a <em>foreign key</em> must be identified. In <code>dbo_students</code>, the foreign key is <code>course_studied</code>, the source of which is within <code>dbo_courses</code>, meaning that the field is <em>referenced</em>. The specific command within SQL is called a <code>CONSTRAINT</code>, and this relationship will be added using another command called <code>ALTER TABLE</code>, which allows tables to be edited even after the schema has been constructed.</p>
<p class="rtejustify">The following code adds the relationship to the database construction script:</p>
<pre>
<code class="language-sql">ALTER TABLE dbo_students
ADD CONSTRAINT FK_course_studied
FOREIGN KEY (course_studied) REFERENCES dbo_courses(course_id);
</code></pre><p class="rtejustify">Using the <code>CONSTRAINT</code> command is not actually necessary, but it's good practice because it means the constraint can be named and it makes maintenance easier. Now that the database is complete, it's time to add some data.</p>
<h2 class="rtejustify">Adding data to the database</h2>
<p class="rtejustify"><code>INSERT INTO &lt;table name&gt;</code> is the command used to directly choose which attributes (i.e., fields) data is added to. The entity name is defined first, then the attributes. Underneath this command is the data that will be added to that entity, creating a tuple. If <code>NOT NULL</code> has been specified, it means that the attribute cannot be left blank. The following code shows how to add records to the table:</p>
<pre>
<code class="language-sql">INSERT INTO dbo_courses(course_id,course_name)
VALUES(001,'Software Engineering');
INSERT INTO dbo_courses(course_id,course_name)
VALUES(002,'Computer Science');
INSERT INTO dbo_courses(course_id,course_name)
VALUES(003,'Computing');

INSERT INTO dbo_students(student_id,student_name,course_studied)
VALUES(001,'student1',001);
INSERT INTO dbo_students(student_id,student_name,course_studied)
VALUES(002,'student2',002);
INSERT INTO dbo_students(student_id,student_name,course_studied)
VALUES(003,'student3',002);
INSERT INTO dbo_students(student_id,student_name,course_studied)
VALUES(004,'student4',003);
</code></pre><p class="rtejustify">Now that the database schema is complete and data is added, it's time to run queries on the database.</p>
<h2 class="rtejustify">Queries</h2>
<p class="rtejustify">Queries follow a set structure using these commands:</p>
<pre>
<code class="language-sql">SELECT &lt;attributes&gt;
FROM &lt;entity&gt;
WHERE &lt;condition&gt;
</code></pre><p class="rtejustify">To display all records within the <code>dbo_courses</code> entity and display the course code and course name, use an asterisk. This is a wildcard that eliminates the need to type all attribute names. (Its use is not recommended in production databases.) The code for this query is:</p>
<pre>
<code class="language-sql">SELECT *
FROM dbo_courses
</code></pre><p class="rtejustify">The output of this query shows all tuples in the table, so all available courses can be displayed:</p>
<pre>
<code class="language-sql">| course_id |          course_name |
|-----------|----------------------|
|         1 | Software Engineering |
|         2 |     Computer Science |
|         3 |            Computing |
</code></pre><p class="rtejustify">In a future article, I'll explain more complicated queries using one of the three types of joins: Inner, Outer, or Cross.</p>
<p class="rtejustify">Here is the completed script:</p>
<pre>
<code class="language-sql">CREATE TABLE dbo_students
(
  student_id INT(2) AUTO_INCREMENT NOT NULL,
  student_name VARCHAR(50),
  course_studied INT(2),
  PRIMARY KEY (student_id)
);

CREATE TABLE dbo_courses
(
  course_id INT(2) AUTO_INCREMENT NOT NULL,
  course_name VARCHAR(30),
  PRIMARY KEY (course_id)
);

ALTER TABLE dbo_students
ADD CONSTRAINT FK_course_studied
FOREIGN KEY (course_studied) REFERENCES dbo_courses(course_id);

INSERT INTO dbo_courses(course_id,course_name)
VALUES(001,'Software Engineering');
INSERT INTO dbo_courses(course_id,course_name)
VALUES(002,'Computer Science');
INSERT INTO dbo_courses(course_id,course_name)
VALUES(003,'Computing');

INSERT INTO dbo_students(student_id,student_name,course_studied)
VALUES(001,'student1',001);
INSERT INTO dbo_students(student_id,student_name,course_studied)
VALUES(002,'student2',002);
INSERT INTO dbo_students(student_id,student_name,course_studied)
VALUES(003,'student3',002);
INSERT INTO dbo_students(student_id,student_name,course_studied)
VALUES(004,'student4',003);

SELECT *
FROM dbo_courses
</code></pre><h2 class="rtejustify">Learning more</h2>
<p class="rtejustify">SQL isn't difficult; I think it is simpler than programming, and the language is universal to different database systems. Note that <code>dbo.&lt;entity&gt;</code> is not a required entity-naming convention; I used it simply because it is the standard in Microsoft SQL Server.</p>
<p class="rtejustify">If you'd like to learn more, the best guide this side of the internet is <a href="https://www.w3schools.com/sql/default.asp" target="_blank">W3Schools.com</a>'s comprehensive guide to SQL for all database platforms.</p>
<p class="rtejustify">Please feel free to play around with my database. Also, if you have suggestions or questions, please respond in the comments.</p>
