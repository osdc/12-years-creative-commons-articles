<p>For many of us 2016 flew by, and we didn't complete all our New Year's resolutions or mark everything off our "2016 To Do" lists. I didn't have nearly enough time to play with the Raspberry Pi this year, and my list of projects I want to do keeps growing. In this article I've rounded up 8 recent Raspberry Pi projects that I haven't made yet, but that made it onto my "2017 To Do" list.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on Raspberry Pi</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/resources/what-raspberry-pi?src=raspberry_pi_resource_menu1&amp;intcmp=701f2000000h4RcAAI">What is Raspberry Pi?</a></li>
<li><a href="https://opensource.com/downloads/raspberry-pi-guide?src=raspberry_pi_resource_menu2&amp;intcmp=701f2000000h4RcAAI">eBook: Guide to Raspberry Pi</a></li>
<li><a href="https://opensource.com/downloads/getting-started-raspberry-pi-cheat-sheet?src=raspberry_pi_resource_menu3&amp;intcmp=701f2000000h4RcAAI">Getting started with Raspberry Pi cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/kubernetes-raspberry-pi?src=raspberry_pi_resource_menu6&amp;intcmp=701f2000000h4RcAAI">eBook: Running Kubernetes on your Raspberry Pi</a></li>
<li><a href="https://www.redhat.com/en/resources/data-intensive-applications-hybrid-cloud-blueprint-detail?src=raspberry_pi_resource_menu7&amp;intcmp=701f2000000h4RcAAI">Whitepaper: Data-intensive intelligent applications in a hybrid cloud blueprint</a></li>
<li><a href="https://www.redhat.com/en/topics/edge-computing?src=raspberry_pi_resource_menu5&amp;intcmp=701f2000000h4RcAAI">Understanding edge computing</a></li>
<li><a href="https://opensource.com/tags/raspberry-pi?src=raspberry_pi_resource_menu4&amp;intcmp=701f2000000h4RcAAI">Our latest on Raspberry Pi </a></li>
</ul></div>
</div>
</div>
</div>

<h2>Recent Raspberry Pi projects to try</h2>
<h3>1. Magic Mirror<sup>2</sup></h3>
<p>Magic Mirror<sup>2</sup> was created by Michael Teeuw. This project allows you to convert your hall or bathroom mirror into a personal assistant. Both the Raspberry Pi 2 and 3 are supported by this project, and you can count on a rapidly growing community to help you build your own Magic Mirror<sup>2</sup>. (License: MIT)</p>
<ul><li>Michael Teeuw's Twitter: <a href="https://twitter.com/MichMich" target="_blank">@MichMich</a></li>
<li><a href="https://magicmirror.builders/" target="_blank">Magic Mirror<sup>2 </sup>site</a></li>
<li><a href="http://michaelteeuw.nl/tagged/magicmirror" target="_blank">Michael Teeuw, Xony Labs</a></li>
<li><a href="https://github.com/MichMich/MagicMirror" target="_blank">Michael Teeux's Magic Mirror<sup>2</sup> at GitHub</a></li>
</ul><p><iframe allowfullscreen="" frameborder="0" height="293" mozallowfullscreen="" src="https://player.vimeo.com/video/171152845" webkitallowfullscreen="" width="520"></iframe></p>
<h3>2. Kodi Media Center</h3>
<p>Kodi Media Center is an open source media player that runs in several different platforms. Formerly known as XBMC Media Center, it is one of the most popular projects for running on a Raspberry Pi. (License: GPLv2)</p>
<ul><li><a href="http://kodi.wiki/view/HOW-TO:Install_Kodi_on_Raspberry_Pi" target="_blank">How to install Kodi on Raspberry Pi</a></li>
<li><u><a href="http://kodi.wiki/view/raspberry_Pi" target="_blank">Official Kodi Wiki Raspberry Pi page</a> </u></li>
<li><a href="https://mediaexperience.com/raspberry-pi-xbmc-with-raspbmc/" target="_blank">Tuukka's Raspberry Pi Kodi installation tutorial</a></li>
</ul><p><iframe allowfullscreen="" frameborder="0" height="293" src="https://www.youtube.com/embed/4oMongjNslg?rel=0" width="520"></iframe></p>
<h3>3. Raspberry Pi Weather Station</h3>
<p>The Raspberry Pi Weather Station is a project created by Peter Kodermac that provides all the information to build your own weather station. Peter gives you the code and recommends the best type of sensors for this scientific project.</p>
<ul><li>Peter Kodermac's email: <a href="mailto:peter@raspberryweather.com">peter@raspberryweather.com</a></li>
<li><a href="http://www.raspberryweather.com/" target="_blank">Raspberry Pi Weather Station website</a></li>
<li><a href="https://github.com/peterkodermac/Raspberry-Weather" target="_blank">Raspberry Pi Weather Station GitHub</a></li>
</ul><h3>4. Fedora 25 on a Pi</h3>
<p>The Fedora 25 project is an open source operating system known for its simplicity, which makes it an excellent product for those new to open source development. <a href="https://fedoramagazine.org/fedora-25-released/" target="_blank">Fedora 25</a> was recently released and is the first Fedora release with official support for the Raspberry Pi. Fedora 25 supports the Raspberry Pi B+ version 2 and 3. Non-official spins of the Fedora distribution have been available in the past.</p>
<ul><li><a href="https://fedoraproject.org/wiki/Raspberry_Pi" target="_blank">Fedora Wiki's Raspberry Pi page</a></li>
<li><a href="http://pidora.ca/" target="_blank">Pidora Fedora remix</a></li>
</ul><p>(I plan to review Fedora 25 on the Raspberry Pi in an upcoming article on Opensource.com.)</p>
<h3>5. Pi Hole server-level ad blocking</h3>
<p>Raspberry Pi is useful for creating powerhouse ad blocking on a network. Instead of blocking ads at the browser level and having to manage the different extensions, why not block ads for your entire network? That's what Pi Hole will let you to do. It claims it blocks more than 100,000 ad-serving domains from your network. This may be the first project I try when I have time. (License<b>:</b> GPLv2)</p>
<ul><li><a href="https://pi-hole.net/" target="_blank">Pi Hole site</a></li>
<li><a href="https://github.com/pi-hole/pi-hole" target="_blank">Pi Hole GitHub</a></li>
</ul><p><iframe allowfullscreen="" frameborder="0" height="293" src="https://www.youtube.com/embed/9Eti3xibiho?rel=0" width="520"></iframe></p>
<h3>6. RetroPi retro gaming machine</h3>
<p>The RetroPi retro gaming machine is an open source project that transforms your Raspberry Pi into a time machine, allowing you to enjoy some of good old games of the '70s, '80s, '90s and 2000s. (License: GPLv3)</p>
<ul><li><a href="https://retropie.org.uk/" target="_blank">RetroPi Gaming page</a></li>
<li><a href="https://github.com/RetroPie/RetroPie-Setup" target="_blank">RetroPi setup GitHub</a></li>
<li><a href="https://github.com/RetroPie/RetroPie-Setup/wiki/Supported-Systems" target="_blank">A list of supported emulators that run under RetroPie</a></li>
</ul><p><iframe allowfullscreen="" frameborder="0" height="293" src="https://www.youtube.com/embed/xvYX_7iRRI0?rel=0" width="520"></iframe></p>
<h3>7. Security system</h3>
<p>A comprehensive tutorial by developer Max Williams explains how to build a security system with a Raspberry Pi, including a camera, a motion sensor, and notifications via telegram. (License: GPLv2)</p>
<ul><li><a href="https://www.hackster.io/FutureSharks/raspberry-pi-security-system-with-motion-detection-camera-bed172" target="_blank">Motion-sensing Raspberry Pi security system page</a></li>
<li><a href="https://github.com/FutureSharks/rpi-security" target="_blank">Raspberry Pi Security System GitHub</a></li>
</ul><h3>8. RTAndroid (Real-Time Android)</h3>
<p>Running Android on a Raspberry Pi is being explored in earnest, most notably at the Aachen University in Germany. The project is called RTAndroid (Real-Time Android) and, although still a work in progress, it is interesting and intriguing.</p>
<ul><li><a href="https://www.youtube.com/watch?v=Df-bMWONIYk" target="_blank">Raspberry Pi 3 Android 7.0 with Google Play and Android Root (video)</a></li>
<li><a href="https://git.embedded.rwth-aachen.de/rtandroid/downloads/raspberry-pi/" target="_blank">RTAndroid Raspberry Pi installation tutorial</a></li>
</ul><p><iframe allowfullscreen="" frameborder="0" height="293" src="https://www.youtube.com/embed/Df-bMWONIYk?rel=0" width="520"></iframe></p>
