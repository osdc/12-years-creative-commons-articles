<p>Playing adventure games has always been a big part of my experience with computers. From the earliest text-based adventure games to 2D pixel art, full-motion video, and 3D games, the adventure game genre has provided me with a lot of fond memories.</p>
<p>Sometimes I want to revisit those old games, but many were released before Linux was even a thing, so how do I go about replaying those games? I use <a href="https://www.scummvm.org/" target="_blank">ScummVM</a>, which is honestly one of my favorite open source projects.</p>
<h2 id="what-is-scummvm">What is ScummVM</h2>
<p><article class="align-center media media--type-image media--view-mode-full" title="ScummVM"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/scummvm.png" width="675" height="380" alt="ScummVM" title="ScummVM" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Joshua Allen Holm, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>ScummVM is a program designed to play old adventure games on modern hardware. Originally designed to run games developed using LucasArt's Script Creation Utility for Maniac Mansion (SCUMM), ScummVM now supports many different game engines. It can play almost all of the classic Sierra On-Line and LucasArts adventure games as well as a wide selection of adventure games from other publishers. ScummVM does not support <em>every</em> adventure game (yet), but it can be used to play hundreds of them. ScummVM is available for multiple platforms, including Windows, macOS, Linux, Android, iOS, and several game consoles.</p>
<h2 id="why-use-scummvm">Why use ScummVM</h2>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Great Content</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=7016000000127cYAAQ">Free online course: RHEL technical overview</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=7016000000127cYAAQ">Learn Advanced Linux Commands</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Download Cheat Sheets</a></li>
<li><a href="https://opensource.com/alternatives?intcmp=7016000000127cYAAQ">Find an Open Source Alternative</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=7016000000127cYAAQ">Read Top Linux Content</a></li>
<li><a href="https://opensource.com/resources?intcmp=7016000000127cYAAQ">Check out open source resources</a></li>
</ul></div>
</div>
</div>
</div>
<p>There are plenty of ways to play old games on modern hardware, but they tend to be more complicated than using ScummVM. <a href="https://www.dosbox.com/" target="_blank">DOSBox</a> can be used to play DOS games, but it requires tweaking to get the settings right so that the game plays at the right speed. Windows games can be played using <a href="https://www.winehq.org/" target="_blank">WINE</a>, but that requires both the game and the game's installer to be compatible with WINE.</p>
<p>Even if a game runs under WINE, some games still do not work well on modern hardware because the hardware is too fast. One example of this is a puzzle in King's Quest VII that involves taking a lit firecracker somewhere. On modern hardware, the firecracker explodes way too quickly, which makes it impossible to get to the right location without the character dying multiple times.</p>
<p>ScummVM eliminates many of the problems present in other methods for playing retro adventure games. If ScummVM supports a game, it is straightforward to configure and play. In most cases, copying the game files from the original game discs to a directory and adding that directory in ScummVM is all that is needed to play the game. For games that came on multiple discs, it might be necessary to rename some files to avoid file name conflicts. The instructions for what data files are needed and any renaming instructions are documented on the ScummVM Wiki page for <a href="https://wiki.scummvm.org/index.php?title=Category:Supported_Games" target="_blank">each supported game</a>.</p>
<p>One of the wonderful things about ScummVM is how each new release adds support for more games. ScummVM 2.2.0 added support for a dozen interactive fiction interpreters, which means ScummVM can now play hundreds of text-based adventure games. The development branch of ScummVM, which should become version 2.3.0 soon, integrates <a href="https://www.residualvm.org/" target="_blank">ResidualVM</a>'s support for 3D adventure games, so now ScummVM can be used to play Grim Fandango, Myst III: Exile, and The Longest Journey. The development branch also recently added support for games created using <a href="https://www.adventuregamestudio.co.uk/" target="_blank">Adventure Game Studio</a>, which adds hundreds, possibly thousands, of games to ScummVM's repertoire.</p>
<h2 id="how-to-install-scummvm">How to install ScummVM</h2>
<p>If you want to install ScummVM from your Linux distribution's repositories, the process is very simple. You just need to run one command. However, your distribution might offer an older release of ScummVM that does not support as many games as the latest release, so do keep that in mind.</p>
<p><strong>Install ScummVM on Debian/Ubuntu:</strong></p>
<pre><code class="language-bash">sudo apt install scummvm</code></pre><p><strong>Install ScummVM on Fedora:</strong></p>
<pre><code class="language-bash">sudo dnf install scummvm</code></pre><h3 id="install-scummvm-using-flatpak-or-snap">Install ScummVM using Flatpak or Snap</h3>
<p>ScummVM is also available as a Flatpak and as a Snap. If you use one of those options, you can use one of the following commands to install the relevant version, which should always be the latest release of ScummVM:</p>
<pre><code class="language-bash">flatpak install flathub org.scummvm.ScummVM</code></pre><p>or</p>
<pre><code class="language-bash">snap install scummvm</code></pre><h3 id="compile-the-development-branch-of-scummvm">Compile the development branch of ScummVM</h3>
<p>If you want to try the latest and greatest features in the not-yet-stable development branch of ScummVM, you can do so by compiling ScummVM from the source code. Do note that the development branch is constantly changing, so things might not always work correctly. If you are still interested in trying out the development branch, follow the instructions below.</p>
<p>To start, you will need the required development tools and libraries for your distribution, which are listed on the <a href="https://wiki.scummvm.org/index.php/Compiling_ScummVM/GCC" target="_blank">Compiling ScummVM/GCC page</a> on the ScummVM Wiki.</p>
<p>Once you have the prerequisites installed, run the following commands:</p>
<pre><code class="language-bash">git clone https://github.com/scummvm/scummvm.git

cd scummvm

./configure

make

sudo make install</code></pre><h2 id="add-games-to-scummvm">Add games to ScummVM</h2>
<p>Adding games to ScummVM is the last thing you need to do before playing. If you do not have any supported adventure games in your collection, you can download 11 wonderful games from the <a href="https://www.scummvm.org/games/" target="_blank">ScummVM Games page</a>. You can also purchase many of the games supported by ScummVM from <a href="https://www.gog.com/" target="_blank">GOG.com</a>. If you purchase a game from GOG.com and need to extract the game files from the GOG download, you can use the <a href="https://constexpr.org/innoextract/" target="_blank">innoextract</a> utility.</p>
<p>Most games need to be in their own directory (the only exceptions to this are games that consist of a single data file), so it is best to begin by creating a directory to store your ScummVM games. You can do this using the command line or a graphical file manager. Where you store your games does not matter (except in the case of the ScummVM Flatpak, which is a sandbox and requires the games to be stored in the <code>~/Documents</code> directory). After creating this directory, place the data files for each game in their own subdirectories.</p>
<p>Once the files are copied to where you want them, run ScummVM and add the game to the collection by clicking <strong>Add Game…</strong>, selecting the appropriate directory in the file-picker dialog box that opens, and clicking <strong>Choose</strong>. If ScummVM properly detects the game, it will open its settings options. You can select advanced configuration options from the various tabs if you want (which can also be changed later by using the <strong>Edit Game…</strong> button), or you can just click <strong>OK</strong> to add the game with the default options. If the game is not detected, check the <a href="https://wiki.scummvm.org/index.php/Category:Supported_Games" target="_blank">Supported Games pages</a> on the ScummVM Wiki for details about special instructions that might be needed for a particular game's data files.</p>
<p>The only thing left to do now is select the game in ScummVM's list of games, click on <strong>Start</strong>, and enjoy replaying an old favorite or experiencing a classic adventure game for the first time.</p>
