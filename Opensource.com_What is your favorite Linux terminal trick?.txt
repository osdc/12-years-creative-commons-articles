<p>Do you remember the first time you watched somebody who <em>really </em>knew their way around the Linux terminal hard at work?</p>
<p>It may have seemed like magic to you. Or, at the very least, it seemed like something out of the movie <em>Hackers</em>.<br /></p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>The Linux Terminal</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/life/17/10/top-terminal-emulators?intcmp=7016000000127cYAAQ">Top 7 terminal emulators for Linux</a></li>
<li><a href="https://opensource.com/article/17/2/command-line-tools-data-analysis-linux?intcmp=7016000000127cYAAQ">10 command-line tools for data analysis in Linux</a></li>
<li><a href="https://opensource.com/downloads/advanced-ssh-cheat-sheet?intcmp=7016000000127cYAAQ">Download Now: SSH cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=7016000000127cYAAQ">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://opensource.com/tags/command-line?intcmp=7016000000127cYAAQ">Linux command line tutorials</a></li>
</ul></div>
</div>
</div>
</div>

<p>Of course, in reality, no one is sitting around typing in commands at sixty words per minute, watching screens of output fly by as they give their machine further instructions in a near constant rapidity. But when you get in the zone, sometimes, if only for a few minutes, it can feel that way.</p>
<p>What's your favorite trick for terminal productivity? Maybe it's a simple alias that you set up for a long string of options on a command you use frequently. Maybe it's a collection of short scripts you use to automate the boring part of your workflow. Perhaps it's your mastery of a terminal multiplexer like <strong>screen</strong> or <strong>tmux</strong>. Or maybe your memorization of all the Bash keyboard shortcuts is what finally made you feel like a <a href="https://opensource.com/article/18/2/command-line-heroes-original-podcast">command line hero</a>.</p>
<p>Whatever your favorite trick, take a moment in the spirit of open source and share it with the community. What do you wish you knew when you were starting out at the terminal, and why? Let us know in the comments below.</p>
