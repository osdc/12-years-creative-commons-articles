<p>As the CEO and co-founder of <a href="https://profian.com/" target="blank">Profian</a>, a start-up security company, I've been part of our effort to hire developers to work on <a href="https://enarx.dev/" target="blank">Enarx</a>, a security project that deals with confidential computing, written almost exclusively in <a href="https://opensource.com/article/21/3/rust-programmer">Rust</a> (with a bit of Assembly). Profian has now found all the people it was looking for in this search, with a couple of developers due to start in the next few weeks. However, new contributors are absolutely welcome to Enarx, and if things continue to go well, the company will definitely want to hire more folks in the future.</p>
<p>Hiring people is not easy, and Profian had a set of specialized requirements that made the task even more difficult. I thought it would be useful and interesting for the community to share how we approached the problem.</p>
<h2>What were we looking for?</h2>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More open source career advice</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7013a000002DRawAAG">Open source cheat sheets</a></li>
<li><a href="https://developers.redhat.com/learn/linux-starter-kit-for-developers/?intcmp=7013a000002DRawAAG">Linux starter kit for developers</a></li>
<li><a href="https://www.redhat.com/sysadmin/questions-for-employer?intcmp=7013a000002DRawAAG">7 questions sysadmins should ask a potential employer before taking a job</a></li>
<li><a href="https://www.redhat.com/architect/?intcmp=7013a000002DRawAAG">Resources for IT artchitects</a></li>
<li><a href="https://enterprisersproject.com/article/2019/3/article-cheat-sheet?intcmp=7013a000002DRawAAG">Cheat sheet: IT job interviews</a></li>
</ul></div>
</div>
</div>
</div>
<p>These are the specialized requirements I'm talking about:</p>
<ul><li>
<p><b>Systems programming:</b> Profian mainly needs people who are happy programming at the systems layer. This is pretty far down the stack, with lots of interactions directly with hardware or the OS. To create client-server pieces, for instance, we have to write quite a lot of the protocols, manage the crypto, and so forth, and the tools for this aren't all very mature (see "Rust" below).</p>
</li>
<li>
<p><b>Rust:</b> Almost all of the project is written in Rust, and what isn't is written in Assembly language (currently exclusively x86, though that may change as we add more platforms). Rust is new, cool, and exciting, but it's still quite young, and some areas don't have all the support you might like or aren't as mature as you'd hope—everything from cryptography through multithreading libraries and compiler/build infrastructure.</p>
</li>
<li>
<p><b>Distributed team:</b> Profian is building a team of folks where we can find them. Profian has developers in Germany, Finland, the Netherlands, North Carolina (US), Massachusetts (US), Virginia (US), and Georgia (US). I'm in the United Kingdom, our community manager is in Brazil, and we have interns in India and Nigeria. We knew from the beginning that we wouldn't have everyone in one place, and this required people who would be able to communicate and collaborate with people via video, chat, and (at worst) email.</p>
</li>
<li>
<p><b>Security:</b> Enarx is a security project. Although we weren't specifically looking for security experts, we need people who can think and work with security top of mind and design and write code that is applicable and appropriate for the environment.</p>
</li>
<li>
<p><b>Git:</b> All of our code is stored in git (mainly <a href="https://github.com/enarx/" target="blank">GitHub</a>, with a bit of GitLab thrown in). so much of our interaction around code revolves around git that anybody joining us would need to be very comfortable using it as a standard tool in their day-to-day work.</p>
</li>
<li>
<p><b>Open source:</b> Open source isn't just a licence; it's a mindset and, equally important, a way of collaborating. A great deal of open source software is created by people who aren't geographically co-located and who might not even see themselves as a team. We needed to know that the people we hired, while gelling as a close team within the company, would be able to collaborate with people outside the organisation and embrace Profian's "open by default" culture, not just for code, but for discussions, communications, and documentation.</p>
</li>
</ul><h2>How did we find them?</h2>
<p>As I've mentioned elsewhere, <a href="https://aliceevebob.com/2021/11/09/recruiting-is-hard/" target="blank">recruiting is hard</a>. Profian used a variety of means to find candidates, with varying levels of success:</p>
<ul><li>LinkedIn job adverts</li>
<li>LinkedIn searches</li>
<li>Language-specific discussion boards and hiring boards (e.g., Reddit)</li>
<li>An external recruiter (shout out to Gerald at <a href="https://www.interstem.co.uk/">Interstem</a>)</li>
<li>Word-of-mouth/personal recommendations</li>
</ul><p>It's difficult to judge between these sources in terms of quality, but without an external recruiter, we'd certainly have struggled with quantity (and we had some great candidates from that pathway, too).</p>
<h2>How did we select them?</h2>
<p>We needed to measure all of the candidates against all of the requirements noted above, but not all of them were equal. For instance, although we were keen to hire Rust programmers, someone with strong C/C++ skills at the systems level would be able to pick up Rust quickly enough to be useful. On the other hand, a good knowledge of using git was absolutely vital, as we couldn't spend time working with new team members to bring them up to speed on our way of working.</p>
<p>A strong open source background was, possibly surprisingly, not a requirement, but the mindset to work in that sort of model was, and anyone with a history of open source involvement is likely to have a good knowledge of git. The same goes for the ability to work in a distributed team: So much of open source is distributed that involvement in almost any open source community was a positive indicator. Security, we decided, was a "nice-to-have" qualification.</p>
<p>We wanted to keep the process simple and quick. Profian doesn't have a dedicated HR or People function, and we're busy trying to get code written. This is what we ended up with (with slight variations), and we tried to complete it within 1-2 weeks:</p>
<ol><li>Initial CV/resume/GitHub/GitLab/LinkedIn review to decide whether to interview</li>
<li>30-40 minute discussion with me as CEO, to find out if they might be a good cultural fit, to give them a chance to find out about us, and to get an idea if they were as technically adept as they appeared in Step 1</li>
<li>Deep dive technical discussion led by Nathaniel, usually with me there</li>
<li>Chat with other members of the team</li>
<li>Coding exercise</li>
<li>Quick decision (usually within 24 hours)</li>
</ol><p>The coding exercise was key, but we decided against the usual approach. Our view was that a pure "algorithm coding" exercise beloved by many tech companies was pretty much useless for what we wanted: to find out whether a candidate could quickly understand a piece of code, fix some problems, and work with the team to do so. We created a GitHub repository with some almost-working Rust code in it (in fact, we ended up using two, with one for people a little higher up the stack), then instructed candidates to fix it, perform some git-related processes on it, and improve it slightly, adding tests along the way.</p>
<p>An essential part of the test was to get candidates to interact with the team via our chat room(s). We scheduled 15 minutes on a video call for setup and initial questions, two hours for the exercise ("open book" – as well as talking to the team, candidates were encouraged to use all resources available to them on the Internet), followed by a 30-minute wrap-up session where the team could ask questions, and the candidate could reflect on the task. This conversation, combined with the chat interactions during the exercise, allowed us to get an idea of how well the candidate was able to communicate with the team. Afterwards, the candidate would drop off the call, and we'd most often decide within 5-10 minutes whether we wanted to hire them.</p>
<p>This method generally worked very well. Some candidates struggled with the task, some didn't communicate well, some failed to do well with the git interactions – these were the people we didn't hire. It doesn't mean they're not good coders or might not be a good fit for the project or the company later on, but they didn't meet the criteria we need now. Of the developers we hired, the level of Rust experience and need for interaction with the team varied, but the level of git expertise and their reactions to our discussions afterwards were always sufficient for us to decide to take them.</p>
<h2>Reflections</h2>
<p>On the whole, I don't think we'd change a huge amount about the selection process—though I'm pretty sure we could do better with the search process. The route through to the coding exercise allowed us to filter out quite a few candidates, and the coding exercise did a great job of helping us pick the right people. Hopefully, everyone who's come through the process will be a great fit and produce great code (and tests and documentation and …) for the project. Time will tell!</p>
<hr /><p>This article originally appeared on <a href="https://aliceevebob.com/">Alice, Eve and Bob – a security blog</a> and is republished with permission.</p>
