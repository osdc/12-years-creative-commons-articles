<p>Whether I'm using a laptop my employer assigned to me or a workstation I built from vendor parts, I seem to have an endless capacity to forget my computer's specifications. One of the great things about Linux is its <code>/proc</code> filesystem, a dynamically populated virtual expression of the system's hardware. It's convenient when you want to see the specifics of your CPU (<code>cat /proc/cpuinfo</code>), uptime (<code>cat /proc/uptime</code>), a list of mounted filesystems (<code>ls -R /proc/fs/</code>), and so on.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Linux resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux commands cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Free online course: RHEL Technical Overview</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux networking cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">SELinux cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux common commands cheat sheet</a></li>
<li><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">What are Linux containers?</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Our latest Linux articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>Sometimes, though, it's nice to have everything you need (and what you don't know you need) all in one place for your perusal. The KDE Plasma desktop provides an application called Info Center (sometimes also called <a href="https://userbase.kde.org/KInfoCenter" target="_blank">KInfoCenter</a>), a place to help you know what, where, and how much you're running.</p>
<h2 id="installing-kinfocenter">Installing KInfoCenter</h2>
<p>If you're already running the <a href="https://opensource.com/article/19/12/linux-kde-plasma">KDE Plasma desktop</a>, then KInfoCenter is probably already installed. Otherwise, you can find the application in your distribution's software repository.</p>
<p>For example, on Fedora or CentOS Stream:</p>
<pre><code class="language-bash">$ sudo dnf install kinfocenter</code></pre><h2 id="system-information">System information</h2>
<p>When Info Center is launched, the default screen is the <strong>About System</strong> pane. This displays the versions of your Plasma desktop, KDE Frameworks, and Qt: all the technologies that work together to provide the desktop. It also displays the Linux kernel version and architecture and gives you a quick hardware overview, listing both your CPU and RAM.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="KInfoCenter's main display"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/kinfocenter-main.png" width="675" height="460" alt="KInfoCenter's main display" title="KInfoCenter's main display" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Seth Kenlon, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<h2 id="memory-and-resources">Memory and resources</h2>
<p>Maybe seeing the total RAM installed on your system isn't specific enough for you. In that case, you can open the <strong>Memory</strong> pane to see a detailed report about how your RAM is being used. This updates dynamically, so you can use it to monitor the effects an application or activity has on your system.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="KInfoCenter's Memory pane"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/kinfocenter-memory.png" width="675" height="459" alt="KInfoCenter's Memory pane" title="KInfoCenter's Memory pane" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Seth Kenlon, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>If you're on a laptop, <strong>Energy Information</strong> displays your power-saving settings. If you have file indexing active, you can view the status of the indexer in the <strong>File Indexer Monitor</strong> panel.</p>
<h2 id="devices">Devices</h2>
<p>The <strong>Device Information</strong> folder contains several panes you can access for details about the physical peripherals inside or connected to your computer. This covers <em>everything</em>, including USB devices, hard drives, processors, PCI slots, and more.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="KInfoCenter's Device Information pane"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/kinfocenter-peripherals.png" width="675" height="537" alt="KInfoCenter's Device Information pane" title="KInfoCenter's Device Information pane" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Seth Kenlon, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>This isn't just a broad overview, either. KInfoCenter gives you nearly everything there is to know about the components you're running. For hard drives, it provides a list of partitions, the SATA port the drive is connected to, the drive label or name you've given it, UUID, size, partition, the filesystem, whether it's mounted and where, and more. For the CPU, it provides the product name, vendor, number of cores (starting at 0), maximum clock speed, interrupt information, and supported instruction sets. The list goes on and on for every type of device you can think of.</p>
<h2 id="network-and-ip-address">Network and IP address</h2>
<p>Maybe you're tired of parsing the verbose output of <code>ip address show</code>. Maybe you're too lazy to create an alias for <code>ip address show | grep --only-matching "inet 10.*" | cut -f2 -d" "</code>. Whatever the reason, sometimes you want an easy way to get a machine's IP address. KInfoCenter is the answer because the <strong>Network Information</strong> panel contains its host's IP address. In fact, it lists both the active hardware-based IP addresses as well as active bridges for virtual machines.</p>
<p>It seems basic, but this simple KInfoCenter feature has saved me minutes of frustration when trying to obtain an IP address quickly over a support call so I could SSH into the machine in question and fix a problem. The network panel also provides information about <a href="https://opensource.com/article/21/4/share-files-linux-windows">Samba shares</a>, the open source file sharing service you can run locally to swap files between computers on your network easily.</p>
<h2 id="graphics">Graphics</h2>
<p>As if that's not enough, KInfoCenter also features a <strong>Graphical Information</strong> panel so you can get details about your graphics server, whether you're running Wayland or X11. You can get data on your display's dimensions, resolution (you may remember when 72 DPI was standard, but this panel assures you that you're running a more modern 92 DPI), bit depth, and more. It also provides information on OpenGL or Vulkan, including what card is being used to render graphics, what extensions are in use, what kernel module is installed, and so on.</p>
<h2 id="kinfocenter-more-like-klifesaver">KInfoCenter? More like KLifeSaver</h2>
<p>I regularly pin KInfoCenter to the KDE Kicker or create a shortcut to it on the desktop so that users I support can get there easily whenever they need to know their architecture, RAM, or IP address. It's the most friendly aggregation of system information I've seen on any operating system, much less on any Linux desktop. Install KInfoCenter today. You might not use it right away, but you'll need it someday, and when you do, you'll be glad you have it.</p>
