<p>Everybody's a critic, but today, we're asking you to evaluate yourself. How sharp are your programming skills?<br /></p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Programming and development</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ&amp;src=programming_resource_menu4">Red Hat Developers Blog</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Programming cheat sheets</a></li>
<li><a href="https://www.redhat.com/en/services/training/learning-subscription?intcmp=7016000000127cYAAQ">Try for free: Red Hat Learning Subscription</a></li>
<li><a href="https://opensource.com/downloads/bash-programming-guide?intcmp=7016000000127cYAAQ">eBook: An introduction to programming with Bash</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/bash-shell-cheat-sheet?intcmp=7016000000127cYAAQ">Bash Shell Scripting Cheat Sheet</a></li>
<li><a href="https://developers.redhat.com/e-books/modernizing-enterprise-java?intcmp=7016000000127cYAAQ">eBook: Modernizing Enterprise Java</a></li>
</ul></div>
</div>
</div>
</div>

<p>In the world of open source programming, you'll readily find people of all different skill levels. In fact, that's one of the best parts about it—beginners can gain valuable experience, while more seasoned developers have the opportunity to give back in the form of not only code, but mentorship as well. In a healthy project community, having contributors who span a spectrum of ability levels isn't a bug, it's a feature.</p>
<p>So today we're asking you: How would you rate yourself as a developer? Are you a rockstar (or a <a href="https://opensource.com/life/15/5/8-ways-developers-can-be-like-willie-nelson">Willie Nelson</a>)? Or are you just getting started on your path to greatness? Somewhere in between? Or not a coder at all?</p>
<p>Regardless of where you stand, let us know in our poll above, and then in the comments below, give us a little color about why you answered how you did. What makes a developer an expert? What was that moment when you knew you were no longer a beginner? And what life experiences helped you get to whatever point you are on your path today?</p>
