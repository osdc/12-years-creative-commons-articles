<p><strong>Happy Pi Day!</strong></p>
<p>Every year on March 14th, we geeks celebrate Pi Day. In the way we abbreviate dates—MMDD—March 14 is written 03/14, which numerically reminds us of 3.14, or the first three numbers of <a href="https://www.piday.org/million/" target="_blank">pi</a>. What many Americans don't realize is that virtually no other country in the world uses this <a href="https://en.wikipedia.org/wiki/Date_format_by_country" target="_blank">date format</a>, so Pi Day pretty much only works in the US, though it is celebrated globally.</p>
<p>Wherever you are in the world, let's celebrate the Raspberry Pi and wrap up this series by reviewing the topics we've covered in the past two weeks:</p>
<ul><li>Day 1: <a href="https://opensource.com/article/19/3/which-raspberry-pi-choose">Which Raspberry Pi should you choose?</a></li>
<li>Day 2: <a href="https://opensource.com/article/19/3/how-buy-raspberry-pi">How to buy a Raspberry Pi</a></li>
<li>Day 3: <a href="https://opensource.com/article/19/3/how-boot-new-raspberry-pi">How to boot up a new Raspberry Pi</a></li>
<li>Day 4: <a href="https://opensource.com/article/19/3/learn-linux-raspberry-pi">Learn Linux with the Raspberry Pi</a></li>
<li>Day 5: <a href="https://opensource.com/article/19/3/teach-kids-program-raspberry-pi">5 ways to teach kids to program with Raspberry Pi</a></li>
<li>Day 6: <a href="https://opensource.com/article/19/3/programming-languages-raspberry-pi">3 popular programming languages you can learn with Raspberry Pi</a></li>
<li>Day 7: <a href="https://opensource.com/article/19/3/how-raspberry-pi-update">How to keep your Raspberry Pi updated</a></li>
<li>Day 8: <a href="https://opensource.com/article/19/3/raspberry-pi-entertainment">How to use your Raspberry Pi for entertainment</a></li>
<li>Day 9: <a href="https://opensource.com/article/19/3/play-games-raspberry-pi">Play games on the Raspberry Pi</a></li>
<li>Day 10: <a href="https://opensource.com/article/19/3/gpio-pins-raspberry-pi">Let's get physical: How to use GPIO pins on the Raspberry Pi</a></li>
<li>Day 11: <a href="https://opensource.com/article/19/3/learn-about-computer-security-raspberry-pi">Learn about computer security with the Raspberry Pi</a></li>
<li>Day 12: <a href="https://opensource.com/article/19/3/do-math-raspberry-pi">Do advanced math with Mathematica on the Raspberry Pi</a></li>
<li>Day 13: <a href="https://opensource.com/article/19/3/contribute-raspberry-pi-community">Contribute to the Raspberry Pi community</a></li>
</ul><p><article class="align-center media media--type-image media--view-mode-full" title="Pi Day illustration"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/raspberrypi_14_piday.jpg" width="650" height="496" alt="Pi Day illustration" title="Pi Day illustration" /></div>
      
  </article></p>
<ul></ul><p>I'll end this series by thanking everyone who was brave enough to follow along and especially those who learned something from it during these past 14 days! I also want to encourage everyone to keep expanding their knowledge about the Raspberry Pi and all of the open (and closed) source technology that has been built around it.</p>
<p>I also encourage you to learn about other cultures, philosophies, religions, and worldviews. What makes us human is this amazing (and sometimes amusing) ability that we have to adapt not only to external environmental circumstances—but also intellectual ones.</p>
<p>No matter what you do, keep learning!</p>
