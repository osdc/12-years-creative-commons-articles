<p>Interested in keeping track of what's happening in the open source cloud? Opensource.com is your source for news in <a href="https://opensource.com/resources/what-is-openstack" target="_blank" title="What is OpenStack?">OpenStack</a>, the open source cloud infrastructure project.</p>
<p><!--break--></p>
<h3>OpenStack around the web</h3>
<p>There's a lot of interesting stuff being written about OpenStack. Here's a sampling:</p>
<ul><li><a href="http://superuser.openstack.org/articles/what-you-need-to-know-about-astara" target="_blank">What you need to know about Astara</a>: Introducting OpenStack's newest project.</li>
<li><a href="http://itknowledgeexchange.techtarget.com/storage-soup/openstack-manila-project-leader-previews-liberty-mitaka-releases/" target="_blank" style="font-size: 13.008px; line-height: 1.538em; background-color: transparent;">OpenStack Manila project leader previews Liberty, Mitaka releases</a>: Another look at what's coming up.</li>
<li><a href="http://www.lightreading.com/data-center/cloud-strategies/openstack-and-ovs-from-love-hate-relationship-to-match-made-in-heaven/a/d-id/718578" target="_blank">OpenStack &amp; OVS</a>: From love-hate relationship to match made in heaven.</li>
<li><a href="http://www.techrepublic.com/article/no-business-is-too-small-for-openstack/" target="_blank">No business is too small for OpenStack</a>: Why small organizations should not necessarily avoid the platform.</li>
<li><a href="http://www.linuxinsider.com/story/At-the-Heart-of-OpenStack-Evolution-82584.html" target="_blank">At the heart of OpenStack evolution</a>: As it matures, OpenStack's parallel to Linux is clearer.</li>
<li><a href="http://www.openstack.org/blog/2015/10/technical-committee-highlights-october-7-2015/" target="_blank">Technical Committee highlights</a>: What's happening in the lead up to the summit?</li>
<li><a href="http://superuser.openstack.org/articles/an-openstack-security-primer" target="_blank" style="font-size: 13.008px; line-height: 1.538em; background-color: transparent;">An OpenStack security primer</a>: Meet the troubleshooters and firefighters of the OpenStack security project.</li>
</ul><h3>OpenStack discussions</h3>
<p>Here's a sample of some of the most active discussions this week on the OpenStack developers' listserv. For a more in-depth look, why not <a href="http://lists.openstack.org/pipermail/openstack-dev/" target="_blank" title="OpenStack listserv">dive in</a> yourself?</p>
<ul><li><a href="http://lists.openstack.org/pipermail/openstack-dev/2015-October/076000.html" target="_blank" style="font-size: 13.008px; line-height: 1.538em; background-color: transparent;">Proposed Design Summit track/room/time allocation</a>: How best should limited time be divided?</li>
<li><a href="http://lists.openstack.org/pipermail/openstack-dev/2015-October/076373.html" target="_blank">Scheduler proposal</a>: A look back at an experiment.</li>
<li><a href="http://lists.openstack.org/pipermail/openstack-dev/2015-October/076518.html" target="_blank">Requests + urllib3 + distro packages</a>: Finding a permanent fix.</li>
<li><a href="http://lists.openstack.org/pipermail/openstack-dev/2015-October/076327.html" target="_blank">Naming N and O releases nowish</a>: With the summit locations known, are the releases ready for naming?</li>
</ul><h3>Cloud &amp; OpenStack events</h3>
<p>Here's what is happening this week. Something missing? <a href="mailto:osdc-admin@redhat.com">Email</a> us and let us know, or add it to our <a href="https://opensource.com/resources/conferences-and-events-monthly" target="_blank" title="Events Calendar">events calendar</a>.</p>
<ul><li><a href="http://www.meetup.com/OpenStack-User-Group-Nordics/events/225365322/" target="_blank">OpenStack Cinder deep dive</a>: Monday, October 12; Stockholm, Sweden.</li>
<li><a href="http://www.meetup.com/Turkey-OpenStack-Meetup/events/225916298/" target="_blank">OpenStack with Ceph storage</a>: Wednesday, October 14; Istanbul, Turkey.</li>
<li><a href="http://www.meetup.com/OpenStackDC/events/224954116/" target="_blank">OpenStack installation methods and software defined storage</a>: Thursday, October 15; Washington, DC.</li>
<li><a href="http://www.meetup.com/openstack/events/223988220/" target="_blank">The good, the bad, and the ugly of OpenStack APIs</a>: Thursday, October 15; San Francisco, CA.</li>
<li><a href="http://www.meetup.com/openstack-atlanta/events/225414555/" target="_blank">OpenStack Liberty</a>: Thursday, October 15; Atlanta, GA.</li>
</ul><p><em>Interested in the OpenStack project development meetings? A complete list of these meetings' regular schedules is available <a href="https://wiki.openstack.org/wiki/Meetings" target="_blank" title="OpenStack Meetings">here</a>. Thanks to the <a href="http://www.openstack.org/blog/2015/10/openstack-weekly-community-newsletter-oct-3-oct-9/" target="_blank">OpenStack Community Weekly Newsletter</a> for many of these linked articles.</em></p>
