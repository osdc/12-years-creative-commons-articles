<p><a href="https://opensource.com/resources/what-are-microservices">Microservices</a> and <a href="https://opensource.com/article/19/6/what-is-graphql">GraphQL</a> are a great combination, like bread and butter. They're both great on their own and even better together. Knowing the health of your microservices is important because they run important services—it would be foolish to wait until something critical breaks before diagnosing a problem. It doesn't take much effort to let GraphQL help you detect issues early.</p>
<figure class="image file-default media-element"><img alt="GraphQL in Microservices" data-delta="1" data-fid="480441" data-media-element="1" src="https://opensource.com/sites/default/files/uploads/graphql-microservices.png" title="GraphQL in Microservices" typeof="foaf:Image" width="675" height="338" /><figcaption>GraphQL in Microservices</figcaption></figure><p>Routine health checks allow you to watch and test your services to get early notifications about problems before they affect your business, clients, or project. That's easy enough to say, but what does it really mean to do a health check?</p>
<p>Here are the factors I think about when designing a service checkup:</p>
<p><strong>Requirements for a server health check:</strong></p>
<ol><li>I need to understand the availability status of my microservice.</li>
<li>I want to be able to manage the server load.</li>
<li>I want end-to-end (e2e) testing of my microservices.</li>
<li>I should be able to predict outages.</li>
</ol><figure class="image media-wysiwyg-align-center file-default media-element"><img alt="Service health in microservices" data-delta="2" data-fid="480446" data-media-element="1" src="https://opensource.com/sites/default/files/uploads/servicehealth.png" title="Service health in microservices" typeof="foaf:Image" width="675" height="273" /><figcaption>Service health in microservices</figcaption></figure><h2 id="ways-to-do-server-health-checks">Ways to do server health checks</h2>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Explore the open source cloud</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/topics/cloud?sc_cid=7013a000002gp8aAAA">Understanding clouds</a></li>
<li><a href="https://www.redhat.com/en/services/training/do092-developing-cloud-native-applications-microservices-architectures?sc_cid=7013a000002gp8aAAA">Free online course: Developing cloud-native applications with microservices architectures</a></li>
<li><a href="https://www.redhat.com/en/topics/cloud-computing/what-is-hybrid-cloud?sc_cid=7013a000002gp8aAAA">What is hybrid cloud?</a></li>
<li><a href="https://go.redhat.com/hybrid-cloud-strategies-20180912?sc_cid=7013a000002gp8aAAA">eBook: Building a hybrid cloud strategy</a></li>
<li><a href="https://www.redhat.com/en/topics/containers/what-is-kubernetes?sc_cid=7013a000002gp8aAAA">What is Kubernetes?</a></li>
<li><a href="https://www.redhat.com/en/topics/edge-computing?sc_cid=7013a000002gp8aAAA">Understanding edge computing</a></li>
<li><a href="https://www.redhat.com/architect/?sc_cid=7013a000002gp8aAAA">Latest articles for IT architects</a></li>
</ul></div>
</div>
</div>
</div>
<p>Coming up with health checks can be tricky because, in theory, there's nearly an infinite number of things you could check for. I like to start small and run the most basic test: a ping test. This simply tests whether the server running the application is available. Then I ramp up my tests to evaluate specific concerns, thinking about the elements of my server that are most important. I think about the things that would be disastrous should they disappear suddenly.</p>
<ol><li><strong>Ping check:</strong> Ping is the simplest monitor type. It just checks that your application is online.</li>
<li><strong>Scripted browser:</strong> Scripted browsers are more advanced; browser automation tools like <a href="https://www.selenium.dev/" target="_blank">Selenium</a> enable you to implement custom monitoring rule sets.</li>
<li><strong>API tests:</strong> API tests are used to monitor API endpoints. This is an advanced version of the ping check model, where you can define the monitoring plan based on the API responses.</li>
</ol><h2 id="health-check-with-graphql">Health check with GraphQL</h2>
<p>In a typical REST-based microservice, you need to build health check features from scratch. It's a time-intensive process, but it's not something you have to worry about with GraphQL.</p>
<p>According to its <a href="https://graphql.org/" target="_blank">website</a>:</p>
<blockquote><p>"GraphQL is a query language for APIs and a runtime for fulfilling those queries with your existing data. GraphQL provides a complete and understandable description of the data in your API, gives clients the power to ask for exactly what they need and nothing more, makes it easier to evolve APIs over time, and enables powerful developer tools."</p>
</blockquote>
<p>When you bootstrap a GraphQL microservice, you also get a provision to monitor the health of the microservice. This is something of a hidden gem.</p>
<p>As I mentioned above, you can perform API tests as well as ping checks with the GraphQL endpoint.</p>
<p>Apollo GraphQL Server provides a default endpoint that returns information about your microservices and server health. It's not very complex: it returns status code 200 if the server is running.</p>
<p>The default endpoint is <code>&lt;server-host&gt;/.well-known/apollo/server-health</code>.</p>
<figure class="image media-wysiwyg-align-center file-default media-element"><img alt="Health Check with GraphQL" data-delta="3" data-fid="480451" data-media-element="1" src="https://opensource.com/sites/default/files/uploads/healthcheck.png" title="Health Check with GraphQL" typeof="foaf:Image" width="675" height="146" /><figcaption autocomplete="off">Health Check with GraphQL</figcaption></figure><h2 id="advanced-health-checks">Advanced health checks</h2>
<p>In some cases, basic health checks may not be enough to ensure the integrity of a system. For example, tightly coupled systems require more business logic to ensure the health of the system.</p>
<p>Apollo GraphQL is efficient enough to manage this use case by declaring an <code>onHealthCheck</code> function while defining the server:</p>
<pre><code class="language-java">* Defining the Apollo Server */
const apollo = new ApolloServer({
  playground: process.env.NODE_ENV !== 'production',
  typeDefs: gqlSchema,
  resolvers: resolver,
  onHealthCheck: () =&gt; {
    return new Promise((resolve, reject) =&gt; {
      // Replace the `true` in this conditional with more specific checks!
      if (true) {
        resolve();
      } else {
        reject();
      }
    });
  }
});</code></pre><p>When you define an <code>onHealthCheck</code> method, it returns a promise that <em>resolves</em> if the server is ready and <em>rejects</em> if there is an error.</p>
<p>GraphQL makes monitoring APIs easier. In addition, using it for your server infrastructure makes things scalable. If you want to try adopting GraphQL as your new infrastructure definition, see my GitHub repo for <a href="https://github.com/riginoommen/example-graphql" target="_blank">example code and configuration</a>.</p>
