<p>We are excited to announce the first ever <a title="Open Humanities Awards" href="http://openhumanitiesawards.org/" target="_blank">Open Humanities Awards</a>. There are €15,000 worth of prizes on offer for 3-5 projects that use open content, open data, or open source tools to further humanities teaching and research. Whether you’re interested in patterns of allusion in Aristotle, networks of correspondence in the Jewish Enlightenment, or digitising public domain editions of Dante, we’d love to hear about the kinds of open projects that could support your interest!</p>
<h3>Why are we running these Awards?</h3>
<p>Humanities research is based on the interpretation and analysis of a wide variety of cultural artefacts including texts, images, and audiovisual material. Much of this material is now freely and openly available on the internet enabling people to discover, connect, and contextualise cultural artefacts in ways that were previously very difficult.</p>
<p>We want to make the most of this new opportunity by encouraging budding developers and humanities researchers to collaborate and start new projects that use this open content and data paving the way for a vibrant cultural and research commons to emerge.</p>
<h3>Who can apply?</h3>
<p>The Awards are open to any citizen of the EU.</p>
<h3>Who is judging the Awards?</h3>
<p>The Awards will be judged by a stellar cast of leading Digital Humanists:</p>
<ul><li><span style="letter-spacing: 0px;"><a href="http://www.ibi.hu-berlin.de/institut/personen/gradmann" target="_blank">Professor Stefan Gradmann, Humboldt University</a></span></li>
<li><span style="letter-spacing: 0px;"><a href="http://www.tcd.ie/English/staff/academic-staff/susan-schriebman.php" target="_blank">Dr. Susan Schreibman, Trinity College Dublin</a></span></li>
<li><span style="letter-spacing: 0px;"><a href="http://www.kcl.ac.uk/artshums/depts/ddh/people/academic/prescott/index.aspx" target="_blank">Professor Andrew Prescott, Kings College London</a></span></li>
<li><span style="letter-spacing: 0px;"><a href="http://www.oerc.ox.ac.uk/people/david-robey" target="_blank">Professor David Robey, University of Oxford</a></span></li>
<li><span style="letter-spacing: 0px;"><a href="http://www.ucl.ac.uk/dis/people/melissaterras" target="_blank">Dr. Melissa Terras, University College London</a></span></li>
<li><span style="letter-spacing: 0px;"><a href="https://republicofletters.stanford.edu/" target="_blank">Nicole Coleman, Stanford University Humanities Center</a></span></li>
<li><span style="letter-spacing: 0px;"><a href="https://www.linguistik.hu-berlin.de/institut/professuren/korpuslinguistik/mitarbeiter-innen/laurent/laurent/" target="_blank">Dr. Laurent Romary, INRIA</a></span></li>
</ul><h3><span style="letter-spacing: 0px;">What do we want to see?</span></h3>
<p>We are challenging humanities researchers, designers, and developers to create innovative projects open content, open data, or open source to further teaching or research in the humanities.</p>
<p>For example you might want to:</p>
<ul><li><span style="letter-spacing: 0px;">Start a project to collaboratively transcribe, annotate, or translate public domain texts</span></li>
<li><span style="letter-spacing: 0px;">Explore patterns of citation, allusion, and influence using bibliographic metadata or textmining</span></li>
<li><span style="letter-spacing: 0px;">Analyse and/or visually represent complex networks or hidden patterns in collections of texts</span></li>
<li><span style="letter-spacing: 0px;">Use computational tools to generate new insights into collections of public domain images, audio, or texts</span></li>
</ul><p><span style="letter-spacing: 0px;">You could start a project from scratch or build on an existing project. For inspiration you can have a look at the <a title="OpenGlam open source tools" href="http://openglam.org/culture-labs/" target="_blank">open source tools</a> the Open Knowledge Foundation has developed for use with cultural resources.</span></p>
<h3>Who is behind the awards?</h3>
<p>The Awards are being coordinated by the Open Knowledge Foundation and are part of the <a title="DM2E project" href="http://dm2e.eu/" target="_blank">DM2E project</a>. They are also supported by the <a title="Digital Humanities Quarterly" href="http://www.digitalhumanities.org/dhq/" target="_blank">Digital Humanities Quarterly</a>.</p>
<h3>How to apply</h3>
<p>Applications are <a title="Apply to Open Humanities Awards" href="http://openhumanitiesawards.org/" target="_blank">open</a> and the deadline is March 12.</p>
<p><span style="letter-spacing: 0px;">For more information, and to see the rules and ideas for open datasets and tools, visit <a href="http://openhumanitiesawards.org/" target="_blank">openhumanitiesawards.org</a>.</span></p>
<p><em><span style="letter-spacing: 0px;">Originally posted on the <a title="€15,000 OF PRIZES ON OFFER FOR OPEN HUMANITIES PROJECTS" href="http://blog.okfn.org/2013/02/13/e15000-of-prizes-on-offer-for-open-humanities-projects/" target="_blank">OKF blog</a>. Reposted under Creative Commons.</span></em></p>
