<p><a href="http://gnocchi.xyz/index.html" target="_blank">Gnocchi</a> is an open source <a href="https://en.wikipedia.org/wiki/Time_series_database " target="_blank">time series database</a> created in 2014 when OpenStack was looking for a highly scalable, fault-tolerant time series database that did not depend on a specialized database (e.g., Hadoop, Cassandra, etc.).</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on OpenStack</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/resources/what-is-openstack?intcmp=7016000000127cYAAQ">What is OpenStack?</a></li>
<li><a href="https://opensource.com/resources/openstack/tutorials?intcmp=7016000000127cYAAQ">OpenStack tutorials</a></li>
<li><a href="https://opensource.com/tags/openstack?intcmp=7016000000127cYAAQ">More OpenStack articles</a></li>
<li><a href="https://www.rdoproject.org/?intcmp=7016000000127cYAAQ">The RDO Project</a></li>
</ul></div>
</div>
</div>
</div>
<p>Gnocchi was originally built inside <a href="http://openstack.org " target="_blank">OpenStack</a>, but later moved out of the project, because it was built to be platform-agnostic. Even so, Gnocchi is still used as the main time series backend by this cloud platform; for example, <a href="https://github.com/openstack/ceilometer " target="_blank">OpenStack Ceilometer</a> leverages Gnocchi's large scalability and high-availability properties to ensure its telemetry is always up and fast.</p>
<p>The problem that Gnocchi solves is storage and indexing of time series data and resources at large scale. Modern cloud platforms are not only huge, but they also are dynamic and potentially multi-tenant. Gnocchi takes all of that into account.</p>
<h2>Aggregation</h2>
<p>Gnocchi takes a unique approach to time series storage: Rather than storing raw data points, it aggregates them before storing them. This built-in feature is different from most other time series databases, which usually support this mechanism as an option and compute aggregation (average, minimum, etc.) at query time.</p>
<p>Because Gnocchi computes all the aggregations at ingestion, getting the data back is extremely fast, as it just needs to read back the pre-computed results.</p>
<p>The way those data points are aggregated is configurable on a per-metric basis, using an archive policy. An archive policy defines which aggregations to compute and how many aggregates to keep. Gnocchi supports a wild number of aggregation methods, such as minimum, maximum, average, Nth percentile, standard deviation, etc. Those aggregations are computed over a period of time (called granularity) and are kept for a defined timespan. Aggregates are stored in a compressed format, ensuring the data points take as little space as possible.</p>
<p>For example, imagine you define an archive policy to keep the average, minimum, and maximum of a time series with five-minute granularity for 30 days. In that case, Gnocchi will compute the average, minimum, and maximum of the values over five-minute ranges, keeping up to 8,640 points (the number of five-minute aggregates you can have over 30 days).</p>
<h2>Getting started</h2>
<p>Before installing Gnocchi, you need to decide which measures and aggregate storage drivers you want to use. Gnocchi can leverage highly scalable storage systems, such as Ceph, OpenStack Swift, Redis, or even Amazon S3. If none of those options are available to you, you can still use a standard file system.</p>
<p>You also need a database to index the resources and metrics that Gnocchi will handle—both PostgreSQL and MySQL are supported.</p>
<p><article class="media media--type-image media--view-mode-full" title="Architecture diagram"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/architecture.png" width="766" height="505" alt="Architecture diagram" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p><a href="http://gnocchi.xyz/" target="_blank" rel="ugc">Gnocchi</a>. <a href="https://github.com/gnocchixyz/gnocchi/blob/master/LICENSE" target="_blank" rel="ugc">Apache License 2.0</a></p>
</div>
      
  </article></p>
<p>The <a href="http://gnocchi.xyz/install.html " target="_blank">installation page</a> describes how to set up Gnocchi and write its configuration file. Once this configuration file is written, the <strong>gnocchi-upgrade</strong> program will set up the storage engine and the database index so they are ready to be used.</p>
<p>Gnocchi is composed of two central services: an HTTP server, providing a REST API, and a metric processing daemon. The former is called <strong>gnocchi-api</strong>; the latter is named <strong>gnocchi-metricd</strong>. You need to run both services to use Gnocchi. The REST API will be used by clients to query Gnocchi and write data to it. The <strong>metricd</strong> service will ingest measures received by the API, compute the aggregates, and store them in the long-term aggregate storage.</p>
<p>Both those services are stateless and therefore horizontally scalable. Contrary to many time series databases, there is no limit on the number of <strong>metricd</strong> daemons or API endpoints that you can run with Gnocchi. If your load starts to increase, you just need to spawn more daemons to handle the flow of new requests. The same applies if you want to handle high-availability scenarios: just start more Gnocchi daemons on independent servers.</p>
<h2>Using the command-line tool</h2>
<p>Once Gnocchi is deployed, you can install the Gnocchi client command-line tool using pip (the Python package installer); just type <strong>pip install gnocchiclient</strong>. It sends requests to the HTTP REST API and formats and prints back the replies.</p>
<p>To organize metrics, Gnocchi provides the notion of resources. A resource can have any number of metrics attached to it:</p>
<pre>
<code class="language-bash">$ gnocchi resource create server42
+-----------------------+--------------------------------------+
| Field                 | Value                                |
+-----------------------+--------------------------------------+
| created_by_project_id |                                      |
| created_by_user_id    | admin                                |
| creator               | admin                                |
| ended_at              | None                                 |
| id                    | 43164383-6dc3-5034-a675-9a39493ca7df |
| metrics               |                                      |
| original_resource_id  | server42                             |
| project_id            | None                                 |
| revision_end          | None                                 |
| revision_start        | 2017-10-20T13:19:46.902754+00:00     |
| started_at            | 2017-10-20T13:19:46.902642+00:00     |
| type                  | generic                              |
| user_id               | None                                 |
+-----------------------+--------------------------------------+
</code></pre><p>Gnocchi handles revisions of resources; if any attribute is changed, Gnocchi will record that change and create a new entry in the history of the resource.</p>
<p>Once this resource is created, we can create a new metric for this resource:</p>
<pre>
<code class="language-bash">$ gnocchi metric create -r server42 cpu
+------------------------------------+------------------------------------------------------------------+
| Field                              | Value                                                            |
+------------------------------------+------------------------------------------------------------------+
| archive_policy/aggregation_methods | std, count, min, max, sum, mean                                  |
| archive_policy/back_window         | 0                                                                |
| archive_policy/definition          | - points: 8640, granularity: 0:05:00, timespan: 30 days, 0:00:00 |
| archive_policy/name                | low                                                              |
| created_by_project_id              |                                                                  |
| created_by_user_id                 | admin                                                            |
| creator                            | admin                                                            |
| id                                 | ab0b5dab-31e9-4760-a58f-0dc324047f9f                             |
| name                               | cpu                                                              |
| resource/created_by_project_id     |                                                                  |
| resource/created_by_user_id        | admin                                                            |
| resource/creator                   | admin                                                            |
| resource/ended_at                  | None                                                             |
| resource/id                        | 43164383-6dc3-5034-a675-9a39493ca7df                             |
| resource/original_resource_id      | server42                                                         |
| resource/project_id                | None                                                             |
| resource/revision_end              | None                                                             |
| resource/revision_start            | 2017-10-20T13:19:46.902754+00:00                                 |
| resource/started_at                | 2017-10-20T13:19:46.902642+00:00                                 |
| resource/type                      | generic                                                          |
| resource/user_id                   | None                                                             |
| unit                               | None                                                             |
+------------------------------------+------------------------------------------------------------------+
</code></pre><p>This creates a metric named <strong>cpu</strong> and attaches it to the resource <strong>server42</strong>. The archive policy used by default for new metrics is low, which is provided by default when installing Gnocchi. The low archive policy stores various aggregation methods for 30 days over five minutes granularity.</p>
<p>This new metric has no measures, so it is time to send a few:</p>
<pre>
<code class="language-bash">$ gnocchi measures add -m "2017-10-20 12:00@42" -m "2017-10-20 12:03@18" -m "2017-10-20 12:06@56" -r server42 cpu
$  gnocchi measures show -r server42 cpu
+---------------------------+-------------+-------+
| timestamp                 | granularity | value |
+---------------------------+-------------+-------+
| 2017-10-20T12:00:00+02:00 |       300.0 |  30.0 |
| 2017-10-20T12:05:00+02:00 |       300.0 |  56.0 |
+---------------------------+-------------+-------+
</code></pre><p>The three measures sent are aggregated into two data points for 12:00 and 12:05. By default, the aggregation method shown is <strong>mean</strong>, but you can request any other aggregation method:</p>
<pre>
<code class="language-bash">$ gnocchi measures show --aggregation count -r server42 cpu
+---------------------------+-------------+-------+
| timestamp                 | granularity | value |
+---------------------------+-------------+-------+
| 2017-10-20T12:00:00+02:00 |       300.0 |   2.0 |
| 2017-10-20T12:05:00+02:00 |       300.0 |   1.0 |
+---------------------------+-------------+-------+
</code></pre><p>The <strong>count</strong> aggregation method computes the number of data points received over each interval.</p>
<p>The default resource type is <strong>generic</strong>, but you can create your own resource type with any extra number of attributes. Those attributes can be optional and can have specific types to make sure your data is properly structured and easy to consume:</p>
<pre>
<code class="language-bash">$ gnocchi resource-type create -a ipaddress:string:true network-switch
+----------------------+----------------------------------------------------------+
| Field                | Value                                                    |
+----------------------+----------------------------------------------------------+
| attributes/ipaddress | max_length=255, min_length=0, required=True, type=string |
| name                 | network-switch                                           |
| state                | active                                                   |
+----------------------+----------------------------------------------------------+
$ gnocchi resource create --type network-switch --attribute ipaddress:192.168.2.3 sw18-prs.example.com
+-----------------------+--------------------------------------+
| Field                 | Value                                |
+-----------------------+--------------------------------------+
| created_by_project_id |                                      |
| created_by_user_id    | admin                                |
| creator               | admin                                |
| ended_at              | None                                 |
| id                    | 4fbdf14d-5650-58f5-b232-dc3cf91c28d0 |
| ipaddress             | 192.168.2.3                          |
| metrics               |                                      |
| original_resource_id  | sw18-prs.example.com                 |
| project_id            | None                                 |
| revision_end          | None                                 |
| revision_start        | 2017-10-20T15:08:30.935529+00:00     |
| started_at            | 2017-10-20T15:08:30.935513+00:00     |
| type                  | network-switch                       |
| user_id               | None                                 |
+-----------------------+--------------------------------------+
</code></pre><p>If the attribute value ever changes, its history can be retrieved:</p>
<pre>
<code class="language-bash">$ gnocchi resource update --type network-switch --attribute ipaddress:192.168.2.48 sw18-prs.example.com
+-----------------------+--------------------------------------+
| Field                 | Value                                |
+-----------------------+--------------------------------------+
| created_by_project_id |                                      |
| created_by_user_id    | admin                                |
| creator               | admin                                |
| ended_at              | None                                 |
| id                    | 4fbdf14d-5650-58f5-b232-dc3cf91c28d0 |
| ipaddress             | 192.168.2.48                         |
| metrics               |                                      |
| original_resource_id  | sw18-prs.example.com                 |
| project_id            | None                                 |
| revision_end          | None                                 |
| revision_start        | 2017-10-20T15:10:11.903018+00:00     |
| started_at            | 2017-10-20T15:08:30.935513+00:00     |
| type                  | network-switch                       |
| user_id               | None                                 |
+-----------------------+--------------------------------------+

$ gnocchi resource history --format yaml --type network-switch sw18-prs.example.com
- creator: admin
  ended_at: null
  id: 4fbdf14d-5650-58f5-b232-dc3cf91c28d0
  ipaddress: 192.168.2.3
  metrics: {}
  original_resource_id: sw18-prs.example.com
  project_id: null
  revision_end: '2017-10-20T15:10:11.903018+00:00'
  revision_start: '2017-10-20T15:08:30.935529+00:00'
  started_at: '2017-10-20T15:08:30.935513+00:00'
  type: network-switch
  user_id: null
- creator: admin
  ended_at: null
  id: 4fbdf14d-5650-58f5-b232-dc3cf91c28d0
  ipaddress: 192.168.2.48
  metrics: {}
  original_resource_id: sw18-prs.example.com
  project_id: null
  revision_end: null
  revision_start: '2017-10-20T15:10:11.903018+00:00'
  started_at: '2017-10-20T15:08:30.935513+00:00'
  type: network-switch
  user_id: null
</code></pre><p>The revision fields indicate what time the changes were made. Both the old and the new attribute values of <strong>ipaddress</strong> are accessible, making it easy to trace how a resource has been modified.</p>
<p>Gnocchi offers other features, like search in resources and metrics, advanced cross-aggregation of metrics, access control list (ACL) management, and more.</p>
<h2>Integration with other tools</h2>
<p>Gnocchi can integrate with many other tools. For example, it is possible to use its <a href="http://gnocchi.xyz/collectd.html " target="_blank">collectd plugin</a> to collect metrics with <strong>collectd</strong> and send them to Gnocchi.</p>
<p>Gnocchi also supports <a href="https://opensource.com/article/17/8/linux-grafana" target="_blank">Grafana</a>, the standard tool for displaying charts from time series databases, through a <a href="http://gnocchi.xyz/grafana.html " target="_blank">Grafana plugin</a>.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Grafana integration with Gnocchi"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/gnocchi_graph.png" width="700" height="349" alt="Grafana integration with Gnocchi" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<h2>Gnocchi's roadmap</h2>
<p>Gnocchi is <a href="https://github.com/gnocchixyz/gnocchi" target="_blank">actively developed</a>, and new features are always coming. The next version (4.1) should provide more computing possibilities for cross-metric aggregation. Also, an ingestion point for <a href="https://prometheus.io/" target="_blank">Prometheus</a> is being built so Gnocchi can be used as a long-term and scalable storage option for the Prometheus time series alerting and monitoring system.</p>
