<p>In May, we asked some of our contributors <a href="https://opensource.com/article/21/5/breakfast">what they eat for breakfast</a>. After all, busy developers, sysadmins, and other IT pros need to fuel up quickly before they are ready to take on the rigors of their jobs.</p>
<p>After reading about our contributors' morning meals, many Opensource.com readers were quick to share what they eat to start the day. So if the first <a href="https://opensource.com/article/21/5/breakfast">16 quick breakfasts</a> didn't feed your hunger, here are 10 more options.</p>
<p>Regularly, especially on Mondays, whatever is left over from the previous day's lunch or dinner, Mexican style. —<a href="https://opensource.com/users/darkaxl">Alex Callejas Garcia</a></p>
<p>A bowl of Raisin Bran Crunch cereal with 2% milk while browsing the overnight emails, followed by a mug of tea with a little sugar and a splash of milk for the first GitHub reviews of the day. —<a href="https://opensource.com/users/tomsweeneyredhat">Tom Sweeney</a></p>
<p>I often make smoothies with bananas, iced coffee, almond milk, cacao nibs, chia seeds, and almond butter. Now that I'm at home so much, I also premake low-sugar chia puddings in small containers for the days when we don't have time to make smoothies. For the puddings, I do different flavors every week like chocolate peanut butter, pandan coconut, maple cinnamon pecan, tahini date, pistachio, chocolate cinnamon, etc. Is it weird that new flavors of breakfast pudding pass for excitement at my house? —<a href="https://www.linkedin.com/in/denicholson/" target="_blank">Deb Nicolson</a></p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More open source career advice</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7013a000002DRawAAG">Open source cheat sheets</a></li>
<li><a href="https://developers.redhat.com/learn/linux-starter-kit-for-developers/?intcmp=7013a000002DRawAAG">Linux starter kit for developers</a></li>
<li><a href="https://www.redhat.com/sysadmin/questions-for-employer?intcmp=7013a000002DRawAAG">7 questions sysadmins should ask a potential employer before taking a job</a></li>
<li><a href="https://www.redhat.com/architect/?intcmp=7013a000002DRawAAG">Resources for IT artchitects</a></li>
<li><a href="https://enterprisersproject.com/article/2019/3/article-cheat-sheet?intcmp=7013a000002DRawAAG">Cheat sheet: IT job interviews</a></li>
</ul></div>
</div>
</div>
</div>
<p>Almost every morning is a bowl of Greek yogurt with some granola (currently in front of me is a bowl with ginger and mango granola; it might be a liiiiiitle too gingery for first thing in the morning!), some fruit, and a very large cup of coffee. I generally eat breakfast while reading the morning email and letting the coffee prepare me for the rest of the day. —<a href="https://opensource.com/users/clcollins">Chris Collins</a></p>
<p>It varies, but locally roasted coffee is my go-to. Sometimes I make a cold brew for the week. Then, either waffles with peanut butter, an omelet with cheese (still working on my flip, LOL), or toast with mashed avocado topped with ginger and other spices. —<a href="https://opensource.com/users/rfreed">Spencer Hunley</a></p>
<p>Porridge from oats+milk+water, some sweetener, tea, cut cucumber, and sweet red pepper. We eat together, my wife, our five kids, and me. For a sweetener, my wife adds white sugar and cinnamon, and the rest of us add a <a href="https://en.wikibooks.org/wiki/Cookbook:Chocolate_Spread" target="_blank">homemade chocolate spread</a>. The milk used to be basic 3% fat cow milk sold in the supermarket, but in recent weeks, it's gradually been replaced with goat milk from our goat (which gave birth [recently]). Normally, we then send the kids in a hurry to school, but today there is no school due to the bombing from the Gaza Strip... (but we are quiet and fine here, no worries). —<a href="https://opensource.com/users/didib">Yedidyah Bar David</a></p>
<p>I do a drive-through breakfast and podcast listening session every morning to clear my head before buckling down for the day... so, a biscuit and coffee from Bojangles or Starbucks. Sometimes, when <em>crisis mode</em> hits early in the morning, I regretfully cancel the morning's drive time and pound down a pre-mixed Soylent shake instead. Usually while standing in front of the refrigerator. It's not fun, sure, but it's breakfast sorted in literally 10 seconds, and I don't end up with a stress-and-hunger headache at 2pm when I still haven't gotten lunch yet due to the same crisis that preempted breakfast! —<a href="https://opensource.com/users/jim-salter">Jim Salter</a></p>
<p>I eat frantically like an animal over the sink whatever is left over from the kids' plates before I rush them to school. —<a href="https://opensource.com/users/peteherzog">Pete Herzog</a></p>
<p>Granola with strawberries and almond milk most days. Cream of wheat and bacon twice a week. Gotta have that bacon!! —<a href="https://opensource.com/users/dboth">David Both</a></p>
<p>Each morning is a bit different, but I almost always end my breakfast with fruit muesli with honey. Before that, it varies: a tuna or mackerel salad, bread with honey, or bread with ham, and Dutch or Swiss cheese. Sometimes I make random combinations of these things. So, lots of calories to start a day! I do not eat anything after 3pm, except for special occasions. —<a href="https://opensource.com/users/czanik">Peter Czanik</a></p>
<p>What is your favorite breakfast for busy mornings? Please share in the comments.</p>
