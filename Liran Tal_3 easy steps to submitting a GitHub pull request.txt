<p>Making a change to open source code can be a rush.</p>
<p>Recently I was working on software to provide a console command-line interface for users to interact with the <a href="https://github.com/lirantal/operations-orchestration-api" target="_blank">NodeJS API client</a> for HPE's Operations Orchestration product. I realized I needed to use a library someone else wrote to parse all the arguments and display a nice command usage output. Something like this:</p>
<p><img alt="Command usage output screenshot" src="https://opensource.com/sites/default/files/commandusageoutput_0.png" width="520" height="168" /></p>
<p>I began searching for a library to use in <a href="https://www.npmjs.com/" target="_blank">npm</a>, the largest repository of open source NodeJS packages. Among npm's most popular libraries is <strong>command-line-args</strong>. I gave it a try and found it had almost everything I needed, including tests and code coverage. It's also well-maintained with frequent releases. Then, I hit a snag. I wanted to add some description to my command arguments, but couldn't find anything in the documentation about it. After inspecting the source code a bit further, I found references to a description property that could be configured.</p>
<p>Great, right? I found the solution to my problem, and I'm done.</p>
<p>Well, not exactly. What if someone else has the same problem in the future? They might not be able to find the solution and rule out this library because it doesn't seem to support a description configuration. Open source software works best when we pay things forward, so when you find something that can be improved, take the next step to improve it yourself.</p>
<h2>3 easy steps to submit a pull request on GitHub</h2>
<p><strong>1. Create your own copy of the code repository by forking the original.</strong></p>
<p>While logged in to GitHub and browsing the repository page for the project, locate and click the <strong>Fork</strong> button at the top right of the page. Your new personal repository will be created.</p>
<p>At this point, you can make any modifications you wish and suggest them to the parent repository you forked from. You can either clone the new forked repository on your own computer or edit files through the GitHub UI.</p>
<p>If you don't have a preferred Git UI tool but you do have Git installed, the following command will clone the repository to a local directory:</p>
<pre>
<code class="language-bash">git clone <repository></repository></code></pre><p><strong>2. Make changes to the required file on your new forked repository.</strong></p>
<p>If you want to make simple changes to things like README files, you can easily do this editing from GitHub's web site. To do so, locate the file you wish to edit and click on it to view it. Then, locate the top right <strong>pen icon</strong> to edit the file right in the browser. Make any changes you wish, add a commit message, and click <strong>Commit</strong>.</p>
<p>If you need to make modifications to files on a local cloned repository then after you're done making changes, don't forget to commit the files:</p>
<pre>
<code class="language-bash">git add .
git commit -m "a commit message"</code></pre><p>and then push the changes to your remote repository on GitHub:</p>
<pre>
<code class="language-bash">git push origin</code></pre><p><strong>3. Create a pull request to merge and incorporate your changes into the original repository.</strong></p>
<p>Creating a <strong>pull request</strong> allows upstream repositories (the original project you forked from) to accept your suggestions for improvements and fixes.</p>
<p>To create a new pull request, simply locate the green button on your repository project page called <strong>New pull request</strong> and you'll be presented with the list of changes you've made on your personal forked repository. On the top of the pull request, the base project is the upstream repository which you forked from. The page will show full list of changes to each file (a diff) which you can review. To proceed, click on the green <strong>Create pull request</strong> button and confirm the title and description. Then just submit it and you're done!</p>
<p>Your contribution will be sent to the project maintainer.</p>
<p><img alt="GitHub comment screenshot" src="https://opensource.com/sites/default/files/githubscreenshot.png" width="520" height="256" /></p>
<p>I should note that the maintainer may not choose to incorporate your contribution for any number of reasons, including code style standards and commit message guidelines. It is also possible that the project isn't actively maintained anymore and you will not get attention from the maintainers. For this reason, it's always best to consult a file commonly found in projects called <strong>CONTRIBUTING.md</strong>, which provides guidelines for how to contribute code back to the project. Of course, you'll also want to make sure the project is still maintained and ready to accept contributions.</p>
<p>What has your experience with submitting changes to open source repositories been? Do you have any advice to give? Let us know in the comments.</p>
