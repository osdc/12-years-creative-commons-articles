<p id="the-tac-command">The <strong>tac</strong> command is essentially the <a href="https://opensource.com/article/19/2/getting-started-cat-command"><strong>cat</strong></a> command, but its purpose is to concatenate files in reverse. Like <strong>cat</strong>, it has a convenient fallback mode to print to <em>standard output</em> (STDOUT) if no output file is provided, making it one of those commands that are more often used as a lazy pager—like <strong>less</strong> and <strong>more</strong>—than the function it is named for.</p>
<p>The <strong>cat</strong> command is often overused and abused, and <strong>tac</strong> is often taken as a joke command like <strong>ddate</strong> or <strong>cowsay</strong>. It often gets paraded out in April Fool’s day articles detailing stupid terminal tricks. So, it may come as a surprise that <strong>tac</strong> actually has a legitimate reason to exist.</p>
<p>It’s actually a useful command.</p>
<h2 id="tacs-purpose">What is the purpose of tac?</h2>
<p>The <strong>tac</strong> man page does a rather poor job of describing its own function:</p>
<pre><code class="language-text">Write each FILE to standard output, last line first.</code></pre><p>Taking that statement as it’s written, <strong>tac</strong> should print the last line of a file, then print the file starting back at line one:</p>
<pre><code class="language-bash">$ cat metasyntactic.list
foobar
foo
bar
baz

$ tac metasyntactic.list
baz
foobar
foo
bar</code></pre><p>That’s not what it does, though. Its info page is much clearer:</p>
<pre><code class="language-text">copies each FILE (‘-’ means standard input), 
or standard input if none are given, 
to standard output, reversing the records 
(lines by default) in each separately.</code></pre><p>For example:</p>
<pre><code class="language-bash">$ tac metasyntactic.list
baz
bar
foo
foobar</code></pre><p>Ignoring the fact that <strong>tac</strong> gives you everything in reverse, it has a few surprisingly useful and unique options.</p>
<h2 id="tac-and-separators">Tac and separators</h2>
<p>As the info page indicates, the file doesn’t have to be delimited by line, meaning that <strong>tac</strong> is equally as effective with, for example, a CSV file. You define a file’s separator character with the <strong>--separator</strong> or <strong>-s</strong> option, along with the delimiter used in the file.</p>
<p>For a CSV file, the character is probably a comma (<strong>,</strong>), but you can define any character. If a file doesn’t terminate with the separator character, though, then you get an unexpected result:</p>
<pre><code class="language-bash">$ tac --separator="," metasyntactic.csv
bazbar,foo,foobar</code></pre><p>There is no separator character between the first two items. The file’s final record (the string following the final separator, in this case, a comma) is not itself followed by a comma, so it’s treated as a non-record by <strong>tac</strong>. To account for this issue, use the <strong>--before</strong> or <strong>-b</strong> option, which places the separator character before each record:</p>
<pre><code class="language-bash">$ tac --separator="," --before metasyntactic.csv
baz,bar,foo,foobar</code></pre><p>The separator character doesn’t have to be a single character. It can also be a regular expression (regex).</p>
<h2 id="tac-and-regular-expressions">Tac and regular expressions</h2>
<p>A full explanation of regex is out of scope for this article, but it’s worth mentioning that extended <a href="https://opensource.com/article/19/7/what-posix-richard-stallman-explains">POSIX</a> is supported by means of an <a href="https://opensource.com/article/19/8/what-are-environment-variables" target="_blank"><span style="color:null;">environment variable</span></a>. Extended regex greatly enhances the readability of a regular expression, and for the sake of simplicity, that’s what this example uses. Assume you have a file containing strings all separated by integers:</p>
<pre><code class="language-bash">$ cat metasyntactic.txt
foobar123foo456bar789baz898</code></pre><p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Linux resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux commands cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Free online course: RHEL Technical Overview</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux networking cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">SELinux cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux common commands cheat sheet</a></li>
<li><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">What are Linux containers?</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Our latest Linux articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>You can reliably predict that the strings you care about are separated by integers, but you cannot reliably predict what those integers will be. That’s exactly the problem regex is meant to solve.</p>
<p>To use regex in your <strong>tac</strong> command, use the <strong>--regex</strong> or <strong>-r</strong> option before your <strong>--separator</strong> definition. Also, unless it’s already set in your environment, you must activate the <strong>REG_EXTENDED</strong> environment variable. You can set this variable to anything but zero to activate it, and you can do that in all the usual ways:</p>
<ul><li>Export the variable for the shell session you’re using.</li>
<li>Set the environment variable in your shell configuration file (such as <strong>~/.bashrc</strong>).</li>
<li>Prepend the environment variable to the <strong>tac</strong> command (in Bash, Zsh, and similar), as shown in the example below:</li>
</ul><pre><code class="language-bash">$ REG_EXTENDED=1 tac --regex \
--separator='[0-9]+' metasyntactic.txt
89baz898bar765foo432foobar1</code></pre><p>The regex option doesn’t handle non-terminated records well, though, even using the <strong>--before</strong> option. You may have to adjust your source file if that feature is important to you.</p>
<h2 id="tacs-use-cases">When to use tac</h2>
<p>These simple yet useful parsing options make <strong>tac</strong> worth using as an uncomplicated, minimalist parsing command. For those simple jobs that aren’t quite worth writing an <a href="https://www.gnu.org/software/gawk/" target="_blank">AWK</a> or <a href="https://www.perl.org" target="_blank">Perl</a> expression for, <strong>tac</strong> just might be a sensible solution.</p>
<p>The <strong>tac</strong> command is limited, obviously, because it doesn’t manipulate records in any way aside from reversing them. But sometimes that’s the only list manipulation you need.</p>
<p>For instance, if you’re packaging software for distribution, it’s not unusual to have a list of dependencies that are required for installation. Depending on how you gathered this list, you may have it in the order you established the dependencies were required instead of the order in which they must be installed.</p>
<p>This practice is relatively common because compiler errors hit the high-level dependencies first. That is, if your system is missing <strong>libavcodec</strong> then GCC stops and alerts you; but since GCC hasn’t gotten a chance to probe your system for <strong>libvorbis</strong> and <strong>libvpx</strong>, for example, it can’t tell you that those dependencies are also missing (and, often, required to exist on your system before compiling <strong>libavcodec</strong>).</p>
<p>So, your list of dependency grows in top-down form as you discover what libraries your system needs to build the libraries that the libraries need (and so on). At the end of such a process, <strong>tac</strong> is the quick and easy way to reverse that list.</p>
<p>Another common annoyance is log files. Entries are generally appended to a log file, so admins use <strong>tail</strong> to see the latest errors. That works well, but there are times you want to see a "chunk" of entries without knowing how far back you need to go. The <strong>tac</strong> command piped to <strong>less</strong> or <strong>more</strong> puts the latest entries at the top of your screen.</p>
<p>Finally, many configuration files have no clear termination marker for a given section. You can look up <strong>awk</strong> and <a href="https://www.gnu.org/software/sed/manual/sed.html" target="_blank"><strong>sed</strong></a> commands to devise a way to determine when a block in a config file ends, or you can use <strong>tac</strong> to reverse the order such that once your parser has found the first relevant entry in that block, it also knows when to <em>stop</em> reading, because what used to be the header is now a footer.</p>
<h2 id="tac-on">Tac on</h2>
<p>There are plenty of other great uses for <strong>tac</strong>, and probably a bunch of reasons that <strong>tac</strong> is too rudimentary to be a solution. Your system likely has it installed, however, so remember this command the next time you find that edge case in your workflow that really really needs to be at<em>tac</em>ked in reverse.</p>
