<p>Jenkins is a free and open source automation server for building, testing, and deploying code. It's the backbone of continuous integration and continuous delivery (CI/CD) and can save developers hours each day and protect them from having failed code go live. When code does fail, or when a developer needs to see the output of tests, <a href="https://www.jenkins.io/" target="_blank">Jenkins</a> provides log files for review.</p>
<p>The default Jenkins pipeline logs can be difficult to read. This quick summary of Jenkins logging basics offers some tips (and code) on how to make them more readable.</p>
<h2 id="what-you-get">What you get</h2>
<p>Jenkins pipelines are split into <a href="https://www.jenkins.io/doc/book/pipeline/syntax/#stage" target="_blank">stages</a>. Jenkins automatically logs the beginning of each stage, like this:</p>
<pre><code class="language-text">[Pipeline] // stage
[Pipeline] stage (hide)
[Pipeline] { (Apply all openshift resources)
[Pipeline] dir</code></pre><p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on Automation</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/engage/automated-enterprise-ebook-20171115?intcmp=7016000000127cYAAQ">Download now: The automated enterprise eBook</a></li>
<li><a href="https://www.redhat.com/en/services/training/do007-ansible-essentials-simplicity-automation-technical-overview?intcmp=7016000000127cYAAQ">Free online course: Ansible essentials</a></li>
<li><a href="https://opensource.com/downloads/ansible-cheat-sheet?intcmp=7016000000127cYAAQ">Ansible cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/home-automation-ebook?intcmp=7016000000127cYAAQ">eBook: A practical guide to home automation using open source tools</a></li>
<li><a href="https://opensource.com/downloads/ansible-quickstart?intcmp=7016000000127cYAAQ">A quickstart guide to Ansible</a></li>
<li><a href="https://opensource.com/tags/automation?intcmp=7016000000127cYAAQ">More articles about open source automation</a></li>
</ul></div>
</div>
</div>
</div>
<p>The text is displayed without much contrast, and important things (like the beginning of a stage) aren't highlighted. In a pipeline log several hundred lines long, finding where one stage starts and another ends, especially if you're casually browsing the logs looking for a particular stage, can be daunting.</p>
<p>Jenkins pipelines are written as a mix of <a href="https://opensource.com/article/20/12/groovy">Groovy</a> and shell scripting. In the Groovy code, logging is sparse; many times, it consists of grayed-out text in the command without details. In the shell scripts, debugging mode (<code>set -x</code>) is turned on, so every shell command is fully realized (variables are dereferenced and values printed) and logged in detail, as is the output.</p>
<p>It can be tedious to read through the logs to get relevant information, given that there can be so much. Since the Groovy logs that proceed and follow a shell script in a pipeline aren't very expressive, many times they lack context:</p>
<pre><code class="language-text">[Pipeline] dir
Running in /home/jenkins/agent/workspace/devop-master/devops-server-pipeline/my-repo-dir/src
[Pipeline] { (hide)
[Pipeline] findFiles
[Pipeline] findFiles
[Pipeline] readYaml
[Pipeline] }</code></pre><p>I can see what directory I am working in, and I know I was searching for file(s) and reading a YAML file using Jenkins' steps. But what was I looking for, and what did I find and read?</p>
<h2 id="what-can-be-done">What can be done?</h2>
<p>I'm glad you asked because there are a few simple practices and some small snippets of code that can help. First, the code:</p>
<pre><code class="language-text">def echoBanner(def ... msgs) {
   echo createBanner(msgs)
}

def errorBanner(def ... msgs) {
   error(createBanner(msgs))
}

def createBanner(def ... msgs) {
   return """
       ===========================================

       ${msgFlatten(null, msgs).join("\n        ")}

       ===========================================
   """
}

// flatten function hack included in case Jenkins security
// is set to preclude calling Groovy flatten() static method
// NOTE: works well on all nested collections except a Map
def msgFlatten(def list, def msgs) {
   list = list ?: []
   if (!(msgs instanceof String) &amp;&amp; !(msgs instanceof GString)) {
       msgs.each { msg -&gt;
           list = msgFlatten(list, msg)
       }
   }
   else {
       list += msgs
   }

   return  list
}</code></pre><p>Add this code to the end of each pipeline or, to be more efficient, <a href="https://www.jenkins.io/doc/pipeline/steps/workflow-cps/#load-evaluate-a-groovy-source-file-into-the-pipeline-script" target="_blank">load a Groovy file</a> or make it part of a <a href="https://www.jenkins.io/doc/book/pipeline/shared-libraries/" target="_blank">Jenkins shared library</a>.</p>
<p>At the start of each stage (or at particular points within a stage), simply call <code>echoBanner</code>:</p>
<pre><code class="language-yaml">echoBanner("MY STAGE", ["DOING SOMETHING 1", "DOING SOMETHING 2"])</code></pre><p>Your logs in Jenkins will display the following:</p>
<pre><code class="language-text">    ===========================================

    MY STAGE
    DOING SOMETHING 1
    DOING SOMETHING 2

    ===========================================</code></pre><p>The banners are very easy to pick out in the logs. They also help define the pipeline flow when used properly, and they break the logs up nicely for reading.</p>
<p>I have used this for a while now professionally in a few places. The feedback has been very positive regarding helping make pipeline logs more readable and the flow more understandable.</p>
<p>The <code>errorBanner</code> method above works the same way, but it fails the script immediately. This helps highlight where and what caused the failure.</p>
<h2 id="best-practices">Best practices</h2>
<ol><li>Use <code>echo</code> Jenkins steps liberally throughout your Groovy code to inform the user what you're doing. These can also help with documenting your code.</li>
<li>Use empty log statements (an empty echo step in Groovy, <code>echo ''</code>, or just <code>echo</code> in shell) to break up the output for easier readability. You probably use empty lines in your code for the same purpose.</li>
<li>Avoid the trap of using <code>set +x</code> in your scripts, which hides logging executed shell statements. It doesn't so much clean up your logs as it makes your pipelines a black box that hides what your pipeline is doing and any errors that appear. Make sure your pipelines' functionality is as transparent as possible.</li>
<li>If your pipeline creates intermediate artifacts that developers and/or DevOps personnel could use to help debug issues, then log their contents, too. Yes, it makes the logs longer, but it's only text. It will be useful information at some point, and what else is a log (if utilized properly) than a wealth of information about what happened and why?</li>
</ol><h2 id="kubernetes-secrets-where-full-transparency-won't-work">Kubernetes Secrets: Where full transparency won't work</h2>
<p>There are some things that you <em>don't</em> want to end up in your logs and be exposed. If you're using Kubernetes and referencing data held in a Kubernetes Secret, then you definitely don't want that data exposed in a log because the data is only obfuscated and not encrypted.</p>
<p>Imagine you want to take some data held in a Secret and inject it into a templated JSON file. (The full contents of the Secret and the JSON template are irrelevant for this example.) You want to be transparent and log what you're doing since that's best practice, but you don't want to expose your Secret data.</p>
<p>Change your script's mode from debugging (<code>set -x</code>) to command logging (<code>set -v</code>). At the end of the sensitive portion of the script, reset the shell to debugging mode:</p>
<pre><code class="language-bash">sh """
   # change script mode from debugging to command logging
   set +x -v

   # capture data from secret in shell variable
   MY_SECRET=\$(kubectl get secret my-secret --no-headers -o 'custom-column=:.data.my-secret-data')

   # replace template placeholder inline
   sed s/%TEMPLATE_PARAM%/${MY_SECRET_DATA}/ my-template-file.json

   # do something with modified template-file.json...

   # reset the shell to debugging mode
   set -x +v
"""</code></pre><p>This will output this line to the logs:</p>
<pre><code class="language-bash">sed s/%TEMPLATE_PARAM%/${MY_SECRET_DATA}/ my-template-file.json</code></pre><p>This doesn't realize the shell variable <code>MY_SECRET_DATA</code>, unlike in shell debug mode. Obviously, this isn't as helpful as debug mode if a problem occurs at this point in the pipeline and you're trying to figure out what went wrong. But it's the best balance between keeping your pipeline execution transparent for both developers and DevOps while also keeping your Secrets hidden.</p>
