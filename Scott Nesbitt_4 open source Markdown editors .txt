<p>I do most of my writing in a text editor and format it with <a href="https://en.wikipedia.org/wiki/Markdown" target="_blank">Markdown</a>—articles, essays, blog posts, and much more. I'm not the only one, either. Not only do countless people write with Markdown, but there are also more than a few publishing tools built around it.</p>
<p>Who'd have thought that a simple way to format web documents created by John Gruber and the late Aaron Schwartz would become so popular?</p>
<p>While most of my writing takes place in a text editor, I can understand the appeal of a dedicated Markdown editor. You get quick access to formatting, you can easily convert your documents to other formats, and you can get an instant preview.</p>
<p>If you're thinking about going Markdown and are looking for a dedicated editor, here are four open source options for your writing pleasure.</p>
<h2>Ghostwriter</h2>
<p><a href="https://wereturtle.github.io/ghostwriter/" target="_blank">Ghostwriter</a> ranks in the top three of the dedicated Markdown editors I've used or tried. And I've used or tried a few!</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Ghostwriter"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/markdown-editors_ghostwriter.png" width="675" height="437" alt="Ghostwriter" title="Ghostwriter" /></div>
      
  </article></p>
<p>As an editor, Ghostwriter is pretty much a blank canvas. Start typing and adding formatting by hand. If you don't want to do that, or are just learning Markdown and don't know what to add, you can select certain formatting from Ghostwriter's Format menu.</p>
<p>Admittedly, it's just basic formatting—lists, character formatting, and indenting. You'll have to add headings, code, and the like by hand. But the Task List option is interesting. I know a few people who create their to-do lists in Markdown, and this option makes creating and maintaining one much easier.</p>
<p>What sets Ghostwriter apart from other Markdown editors is its range of export options. You can choose the Markdown processor you want to use, including <a href="https://github.com/vmg/sundown" target="_blank">Sundown</a>, <a href="https://pandoc.org" target="_blank">Pandoc</a>, or <a href="https://www.pell.portland.or.us/~orc/Code/discount/" target="_blank">Discount</a>. With a couple of clicks, you can convert what you're writing to HTML5, ODT, EPUB, LaTeX, PDF, or a Word document.</p>
<h2>Abricotine</h2>
<p>If you like your Markdown editors simple, you'll like <a href="http://abricotine.brrd.fr/" target="_blank">Abricotine</a>. But don't let its simplicity fool you; Abricotine packs quite a punch.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Abricotine"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/markdown-editors_abricotine.png" width="675" height="460" alt="Abricotine" title="Abricotine" /></div>
      
  </article></p>
<p>Like any other editor, you can enter formatting by hand or use the Format or Insert menus. Abricotine also has a menu for inserting a <a href="https://guides.github.com/features/mastering-markdown/" target="_blank">GitHub Flavored Markdown</a> table. There are 16 pre-packaged table formats, and you can add rows or columns as you need them. If the tables are looking a bit messy, you can pretty them up by pressing Ctrl+Shift+B.</p>
<p>Abricotine can automatically display images, links, and math. You can turn all that off if you want to. The editor is, however, limited to exporting documents as HTML.</p>
<h2>Mark Text</h2>
<p>Like Abricotine, <a href="https://marktext.github.io/website/" target="_blank">Mark Text</a> is a simple Markdown editor that might surprise you. It has a few features you might not expect and does quite a good job of handling documents formatted with Markdown.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Mark Text"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/markdown-editors_marktext.png" width="675" height="450" alt="Mark Text" title="Mark Text" /></div>
      
  </article></p>
<p>Mark Text stays out of your way. There are no menus or toolbars. You get to the commands and functions by clicking the stacked menu in the top-left corner of the editor window. It's just you and your words.</p>
<p>The editor's default mode is a semi-WYSIWYG view, although you can change that to see the formatting code you've added to your writing. As for that formatting, Mark Text supports GitHub Flavored Markdown, so you can add tables and formatted blocks of code with syntax highlighting. In its default view, the editor displays any images in your document.</p>
<p>While Mark Text lacks Ghostwriter's range of export options, you can save your files as HTML or PDF. The output doesn't look too bad, either.</p>
<h2>Remarkable</h2>
<p><a href="https://remarkableapp.github.io/" target="_blank">Remarkable</a> lies somewhere between Ghostwriter and Abricotine or Mark Text. It has that two-paned interface, but with a slightly more modern look. And it has a few useful features.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Remarkable"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/markdown-editors_remarkable.png" width="675" height="439" alt="Remarkable" title="Remarkable" /></div>
      
  </article></p>
<p>The first thing you notice about Remarkable is its <a href="https://en.wikipedia.org/wiki/Material_Design" target="_blank">Material Design</a>-inspired look and feel. It's not for everyone, and I'll be honest: It took me a little while to get used to it. Once you do, it's easy to use.</p>
<p>You get quick access to formatting from the toolbar and the menus. You can also change the preview pane's style using one of 11 built-in Cascading Style Sheets or by creating one of your own.</p>
<p>Remarkable's export options are limited—you can export your work only as HTML or PDF files. You can, however, copy an entire document or a selected portion as HTML, which you can paste into another document or editor. </p>
<hr /><p>Do you have a favorite dedicated Markdown editor? Why not share it by leaving a comment?</p>
