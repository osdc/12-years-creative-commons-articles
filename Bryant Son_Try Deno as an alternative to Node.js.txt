<p><a href="https://deno.land/" target="_blank">Deno</a> is a simple, modern, and secure runtime for JavaScript and TypeScript. It uses the JavaScript and WebAssembly engine <a href="https://v8.dev/" target="_blank">V8</a> and is built in <a href="https://opensource.com/article/20/12/rust">Rust</a>. The project, open source under an MIT License, was created by <a href="https://en.wikipedia.org/wiki/Ryan_Dahl" target="_blank">Ryan Dahl</a>, the developer who created Node.js.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Programming and development</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ&amp;src=programming_resource_menu4">Red Hat Developers Blog</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Programming cheat sheets</a></li>
<li><a href="https://www.redhat.com/en/services/training/learning-subscription?intcmp=7016000000127cYAAQ">Try for free: Red Hat Learning Subscription</a></li>
<li><a href="https://opensource.com/downloads/bash-programming-guide?intcmp=7016000000127cYAAQ">eBook: An introduction to programming with Bash</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/bash-shell-cheat-sheet?intcmp=7016000000127cYAAQ">Bash Shell Scripting Cheat Sheet</a></li>
<li><a href="https://developers.redhat.com/e-books/modernizing-enterprise-java?intcmp=7016000000127cYAAQ">eBook: Modernizing Enterprise Java</a></li>
</ul></div>
</div>
</div>
</div>
<p>Deno's <a href="https://github.com/denoland/deno/blob/v1.5.4/docs/introduction.md" target="_blank">GitHub repository</a> outlines its goals:</p>
<blockquote><ul><li>Only ship a single executable (<code>deno</code>)</li>
<li>Provide secure defaults
<ul><li>Unless specifically allowed, scripts can't access files, the environment, or the network.</li>
</ul></li>
<li>Browser compatible: The subset of Deno programs which are written completely in JavaScript and do not use the global <code>Deno</code> namespace (or feature test for it), ought to also be able to be run in a modern web browser without change.</li>
<li>Provide built-in tooling like unit testing, code formatting, and linting to improve developer experience.</li>
<li>Does not leak V8 concepts into user land.</li>
<li>Be able to serve HTTP efficiently</li>
</ul></blockquote>
<p>The repo also describes how Deno is different from NodeJS:</p>
<blockquote><ul><li>Deno does not use npm.
<ul><li>It uses modules referenced as URLs or file paths.</li>
</ul></li>
<li>Deno does not use <code>package.json</code> in its module resolution algorithm.</li>
<li>All async actions in Deno return a promise. Thus Deno provides different APIs than Node.</li>
<li>Deno requires explicit permissions for file, network, and environment access.</li>
<li>Deno always dies on uncaught errors.</li>
<li>Uses "ES Modules" and does not support <code>require()</code>. Third party modules are imported via URLs:<br /><pre><code class="language-bash">import * as log from "https://deno.land/std@$STD_VERSION/log/mod.ts";</code></pre></li>
</ul></blockquote>
<h2 id="" install-deno="">Install Deno</h2>
<p><article class="align-center media media--type-image media--view-mode-full" title="Deno GitHub repo"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/denoland.jpg" width="675" height="381" alt="Deno GitHub repo" title="Deno GitHub repo" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Bryant Son, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>Deno's website has <a href="https://deno.land/#installation" target="_blank">installation instructions</a> for various operating systems, and its complete source code is available in its <a href="https://github.com/denoland/deno" target="_blank">GitHub repo</a>. I run macOS, so I can install Deno with HomeBrew:</p>
<pre><code class="language-bash">$ brew install deno</code></pre><p run-deno="">On Linux, you can download, read, and then run the install script from Deno's server:</p>
<pre><code class="language-bash">$ curl -fsSL https://deno.land/x/install/install.sh
$ sh ./install.sh</code></pre><h2 id="" run-deno="">Run Deno</h2>
<p>After installing Deno, the easiest way to run it is:</p>
<pre><code class="language-bash">$ deno run https://deno.land/std/examples/welcome.ts</code></pre><p>If you explore the <a href="https://deno.land/std/examples/welcome.ts" target="_blank">welcome example</a>, you should see a single line that prints "Welcome to Deno" with a dinosaur icon. Here is slightly a more complicated version that also can be found on the website:</p>
<pre><code class="language-javascript">import { serve } from "https://deno.land/std@0.83.0/http/server.ts";
const s = serve({ port: 8000 });
console.log("http://localhost:8000/");
for await (const req of s) {
  req.respond({ body: "Hello World\n" });
}</code></pre><p>Save the file with a <code>.tx</code> extension. Run it with:</p>
<pre><code class="language-bash">$ deno run --allow-net &lt;name-of-your-first-deno-file.ts&gt;</code></pre><p>The <code>--allow-net</code> flag might not be necessary, but you can use it if you see an error like <code>error: Uncaught PermissionDenied: network access to "0.0.0.0:8000</code>.</p>
<p>Now, open a browser and visit <code>localhost:8080</code>. It should print "Hello, World!"</p>
<p>That's it! You can learn more about Deno in this video I recorded.</p>
<p><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/vSJn8LB_h8k" width="560"></iframe></p>
<p>What do you think about Deno? Please share your feedback in the comments.</p>
