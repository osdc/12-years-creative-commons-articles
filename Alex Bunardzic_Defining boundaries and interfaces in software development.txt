<p>Zombies are bad at understanding boundaries. They trample over fences, tear down walls, and generally get into places they don't belong. In the previous articles in this series, I explained why tackling coding problems all at once, as if they were hordes of zombies, is a mistake.</p>
<p><strong>ZOMBIES</strong> is an acronym that stands for:</p>
<p><strong>Z</strong> – Zero<br /><br /><strong>O</strong> – One<br /><br /><strong>M</strong> – Many (or more complex)<br /><br /><strong>B</strong> – Boundary behaviors<br /><br /><strong>I</strong> – Interface definition<br /><br /><strong>E</strong> – Exercise exceptional behavior<br /><br /><strong>S</strong> – Simple scenarios, simple solutions</p>
<p>In the first two articles in this series, I demonstrated the first three <strong>ZOMBIES</strong> principles of <strong>Zero</strong>, <strong>One</strong>, and <strong>Many</strong>. The first article <a href="https://opensource.com/article/21/1/zombies-zero">implemented <strong>Z</strong>ero</a>, which provides the simplest possible path through your code. The second article <a href="https://opensource.com/article/21/1/zombies-2-one-many">performed tests</a> with <strong>O</strong>ne and <strong>M</strong>any samples. In this third article, I'll take a look at <strong>B</strong>oundaries and <strong>I</strong>nterfaces. </p>
<h2 id="back-to-one">Back to One</h2>
<p>Before you can tackle <strong>B</strong>oundaries, you need to circle back (iterate).</p>
<p>Begin by asking yourself: What are the boundaries in e-commerce? Do I need or want to limit the size of a shopping basket? (I don't think that would make any sense, actually).</p>
<p>The only reasonable boundary at this point would be to make sure the shopping basket never contains a negative number of items. Write an executable expectation that expresses this limitation:</p>
<pre><code class="language-csharp">[Fact]
public void Add1ItemRemoveItemRemoveAgainHas0Items() {
	var expectedNoOfItems = 0;
	var actualNoOfItems = -1;
	Assert.Equal(expectedNoOfItems, actualNoOfItems);
}</code></pre><p>This says that if you add one item to the basket, remove that item, and remove it again, the <code>shoppingAPI</code> instance should say that you have zero items in the basket.</p>
<p>Of course, this executable expectation (microtest) fails, as expected. What is the bare minimum modification you need to make to get this microtest to pass?</p>
<pre><code class="language-csharp">[Fact]
public void Add1ItemRemoveItemRemoveAgainHas0Items() {
	var expectedNoOfItems = 0;
	Hashtable item = new Hashtable();
	shoppingAPI.AddItem(item);
	shoppingAPI.RemoveItem(item);
	var actualNoOfItems = shoppingAPI.RemoveItem(item);
	Assert.Equal(expectedNoOfItems, actualNoOfItems);
}</code></pre><p>This encodes an expectation that depends on the <code>RemoveItem(item)</code> capability. And because that capability is not in your <code>shippingAPI</code>, you need to add it.</p>
<p>Flip over to the <code>app</code> folder, open <code>IShippingAPI.cs</code> and add the new declaration:</p>
<pre><code class="language-csharp">int RemoveItem(Hashtable item);</code></pre><p>Go to the implementation class (<code>ShippingAPI.cs</code>), and implement the declared capability:</p>
<pre><code class="language-bash">public int RemoveItem(Hashtable item) {
	basket.RemoveAt(basket.IndexOf(item));
	return basket.Count;
}</code></pre><p>Run the system, and you get an error:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Error"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/error_0.png" width="675" height="44" alt="Error" title="Error" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Alex Bunardzic, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>The system is trying to remove an item that does not exist in the basket, and it crashes. Add a little bit of defensive programming:</p>
<pre><code class="language-csharp">public int RemoveItem(Hashtable item) {
	if(basket.IndexOf(item) &gt;= 0) {
		basket.RemoveAt(basket.IndexOf(item));
	}
	return basket.Count;
}</code></pre><p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Programming and development</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ&amp;src=programming_resource_menu4">Red Hat Developers Blog</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Programming cheat sheets</a></li>
<li><a href="https://www.redhat.com/en/services/training/learning-subscription?intcmp=7016000000127cYAAQ">Try for free: Red Hat Learning Subscription</a></li>
<li><a href="https://opensource.com/downloads/bash-programming-guide?intcmp=7016000000127cYAAQ">eBook: An introduction to programming with Bash</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/bash-shell-cheat-sheet?intcmp=7016000000127cYAAQ">Bash Shell Scripting Cheat Sheet</a></li>
<li><a href="https://developers.redhat.com/e-books/modernizing-enterprise-java?intcmp=7016000000127cYAAQ">eBook: Modernizing Enterprise Java</a></li>
</ul></div>
</div>
</div>
</div>
<p>Before you try to remove the item from the basket, check if it is in the basket. (You could've tried by catching the exception, but I feel the above logic is easier to read and follow.)</p>
<h2 id="more-specific-expectations">More specific expectations</h2>
<p>Before we move to more specific expectations, let's pause for a second and examine what is meant by interfaces. In software engineering, an interface denotes a specification, or a description of some capability. In a way, interface in software is similar to a recipe in cooking. It lists the ingredients that make the cake but it is not actually edible. We follow the specified description in the recipe in order to bake the cake.</p>
<p>Similarly here, we define our service by first specifying what is this service capable of. That specification is what we call interface. But interface itself cannot provide any services to us. It is a mere blueprint which we then use and follow in order to implement specified capabilities.</p>
<p>So far, you have implemented the interface (partially; more capabilities will be added later) and the processing boundaries (you cannot have a negative number of items in the shopping basket). You instructed the <code>shoppingAPI</code> how to add items to the shopping basket and confirmed that the addition works by running the <code>Add2ItemsBasketHas2Items</code> test.</p>
<p>However, just adding items to the basket does not an e-commerce app make. You need to be able to calculate the total of the items added to the basket—time to add another expectation.</p>
<p>As is the norm by now (hopefully), start with the most straightforward expectation. When you add one item to the basket and the item price is $10, you expect the shopping API to correctly calculate the total as $10.</p>
<p>Your fifth test (the fake version):</p>
<pre><code class="language-csharp">[Fact]
public void Add1ItemPrice10GrandTotal10() {
	var expectedTotal = 10.00;
	var actualTotal = 0.00;
	Assert.Equal(expectedTotal, actualTotal);
}</code></pre><p>Make the <code>Add1ItemPrice10GrandTotal10</code> test fail by using the good old trick: hard-coding an incorrect actual value. Of course, your previous three tests succeed, but the new fourth test fails:</p>
<pre><code class="language-csharp">A total of 1 test files matched the specified pattern.
[xUnit.net 00:00:00.57] tests.UnitTest1.Add1ItemPrice10GrandTotal10 [FAIL]
  X tests.UnitTest1.Add1ItemPrice10GrandTotal10 [4ms]
  Error Message:
   Assert.Equal() Failure
Expected: 10
Actual: 0

Test Run Failed.
Total tests: 4
     Passed: 3
	 Failed: 1
 Total time: 1.0320 Seconds</code></pre><p>Replace the hard-coded value with real processing. First, see if you have any such capability in your interface that would enable it to calculate order totals. Nope, no such thing. So far, you have declared only three capabilities in your interface:</p>
<ol><li><code>int NoOfItems();</code></li>
<li><code>int AddItem(Hashtable item);</code></li>
<li><code>int RemoveItem(Hashtable item);</code></li>
</ol><p>None of those indicates any ability to calculate totals. You need to declare a new capability:</p>
<pre><code class="language-csharp">double CalculateGrandTotal();</code></pre><p>This new capability should enable your <code>shoppingAPI</code> to calculate the total amount by traversing the collection of items it finds in the shopping basket and adding up the item prices.</p>
<p>Flip over to your tests and change the fifth test:</p>
<pre><code class="language-csharp">[Fact]
public void Add1ItemPrice10GrandTotal10() {
	var expectedGrandTotal = 10.00;
	Hashtable item = new Hashtable();
	item.Add("00000001", 10.00);
	shoppingAPI.AddItem(item);
	var actualGrandTotal = shoppingAPI.CalculateGrandTotal();
	Assert.Equal(expectedGrandTotal, actualGrandTotal);
}</code></pre><p>This test declares your expectation that if you add an item priced at $10 and then call the <code>CalculateGrandTotal()</code> method on the shopping API, it will return a grand total of $10. Which is a perfectly reasonable expectation since that's how the API should calculate.</p>
<p>How do you implement this capability? As always, fake it first. Flip over to the <code>ShippingAPI</code> class and implement the <code>CalculateGrandTotal()</code> method, as declared in the interface:</p>
<pre><code class="language-csharp">public double CalculateGrandTotal() {
		return 0.00;
}</code></pre><p>You're hard-coding the return value as 0.00, just to see if the test (your first customer) will be able to run it and whether it will fail. Indeed, it does run fine and fails, so now you must implement processing logic to calculate the grand total of the items in the shopping basket properly:</p>
<pre><code class="language-csharp">public double CalculateGrandTotal() {
	double grandTotal = 0.00;
	foreach(var product in basket) {
		Hashtable item = product as Hashtable;
		foreach(var value in item.Values) {
			grandTotal += Double.Parse(value.ToString());
		}
	}
	return grandTotal;
}</code></pre><p>Run the system. All five tests succeed!</p>
<h2 id="from-one-to-many">From One to Many</h2>
<p>Time for another iteration. Now that you have built the system by iterating to handle the <strong>Z</strong>ero, <strong>O</strong>ne (both very simple and a bit more elaborate scenarios), and <strong>B</strong>oundary scenarios (no negative number of items in the basket), you must handle a bit more elaborate scenario for <strong>M</strong>any. </p>
<p>A quick note: as we keep iterating and returning back to the concerns related to <strong>O</strong>ne, <strong>M</strong>any, and <strong>B</strong>oundaries (we are refining our implementation), some readers may expect that we should also rework the <strong>I</strong>nterface. As we will see later on, our interface is already fully fleshed out, and we see no need to add more capabilities at this point. Keep in mind that interfaces should be kept lean and simple; there is not much advantage in proliferating interfaces, as that only adds more noise to the signal. Here, we are following the principle of Occam's Razor, which states that entities should not multiply without a very good reason. For now, we are pretty much done with describing the expected capabilities of our API. We're now rolling up our sleeves and refining the implementation.</p>
<p>The previous iteration enabled the system to handle more than one item placed in the basket. Now, enable the system to calculate the grand total for more than one item in the basket. First things first; write the executable expectation:</p>
<pre><code class="language-csharp">[Fact]
public void Add2ItemsGrandTotal30() {
	var expectedGrandTotal = 30.00;
	var actualGrandTotal = 0.00;
	Assert.Equal(expectedGrandTotal, actualGrandTotal);
}</code></pre><p>You "cheat" by hard-coding all values first and then do your best to make sure the expectation fails.</p>
<p>And it does, so now is the time to make it pass. Modify your expectation by adding two items to the basket and then running the <code>CalculateGrandTotal()</code> method:</p>
<pre><code class="language-csharp">[Fact]
public void Add2ItemsGrandTotal30() {
	var expectedGrandTotal = 30.00;
	Hashtable item = new Hashtable();
	item.Add("00000001", 10.00);
	shoppingAPI.AddItem(item);
	Hashtable item2 = new Hashtable();
	item2.Add("00000002", 20.00);
	shoppingAPI.AddItem(item2);
	var actualGrandTotal = shoppingAPI.CalculateGrandTotal();
	Assert.Equal(expectedGrandTotal, actualGrandTotal);
}</code></pre><p>And it passes. You now have six microtests pass successfuly; the system is back to steady-state!</p>
<h2 id="setting-expectations">Setting expectations</h2>
<p>As a conscientious engineer, you want to make sure that the expected acrobatics when users add items to the basket and then remove some items from the basket always calculate the correct grand total. Here comes the new expectation:</p>
<pre><code class="language-csharp">[Fact]
public void Add2ItemsRemoveFirstItemGrandTotal200() {
	var expectedGrandTotal = 200.00;
	var actualGrandTotal = 0.00;
	Assert.Equal(expectedGrandTotal, actualGrandTotal);
}</code></pre><p>This says that when someone adds two items to the basket and then removes the first item, the expected grand total is $200.00. The hard-coded behavior fails, and now you can elaborate with more specific confirmation examples and running the code:</p>
<pre><code class="language-csharp">[Fact]
public void Add2ItemsRemoveFirstItemGrandTotal200() {
	var expectedGrandTotal = 200.00;
	Hashtable item = new Hashtable();
	item.Add("00000001", 100.00);
	shoppingAPI.AddItem(item);
	Hashtable item2 = new Hashtable();
	item2.Add("00000002", 200.00);
	shoppingAPI.AddItem(item2);
	shoppingAPI.RemoveItem(item);
	var actualGrandTotal = shoppingAPI.CalculateGrandTotal();
	Assert.Equal(expectedGrandTotal, actualGrandTotal);
}</code></pre><p>Your confirmation example, coded as the expectation, adds the first item (ID "00000001" with item price $100.00) and then adds the second item (ID "00000002" with item price $200.00). You then remove the first item from the basket, calculate the grand total, and assert if it is equal to the expected value.</p>
<p>When this executable expectation runs, the system meets the expectation by correctly calculating the grand total. You now have seven tests passing! The system is working; nothing is broken!</p>
<pre><code class="language-csharp">Test Run Successful.
Total tests: 7
     Passed: 7
 Total time: 0.9544 Seconds</code></pre><h2 id="more-to-come">More to come</h2>
<p>You're up to <strong>ZOMBI</strong> now, so in the next article, I'll cover <strong>E</strong>. Until then, try your hand at some tests of your own!</p>
