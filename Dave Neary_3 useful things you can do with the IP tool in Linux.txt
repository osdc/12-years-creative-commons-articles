<p>It has been more than a decade since the <code>ifconfig</code> command has been deprecated on Linux in favor of the <code>iproute2</code> project, which contains the magical tool <code>ip</code>. Many online tutorial resources still refer to old command-line tools like <code>ifconfig</code>, <code>route</code>, and <code>netstat</code>. The goal of this tutorial is to share some of the simple networking-related things you can do easily using the <code>ip</code> tool instead.</p>
<h2>Find your IP address</h2>
<pre><code class="language-text">[dneary@host]$ ip addr show
[snip]
44: wlp4s0: &lt;BROADCAST,MULTICAST,UP,LOWER_UP&gt; mtu 1500 qdisc mq state UP group default qlen 1000
	link/ether 5c:e0:c5:c7:f0:f1 brd ff:ff:ff:ff:ff:ff
	inet 10.16.196.113/23 brd 10.16.197.255 scope global dynamic wlp4s0
   	valid_lft 74830sec preferred_lft 74830sec
	inet6 fe80::5ee0:c5ff:fec7:f0f1/64 scope link
   	valid_lft forever preferred_lft forever
</code></pre><p><code>ip addr show</code> will show you a lot of information about all of your network link devices. In this case, my wireless Ethernet card (wlp4s0) is the IPv4 address (the <code>inet</code> field) <code>10.16.196.113/23</code>. The <code>/23</code> means that there are 23 bits of the 32 bits in the IP address, which will be shared by all of the IP addresses in this subnet. IP addresses in the subnet will range from <code>10.16.196.0 to 10.16.197.254</code>. The broadcast address for the subnet (the <code>brd</code> field after the IP address) <code>10.16.197.255</code> is reserved for broadcast traffic to all hosts on the subnet.</p>
<p>We can show only the information about a single device using <code>ip addr show dev wlp4s0</code>, for example.</p>
<h2>Display your routing table</h2>
<pre><code class="language-text">[dneary@host]$ ip route list
default via 10.16.197.254 dev wlp4s0 proto static metric 600
10.16.196.0/23 dev wlp4s0 proto kernel scope link src 10.16.196.113 metric 601
192.168.122.0/24 dev virbr0 proto kernel scope link src 192.168.122.1 linkdown
</code></pre><p>The routing table is the local host's way of helping network traffic figure out where to go. It contains a set of signposts, sending traffic to a specific interface, and a specific next waypoint on its journey.</p>
<p>If you run any virtual machines or containers, these will get their own IP addresses and subnets, which can make these routing tables quite complicated, but in a single host, there are typically two instructions. For local traffic, send it out onto the local Ethernet, and the network switches will figure out (using a protocol called ARP) which host owns the destination IP address, and thus where the traffic should be sent. For traffic to the internet, send it to the local gateway node, which will have a better idea how to get to the destination.</p>
<p>In the situation above, the first line represents the external gateway for external traffic, the second line is for local traffic, and the third is reserved for a virtual bridge for VMs running on the host, but this link is not currently active.</p>
<h2>Monitor your network configuration</h2>
<pre><code class="language-text">[dneary@host]$ ip monitor all
[dneary@host]$ ip -s link list wlp4s0
</code></pre><p>The <code>ip monitor</code> command can be used to monitor changes in routing tables, network addressing on network interfaces, or changes in ARP tables on the local host. This command can be particularly useful in debugging network issues related to containers and networking, when two VMs should be able to communicate with each other but cannot.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Linux resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux commands cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Free online course: RHEL Technical Overview</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux networking cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">SELinux cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux common commands cheat sheet</a></li>
<li><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">What are Linux containers?</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Our latest Linux articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>When used with <code>all</code>, <code>ip monitor</code> will report all changes, prefixed with one of <code>[LINK]</code> (network interface changes), <code>[ROUTE]</code> (changes to a routing table), <code>[ADDR]</code> (IP address changes), or <code>[NEIGH]</code> (nothing to do with horses—changes related to ARP addresses of neighbors).</p>
<p>You can also monitor changes on specific objects (for example, a specific routing table or an IP address).</p>
<p>Another useful option that works with many commands is <code>ip -s</code>, which gives some statistics. Adding a second <code>-s</code> option adds even more statistics. <code>ip -s link list wlp4s0</code> above will give lots of information about packets received and transmitted, with the number of packets dropped, errors detected, and so on.</p>
<h2>Handy tip: Shorten your commands</h2>
<p>In general, for the <code>ip</code> tool, you need to include only enough letters to uniquely identify what you want to do. Instead of <code>ip monitor</code>, you can use <code>ip mon</code>. Instead of <code>ip addr list</code>, you can use <code>ip a l</code>, and you can use <code>ip r</code> in place of <code>ip route</code>. <code>Ip link list</code> can be shorted to <code>ip l ls</code>. To read about the many options you can use to change the behavior of a command, visit the <a href="https://www.systutorials.com/docs/linux/man/8-ip-route/" target="_blank">ip manpage</a>.</p>
