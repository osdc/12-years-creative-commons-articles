<p>At Red Hat, part of my job is to ensure that company messages maintain their meaning and effectiveness in my native language—Italian—so that customers in my region can learn not only about our products and services but also about <a href="https://www.redhat.com/en/book-of-red-hat#our-values">our organizational values</a>.</p>
<p>The work tends to be simple and straightforward. But in an open organization, even the tasks that seem small can present big opportunities for learning about the power of working the open way.</p>
<p>That was the case for me recently, when what I thought would be a quick translation exercise turned into a lesson in the <a href="https://opensource.com/open-organization/resources/open-decision-framework">benefits of open decision making</a>.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Learn about open organizations</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://theopenorganization.org">Download resources</a></li>
<li><a href="https://theopenorganization.community">Join the community</a></li>
<li><a href="https://opensource.com/open-organization/resources/open-org-definition?src=too_resource_menu3a">What is an open organization?</a></li>
<li><a href="https://opensource.com/open-organization/resources/open-org-maturity-model?src=too_resource_menu4a">How open is your organization?</a></li>
</ul></div>
</div>
</div>
</div>

<h2>A crowdsourced decision</h2>
<p>A few months ago, I noticed a post on our company's internal collaboration platform that seemed to be calling my name. Colleagues from around the world were leaving comments on translated versions of one particular (and very important) corporate message: the <a href="https://www.redhat.com/en/book-of-red-hat#our-vision-intro">company's mission statement</a>. And they had questions about the Italian translation.</p>
<p>So I joined the conversation with no hesitation, assuming I'd engage in a quick exchange of opinions and reach a conclusion about the best way to translate Red Hat's mission statement:</p>
<p><em>To be the catalyst in communities of customers, contributors, and partners creating better technology the open source way.</em></p>
<p>That's a single sentence consisting of less than 20 words. Translating it into another language should be a no-brainer, right? If anything, the work should take no longer than a few minutes: Read it out loud, spot room for improvement, swap a word for a more effective synonym, maybe rephrase a bit, and you're done!</p>
<p>As a matter of fact, that's not always the case.</p>
<p>Translations of the mission statement in a few languages were already available, but comments from colleagues reflected a need for some review. And as more Red Hatters from different parts of the globe joined the discussion and shared their perspectives, I began to see many possibilities for solving this translation problem—and the challenges that come with this abundance of ideas.</p>
<p>Seeing so many inspiring comments can make reaching a decision much more complicated. One might put concepts into words in a <em>number</em> of ways, but how do we all agree on the <em>best</em> way? How do we deliver a message that reflects our common goal? How do we make sure that all employees can identify with it? And how do we ensure that our customers understand how we can help them grow?</p>
<p></p><div class="embedded-callout-text callout-float-right">It's true: When you're about to make a decision that has an impact on other people, you have to ask yourself—and each other—a lot of questions.</div>
<p>It's true: When you're about to make a decision that has an impact on other people, you have to ask yourself—and each other—a lot of questions.</p>
<h2>Found in translation</h2>
<p>Localising a message for our readers around the world means more than just translating it word by word. A literal translation may not be fully understandable, especially when the original message was written with English native speakers in mind. On the other hand, a creative translation may be just one of many possible interpretations of the original text. And then there are personal preferences: Which translation sounds better to me? Is there a particular reason? If so, how do I get my point across?</p>
<p>Linguistic debates can highlight complex aspects of communication, from the subtle nuances in the meaning of a particular word to the cultural traits, mindsets, and levels of familiarity with a topic that define how the same message can be perceived by readers from different countries. Our own conversations delved into known linguistic dilemmas like the use of anglicisms and the disambiguation of expressions that can have more than one interpretation.</p>
<p>Localisation teams must ensure that messages are delivered accurately and consistently. Working collaboratively is essential to achieving this goal.</p>
<p>To get started, we only had to look at how our original mission statement had come together in the first place. Since we were trying to do something that someone else had done before, we needed to ask: "<a href="https://www.managementexchange.com/blog/how-red-hat-used-open-source-way-develop-company-mission">How did Red Hat find its mission</a>?"</p>
<p>The answer? By asking the whole company what it should be.</p>
<p></p><div class="embedded-callout-text callout-float-right">Opening up the conversation doesn't always accelerate the decision making process.</div>
<p>It may be counterintuitive, but engaging the broader community can be a very efficient way to overcome uncertainties and address problems you weren't even aware of. Opening up the conversation doesn't always accelerate the decision making process, but by bringing people together you allow them to draw a clearer picture of the problem and provide you with the elements you need to make better decisions.</p>
<p>With that in mind, all you have to do is ask other teams to contribute, then give them a reasonable amount of time to make room in their busy schedule and think about how they can help. Suggestions will come, and how you will channel the incoming information is up to you.</p>
<p>Surveys are often a great way to gather feedback and keep track of incoming responses, and this is the approach we chose. We submitted a new translation, and asked our teams in Italy to tell us if they were happy with it. The majority of the respondents were. However, a few participants had their own versions to share (and valid comments to add).</p>
<p>After considering everyone's input, we submitted a second survey. We asked the participants to choose between the solution we offered in the first survey and a <em>newer</em> version, one we obtained by incorporating the feedback we'd received from our recent collaborative discussion.</p>
<p>The second option was the winner.</p>
<p>The initial translation did not represent Red Hat's mission as clearly and faithfully as the improved statement did. For example, in the original version, Red Hat was a “point of reference” rather than a "catalyst," and worked with "a community" rather than a larger and more diverse number of "communities." We were using the word "collaborators" instead of "contributors," and our actions were following a set of "principles" rather than the open source "way."</p>
<p>The changes we made reaffirmed Red Hat's role in the communities and its commitment to open source technology, but also to open source as a way of working.</p>
<p></p><div class="embedded-callout-text callout-float-right">Don't be afraid to jump into a conversation—or even to take the lead—if you think you can help to make better decisions.</div>
<p>So here is some advice this experience taught me: Don't be afraid to jump into a conversation—or even to take the lead—if you think you can help to make better decisions. Because as Jim Whitehurst likes to say, in the open source world, we believe <a href="https://opensource.com/open-organization/16/8/how-make-meritocracy-work">the best ideas should win</a>, no matter where they come from.</p>
<p>In short, here is my five-point checklist for open decision-making, regardless of the task you face:</p>
<ol><li>Encourage wide participation.</li>
<li>Welcome all responses as they arrive.</li>
<li>Identify the best ideas and get the most out of everyone's suggestions.</li>
<li>Answer questions and address concerns.</li>
<li>Communicate the outcome, and summarize the steps of the decision process. In doing so, reassert the value of broad collaboration.</li>
</ol><p>One more thing (I know I said I only had five more points, but don't switch off yet): Keep listening to what people might have to say about the decision you just made together.</p>
<p>The best ideas may take longer to brew.</p>
