<p>In this week's Top 5, we highlight resources for teaching and learning Python, thoughts from a longtime Linux user on system monitoring tools, DevOps as a crucial element of modern IT, an overview of The National Science Digital Library, and what sets Krita apart from other open source digital painting tools.</p>
<p><iframe allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/kYphbDXD4uc" width="520"></iframe></p>
<h2>Top 5 articles of the week</h2>
<p><strong>5. <a href="https://opensource.com/life/16/4/nick-hamilton-linuxfest-northwest-2016-krita" target="_blank">What sets Krita apart from other open source digital painting tools</a></strong></p>
<p>In this interview with Nick Hamilton, he talks about what he loves about Krita, prior to his talk introducing it to attendees at this year's <a href="https://www.linuxfestnorthwest.org/2016" target="_blank">LinuxFest Northwest</a> conference.</p>
<p><strong>4. <a href="https://opensource.com/education/16/4/national-science-digital-library" target="_blank">Free, high-quality education resources from the National Science Digital Library</a></strong></p>
<p>Don Watkins gives us a great overview of the The National Science Digital Library, which is a collection of high-quality, online educational resources for teaching and learning science.</p>
<p><strong>3. <a href="https://opensource.com/business/16/4/linuxfest-northwest-interview-corey-quinn" target="_blank">Decoding DevOps, Docker, and Git</a></strong></p>
<p>Corey Quinn talks about the idea that although there may not be a set of accepted standards on how to do DevOps "right," it is a crucial element of modern IT. Corey will be speaking at the LinuxFest Northwest conference this weekend.</p>
<p><strong>2. <a href="https://opensource.com/business/16/4/linuxfest-northwest-interview-ilan-rabinovitch" target="_blank">Finding the signal in the noise of Linux system monitoring</a></strong></p>
<p>Longtime Linux user Ilan Rabinovitch talks about system monitoring and shares his preferred tools. Ilan will also be speaking at the LinuxFest Northwest.</p>
<p><strong>1. <a href="https://opensource.com/education/16/4/teaching-python-and-more-with-oer" target="_blank">11 resources for teaching and learning Python</a></strong></p>
<p>Brian Hall is an educator and developer whose experience includes taking computers apart with kindergarten students and teaching Python programming to college students.</p>
