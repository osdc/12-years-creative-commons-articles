<p>Interested in keeping track of what's happening in the open source cloud? Opensource.com is your source for news in <a href="https://opensource.com/resources/what-is-openstack" target="_blank" title="What is OpenStack?">OpenStack</a>, the open source cloud infrastructure project.</p>
<p>In this special edition of our weekly OpenStack news, we round up the news and events from the Tokyo OpenStack Summit last week. Our roundup of the developers' listserv will return next week when OpenStack's development community is back in action.</p>
<p><!--break--></p>
<h3>OpenStack around the web</h3>
<p>There's a lot of interesting stuff being written about OpenStack. Here's a sampling:</p>
<ul><li><a href="http://superuser.openstack.org/articles/openstack-documentation-why-it-s-important-and-how-you-can-contribute" target="_blank">OpenStack documentation</a>: Why it's important and how you can contribute.</li>
<li><a href="http://www.eweek.com/cloud/why-contributing-to-openstack-makes-sense-for-vendors.html" target="_top">Why contributing to OpenStack makes sense for vendors</a>: It takes a broad community to build a successful project.</li>
<li><a href="http://superuser.openstack.org/articles/finding-your-way-around-openstack-professional-certification-and-the-project-navigator" target="_blank">Finding your way around OpenStack</a>: Professional certification and the project navigator.</li>
<li><a href="http://www.enterprisenetworkingplanet.com/datacenter/openstack-tokyo-the-ascendance-of-cloud-networking.html" target="_blank">OpenStack Tokyo</a>: The ascendance of cloud networking.</li>
<li><a href="http://superuser.openstack.org/articles/why-openstack-neutron-just-grabbed-center-stage" target="_blank">Why OpenStack Neutron just grabbed center stage</a>: Day two of the Tokyo Summit keynotes.</li>
<li><a href="http://www.eweek.com/cloud/openstack-foundation-expands-its-efforts-at-tokyo-summit.html" target="_blank">OpenStack Foundation expands its efforts at Tokyo Summit</a>: An interview with the OpenStack board chairman.</li>
<li><a href="http://www.datamation.com/open-source/the-numbers-behind-openstacks-success.html" target="_blank">The numbers behind OpenStack's success</a>: Statistics revealed that shows just diverse and large the project really is.</li>
<li><a href="http://www.rcrwireless.com/20151030/network-function-virtualization-nfv/nfv-and-openstack-trends-tag2" target="_blank">NFV and OpenStack trends</a>: Expanding and enhancing the NFV and OpenStack environment.</li>
<li><a href="http://superuser.openstack.org/articles/october-2015-user-survey-highlights-increasing-maturity-of-openstack-deployments" target="_blank">October 2015 user survey</a>: Highlighting increased maturity of OpenStack deployments.</li>
<li><a href="http://diginomica.com/2015/10/30/openstack-summit-can-the-community-conquer-the-wider-market/#.VjbPAmerSV4" target="_blank">OpenStack Summit</a>: Can the community conquer the wider market?</li>
<li><a href="http://superuser.openstack.org/articles/defcore-defining-the-interoperability-standard-for-openstack" target="_blank">DefCore</a>: Defining the interoperability standard for OpenStack.</li>
</ul><h3>Cloud &amp; OpenStack events</h3>
<p>Here's what is happening this week. Something missing? <a href="mailto:osdc-admin@redhat.com">Email</a> us and let us know, or add it to our <a href="https://opensource.com/resources/conferences-and-events-monthly" target="_blank" title="Events Calendar">events calendar</a>.</p>
<ul><li><a href="http://www.meetup.com/OpenStack-DFW/events/225205780/" target="_blank">OpenStack Cinder implementation today and new trends for tomorrow</a>: November 4, 2015; Richardson, TX.</li>
<li><a href="http://www.meetup.com/OpenStack-X/events/225616146/" target="_blank">OpenStack Liberty &amp; more</a>: November 4, 2015; Köln, Germany.</li>
<li><a href="http://www.meetup.com/Toulouse-DevOps/events/226016373/" target="_blank">What's new inside OpenStack</a>: November 4, 2015; Toulouse, France.</li>
</ul><p><em>Interested in the OpenStack project development meetings? A complete list of these meetings' regular schedules is available <a href="https://wiki.openstack.org/wiki/Meetings" target="_blank" title="OpenStack Meetings">here</a>.</em></p>
