<p>In the second article in this series, I talked about <a href="https://opensource.com/article/20/11/cloud-vs-local-home-automation">local control vs. cloud connectivity</a> and some things to consider for your home automation setup.</p>
<p>In this third article, I will discuss the underlying technology for connecting devices to <a href="https://opensource.com/article/20/11/home-assistant">Home Assistant</a>, including the dominant protocols that smart devices use to communicate and some things to think about before purchasing smart devices.</p>
<h2 id="controlling-devices-with-home-assistant">Connecting devices to Home Assistant</h2>
<p>Many different devices work with Home Assistant. Some connect through a cloud service, and others work by communicating with a central unit, such as a <a href="https://www.smartthings.com/" target="_blank">SmartThings Hub</a>, that Home Assistant communicates with. And still others have a facility to communicate over your local network.</p>
<p>For a device to be truly useful, one of its key features must be wireless connectivity. There are currently three dominant wireless protocols that smart devices use: WiFi, Z-Wave, and Zigbee. I'll do a quick breakdown of each including their pros and cons.</p>
<p><strong>A note about wireless spectra:</strong> Spectra are measured in hertz (Hz). A gigahertz (GHz) is 1 billion Hz. In general, the larger the number of Hz, the more data can be transmitted and the faster the connection. However, higher frequencies are more susceptible to interference and do not travel very well through solid objects. Lower frequencies can travel further and pass through solid objects more readily, but the trade-off is they cannot send much data.</p>
<h2 id="wifi">WiFi</h2>
<p><a href="https://en.wikipedia.org/wiki/Wi-Fi" target="_blank">WiFi</a> is the most widely known of the three standards. These devices are the easiest to get up and running if you are starting from scratch. This is because almost everyone interested in home automation already has a WiFi router or an access point. In fact, in most countries in the western world, WiFi is considered almost on the same level as running water; if you go to a hotel, you expect a clean, temperature-controlled room with a WiFi password provided at check-in.</p>
<p>Therefore, Internet of Things (IoT) devices that use the WiFi protocol require no additional hardware to get started. Plug in the new device, launch a vendor-provided application or a web browser, enter your credentials, and you're done.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on Automation</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/engage/automated-enterprise-ebook-20171115?intcmp=7016000000127cYAAQ">Download now: The automated enterprise eBook</a></li>
<li><a href="https://www.redhat.com/en/services/training/do007-ansible-essentials-simplicity-automation-technical-overview?intcmp=7016000000127cYAAQ">Free online course: Ansible essentials</a></li>
<li><a href="https://opensource.com/downloads/ansible-cheat-sheet?intcmp=7016000000127cYAAQ">Ansible cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/home-automation-ebook?intcmp=7016000000127cYAAQ">eBook: A practical guide to home automation using open source tools</a></li>
<li><a href="https://opensource.com/downloads/ansible-quickstart?intcmp=7016000000127cYAAQ">A quickstart guide to Ansible</a></li>
<li><a href="https://opensource.com/tags/automation?intcmp=7016000000127cYAAQ">More articles about open source automation</a></li>
</ul></div>
</div>
</div>
</div>
<p>It's important to note that almost all moderate- to low-priced IoT devices use the 2.4GHz wireless spectrum. Why does this matter? Well, 2.4GHz has been around so long that virtually all devices—from cordless phones to smart bulbs—use this spectrum. In most countries, there are generally only about a dozen channels that off-the-shelf devices can broadcast and receive on. Like overloading a cell tower when too many users attempt to make phone calls during an emergency, channels can become overcrowded and susceptible to outside interference.</p>
<p>While well-behaving smart devices use little-to-no bandwidth, if they struggle to send/receive messages due to overcrowding on the spectrum, your automation will have mixed results. A WiFi access point can only communicate with one client at a time. That means the more devices you have on WiFi, the greater the chance that someone on the network will have to wait their turn to communicate.</p>
<p><strong>Pros:</strong></p>
<ul><li>Ubiquitous</li>
<li>Tend to be inexpensive</li>
<li>Easy to set up</li>
<li>Easy to extend the range</li>
<li>Uses existing network</li>
<li>Requires no hub</li>
</ul><p><strong>Cons:</strong></p>
<ul><li>Can suffer from interference from neighboring devices or adjacent networks</li>
<li>Uses the most populated 2.4GHz spectrum</li>
<li>Your router limits the number of devices</li>
<li>Uses more power, which means less or no battery-powered devices</li>
<li>Has the potential to impact latency-sensitive activities like gaming over WiFi</li>
<li>Most off-the-shelf products require an internet connection</li>
</ul><h2 id="z-wave">Z-Wave</h2>
<p><a href="https://www.z-wave.com/" target="_blank">Z-Wave</a> is a closed wireless protocol controlled and maintained by a company named Zensys. Because it is controlled by a single entity, all devices are guaranteed to work together. There is one standard and one implementation. This means that you never have to worry about which device you buy from which manufacturer; they will always work.</p>
<p>Z-Wave operates in the 0.9GHz spectrum, which means it has the largest range of the popular protocols. A central hub is required to coordinate all the devices on a Z-Wave ecosystem. Z-Wave operates on a <a href="https://en.wikipedia.org/wiki/Mesh_networking" target="_blank">mesh network</a> topology, which means that every device acts as a potential repeater for other devices. In theory, this allows a much greater coverage area. Z-Wave limits the number of "hops" to 4. That means that, in order for a signal to get from a device to a hub, it can only travel through four devices. This could be a positive or a negative, depending on your perspective. </p>
<p>On the one hand, it reduces the ecosystem's maximum latency by preventing packets from traveling through a significant number of devices before reaching the destination. The more devices a signal must go through, the longer it can take for devices to become responsive.</p>
<p>On the other hand, it means that you need to be more strategic about providing a good path from your network's extremities back to the hub. Remember, the lower frequency that enables greater distance also limits the speed and amount of data that can be transferred. This is currently not an issue, but no one knows what size messages future smart devices will want to send.</p>
<p><strong>Pros:</strong></p>
<ul><li>Z-Wave compatibility guaranteed</li>
<li>Form mesh network </li>
<li>Low powered and can be battery powered</li>
<li>Mesh networks become more reliable with more devices</li>
<li>Uses 0.9GHz and can transmit up to 100 meters</li>
<li>Least likely of the three to have signal interference from solid objects or external sources</li>
</ul><p><strong>Cons:</strong></p>
<ul><li>Closed protocol</li>
<li>Costs the most</li>
<li>Maximum of four hops in the mesh</li>
<li>Can support up to 230 devices per network</li>
<li>Uses 0.9GHz, which is the slowest of all protocols</li>
</ul><h2 id="zigbee">Zigbee</h2>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on Automation</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/engage/automated-enterprise-ebook-20171115?intcmp=7016000000127cYAAQ">Download now: The automated enterprise eBook</a></li>
<li><a href="https://www.redhat.com/en/services/training/do007-ansible-essentials-simplicity-automation-technical-overview?intcmp=7016000000127cYAAQ">Free online course: Ansible essentials</a></li>
<li><a href="https://opensource.com/downloads/ansible-cheat-sheet?intcmp=7016000000127cYAAQ">Ansible cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/home-automation-ebook?intcmp=7016000000127cYAAQ">eBook: A practical guide to home automation using open source tools</a></li>
<li><a href="https://opensource.com/downloads/ansible-quickstart?intcmp=7016000000127cYAAQ">A quickstart guide to Ansible</a></li>
<li><a href="https://opensource.com/tags/automation?intcmp=7016000000127cYAAQ">More articles about open source automation</a></li>
</ul></div>
</div>
</div>
</div>
<p>Unlike Z-Wave, <a href="https://zigbeealliance.org/" target="_blank">Zigbee</a> is an open standard. This can be a pro or a con, depending on your perspective. Because it is an open standard, manufacturers are free to alter the implementation to suit their products. To borrow an analogy from one of my favorite YouTube channels, <a href="https://www.youtube.com/channel/UC2gyzKcHbYfqoXA5xbyGXtQ" target="_blank">The Hook Up</a>, Zigbee is like going through a restaurant drive-through. Having the same standard means you will always be able to speak to the restaurant and they will be able to hear you. However, if you speak a different language than the drive-through employee, you won't be able to understand each other. Both of you can speak and hear each other, but the meaning will be lost.</p>
<p>Similarly, the Zigbee standard allows all devices on a Zigbee network to "hear" each other, but different implementations mean they may not "understand" each other. Fortunately, more often than not, your Zigbee devices should be able to interoperate. However, there is a non-trivial chance that your devices will not be able to understand each other. When this happens, you may end up with multiple networks that could interfere with each other.</p>
<p>Like Z-Wave, Zigbee employs a mesh network topology but has no limit to the number of "hops" devices can use to communicate with the hub. This, combined with some tweaks to the standard, means that Zigbee theoretically can support more than 65,000 devices on a single network.</p>
<p><strong>Pros:</strong></p>
<ul><li>Open standard</li>
<li>Form mesh network</li>
<li>Low-powered and can be battery powered</li>
<li>Can support over 65,000 devices</li>
<li>Can communicate faster than Z-Wave</li>
</ul><p><strong>Cons:</strong></p>
<ul><li>No guaranteed compatibility</li>
<li>Can form separate mesh networks that interfere with each other</li>
<li>Uses the oversaturated 2.4GHz spectrum</li>
<li>Transmits only 10 to 30 meters</li>
</ul><h2 id="pick-your-protocol">Pick your protocol</h2>
<p>Perhaps you already have some smart devices. Or maybe you are just starting to investigate your options. There is a lot to consider when you're buying devices. Rather than focusing on the lights, sensors, smart plugs, thermometers, and the like, it's perhaps more important to know which protocol (WiFi, Z-Wave, or Zigbee) you want to use.</p>
<p>Whew! I am finally done laying home automation groundwork. In the next article, I will show you how to start the initial installation and configuration of a Home Assistant virtual machine.</p>
