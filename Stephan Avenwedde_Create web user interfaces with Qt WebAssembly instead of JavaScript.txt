<p>When I first heard about <a href="https://webassembly.org/" target="_blank">WebAssembly</a> and the possibility of creating web user interfaces with Qt, just like I would in ordinary C++, I decided to take a deeper look at the technology.</p>
<p>My open source project <a href="https://github.com/hANSIc99/Pythonic" target="_blank">Pythonic</a> is completely Python-based (PyQt), and I use C++ at work; therefore, this minimal, straightforward WebAssembly tutorial uses Python on the backend and C++ Qt WebAssembly for the frontend. It is aimed at programmers who, like me, are not familiar with web development.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Header Qt C++ frontend"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/cpp_qt.png" width="550" height="566" alt="Header Qt C++ frontend" title="Header Qt C++ frontend" /></div>
      
  </article></p>
<h2>TL;DR</h2>
<pre><code class="language-text">git clone https://github.com/hANSIc99/wasm_qt_example

cd wasm_qt_example

python mysite.py</code></pre><p>Then visit <a href="http://127.0.0.1:7000">http://127.0.0.1:7000</a> with your favorite browser.</p>
<h2 id="what-is-webassembly">What is WebAssembly?</h2>
<p>WebAssembly (often shortened to Wasm) is designed primarily to execute portable binary code in web applications to achieve high-execution performance. It is intended to coexist with JavaScript, and both frameworks are executed in the same sandbox. <a href="https://pspdfkit.com/blog/2018/a-real-world-webassembly-benchmark/" target="_blank">Recent performance benchmarks</a> showed that WebAssembly executes roughly 10–40% faster,  depending on the browser, and given its novelty, we can still expect improvements. The downside of this great execution performance is its widespread adoption as the preferred malware language. Crypto miners especially benefit from its performance and harder detection of evidence due to its binary format.</p>
<h2 id="toolchain">Toolchain</h2>
<p>There is a <a href="https://wiki.qt.io/Qt_for_WebAssembly#Getting_Started" target="_blank">getting started guide</a> on the Qt wiki. I recommend sticking exactly to the steps and versions mentioned in this guide. You may need to select your Qt version carefully, as different versions have different features (such as multi-threading), with improvements happening with each release.</p>
<p>To get executable WebAssembly code, simply pass your Qt C++ application through <a href="https://emscripten.org/docs/introducing_emscripten/index.html" target="_blank">Emscripten</a>. Emscripten provides the complete toolchain, and the build script couldn't be simpler:</p>
<pre><code class="language-cpp">#!/bin/sh
source ~/emsdk/emsdk_env.sh
~/Qt/5.13.1/wasm_32/bin/qmake
make</code></pre><p>Building takes roughly 10 times longer than with a standard C++ compiler like Clang or g++. The build script will output the following files:</p>
<ul><li>WASM_Client.js</li>
<li>WASM_Client.wasm</li>
<li>qtlogo.svg</li>
<li>qtloader.js</li>
<li>WASM_Client.html</li>
<li>Makefile (intermediate)</li>
</ul><p>The versions on my (Fedora 30) build system are:</p>
<ul><li>emsdk: 1.38.27</li>
<li>Qt: 5.13.1</li>
</ul><h2 id="frontend">Frontend</h2>
<p>The frontend provides some functionalities based on <a href="https://en.wikipedia.org/wiki/WebSocket" target="_blank">WebSocket</a>.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Qt-made frontend in browser"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/wasm_frontend.png" width="550" height="300" alt="Qt-made frontend in browser" title="Qt-made frontend in browser" /></div>
      
  </article></p>
<ul><li><strong>Send message to server:</strong> Send a simple string message to the server with a WebSocket. You could have done this also with a simple HTTP POST request.</li>
<li><strong>Start/stop timer:</strong> Create a WebSocket and start a timer on the server to send messages to the client at a regular interval.</li>
<li><strong>Upload file:</strong> Upload a file to the server, where the file is saved to the home directory (<strong>~/</strong>) of the user who runs the server.</li>
</ul><p>If you adapt the code and face a compiling error like this:</p>
<pre><code class="language-text">error: static_assert failed due to
 requirement ‘bool(-1 == 1)’ “Required feature http for file
 ../../Qt/5.13.1/wasm_32/include/QtNetwork/qhttpmultipart.h not available.”
QT_REQUIRE_CONFIG(http);</code></pre><p>it means that the requested feature is not available for Qt Wasm.</p>
<h2 id="backend">Backend</h2>
<p>The server work is done by <a href="https://eventlet.net/" target="_blank">Eventlet</a>. I chose Eventlet because it is lightweight and easy to use. Eventlet provides WebSocket functionality and supports threading.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Decorated functions for WebSocket handling"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/python_backend.png" width="675" height="473" alt="Decorated functions for WebSocket handling" title="Decorated functions for WebSocket handling" /></div>
      
  </article></p>
<p>Inside the repository under <strong>mysite/template</strong>, there is a symbolic link to <strong>WASM_Client.html</strong> in the root path. The static content under <strong>mysite/static</strong> is also linked to the root path of the repository. If you adapt the code and do a recompile, you just have to restart Eventlet to update the content to the client.</p>
<p>Eventlet uses the Web Server Gateway Interface for Python (WSGI). The functions that provide the specific functionality are extended with decorators.</p>
<p>Please note that this is an absolute minimum server implementation. It doesn't implement any multi-user capabilities — every client is able to start/stop the timer, even for other clients.</p>
<h2 id="conclusion">Conclusion</h2>
<p>Take this example code as a starting point to get familiar with WebAssembly without wasting time on minor issues. I don't make any claims for completeness nor best-practice integration. I walked through a long learning curve until I got it running to my satisfaction, and I hope this gives you a brief look into this promising technology.</p>
