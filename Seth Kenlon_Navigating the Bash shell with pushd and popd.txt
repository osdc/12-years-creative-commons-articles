<p>The <strong>pushd</strong> and <strong>popd</strong> commands are built-in features of the Bash shell to help you "bookmark" directories for quick navigation between locations on your hard drive. You might already feel that the terminal is an impossibly fast way to navigate your computer; in just a few key presses, you can go anywhere on your hard drive, attached storage, or network share. But that speed can break down when you find yourself going back and forth between directories, or when you get "lost" within your filesystem. Those are precisely the problems <strong>pushd</strong> and <strong>popd</strong> can help you solve.</p>
<h2 id="pushd">pushd</h2>
<p>At its most basic, <strong>pushd</strong> is a lot like <strong>cd</strong>. It takes you from one directory to another. Assume you have a directory called <strong>one</strong>, which contains a subdirectory called <strong>two</strong>, which contains a subdirectory called <strong>three</strong>, and so on. If your current working directory is <strong>one</strong>, then you can move to <strong>two</strong> or <strong>three</strong> or anywhere with the <strong>cd</strong> command:</p>
<pre><code class="language-bash">$ pwd
one
$ cd two/three
$ pwd
three</code></pre><p>You can do the same with <strong>pushd</strong>:</p>
<pre><code class="language-bash">$ pwd
one
$ pushd two/three
~/one/two/three ~/one
$ pwd
three</code></pre><p>The end result of <strong>pushd</strong> is the same as <strong>cd</strong>, but there's an additional intermediate result: <strong>pushd</strong> echos your destination directory and your point of origin. This is your <em>directory stack</em>, and it is what makes <strong>pushd</strong> unique.</p>
<h2 id="stacks">Stacks</h2>
<p>A stack, in computer terminology, refers to a collection of elements. In the context of this command, the elements are directories you have recently visited by using the <strong>pushd</strong> command. You can think of it as a history or a breadcrumb trail.</p>
<p>You can move all over your filesystem with <strong>pushd</strong>; each time, your previous and new locations are added to the stack:</p>
<pre><code class="language-bash">$ pushd four
~/one/two/three/four ~/one/two/three ~/one
$ pushd five
~/one/two/three/four/five ~/one/two/three/four ~/one/two/three ~/one</code></pre><h2 id="navigating-the-stack">Navigating the stack</h2>
<p>Once you've built up a stack, you can use it as a collection of bookmarks or fast-travel waypoints. For instance, assume that during a session you're doing a lot of work within the <strong>~/one/two/three/four/five</strong> directory structure of this example. You know you've been to <strong>one</strong> recently, but you can't remember where it's located in your <strong>pushd</strong> stack. You can view your stack with the <strong>+0</strong> (that's a plus sign followed by a zero) argument, which tells <strong>pushd</strong> not to change to any directory in your stack, but also prompts <strong>pushd</strong> to echo your current stack:</p>
<pre><code class="language-bash">$ pushd +0
~/one/two/three/four ~/one/two/three ~/one ~/one/two/three/four/five</code></pre><p>Alternatively, you can view the stack with the <strong>dirs </strong>command, and you can see the index number for each directory by using the <strong>-v </strong>option:</p>
<pre><code class="language-bash">$ dirs -v
0  ~/one/two/three/four
1  ~/one/two/three 
2  ~/one
3  ~/one/two/three/four/five</code></pre><p>The first entry in your stack is your current location. You can confirm that with <strong>pwd</strong> as usual:</p>
<pre><code class="language-bash">$ pwd
~/one/two/three/four</code></pre><p>Starting at 0 (your current location and the first entry of your stack), the <em>second</em> element in your stack is <strong>~/one</strong>, which is your desired destination. You can move forward in your stack using the <strong>+2</strong> option:</p>
<pre><code class="language-bash">$ pushd +2
~/one ~/one/two/three/four/five ~/one/two/three/four ~/one/two/three
$ pwd
~/one</code></pre><p>This changes your working directory to <strong>~/one</strong> and also has shifted the stack so that your new location is at the front.</p>
<p>You can also move backward in your stack. For instance, to quickly get to <strong>~/one/two/three</strong> given the example output, you can move back by one, keeping in mind that <strong>pushd</strong> starts with 0:</p>
<pre><code class="language-bash">$ pushd -0
~/one/two/three ~/one ~/one/two/three/four/five ~/one/two/three/four</code></pre><h2 id="adding-to-the-stack">Adding to the stack</h2>
<p>You can continue to navigate your stack in this way, and it will remain a static listing of your recently visited directories. If you want to add a directory, just provide the directory's path. If a directory is new to the stack, it's added to the list just as you'd expect:</p>
<pre><code class="language-bash">$ pushd /tmp
/tmp ~/one/two/three ~/one ~/one/two/three/four/five ~/one/two/three/four</code></pre><p>But if it already exists in the stack, it's added a second time:</p>
<pre><code class="language-bash">$ pushd ~/one
~/one /tmp ~/one/two/three ~/one ~/one/two/three/four/five ~/one/two/three/four</code></pre><p>While the stack is often used as a list of directories you want quick access to, it is really a true history of where you've been. If you don't want a directory added redundantly to the stack, you must use the <strong>+N</strong> and <strong>-N</strong> notation.</p>
<h2 id="removing-directories-from-the-stack">Removing directories from the stack</h2>
<p>Your stack is, obviously, not immutable. You can add to it with <strong>pushd</strong> or remove items from it with <strong>popd</strong>.</p>
<p>For instance, assume you have just used <strong>pushd</strong> to add <strong>~/one</strong> to your stack, making <strong>~/one</strong> your current working directory. To remove the first (or "zeroeth," if you prefer) element:</p>
<pre><code class="language-bash">$ pwd
~/one
$ popd +0
/tmp ~/one/two/three ~/one ~/one/two/three/four/five ~/one/two/three/four
$ pwd
~/one</code></pre><p>Of course, you can remove any element, starting your count at 0:</p>
<pre><code class="language-bash">$ pwd ~/one
$ popd +2 
/tmp ~/one/two/three ~/one/two/three/four/five ~/one/two/three/four
$ pwd ~/one</code></pre><p>You can also use <strong>popd</strong> from the back of your stack, again starting with 0. For example, to remove the final directory from your stack:</p>
<pre><code class="language-bash">$ popd -0
/tmp ~/one/two/three ~/one/two/three/four/five</code></pre><p>When used like this, <strong>popd</strong> does not change your working directory. It only manipulates your stack.</p>
<h2 id="navigating-with-popd">Navigating with popd</h2>
<p>The default behavior of <strong>popd</strong>, given no arguments, is to remove the first (zeroeth) item from your stack and make the next item your current working directory.</p>
<p>This is most useful as a quick-change command, when you are, for instance, working in two different directories and just need to duck away for a moment to some other location. You don't have to think about your directory stack if you don't need an elaborate history:</p>
<pre><code class="language-bash">$ pwd
~/one
$ pushd ~/one/two/three/four/five
$ popd
$ pwd
~/one</code></pre><p>You're also not required to use <strong>pushd</strong> and <strong>popd</strong> in rapid succession. If you use <strong>pushd</strong> to visit a different location, then get distracted for three hours chasing down a bug or doing research, you'll find your directory stack patiently waiting (unless you've ended your terminal session):</p>
<pre><code class="language-bash">$ pwd ~/one
$ pushd /tmp
$ cd {/etc,/var,/usr}; sleep 2001
[...]
$ popd
$ pwd
~/one</code></pre><h2 id="pushd-and-popd-in-the-real-world">Pushd and popd in the real world</h2>
<p>The <strong>pushd</strong> and <strong>popd</strong> commands are surprisingly useful. Once you learn them, you'll find excuses to put them to good use, and you'll get familiar with the concept of the directory stack. Getting comfortable with <strong>pushd</strong> was what helped me understand <strong>git stash</strong>, which is entirely unrelated to <strong>pushd</strong> but similar in conceptual intangibility.</p>
<p>Using <strong>pushd</strong> and <strong>popd</strong> in shell scripts can be tempting, but generally, it's probably best to avoid them. They aren't portable outside of Bash and Zsh, and they can be obtuse when you're re-reading a script (<strong>pushd +3</strong> is less clear than <strong>cd $HOME/$DIR/$TMP</strong> or similar).</p>
<p>Aside from these warnings, if you're a regular Bash or Zsh user, then you can and should try <strong>pushd</strong> and <strong>popd</strong>.</p>
