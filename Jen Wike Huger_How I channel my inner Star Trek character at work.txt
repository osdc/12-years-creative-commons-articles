<p>In a recent <a href="https://twitter.com/JenWike/status/1318188923298906113" target="_blank">Twitter thread</a>, I self-identified as "some days Deanna, some days Riker." Others shared their own "Star Trek Spectrum," from Worf to O'Brien and McCoy to Neelix. That led me to think more about Deanna Troi: the half-human, half-Betazoid empath who served as Lieutenant Commander and the ship's counselor for most of <em>Star Trek: The Next Generation</em>.</p>
<p>Ever since my middle school days, I felt a kinship with her as I watched her observe and respond to situations and the people in them. Today, as an open source community manager, I think about how these traits are vital to the role of nurturing and guiding a group of people. Here's how I channel my inner Deanna Troi at work.</p>
<p><article class="media media--type-image media--view-mode-full"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/pictures/deannacover001.jpg" width="2000" height="1080" alt="Deanna Troi from StarTrek.com" title="Deanna Troi from StarTrek.com" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>Deanna Troi from <a href="https://www.startrek.com/news/the-radical-empathy-of-deanna-troi" target="_blank" rel="ugc">StarTrek.com</a></p>
<p> </p>
</div>
      
  </article></p>
<h2>Holding space</h2>
<p>Instead of speaking the next word, I take a breath. I try not to rush in with something to say in response to the things that happen and in response to conversations with the people that make up our community. I listen, and then when the time is right, I speak. That can be hard especially when I am feeling confident in how to move forward and am eager to show the way. I've learned that taking a beat and slowing down, rarely does harm if coupled with clarity around the situation. Sometimes it is important to act quickly, to step in and guide the community around an issue, but even swift action can be accompanied with thoughtful deliberation. </p>
<p>To achieve this, also give yourself space to think and slow down. If you need to think through something, go for a walk, make a tea or meal, and say no to that next meeting to give yourself the time you need to do your job with integrity. </p>
<h2>Radical empathy</h2>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More open source career advice</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7013a000002DRawAAG">Open source cheat sheets</a></li>
<li><a href="https://developers.redhat.com/learn/linux-starter-kit-for-developers/?intcmp=7013a000002DRawAAG">Linux starter kit for developers</a></li>
<li><a href="https://www.redhat.com/sysadmin/questions-for-employer?intcmp=7013a000002DRawAAG">7 questions sysadmins should ask a potential employer before taking a job</a></li>
<li><a href="https://www.redhat.com/architect/?intcmp=7013a000002DRawAAG">Resources for IT artchitects</a></li>
<li><a href="https://enterprisersproject.com/article/2019/3/article-cheat-sheet?intcmp=7013a000002DRawAAG">Cheat sheet: IT job interviews</a></li>
</ul></div>
</div>
</div>
</div>
<p>Truly hearing and listening to others, then digesting that information, often leads to empathy. Even when we don't feel the same way—often due to differing perspectives—we can get closer to empathizing with others when we listen closely and fully. And why would we want to do that? Because understanding someone allows us to relate to them, and then we can find a solid place from which to nurture and guide them.</p>
<p>Getting to know each person individually is something I recommend, and if the community you are a part of is too big for that, don't shut down the idea of reaching out at all. Start from where you are, and connect with the next closest person—you have a meeting set up to onboard them, you interacted on social media, you fixed a bug they submitted, etc. This kind of growth is authentic and sustainable.</p>
<h2>Tapping into intuition</h2>
<p>At first glance, it may seem that those best fit for a role at the helm are those who identify as extroverted and easily heard, however, I've found those who know how to silence the noise and follow their own inner certainty are those who can inspire others to do the same. And that is where the power is, inside all of us, not just one of us.</p>
<p>Open source communities are continuing to be called upon to innovate, act, and lead, and these traits are important not just for leaders to cultivate but for everyone involved in working together as a group to achieve a goal or collaborate on a project.</p>
