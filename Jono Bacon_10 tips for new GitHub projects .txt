<p><a href="https://github.com/" target="_blank">GitHub</a> has become a fairly central part of many open source projects. Although many people focus on the code-hosting aspect of GitHub, the platform also includes comprehensive features for issue management, code review, and integration with many other tools and platforms.</p>
<p>For new open source projects, however, getting started and ensuring that GitHub repos are in tip-top shape and ready to attract new developers can be a little overwhelming. To smooth this transition, here are 10 tips for rocking your octo-project and getting your new project off to a great start.</p>
<h2>1. Set a <b>LICENSE</b> file</h2>
<p>The first thing you should do is choose a license for your project and ensure the license text lives in the LICENSE file. There are sadly too many projects on GitHub with a missing license and this makes it more difficult for people to use and contribute code.</p>
<p>If you are unsure which license to choose for your project, there are <a href="https://opensource.org/licenses" target="_blank">great resources</a> online for choosing what is best for you.</p>
<h2>2. Create a <b>README.md</b></h2>
<p>When you visit a GitHub repo for first time, the README.md file is rendered underneath the code. This is a fantastic opportunity to introduce your project, explain how you build the code, and explain how people can get involved and participate. Remember, .md files are <a href="http://daringfireball.net/projects/markdown/" target="_blank">Markdown files</a> and you can use Markdown syntax to make them look all puuuuurty. <i>OK, I admit, that was creepy.</i></p>
<h2>3. Create a <b>CONTRIBUTING.md</b></h2>
<p>In a similar fashion, the CONTRIBUTING.md file is designed to show how people can participate in a GitHub repo. When this file is present, GitHub will display a little note inviting people to learn more about contributing to your project. Users who click on that link will see the contents of CONTRIBUTING.md.</p>
<p>Again, this is a great opportunity to explain how people can participate. In this file you should provide simple step-by-step instructions for how prospective community members can clone and build the code, explore issues/features to work on, fork the code, contribute back their changes, and which communication channels they can use for help and support.</p>
<h2>4. Label bugs as 'Bitesize'</h2>
<p>One of the most difficult areas for a new potential contributor to get started in is learning what they can work on to help with a project. Of course, the list of issues is often a good place to start, but many issues are riddled with context and nuance which can make this difficult.</p>
<p>A simple solution to this is to create a <i>Bitesize</i> label that you can apply to bugs that would be simple for beginners—this could include minor UI issues, string issues, translations fixes, and more. Simply label these bugs when you triage them and then provide a URL in your README.md and CONTRIBUTING.md files to point prospective contributors at the list to get started.</p>
<h2>5. Checkboxes for items</h2>
<p>A neat little feature in GitHub is the ability to use Markdown to add checkboxes to issues. This can be helpful when you file an issue for a larger feature with a number of tasks that need to be completed. It means you then don't need to file lots of issues for each task. To add a checkbox simple use this syntax: <code>[ ]</code></p>
<p>What is neat is that when you add a number of these checkboxes within a single issue, the issue list will show how many checked items have been completed next to the issue title. This provides a handy way of determining which larger issues are making progress and which are not.</p>
<h2>6. Issue templates</h2>
<p>Speaking of issues, one of the most frustrating aspects of managing bugs is when your users submit bugs that are not particularly helpful for a developer to resolve. In general, you want your users to provide the following information:</p>
<ul><li>A crisp and descriptive summary of the issue</li>
<li>A more detailed description of what happens and why it doesn't work</li>
<li>The expect result and how the current is not correct</li>
<li>A step-by-step set of instructions explaining how the developer can reproduce the problem.</li>
<li>A list of details about the user's system (e.g., operating system, version of the software, and any other details</li>
</ul><p>This is a lot of information for a typical user to provide. To make this easier, GitHub has an <a href="https://github.com/blog/2111-issue-and-pull-request-templates" target="_blank">Issue Templates</a> feature where you can pre-fill a new issue with content and questions the user should fill in. Simply create an ISSUE_TEMPLATE.md file in a .github directory in your code and add the Markdown in that file and it will be displayed in the main text box in a new issue. You can do the same for pull requests with a PULL_REQUEST_TEMPLATE.md.</p>
<h2>7. Use the wiki</h2>
<p>One of the most important aspects of creating a strong community is ensuring new participants can learn how to get involved. Ideally you want to provide documentation about your project such as:</p>
<ul><li>A step-by-step tutorial on how to fork the code, work on a new feature, and how the code review process works.</li>
<li>Coding standards, conventions, and requirements.</li>
<li>A breakdown of the code, how it is structured, and what the different source files do.</li>
<li>Details about how the project communicates, makes decisions, and more.</li>
</ul><p>Every GitHub project comes with a built-in wiki that is a perfect place for this kind of content. What's more, each wiki is actually a repository that you can commit to. This makes it easy to split the load up among contributors to maintain documentation about the project and how to participate.</p>
<h2>8. Build a GitHub pages site</h2>
<p>Although the wiki is a great place for documentation, it is not the best place for a website about your project. For this GitHub provides GitHub Pages—a free service for building a project website.</p>
<p>GitHub Pages is powered by <a href="https://jekyllrb.com/" target="_blank">Jekyll</a>, a static content management system. This makes it simple to build a site, maintain it in a repo, and iterate and improve on it.</p>
<h2>9. Waffle.io/ZenHub for planning</h2>
<p>GitHub currently doesn't include particularly comprehensive project management tools. If you are building a project and want to manage target deadlines and assign tasks and feature goals to those deadlines, you might need a project management system. Fortunately there are a few great options, both of which integrate tightly with GitHub:</p>
<ul><li><a href="https://www.zenhub.io/">ZenHub</a> is a particularly comprehensive project management system where you can track work boards, tasks, and TODO items, all of which integrate with existing GitHub projects via issues and other features. ZenHub integrates really nicely and adds a layer of functionality that is noticeably missing in GitHub.</li>
<li><a href="https://waffle.io/">Waffle</a> is a Trello-like platform that allows you to manage kanban cards for tasks that integrate fully with GitHub issues.</li>
</ul><p>I recommend you try both and see which works best for you.</p>
<h2>10. Use Travis CI for continuous integration</h2>
<p>Continuous integration has become a core part of how modern software is being built. The basic gist (pun intended) of CI is that developers should be regularly committing to the repo and automated systems should be regularly building the code with production tools and reporting if the builds fail or pass. This reduces the possibility of developers working on big features that are difficult to land and possibly introduce bugs.</p>
<p><a href="https://travis-ci.org/" target="_blank">Travis CI</a> provides a really simple solution to this. You simply log in with your GitHub account, tell Travis CI what you want to build, and you can see the reports. You can even integrate Travis CI with Heroku and Slack.</p>
<p>So there we have it: 10 GitHub tips to help you get your new GitHub project off to a rocking start. Be sure to let us know your own tips and tricks in the comments.</p>
