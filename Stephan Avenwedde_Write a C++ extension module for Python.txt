<p>In a previous article, I gave an overview of <a href="https://opensource.com/article/22/9/python-interpreters-2022">six Python interpreters</a>. On most systems, the CPython interpreter is the default, and also the poll in my last article showed that CPython is the most popular one. Specific to CPython is the ability to write Python modules in C using CPythons extensions API. Writing Python modules in C allows you to move computation-intensive code to C while preserving the ease of access of Python.</p>
<p>In this article, I’ll show you how to write an extension module. Instead of plain C, I use C++ because most compilers usually understand both. I have to mention one major drawback in advance: Python modules built this way are not portable to other interpreters. They only work in conjunction with the CPython interpreter. So if you are looking for a more portable way of interacting with C libraries, consider using the <a href="https://docs.python.org/3/library/ctypes.html#module-ctypes">ctypes</a> module.</p>
<h2>Source code</h2>
<p>As usual, you can find the related source code on <a href="https://github.com/hANSIc99/PythonCppExtension">GitHub</a>. The C++ files in the repository have the following purpose:</p>
<ul><li><code>my_py_module.cpp</code>: Definition of the Python module <em>MyModule</em></li>
<li><code>my_cpp_class.h</code>: A header-only C++ class which gets exposed to Python</li>
<li><code>my_class_py_type.h/cpp</code>: Python representation of our C++ class</li>
<li><code>pydbg.cpp</code>: Separate application for debugging purpose</li>
</ul><p>The Python module you build in this article won’t have any meaningful use, but is a good example.</p>
<h2>Build the module</h2>
<p>Before looking into the source code, you can check whether the module compiles on your system. <a href="https://opensource.com/article/21/5/cmake">I use CMake</a> for creating the build configuration, so CMake must be installed on your system. In order to configure and build the module, you can either let Python run the process:</p>
<pre>
<code>$ python3 setup.py build</code></pre><p>Or run the process manually:</p>
<pre>
<code>$ cmake -B build
$ cmake --build build</code></pre><p>After that, you have a file called <code>MyModule.so</code> in the <code>/build</code> subdirectory.</p>
<h2>Defining an extension module</h2>
<p>First, take a look on <code>my_py_module.cpp</code>, in particular, the function <code>PyInit_MyModule</code>:</p>
<pre>
<code>PyMODINIT_FUNC
PyInit_MyModule(void) {
    PyObject* module = PyModule_Create(&amp;my_module);
    
    PyObject *myclass = PyType_FromSpec(&amp;spec_myclass);
    if (myclass == NULL){
        return NULL;
    }
    Py_INCREF(myclass);
    
    if(PyModule_AddObject(module, "MyClass", myclass) &lt; 0){
        Py_DECREF(myclass);
        Py_DECREF(module);
        return NULL;
    }
    return module;
}</code></pre><p>This is the most important code in this example because it acts as the entry point for CPython. In general, when a Python C extension is compiled and made available as a shared object binary, CPython searches for the function <code>PyInit_&lt;ModuleName&gt;</code> in the eponymous binary (<code>&lt;ModuleName&gt;.so</code>) and executes it when attempting to import it.</p>
<p>All Python types, whether declarations or instances, are exposed as pointers to <a href="https://docs.python.org/release/3.9.1/c-api/structures.html?highlight=pyobject#c.PyObject">PyObject</a>. In the first part of this function, the root definition of the module is created by running <code>PyModule_Create(...)</code>. As you can see in the module specification (<code>my_module</code>, same file), it doesn’t have any special functionality.</p>
<p>Afterward, <a href="https://docs.python.org/3/c-api/type.html#c.PyType_FromSpec">PyType_FromSpec</a> is called to create a Python <a href="https://docs.python.org/3/c-api/typeobj.html#heap-types">heap type</a> definition for the custom type MyClass. A heap type corresponds to a Python class. The type definition is then assigned to the module MyModule.</p>
<p><em>Note that if one of the functions fails, the reference count of previously created PyObjects must be decremented so that they get deleted by the interpreter.</em></p>
<p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">More Python resources</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://www.redhat.com/en/topics/middleware/what-is-ide?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="What is an IDE?">What is an IDE?</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-python-37-beginners?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Cheat sheet: Python 3.7 for beginners">Cheat sheet: Python 3.7 for beginners</a></div>
              <div class="field__item"><a href="https://opensource.com/resources/python/gui-frameworks?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Top Python GUI frameworks">Top Python GUI frameworks</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/7-essential-pypi-libraries?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Download: 7 essential PyPI libraries">Download: 7 essential PyPI libraries</a></div>
              <div class="field__item"><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Red Hat Developers">Red Hat Developers</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Register for your free Red Hat account">Register for your free Red Hat account</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/python?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Latest Python articles">Latest Python articles</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<h2>Specifying a Python type</h2>
<p>The specification for the type MyClass is found inside <a href="https://github.com/hANSIc99/PythonCppExtension/blob/main/my_class_py_type.h">my_class_py_type.h</a> as an instance of <a href="https://docs.python.org/3/c-api/type.html#c.PyType_Spec">PyType_Spec</a>:</p>
<pre>
<code>static PyType_Spec spec_myclass = {
    "MyClass",                                  // name
    sizeof(MyClassObject) + sizeof(MyClass),    // basicsize
    0,                                          // itemsize
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,   // flags
    MyClass_slots                               // slots
};</code></pre><p>This structure defines some basic type information for the class. The value passed for the size consists of the size of the Python representation (<code>MyClassObject</code>) and the size of the plain C++ class (<code>MyClass</code>). The <code>MyClassObject</code> is defined as follows:</p>
<pre>
<code>typedef struct {
    PyObject_HEAD
    int         m_value;
    MyClass*    m_myclass;
} MyClassObject;</code></pre><p>The Python representation is basically of type <a href="https://docs.python.org/release/3.9.1/c-api/structures.html?highlight=pyobject#c.PyObject">PyObject</a>, defined by the macro <code>PyObject_HEAD</code>, and some additional members. The member <code>m_value</code> is exposed as ordinary class member while the member <code>m_myclass</code> is only accessible from inside C++ code.</p>
<p>The <a href="https://docs.python.org/release/3.9.1/c-api/type.html?highlight=pytype_slot#c.PyType_Slot">PyType_Slot</a> defines some additional functionality:</p>
<pre>
<code>static PyType_Slot MyClass_slots[] = {
    {Py_tp_new,     (void*)MyClass_new},
    {Py_tp_init,    (void*)MyClass_init},
    {Py_tp_dealloc, (void*)MyClass_Dealloc},
    {Py_tp_members, MyClass_members},
    {Py_tp_methods, MyClass_methods},
    {0, 0} /* Sentinel */
};</code></pre><p>Here, the jump addressed for some initialization and de-initialization functions are set as well as ordinary class methods and members. Additional functionality, like assigning an initial attribute dictionary, could also be set, but this is optional. Those definitions usually end with a sentinel, consisting of <code>NULL</code> values.</p>
<p>To complete the type specification, here is the method and member table:</p>
<pre>
<code>static PyMethodDef MyClass_methods[] = {
    {"addOne", (PyCFunction)MyClass_addOne, METH_NOARGS,  PyDoc_STR("Return an incrmented integer")},
    {NULL, NULL} /* Sentinel */
};

static struct PyMemberDef MyClass_members[] = {
    {"value", T_INT, offsetof(MyClassObject, m_value)},
    {NULL} /* Sentinel */
};</code></pre><p>In the method table, the Python method <code>addOne</code> is defined, and it points to the related function <code>MyClass_addOne</code>. This function acts as a wrapper. It invokes the <code>addOne()</code> method in the C++ class.</p>
<p>In the member table, there is just one member defined for demonstration purposes. Unfortunately, the use of <a href="https://en.cppreference.com/w/cpp/types/offsetof">offsetof</a> in <a href="https://docs.python.org/release/3.9.1/c-api/structures.html?highlight=pymemberdef#c.PyMemberDef">PyMemberDef</a> doesn’t allow C++ specific types to be added to <code>MyClassObject</code>. If you try to place some C++ type container (such as <a href="https://en.cppreference.com/w/cpp/utility/optional">std::optional</a>), the compiler complains about it in the form of warnings related to memory layout.</p>
<h2>Initialization and de-initialization</h2>
<p>The method <code>MyClass_new</code> acts only to provide initial values for <code>MyClassObject</code> and allocates memory for the base type:</p>
<pre>
<code>PyObject *MyClass_new(PyTypeObject *type, PyObject *args, PyObject *kwds){
    std::cout &lt;&lt; "MtClass_new() called!" &lt;&lt; std::endl;

    MyClassObject *self;
    self = (MyClassObject*) type-&gt;tp_alloc(type, 0);
    if(self != NULL){ // -&gt; allocation successfull
        // assign initial values
        self-&gt;m_value   = 0;
        self-&gt;m_myclass = NULL; 
    }
    return (PyObject*) self;
}</code></pre><p>Actual initialization takes place in <code>MyClass_init</code>, which corresponds to the <a href="https://docs.python.org/3/library/dataclasses.html?highlight=__init__">__init__()</a> method in Python:</p>
<pre>
<code>int MyClass_init(PyObject *self, PyObject *args, PyObject *kwds){
    
    ((MyClassObject *)self)-&gt;m_value = 123;
    
    MyClassObject* m = (MyClassObject*)self;
    m-&gt;m_myclass = (MyClass*)PyObject_Malloc(sizeof(MyClass));

    if(!m-&gt;m_myclass){
        PyErr_SetString(PyExc_RuntimeError, "Memory allocation failed");
        return -1;
    }

    try {
        new (m-&gt;m_myclass) MyClass();
    } catch (const std::exception&amp; ex) {
        PyObject_Free(m-&gt;m_myclass);
        m-&gt;m_myclass = NULL;
        m-&gt;m_value   = 0;
        PyErr_SetString(PyExc_RuntimeError, ex.what());
        return -1;
    } catch(...) {
        PyObject_Free(m-&gt;m_myclass);
        m-&gt;m_myclass = NULL;
        m-&gt;m_value   = 0;
        PyErr_SetString(PyExc_RuntimeError, "Initialization failed");
        return -1;
    }

    return 0;
}</code></pre><p>If you want to have arguments passed during initialization, you must call <a href="https://docs.python.org/3/c-api/arg.html#c.PyArg_ParseTuple">PyArg_ParseTuple</a> at this point. For the sake of simplicity, all arguments passed during initialization are ignored in this example. In the first part of the function, the <code>PyObject</code> pointer (<code>self</code>) is reinterpreted to a pointer to <code>MyClassObject</code> in order to get access to our additional members. Additionally, the memory for the C++ class is allocated and its constructor is executed.</p>
<p>Note that exception handling and memory allocation (and de-allocation) must be carefully done in order to prevent memory leaks. When the reference count drops to zero, the <code>MyClass_dealloc</code> function takes care of freeing all related heap memory. There’s a <a href="https://docs.python.org/3/c-api/memory.html">dedicated chapter</a> in the documentation about memory management for C and C++ extensions.</p>
<h2>Method wrapper</h2>
<p>Calling a related C++ class method from the Python class is easy:</p>
<pre>
<code>PyObject* MyClass_addOne(PyObject *self, PyObject *args){
    assert(self);

    MyClassObject* _self = reinterpret_cast&lt;MyClassObject*&gt;(self);
    unsigned long val = _self-&gt;m_myclass-&gt;addOne();
    return PyLong_FromUnsignedLong(val);
}</code></pre><p>Again, the <code>PyObject*</code> argument (<code>self</code>) is casted to <code>MyClassObject*</code> in order to get access to <code>m_myclass</code>, a pointer to the C++ class instance. With this information, the classes method <code>addOne()</code> is called and the result is returned in form of a <a href="https://docs.python.org/3/c-api/long.html">Python integer object</a>.</p>
<h2>3 ways to debug</h2>
<p>For debugging purposes, it can be valuable to compile the CPython interpreter in debugging configuration. A detailed description can be found in the <a href="https://docs.python.org/3/c-api/intro.html#debugging-builds">official documentation</a>. It’s possible to follow the next steps, as long as additional debug symbols for the pre-installed interpreter are downloaded.</p>
<h3>GNU Debugger</h3>
<p>Good old <a href="https://opensource.com/article/21/3/debug-code-gdb" target="_blank">GNU Debugger (GDB)</a> is, of course, also useful here. I include a <a href="https://github.com/hANSIc99/PythonCppExtension/blob/main/gdbinit">gdbinit</a> file, defining some options and breakpoints. There’s also the <a href="https://github.com/hANSIc99/PythonCppExtension/blob/main/gdb.sh">gdb.sh</a> script, which creates a debug build and initiates a GDB session:</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-11/gdb_session_b_0.png" width="1144" height="574" alt="Gnu Debugger (GDB) is useful for your Python C and C++ extensions." /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Stephan Avenwedde, CC BY-SA 4.0)</p>
</div>
      
  </article><p>GDB invokes the CPython interpreter with the script file <a href="https://github.com/hANSIc99/PythonCppExtension/blob/main/main.py">main.py</a>. The script file allows you to easily define all the actions you want to perform with the Python extension module.</p>
<h3>C++ application</h3>
<p>Another approach is to embed the CPython interpreter in a separate C++ application. In the repository, this can be found in the file <a href="https://github.com/hANSIc99/PythonCppExtension/blob/main/pydbg.cpp">pydbg.cpp</a>:</p>
<pre>
<code>int main(int argc, char *argv[], char *envp[])
{
    Py_SetProgramName(L"DbgPythonCppExtension");
    Py_Initialize();

    PyObject *pmodule = PyImport_ImportModule("MyModule");
    if (!pmodule) {
        PyErr_Print();
        std::cerr &lt;&lt; "Failed to import module MyModule" &lt;&lt; std::endl;
        return -1;
    }

    PyObject *myClassType = PyObject_GetAttrString(pmodule, "MyClass");
    if (!myClassType) {
        std::cerr &lt;&lt; "Unable to get type MyClass from MyModule" &lt;&lt; std::endl;
        return -1;
    }

    PyObject *myClassInstance = PyObject_CallObject(myClassType, NULL);

    if (!myClassInstance) {
        std::cerr &lt;&lt; "Instantioation of MyClass failed" &lt;&lt; std::endl;
        return -1;
    }

    Py_DecRef(myClassInstance); // invoke deallocation
    return 0;
}</code></pre><p>Using the <a href="https://docs.python.org/3/extending/embedding.html#very-high-level-embedding">high level interface</a>, it’s possible to include the extension module and perform actions on it. This allows you to debug in the native IDE environment. It also gives you finer control of the variables passed from and to the extension module.</p>
<p>The drawback is the high expense of creating an extra application.</p>
<h3>VSCode and VSCodium LLDB extension</h3>
<p>Using a debugger extension like <a href="https://github.com/vadimcn/vscode-lldb">CodeLLDB</a> is probably the most convenient debugging option. The repository includes the VSCode or VSCodium configuration files for building the extension (<a href="https://github.com/hANSIc99/PythonCppExtension/blob/main/.vscode/tasks.json">task.json</a>, <a href="https://github.com/microsoft/vscode-cmake-tools">CMake Tools</a>) and invoking the debugger (<a href="https://github.com/hANSIc99/PythonCppExtension/blob/main/.vscode/launch.json">launch.json</a>). This approach combines the advantages of the previous ones: Debugging in an graphical IDE, defining actions in a Python script file or even dynamically in the interpreter prompt.</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-11/vscodium_debug_session.png" width="1389" height="908" alt="VSCodium features an integrated debugger." /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Stephan Avenwedde, CC BY-SA 4.0)</p>
</div>
      
  </article><h2>Extend C++ with Python</h2>
<p>All functionality available from Python code is also available from within a C or C++ extension. While coding in Python is often considered as an easy win, extending Python in C or C++ can also be a pain. On the other hand, while native Python code is slower than C++, a C or C++ extension makes it possible to elevate a computation-intensive task to the speed of native machine code.</p>
<p>You must also consider the usage of an ABI. The stable ABI provides a way to maintain backwards compatibility to older versions of CPython as described in <a href="https://docs.python.org/3/c-api/stable.html">the documentation</a>.</p>
<p>In the end, you must weigh the advantages and disadvantages yourself. Should you decide to use C extensions to make certain functionality available to you in Python, you’ve seen how it can be done.</p>
