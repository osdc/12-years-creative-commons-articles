<p>In the Java programming language, a hashmap is a list of associated values. Java uses hashmaps to store data. If you keep a lot of structured data, it's useful to know the various retrieval methods available.</p>
<p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">More on Java</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://developers.redhat.com/topics/enterprise-java/?intcmp=7013a000002Cxq6AAC" data-analytics-category="resource list" data-analytics-text="What is enterprise Java programming?">What is enterprise Java programming?</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/engage/open-source-alternative-s-202104291240?intcmp=7013a000002Cxq6AAC" data-analytics-category="resource list" data-analytics-text="An open source alternative to Oracle JDK">An open source alternative to Oracle JDK</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/java-cheat-sheet?intcmp=7013a000002Cxq6AAC" data-analytics-category="resource list" data-analytics-text="Java cheat sheet">Java cheat sheet</a></div>
              <div class="field__item"><a href="https://developers.redhat.com/products/openjdk/overview?intcmp=7013a000002Cxq6AAC" data-analytics-category="resource list" data-analytics-text="Red Hat build of OpenJDK">Red Hat build of OpenJDK</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/services/training/do092-developing-cloud-native-applications-microservices-architectures?intcmp=7013a000002Cxq6AAC" data-analytics-category="resource list" data-analytics-text="Free online course: Developing cloud-native applications with microservices">Free online course: Developing cloud-native applications with microservices</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Register for your free Red Hat account">Register for your free Red Hat account</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/java?intcmp=7013a000002Cxq6AAC" data-analytics-category="resource list" data-analytics-text="Fresh Java articles">Fresh Java articles</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<h2>Create a hashmap</h2>
<p>Create a hashmap in Java by importing and using the <code>HashMap</code> class. When you create a hashmap, you must define what constitutes a valid value (such as <code>Integer</code> or <code>String</code>). A hashmap holds pairs of values.</p>
<pre>
<code class="language-java">package com.opensource.example;

import java.util.HashMap;

public class Mapping {
    public static void main(String[] args) {
        HashMap&lt;String, String&gt; myMap = new HashMap&lt;&gt;();
        myMap.put("foo", "hello");
        myMap.put("bar", "world");

        System.out.println(myMap.get("foo") + " " + myMap.get("bar"));
        System.out.println(myMap.get("hello") + " " + myMap.get("world"));
    }
}
</code></pre><p>The <code>put</code> method allows you to add data to your hashmap, and the <code>get</code> method retrieves data from the hashmap.</p>
<p>Run the code to see the output. Assuming you've saved the code in a file called <code>main.java</code>, you can run it directly with the <code>java</code> command:</p>
<pre>
<code class="language-bash">$ java ./main.java
hello world
null null</code></pre><p>Calling the second values in the hashmap returns <code>null</code>, so the first value is essentially a key and the second a value. This is known as a dictionary or associative array in some languages.</p>
<p>You can mix and match the types of data you put into a hashmap as long as you tell Java what to expect when creating it:</p>
<pre>
<code class="language-java">package com.opensource.example;

import java.util.HashMap;

public class Mapping {
    public static void main(String[] args) {
        HashMap&lt;Integer, String&gt; myMap = new HashMap&lt;&gt;();
        myMap.put(71, "zombie");
        myMap.put(2066, "apocalypse");

        System.out.println(myMap.get(71) + " " + myMap.get(2066));
    }
}</code></pre><p>Run the code:</p>
<pre>
<code class="language-bash">$ java ./main.java
zombie apocalypse</code></pre><h2>Iterate over a hashmap with forEach</h2>
<p>There are many ways to retrieve all data pairs in a hashmap, but the most direct method is a <code>forEach</code> loop:</p>
<pre>
<code class="language-java">package com.opensource.example;

import java.util.HashMap;

public class Mapping {

    public static void main(String[] args) {
        HashMap&lt;String, String&gt; myMap = new HashMap&lt;&gt;();
        myMap.put("foo", "hello");
        myMap.put("bar", "world");

        // retrieval
        myMap.forEach( (key, value) -&gt;
                System.out.println(key + ": " + value));
    }
}</code></pre><p>Run the code:</p>
<pre>
<code class="language-bash">$ java ./main.java
bar: world
foo: hello</code></pre><h2>Structured data</h2>
<p>Sometimes it makes sense to use a Java hashmap so you don't have to keep track of dozens of individual variables. Once you understand how to structure and retrieve data in a language, you're empowered to generate complex data in an organized and convenient way. A hashmap isn't the only data structure you'll ever need in Java, but it's a great one for related data.</p>
