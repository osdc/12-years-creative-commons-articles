<p>Linux offers versatile user/group structures. In this article, we will explore how to create and add users to a group.</p>
<p><em>Note: These instructions work when using Red Hat Enterprise Linux, Fedora, and CentOS. They have also been verified on Fedora. </em></p>
<h2>Users</h2>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Linux resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux commands cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Free online course: RHEL Technical Overview</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux networking cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">SELinux cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux common commands cheat sheet</a></li>
<li><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">What are Linux containers?</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Our latest Linux articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>In Linux, every process has an associated user, which tells you who initiated the process. Every file/directory is owned by a user and a group. Users who are associated with a file/directory can tell which user has access to that file and what they can do with it. A user who is associated with a process determines what that process can access.</p>
<h2>Groups</h2>
<p>Groups are collections of users. Groups determine the specific access rights users have to files, directories, and processes. As shown below, a user can be part of more than one group at a given time.</p>
<p>To see what user you are logged in as and what groups you are in, run the <code>id</code> command:</p>
<p><article class="media media--type-image media--view-mode-full" title="image 1"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/life-uploads/image_1.png" width="733" height="99" alt="image 1" title="image 1" /></div>
      
  </article></p>
<p>For example, <code>uid=1000(kkulkarn)</code> shows that I am logged in as <code>kkulkarn</code> (my username) and my user id is <code>1000</code>.</p>
<p><code>gid=1000(kkulkarn)</code> tells what primary group I am in, and <code>groups=...</code> tells what other groups I am in. These other groups are known as supplementary groups.</p>
<h2>Creating a user</h2>
<p>Run the command shown below to create the user <code>alice</code>. <code>sudo</code> is required as a prefix if you get a <code>Permission denied</code> error.</p>
<p><article class="media media--type-image media--view-mode-full" title="Image 2"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/life-uploads/image_2.png" width="240" height="19" alt="Image 2" title="Image 2" /></div>
      
  </article></p>
<p>Since we did not set a password when we created the user alice, to switch users and become alice, we need to run following command:</p>
<pre>
<code class="language-text">sudo passwd alice</code></pre><p>It will prompt: <code>New password</code> and <code>Retype new password</code>. I set the password as <code>demo,</code> and the system responded: <code>BAD PASSWORD</code> because it is too short and therefore vulnerable to attacks. But I continued, and the password was set; here is the following message output:</p>
<pre>
<code class="language-text">passwd: all authentication tokens updated successfully.</code></pre><p>
<article class="media media--type-image media--view-mode-full"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/useraddterminal.png" width="669" height="109" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>Screenshot by author, CC BY</p>
</div>
      
  </article></p>
<p>Now let’s switch to the new user, alice, by using <code>su - alice,</code> as shown below. Enter the password <code>demo</code> when prompted.</p>
<p><article class="media media--type-image media--view-mode-full" title="image 4"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/life-uploads/image_4.png" width="330" height="117" alt="image 4" title="image 4" /></div>
      
  </article></p>
<p>As you can see, the prompt shows that now we are working as <code><a href="mailto:%60alice@localhost">alice@localhost</a>.</code>Check <code>pwd</code> (the present working directory) and you will see we are in the <code>home</code> directory for the user alice.</p>
<p>Note: To use <code>sudo,</code> you need to be part of a supplementary group called <code>wheel</code>; otherwise you may see an error: &lt;<code>username&gt; is not in sudoers file. This incident will be reported</code>:</p>
<p><article class="media media--type-image media--view-mode-full" title="image 5"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/life-uploads/image_5.png" width="630" height="91" alt="image 5" title="image 5" /></div>
      
  </article></p>
<p>Here’s how to fix that situation.</p>
<h2>How to add alice to the group ‘wheel’ to give sudo access</h2>
<p>Run:</p>
<pre>
<code><code class="language-text">id alice</code></code></pre><p>and you will see the following output:</p>
<p><article class="media media--type-image media--view-mode-full" title="image 6"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/life-uploads/image_6.png" width="709" height="69" alt="image 6" title="image 6" /></div>
      
  </article></p>
<p>That tells us what primary and supplementary groups alice is part of.</p>
<p>Let’s modify alice to be part of group <code>wheel</code> using the following command:</p>
<p><article class="media media--type-image media--view-mode-full" title="image 7"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/life-uploads/image_7.png" width="341" height="23" alt="image 7" title="image 7" /></div>
      
  </article></p>
<p>Using the command <code>usermod</code> and options <code>-aG,</code> tells the system to add alice to the supplementary group <code>wheel</code>. Note that the <code>usermod</code> command will not show any output if you run it correctly. If you then run <code>id alice</code>, you should see this output:</p>
<p> </p>
<p><article class="media media--type-image media--view-mode-full" title="image 8"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/life-uploads/image_8.png" width="646" height="69" alt="image 8" title="image 8" /></div>
      
  </article></p>
<p>Since alice is now part of the group <code>wheel</code>, we can switch the user to alice, and she should be able to create the directory <code>dir1</code> as sudo user:</p>
<p><article class="media media--type-image media--view-mode-full" title="image 9"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/life-uploads/image_9.png" width="454" height="166" alt="image 9" title="image 9" /></div>
      
  </article></p>
<p>If you run <code>ls -la</code>, you can see that <code>dir1</code> has both the user and the group as <code>root</code> user, as we ran <code>mkdir</code><i> </i>command as <code>sudo</code> user. But if you run it without <code>sudo</code>, <code>dir1</code> would be owned by the user <code><code class="language-text">alice </code></code>and the group <code><code class="language-text">alice</code></code>.</p>
<p>Now that you've seen a user and a group in Linux, how do you create a user and modify it to add it to a group? The last thing you might want to do is delete the user you created for this demo. I won’t explain how to do that, but I will leave you with the commands below; run it and see the output yourself:</p>
<pre>
<code class="language-text">id alice

sudo userdel -r alice

id alice</code></pre><p>For more information, check the help for these commands by using the --help or -h option, or run <code>man</code> to<command_name> open Linux Manual Pages.</command_name></p>
