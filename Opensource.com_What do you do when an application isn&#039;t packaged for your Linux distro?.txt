<p><a href="https://opensource.com/article/18/7/evolution-package-managers">Package managers</a> make life so easy that many of us have forgotten what things were like in the olden days when getting a piece of software to work with your system was a real test of patience and endurance.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Linux resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux commands cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Free online course: RHEL Technical Overview</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux networking cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">SELinux cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux common commands cheat sheet</a></li>
<li><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">What are Linux containers?</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Our latest Linux articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>But even so, not every piece of software comes readily packaged for your distribution of choice. Maybe you're lucky and it's a single file binary (from a trusted, verifiable source only, we hope!). Maybe it's a <a href="https://opensource.com/article/17/7/how-unzip-targz-file">.tar.gz file</a> that you simply need to decompress. Perhaps it comes as a Flatpak or Snap file which will work across distributions. Or maybe you're going to end up compiling from source. May the dependency gods smile upon your efforts!</p>
<p>Whatever you decide to do, we hope it brings a little bit of appreciation for the hard work that the package maintainers who keep your Linux distribution do every day in order to make the consumption of software easier.</p>
<p>Every time you yum, dnf, zypper, apt, apt-get, or pacman, we hope you'll take a moment to be thankful for the tireless packagers, and maybe everyone once in a while, <a href="https://opensource.com/business/14/2/thank-a-linux-packager-today">wish them a thank you</a>.</p>
