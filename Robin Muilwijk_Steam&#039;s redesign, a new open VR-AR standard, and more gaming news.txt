<p>In this open gaming roundup, we take a look at a new virtual and augmented reality standard, Steam's new look, and more.</p>
<p></p><center>
<p><strong>Open gaming roundup for February 25-March 11, 2017</strong></p>
<p></p></center>
<h2>Valve confirms Steam redesign</h2>
<p>The Valve team behind Steam confirmed it's working on a new look for the gaming platform in <a href="https://youtu.be/atwE-K8y-ws?t=30m" target="_blank">a video interview</a> published Feb. 20. The news first broke when SteamDB shared screenshots on Twitter.</p>
<p></p><center><br /><div class="media_embed">
<blockquote class="twitter-tweet" data-lang="en" height="" width=""><p dir="ltr" lang="en" xml:lang="en">These images shipped with the new Steam Client beta. Filename mentions "SteamU". Original files can be found here: <a href="https://t.co/Xea2ZuMCgO">https://t.co/Xea2ZuMCgO</a> <a href="https://t.co/fybGy6g6ro">pic.twitter.com/fybGy6g6ro</a></p>
<p>— Steam Database (@SteamDB) <a href="https://twitter.com/SteamDB/status/826888948660191232">February 1, 2017</a></p></blockquote>
<script async="" charset="utf-8" height="" src="//platform.twitter.com/widgets.js" width=""></script></div>
<p></p></center>
<h2>Khronos announces OpenXR, a new VR standard</h2>
<p>The Khronos Group <a href="https://www.khronos.org/blog/the-openxr-working-group-is-here" target="_blank">has decided on a name</a> for its upcoming open standard for virtual and augmented reality: OpenXR.</p>
<p>"VR and AR have experienced a boom of interest recently, and with that, a flood of hardware and software companies have begun spinning up efforts in the field. While variety is great, the growing number of devices, each with their own incompatible APIs is increasing fragmentation. The time for standardization is now!"</p>
<p>To encourage innovation, the OpenXR working group will create the standard as open and royalty-free. More information can be found at <a href="https://www.khronos.org/openxr" target="_blank">khronos.org/openxr</a>.</p>
<h2>In other news</h2>
<ul><li><a href="https://steamcommunity.com/games/596420/announcements/detail/521693426582988261" target="_blank">Valve announces 'Steam Audio,' an SDK of advanced audio tools</a></li>
<li><a href="https://www.gamingonlinux.com/articles/editorial-steam-machines-are-not-dead-plus-a-video-from-the-linux-gamer.9178" target="_blank">Steam Machines are not dead</a></li>
<li><a href="https://www.gamingonlinux.com/articles/an-explanation-of-what-mesa-is-and-what-graphics-cards-use-it.9244" target="_blank">An explanation of what Mesa is and what graphics cards use it</a></li>
<li><a href="https://www.gamingonlinux.com/articles/mesa-171-release-is-now-scheduled-for-may.9278" target="_blank">Mesa 17.1 release is now scheduled for May</a></li>
</ul><h2>New games on Linux</h2>
<h3>Torment: Tides of Numenera</h3>
<p><em>Torment: Tides of Numenera</em> was launched on Linux and is available on <a href="http://store.steampowered.com/app/272270/" target="_blank">Steam</a>. From the game description:</p>
<blockquote><p><em>Torment: Tides of Numenera</em> is the thematic successor to <em>Planescape: Torment</em>, one of the most critically acclaimed role-playing games of all time. Immerse yourself in a single-player, isometric, story-driven RPG set in Monte Cook's Numenera universe. What does one life matter? Find your answer.</p>
</blockquote>
<p></p><center><br /><div class="media_embed" height="366px" width="650px">
<iframe allowfullscreen="" frameborder="0" height="366px" src="https://www.youtube.com/embed/cKwRev64-PI?ecver=1" width="650px"></iframe></div>
<p></p></center>
<h3>DiRT Rally</h3>
<p><em>DiRT Rally</em> is now available on <a href="http://store.steampowered.com/app/310560/" target="_blank">Steam</a> for Linux. From the game description:</p>
<blockquote><p><em>DiRT Rally</em> is the most authentic and thrilling rally game ever made, road-tested over 80 million miles by the DiRT community. It perfectly captures that white knuckle feeling of racing on the edge as you hurtle along dangerous roads, knowing that one crash could irreparably harm your stage time.</p>
</blockquote>
<p></p><center><br /><div class="media_embed" height="366px" width="650px">
<iframe allowfullscreen="" frameborder="0" height="366px" src="https://www.youtube.com/embed/3USVRp0j8sc?ecver=1" width="650px"></iframe></div>
<p></p></center>
