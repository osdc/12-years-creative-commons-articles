<p>The National Science Foundation (NSF) just announced US$ 21 million to fund open source development through a new program: Pathways to Enable Open-Source Ecosystems (PEOSE).</p>
<p>Last year it was clearly time to start seriously thinking about a <a href="https://opensource.com/article/21/11/open-source-hardware-careers">career in open source</a>, and organizations as large as the UN noticeably <a href="https://opensource.com/article/21/11/open-source-un-sustainability">leaned in to open source</a>. I am still hiring graduate students for my <a href="https://www.appropedia.org/FAST_application_process" target="_blank">open source tech laboratory</a> in Canada. Now it looks like I will have some stiff competition for the best talent from my American colleagues.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Great Content</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=7016000000127cYAAQ">Free online course: RHEL technical overview</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=7016000000127cYAAQ">Learn Advanced Linux Commands</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Download Cheat Sheets</a></li>
<li><a href="https://opensource.com/alternatives?intcmp=7016000000127cYAAQ">Find an Open Source Alternative</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=7016000000127cYAAQ">Read Top Linux Content</a></li>
<li><a href="https://opensource.com/resources?intcmp=7016000000127cYAAQ">Check out open source resources</a></li>
</ul></div>
</div>
</div>
</div>

<h2>Why the NSF is choosing open source</h2>
<p>The NSF is an independent agency of the United States government and the premier funding agency for fundamental research and education in all non-medical fields of science and engineering. They are now major open source supporters as well. To be fair, the National Institutes of Health (NIH), which funds medical research in the United States, is also already a big fan of open source with the first open-access mandate and even an open source <a href="https://3dprint.nih.gov/" target="_blank">3-D Printing Exchange</a> made famous during the pandemic.</p>
<p>With PEOSE, the NSF hopes to harness the power of open source development for the creation of new technology solutions to problems of national and societal importance. Existing NSF-funded research projects already result in publicly accessible, modifiable, and distributable open source software or data platforms and now even open hardware that catalyzes further innovation. Open source <a href="https://opensource.com/article/20/10/open-source-hardware-savings">savings for scientists</a> are large. The <a href="https://www.academia.edu/13799962/Return_on_Investment_for_Open_Source_Hardware_Development" target="_blank">return on investment (ROI)</a> for funders of open source development <u>(</u>100s-1,000s% after only a few months<u>)</u> is frankly too high to ignore. The NSF wants to follow the best examples of open source development where the product is widely adopted and forms the foundation of a self-sustaining open source ecosystem (OSE). A distributed community of developers and a broad base of users across academia, industry, and government make up these OSEs. Think Linux for software, Arduino for electronics, or RepRap for manufacturing hardware.</p>
<p>The NSF is specifically funding new OSE managing organizations. Each organization will create and maintain the infrastructure for a specific OS product or class of products. They want “more secure open source products, increased coordination of developer contributions, and a more focused route to impactful technologies.” Best of all, the NSF is putting its money where its mouth is. They anticipate giving out 30 awards:<strong> </strong>20 Phase I awards of up to US$ 300,000 each for one year and 10 Phase II awards of up to US$ 1,500,000 for up to two years.</p>
<p>They are not funding open source research products or tools with this call (there are other calls for specific tech), and they are not looking to fund existing well-resourced open source communities and ecosystems. Instead, the program aims to fund new managing organizations that catalyze community-driven development and growth of OSEs.</p>
<p>They want to grow the community of researchers who develop and contribute to open source and enable pathways for collaboration that lead to new technologies that have broad impacts on society.</p>
<p>If you are working on an open source project that might benefit from this kind of funding, check it out <a href="https://www.nsf.gov/pubs/2022/nsf22572/nsf22572.htm?WT.mc_ev=click&amp;WT.mc_id=USNSF_32&amp;utm_medium=email&amp;utm_source=govdelivery" target="_blank">here</a>. Phase I applications are due May 12, 2022, and Phase II applications are due October 21, 2022.</p>
<p>Good luck!</p>
