<p>Kubernetes has two types of objects that can inject configuration data into a container when it starts up: Secrets and ConfigMaps. <a href="https://www.kubernetesbyexample.com/en/concept/secrets">Secrets</a> and ConfigMaps behave similarly in <a href="https://opensource.com/resources/what-is-kubernetes">Kubernetes</a>, both in how they are created and because they can be exposed inside a container as mounted files or volumes or environment variables.</p>
<p>To explore Secrets and ConfigMaps, consider the following scenario:</p>
<blockquote><p>You're running the <a href="https://hub.docker.com/_/mariadb" target="_blank">official MariaDB container image</a> in Kubernetes and must do some configuration to get the container to run. The image requires an environment variable to be set for <strong>MYSQL_ROOT_PASSWORD</strong>, <strong>MYSQL_ALLOW_EMPTY_PASSWORD</strong>, or <strong>MYSQL_RANDOM_ROOT_PASSWORD</strong> to initialize the database. It also allows for extensions to the MySQL configuration file <strong>my.cnf</strong> by placing custom config files in <strong>/etc/mysql/conf.d</strong>.</p>
</blockquote>
<p>You could build a custom image, setting the environment variables and copying the configuration files into it to create a bespoke container image. However, it is considered a best practice to create and use generic images and add configuration to the containers created from them, instead. This is a perfect use-case for ConfigMaps and Secrets. The <strong>MYSQL_ROOT_PASSWORD</strong> can be set in a Secret and added to the container as an environment variable, and the configuration files can be stored in a ConfigMap and mounted into the container as a file on startup.</p>
<p>Let's try it out!</p>
<h2 id="a-note-about-kubectl">But first: A quick note about Kubectl</h2>
<p>Make sure that your version of the <strong>kubectl</strong> client command is the same or newer than the Kubernetes cluster version in use.</p>
<p>An error along the lines of: </p>
<pre><code class="language-text">error: SchemaError(io.k8s.api.admissionregistration.v1beta1.ServiceReference): invalid object doesn't have additional properties</code></pre><p>may mean the client version is too old and needs to be upgraded.   The <a href="https://kubernetes.io/docs/tasks/tools/install-kubectl/" target="_blank">Kubernetes Documentation for Installing Kubectl</a> has instructions for installing the latest client on various platforms.</p>
<p>If you're using Docker for Mac, it also installs its own version of <strong>kubectl</strong>, and that may be the issue. You can install a current client with <strong>brew install</strong>, replacing the symlink to the client shipped by Docker:</p>
<pre><code class="language-bash">$ rm /usr/local/bin/kubectl
$ brew link --overwrite kubernetes-cli</code></pre><p>The newer <strong>kubectl</strong> client should continue to work with Docker's Kubernetes version.</p>
<h2 id="secrets">Secrets</h2>
<p>Secrets are a Kubernetes object intended for storing a small amount of sensitive data. It is worth noting that Secrets are stored base64-encoded within Kubernetes, so they are not wildly secure. Make sure to have appropriate <a href="https://kubernetes.io/docs/reference/access-authn-authz/rbac/" target="_blank">role-based access controls</a> (RBAC) to protect access to Secrets. Even so, extremely sensitive Secrets data should probably be stored using something like <a href="https://www.vaultproject.io/" target="_blank">HashiCorp Vault</a>. For the root password of a MariaDB database, however, base64 encoding is just fine.</p>
<h3 id="creating-a-secret-manually">Create a Secret manually</h3>
<p>To create the Secret containing the <strong>MYSQL_ROOT_PASSWORD</strong>, choose a password and convert it to base64:</p>
<pre><code class="language-bash"># The root password will be "KubernetesRocks!"
$ echo -n 'KubernetesRocks!' | base64
S3ViZXJuZXRlc1JvY2tzIQ==</code></pre><p>Make a note of the encoded string. You need it to create the YAML file for the Secret:</p>
<pre><code class="language-yaml">apiVersion: v1
kind: Secret
metadata:
  name: mariadb-root-password
type: Opaque
data:
  password: S3ViZXJuZXRlc1JvY2tzIQ==</code></pre><p>Save that file as <strong>mysql-secret.yaml</strong> and create the Secret in Kubernetes with the <strong>kubectl apply</strong> command:</p>
<pre><code class="language-bash">$ kubectl apply -f mysql-secret.yaml
secret/mariadb-root-password created</code></pre><h3 id="view-the-newly-created-secret">View the newly created Secret</h3>
<p>Now that you've created the Secret, use <strong>kubectl describe</strong> to see it:</p>
<pre><code class="language-bash">$ kubectl describe secret mariadb-root-password
Name:         mariadb-root-password
Namespace:    secrets-and-configmaps
Labels:       &lt;none&gt;
Annotations:
Type:         Opaque

Data
====
password:  16 bytes</code></pre><p>Note that the <strong>Data</strong> field contains the key you set in the YAML: <strong>password</strong>. The value assigned to that key is the password you created, but it is not shown in the output. Instead, the value's size is shown in its place, in this case, 16 bytes.</p>
<p>You can also use the <strong>kubectl edit secret &lt;secretname&gt;</strong> command to view and edit the Secret. If you edit the Secret, you'll see something like this:</p>
<pre><code class="language-bash"># Please edit the object below. Lines beginning with a '#' will be ignored,
# and an empty file will abort the edit. If an error occurs while saving this file will be
# reopened with the relevant failures.
#
apiVersion: v1
data:
  password: S3ViZXJuZXRlc1JvY2tzIQ==
kind: Secret
metadata:
  annotations:
    kubectl.kubernetes.io/last-applied-configuration: |
      {"apiVersion":"v1","data":{"password":"S3ViZXJuZXRlc1JvY2tzIQ=="},"kind":"Secret","metadata":{"annotations":{},"name":"mariadb-root-password","namespace":"secrets-and-configmaps"},"type":"Opaque"}
  creationTimestamp: 2019-05-29T12:06:09Z
  name: mariadb-root-password
  namespace: secrets-and-configmaps
  resourceVersion: "85154772"
  selfLink: /api/v1/namespaces/secrets-and-configmaps/secrets/mariadb-root-password
  uid: 2542dadb-820a-11e9-ae24-005056a1db05
type: Opaque</code></pre><p>Again, the <strong>data</strong> field with the <strong>password</strong> key is visible, and this time you can see the base64-encoded Secret.</p>
<h3 id="decode-the-secret">Decode the Secret</h3>
<p>Let's say you need to view the Secret in plain text, for example, to verify that the Secret was created with the correct content. You can do this by decoding it.</p>
<p>It is easy to decode the Secret by extracting the value and piping it to base64. In this case, you will use the output format <strong>-o jsonpath=&lt;path&gt;</strong> to extract only the Secret value using a JSONPath template.</p>
<pre><code class="language-bash"># Returns the base64 encoded secret string
$ kubectl get secret mariadb-root-password -o jsonpath='{.data.password}'
S3ViZXJuZXRlc1JvY2tzIQ==

# Pipe it to `base64 --decode -` to decode:
$ kubectl get secret mariadb-root-password -o jsonpath='{.data.password}' | base64 --decode -
KubernetesRocks!</code></pre><h3 id="another-way-to-create-secrets">Another way to create Secrets</h3>
<p>You can also create Secrets directly using the <strong>kubectl create secret</strong> command. The MariaDB image permits setting up a regular database user with a password by setting the <strong>MYSQL_USER</strong> and <strong>MYSQL_PASSWORD</strong> environment variables. A Secret can hold more than one key/value pair, so you can create a single Secret to hold both strings. As a bonus, by using <strong>kubectl create secret</strong>, you can let Kubernetes mess with base64 so that you don't have to.</p>
<pre><code class="language-bash">$ kubectl create secret generic mariadb-user-creds \
      --from-literal=MYSQL_USER=kubeuser\
      --from-literal=MYSQL_PASSWORD=kube-still-rocks
secret/mariadb-user-creds created</code></pre><p>Note the <strong>--from-literal</strong>, which sets the key name and the value all in one. You can pass as many <strong>--from-literal</strong> arguments as you need to create one or more key/value pairs in the Secret.</p>
<p>Validate that the username and password were created and stored correctly with the <strong>kubectl get secrets</strong> command:</p>
<pre><code class="language-bash"># Get the username
$ kubectl get secret mariadb-user-creds -o jsonpath='{.data.MYSQL_USER}' | base64 --decode -
kubeuser

# Get the password
$ kubectl get secret mariadb-user-creds -o jsonpath='{.data.MYSQL_PASSWORD}' | base64 --decode -
kube-still-rocks</code></pre><h2 id="configmaps">ConfigMaps</h2>
<p>ConfigMaps are similar to Secrets. They can be created and shared in the containers in the same ways. The only big difference between them is the base64-encoding obfuscation. ConfigMaps are intended for non-sensitive data—configuration data—like config files and environment variables and are a great way to create customized running services from generic container images.</p>
<h3 id="create-a-configmap">Create a ConfigMap</h3>
<p>ConfigMaps can be created in the same ways as Secrets. You can write a YAML representation of the ConfigMap manually and load it into Kubernetes, or you can use the <strong>kubectl create configmap</strong> command to create it from the command line. The following example creates a ConfigMap using the latter method but, instead of passing literal strings (as with <strong>--from-literal=&lt;key&gt;=&lt;string&gt;</strong> in the Secret above), it creates a ConfigMap from an existing file—a MySQL config intended for <strong>/etc/mysql/conf.d</strong> in the container. This config file overrides the <strong>max_allowed_packet</strong> setting that MariaDB sets to 16M by default.</p>
<p>First, create a file named <strong>max_allowed_packet.cnf</strong> with the following content:</p>
<pre><code class="language-text">[mysqld]
max_allowed_packet = 64M</code></pre><p>This will override the default setting in the <strong>my.cnf</strong> file and set <strong>max_allowed_packet</strong> to 64M.</p>
<p>Once the file is created, you can create a ConfigMap named <strong>mariadb-config</strong> using the <strong>kubectl create configmap</strong> command that contains the file:</p>
<pre><code class="language-bash">$ kubectl create configmap mariadb-config --from-file=max_allowed_packet.cnf
configmap/mariadb-config created</code></pre><p>Just like Secrets, ConfigMaps store one or more key/value pairs in their Data hash of the object. By default, using <strong>--from-file=&lt;filename&gt;</strong> (as above) will store the contents of the file as the value, and the name of the file will be stored as the key. This is convenient from an organization viewpoint. However, the key name can be explicitly set, too. For example, if you used <strong>--from-file=max-packet=max_allowed_packet.cnf</strong> when you created the ConfigMap, the key would be <strong>max-packet</strong> rather than the file name. If you had multiple files to store in the ConfigMap, you could add each of them with an additional <strong>--from-file=&lt;filename&gt;</strong> argument.</p>
<h3 id="view-the-new-configmap-and-read-the-data">View the new ConfigMap and read the data</h3>
<p>As mentioned, ConfigMaps are not meant to store sensitive data, so the data is not encoded when the ConfigMap is created. This makes it easy to view and validate the data and edit it directly.</p>
<p>First, validate that the ConfigMap was, indeed, created:</p>
<pre><code class="language-bash">$ kubectl get configmap mariadb-config
NAME             DATA      AGE
mariadb-config   1         9m</code></pre><p>The contents of the ConfigMap can be viewed with the <strong>kubectl describe</strong> command. Note that the full contents of the file are visible and that the key name is, in fact, the file name, <strong>max_allowed_packet.cnf</strong>.</p>
<pre><code class="language-bash">$ kubectl describe cm mariadb-config
Name:         mariadb-config
Namespace:    secrets-and-configmaps
Labels:       &lt;none&gt;
Annotations:  &lt;none&gt;


Data
====
max_allowed_packet.cnf:
----
[mysqld]
max_allowed_packet = 64M

Events:  &lt;none&gt;</code></pre><p>A ConfigMap can be edited live within Kubernetes with the <strong>kubectl edit</strong> command. Doing so will open a buffer with the default editor showing the contents of the ConfigMap as YAML. When changes are saved, they will immediately be live in Kubernetes. While not really the <em>best</em> practice, it can be handy for testing things in development.</p>
<p>Say you want a <strong>max_allowed_packet</strong> value of 32M instead of the default 16M or the 64M in the <strong>max_allowed_packet.cnf</strong> file. Use <strong>kubectl edit configmap mariadb-config</strong> to edit the value:</p>
<pre><code class="language-bash">$ kubectl edit configmap mariadb-config

# Please edit the object below. Lines beginning with a '#' will be ignored,
# and an empty file will abort the edit. If an error occurs while saving this file will be
# reopened with the relevant failures.
#
apiVersion: v1

data:
  max_allowed_packet.cnf: |
    [mysqld]
    max_allowed_packet = 32M
kind: ConfigMap
metadata:
  creationTimestamp: 2019-05-30T12:02:22Z
  name: mariadb-config
  namespace: secrets-and-configmaps
  resourceVersion: "85609912"
  selfLink: /api/v1/namespaces/secrets-and-configmaps/configmaps/mariadb-config
  uid: c83ccfae-82d2-11e9-832f-005056a1102f</code></pre><p>After saving the change, verify the data has been updated:</p>
<pre><code class="language-bash"># Note the '.' in max_allowed_packet.cnf needs to be escaped
$ kubectl get configmap mariadb-config -o "jsonpath={.data['max_allowed_packet\.cnf']}"

[mysqld]
max_allowed_packet = 32M</code></pre><h2 id="using-secrets-and-configmaps">Using Secrets and ConfigMaps</h2>
<p>Secrets and ConfigMaps can be mounted as environment variables or as files within a container. For the MariaDB container, you will need to mount the Secrets as environment variables and the ConfigMap as a file. First, though, you need to write a Deployment for MariaDB so that you have something to work with. Create a file named <strong>mariadb-deployment.yaml</strong> with the following:</p>
<pre><code class="language-yaml">apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: mariadb
  name: mariadb-deployment
spec:
  replicas: 1
  selector:
    matchLabels:
      app: mariadb
  template:
    metadata:
      labels:
        app: mariadb
    spec:
      containers:
      - name: mariadb
        image: docker.io/mariadb:10.4
        ports:
        - containerPort: 3306
          protocol: TCP
        volumeMounts:
        - mountPath: /var/lib/mysql
          name: mariadb-volume-1
      volumes:
      - emptyDir: {}
        name: mariadb-volume-1</code></pre><p>This is a bare-bones Kubernetes Deployment of the official MariaDB 10.4 image from Docker Hub. Now, add your Secrets and ConfigMap.</p>
<h3 id="adding-the-secrets-to-the-deployment-as-environment-variables">Add the Secrets to the Deployment as environment variables</h3>
<p>You have two Secrets that need to be added to the Deployment:</p>
<ol><li><strong>mariadb-root-password</strong> (with one key/value pair)</li>
<li><strong>mariadb-user-creds</strong> (with two key/value pairs)</li>
</ol><p>For the <b>mariadb-root-password</b> Secret, specify the Secret and the key you want by adding an <strong>env</strong> list/array to the container spec in the Deployment and setting the environment variable value to the value of the key in your Secret. In this case, the list contains only a single entry, for the variable <strong>MYSQL_ROOT_PASSWORD</strong>.</p>
<pre><code class="language-yaml"> env:
   - name: MYSQL_ROOT_PASSWORD
     valueFrom:
       secretKeyRef:
         name: mariadb-root-password
         key: password</code></pre><p>Note that the name of the object is the name of the environment variable that is added to the container. The <strong>valueFrom</strong> field defines <strong>secretKeyRef</strong> as the source from which the environment variable will be set; i.e., it will use the value from the <strong>password</strong> key in the <strong>mariadb-root-password</strong> Secret you set earlier.</p>
<p>Add this section to the definition for the <strong>mariadb</strong> container in the <strong>mariadb-deployment.yaml</strong> file. It should look something like this:</p>
<pre><code class="language-yaml"> spec:
   containers:
   - name: mariadb
     image: docker.io/mariadb:10.4
     env:
       - name: MYSQL_ROOT_PASSWORD
         valueFrom:
           secretKeyRef:
             name: mariadb-root-password
             key: password
     ports:
     - containerPort: 3306
       protocol: TCP
     volumeMounts:
     - mountPath: /var/lib/mysql
       name: mariadb-volume-1</code></pre><p>In this way, you have explicitly set the variable to the value of a specific key from your Secret. This method can also be used with ConfigMaps by using <strong>configMapRef</strong> instead of <strong>secretKeyRef</strong>.</p>
<p>You can also set environment variables from <em>all</em> key/value pairs in a Secret or ConfigMap to automatically use the key name as the environment variable name and the key's value as the environment variable's value. By using <strong>envFrom</strong> rather than <strong>env</strong> in the container spec, you can set the <strong>MYSQL_USER</strong> and <strong>MYSQL_PASSWORD</strong> from the <strong>mariadb-user-creds</strong> Secret you created earlier, all in one go:</p>
<pre><code class="language-yaml">envFrom:
- secretRef:
    name: mariadb-user-creds</code></pre><p><strong>envFrom</strong> is a list of sources for Kubernetes to take environment variables. Use <strong>secretRef</strong> again, this time to specify <strong>mariadb-user-creds</strong> as the source of the environment variables. That's it! All the keys and values in the Secret will be added as environment variables in the container.</p>
<p>The container spec should now look like this:</p>
<pre><code class="language-yaml">spec:
  containers:
  - name: mariadb
    image: docker.io/mariadb:10.4
    env:
      - name: MYSQL_ROOT_PASSWORD
        valueFrom:
          secretKeyRef:
            name: mariadb-root-password
            key: password
    envFrom:
    - secretRef:
        name: mariadb-user-creds
    ports:
    - containerPort: 3306
      protocol: TCP
    volumeMounts:
    - mountPath: /var/lib/mysql
      name: mariadb-volume-1</code></pre><p><em>Note:</em> You could have just added the <strong>mysql-root-password</strong> Secret to the <strong>envFrom</strong> list and let it be parsed as well, as long as the <strong>password</strong> key was named <strong>MYSQL_ROOT_PASSWORD</strong> instead. There is no way to manually specify the environment variable name with <strong>envFrom</strong> as with <strong>env</strong>.</p>
<h3 id="adding-the-max_allowed_packet.cnf-file-to-the-deployment-as-a-volumemount">Add the max_allowed_packet.cnf file to the Deployment as a volumeMount</h3>
<p>As mentioned, both <strong>env</strong> and <strong>envFrom</strong> can be used to share ConfigMap key/value pairs with a container as well. However, in the case of the <strong>mariadb-config</strong> ConfigMap, your entire file is stored as the value to your key, and the file needs to exist in the container's filesystem for MariaDB to be able to use it. Luckily, both Secrets and ConfigMaps can be the source of Kubernetes "volumes" and mounted into the containers instead of using a filesystem or block device as the volume to be mounted.</p>
<p>The <strong>mariadb-deployment.yaml</strong> already has a volume and volumeMount specified, an <strong>emptyDir</strong> (effectively a temporary or ephemeral) volume mounted to <strong>/var/lib/mysql</strong> to store the MariaDB data:</p>
<pre><code class="language-yaml">&lt;...&gt;

  volumeMounts:
  - mountPath: /var/lib/mysql
    name: mariadb-volume-1

&lt;...&gt;

volumes:
- emptyDir: {}
name: mariadb-volume-1

&lt;...&gt;</code></pre><p><em>Note:</em> This is not a production configuration. When the Pod restarts, the data in the <strong>emptyDir</strong> volume is lost. This is primarily used for development or when the contents of the volume don't need to be persistent.</p>
<p>You can add your ConfigMap as a source by adding it to the volume list and then adding a volumeMount for it to the container definition:</p>
<pre><code class="language-yaml">&lt;...&gt;

  volumeMounts:
  - mountPath: /var/lib/mysql
    name: mariadb-volume-1
  - mountPath: /etc/mysql/conf.d
    name: mariadb-config

&lt;...&gt;

volumes:
- emptyDir: {}
  name: mariadb-volume-1
- configMap:
    name: mariadb-config
    items:
      - key: max_allowed_packet.cnf
        path: max_allowed_packet.cnf
  name: mariadb-config-volume

&lt;...&gt;</code></pre><p>The <strong>volumeMount</strong> is pretty self-explanatory—create a volume mount for the <strong>mariadb-config-volume</strong> (specified in the <strong>volumes</strong> list below it) to the path <strong>/etc/mysql/conf.d</strong>.</p>
<p>Then, in the <strong>volumes</strong> list, <strong>configMap</strong> tells Kubernetes to use the <strong>mariadb-config</strong> ConfigMap, taking the contents of the key <strong>max_allowed_packet.cnf</strong> and mounting it to the path <strong>max_allowed_packed.cnf</strong>. The name of the volume is <strong>mariadb-config-volume</strong>, which was referenced in the <strong>volumeMounts</strong> above.</p>
<p><em>Note:</em> The <strong>path</strong> from the <strong>configMap</strong> is the name of a file that will contain the contents of the key's value. In this case, your key was a file name, too, but it doesn't have to be. Note also that <strong>items</strong> is a list, so multiple keys can be referenced and their values mounted as files. These files will all be created in the <strong>mountPath</strong> of the <strong>volumeMount</strong> specified above: <strong>/etc/mysql/conf.d</strong>.</p>
<h2 id="creating-a-mariadb-instance-from-the-deployment">Create a MariaDB instance from the Deployment</h2>
<p>At this point, you should have enough to create a MariaDB instance. You have two Secrets, one holding the <strong>MYSQL_ROOT_PASSWORD</strong> and another storing the <strong>MYSQL_USER</strong>, and the <strong>MYSQL_PASSWORD</strong> environment variables to be added to the container. You also have a ConfigMap holding the contents of a MySQL config file that overrides the <strong>max_allowed_packed</strong> value from its default setting.</p>
<p>You also have a <strong>mariadb-deployment.yaml</strong> file that describes a Kubernetes deployment of a Pod with a MariaDB container and adds the Secrets as environment variables and the ConfigMap as a volume-mounted file in the container. It should look like this:</p>
<pre><code class="language-yaml">apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: mariadb
  name: mariadb-deployment
spec:
  replicas: 1
  selector:
    matchLabels:
      app: mariadb
  template:
    metadata:
      labels:
        app: mariadb
    spec:
      containers:
      - image: docker.io/mariadb:10.4
        name: mariadb
        env:
          - name: MYSQL_ROOT_PASSWORD
            valueFrom:
              secretKeyRef:
                name: mariadb-root-password
                key: password
        envFrom:
        - secretRef:
            name: mariadb-user-creds
        ports:
        - containerPort: 3306
          protocol: TCP
        volumeMounts:
        - mountPath: /var/lib/mysql
          name: mariadb-volume-1
        - mountPath: /etc/mysql/conf.d
          name: mariadb-config-volume
      volumes:
      - emptyDir: {}
        name: mariadb-volume-1
      - configMap:
          name: mariadb-config
          items:
            - key: max_allowed_packet.cnf
              path: max_allowed_packet.cnf
        name: mariadb-config-volume</code></pre><h3 id="create-the-mariadb-instance">Create the MariaDB instance</h3>
<p>Create a new MariaDB instance from the YAML file with the <strong>kubectl create</strong> command:</p>
<pre><code class="language-bash">$ kubectl create -f mariadb-deployment.yaml
deployment.apps/mariadb-deployment created</code></pre><p>Once the deployment has been created, use the <strong>kubectl get</strong> command to view the running MariaDB pod:</p>
<pre><code class="language-bash">$ kubectl get pods
NAME                                  READY     STATUS    RESTARTS   AGE
mariadb-deployment-5465c6655c-7jfqm   1/1       Running   0          3m</code></pre><p>Make a note of the Pod name (in this example, it's <strong>mariadb-deployment-5465c6655c-7jfqm</strong>). Note that the Pod name will differ from this example.</p>
<h3 id="verify-the-instance-is-using-the-secrets-and-configmap">Verify the instance is using the Secrets and ConfigMap</h3>
<p>Use the <strong>kubectl exec</strong> command (with your Pod name) to validate that the Secrets and ConfigMaps are in use. For example, check that the environment variables are exposed in the container:</p>
<pre><code class="language-bash">$ kubectl exec -it mariadb-deployment-5465c6655c-7jfqm env |grep MYSQL
MYSQL_PASSWORD=kube-still-rocks
MYSQL_USER=kubeuser
MYSQL_ROOT_PASSWORD=KubernetesRocks!</code></pre><p>Success! All three environment variables—the one using the <strong>env</strong> setup to specify the Secret, and two using <strong>envFrom</strong> to mount all the values from the Secret—are available in the container for MariaDB to use.</p>
<p>Spot check that the <strong>max_allowed_packet.cnf</strong> file was created in <strong>/etc/mysql/conf.d</strong> and that it contains the expected content:</p>
<pre><code class="language-bash">$ kubectl exec -it mariadb-deployment-5465c6655c-7jfqm ls /etc/mysql/conf.d
max_allowed_packet.cnf

$ kubectl exec -it mariadb-deployment-5465c6655c-7jfqm cat /etc/mysql/conf.d/max_allowed_packet.cnf
[mysqld]
max_allowed_packet = 32M</code></pre><p>Finally, validate that MariaDB used the environment variable to set the root user password and read the <strong>max_allowed_packet.cnf</strong> file to set the <strong>max_allowed_packet</strong> configuration variable. Use the <strong>kubectl exec</strong> command again, this time to get a shell inside the running container and use it to run some <strong>mysql</strong> commands:</p>
<pre><code class="language-bash">$ kubectl exec -it mariadb-deployment-5465c6655c-7jfqm /
bin/sh

# Check that the root password was set correctly
$ mysql -uroot -p${MYSQL_ROOT_PASSWORD} -e 'show databases;'
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
+--------------------+

# Check that the max_allowed_packet.cnf was parsed
$ mysql -uroot -p${MYSQL_ROOT_PASSWORD} -e "SHOW VARIABLES LIKE 'max_allowed_packet';"
+--------------------+----------+
| Variable_name      | Value    |
+--------------------+----------+
| max_allowed_packet | 33554432 |
+--------------------+----------+</code></pre><h2 id="conclusion">Advantages of Secrets and ConfigMaps</h2>
<p>This exercise explained how to create Kubernetes Secrets and ConfigMaps and how to use those Secrets and ConfigMaps by adding them as environment variables or files inside of a running container instance. This makes it easy to keep the configuration of individual instances of containers separate from the container image. By separating the configuration data, overhead is reduced to maintaining only a single image for a specific type of instance while retaining the flexibility to create instances with a wide variety of configurations.</p>
