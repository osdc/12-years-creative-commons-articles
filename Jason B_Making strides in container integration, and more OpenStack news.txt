<p>Interested in keeping track of what's happening in the open source cloud? Opensource.com is your source for news in <a href="https://opensource.com/resources/what-is-openstack" target="_blank" title="What is OpenStack?">OpenStack</a>, the open source cloud infrastructure project.</p>
<p><!--break--></p>
<h3>OpenStack around the web</h3>
<p>There's a lot of interesting stuff being written about OpenStack. Here's a sampling:</p>
<ul><li><a href="http://superuser.openstack.org/articles/mastering-containers-with-openstack-a-white-paper" target="_blank">Mastering containers with OpenStack</a>: As containers gain major ground, a new white paper from the OpenStack Foundation highlights how to succeed with them.</li>
<li><a href="http://galsagie.github.io//sdn/openstack/docker/kuryr/neutron/2015/08/24/kuryr-part1/" target="_blank" style="font-size: 13.0080003738403px; line-height: 1.538em; background-color: transparent;">Bringing containers networking to OpenStack Neutron</a>: An introduction to the Kuryr project.</li>
<li><a href="http://superuser.openstack.org/articles/openstack-silicon-valley-zeroes-in-on-containers" target="_blank">OpenStack Silicon Valley zeroes in on containers</a>: The second annual event welcomed thought leaders and users this week to discuss the current state of containers and predict what's next.</li>
<li><a href="http://www.eweek.com/cloud/openstack-continues-to-push-cloud-integration-vision.html" target="_blank">OpenStack continues to push cloud integration vision</a>: How OpenStack ties together different types of systems, including containers and other emerging technologies.</li>
<li><a href="https://opensource.com/business/15/8/new-guides-mastering-openstack-cloud" target="_blank" style="font-size: 13.0080003738403px; line-height: 1.538em; background-color: transparent;">8 new tutorials for OpenStack users and developers</a>: Lots of new resources for helping you learn about OpenStack.</li>
<li><a href="http://ttx.re/openstack-common-culture.html" target="_blank" style="font-size: 13.0080003738403px; line-height: 1.538em; background-color: transparent;">OpenStack common culture</a>: What does it mean to <em style="font-size: 13.0080003738403px; line-height: 1.538em; background-color: transparent;">be OpenStack</em> as the community evolves?</li>
</ul><h3>OpenStack discussions</h3>
<p>Here's a sample of some of the most active discussions this week on the OpenStack developers' listserv. For a more in-depth look, why not <a href="http://lists.openstack.org/pipermail/openstack-dev/" target="_blank" title="OpenStack listserv">dive in</a> yourself?</p>
<ul><li><a href="http://lists.openstack.org/pipermail/openstack-dev/2015-August/072710.html" target="_blank">Attaching arbitrary key/value pairs to resources</a>: Security and the Neutron API.</li>
<li><a href="http://lists.openstack.org/pipermail/openstack-dev/2015-August/073134.html" target="_blank">40% failure on Neutron Python 3.4 tests in the gate</a>: Solutions for preventing backup at the gate.</li>
<li><a href="http://lists.openstack.org/pipermail/openstack-dev/2015-August/073070.html" target="_blank">"Latest" microversion considered dangerous</a>: Should Manila transmit its actual version number?</li>
<li><a href="http://lists.openstack.org/pipermail/openstack-dev/2015-August/072890.html" target="_blank">Documentation on how to start contributing</a>: How to get involved with the Murano project.</li>
<li><a href="http://lists.openstack.org/pipermail/openstack-dev/2015-August/072763.html" target="_blank">VPNaaS API change is not backward compatible</a>: Three upcoming API changes you should be aware of.</li>
</ul><h3>Cloud &amp; OpenStack events</h3>
<p>Here's what is happening this week. Something missing? <a href="mailto:osdc-admin@redhat.com">Email</a> us and let us know, or add it to our <a href="https://opensource.com/resources/conferences-and-events-monthly" target="_blank" title="Events Calendar">events calendar</a>.</p>
<ul><li><a href="http://www.meetup.com/Australian-OpenStack-User-Group/events/223094464/" target="_blank">Australian OpenStack user group</a>: August 31; Brisbane, Australia.</li>
<li><a href="http://www.meetup.com/OpenStack-Colorado/events/224474987/" target="_blank">Talk about OpenStack architectures</a>: August 31; Fort Collins, CO.</li>
<li><a href="http://www.meetup.com/OpenStack-DFW/events/224930443/" target="_blank">DFW OpenStack meetup</a>: September 2; Richardson, TX.</li>
<li><a href="http://www.meetup.com/raleighopensourcecloud/events/223028167/" target="_blank">Raleigh open source cloud meetup</a>: September 2; Raleigh, NC.</li>
<li><a href="http://www.meetup.com/OpenStack-Nova/events/222872867/" target="_blank">Large scale OpenStack operations best practices</a>: September 3; Reston, VA.</li>
<li><a href="http://www.meetup.com/OpenStack-Richmond/events/224593249/" target="_blank">Build an OpenStack cloud</a>: September 3; Glen Allen, VA.</li>
</ul><p><em>Interested in the OpenStack project development meetings? A complete list of these meetings' regular schedules is available <a href="https://wiki.openstack.org/wiki/Meetings" target="_blank" title="OpenStack Meetings">here</a>.</em></p>
