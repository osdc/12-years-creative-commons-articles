<p>In this week's Top 5, we highlight Linux filesystems, your favorite "old" programming language, running commands at shutdown on Linux, and Perl's new school users and old school history.</p>
<p><iframe width="520" height="315" src="https://www.youtube.com/embed/UCflD0y9Jj4" frameborder="0" allowfullscreen=""></iframe></p>
<h2>Top 5 articles of the week</h2>
<p><strong>5. <a href="https://opensource.com/life/16/11/perl-and-birth-dynamic-web">Perl and the birth of the dynamic web</a></strong></p>
<p>One writer tells the fascinating story of Perl's role in the dynamic web, which is to say, how we got web servers to do more than serve static HTML documents.</p>
<p><strong>4. <a href="https://opensource.com/life/16/10/perl-continues-attract-new-users">Perl might be old school, but it continues to attract new users</a></strong></p>
<p>A <a href="http://www.activestate.com/blog/2016/06/perl-survey-2016" target="_blank">survey of ActiveState users</a> conducted earlier this year showed that one-third of the respondents have three or fewer years of experience with Perl. And, nearly half of all respondents reported using Perl for fewer than five years. This article reviews Perl's career outlook as well as Perl benefits and modules.</p>
<p><strong>3. <a href="https://opensource.com/life/16/11/running-commands-shutdown-linux">How to run commands at shutdown on Linux</a></strong></p>
<p>Ben Cotton tells us that Linux and Unix systems have long made it pretty easy to run a command on boot. But as it turns out, running a command on shutdown is a little more complicated.</p>
<p><strong>2. <a href="https://opensource.com/business/16/11/which-ancient-programming-language-do-you-use-most">Which 'ancient' programming language do you use the most?</a></strong></p>
<p>Over 2,000 people have taken this poll so far. The common thread that holds the programming languages chosen for this poll together is that each is now over a third of a century in age, and for better or for worse, someone out there is still using them in some way. Chime in with your vote!</p>
<p><strong>1. <a href="https://opensource.com/life/16/10/introduction-linux-filesystems">An introduction to Linux filesystems</a></strong></p>
<p>A high-level discussion of Linux filesystems including definitions, functions, directory structure, filesystem types, and mounting.</p>
