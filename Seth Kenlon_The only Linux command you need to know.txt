<p id="cheat-on-man-and-info">Information about Linux and open source abounds on the internet, but when you're entrenched in your work there's often a need for quick documentation. Since the early days of Unix, well before Linux even existed, there's been the <code>man</code> (short for "manual") and <code>info</code> commands, both of which display official project documentation about commands, configuration files, system calls, and more.</p>
<p>There's a debate over whether <code>man</code> and <code>info</code> pages are meant as helpful reminders for users who already know how to use a tool, or an intro for first time users. Either way, both <code>man</code> and <code>info</code> pages describe tools and how to use them, and rarely address specific tasks and how to accomplish them. It's for that very reason that the <code>cheat</code> command was developed.</p>
<p>For instance, suppose you can't remember how to <a href="https://opensource.com/article/17/7/how-unzip-targz-file">unarchive a tar file</a>. The <code>man</code> page provides you with all the options you require, but it leaves it up to you to translate this information into a functional command:</p>
<pre>
<code class="language-bash">tar -A [OPTIONS] ARCHIVE ARCHIVE
tar -c [-f ARCHIVE] [OPTIONS] [FILE...]
tar -d [-f ARCHIVE] [OPTIONS] [FILE...]
tar -t [-f ARCHIVE] [OPTIONS] [MEMBER...]
tar -r [-f ARCHIVE] [OPTIONS] [FILE...]
tar -u [-f ARCHIVE] [OPTIONS] [FILE...]
tar -x [-f ARCHIVE] [OPTIONS] [MEMBER...]</code></pre><p>That's exactly what some users need, but it confounds other users. The cheat sheet for tar, by contrast, provides complete common commands:</p>
<pre>
<code class="language-bash">$ cheat tar

# To extract an uncompressed archive:
tar -xvf /path/to/foo.tar

# To extract a .tar in specified Directory:
tar -xvf /path/to/foo.tar -C /path/to/destination/

# To create an uncompressed archive:
tar -cvf /path/to/foo.tar /path/to/foo/

# To extract a .tgz or .tar.gz archive:
tar -xzvf /path/to/foo.tgz
tar -xzvf /path/to/foo.tar.gz
[...]</code></pre><p>It's exactly what you need, when you need it.</p>
<p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">More Linux resources</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux commands cheat sheet">Linux commands cheat sheet</a></div>
              <div class="field__item"><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Advanced Linux commands cheat sheet">Advanced Linux commands cheat sheet</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Free online course: RHEL technical overview">Free online course: RHEL technical overview</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux networking cheat sheet">Linux networking cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="SELinux cheat sheet">SELinux cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux common commands cheat sheet">Linux common commands cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="What are Linux containers?">What are Linux containers?</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Register for your free Red Hat account">Register for your free Red Hat account</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Our latest Linux articles">Our latest Linux articles</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<h2 id="the-cheat-command">The Linux cheat command</h2>
<p>The <code>cheat</code> command is a utility to search for and display a list of example tasks you might do with a Linux command. As with many Unix commands, there are different implementations of the same concept, including one <a href="https://github.com/cheat/cheat">written in Go</a> and one, which I help maintain, <a href="https://gitlab.com/slackermedia/cheat">written in just 100 lines of Bash</a>.</p>
<p>To install the Go version, download <a href="https://github.com/cheat/cheat/releases">the latest release</a> and put it somewhere in <a href="https://opensource.com/article/17/6/set-path-linux">your path</a>, such as <code>~/.local/bin/</code> or <code>/usr/local/bin</code>. To install the Bash version, download the latest release and run the <code>install-cheat.sh</code> script:</p>
<pre>
<code class="language-bash">$ sh ./install-cheat.sh</code></pre><p>Or to configure the installation, use <a href="https://opensource.com/article/19/7/introduction-gnu-autotools">Autotools</a>:</p>
<pre>
<code class="language-bash">$ aclocal ; autoconf
$ automake --add-missing ; autoreconf
$ ./configure --prefix=$HOME/.local
$ make
$ make install</code></pre><h2 id="get-cheat-sheets-for-your-terminal">Get cheat sheets for your Linux terminal</h2>
<p>Cheat sheets are just plain text files containing common commands. The main collection of cheat sheets is available at <a href="https://github.com/cheat/cheatsheets">Github.com/cheat/cheatsheets</a>. The Go version of cheat downloads cheatsheets for you when you first run the command. If you're using the Bash version of cheat, the <code>--fetch</code> option downloads cheatsheets for you:</p>
<pre>
<code class="language-bash">$ cheat --fetch</code></pre><p>As with <code>man</code> pages, you can have multiple collections of cheat sheets on your system. The Go version of cheat uses a <a href="https://opensource.com/article/21/9/yaml-cheat-sheet">YAML</a> config file to define where each collection is located. The Bash version defines the path during the install, and by default downloads the <a href="https://github.com/cheat/cheatsheets">Github.com/cheat/cheatsheets</a> collection as well as <a href="http://Opensource.com">Opensource.com</a>'s own <a href="https://gitlab.com/opensource.com/cheatsheets">Gitlab.com/opensource.com/cheatsheets</a> collection.</p>
<h2 id="list-cheat-sheets">List cheat sheets</h2>
<p>To list the cheat sheets on your system, use the <code>--list</code> option:</p>
<pre>
<code class="language-bash">$ cheat --list
7z
ab
acl
alias
ansi
ansible
ansible-galaxy
ansible-vault
apk
[...]</code></pre><h2 id="view-a-cheat-sheet">View a Linux cheat sheet</h2>
<p>Viewing a cheat sheet is as easy as viewing a <code>man</code> or <code>info</code> page. Just provide the name of the command you need help with:</p>
<pre>
<code class="language-bash">$ cheat alias

# To show a list of your current shell aliases:
alias

# To alias `ls -l` to `ll`:
alias ll='ls -l'</code></pre><p>By default, the <code>cheat</code> command uses your environment's pager. Your pager is set with the <code>PAGER</code> <a href="https://opensource.com/article/19/8/what-are-environment-variables">environment variable</a>. You can override that temporarily by redefining the <code>PAGER</code> variable before running the <code>cheat</code> command:</p>
<pre>
<code class="language-bash">$ PAGER=most cheat less</code></pre><p>If you just want to <a href="https://opensource.com/article/19/2/getting-started-cat-command">cat</a> the cheat sheet into your terminal without a pager, the Bash version has a <code>--cat</code> option for convenience:</p>
<pre>
<code class="language-bash">$ cheat --cat less</code></pre><h2 id="its-not-actually-cheating">It's not actually cheating</h2>
<p>The cheat system cuts to the chase. You don't have to piece together clues about how to use a command. You just follow the examples. Of course, for complex commands, it's not a shortcut for a thorough study of the actual documentation, but for quick reference, it's as fast as it gets.</p>
<p>You can even create your own cheat sheet just by placing a file in one of the cheat sheet collections. Good news! Because the projects are open source, you can contribute your personal cheat sheets to the GitHub collection. And more good news! When there's a new Opensource.com <a href="https://opensource.com/downloads" target="_blank">cheat sheet</a> release, we'll include a plain text version from now on so you can add that to your collection.</p>
<p>The command is called <code>cheat</code>, but as any Linux user will assure you, it's not actually cheating. It's working smarter, the open source way.</p>
