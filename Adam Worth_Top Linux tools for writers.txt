<p>If you've read <a href="https://opensource.com/article/18/2/my-linux-story-Antergos" target="_blank">my article about how I switched to Linux</a>, then you know that I’m a superuser. I also stated that I’m not an “expert” on anything. That’s still fair to say. But I have learned many helpful things over the last several years, and I'd like to pass these tips along to other new Linux users.</p>
<p>Today, I’m going to discuss the tools I use when I write. I based my choices on three criteria:</p>
<ol><li align="left">My main writing tool must be compatible for any publisher when I submit stories or articles.</li>
<li align="left">The software must be quick and simple to use.</li>
<li align="left">Free is good.</li>
</ol><p>There are some wonderful all-in-one free solutions, such as:</p>
<ol><li><a href="http://www.bibisco.com/" target="_blank">bibisco</a></li>
<li><a href="http://www.theologeek.ch/manuskript/" target="_blank">Manuskript</a></li>
<li><a href="http://ostorybook.tuxfamily.org/index.php?lng=en" target="_blank">oStorybook</a></li>
</ol><p>However, I tend to get lost and lose my train of thought when I'm trying to find information, so I opted to go with multiple applications that suit my needs. Also, I don’t want to be reliant on the internet in case service goes down. I set these programs up on my monitor so I can see them all at once.</p>
<p>Consider the following tools suggestions—everyone works differently, and you might find some other app that better fits the way you work. These tools are current to this writing:</p>
<h2>Word processor</h2>
<p><a href="https://www.libreoffice.org/" target="_blank">LibreOffice 6.0.1</a>. Until recently, I used <a href="http://wps-community.org/" target="_blank">WPS</a>, but font-rendering problems (Times New Roman was always in bold format) nixed it. The newest version of LibreOffice adapts to Microsoft Office very nicely, and the fact that it's open source ticks the box for me.</p>
<h2>Thesaurus</h2>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Linux resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux commands cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Free online course: RHEL Technical Overview</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux networking cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">SELinux cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux common commands cheat sheet</a></li>
<li><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">What are Linux containers?</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Our latest Linux articles</a></li>
</ul></div>
</div>
</div>
</div>
<p><a href="https://sourceforge.net/projects/artha/" target="_blank">Artha</a> gives you synonyms, antonyms, derivatives, and more. It’s clean-looking and <em>fast.</em> Type the word "fast," for example, and you'll get the dictionary definition as well as the other options listed above. Artha is a huge gift to the open source community, and more people should try it as it seems to be one of those obscure little programs. If you’re using Linux, install this application now. You won’t regret it.</p>
<h2>Note-taking</h2>
<p><a href="http://zim-wiki.org/" target="_blank">Zim</a> touts itself as a desktop wiki, but it’s also the easiest multi-level note-taking app you’ll find anywhere. There are other, prettier note-taking programs available, but Zim is exactly what I need to manage my characters, locations, plots, and sub-plots.</p>
<h2>Submission tracking</h2>
<p>I once used a proprietary piece of software called <a href="http://www.filemaker.com/" target="_blank">FileMaker Pro</a>, and it spoiled me. There are plenty of database applications out there, but in my opinion the easiest one to use is <a href="https://www.glom.org/" target="_blank">Glom</a>. It suits my needs graphically, letting me enter information in a form rather than a table. In Glom, you create the form you need so you can see relevant information instantly (for me, digging through a spreadsheet table to find information is like dragging my eyeballs over shards of glass). Although Glom no longer appears to be in development, it remains relevant.</p>
<h2>Research</h2>
<p>I’ve begun using <a href="https://www.startpage.com/" target="_blank">StartPage.com</a> as my default search engine. Sure, <a href="https://www.google.com/" target="_blank">Google</a> can be one of your best friends when you're writing. But I don't like how Google tracks me every time I want to learn about a specific person/place/thing. So I use StartPage.com instead; it's fast and does not track your searches. I also use <a href="https://duckduckgo.com/" target="_blank">DuckDuckGo.com</a> as an alternative to Google.</p>
<h2>Other tools</h2>
<p><a href="https://www.chromium.org/" target="_blank">Chromium Browser</a> is an open source version of <a href="https://www.google.com/chrome/" target="_blank">Google Chrome</a>, with privacy plugins.</p>
<p>Though <a href="https://www.mozilla.org/en-US/thunderbird/" target="_blank">Thunderbird</a>, from <a href="https://www.mozilla.org/en-US/" target="_blank">Mozilla</a>, is a great program, I find <a href="https://wiki.gnome.org/Apps/Geary" target="_blank">Geary</a> a much quicker and lighter email app. For more on open source email apps, read <a href="https://opensource.com/users/jason-baker" target="_blank">Jason Baker</a>'s excellent article, <a href="https://opensource.com/business/18/1/desktop-email-clients" target="_blank">Top 6 open source desktop email clients</a>.</p>
<p>As you might have noticed, my taste in apps tends to merge the best of Windows, MacOS, and the open source Linux alternatives mentioned here. I hope these suggestions help you discover helpful new ways to compose (thank you, Artha!) and track your written works.</p>
<p>Happy writing!</p>
