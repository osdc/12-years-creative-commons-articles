<p>You wrote a Python library. I'm sure it's amazing! Wouldn't it be neat if it was easy for people to use it? Here is a checklist of things to think about and concrete steps to take when open sourcing your Python library.</p>
<h2 id="source">1. Source</h2>
<p>Put the code up on <a href="https://github.com/" target="_blank">GitHub</a>, where most open source projects happen and where it is easiest for people to submit pull requests.</p>
<h2 id="license">2. License</h2>
<p>Choose an open source license. A good, permissive default is the <a href="https://en.wikipedia.org/wiki/MIT_License" target="_blank">MIT License</a>. If you have specific requirements, Creative Common's <a href="https://choosealicense.com/" target="_blank">Choose a License</a> can guide you through the alternatives. Most importantly, there are three rules to keep in mind when choosing a license:</p>
<ul><li>Don't create your own license.</li>
<li>Don't create your own license.</li>
<li>Don't create your own license.</li>
</ul><h2 id="readme">3. README</h2>
<p>Put a file called README.rst, formatted with ReStructured Text, at the top of your tree.</p>
<p>GitHub will render ReStructured Text just as well as Markdown, and ReST plays better with Python's documentation ecosystem.</p>
<h2 id="tests">4. Tests</h2>
<p>Write tests. This is not useful just for you: it is useful for people who want to make patches that avoid breaking related functionality.</p>
<p>Tests help collaborators collaborate.</p>
<p>Usually, it is best if they are runnable with <a href="https://docs.pytest.org/en/latest/" target="_blank"><strong>pytest</strong></a>. There are other test runners—but very little reason to use them.</p>
<h2>5. Style</h2>
<p>Enforce style with a linter: PyLint, Flake8, or Black with <strong>--check</strong>. Unless you use Black, make sure to specify configuration options in a file checked into source control.</p>
<h2 id="api-documentation">6. API documentation</h2>
<p>Use docstrings to document modules, functions, classes, and methods.</p>
<p>There are a few styles you can use. I prefer the <a href="https://github.com/google/styleguide/blob/gh-pages/pyguide.md" target="_blank">Google-style docstrings</a>, but <a href="https://www.python.org/dev/peps/pep-0287/" target="_blank">ReST docstrings</a> are an option.</p>
<p>Both Google-style and ReST docstrings can be processed by Sphinx to integrate API documentation with prose documentation.</p>
<h2 id="prose-documentation">7. Prose documentation</h2>
<p>Use <a href="http://www.sphinx-doc.org/en/master/" target="_blank">Sphinx</a>. (Read <a href="https://opensource.com/article/18/11/building-custom-workflows-sphinx">our article on it</a>.) A tutorial is useful, but it is also important to specify what this thing <em>is</em>, what it is good for, what it is bad for, and any special considerations.</p>
<h2 id="building">8. Building</h2>
<p>Use <strong>tox</strong> or <strong>nox</strong> to automatically run your tests and linter and build the documentation. These tools support a "dependency matrix." These matrices tend to explode fast, but try to test against a reasonable sample, such as Python versions, versions of dependencies, and possibly optional dependencies you install.</p>
<h2 id="packaging">9. Packaging</h2>
<p>Use <a href="https://pypi.org/project/setuptools/" target="_blank">setuptools</a>. Write a <strong>setup.py</strong> and a <strong>setup.cfg</strong>. If you support both Python 2 and 3, specify universal wheels in the <strong>setup.cfg</strong>.</p>
<p>One thing <strong>tox</strong> or <strong>nox</strong> should do is build a wheel and run tests against the installed wheel.</p>
<p>Avoid C extensions. If you <em>absolutely</em> need them for performance or binding reasons, put them in a separate package. Properly packaging C extensions deserves its own post. There are a lot of gotchas!</p>
<h2 id="continuous-integration">10. Continuous integration</h2>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Python Resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/topics/middleware/what-is-ide?intcmp=7016000000127cYAAQ">What is an IDE?</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-python-37-beginners?intcmp=7016000000127cYAAQ">Cheat sheet: Python 3.7 for beginners</a></li>
<li><a href="https://opensource.com/resources/python/gui-frameworks?intcmp=7016000000127cYAAQ">Top Python GUI frameworks</a></li>
<li><a href="https://opensource.com/downloads/7-essential-pypi-libraries?intcmp=7016000000127cYAAQ">Download: 7 essential PyPI libraries</a></li>
<li><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ">Red Hat Developers</a></li>
<li><a href="https://opensource.com/tags/python?intcmp=7016000000127cYAAQ">Latest Python content</a></li>
</ul></div>
</div>
</div>
</div>
<p>Use a public continuous integration runner. <a href="https://travis-ci.org/" target="_blank">TravisCI</a> and <a href="https://circleci.com/" target="_blank">CircleCI</a> offer free tiers for open source projects. Configure GitHub or other repo to require passing checks before merging pull requests, and you'll never have to worry about telling people to fix their tests or their style in code reviews.</p>
<h2 id="versions">11. Versions</h2>
<p>Use either <a href="https://semver.org/" target="_blank">SemVer</a> or <a href="https://calver.org/" target="_blank">CalVer</a>. There are many tools to help manage versions: <a href="https://pypi.org/project/incremental/" target="_blank">incremental</a>, <a href="https://pypi.org/project/bumpversion/" target="_blank">bumpversion</a>, and <a href="https://pypi.org/project/setuptools_scm/" target="_blank">setuptools_scm</a> are all packages on PyPI that help manage versions for you.</p>
<h2 id="release">12. Release</h2>
<p>Release by running <strong>tox</strong> or <strong>nox</strong> and using <strong>twine</strong> to upload the artifacts to PyPI. You can do a "test upload" by running <a href="https://opensource.com/article/18/7/setting-devpi">DevPI</a>.</p>
