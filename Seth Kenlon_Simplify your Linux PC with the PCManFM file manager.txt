<p>The PCMan File Manager, or PCManFM for short, is a fast and lightweight file manager that's full of features. It was developed for the <a href="https://opensource.com/article/19/12/lxqt-lxde-linux-desktop">LXDE</a> desktop environment, but is a stand-alone application and can be used with the desktop or window manager of your choice.</p>
<h2 id="_install_pcmanfm">Install PCManFM</h2>
<p>On Linux, you can probably find PCManFM in your software repository. For instance, on Fedora, Mageia, and similar:</p>
<pre class="highlight">
<code class="language-bash" data-lang="bash">$ sudo dnf install pcmanfm</code></pre><p>On Debian, Elementary, and similar:</p>
<pre class="highlight">
<code class="language-bash" data-lang="bash">$ sudo apt install pcmanfm</code></pre><article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-10/pcmanfilemanager.png" width="575" height="412" alt="Image of the PCMan file manager." /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Seth Kenlon, CC BY-SA 4.0)</p>
</div>
      
  </article><p>PCManFM doesn't have to replace your desktop's file manager, but some distributions assume that when you install a "third party" file manager, you want it to take precedence over the default. Depending on the desktop you're using, there are different ways of setting your default file manager. Usually, it's in <strong>System Settings</strong> under <strong>Default Applications</strong>.</p>
<p>If your desktop environment or window manager has no interface to select your default applications, you can set your preference in the <code>~/.local/share/applications/mimeapps.list</code> file. To designate a file manager as default, place it at the top of the <code>[Default Applications]</code> section, first specifying the file type and then the name of the application file (as it appears in <code>/usr/share/applications</code>) you want to use to open it:</p>
<pre class="highlight">
<code class="language-bash" data-lang="bash">inode/directory=myfilemanager.desktop;</code></pre><h2 id="_pcmanfm">PCManFM</h2>
<p>If you're a fan of GNOME 2 or the Mate project's <a href="https://opensource.com/article/22/12/linux-file-manager-caja" target="_blank" title="caja">Caja file manager</a>, then PCManFM is a great alternative to consider. PCManFM is a lot like Caja in design, but it's not bound to the desktop in the way Caja is, so it's available even on the latest GNOME desktop.</p>
<p>The default layout of PCManFM has a helpful toolbar near the top of the window, a side panel providing quick access to common directories and drives, and a status bar with details about your current selection. You can hide or show any of these elements using the <strong>View</strong> menu.</p>
<p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">More Linux resources</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux commands cheat sheet">Linux commands cheat sheet</a></div>
              <div class="field__item"><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Advanced Linux commands cheat sheet">Advanced Linux commands cheat sheet</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Free online course: RHEL technical overview">Free online course: RHEL technical overview</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux networking cheat sheet">Linux networking cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="SELinux cheat sheet">SELinux cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux common commands cheat sheet">Linux common commands cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="What are Linux containers?">What are Linux containers?</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Register for your free Red Hat account">Register for your free Red Hat account</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Our latest Linux articles">Our latest Linux articles</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<h2 id="_tabs_and_panels">Tabs and panels</h2>
<p>PCManFM also uses tabs. If you've never used a tabbed file manager before, then think of a web browser and how it uses tabs to let you open multiple web pages in just one window. PCManFM can similarly open several directories in the same window.</p>
<p>To transfer a file or folder from one tab to another, just drag the file's icon to the tab and hover. After a nominal delay, PCManFM brings the target tab to the front so you can continue your drag-and-drop operation. It takes some getting used to if you're not used to interacting with tabs in a file manager, but it doesn't take long, and it's a very powerful feature for decluttering your workspace.</p>
<p>Another nice feature of the PCManFM interface is its ability to split a window into two panels. Each panel is effectively a tab, but each one only takes up half of the window.</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-10/%E2%80%8BDual.panel_.in%20PCManFM.png" width="640" height="410" alt="Image of dual panels in PCMan.png" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Seth Kenlon, CC BY-SA 4.0)</p>
</div>
      
  </article><p>This makes dragging from one to the other just as easy and natural as dragging a file into a folder. I find it useful for comparing the contents of folders, too.</p>
<h2 id="_file_management_with_pcman">File management with PCMan</h2>
<p>PCManFM is a great little file manager with all the basic features you need on an everyday basis. It's a natural replacement for a file manager you might find too complicated, as well as a great option on <a href="https://opensource.com/article/22/10/obsolete-computer-linux-opportunity" target="_blank">old computers</a> that might struggle with a file manager that's constantly drawing thumbnails and refreshing and generating animations. PCMan focuses on the core task of a file manager: managing files. Try it out on your Linux PC.</p>
