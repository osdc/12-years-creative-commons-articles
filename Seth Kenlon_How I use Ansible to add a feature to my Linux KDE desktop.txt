<p>I run the KDE Plasma Desktop on my computer because it's a <a href="https://opensource.com/article/19/12/linux-kde-plasma">flexible environment</a> with lots of options for customization. Having choices in your desktop is about more than just having lots of menus and buttons to activate or deactivate. The thing I love most about KDE Plasma Desktop is the ability to add my own features to it. One reason this is possible is KServices, a simple but powerful plugin framework for handling desktop services.</p>
<h2 id="_add_functions_to_the_right_click_menu">Add functions to the right-click menu</h2>
<p>In the KDE Plasma Desktop, there's usually a contextual menu available when you right-click on something, such as a directory or a file. You can add items to the right-click menu by creating your own KService, and you don't need anything more than a rudimentary understanding of Bash to make it work.</p>
<p>First, create a new directory for your service menu:</p>
<pre class="highlight">
<code class="language-bash" data-lang="bash">$ mkdir -p ~/.local/share/kio/servicemenus</code></pre><p>Add a <code>.desktop</code> file:</p>
<pre class="highlight">
<code class="language-bash" data-lang="bash">$ touch ~/.local/share/kio/servicemenus/hello.desktop</code></pre><p>Open the <code>hello.desktop</code> file in a text editor. A <code>.desktop</code> file is a small configuration file used by the menu system of the Linux desktop. Here's a simple KServices file that generates a <code>hello.txt</code> file in the directory you select:</p>
<pre class="highlight">
<code class="language-text" data-lang="text">[Desktop Entry]
Type=Service
MimeType=inode/directory;
Actions=Hello

[Desktop Action Hello]
Name=Say hello
Icon=/usr/share/icons/breeze-dark/actions/symbolic/file-library-symbolic.svg
Exec=touch %f/hello.txt</code></pre><ul><li>
<p>The first configuration block tells your system that this <code>.desktop</code> file is a service rather than, for instance, an application.</p>
</li>
<li>
<p>The <code>MimeType</code> tells the Plasma Desktop to only show this action as an option when you right-click on a folder, not a file.</p>
</li>
<li>
<p>The <code>Actions</code> line identifies what action is taken when this service is activated. The name <code>Hello</code> is arbitrary, and refers to the next configuration block, which is a <code>Desktop Action</code> configuration with the name <code>Hello</code>.</p>
</li>
</ul><p>Here's what the next configuration block means:</p>
<ul><li>
<p>The <code>Desktop Action</code> definition named <code>Hello</code>.</p>
</li>
<li>
<p>The values for <code>Name</code> and <code>Icon</code> appear in the right-click menu.</p>
</li>
<li>
<p>The <code>Exec</code> line is the command you want to run when your service is selected. As a simple demonstration, this <code>.desktop</code> file just creates an empty file called <code>hello.txt</code> in the location that you right-clicked on (represented by the special variable <code>%f</code>).</p>
</li>
</ul><p>Save the <code>.desktop</code> file, and then make it executable:</p>
<pre class="highlight">
<code class="language-bash" data-lang="bash">$ chmod +x ~/.local/share/kio/servicemenus/hello.desktop</code></pre><p>Start a new instance of the <a href="https://opensource.com/article/22/12/linux-file-manager-dolphin" target="_blank">Dolphin file manager</a> and create an empty folder. Then right-click on the folder and navigate to <strong>Actions</strong>. In the <strong>Actions</strong> menu, there's a new service available, and it's called <strong>Say hello</strong> because that's what your <code>.desktop</code> file has set in the <code>Name</code> field.</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2023-02/kde-plasma-desktop-kservices.png" width="731" height="436" alt="​Image showing that KServices appears in the right-click menu of Dolphin. " /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Seth Kenlon, CC BY-SA 4.0)</p>
</div>
      
  </article><p>Look in the folder after running the service to see the empty <code>hello.txt</code> file that's been created.</p>
<h2 id="_mimetypes">Mimetypes</h2>
<p>This sample KService only works on directories because the <code>.desktop</code> file defining the service specifies the <code>Mimetype: inode/directory</code>. That's what tells Dolphin not to display the service when you right-click on a file.</p>
<p>Using mimetypes, you can create highly specific services based on what kind of file system object you select when you right-click. The <code>perl-file-mimeinfo</code> package provides a <code>mimetype</code> command, which you can use to get the mimetype of any file. Install it with your distribution's package manager, and then try it out:</p>
<pre class="highlight">
<code class="language-bash" data-lang="bash">$ mimetype example.txt
example.txt: text/plain
$ mimetype Photos/example.webp
Photos/example.webp: image/webp</code></pre><p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">More Linux resources</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux commands cheat sheet">Linux commands cheat sheet</a></div>
              <div class="field__item"><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Advanced Linux commands cheat sheet">Advanced Linux commands cheat sheet</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Free online course: RHEL technical overview">Free online course: RHEL technical overview</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux networking cheat sheet">Linux networking cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="SELinux cheat sheet">SELinux cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux common commands cheat sheet">Linux common commands cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="What are Linux containers?">What are Linux containers?</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Register for your free Red Hat account">Register for your free Red Hat account</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Our latest Linux articles">Our latest Linux articles</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<h2 id="_executables">Executables</h2>
<p>I demonstrated a simple "hello world" example in this article, but KServices can be as complex as you need them to be. The <code>Exec</code> line of your KService <code>.desktop</code> file can launch any application or script, and the <code>%f</code> variable ensures that the target or destination of whatever gets launched is what you've right-clicked on.</p>
<p>For my own workflow, I used to use <a href="https://opensource.com/article/17/7/managing-creative-assets-planter">Planter</a> to quickly construct a project environment. Lately, though, I've switched to Ansible and this KService:</p>
<pre class="highlight">
<code class="language-bash" data-lang="bash">[Desktop Entry]
Type=Service
MimeType=inode/directory;
Actions=planter

[Desktop Action planter]
Name=Create project directory
Icon=/usr/share/icons/breeze-dark/actions/symbolic/folder-new-symbolic.svg
Exec=ansible-playbook /home/seth/Ansible/playbooks/standard_dirs.yaml -e dir=%f</code></pre><p>Here's my Ansible playbook:</p>
<pre class="highlight">
<code class="language-yaml" data-lang="yaml">---
- hosts: localhost
  tasks:
    - name: Create directories
      ansible.builtin.file:
        path: "{{ item }}"
        state: directory
      with_items:
        - '{{ dir }}/video'
        - '{{ dir }}/edit'
        - '{{ dir }}/audio'
        - '{{ dir }}/title'
        - '{{ dir }}/render'
        - '{{ dir }}/graphic'
        - '{{ dir }}/photo'</code></pre><p>When I right-click on a directory and select <strong>Create project directory</strong>, the subdirectories I need for media projects are added to that directory. It's a simple feature for a desktop, and a little unique to a specific workflow, but it's the feature I want. And thanks to KServices, it's a feature I have. Try out KServices in the KDE Plasma Desktop for yourself, and add the feature you want.</p>
