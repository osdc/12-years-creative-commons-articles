<p>One of the key ideas of DevOps is <i>infrastructure-as-code</i>—having the infrastructure for your delivery/deployment pipeline expressed in code—just as the products that flow it.</p>
<p>The <a href="https://opensource.com/sitewide-search?search_api_views_fulltext=jenkins">Jenkins</a> workflow tool is one of the primary applications that has been used to create many Continuous Delivery/Deployment pipelines. This was most commonly done via defining a series of individual jobs for the various pipeline tasks. Each job was configured via web forms—filling in text boxes, selecting entries from drop-down lists, etc. And then the series of jobs were strung together, each triggering the next, into a pipeline.</p>
<p>Although widely used, this way of creating and connecting Jenkins jobs to form a pipeline was challenging. It did not meet the definition of <i>infrastructure-as-code</i>. Job configurations were stored only as XML files within the Jenkins configuration area. This meant that the files were not easily readable or directly modifiable. And the Jenkins application itself provided the user's primary view and access to them.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Programming and development</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ&amp;src=programming_resource_menu4">Red Hat Developers Blog</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Programming cheat sheets</a></li>
<li><a href="https://www.redhat.com/en/services/training/learning-subscription?intcmp=7016000000127cYAAQ">Try for free: Red Hat Learning Subscription</a></li>
<li><a href="https://opensource.com/downloads/bash-programming-guide?intcmp=7016000000127cYAAQ">eBook: An introduction to programming with Bash</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/bash-shell-cheat-sheet?intcmp=7016000000127cYAAQ">Bash Shell Scripting Cheat Sheet</a></li>
<li><a href="https://developers.redhat.com/e-books/modernizing-enterprise-java?intcmp=7016000000127cYAAQ">eBook: Modernizing Enterprise Java</a></li>
</ul></div>
</div>
</div>
</div>
<p>The growth of DevOps has led to more tooling that can implement the infrastructure-as-code paradigm to a larger degree. Jenkins has somewhat lagged in this area until the release of Jenkins 2. (Here <i>Jenkins 2</i> is the name we are generally applying to newer versions that support the <i>pipeline-as-code</i> functionality, as well as other features.) In this article, we'll learn how we can create pipelines as code in Jenkins 2. We'll do this using two different styles of pipeline syntax—scripted and declarative. We'll also touch on how we can store our pipeline code outside of Jenkins, but still have Jenkins run it when it changes, to meet one of the goals for implementing DevOps.</p>
<p>Let's start by talking about the foundations. Jenkins and its plugins make the building blocks available for your pipeline tasks via its own programming steps—the Jenkins domain-specific language (DSL).</p>
<h2>DSL steps</h2>
<p>When Jenkins started evolving toward the pipeline-as-code model, some of the earliest changes came in the form of a plugin—the <i>workflow</i> plugin. This included the creation of a set of initial DSL steps that allowed for coding up simple jobs in Jenkins and, by extension, simple pipelines.</p>
<p>These days, the different DSL steps available in Jenkins are provided by many different plugins. For example, we have a <i>git</i> step provided via the Git plugin to retrieve source code from the source control system. An example of its use is shown in Listing 1:</p>
<pre><code class="language-text">node ('worker_node1') {
     stage('Source') {
         // Get some code from our Git repository
         git 'http://github.com/brentlaster/roarv2'
      }
}</code></pre><p class="rtecenter"><em>Listing 1: Example pipeline code line using the Git step.</em></p>
<p>In fact, to be compatible with Jenkins 2, current plugins are expected to supply DSL steps for use in pipelines. Pipelines incorporate DSL steps to do the actions traditionally done through the web forms. But, to make up a full pipeline, there are still other supporting pieces and structure needed. Jenkins 2 allows two styles of structure and syntax for building out pipelines. These are referred to as <i>scripted</i> and <i>declarative</i>.</p>
<h2>Scripted Pipelines</h2>
<p>The original approach for creating pipelines in Jenkins 2 is now referred to as <i>scripted</i>. At the highest level, <a href="https://jenkins.io/doc/book/pipeline/syntax/#scripted-pipeline">Scripted Pipelines</a> are wrapped in a <i>node</i> block. Here a <i>node</i> refers to a system that contains the Jenkins <i>agent</i> pieces and can run jobs (formerly referred to as a <i>slave</i> instance). The node gets mapped to a system by using a <i>label</i>. A label is simply an identifier that has been added when configuring one or more nodes in Jenkins (done via the global <i>Manage Nodes</i> section). An example is shown in Figure 1.</p>
<p><article class="media media--type-image media--view-mode-full" title="Figure 1. Node configuration with labels"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/laster_f1.png" width="1441" height="441" alt="Figure 1. Node configuration with labels" title="Figure 1. Node configuration with labels" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>Figure 1. Node configuration with labels</p>
</div>
      
  </article></p>
<p>The node <i>block</i> is a construct from the <a href="https://opensource.com/sitewide-search?search_api_views_fulltext=groovy">Groovy programming language</a> called a <i>closure</i> (denoted by the opening and closing braces). In fact, Scripted Pipelines can include and make use of any valid Groovy code. As an example, Listing 2 shows Scripted Pipeline code that does several things:</p>
<ul><li>retrieves source code (via the <code>git</code> pipeline step);</li>
<li>gets the value of a globally defined Gradle installation (via the <code>tool</code> pipeline step) and puts it into a Groovy variable; and</li>
<li>calls the shell to execute it (via the <code>sh</code> pipeline step):<br /><pre><code class="language-text">// Scripted Pipeline
node ('worker_node1') {
  // get code from our Git repository
  git 'http://github.com/brentlaster/roarv2'
   // get Gradle HOME value
  def gradleHome = tool 'gradle4'
   // run Gradle to execute compile and unit testing
  sh "'${gradleHome}/bin/gradle' clean compileJava test"
}</code></pre></li>
</ul><p class="rtecenter"><em>Listing 2: Scripted syntax for using the tool and sh steps.</em></p>
<p>Here <i>gradleHome</i> is a Groovy variable used to support the DSL steps. Such a pipeline can be defined via Jenkins by creating a new <i>Pipeline</i> project and typing the code in the Pipeline Editor section at the bottom of the page for the new project, as shown in Figure 2:</p>
<p><article class="media media--type-image media--view-mode-full" title="Figure 2: Defining a new Pipeline project"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/laster_f2.png" width="886" height="743" alt="Figure 2: Defining a new Pipeline project" title="Figure 2: Defining a new Pipeline project" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>Figure 2: Defining a new Pipeline project</p>
</div>
      
  </article></p>
<p>Although this simple node block is technically valid syntax, Jenkins pipelines generally have a further level of granularity—stages. A <i>stage</i> is a way to divide up the pipeline into logical functional units. It also serves to group DSL steps and Groovy code together to do targeted functionality. Listing 3 shows our Scripted Pipeline with a stage definition for the build pieces and one for the source management piece:</p>
<pre><code class="language-text">// Scripted Pipeline with stages
node ('worker_node1') {
  stage('Source') { // Get code
    // get code from our Git repository
    git 'http://github.com/brentlaster/roarv2'
  }
  stage('Compile') { // Compile and do unit testing
    // get Gradle HOME value
    def gradleHome = tool 'gradle4'
    // run Gradle to execute compile and unit testing
    sh "'${gradleHome}/bin/gradle' clean compileJava test"
  }
}</code></pre><p class="rtecenter"><em>Listing 3: Scripted syntax with stages</em></p>
<p>Each stage in a pipeline also gets its own output area in the new default Jenkins output screen—the <i>Stage View</i>. As shown in Figure 3, the Stage View output is organized as a matrix, with each row representing a run of the job, and each column mapped to a defined stage in the pipeline:</p>
<p><article class="media media--type-image media--view-mode-full" title="Figure 3: Defined stages and Stage View output"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/laster_f3.png" width="971" height="826" alt="Figure 3: Defined stages and Stage View output" title="Figure 3: Defined stages and Stage View output" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>Figure 3: Defined stages and Stage View output</p>
</div>
      
  </article></p>
<p>Each cell in this matrix (intersection of a row and column) also shows timing information and uses colors as an indication of success or failure. The key to each color is shown in Table 1:</p>
<table border="1" cellpadding="1" cellspacing="1" style="width: 500px;"><caption><em>Table 1: Color codes for stage execution</em></caption>
<tbody><tr><td>
<p style="margin-bottom: 0.11in; line-height: 108%">Color</p>
</td>
<td>Meaning</td>
</tr><tr><td>White</td>
<td>Stage has not been run yet</td>
</tr><tr><td>Blue stripes</td>
<td>Processing in progress</td>
</tr><tr><td>Green</td>
<td>Stage succeeded</td>
</tr><tr><td>Red</td>
<td>Stage succeeded, but downstream stage failed</td>
</tr><tr><td>Red stripes</td>
<td>Stage failed</td>
</tr></tbody></table><p>Additionally, by hovering over the cell, pop-ups will provide more information about the execution of that stage with links to logs, as shown in Figure 4:</p>
<p><article class="media media--type-image media--view-mode-full" title="Figure 4: Drilling down into the logs in stage output"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/laster_f4.png" width="810" height="595" alt="Figure 4: Drilling down into the logs in stage output" title="Figure 4: Drilling down into the logs in stage output" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><em>Figure 4: Drilling down into the logs in stage output</em></p>
</div>
      
  </article></p>
<p>Although Scripted Pipelines provide a great deal of flexibility, and can be comfortable and familiar for programmers, they do have their drawbacks for some users. In particular:</p>
<ul><li>It can feel like you need to know Groovy to create and modify them.</li>
<li>They can be challenging to learn to use for traditional Jenkins users.</li>
<li>They provide Java-style tracebacks to show errors.</li>
<li>There is no built-in post-build processing.</li>
</ul><p>The last item in the list above can be a big one if you're used to being able to do tasks such as sending email notifications or archiving results via the post-build processing in Freestyle jobs. If your Scripted Pipeline code fails and throws an exception, then the end of your pipeline may never be reached; however, you can handle this with a Java-style try-catch block around your main Scripted Pipeline code as shown in Figure 5:</p>
<p><article class="media media--type-image media--view-mode-full" title="Figure 5: try-catch block to emulate post-build notifications"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/laster_f5.png" width="491" height="186" alt="Figure 5: try-catch block to emulate post-build notifications" title="Figure 5: try-catch block to emulate post-build notifications" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><em>Figure 5: try-catch block to emulate post-build notifications</em></p>
</div>
      
  </article></p>
<p>Here we have a <i>final</i> stage called <i>Notify</i>. This stage will always be executed whether the earlier parts of our pipeline succeed or fail. This is because the try-catch structure will catch any exception caused by a failure and allow control to continue afterwards. (Jenkins 2 also has a catchError block that is a simplified, limited version of the try-catch construct.)</p>
<p>This kind of workaround adds to the impression that you need to know some programming to do things in Scripted Pipelines that you previously got for free with Freestyle. In an effort to simplify things for those coming from traditional Jenkins and take some of the Java/Groovy-isms out of the mix, CloudBees and the Jenkins Community created a second style of syntax for writing pipelines—declarative.</p>
<h2>Declarative Pipelines</h2>
<p>As the name suggests, declarative syntax is more about declaring what you want in your pipeline and less about coding the logic to do it. It still uses the DSL steps as its base, but includes a well-defined structure around the steps. This structure includes many <i>directives</i> and sections that specify the items you want in your pipeline. An example Declarative Pipeline is shown in Listing 4:</p>
<pre><code class="language-text">pipeline {
  agent { label 'worker_node1' }
  stages {
    stage('Source') { // Get code
      steps {
        // get code from our Git repository
        git 'https://github.com/brentlaster/roarv2'
      }
    }
    stage('Compile') { // Compile and do unit testing
      tools {
        gradle 'gradle4'
      }
      steps {
        // run Gradle to execute compile and unit testing
        sh 'gradle clean compileJava test'
      }
    }
  }
}</code></pre><p class="rtecenter"><em>Listing 4: Example Declarative Pipeline</em></p>
<p>One of the first things you may notice here is that we have an <i>agent</i> block surrounding our code instead of a <i>node</i> block. You can think of agents and nodes as being the same thing—specifying a system on which to run. (However, agent also includes additional ways to create systems, such as via Dockerfiles.)</p>
<p>Beyond that, we have other directive blocks in our pipeline, such as the <i>tools</i> one, where we declare our Gradle installation to use (rather than assigning it to a variable as we did in the scripted syntax). In the declarative syntax, you cannot use Groovy code such as assignments or loops. You are restricted to the structured sections/blocks and the DSL steps.</p>
<p>Additionally, in a Declarative Pipeline, the DSL steps within a stage must be enclosed in a <i>steps</i> directive. This is because you can also declare other things besides steps in a stage.</p>
<p>Figure 6 shows a diagram from the <i><a href="http://shop.oreilly.com/product/0636920064602.do">Jenkins 2: Up and Running</a></i> book of all the sections you can have in a Declarative Pipeline. The way to read this chart is that items with solid line borders are required, and items with dotted line borders are optional:</p>
<p><article class="media media--type-image media--view-mode-full" title="Figure 6: Diagram of Declarative Pipeline sections"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/laster_f6.png" width="401" height="739" alt="Figure 6: Diagram of Declarative Pipeline sections" title="Figure 6: Diagram of Declarative Pipeline sections" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><em>Figure 6: Diagram of Declarative Pipeline sections</em></p>
</div>
      
  </article></p>
<p>You can probably get an idea of what the different sections do from their names. For example, <i>environment</i> sets environment variables and <em>tools</em> declares the tooling applications we want to use. We won't go into all the details on each one here, but the book has an entire chapter on Declarative Pipelines.</p>
<p>Note that there is a <i>post</i> section here. This emulates the functionality of the post-build processing from the Freestyle jobs and also replaces the need for syntax like the try-catch construct we used in the Scripted Pipeline. Also notice that the <i>post</i> block can occur at the end of a stage as well.</p>
<p>This kind of well-defined structure provides several advantages for working with Declarative Pipelines in Jenkins. They include:</p>
<ul><li>easier transition from Freestyle to pipelines-as-code because you are declaring what you need (similar to how you fill in forms in traditional Jenkins);</li>
<li>tighter and clearer syntax/requirements checking; and</li>
<li>tighter integration with the new Blue Ocean graphical interface (well-structured sections in a pipeline map easier to individual graphical objects).</li>
</ul><p>You may be wondering what the disadvantages of Declarative Pipelines are. If you need to go beyond the scope of what the declarative syntax allows, you have to employ other means. For example, if you wanted to assign a variable to a value and use it in your pipeline, you can't do that in declarative syntax. Some plugins can also require that sort of thing to use them in a pipeline. So, they aren't directly compatible with the declarative syntax.</p>
<p>What do you do in those cases? You can add a <i>script</i> block around small amounts of non-declarative code in a stage. Code within a <i>script</i> block in a Declarative Pipeline can be any valid scripted syntax.</p>
<p>For more substantial non-declarative code, the recommended approach is to put it into an external library (called <i>shared pipeline libraries</i> in Jenkins 2). There are a few other methods as well, which are less <i>sanctioned</i>—meaning they may not be supported in the future. (The <i>Jenkins 2: Up and Running</i> book goes into much more detail on shared libraries and other alternative approaches if you are interested in finding out more.)</p>
<p>Hopefully this gives you a good idea of the differences between Scripted Pipelines and Declarative Pipelines in Jenkins 2. One last dimension to consider here is that, with Jenkins 2, you don't need to have your pipeline code stored in the Jenkins application itself. You can create it as (or copy it into) an external text file with the special name <i>Jenkinsfile</i>. Then that file can be stored with the source code in the same repository in the desired branch.</p>
<h2>Jenkinsfiles</h2>
<p>Although the Jenkins application is the main environment for developing and running our pipelines, the pipeline code itself can also be placed into an external file named <i>Jenkinsfile</i>. Doing this then allows us to treat our pipeline code as any other text file—meaning it can be stored and tracked in source control and go through regular processes, such as code reviews.</p>
<p>Moreover, the pipeline code can be stored with the product source and live along side it, getting us to that desired point of our pipeline being infrastructure-as-code.</p>
<p>Figure 7 shows a Jenkinsfile stored in a source control repository:</p>
<p><article class="media media--type-image media--view-mode-full" title="Figure 7: Jenkinsfile in source control"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/laster_f7.png" width="1052" height="460" alt="Figure 7: Jenkinsfile in source control" title="Figure 7: Jenkinsfile in source control" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><em>Figure 7: Jenkinsfile in source control</em></p>
</div>
      
  </article></p>
<p>The contents of the Jenkinsfile in source control are shown in Figure 8:</p>
<p><article class="media media--type-image media--view-mode-full" title="Figure 8: An example Jenkinsfile"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/laster_f8.png" width="809" height="903" alt="Figure 8: An example Jenkinsfile" title="Figure 8: An example Jenkinsfile" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><em>Figure 8: An example Jenkinsfile</em></p>
</div>
      
  </article></p>
<p>You may think this is an exact copy of our pipeline from earlier, but there are a couple of subtle differences. For one, we've added the groovy designator at the top. More importantly though, notice the step that gets code from the source control system. Instead of the git step that we used previously, we have an <i>scm checkout</i> step. This is a shortcut that you can take in your Jenkinsfile. Because the Jenkinsfile lives in the same repository (and branch) as the code on which it will be operating, it already assumes that this is where it should get the source code from. This means that we don't need to tell it that information explicitly.</p>
<p>The advantage of using a Jenkinsfile is that your pipeline definition lives with the source for the product going through the pipeline. Within Jenkins, you can then create a project of type <i>Multibranch Pipeline</i> and point it to the source repository with the Jenkinsfile, via the <i>Branch Sources</i> section of the job configuration. Figure 9 shows the two steps:</p>
<p><article class="media media--type-image media--view-mode-full" title="Figure 9: Creating and configuring a Multibranch Pipeline"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/laster_f9a.png" width="1244" height="266" alt="Figure 9: Creating and configuring a Multibranch Pipeline" title="Figure 9: Creating and configuring a Multibranch Pipeline" /></div>
      
  </article></p>
<p><article class="media media--type-image media--view-mode-full" title="Figure 9: Creating and configuring a Multibranch Pipeline"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/laster_f9b.png" width="982" height="506" alt="Figure 9: Creating and configuring a Multibranch Pipeline" title="Figure 9: Creating and configuring a Multibranch Pipeline" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter" style="margin-bottom: 0.11in; line-height: 108%;">Figure 9: Creating and configuring a Multibranch Pipeline</p>
</div>
      
  </article></p>
<p>Jenkins will then scan through each branch in the repository and check to see whether the branch has a Jenkinsfile. If it does, it will automatically create and configure a sub-project within the Multibranch Pipeline project to run the pipeline for that branch. Figure 10 shows an example of the scanning running and looking for Jenkinsfiles to create the jobs in a Multibranch Pipeline:</p>
<p><article class="media media--type-image media--view-mode-full" title="Figure 10: Jenkins checking for Jenkinsfiles to automatically setup jobs in a Multibranch Pipeline"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/laster_f10_0.png" width="1051" height="879" alt="Figure 10: Jenkins checking for Jenkinsfiles to automatically setup jobs in a Multibranch Pipeline" title="Figure 10: Jenkins checking for Jenkinsfiles to automatically setup jobs in a Multibranch Pipeline" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter" style="margin-bottom: 0.11in; line-height: 108%;"><em>Figure 10: Jenkins checking for Jenkinsfiles to automatically setup jobs in a Multibranch Pipeline</em></p>
</div>
      
  </article></p>
<p>And Figure 11 shows the jobs in a Multibranch Pipeline after executing against the Jenkinsfiles and source repositories:</p>
<p><article class="media media--type-image media--view-mode-full" title="Figure 11: Automatically created Multibranch Pipeline jobs based on Jenkinsfiles in source code branches"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/laster_f11.png" width="1171" height="523" alt="Figure 11: Automatically created Multibranch Pipeline jobs based on Jenkinsfiles in source code branches" title="Figure 11: Automatically created Multibranch Pipeline jobs based on Jenkinsfiles in source code branches" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><em>Figure 11: Automatically created Multibranch Pipeline jobs based on Jenkinsfiles in source code branches</em></p>
</div>
      
  </article></p>
<p>With Jenkinsfiles, we can accomplish the DevOps goal of our infrastructure (or at least the pipeline definition) being treated as code. If the pipeline needs to change, we can pull, edit, and push the Jenkinsfile within source control, just as we would for any other file. We can also do things like code reviews on the pipeline script.</p>
<p>There is one potential downside to using Jenkinsfiles: Discovering problems up front when you are working in the external file and not in the environment of the Jenkins application can be more challenging. One approach to dealing with this is developing the code within the Jenkins application as a Pipeline project first. Then, you can convert it to a Jenkinsfile afterward. Also, there is a Declarative Pipeline linter application that you can run against Jenkinsfiles, outside of Jenkins, to detect problems early.</p>
<h2>Conclusion</h2>
<p>Hopefully this article has given you a good idea of the differences and use of both Scripted and Declarative Pipelines as well as how they can be included with the source as Jenkinsfiles. If you'd like to find out more about any of these topics or related topics in Jenkins 2, you can search the web or check out the <i>Jenkins 2: Up and Running</i> book for lots more examples and explanations on using this technology to empower DevOps in your organization.</p>
