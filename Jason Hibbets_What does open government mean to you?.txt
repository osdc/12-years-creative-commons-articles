<p>Open government means different things to different people. Is it about transparency, collaboration, or participation? Maybe it’s a combination of all three? If you listen to <a title="Tim O'Reilly on open government" href="http://www.youtube.com/watch?v=dYB8xokkWjg" target="_blank">Tim O’Reilly speak</a> about open government, he'll tell you about his vision of government as a <em>platform</em>. </p>
<p><!--break--></p><p>O'Reilly talks about the components of web 2.0: cloud computing, social media, and much more. Particularly, the goods and services that government could produce. He keys in on the ability of the marketplace to deliver on big data supplied by the government; think about how the <a title="Surfing the open data wave" href="https://opensource.com/government/11/10/surfing-open-data-wave" target="_blank">NOAA provides forecast data</a>. </p>
<p>While at the Code for America Summit in San Francisco, CA this year (October 1-3) I had the chance to find out how others on the front lines of the open government movement define <em>open government</em>. I asked several participants what it means to them—their answers proved to be diverse, yet conforming. </p>
<h3>Forest Frizzell, Deputy Director City of Honolulu</h3>
<blockquote><p>"It’s being open, inclusive, and collaborative with citizens so they feel comfortable with what government is doing. So that citizens want to be part of the solution. It’s also about driving efficiencies from the governments. We do things based on tradition, not law and policy. The open government process is a vehicle to fix that."</p>
</blockquote>
<h3>Jen Palhka, Founder and Executive Director of Code for America</h3>
<blockquote><p>"It’s not just about being open so that we can hold government accountable. It’s symbolically opening the door so that it’s inviting for citizens."</p>
</blockquote>
<h3>Eugene Kim, Cofounder of Groupaya</h3>
<blockquote><p>"Ultimately, open government is about engagement and transparency. It doesn’t require technology. It’s something that people are already practicing. Seeing a situation where people can practice government in a practical way. And we need those good stories about government."</p>
</blockquote>
<h3>Brian Gryth, Founder and President of OpenColorado</h3>
<blockquote><p>"It’s the way our government is suppose to work. It’s democracy in action. It’s a government that is responsive to its people. It allows people to be more involved. Transparency builds trust. It builds a window that allows citizens to see what their government is doing. It’s building community and it’s what the founders believed in. It’s realizing the constitution."</p>
</blockquote>
<h3>Abhi Nemani, Director of Strategy and Communications at Code for America</h3>
<blockquote><p>"Governments that work like the Internet. Networked. Generative. And reflective of who we are."</p>
</blockquote>
<h3>Jason Lally, Director of the Decision Lab for PlaceMatters</h3>
<blockquote><p>"Open government means the re-establishment of trust in democratic values. Today, we hear a lot of noise in politics. The open government approach is non-partisan and it’s really about connecting people back to their communities. That means people will be connected with each other. And that’s exciting!"</p>
</blockquote>
<h3>Cyd Harrell, adviser for Code for America</h3>
<blockquote><p>"I think of it in slightly different terms. A government is for citizens and it must deliver a cycle of trust so that citizens can participate in government. There are three design values that are part of this: respect, participation, and unity. The important one is unity, because it brings citizens and government back on the same side."</p>
</blockquote>
<h2>What does open government mean to you?</h2>
<p>Even though advocates of open government define it differently, the mission is constant throughout: open government is about empowerment. So what does it mean to you? Share your thoughts about open government in the comments and let us know if being more open is making your government better. </p>
<h3>Update</h3>
<p class="title">This post has been translated: <em>Governo Aberto: o que isso significa para você?</em> at <a title="http://brodtec.com/diario/governo-aberto-o-que-isso-significa-para-voc" href="http://brodtec.com/diario/governo-aberto-o-que-isso-significa-para-voc" target="_blank">brodtec.com</a> and <a title="www.dicas-l.com.br" href="http://www.dicas-l.com.br/brod/brod_201212112229.php#.UMiLWuf9ThI" target="_blank">www.dicas-l.com.br</a>. Thank you <a title="Cesar Brod" href="https://opensource.com/users/cesar-brod" target="_self">Cesar Brod</a>.</p>
<p><span class="submitted"> </span></p>
