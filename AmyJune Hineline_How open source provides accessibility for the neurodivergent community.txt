<p>Not everyone uses the internet or digital assets in the same way. When some people think about accessibility, they only think of people with physical disabilities, which accounts for a portion of disabilities worldwide. According to the National Cancer Institute, 15-20% of people live with neurodivergence. This includes:</p>
<ul><li>Autism</li>
<li>Attention-deficit hyperactivity disorder (ADHD).</li>
<li>Dyslexia</li>
<li>Dyscalculia</li>
<li>Dysgraphia</li>
<li>Mental health conditions like bipolar disorder, obsessive-compulsive disorder, and more</li>
</ul><p>We published some great articles in 2022 about how to include folks who live with neurodiversity. Here are my top picks.</p>
<h2>Light and dark modes</h2>
<p>Light sensitivity is common in people who are autistic, as well as folks who just had their eyes dilated or have a migraine. There are two articles from 2022 that support the configurability of light and dark modes.</p>
<p>In Ayush Sharma's <a href="https://opensource.com/article/22/4/light-dark-mode-jekyll">A practical guide to light and dark mode in Jekyll</a>, you walk through how to tighten up HTML and then utilize CSS and Sass to create classes. Once you consolidate your styles, you can reuse them, define the new styles, and apply them to the HTML elements. Lastly, you can add a toggle so folks can easily switch modes.</p>
<p><a href="https://opensource.com/article/22/9/dark-theme-website">A beginner's guide to making a dark theme for a website</a> by Sachin Samal introduces a beginner-friendly way of programming a dark theme. Samal gives you practical code examples of how icons can be inserted and how to toggle between themes. It reminds you that you can utilize your browser inspector to play around with styling.</p>
<p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">Our favorite resources about open source</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-git?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Git cheat sheet">Git cheat sheet</a></div>
              <div class="field__item"><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Advanced Linux commands cheat sheet">Advanced Linux commands cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/alternatives?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Open source alternatives">Open source alternatives</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Free online course: RHEL technical overview">Free online course: RHEL technical overview</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Register for your free Red Hat account">Register for your free Red Hat account</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Check out more cheat sheets">Check out more cheat sheets</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<h2>Learning difficulties</h2>
<p>Amar Gandhi's <a href="https://opensource.com/article/22/4/open-source-tools-learning-difficulties">3 open source tools for people with learning difficulties</a> outlines a few tools he uses to customize how he views and uses open source tools. NextCloud can be used on any device and can serve as a <a href="https://opensource.com/article/22/10/pwa-web-browser">progressive web app (PWA)</a>. It also uses a dashboard so you can view everything at once. Photoprism can be used as a gallery and for storage. It is similar to NextCloud because you can use it as a PWA. This allows you to access any of your photos from any device. The last tool highlighted is the OpenDyslexic font which can help prevent confusion between letters (b and d, for example) through flipping and swapping.</p>
<p><a href="https://opensource.com/article/22/5/my-journey-c-neurodiverse-perspective">My open source journey with C from a neurodiverse perspective</a> is a memoir of Rikard Grossman-Nielsen's road to programming. As he moved from a student to a teacher, he realized that everyone learns to program differently. Addressing neurodivergent students, he recommends one way to learn C is to build games. But in the big scheme of how to learn, you should find what you're passionate about. You should also find what learning methods work for you. This will help you discover what works for you in the learning process.</p>
<h2>Open source for neurodiversity</h2>
<p>Open source software is an excellent resource for everyone including those on the neurodivergent spectrum. There are many programs available to help minimize the impact of symptoms that many people suffer from. If you're looking for a program that might help reduce your migraines or any other issue try looking into what open source software has to offer!</p>
