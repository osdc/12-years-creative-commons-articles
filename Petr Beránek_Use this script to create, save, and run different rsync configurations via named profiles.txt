<p id="rpf">The <strong>rpf</strong> script allows you to create, save, and run different rsync configurations via named profiles.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Programming and development</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ&amp;src=programming_resource_menu4">Red Hat Developers Blog</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Programming cheat sheets</a></li>
<li><a href="https://www.redhat.com/en/services/training/learning-subscription?intcmp=7016000000127cYAAQ">Try for free: Red Hat Learning Subscription</a></li>
<li><a href="https://opensource.com/downloads/bash-programming-guide?intcmp=7016000000127cYAAQ">eBook: An introduction to programming with Bash</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/bash-shell-cheat-sheet?intcmp=7016000000127cYAAQ">Bash Shell Scripting Cheat Sheet</a></li>
<li><a href="https://developers.redhat.com/e-books/modernizing-enterprise-java?intcmp=7016000000127cYAAQ">eBook: Modernizing Enterprise Java</a></li>
</ul></div>
</div>
</div>
</div>
<p>For example, create a new profile named <strong>backup</strong> by typing <strong>rpf -c backup</strong>. Assume that the username is <strong>user</strong>.</p>
<p><strong>rpf</strong> creates the following directories:</p>
<ul><li><strong>/home/user/.rpf</strong></li>
<li><strong>/home/user/.rpf/shared</strong> where you can place config files shared by multiple profiles</li>
<li><strong>/home/user/.rpf/profiles</strong> where all profiles are saved as subdirectories</li>
</ul><p><strong>rpf</strong> also created <strong>/home/user/.rpf/profiles/backup</strong> that contains the files <strong>conf</strong> and <strong>excluded</strong>.</p>
<p>The <strong>conf</strong> file defines rsync's configuration:</p>
<pre><code class="language-text"># rsync config template
#
# Write each rsync option on separate line. For option details see man rsync.
# Empty lines and lines starting with # are ignored. Dynamic references
# (e.g. using command substitution) are not supported.
#
# Config files shared between different profiles should be saved in
# /home/user/.rpf/shared
#
# Example configuration:
#
--verbose
--archive
--human-readable
# exclude all files that match pattern in:
--exclude-from=/home/user/.rpf/profiles/backup/exclude
--relative
# perform trial run, make no changes
--dry-run
# source, e.g.
/home/user
# destination, e.g.
/mnt/usb_drive/users_backup</code></pre><p>Now you can edit, add, or remove rsync options as needed.</p>
<p>In <strong>exclude</strong>, you can define paths or patterns of files and directories you want to exclude from the transfer. To exclude <strong>Trash</strong> and <strong>Downloads</strong>, add the following lines:</p>
<pre><code class="language-text">- /home/user/.local/share/Trash
- /home/user/Downloads</code></pre><p>Or to transfer only <strong>Documents</strong> and <strong>Projects</strong> and exclude everything else:</p>
<pre><code class="language-text">+ /home/user/Documents
+ /home/user/Projects
- **</code></pre><p>For subtler pattern configurations, see the FILTER RULES section of <strong>man rsync</strong>, or Google for tutorials.</p>
<p>When you are ready, you can start rsync transfer by typing <strong>rpf backup</strong>.</p>
<p>That's it.</p>
<p>For additional <strong>rpf</strong> options, see <strong>rpf --help</strong>.</p>
<h2>Security</h2>
<p>Please be aware that <strong>rpf</strong> is not secure against code injection in the <strong>conf</strong> file. Any additional code (e.g., <strong>; ./run_evil_script</strong>) will also be executed. Therefore, protect your <strong>.rpf/</strong> config directory from malicious users by implementing appropriate permissions. Moreover, exploiting this behavior can lead to unexpected side effects.</p>
<h2>The script</h2>
<pre><code class="language-text">#!/usr/bin/env bash
#
# Simple rsync profiler
#
# Author: petrberanek.mail@gmail.com (Petr Beranek)
#
# For usage details type `rpf --help'
#

set -o errexit
set -o nounset

__name=$(basename "${0}")
__version="0.1"

config_dir="${HOME}/.rpf"
profiles_dir="${config_dir}/profiles"
shared_dir="${config_dir}/shared"
help="\
Usage: ${__name} [OPTION...] PROFILE_NAME

${__name} is simple rsync profiler that stores your different rsync
configurations in named profiles.

Options:
    -c, --create-profile PROFILE_NAME   create new profile (profile data
                            are stored in ${config_dir}/PROFILE_NAME).
                            Profile name can contain alphanumeric
                            characters only.
    -s, --show-profile-config PROFILE_NAME  show content of profile
                            configuration file (stored in
                            ${config_dir}/PROFILE_NAME)
    -l, --list-profiles     list all available profiles
    -h, --help              show this help

Example:
    Create new profile by typing
    ${__name} -c PROFILE_NAME

    edit its config files stored by default in
    ${profiles_dir}/PROFILE_NAME

    and then run it by typing
    ${__name} PROFILE_NAME

    That's it.

${__name} comes with ABSOLUTELY NO WARRANTY. This is free software,
and you are welcome to redistribute it under certain conditions. See
the GNU General Public Licence for details.

Email bug reports or enhancement requests to petrberanek.mail@gmail.com.
"


create_profile() {
    # Create dir with given profile name and with default content.
    #
    # Arguments: $1 -- profile name
    #
    # Creates files: conf, exclude
    #
    # If dir with the same name already exists, exits with error.
    #

    local profile_name="${1}"
    local profile_dir="${profiles_dir}/${profile_name}"

    # create default rpf dirs if missing
    if [[ ! -d "${profiles_dir}" ]]; then
        echo "Creating ${profiles_dir}"
        mkdir --parents "${profiles_dir}"
    fi
    if [[ ! -d "${shared_dir}" ]]; then
        echo "Creating ${shared_dir}"
        mkdir --parents "${shared_dir}"
    fi

    # don't overwrite existing profile
    if [[ -d "${profile_dir}" ]]; then
        echo "${__name}: error: profile already exists."
        exit 1
    fi

    echo "Creating ${profile_dir}"
    mkdir "${profile_dir}"

    # create `conf' template
    local conf="${profile_dir}/conf"
    echo "Creating ${conf}"
    cat &lt;&lt; EOF &gt; "${conf}"
# rsync config template
#
# Write each rsync option on separate line. For details see man rsync.
# Empty lines and lines starting with # are ignored. Dynamic references
# (e.g. using command substitution) are not supported.
#
# Config files shared between different profiles should be saved in
# ${shared_dir}
#
# Example configuration:
#
--verbose
--archive
--human-readable
# file with patterns of files and directories in source excluded
# from transfer
--exclude-from="${profiles_dir}/${profile_name}/exclude"
--relative
# perform trial run, make no changes
--dry-run
# source, e.g.
${HOME}
# destination, e.g.
/mnt/usb_drive/my_backup
EOF

    # create `exclude' template
    local exclude="${profile_dir}/exclude"
    echo "Creating ${exclude}"
    cat &lt;&lt; EOF &gt; "${exclude}"
# \`exclude' template
#
# Lines starting with # or ; are ignored. For details see man rsync,
# section FILTER RULES.
#
EOF

    # all done
    echo "OK"
    echo "Edit profile config files in ${profile_dir} to fit your needs."
}


list_profiles() {
    # Show all available rpf profiles.
    #
    # Assumes that all dirs in $profiles_dir are profiles.
    #

    for item in "${profiles_dir}"/*; do
        if [[ -d "${item}" ]]; then
            basename "${item}"
        fi
    done
}


show_help() { echo "${help}"; }


show_profile_config() {
    # Show configuration file for given profile.
    #
    # Arguments: $1 -- profile name
    #

    local profile_name="${1}"
    less "${profiles_dir}/${profile_name}/conf"
}


check_profile_name() {
    # Check that name is not empty and contains alphanumeric chars only.
    #
    # Arguments: $1 -- profile name
    #
    # If test fails, exits with error.
    #

    if [[ -z "${1}" ]]; then
        echo "${__name}: error: empty profile name."
        exit 1
    elif [[ "${1}" =~ [^a-zA-Z0-9] ]]; then
        echo "${__name}: error: non-alphanumeric characters in profile name."
        exit 1
    fi
}


check_profile_exists() {
    # Check that $profile_name exists and is a directory.
    #
    # Arguments: $1 -- profile name
    #
    # If test fails, exits with error.
    #

    local profile_name="${1}"
    if [[ ! -d "${profiles_dir}/${profile_name}" ]]; then
        echo "${__name}: error: profile ${profile_name} does not exist."
        exit 1
    fi
}


check_num_args() {
    # Check that value of $1 = number of arguments (excluding $1)
    #
    # Arguments: $1 -- limit (positive int)
    #
    # If test fails, exits with error.
    #

    local num_args=$(( ${#} - 1 ))  # do not count $1 in total num of args
    if [[ "${1}" -ne "${num_args}" ]]; then
        echo "${__name}: error: expected num args: ${1}, received: $num_args"
        exit 1
    fi
}


run_rsync() {
    # Run rsync with configuration coresponding to given profile name.
    #
    # Arguments: $1 -- profile name
    #

    local profile_name="${1}"
    local visual_div="=============================="
    local parsed_args
    parsed_args=$(grep --invert-match '^#' "${profiles_dir}/${profile_name}/conf" \
                    | tr '\n' ' ')

    # Print debug info
    echo "${visual_div}"
    echo "${__name} version: ${__version}"
    echo "args: ${parsed_args}"
    echo "${visual_div}"

    # Expand $parsed_args - each item from conf file becomes rsync argument
    # shellcheck disable=SC2086
    rsync ${parsed_args}
}


if [[ "${#}" == 0 ]]; then
    show_help
    exit 1
fi
while [[ "${#}" -gt 0 ]]; do
    case "${1}" in
        -c | --create-profile)
            check_num_args 2 "${@}"
            shift
            check_profile_name "${1:-}" # If $1 is not declared, set it empty.
            create_profile "${1}"
            exit 0;;
        -s | --show-profile-config)
            check_num_args 2 "${@}"
            shift
            check_profile_name "${1:-}"
            check_profile_exists "${1}"
            show_profile_config "${1}"
            exit 0;;
        -l | --list-profiles)
            check_num_args 1 "${@}"
            list_profiles
            exit 0;;
        -h | --help)
            check_num_args 1 "${@}"
            show_help
            exit 0;;
        -*)
            echo "${__name}: error: unknown option \`${1}'"
            exit 1;;
        *)
            check_num_args 1 "${@}"
            check_profile_name "${1:-}"
            check_profile_exists "${1}"
            run_rsync "${1}"
            exit 0;;
    esac
    shift
done</code></pre><hr /><p>This was originally published in <a href="https://github.com/beranep/rpf" target="_blank">Petr Beranek's GitHub repository</a> and it is licensed under GPLv3.</p>
