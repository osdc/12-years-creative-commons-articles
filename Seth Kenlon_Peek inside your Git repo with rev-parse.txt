<p>I use Git a lot. In fact, there's probably an argument that I sometimes misuse it. I use Git to power a flat-file CMS, a <a href="https://opensource.com/life/16/8/how-construct-your-own-git-server-part-6">website</a>, and even my <a href="https://opensource.com/article/19/4/calendar-git">personal calendar</a>.</p>
<p>To misuse Git, I write a lot of Git hooks. One of my favorite Git subcommands is <code>rev-parse</code>, because when you're scripting with Git, you need information <i>about</i> your Git repository just as often as you need information <i>from</i> it.</p>
<p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">More on Git</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://opensource.com/resources/what-is-git?intcmp=7013a000002CxqLAAS" data-analytics-category="resource list" data-analytics-text="What is Git?">What is Git?</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-git?intcmp=7013a000002CxqLAAS" data-analytics-category="resource list" data-analytics-text="Git cheat sheet">Git cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-markdown?intcmp=7013a000002CxqLAAS" data-analytics-category="resource list" data-analytics-text="Markdown cheat sheet">Markdown cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/git-tricks-tips?intcmp=7013a000002CxqLAAS" data-analytics-category="resource list" data-analytics-text="eBook: Git tips and tricks">eBook: Git tips and tricks</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Register for your free Red Hat account">Register for your free Red Hat account</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/git?intcmp=7013a000002CxqLAAS" data-analytics-category="resource list" data-analytics-text="New Git articles">New Git articles</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<h2>Getting the top-level directory</h2>
<p>For Git, there are no directories farther back than its own top-level folder. That's in part what makes it possible to move a Git directory from, say, your computer to a thumb drive or a server with no loss of functionality.</p>
<p>Git is only aware of the directory containing a hidden <code>.git</code> directory and any tracked folders below that. The <code>--show-toplevel</code> option displays the root directory of your current Git repository. This is the place where it all starts, at least for Git.</p>
<p>Here's an obvious example of how you might use it:</p>
<pre>
<code class="language-bash">$ cd ~/example.git
$ git rev-parse --show-toplevel
/home/seth/example.git</code></pre><p>It becomes more useful when you're farther in your Git repo. No matter where you roam within a repo, <code>rev-parse --show-toplevel</code> always knows your root directory:</p>
<pre>
<code class="language-bash">$ cd ~/example.git/foo/bar/baz
$ git rev-parse --show-toplevel
/home/seth/example.git</code></pre><p>In a similar way, you can get a pointer to what makes that directory the top level: the hidden <code>.git</code> folder.</p>
<pre>
<code class="language-bash">$ git rev-parse --git-dir
/home/seth/example.com/.git</code></pre><h2>Find your way home</h2>
<p>The <code>--show-cdup</code> option tells you (or your script, more likely) exactly how to get to the top-level directory from your current working directory. It's a lot easier than trying to reverse engineer the output of <code>--show-toplevel</code>, and it's more portable than hoping a shell has <code>pushd</code> and <code>popd</code>.</p>
<pre>
<code class="language-bash">$ git rev-parse --show-cdup
../../..</code></pre><p>Interestingly, you can lie to <code>--show-cdup</code>, if you want to. Use the <code>--prefix</code> option to fake the directory you're making your inquiry from:</p>
<pre>
<code class="language-bash">$ cd ~/example.git/foo/bar/baz
$ git rev-parse --prefix /home/seth/example.git/foo --show-cdup
../</code></pre><h2>Current location</h2>
<p>Should you need confirmation of where a command is being executed from, you can use the <code>--is-inside-work-tree</code> and <code>--is-inside-git-dir</code> options. These return a Boolean value based on the current working directory:</p>
<pre>
<code class="language-bash">$ pwd
.git/hooks
$ git rev-parse --is-inside-git-dir
true

$ git rev-parse --is-inside-work-tree
false</code></pre><h2>Git scripts</h2>
<p>The <code>rev-parse</code> subcommand is utilitarian. It's not something most people are likely to need every day. However, if you write a lot of Git hooks or use Git heavily in scripts, it may be the Git subcommand you always wanted without knowing you wanted it.</p>
<p>Try it out the next time you invoke Git in a script.</p>
