<p>It's HOT! I suppose I can't complain too much about living in paradise, but when my wife and I moved to Hawaii last fall, I didn't really think too much about the weather. Don't get me wrong, the weather is lovely pretty much all the time, and we keep our windows open 24/7, but that means it is pretty warm in the house right now in the middle of summer.</p>
<p>So, where does all this humble bragging intersect with open source? Well, we're planning to get a whole-house fan—one of those big ones that suck all the air out of your house and force it into the attic, pushing all the hot air out of the attic in the process. I am <em>sure</em> this will make the house way cooler, but the geek in me wants to know just how much cooler.</p>
<p>So today, I'm playing with temperature sensors, <a href="https://www.raspberrypi.org/" target="_blank">Raspberry Pis</a>, and <a href="https://www.python.org/" target="_blank">Python</a>.</p>
<p>Play along at home! Nothing like a little #CitizenScience!</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="DHT22 sensor and Raspberry Pi Zero W"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/dht22.png" width="675" height="360" alt="DHT22 sensor and Raspberry Pi Zero W" title="DHT22 sensor and Raspberry Pi Zero W" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup> Charming little development environment, isn't it? (Chris Collins, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>Yes, OK, I could just buy a thermometer or two, check them each day, and see what happens. But why do that when you can totally overengineer a solution, automate the data collection, and graph it all over time, amirite?</p>
<p>Here's what I need:</p>
<ul><li>Raspberry Pi Zero W (or, really, any Raspberry Pi)</li>
<li>DHT22 digital sensor</li>
<li>SD card</li>
</ul><h2 id="connect-the-dht22-sensor-to-the-raspberry-pi">Connect the DHT22 sensor to the Raspberry Pi</h2>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on Raspberry Pi</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/resources/what-raspberry-pi?src=raspberry_pi_resource_menu1&amp;intcmp=701f2000000h4RcAAI">What is Raspberry Pi?</a></li>
<li><a href="https://opensource.com/downloads/raspberry-pi-guide?src=raspberry_pi_resource_menu2&amp;intcmp=701f2000000h4RcAAI">eBook: Guide to Raspberry Pi</a></li>
<li><a href="https://opensource.com/downloads/getting-started-raspberry-pi-cheat-sheet?src=raspberry_pi_resource_menu3&amp;intcmp=701f2000000h4RcAAI">Getting started with Raspberry Pi cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/kubernetes-raspberry-pi?src=raspberry_pi_resource_menu6&amp;intcmp=701f2000000h4RcAAI">eBook: Running Kubernetes on your Raspberry Pi</a></li>
<li><a href="https://www.redhat.com/en/resources/data-intensive-applications-hybrid-cloud-blueprint-detail?src=raspberry_pi_resource_menu7&amp;intcmp=701f2000000h4RcAAI">Whitepaper: Data-intensive intelligent applications in a hybrid cloud blueprint</a></li>
<li><a href="https://www.redhat.com/en/topics/edge-computing?src=raspberry_pi_resource_menu5&amp;intcmp=701f2000000h4RcAAI">Understanding edge computing</a></li>
<li><a href="https://opensource.com/tags/raspberry-pi?src=raspberry_pi_resource_menu4&amp;intcmp=701f2000000h4RcAAI">Our latest on Raspberry Pi </a></li>
</ul></div>
</div>
</div>
</div>
<p>You can find a bunch of inexpensive DHT22 temperature and humidity sensors with a quick web search. The DHT22 is a digital sensor, making it easy to interact with. If you purchase a raw sensor, you'll need a resistor and some soldering skills to get it working (check out Pi My Life Up's DHT22 article for <a href="https://pimylifeup.com/raspberry-pi-humidity-sensor-dht22/" target="_blank">great instructions on working with the raw sensor</a>), but you can also purchase one with a small PCB that includes all that, which is what I did.</p>
<p>The DHT22 with the PCB attached has three pins: a Positive pin (marked with <strong>+</strong>), a Data pin, and a Ground pin (marked with <strong>-</strong>). You can wire the DHT22 directly to the Raspberry Pi Zero W. I used Raspberry Pi Spy's <a href="https://www.raspberrypi-spy.co.uk/2012/06/simple-guide-to-the-rpi-gpio-header-and-pins/" target="_blank">Raspberry Pi GPIO guide</a> to make sure I connected everything correctly.</p>
<p>The Positive pin provides power from the Pi to the DHT22. The DHT22 runs on 3v-6v, so I selected one of the 5v pins on the Raspberry Pi to provide the power. I connected the Data pin on the DHT22 to one of the Raspberry Pi GPIO pins. I am using GPIO4 for this, but any would work; just make a note of the one you choose, as the Python code that reads the data from the sensor will need to know which pin to read from. Finally, I connected the Ground pin on the DHT22 to a ground pin on the Raspberry Pi header.</p>
<p>This is how I wired it up:</p>
<ul><li>DHT22 Positive pin &lt;-&gt; Raspberry Pi GPIO v5 pin (#2)</li>
<li>DHT22 Data pin &lt;-&gt; Raspberry Pi GPIO4 pin (#7)</li>
<li>DHT22 Ground pin &lt;-&gt; Raspberry Pi Group pin (#6)</li>
</ul><p>This diagram from Raspberry Pi Spy shows the pin layout for the Raspberry Pi Zero W (among others).</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Raspberry Pi GPIO header diagram"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/raspberry_pi_gpio_layout_model_b_plus.png" width="675" height="225" alt="Raspberry Pi GPIO header diagram" title="Raspberry Pi GPIO header diagram" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Copyright © 2021 <a href="https://www.raspberrypi-spy.co.uk/2012/06/simple-guide-to-the-rpi-gpio-header-and-pins/" target="_blank" rel="ugc">Matt Hawkins</a>)</sup></p>
</div>
      
  </article></p>
<h2 id="install-the-dht-sensor-software">Install the DHT sensor software</h2>
<p>Before proceeding, make sure you have an operating system installed on the Raspberry Pi Zero W and can connect to it remotely or with a keyboard. If not, consult my article about <a href="https://opensource.com/article/20/5/disk-image-raspberry-pi">customizing different operating system images</a> for Raspberry Pi. I am using <a href="https://www.raspberrypi.org/software/operating-systems/" target="_blank">Raspberry Pi OS Lite</a>, released May 7, 2021, as the image for the Raspberry Pi Zero W.</p>
<p>Once you've installed the operating system on an SD card and booted the Raspberry Pi from the card, there are only a couple of other software packages to install to interact with the DHT22.</p>
<p>First, install the Python Preferred Installer Program (pip) with <code>apt-get</code>, and then use pip to install the <a href="https://github.com/adafruit/Adafruit_Python_DHT" target="_blank">Adafruit DHT sensor library for Python</a> to interact with the DHT22 sensor.</p>
<pre><code class="language-python"># Install pip3
sudo apt-get install python3-pip

# Install the Adafruit DHT sensor library
sudo pip3 install Adafruit_DHT</code></pre><h2 id="get-sensor-data-with-python">Get sensor data with Python</h2>
<p>With the DHT libraries installed, you can connect to the sensor and retrieve temperature and humidity data.</p>
<p>Create a file with:</p>
<pre><code class="language-python">#!/usr/bin/env python3

import sys
import argparse
import time

# This imports the Adafruit DHT software installed via pip
import Adafruit_DHT

# Initialize the DHT22 sensor
SENSOR = Adafruit_DHT.DHT22

# GPIO4 on the Raspberry Pi
SENSOR_PIN = 4

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--fahrenheit", help="output temperature in Fahrenheit", action="https://opensource.com/store_true")

    return parser.parse_args()

def celsius_to_fahrenheit(degrees_celsius):
        return (degrees_celsius * 9/5) + 32

def main():
    args = parse_args()

    while True:
        try:
            # Gather the humidity and temperature
            # data from the sensor; GPIO Pin 4
            humidity, temperature = Adafruit_DHT.read_retry(SENSOR, SENSOR_PIN)

        except RuntimeError as e:
            # GPIO access may require sudo permissions
            # Other RuntimeError exceptions may occur, but
            # are common.  Just try again.
            print(f"RuntimeError: {e}")
            print("GPIO Access may need sudo permissions.")

            time.sleep(2.0)
            continue

        if args.fahrenheit:
            print("Temp: {0:0.1f}*F, Humidity: {1:0.1f}%".format(celsius_to_fahrenheit(temperature), humidity))
        else:
            print("Temp:{0:0.1f}*C, Humidity: {1:0.1f}%".format(temperature, humidity))

        time.sleep(2.0)

if __name__ == "__main__":
    main()</code></pre><p>The important bits here are initializing the sensor and setting the correct GPIO pin to use on the Raspberry Pi:</p>
<pre><code class="language-python"># Initialize the DHT22 sensor
SENSOR = Adafruit_DHT.DHT22

# GPIO4 on the Raspberry Pi
SENSOR_PIN = 4</code></pre><p>Another important bit is reading the data from the sensor with the variables set above for the sensor and pin:</p>
<pre><code class="language-python"># This connects to the sensor "SENSOR"
# Using the Raspberry Pi GPIO Pin 4, "SENSOR_PIN"
    humidity, temperature = Adafruit_DHT.read_retry(SENSOR, SENSOR_PIN)</code></pre><p>Finally, run the script! You should end up with something like this:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Output of the sensor script"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/temperature_sensor.png" width="373" height="210" alt="Output of the sensor script" title="Output of the sensor script" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>84 degrees and 50% humidity. Oof! Hot and humid in here! (Chris Collins, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>Success!</p>
<h2 id="where-to-go-from-here">Where to go from here</h2>
<p>I have three of these DHT22 sensors and three Raspberry Pi Zero Ws connected to my WiFi. I've installed them into some small project boxes, hot-glued the sensors to the outside, and set them up in my living room, office, and bedroom. With this setup, I can collect sensor data from them whenever I want by SSHing into the Raspberry Pi and running this script.</p>
<p>But why stop there? Manually SSHing into them each time is tedious and too much work. I can do better!</p>
<p>In a future article, I'll explain how to set up this script to run automatically at startup with a <a href="https://www.freedesktop.org/software/systemd/man/systemd.service.html" target="_blank">systemd service</a>, set up a web server to display the data, and instrument this script to export data in a format that can be read by <a href="https://prometheus.io/" target="_blank">Prometheus</a>, a monitoring system and time series database. I use Prometheus at work to collect data about OpenShift/Kubernetes clusters, plot trends, and create alerts based on the data. Why not go totally overboard and do the same thing with temperature and humidity data at home? This way, I can get baseline data and then see how well the whole-house fan changes things!</p>
<p>#CitizenScience!</p>
