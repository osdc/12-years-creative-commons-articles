<p>Container platforms and <a href="https://opensource.com/article/17/9/what-edge-computing">edge computing</a> continue to grow, powering major networks and applications across the globe, and Java technologies have evolved new features and improved performance to match steps with modern infrastructure. <a href="https://openjdk.java.net/projects/jdk/17/" target="_blank">Java 17</a> (OpenJDK 17) was released recently (September 2021) with the following major features:</p>
<ul><li><a href="https://openjdk.java.net/jeps/306" target="_blank">Restore Always-Strict Floating-Point Semantics</a></li>
<li><a href="https://openjdk.java.net/jeps/356" target="_blank">Enhanced Pseudo-Random Number Generators</a></li>
<li><a href="https://openjdk.java.net/jeps/403" target="_blank">Strongly Encapsulate JDK Internals</a></li>
<li><a href="https://openjdk.java.net/jeps/406" target="_blank">Pattern Matching for switch (Preview)</a></li>
<li><a href="https://openjdk.java.net/jeps/412" target="_blank">Foreign Function &amp; Memory API (Incubator)</a></li>
<li><a href="https://openjdk.java.net/jeps/414" target="_blank">Vector API (Second Incubator)</a></li>
<li><a href="https://openjdk.java.net/jeps/415" target="_blank">Context-Specific Deserialization Filters</a></li>
</ul><p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on Java</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/topics/enterprise-java/?intcmp=7013a000002Cxq6AAC">What is enterprise Java programming?</a></li>
<li><a href="https://developers.redhat.com/products/openjdk/overview?intcmp=7013a000002Cxq6AAC">Red Hat build of OpenJDK</a></li>
<li><a href="https://opensource.com/downloads/java-cheat-sheet?intcmp=7013a000002Cxq6AAC">Java cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/do092-developing-cloud-native-applications-microservices-architectures?intcmp=7013a000002Cxq6AAC">Free online course: Developing cloud-native applications with microservices architectures</a></li>
<li><a href="https://opensource.com/tags/java?intcmp=7013a000002Cxq6AAC">Fresh Java articles</a></li>
</ul></div>
</div>
</div>
</div>

<p>Developers are wondering how to start implementing application logic using the new features of Java 17 and then build and run them on the same OpenJDK 17 runtime. Luckily, <a href="https://opensource.com/article/21/8/java-quarkus-ebook" target="_self">Quarkus</a> enables the developers to scaffold a new application with Java 17. It also provides a <a href="https://quarkus.io/vision/developer-joy#live-coding" target="_blank">live coding</a> capability that allows developers to focus only on implementing business logic instead of compiling, building, deploying, and restarting runtimes to apply code changes.</p>
<p><strong>Note</strong>: If you haven’t already installed OpenJDK 17, <a href="https://jdk.java.net/17/" target="_blank">download a binary</a> on your operating system.</p>
<p>This tutorial teaches you to use the Pseudo-Random Number Generators (PRNG) algorithms of Java 17 on Quarkus. Get started by scaffolding a new project using the <a href="https://quarkus.io/guides/cli-tooling" target="_blank">Quarkus command-line tool</a> (CLI):</p>
<pre><code class="language-bash">$ quarkus create app prng-example --java=17</code></pre><p>The output looks like this:</p>
<pre><code class="language-java">...
[SUCCESS] ✅  quarkus project has been successfully generated in:
--&gt; /Users/danieloh/quarkus-demo/prng-example
...</code></pre><p>Unlike traditional Java frameworks, Quarkus provides live coding features for developers to rebuild and deploy while the code changes. In the end, this capability accelerates inner loop development for Java developers. Run your Quarkus application using Dev mode:</p>
<pre><code class="language-bash">$ cd prng-example
$ quarkus dev</code></pre><p>The output looks like this:</p>
<pre><code class="language-java">...
INFO  [io.quarkus] (Quarkus Main Thread) Profile dev activated. Live Coding activated.
INFO  [io.quarkus] (Quarkus Main Thread) Installed features: [cdi, resteasy, smallrye-context-propagation, vertx]

--
Tests paused
Press [r] to resume testing, [o] Toggle test output, [:] for the terminal, [h] for more options&gt;</code></pre><p>Java 17 enables developers to generate random integers within a specific range based on the <strong>Xoshiro256PlusPlus</strong> PRNG algorithm. Add the following code to the <code>hello()</code> method in the <code>src/main/java/org/acme</code> directory:</p>
<pre><code class="language-java">RandomGenerator randomGenerator = RandomGeneratorFactory.of("Xoshiro256PlusPlus").create(999);

for ( int i = 0; i &lt; 10 ; i++) {
    int result = randomGenerator.nextInt(11);
    System.out.println(result);
}</code></pre><p>Next, invoke the RESTful API (<code>/hello</code>) to confirm that random integers are generated. Execute the following cURL command line in your local terminal or access the endpoint URL using a web browser:</p>
<pre><code class="language-bash">$ curl localhost:8080/hello</code></pre><p>Go back to the terminal where you’re running Quarkus Dev mode. There you’ll see the following ten random numbers:</p>
<pre><code class="language-bash">4
6
9
5
7
6
5
0
6
10</code></pre><p><strong>Note</strong>: You don’t need to rebuild code and restart the Java runtime at all. You’ll also see the output Hello RESTEasy in the terminal where you ran the <code>curl</code> command line.</p>
<h2>Wrap up</h2>
<p>This article shows how Quarkus allows developers to start a new application development based on OpenJDK 17. Furthermore, Quarkus increases developers’ productivity by live coding. For a production deployment, developers can make a <a href="https://quarkus.io/guides/building-native-image" target="_blank">native executable</a> based on OpenJDK 17 and <a href="https://www.graalvm.org/" target="_blank">GraalVM</a>.</p>
