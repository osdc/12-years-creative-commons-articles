<p>The <a href="https://www.owfgoss.org/" target="_blank">Ozone Widget Framework</a> went open source recently.</p>
<p>Ozone is:</p>
<blockquote><p>A customizable open-source web application that assembles the tools you need to accomplish any task and enables those tools to communicate with each other.</p>
</blockquote>
<p>As any well-behaved open source project, its code is available in a <a title="Ozone on Github" href="https://github.com/ozoneplatform/owf" target="_blank">public repository</a>. What is quite interesting, when one takes a look at the repository, is the following note in its README file:</p>
<blockquote><p>The Ozone Widget Framework is released to the public as Open Source Software, because it's the Right Thing To Do. Also, it was required by <a title="Section 924 of the 2012 National Defense Authorization Act" href="http://www.gpo.gov/fdsys/pkg/PLAW-112publ81/pdf/PLAW-112publ81.pdf" target="_blank">Section 924 of the 2012 National Defense Authorization Act</a>.</p>
</blockquote>
<p>There are two pieces to this statement</p>
<ul><li>Open source is the "Right Thing to Do"</li>
<li>A law passed by Congress <span style="text-decoration: underline;">mandated</span> that the project be made open source</li>
</ul><p>It is rare to find legislation that includes language about software, and more specifically about software development models, much less about requiring open source development.</p>
<p>The text of the Act is quite interesting in <a title="Sec. 924" href="http://www.gpo.gov/fdsys/pkg/PLAW-112publ81/pdf/PLAW-112publ81.pdf" target="_blank">Sec. 924</a>, page 243:</p>
<blockquote><p>(a) MECHANISM FOR INTERNET PUBLICATION OF INFORMATION FOR DEVELOPMENT OF ANALYSIS TOOLS AND APPLICATIONS.</p>
<p>—The Chief Information Officer of the Department of Defense, acting through the Director of the Defense Information Systems Agency, shall implement a mechanism to publish and maintain on the public Internet the application programming interface specifications, a developer’s toolkit, source code, and such other information on, and resources for, the Ozone Widget Framework (OWF) as the Chief Information Officer considers necessary to permit individuals and companies to develop, integrate, and test analysis tools and applications for use by the Department of Defense and the elements of the intelligence community.</p>
</blockquote>
<p>Yes, one must read this twice, to realize that this is the U.S. Congress requiring the <a title="CIO of the Department of Defense" href="http://dodcio.defense.gov/" target="_blank">CIO of the U.S. Department of Defense</a> (DOD) to post a specific software project openly on the web. Note that the language of Section 924 doesn't actually use the words "open source," but it would be nearly impossible to comply otherwise.</p>
<p>In some ways, it's no surprise to see the Department of Defense spearheading this effort. After all, the DOD was one of the early adopters of open source within the U.S. Federal government, releasing <a title="DOD open source rules" href="http://gcn.com/articles/2009/10/28/%7E/media/GIG/GCN/Documents/DOD_opensourcerules.ashx" target="_blank">guidance</a> in 2009 that paved the way for open source software procurement within that agency and eventually others.  With the Ozone Widget Framework,<span style="letter-spacing: 0px;"> U.S. Congress and the Department of Defense recognize that open source is not just valuable commerical software to use, but is also an effective methodology to develop and maintain software used to support mission-critical applications. <br /></span></p>
<p><span style="letter-spacing: 0px;">Following in the footsteps of <a href="https://github.com/whitehouse">Whitehouse.gov</a>, Congress' mandate to release OWF code to the community demonstrates this</span> deeper understanding and appreciation by the U.S. government for an open development environment for software:</p>
<blockquote><p>(b) PROCESS FOR VOLUNTARY CONTRIBUTION OF IMPROVEMENTS BY PRIVATE SECTOR.</p>
<p>—In addition to the requirement under sub-section (a), the Chief Information Officer shall also establish a process by which private individuals and companies may voluntarily contribute the following:</p>
<p><span style="letter-spacing: 0px;">(1) Improvements to the source code and documentation for the Ozone Widget Framework.</span></p>
<p><span style="letter-spacing: 0px;">(2) Alternative or compatible implementations of the published application programming interface specifications for the Framework.</span></p>
</blockquote>
<p>Clear proof that the drafters of the Act understood that open source is much more that just posting software publicly online. In particular, that open development and community involvement are essential ingredients of the open source recipe.</p>
<p>Finally, the Act also calls for active outreach and promotion of the software:</p>
<blockquote><p>(c) ENCOURAGEMENT OF USE AND DEVELOPMENT.</p>
<p>—The Chief Information Officer shall, whenever practicable, encourage and foster the use, support, development, and enhancement of the Ozone Widget Framework by the computer industry and commercial information technology vendors, including the development of tools that are compatible with the Framework.</p>
</blockquote>
<p>Congress is actually reminding us of a good lesson, that is <span style="letter-spacing: 0px;">unfortunately needed for many open source projects:</span></p>
<ul><li>We shall go out there and promote our projects.</li>
<li>We shall put effort on bringing new contributors to our communities.</li>
<li>We shall play nice with other projects and pursue interoperability.</li>
</ul><p>With over <a title="Ohloh languages" href="http://www.ohloh.net/p/owf/analyses/latest/languages_summary" target="_blank">1 million lines of code</a> and an <a title="Apache 2.0 License" href="https://github.com/ozoneplatform/owf/blob/master/LICENSE.txt" target="_blank">Apache 2.0 License</a>, Ozone already has <a title="Ozone on Github" href="https://github.com/ozoneplatform/owf/network" target="_blank">29 forks</a> in Github. It is becoming a great example on how government agencies can finally take advantage of the many benefits that open source has to offer, to better serve the public.</p>
