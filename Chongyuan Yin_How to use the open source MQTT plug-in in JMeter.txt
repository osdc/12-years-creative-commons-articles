<p>In a <a href="https://opensource.com/article/22/10/iot-test-jmeter" target="_blank">previous article</a>, I described how JMeter has built-in support for HTTP, HTTPS, TCP, and other common protocols and has a plug-in extension mechanism.</p>
<p>Through plug-ins, you can support much more than just what's built-in, including MQTT.</p>
<p><a href="https://www.emqx.com/en/mqtt" target="_blank">MQTT</a> is a mainstream protocol in the IoT world. Although it is not a protocol type that comes with JMeter, it is extremely common in IoT testing scenarios. In order to support the load testing of the MQTT protocol, EMQ developed a JMeter-based open source testing plug-in for the MQTT protocol.</p>
<p>This article introduces how to use the MQTT plug-in in JMeter.</p>
<h2>Install the MQTT plug-in on JMeter</h2>
<p>The installation of the MQTT plug-in is similar to other JMeter third-party plug-ins:</p>
<ol><li>Download the latest version of the plug-in <code>mqtt-xmeter-2.0.2-jar-with-dependencies.jar</code> from <a href="https://github.com/xmeter-net/mqtt-jmeter/releases/download/v2.0.2/mqtt-xmeter-2.0.2-jar-with-dependencies.jar" target="_blank">GitHub</a>. The plug-in supports JMeter 3.2 and above.</li>
<li>Copy the plug-in jar package to the plug-in directory of JMeter: <code>$JMETER_HOME/lib/ext</code>.</li>
<li>Restart JMeter.</li>
</ol><p>At the time of writing, the JMeter MQTT plug-in supports a variety of samplers, such as connection, message publish, and message subscription.</p>
<p>These can be combined to build more complex test scenarios.</p>
<h2>MQTT connect sampler</h2>
<p>The Connect Sampler simulates an IoT device and initiates an MQTT connection.</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2023-01/MQTT-connect-sampler.png" width="1520" height="700" alt="MQTT connect sampler interface" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Chongyuan Yin, CC BY-SA 4.0)</p>
</div>
      
  </article><p><strong>Server name or IP:</strong> The address of the MQTT server being tested.</p>
<p><strong>Port number:</strong> Taking the EMQX Broker as an example, the default ports are 1883 for TCP connections and 8883 for SSL connections. Please refer to the specific configuration of the server for the specific port.</p>
<p><strong>MQTT version:</strong> Presently supports MQTT 3.1 and 3.1.1 versions.</p>
<p><strong>Timeout:</strong> Connection timeout setting, in seconds.</p>
<p><strong>Protocols:</strong> Supports TCP, SSL, WS, and WSS connections to MQTT servers. When selecting an SSL or WSS encrypted channel connection, a one-way or two-way authentication (Dual) can be selected. If two-way authentication is required, specify the appropriate client certificate (p12 certificate) and the corresponding file protection password (Secret).</p>
<p><strong>User authentication:</strong> If the MQTT server is configured for user authentication, provide the corresponding Username and Password.</p>
<p><strong>ClientId:</strong> The identity of the virtual user. If <strong>Add random suffix for ClientId</strong> is enabled, a UUID string is added as a suffix to each ClientId and the whole virtual user identifier.</p>
<p><strong>Keep alive:</strong> The interval for sending heartbeat signals. For example, 300 means that the client sends ping requests to the server every 300 seconds to keep the connection active.</p>
<p><strong>Connect attempt max:</strong> The maximum number of reconnection attempts during the first connection. If this number is exceeded, the connection is considered failed. If the user wants to keep trying to reconnect, set this to -1.</p>
<p><strong>Reconnect attempt max:</strong> The maximum number of reconnect attempts during subsequent connections. If this number is exceeded, the connection is considered failed. If the user wants to keep trying to reconnect, set this to -1.</p>
<p><strong>Clean session:</strong> Set this option to false when the user wants to keep the session state between connections or true when the user does not want to keep the session state in new connections.</p>
<h2>MQTT message publish sampler (MQTT Pub Sampler)</h2>
<p>The message publish sampler reuses the MQTT connection established in the Connection Sampler to publish messages to the target MQTT server.</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2023-01/MQTT-pub-sampler.png" width="1520" height="644" alt="MQTT pub sampler interface" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Chongyuan Yin, CC BY-SA 4.0)</p>
</div>
      
  </article><p><strong>QoS Level:</strong> Quality of Service, with values 0, 1, and 2, representing <code>AT_MOST_ONCE</code>, <code>AT_LEAST_ONCE</code>, and <code>EXACTLY_ONCE</code>, respectively, in the MQTT protocol specification.</p>
<p><strong>Retained messages:</strong> If you want to use retained messages, set this option to true to have the MQTT server store the retained messages published by the plug-in using the given QoS. When the subscription occurs on the corresponding topic, the last retained message is delivered directly to the subscriber. Therefore, the subscriber does not have to wait to get the latest status value of the publisher.</p>
<p><strong>Topic name:</strong> The topic of the published message.</p>
<p><strong>Add timestamp in payload:</strong> If enabled, the current timestamp is attached to the beginning of the published message body. Together with the <strong>Payload includes timestamp</strong> option of the message subscription sampler, this can calculate the delay time reached by the message at the message receiving end. If disabled, only the actual message body is sent.</p>
<p><strong>Payloads</strong> and <strong>Message type:</strong> Three message types are presently supported:</p>
<ul><li>String: Ordinary string.</li>
<li>Hex String: A string is presented as a hexadecimal value, such as Hello, which can be represented as 48656C6C6F (where 48 corresponds to the letter H in the ASCII table, and so on). Typically, hexadecimal strings are used to construct non-textual message bodies, such as describing certain private protocol interactions, control information, and so on.</li>
<li>Random string with a fixed length: A random string of a specified length (in bytes) is generated as a message body.</li>
</ul><h2>MQTT message subscription sampler (MQTT Sub Sampler)</h2>
<p>The Message Pub Sampler reuses the MQTT connection established in the Connection Sampler to subscribe to messages from the target MQTT server.</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2023-01/MQTT-sub-sampler.png" width="1520" height="403" alt="MQTT sub sampler interface" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Chongyuan Yin, CC BY-SA 4.0)</p>
</div>
      
  </article><p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">More on edge computing</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://www.redhat.com/en/topics/edge-computing?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Understanding edge computing">Understanding edge computing</a></div>
              <div class="field__item"><a href="https://opensource.com/article/21/2/linux-edge-computing?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Why Linux is critical to edge computing">Why Linux is critical to edge computing</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/kubernetes-raspberry-pi?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="eBook: Running Kubernetes on your Raspberry Pi">eBook: Running Kubernetes on your Raspberry Pi</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/engage/automated-enterprise-ebook-20171115?intcmp=701f2000000h4RcAAI" data-analytics-category="resource list" data-analytics-text="Download now: The automated enterprise eBook">Download now: The automated enterprise eBook</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/home-automation-ebook?intcmp=701f2000000h4RcAAI" data-analytics-category="resource list" data-analytics-text="eBook: A practical guide to home automation using open source tools">eBook: A practical guide to home automation using open source tools</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/engage/automation-at-edge-20220727?intcmp=701f2000000h4RcAAI" data-analytics-category="resource list" data-analytics-text="eBook: 7 examples of automation on the edge">eBook: 7 examples of automation on the edge</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/topics/edge-computing/what-is-edge-machine-learning?intcmp=701f2000000h4RcAAI" data-analytics-category="resource list" data-analytics-text="What is edge machine learning?">What is edge machine learning?</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text=" Register for your free Red Hat account"> Register for your free Red Hat account</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/edge-computing?intcmp=701f2000000h4RcAAI" data-analytics-category="resource list" data-analytics-text="The latest on edge">The latest on edge</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<p><strong>QoS Level:</strong> Quality of Service, the meaning is the same as that for the Message Pub Sampler.</p>
<p><strong>Topic name:</strong> The topic to which the subscribed message belongs. A single message subscription sampler can subscribe to multiple topics, separated by commas.</p>
<p><strong>Payload includes timestamp:</strong> If enabled, the timestamp is parsed from the beginning of the message body, which can be used to calculate the receive delay of the message with the <strong>Add timestamp</strong> in the payload option of the message delivery sampler. If disabled, only the actual message body is parsed.</p>
<p><strong>Sample on:</strong> For the sampling method, the default is <strong>specified elapsed time (ms)</strong>, such as sampling every millisecond. The <strong>number of received messages</strong> can also be selected, such as sampling once for every specified number of messages received.</p>
<p><strong>Debug response:</strong> If checked, the message content is printed in the JMeter response. This option is mainly used for debugging purposes. It isn't recommended to run the test formally when checked in order to avoid affecting the test efficiency.</p>
<h2>MQTT disconnect sampler (MQTT DisConnect)</h2>
<p>Disconnects the MQTT connection established in the connection sampler.</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2023-01/MQTTdisconnect.png" width="1520" height="176" alt="MQTT DisConnect interface" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Chongyuan Yin, CC BY-SA 4.0)</p>
</div>
      
  </article><p>For flexibility, the property values in the above sampler can refer to JMeter's system or custom variables.</p>
<h2>MQTT and JMeter</h2>
<p>In this article, I've introduced the various test components of the JMeter MQTT plug-in. In another article, I'll discuss in detail how to build test scripts with the MQTT plug-in for different test scenarios.</p>
<hr /><p><em>This article originally appeared on <a href="https://www.emqx.com/en/blog/how-to-use-the-mqtt-plugin-in-jmeter" target="_blank">How to Use the MQTT Plug-in in JMeter</a> and is republished with permission.</em></p>
