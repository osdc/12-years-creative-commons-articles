<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>The Linux Terminal</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/life/17/10/top-terminal-emulators?intcmp=7016000000127cYAAQ">Top 7 terminal emulators for Linux</a></li>
<li><a href="https://opensource.com/article/17/2/command-line-tools-data-analysis-linux?intcmp=7016000000127cYAAQ">10 command-line tools for data analysis in Linux</a></li>
<li><a href="https://opensource.com/downloads/advanced-ssh-cheat-sheet?intcmp=7016000000127cYAAQ">Download Now: SSH cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=7016000000127cYAAQ">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://opensource.com/tags/command-line?intcmp=7016000000127cYAAQ">Linux command line tutorials</a></li>
</ul></div>
</div>
</div>
</div>
<p>You've found your way to our 24-day-long Linux command-line toys advent calendar. If this is your first visit to the series, you might be asking yourself what a command-line toy even is. It could be a game or any simple diversion that helps you have fun at the terminal.</p>
<p>Some of you will have seen various selections from our calendar before, but we hope there’s at least one new thing for everyone.</p>
<p>There are many ways to listen to music at the command line; if you've got media stored locally, <strong>cmus</strong> is a great option, but there are <a href="https://opensource.com/life/16/8/3-command-line-music-players-linux">plenty of others</a> as well.</p>
<p>Lots of times when I'm at the terminal, though, I'd really rather just zone out and not pay close attention to picking each song, and let someone else do the work. While I've got plenty of playlists that work for just such a purpose, after a while, even though go stale, and I'll switch over to an internet radio station.</p>
<p>Today's toy, MPlayer, is a versatile multimedia player that will support just about any media format you throw at it. If MPlayer is not already installed, you can probably find it packaged for your distribution. On Fedora, I found it in <a href="https://rpmfusion.org/" target="_blank">RPM Fusion</a> (be aware that this is not an "official" repository for Fedora, so exercise caution):</p>
<pre><code class="language-bash">$ sudo dnf install mplayer</code></pre><p>MPlayer has a slew of command-line options to set depending on your situation. I wanted to listen to the local college radio station here in Raleigh (<a href="https://wknc.org/index.php" target="_blank">88.1 WKNC,</a> they're pretty good!), and so after grabbing the streaming URL from their website, all that took to get my radio up and running, no GUI or web player needed, was:</p>
<pre><code class="language-bash">$ mplayer -nocache -afm ffmpeg http://wknc.sma.ncsu.edu:8000/wknchd1.mp3</code></pre><p>MPlayer is open source under the GPLv3, and you can find out more about the project and download source code from the project's <a href="http://www.mplayerhq.hu/" target="_blank">website</a>.</p>
<p>As I mentioned in yesterday's article, I'm trying to use a screenshot of each toy as the lead image for each article, but as we moved into the world of audio, I had to fudge it a little. So today's image was created from a public domain icon of a radio tower using <strong>img2txt</strong>, which is provided by the <strong>libcaca</strong> package.</p>
<p>Do you have a favorite command-line toy that you we should have included? Our calendar is basically set for the remainder of the series, but we'd still love to feature some cool command-line toys in the new year. Let me know in the comments below, and I'll check it out. And let me know what you thought of today's amusement.</p>
<p>Be sure to check out yesterday's toy, <a href="https://opensource.com/article/18/12/linux-toy-espeak">Let you Linux terminal speak its mind</a>, and come back tomorrow for another!</p>
