<p>If you ever spent time growing up watching spy thrillers, action flicks, or crime movies, you developed a clear picture in your mind of what a hacker's computer screen looked like. Rows upon rows of rapidly moving code, streams of grouped hexadecimal numbers flying past like <a href="http://tvtropes.org/pmwiki/pmwiki.php/Main/MatrixRainingCode" target="_blank">raining code</a> in <em>The Matrix</em>.</p>
<p>Perhaps there's a world map with flashing points of light and a few rapidly updating charts thrown in there for good measure. And probably a 3D rotating geometric shape, because why not? If possible, this is all shown on a ridiculous number of monitors in an ergonomically uncomfortable configuration. I think <em>Swordfish</em> sported seven.</p>
<p> </p>
<div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>The Linux Terminal</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/life/17/10/top-terminal-emulators?intcmp=7016000000127cYAAQ">Top 7 terminal emulators for Linux</a></li>
<li><a href="https://opensource.com/article/17/2/command-line-tools-data-analysis-linux?intcmp=7016000000127cYAAQ">10 command-line tools for data analysis in Linux</a></li>
<li><a href="https://opensource.com/downloads/advanced-ssh-cheat-sheet?intcmp=7016000000127cYAAQ">Download Now: SSH cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=7016000000127cYAAQ">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://opensource.com/tags/command-line?intcmp=7016000000127cYAAQ">Linux command line tutorials</a></li>
</ul></div>
</div>
</div>
</div>
<p>Of course, those of us who pursued technical careers quickly realized that this was all utter nonsense. While many of us have dual monitors (or more), a dashboard of blinky, flashing data is usually pretty antithetical to focusing on work. Writing code, managing projects, and administering systems is not the same thing as day trading. Most of the situations we encounter require a great deal of thinking about the problem we're trying to solve, a good bit of communicating with stakeholders, some researching and organizing information, and very, very little <a href="http://tvtropes.org/pmwiki/pmwiki.php/Main/RapidFireTyping" target="_blank">rapid-fire typing</a>.</p>
<p> </p>
<p>That doesn't mean that we sometimes don't feel like we want to be inside of one of those movies. Or maybe, we're just trying to look like we're "being productive."</p>
<p><strong>Side note: Of course I mean this article in jest.</strong> If you're actually being evaluated on how busy you look, whether that's at your desk or in meetings, you've got a huge cultural problem at your workplace that needs to be addressed. A culture of manufactured busyness is a toxic culture and one that's almost certainly helping neither the company nor its employees.</p>
<p>That said, let's have some fun and fill our screens with some panels of good old-fashioned meaningless data and code snippets. (Well, the data might have some meaning, but not without context.) While there are plenty of fancy GUIs for this (consider checking out <a href="https://hackertyper.net/" target="_blank">Hacker Typer</a> or <a href="http://geektyper.com" target="_blank">GEEKtyper.com</a> for a web-based version), why not just use your standard Linux terminal? For a more old-school look, consider using <a href="https://github.com/Swordfish90/cool-retro-term" target="_blank">Cool Retro Term</a>, which is indeed what it sounds like: A cool retro terminal. I'll use Cool Retro Term for the screenshots below because it does indeed look 100% cooler.</p>
<h2>Genact</h2>
<p>The first tool we'll look at is Genact. Genact simply plays back a sequence of your choosing, slowly and indefinitely, letting your code “compile” while you go out for a coffee break. The sequence it plays is up to you, but included by default are a cryptocurrency mining simulator, Composer PHP dependency manager, kernel compiler, downloader, memory dump, and more. My favorite, though, is the setting which displays SimCity loading messages. So as long as no one checks too closely, you can spend all afternoon waiting on your computer to finish reticulating splines.</p>
<p>Genact has <a href="https://github.com/svenstaro/genact/releases" target="_blank">releases</a> available for Linux, OS X, and Windows, and the Rust <a href="https://github.com/svenstaro/genact" target="_blank">source code</a> is available on GitHub under an <a href="https://github.com/svenstaro/genact/blob/master/LICENSE" target="_blank">MIT license</a>.</p>
<p> </p>
<article class="media media--type-image media--view-mode-full" title="Genact"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/genact.gif" width="600" height="338" alt="Genact" title="Genact" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article><h2>Hollywood</h2>
<p>Hollywood takes a more straightforward approach. It essentially creates a random number and configuration of split screens in your terminal and launches busy looking applications like htop, directory trees, source code files, and others, and switch them out every few seconds. It's put together as a shell script, so it's fairly straightforward to modify as you wish.</p>
<p>The <a href="https://github.com/dustinkirkland/hollywood" target="_blank">source code</a> for Hollywood can be found on GitHub under an <a href="http://www.apache.org/licenses/LICENSE-2.0" target="_blank">Apache 2.0</a> license.</p>
<p> </p>
<article class="media media--type-image media--view-mode-full" title="Hollywood"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/hollywood.gif" width="600" height="338" alt="Hollywood" title="Hollywood" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article><h2>Blessed-contrib</h2>
<p>My personal favorite isn't actually an application designed for this purpose. Instead, it's the demo file for a Node.js-based terminal dashboard building library called Blessed-contrib. Unlike the other two, I actually have used Blessed-contrib's library for doing something that resembles actual work, as opposed to pretend-work, as it is a quite helpful library and set of widgets for displaying information at the command line. But it's also easy to fill with dummy data to fulfill your dream of simulating the computer from <em>WarGames</em>.</p>
<p>The <a href="https://github.com/yaronn/blessed-contrib" target="_blank">source code</a> for Blessed-contrib can be found on GitHub under an <a href="http://opensource.org/licenses/MIT" target="_blank">MIT license</a>.</p>
<p> </p>
<article class="media media--type-image media--view-mode-full" title="Blessed-contrib"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/blessed.gif" width="600" height="338" alt="Blessed-contrib" title="Blessed-contrib" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article><hr /><p>Of course, while these tools make it easy, there are plenty of ways to fill up your screen with nonsense. One of the most common tools you'll see in movies is Nmap, an open source security scanner. In fact, it is so overused as the tool to demonstrate on-screen hacking in Hollywood that the makers have created a page listing some of the movies it has <a href="https://nmap.org/movies/" target="_blank">appeared in</a>, from <em>The Matrix Reloaded</em> to <em>The Bourne Ultimatum</em>, <em>The Girl with the Dragon Tattoo</em>, and even <em>Die Hard 4</em>.</p>
<p>You can create your own combination, of course, using a terminal multiplexer like screen or tmux to fire up whatever selection of data-spitting applications you wish.</p>
<p>What's your go-to screen for looking busy?</p>
