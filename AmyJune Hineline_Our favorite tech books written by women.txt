<p>Whether you're a fast reader or someone who takes a book at a leisurely pace, a whole month is an ample time to explore a new book. As March is Women's History Month, we asked Opensource.com contributors to tell us about their favorite books by women authors.</p>
<hr /><h2>Leadership books</h2>
<p>As an engineer turned agency CTO, one book I consider "required reading" for all of my technical leads and managers is <a href="https://www.oreilly.com/library/view/the-managers-path/9781491973882/">The Manager's Path</a> by Camille Fournier. Fournier does a stellar job outlining all aspects of the sometimes-awkward, sometimes-exhilarating progression from code to mentorship to management to executive leadership. The book is wonderfully tactical and real in its approach and will give you practical leadership tips no matter where you are in your engineering career. I highly recommend it.</p>
<p><strong>—<a href="https://opensource.com/users/kat-white" target="_blank">Kat White</a></strong></p>
<p>These three books inspired and empowered me to own and shape my career in tech. They offer three different points of view: the manager, the engineer, and the hacker. You can understand what it's like to be in these roles and decide the path you want to pursue. You'll get complete and relevant information to act mindfully and to deal with obstacles.</p>
<ul><li>The Manager's Path by Camille Fournier</li>
<li><a href="https://www.oreilly.com/library/view/the-staff-engineers/9781098118723/">The Staff Engineer's Path</a> by Tanya Reilly</li>
<li>Hacking Capitalism by Kris Nóva</li>
</ul><p>Hacking Capitalism can be particularly useful to marginalized people who are willing to fight back.</p>
<p><strong>—<a href="https://opensource.com/users/spotlesstofu">Camilla Conte</a></strong></p>
<p><a href="https://www.kimcrayton.com/profit-without-oppression/">Profit Without Oppression</a> by Kim Crayton</p>
<p><strong>—<a href="https://opensource.com/users/rmcbryde">Rob McBryde</a></strong></p>
<h2>Programming guides</h2>
<ul><li><a href="https://sandimetz.com/99bottles">99 bottles of OOP</a> (it's Ruby) by Sandi Metz</li>
<li>wizard zines by <a href="https://wizardzines.com/">Julia Evans</a></li>
<li><a href="http://www.gregcons.com/KateBlog/BeautifulC30CoreGuidelinesForWritingCleanSafeAndFastCode.aspx">Beautiful C++</a> and <a href="https://www.pearson.ch/Informatik/QUEPublishing/EAN/9780789724663/Special-Edition-Using-Visual-CNET">Using Visual C++</a> by Kate Gregory</li>
</ul><p><strong>­—<a href="https://opensource.com/users/lewiscowles1986">Lewis Cowles</a></strong></p>
<p>I'm inspired by <a href="http://linkedin.com/in/laurathomson" target="_blank">Laura Thomson</a>, current SVP of Engineering at Fastly, Board Trustee at Internet Society, and co-author of the best-selling PHP and MySQL Web Development from Addison-Wesley Professional.</p>
<p><strong>—<a href="https://opensource.com/users/ramsey">Ben Ramsey</a></strong></p>
<p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">Our favorite resources about open source</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-git?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Git cheat sheet">Git cheat sheet</a></div>
              <div class="field__item"><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Advanced Linux commands cheat sheet">Advanced Linux commands cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/alternatives?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Open source alternatives">Open source alternatives</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Free online course: RHEL technical overview">Free online course: RHEL technical overview</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Register for your free Red Hat account">Register for your free Red Hat account</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Check out more cheat sheets">Check out more cheat sheets</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<h2>Women making history</h2>
<p>I enjoyed <a href="https://mitpress.mit.edu/9780262535182/programmed-inequality/">Programmed Inequality</a> by Mar Hicks</p>
<p><strong>—Evelyn Mitchell</strong></p>
<p><a href="https://www.grandcentralpublishing.com/titles/kathy-kleiman/proving-ground/9781538718292/">Proving Ground: The untold story of the six women who programmed the world's first modern computer.</a> by Kathy Kleiman</p>
<p><strong>—<a href="https://opensource.com/users/itsjustdj">DJ Billings</a></strong></p>
<p><a href="https://www.usni.org/press/books/grace-hopper" target="_blank">Grace Hopper: Admiral of the cyber sea</a> by Kathleen Broome Williams</p>
<p><strong>—<a href="https://opensource.com/users/camstonefaux">CamstoneFaux</a></strong></p>
<h2>Share your favorites</h2>
<p>What books are you reading by women authors? Share your recommendations in the comments!</p>
