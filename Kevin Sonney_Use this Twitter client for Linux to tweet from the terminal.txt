<p>Last year, I brought you 19 days of new (to you) productivity tools for 2019. This year, I'm taking a different approach: building an environment that will allow you to be more productive in the new year, using tools you may or may not already be using.</p>
<h2 id="stay-in-tune-with-twitter-with-rainbow-stream">Keep up with Twitter with Rainbow Stream</h2>
<p>I love social networking and microblogging. It's quick, it's easy, and I can share my thoughts with the world really quickly. The drawback is, of course, that almost all the desktop options for non-Windows users are wrappers around the website. <a href="https://twitter.com/home" target="_blank">Twitter</a> has a lot of clients, but what I really want is something lightweight, easy to use, and most importantly, attractive.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Rainbow Stream for Twitter"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/productivity_10-1.png" width="675" height="328" alt="Rainbow Stream for Twitter" title="Rainbow Stream for Twitter" /></div>
      
  </article></p>
<p><a href="https://rainbowstream.readthedocs.io/en/latest/" target="_blank">Rainbow Stream</a> is one of the prettier Twitter clients. It is easy to use and installs quickly with a simple <strong>pip3 install rainbowstream</strong>. On the first run, it will open a browser window and have you authorize with Twitter. Once that is done, you land at a prompt, and your Twitter timeline will start scrolling by.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Rainbow Stream first run"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/productivity_10-2.png" width="675" height="174" alt="Rainbow Stream first run" title="Rainbow Stream first run" /></div>
      
  </article></p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More open source alternatives</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/business/16/3/top-project-management-tools-2016?intcmp=7013a000002D4iFAAS">Open source project management tools</a></li>
<li><a href="https://opensource.com/alternatives/trello?intcmp=7013a000002D4iFAAS">Trello alternatives</a></li>
<li><a href="https://opensource.com/life/15/1/current-state-linux-video-editing?intcmp=7013a000002D4iFAAS">Linux video editors</a></li>
<li><a href="https://opensource.com/life/12/6/design-without-debt-five-tools-for-designers?intcmp=7013a000002D4iFAAS">Open source alternatives to Photoshop</a></li>
<li><a href="https://opensource.com/alternatives?intcmp=7013a000002D4iFAAS">List of open source alternatives</a></li>
<li><a href="https://opensource.com/tags/alternatives?intcmp=7013a000002D4iFAAS">Latest articles about open source alternatives</a></li>
</ul></div>
</div>
</div>
</div>
<p>The most important commands to know are <strong>p</strong> to pause the stream, <strong>r</strong> to resume the stream, <strong>h</strong> to get help, and <strong>t</strong> to post a new tweet. For example, <strong>h tweets</strong> will give you all the options for sending and replying to tweets. Another useful help screen is <strong>h messages</strong>, which gives the commands for working with direct messages, which is something my wife and I use a lot. There are a lot of other commands, and I refer back to help a lot.</p>
<p>As your timeline scrolls by, you can see that it has full UTF-8 support and, with the right font, will show indicators for how many times something was retweeted and liked, as well as icons and emojis.</p>
<p><article class="media media--type-image media--view-mode-full" title="Kill this love"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/day10-image3_1.png" width="3782" height="1770" alt="Kill this love" title="Kill this love" /></div>
      
  </article></p>
<p>One of the <em>best</em> things about Rainbow Stream is that you don't have to give up on photos and images. This feature is off by default, but you can try it out with the <strong>config</strong> command.</p>
<pre><code class="language-text">config IMAGE_ON_TERM = true</code></pre><p>This command renders any images as ASCII art. If you have a photo-heavy stream, this may be a bit much, but I like it. It has a very retro-1990s BBS feel, and I did love the BBS scene in the 1990s.</p>
<p>You can also use Rainbow Stream to manage lists, mute people, block people, follow, unfollow, and everything else that is available with the Twitter API. There is also theme support, so you can customize the stream to your favorite color scheme.</p>
<p>When I'm working and don't want to have yet-another-tab open on my browser, Rainbow Stream lets me keep up in a terminal off to the side.</p>
