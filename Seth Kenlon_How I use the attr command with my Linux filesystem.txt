<p>The term <em>filesystem</em> is a fancy word to describe how your computer keeps track of all the files you create. Whether it's an office document, a configuration file, or thousands of digital photos, your computer has to store a lot of data in a way that's useful for both you and it. Filesystems like Ext4, XFS, JFS, BtrFS, and so on are the "languages" your computer uses to keep track of data.</p>
<p>Your desktop or terminal can do a lot to help you find your data quickly. Your file manager might have, for instance, a filter function so you can quickly see just the image files in your home directory, or it might have a search function that can locate a file by its filename, and so on. These qualities are known as <em>file attributes</em> because they are exactly that: Attributes of the data object, defined by code in file headers and within the filesystem itself. Most filesystems record standard file attributes such as filename, file size, file type, time stamps for when it was created, and time stamps for when it was last visited.</p>
<p>I use the open source XFS filesystem on my computers not for its reliability and high performance but for the subtle convenience of extended attributes.</p>
<h2>Common file attributes</h2>
<p>When you save a file, data about it are saved along with it. Common attributes tell your operating system whether to update the access time, when to synchronize the data in the file back to disk, and other logistical details. Which attributes get saved depends on the capabilities and features of the underlying filesystem.</p>
<p>In addition to standard file attributes (insofar as there are standard attributes), the XFS, Ext4, and BtrFS filesystems can all use extending filesystems.</p>
<h2>Extended attributes</h2>
<p>XFS, Ext4, and BtrFS allow you to create your own arbitrary file attributes. Because you're making up attributes, there's nothing built into your operating system to utilize them, but I use them as "tags" for files in much the same way I use EXIF data on photos. Developers might choose to use extended attributes to develop custom capabilities in applications.</p>
<p>There are two "namespaces" for attributes in XFS: <strong>user</strong> and <strong>root</strong>. When creating an attribute, you must add your attribute to one of these namespaces. To add an attribute to the <strong>root</strong> namespace, you must use the <code>sudo</code> command or be logged in as root.</p>
<h2>Add an attribute</h2>
<p>You can add an attribute to a file on an XFS filesystem with the <code>attr</code> or <code>setfattr</code> commands.</p>
<p>The <code>attr</code> command assumes the <code>user</code> namespace, so you only have to set (<code>-s</code>) a name for your attribute followed by a value (<code>-V</code>):</p>
<pre>
<code class="language-bash">$ attr -s flavor -V vanilla example.txt
Attribute "flavor" set to a 7 byte value for example.txt:
vanilla</code></pre><p>The <code>setfattr</code> command requires that you specify the target namespace:</p>
<pre>
<code class="language-bash">$ setfattr --name user.flavor --value chocolate example.txt</code></pre><h2>List extended file attributes</h2>
<p>Use the <code>attr</code> or <code>getfattr</code> commands to see extended attributes you've added to a file. The <code>attr</code> command defaults to the <strong>user</strong> namespace and uses the <code>-g</code> option to <em>get</em> extended attributes:</p>
<pre>
<code class="language-bash">$ attr -g flavor example.txt
Attribute "flavor" had a 9 byte value for example.txt:
chocolate</code></pre><p>The <code>getfattr</code> command requires the namespace and name of the attribute:</p>
<pre>
<code class="language-bash">$ getfattr --name user.flavor example.txt 
# file: example.txt
user.flavor="chocolate"</code></pre><h2>List all extended attributes</h2>
<p>To see all extended attributes on a file, you can use <code>attr -l</code>:</p>
<pre>
<code class="language-bash">$ attr -l example.txt
Attribute "md5sum" has a 32 byte value for example.txt
Attribute "flavor" has a 9 byte value for example.txt</code></pre><p>Alternately, you can use <code>getfattr -d</code>:</p>
<pre>
<code class="language-bash">$ getfattr -d example.txt
# file: example.txt
user.flavor="chocolate"
user.md5sum="969181e76237567018e14fe1448dfd11"</code></pre><p>Any extended file attribute can be updated with <code>attr</code> or <code>setfattr</code>, just as if you were creating the attribute:</p>
<pre>
<code class="language-bash">$ setfattr --name user.flavor --value strawberry example.txt

$ getfattr -d example.txt
# file: example.txt
user.flavor="strawberry"
user.md5sum="969181e76237567018e14fe1448dfd11"</code></pre><h2>Attributes on other filesystems</h2>
<p>The greatest risk when using extended attributes is forgetting that these attributes are specific to the filesystem they're on. That means when you copy a file from one drive or partition to another, the attributes are lost <em>even if the target filesystem supports extended attributes</em>.</p>
<p>To avoid losing extended attributes, you must use a tool that supports retaining them, such as the <code>rsync</code> command.</p>
<pre>
<code class="language-bash">$ rsync --archive --xattrs ~/example.txt /tmp/</code></pre><p>No matter what tool you use, if you transfer a file to a filesystem that doesn't know what to do with extended attributes, those attributes are dropped.</p>
<p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">More Linux resources</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux commands cheat sheet">Linux commands cheat sheet</a></div>
              <div class="field__item"><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Advanced Linux commands cheat sheet">Advanced Linux commands cheat sheet</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Free online course: RHEL technical overview">Free online course: RHEL technical overview</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux networking cheat sheet">Linux networking cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="SELinux cheat sheet">SELinux cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux common commands cheat sheet">Linux common commands cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="What are Linux containers?">What are Linux containers?</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Register for your free Red Hat account">Register for your free Red Hat account</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Our latest Linux articles">Our latest Linux articles</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<h2>Search for attributes</h2>
<p>There aren't many mechanisms to interact with extended attributes, so the options for using the file attributes you've added are limited. I use extended attributes as a tagging mechanism, which allows me to associate files that have no obvious relation to one another. For instance, suppose I need a Creative Commons graphic for a project I'm working on. Assume I've had the foresight to add the extended attribute <strong>license</strong> to my collection of graphics. I could search my graphic folder with <code>find</code> and <code>getfattr</code> together:</p>
<pre>
<code class="language-bash">find ~/Graphics/ -type f \
-exec getfattr \
--name user.license \
-m cc-by-sa {} \; 2&gt;/dev/null

# file: /home/tux/Graphics/Linux/kde-eco-award.png
user.license="cc-by-sa"
user.md5sum="969181e76237567018e14fe1448dfd11"</code></pre><h2>Secrets of your filesystem</h2>
<p>Filesystems aren't generally something you're meant to notice. They're literally systems for defining a file. It's not the most exciting task a computer performs, and it's not something users are supposed to have to be concerned with. But some filesystems give you some fun, and safe, special abilities, and extended file attributes are a good example. Its use may be limited, but extended attributes are a unique way to add context to your data.</p>
