<p>Git's 17th anniversary is on April 7th, 2022. Since its creation, Git has become a standard tool for software development. It helps programmers track changes they make to their code, and as a code hosting platform, it allows users to find useful applications. If you interact with source code, open source or otherwise, there's a strong likelihood you interact with Git. With so many people using Git daily, I wondered what the open source community's favorite Git commands are. What better way to find out than to ask?</p>
<p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">More on Git</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://opensource.com/resources/what-is-git?intcmp=7013a000002CxqLAAS" data-analytics-category="resource list" data-analytics-text="What is Git?">What is Git?</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-git?intcmp=7013a000002CxqLAAS" data-analytics-category="resource list" data-analytics-text="Git cheat sheet">Git cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-markdown?intcmp=7013a000002CxqLAAS" data-analytics-category="resource list" data-analytics-text="Markdown cheat sheet">Markdown cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/git-tricks-tips?intcmp=7013a000002CxqLAAS" data-analytics-category="resource list" data-analytics-text="eBook: Git tips and tricks">eBook: Git tips and tricks</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Register for your free Red Hat account">Register for your free Red Hat account</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/git?intcmp=7013a000002CxqLAAS" data-analytics-category="resource list" data-analytics-text="New Git articles">New Git articles</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<h2>git diff</h2>
<p>One of the hidden gems of Git is its ability to do word-level diffs.</p>
<pre>
<code class="language-bash">$ git diff --word-diff file1 file2</code></pre><p>It's not just for Git repos, either. It can also diff arbitrary files:</p>
<pre>
<code class="language-bash">$ git diff --word-diff file1 file2</code></pre><p>—<a href="https://opensource.com/users/kartiksubbarao" target="_blank">Kartik Subbarao</a></p>
<h2>git status</h2>
<p>I can't live without the <code>git status</code> command. This one is great if I've recently adjusted my <code>.gitignore</code>, and I'm unsure if I got the syntax correct. It's also nice if I want to quickly see which PHP, SCSS, or JavaScript files are being committed before I do one last PHPCS run-through.</p>
<p>—<a href="https://opensource.com/users/miriamgoldman">Miriam Goldman</a></p>
<h2>git cherry-pick</h2>
<p><span style="font-size:11.5pt; font-variant:normal; white-space:pre-wrap"><span style="font-family:'Open Sans',sans-serif"><span style="color:#1d1c1d"><span style="font-weight:400"><span style="font-style:normal"><span style="text-decoration:none">I use git </span></span></span></span></span></span><code>cherry-pick</code><span style="font-size:11.5pt; font-variant:normal; white-space:pre-wrap"><span style="font-family:'Open Sans',sans-serif"><span style="color:#1d1c1d"><span style="font-weight:400"><span style="font-style:normal"><span style="text-decoration:none"> if I have been working on a branch and want to only keep one or two commits. I can pull out the changes from a single commit and apply them to the branch I'm working on now. It's useful when there are merge conflicts, but there is an important change(s) you need for some reason.</span></span></span></span></span></span></p>
<p>—<a href="https://opensource.com/users/liberatr" target="_blank">Ryan Price</a></p>
<h2>git blame</h2>
<p>I use <code>git blame</code> because invariably when I stare at code and ask, "who did this?!" the answer ends up being <em>me</em>. It's usually something I did long ago, and I've forgotten why it had to be that way. If it wasn't me, then it gives me a pointer to go and ask someone why <em>they</em> did it and why it had to be that way.</p>
<p>—<a href="https://opensource.com/users/warthog9" target="_blank">John 'Warthog9' Hawley</a></p>
<h2>git bisect</h2>
<p>My favorite has to be <code>git bisect</code>. It helps identify the commit that introduced a bug by doing a binary search on commits. Simple to use but incredibly effective.</p>
<p>Also, if I create a script that tells me whether the current source is good or bad, then <code>git bisect run</code> figures everything out on its own!</p>
<p>—<a href="https://opensource.com/users/saud" target="_blank">Mohammed Saud</a></p>
<h2>git stash</h2>
<p>At work, I have to deal with customer projects in order to reproduce specific kinds of behavior. The <code>git stash</code> command allows me to get back to the initial state very quickly after I've applied changes to the projects I want to revert.</p>
<p>—<a href="https://opensource.com/users/hansic99" target="_blank">Stephan Avenwedde</a></p>
<p>Easily <code>git stash</code>! I get derailed a lot into urgent matters (bug fixes) while coding a longer piece. I love how verbose it is. It matches how my brain works. But it's not only about being distracted into more urgent matters. Sometimes, <code>stash</code> serves a higher purpose: The need to chase a divergent solution to something already solved. Maybe it's more elegant or chaotic. Maybe it's just a clean slate, a hiatus that might lead to nothing or something great. Either way, I can always pop back from it and return to my work in progress.</p>
<p>—<a href="https://opensource.com/users/dpino" target="_blank">Diego Pino Navarro</a></p>
<p>I love using <code>git stash</code> and having the ability to stash changes that I'm not sure about and being able to recall them later. It is a great way to be able to noodle around with an idea.</p>
<p>—<a href="https://opensource.com/users/ultimike" target="_blank">Michael Anello</a></p>
<h2>git push</h2>
<p>The first reason is I love <code>git push</code> is that it simply allows you to share your work—your code—with others! The second reason is that <code>push --force</code> is considered harmful and destructive, but I like to see it as a great power that comes with great responsibility. 😊</p>
<p>—<a href="https://opensource.com/users/noaa-barki" target="_blank">Noaa Barki</a></p>
<p>My practical answer: <code>git add -p</code> because it allows me to have that extra review step before I commit my code. Also, I can quickly add just the relevant changes and then do away with any pseudocode or debugging nonsense that's leftover.</p>
<p>My real answer: <code>git push</code> because it reminds me of the song "Kick, Push" by Lupe Fiasco and also means, "I did the thing and all that remains is opening a PR."</p>
<p>—<a href="https://opensource.com/users/qymanab" target="_blank">Qymana Botts</a></p>
<p><strong>[ Keep this command on hand and more with our updated <a href="https://opensource.com/downloads/cheat-sheet-git" target="_blank">Git cheat sheet</a>. ]</strong></p>
<h2>git merge</h2>
<p>My favorite Git command is <code>git merge</code>. Specifically, I love merging development branches into the master branch with the following:</p>
<pre>
<code class="language-bash">$ git merge --strategy recursive -X theirs 3.17.x</code></pre><p>The <code>theirs</code> option within the <code>recursive</code> merge strategy incorporates changes from <em>their</em> side, as long as the changes don't conflict with what they are getting merged into.</p>
<p>—<a href="https://opensource.com/users/joel2001k" target="_blank">Joël Krähemann</a></p>
<p>If you know you never want to override local values when doing a merge, the <code>-Xours</code> flag is a big-time saver, especially when used to sync code between mirrored repos. Should it find a conflict, it forces Git to preserve the local value and ignore any incoming changes.</p>
<p>—<a href="https://opensource.com/users/mandclu" target="_blank">Martin Anderson-Clutz</a></p>
<h2>git reset</h2>
<p>As I find myself constantly having to abandon changes or needing to make sure my local branch is synced with the remote, I use <code>git reset</code> a lot. I also find it a helpful command as it allows me to reset my branch and go back in my repository history.</p>
<p>—<a href="https://opensource.com/users/sedietrich" target="_blank">Sean Dietrich</a></p>
<h2>git reflog or rebase?</h2>
<p>The command <code>git reflog</code> is my Lord and savior, but <code>git rebase -i</code> is my fave because I like to rewrite history.</p>
<p>—<a href="https://opensource.com/users/dangur" target="_blank">Dan Gurin</a></p>
<h2>Your git command</h2>
<p>There are plenty of Git commands to choose from, so what's your favorite? Is there a dream command that Git's lacking? Tell us in the comments!</p>
<h2><a href="https://opensource.com/downloads/cheat-sheet-git" target="_blank">Download our updated Git cheat sheet</a></h2>
