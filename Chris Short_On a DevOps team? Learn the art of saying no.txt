<p>You could say I am a pretty busy person. I'm a husband, the father of an almost-college student and a two-year-old, a DevOps advocate, a CNCF Ambassador, an Opensource.com Community Moderator, a weekly newsletter writer, and a few other things (typing it all out makes me feel like I'm forgetting to do something). One thing I learned last year is that I need to start saying no for my own sanity. The problem I came across was how to say no politely and respectfully.</p>
<p>I was fortunate enough to attend the Google Cloud Developer Community Conference 2018 in March. While I was there, a Google employee asked if I wanted to be part of Google Cloud Platform user experience studies. The employee explained what the program entailed and that there would be monetary compensation. "What a great opportunity!" I thought to myself. "I could help pave the way for awesome new improvements to GCP!" But a friend of mine who was also asked just flat-out said, "No, thank you," and it made me realize that I didn't have time for it either. But I needed to provide an explanation that I would feel good about. I needed to say no politely and respectfully (because I am busy).</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More DevOps resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/resources/devops?intcmp=7013a00000263HlAAI">What is DevOps?</a></li>
<li><a href="https://opensource.com/downloads/devops-hiring-guide?intcmp=7013a00000263HlAAI">The ultimate DevOps hiring guide</a></li>
<li><a href="https://opensource.com/downloads/devops-monitoring-guide?intcmp=7013a00000263HlAAI">DevOps monitoring tools guide</a></li>
<li><a href="https://opensource.com/downloads/devsecops?intcmp=7013a00000263HlAAI">Getting started with DevSecOps</a></li>
<li><a href="https://enterprisersproject.com/cheat-sheet-devops-glossary?intcmp=7013a00000263HlAAI">Download the DevOps glossary</a></li>
<li><a href="https://www.ansible.com/resources/ebooks/ansible-for-devops?intcmp=7013a00000263HlAAI">eBook: Ansible for DevOps</a></li>
<li><a href="https://opensource.com/tags/devops?src=devops_resource_menu2">Latest DevOps articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>Drawing from memory and a quick note in Trello, here's what came out of my mouth: "It's interesting, and normally I would jump at this opportunity. But I have a lot on my plate at the moment. I don't have the time to put my best foot forward. I want to give folks the best version of me possible, so I need to say no." The Google employee said something like, "Wow... I get it and appreciate that answer." I felt good about my answer and knew at that moment: "This is how you say no." It's not that I don't want to; it's that I want to put forth my best and I can't give you that at the moment.</p>
<p>Later, I realized that this is why a company can't do it all themselves. This is why things like cloud providers, infrastructure as code, and abstraction layers exist. When I think of DevOps from a business perspective, I think of core competencies: What is your business good at? Focus on your core competencies, raise the bar iteratively so you can improve them, then add competencies as deemed necessary. Everything else: Find someone or something that can make your business do that thing better.</p>
<p>Cloud providers like GCP, AWS, and Azure are wildly popular because running data centers sucks. Few companies are good at it, and even fewer companies can make a profit doing it. I ask folks, "Are you in the data center business or are you in the <em>X</em> business?" They're in the <em>X</em> business. Their organization will never be in the data center business. Infrastructure as code exists because of the explosion of technology in general. People want their bank statement on their phone and Netflix on their TV while they skim through Facebook on their laptop. None of this is possible without scale. Infrastructure as code makes managing that scale a little bit easier. Abstraction layers like API-driven infrastructure or something like Kubernetes help you scale faster.</p>
<p>Work centers, business units, and companies all need to look at what they are good at and find ways to reduce involvement in things they aren't so good at. Say no because your business is going to burn money trying to get it started, especially if someone else excels at it. You're in the <em>X</em> business—it's not that you don't want to; it's that you want your organization to put forth its best and it can't give that. Be okay with saying no. Be okay with hearing no for the sake of quality (or sanity).</p>
<p>What do you have trouble saying no to? What techniques do you find useful? Share in the comments or tweet us <a href="https://twitter.com/opensourceway" target="_blank">@opersourceway</a> and <a href="https://twitter.com/chrisshort" target="_blank">@chrisshort</a>.</p>
<hr /><p><strong>[See our related story, <a href="https://opensource.com/article/18/2/3-reasons-say-no-devops" target="_blank">3 reasons to say 'no' in DevOps</a>.]</strong></p>
