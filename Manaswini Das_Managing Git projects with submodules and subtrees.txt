<p>If you are into open source development, you have probably worked with Git to manage source code. You might have come across projects with numerous dependencies and/or sub-projects. How do you manage them?</p>
<p>For an open source organization, it can be tricky to achieve single-source documentation and dependency management for the community <em>and</em> the product. The documentation and project often end up fragmented and redundant, which makes them difficult to maintain.</p>
<h2 id="the-need">The need</h2>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Programming and development</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ&amp;src=programming_resource_menu4">Red Hat Developers Blog</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Programming cheat sheets</a></li>
<li><a href="https://www.redhat.com/en/services/training/learning-subscription?intcmp=7016000000127cYAAQ">Try for free: Red Hat Learning Subscription</a></li>
<li><a href="https://opensource.com/downloads/bash-programming-guide?intcmp=7016000000127cYAAQ">eBook: An introduction to programming with Bash</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/bash-shell-cheat-sheet?intcmp=7016000000127cYAAQ">Bash Shell Scripting Cheat Sheet</a></li>
<li><a href="https://developers.redhat.com/e-books/modernizing-enterprise-java?intcmp=7016000000127cYAAQ">eBook: Modernizing Enterprise Java</a></li>
</ul></div>
</div>
</div>
</div>
<p>Suppose you want to use a single project as a child project inside a repository. The traditional method is just to copy the project to the parent repository. But, what if you want to use the same child project in many parent repositories? It wouldn't be feasible to copy the child project into every parent and have to make changes in all of them whenever you update it. This would create redundancy and inconsistency in the parent repositories and make it difficult to update and maintain the child project.</p>
<h2 id="the-solution">Git submodules and subtrees</h2>
<p>What if you could put one project within another using a single command? What if you could just add the project as a child to any number of projects and push changes on the go, whenever you want to? Git provides solutions for this: Git submodules and Git subtrees. These tools were created to support code-sharing development workflows on a more modular level, aspiring to bridge the gap between the Git repository's source-code management (SCM) and the sub-repos within it.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Cherry tree growing on a mulberry tree"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/640px-bialbero_di_casorzo.jpg" width="640" height="480" alt="Cherry tree growing on a mulberry tree" title="Cherry tree growing on a mulberry tree" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>Cherry tree growing on a mulberry tree</sup></p>
</div>
      
  </article></p>
<p>This is a real-life scenario of the concepts this article will cover in detail. If you're already familiar with trees, here is what this model will look like:</p>
<p><article class="media media--type-image media--view-mode-full" title="Tree with subtrees"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/subtree_0.png" width="675" height="319" alt="Tree with subtrees" title="Tree with subtrees" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>CC BY-SA opensource.com</p>
</div>
      
  </article></p>
<h2 id="git-submodules">What are Git submodules?</h2>
<p>Git provides submodules in its default package that enable Git repositories to be nested within other repositories. To be precise, the Git submodule points to a specific commit on the child repository. Here is what Git submodules look like in my <a href="https://github.com/manaswinidas/Docs-test/" target="_blank">Docs-test</a> GitHub repo:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Git submodules screenshot"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/git-submodules_github.png" width="600" height="90" alt="Git submodules screenshot" title="Git submodules screenshot" /></div>
      
  </article></p>
<p>The format <strong>folder@commitId</strong> indicates that the repository is a submodule, and you can directly click on the folder to go to the child repository. The config file called <strong>.gitmodules</strong> contains all the submodule repository details. My repo's <strong>.gitmodules</strong> file looks like this:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Screenshot of .gitmodules file"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/gitmodules.png" width="600" height="112" alt="Screenshot of .gitmodules file" title="Screenshot of .gitmodules file" /></div>
      
  </article></p>
<p>You can use the following commands to use Git submodules in your repositories.</p>
<h3 id="clone-a-repository-and-load-submodules">Clone a repository and load submodules</h3>
<p>To clone a repository containing submodules:</p>
<pre><code class="language-bash">$ git clone --recursive &lt;URL to Git repo&gt;</code></pre><p>If you have already cloned a repository and want to load its submodules:</p>
<pre><code class="language-bash">$ git submodule update --init</code></pre><p>If there are nested submodules:</p>
<pre><code class="language-bash">$ git submodule update --init --recursive</code></pre><h3 id="download-submodules">Download submodules</h3>
<p>Downloading submodules sequentially can be a tedious task, so <strong>clone</strong> and <strong>submodule update</strong> will support the <strong>--jobs</strong> or <strong>-j</strong> parameter.</p>
<p>For example, to download eight submodules at once, use:</p>
<pre><code class="language-bash">$ git submodule update --init --recursive -j 8
$ git clone --recursive --jobs 8 &lt;URL to Git repo&gt;</code></pre><h3 id="pull-submodules">Pull submodules</h3>
<p>Before running or building the parent repository, you have to make sure that the child dependencies are up to date.</p>
<p>To pull all changes in submodules:</p>
<pre><code class="language-bash">$ git submodule update --remote</code></pre><h3 id="create-repositories-with-submodules">Create repositories with submodules</h3>
<p>To add a child repository to a parent repository:</p>
<pre><code class="language-bash">$ git submodule add &lt;URL to Git repo&gt;</code></pre><p>To initialize an existing Git submodule:</p>
<pre><code class="language-bash">$ git submodule init</code></pre><p>You can also create branches and track commits in your submodules by adding <strong>--update </strong>to your <strong>submodule update</strong> command:</p>
<pre><code class="language-bash">$ git submodule update --remote</code></pre><h3 id="update-submodule-commits">Update submodule commits</h3>
<p>As explained above, a submodule is a link that points to a specific commit in the child repository. If you want to update the commit of the submodule, don't worry. You don't need to specify the latest commit explicitly. You can just use the general <strong>submodule update</strong> command:</p>
<pre><code class="language-bash">$ git submodule update</code></pre><p>Just add and commit as you normally would to create and push the parent repository to GitHub.</p>
<h3 id="delete-a-submodule-from-a-parent-repository">Delete a submodule from a parent repository</h3>
<p>Merely deleting a child project folder manually won't remove the child project from the parent repository. To delete a submodule named <strong>childmodule</strong>, use:</p>
<pre><code class="language-bash">$ git rm -f childmodule</code></pre><p>Although Git submodules may appear easy to work with, it can be difficult for beginners to find their way around them.</p>
<h2 id="git-subtrees">What are Git subtrees?</h2>
<p>Git subtrees, introduced in Git 1.7.11, allow you to insert a copy of any repository as a subdirectory of another one. It is one of several ways Git projects can inject and manage project dependencies. It stores the external dependencies in regular commits. Git subtrees provide clean integration points, so they're easier to revert.</p>
<p><span style="color:rgb(14,16,26);background:transparent;margin-top:0pt;margin-bottom:0pt">If you use the <a data-saferedirecturl="https://www.google.com/url?q=https://help.github.com/en/github/using-git/about-git-subtree-merges&amp;source=gmail&amp;ust=1588331330647000&amp;usg=AFQjCNFM8kJmbdstSW0mrBPWpnhcftD3yg" href="https://help.github.com/en/github/using-git/about-git-subtree-merges" target="_blank">subtrees tutorial provided by GitHub</a> to use subtrees, you won't see a </span><strong style="color:rgb(14,16,26);background:transparent;margin-top:0pt;margin-bottom:0pt"><span style="background:transparent;margin-top:0pt;margin-bottom:0pt">.gittrees</span></strong><span style="color:rgb(14,16,26);background:transparent;margin-top:0pt;margin-bottom:0pt"> config file in your local whenever you add a subtree. This makes it difficult to recognize subtrees because subtrees look like general folders, but they are copies of the child repository. The version of Git subtree with the </span><strong style="color:rgb(14,16,26);background:transparent;margin-top:0pt;margin-bottom:0pt"><span style="background:transparent;margin-top:0pt;margin-bottom:0pt">.gittrees</span></strong><span style="color:rgb(14,16,26);background:transparent;margin-top:0pt;margin-bottom:0pt"> config file is not available with the default Git package, so to get the git-subtree with </span><strong style="color:rgb(14,16,26);background:transparent;margin-top:0pt;margin-bottom:0pt"><span style="background:transparent;margin-top:0pt;margin-bottom:0pt">.gittrees</span></strong><span style="color:rgb(14,16,26);background:transparent;margin-top:0pt;margin-bottom:0pt"> config file, you must download git-subtree from the </span><a data-saferedirecturl="https://www.google.com/url?q=https://github.com/git/git/tree/master/contrib/subtree&amp;source=gmail&amp;ust=1588331330647000&amp;usg=AFQjCNGnUdxDbthk8b05jRE95DWWtLfwLw" href="https://github.com/git/git/tree/master/contrib/subtree" style="background:transparent;margin-top:0pt;margin-bottom:0pt;color:rgb(74,110,224)" target="_blank"><strong style="color:rgb(14,16,26);background:transparent;margin-top:0pt;margin-bottom:0pt"><span style="background:transparent;margin-top:0pt;margin-bottom:0pt">/contrib/subtree</span></strong><span style="background:transparent;margin-top:0pt;margin-bottom:0pt"> folder</span></a><span style="color:rgb(14,16,26);background:transparent;margin-top:0pt;margin-bottom:0pt"> in the Git source repository.</span></p>
<p>You can clone any repository containing subtrees, just like any other general repository, but it may take longer because entire copies of the child repository reside in the parent repository.</p>
<p>You can use the following commands to use Git subtrees in your repositories.</p>
<h3 id="add-a-subtree-to-a-parent-repository">Add a subtree to a parent repository</h3>
<p>To add a new subtree to a parent repository, you first need to <strong>remote add</strong> it and then run the <strong>subtree add</strong> command, like:</p>
<pre><code class="language-bash">$ git remote add remote-name &lt;URL to Git repo&gt;
$ git subtree add --prefix=folder/ remote-name &lt;URL to Git repo&gt; subtree-branchname</code></pre><p>This merges the whole child project's commit history to the parent repository.</p>
<h3 id="push-and-pull-changes-to-and-from-the-subtree">Push and pull changes to and from the subtree</h3>
<pre><code class="language-bash">$ git subtree push-all</code></pre><p>or</p>
<pre><code class="language-bash">$ git subtree pull-all</code></pre><h2 id="which-should-you-use">Which should you use?</h2>
<p>Every tool has pros and cons. Here are some features that may help you decide which is best for your use case.</p>
<ul><li>Git submodules have a smaller repository size since they are just links that point to a specific commit in the child project, whereas Git subtrees house the entire child project along with its history.</li>
<li>Git submodules need to be accessible in a server, but subtrees are decentralized.</li>
<li>Git submodules are mostly used in component-based development, whereas Git subtrees are used in system-based development.</li>
</ul><p>A Git subtree isn't a direct alternative to a Git submodule. There are certain caveats that guide where each can be used. If there is an external repository you own and are likely to push code back to, use Git submodule since it is easier to push. If you have third-party code that you are unlikely to push to, use Git subtree since it is easier to pull.</p>
<p>Give Git subtrees and submodules a try and let me know how it goes in the comments.</p>
