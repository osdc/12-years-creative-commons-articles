<p>When people think about open source hardware, they often think about the general category of electronics that can be soldered and needs batteries. While there are <a href="https://certification.oshwa.org/list.html" target="_blank">many</a> fantastic open source pieces of electronics, the overall category of open source hardware is much broader. This month we take a look at open source hardware that you can take out into the world, no power outlet or batteries required.</p>
<h3>Hummingbird Hammocks</h3>
<p><a href="https://hummingbirdhammocks.com/" target="_blank">Hummingbird Hammocks</a> offers an entire line of open source camping gear. You can set up an open source <a href="https://certification.oshwa.org/us000102.html" target="_blank">rain tarp</a>...</p>
<p><article class="media media--type-image media--view-mode-full" title="An open source rain tarp from Hummingbird Hammocks"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/01-hummingbird_hammocks_rain_tarp.png" width="645" height="645" alt="An open source rain tarp from Hummingbird Hammocks" title="An open source rain tarp from Hummingbird Hammocks" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>An open source rain tarp from Hummingbird Hammocks.</sup></p>
</div>
      
  </article></p>
<p>...with open source <a href="https://certification.oshwa.org/us000105.html" target="_blank">friction adjusters</a></p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Open source friction adjusters from Hummingbird Hammocks."><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/02-hummingbird_hammocks_friction_adjusters_400_px.png" width="400" height="312" alt="Open source friction adjusters from Hummingbird Hammocks." title="Open source friction adjusters from Hummingbird Hammocks." /></div>
      
  </article></p>
<p>...over your open source <a href="https://certification.oshwa.org/us000095.html" target="_blank">hammock</a></p>
<p><article class="align-center media media--type-image media--view-mode-full" title="An open source hammock from Hummingbird Hammocks."><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/03-hummingbird_hammocks_hammock_400_px.png" width="400" height="400" alt="An open source hammock from Hummingbird Hammocks." title="An open source hammock from Hummingbird Hammocks." /></div>
      
  </article></p>
<p>...hung with open source <a href="https://certification.oshwa.org/us000098.html" target="_blank">tree straps</a>.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Open source tree straps from Hummingbird Hammocks."><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/04-hummingbird_hammocks_tree_straps_400_px_0.png" width="400" height="400" alt="Open source tree straps from Hummingbird Hammocks." title="Open source tree straps from Hummingbird Hammocks." /></div>
      
  </article></p>
<p>The design for each of these items is fully documented, so you can even use them as a starting point for making your own outdoor gear (if you are willing to trust friction adjusters you design yourself).</p>
<h3>Openfoil</h3>
<p><a href="https://certification.oshwa.org/fr000004.html" target="_blank">Openfoil</a> is an open source hydrofoil for kitesurfing. Hydrofoils are attached to the bottom of kiteboards and allow the rider to rise out of the water. This aspect of the design makes riding in low wind situations and with smaller kites easier. It can also reduce the amount of noise the board makes on the water, making for a quieter experience. Because this hydrofoil is open source you can customize it to your needs and adventure tolerance.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Openfoil, an open source hydrofoil for kitesurfing."><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/05-openfoil-original_size.png" width="386" height="217" alt="Openfoil, an open source hydrofoil for kitesurfing." title="Openfoil, an open source hydrofoil for kitesurfing." /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>Openfoil, an open source hydrofoil for kitesurfing.</sup></p>
</div>
      
  </article></p>
<h3>Solar water heater</h3>
<p>If you prefer your outdoors-ing a bit closer to home, you could build this open source <a href="https://certification.oshwa.org/mx000002.html" target="_blank">solar water heater</a> created by the <a href="http://www.fundacionanisa.org/index.php?lang=en" target="_blank">Anisa Foundation</a>. This appliance focuses energy from the sun to heat water that can then be used in your home, letting you reduce your carbon footprint without having to give up long, hot showers. Of course, you can also <a href="https://thingspeak.com/channels/72565" target="_blank">monitor its temperature </a>over the internet if you need to feel connected.</p>
<p><span style="color:#e74c3c;"></span></p>
<article class="align-center media media--type-image media--view-mode-full" title="An open source solar water heater from the Anisa Foundation."><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/06-solar_water_heater_500_px.png" width="500" height="375" alt="An open source solar water heater from the Anisa Foundation." title="An open source solar water heater from the Anisa Foundation." /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>An open source solar water heater from the Anisa Foundation.</sup></p>
</div>
      
  </article><p></p>
<h2>Wrapping up</h2>
<p>As these projects make clear, open source hardware is more than just electronics. You can take it with you to the woods, to the beach, or just to your roof. Next month we’ll talk about open source instruments and musical gear. Until then, <a href="https://certification.oshwa.org/" target="_blank">certify</a> your open source hardware! </p>
