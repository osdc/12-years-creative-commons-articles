<p>Recently, the microSD (secure digital) card in my <a href="https://opensource.com/resources/raspberry-pi" target="_blank">Raspberry Pi</a> stopped working. It had been in constant use as a server for almost two years, and this provided a good opportunity to start fresh and correct a few problems. After its initial installation, it began experiencing disk problems and the official Raspberry Pi operating system (OS) received a significant update (and was renamed from Raspbian to Raspberry Pi OS). So I acquired a new microSD card and preceded to rebuild.</p>
<p>Although this Raspberry Pi 3 Model B isn't the latest hardware, it is still adequate for running a minimal server for various services. I think my original installation used the full operating system image that includes the graphical user interface and a lot of other software packages unnecessary for my needs.</p>
<p>This step-by-step guide shows how I set up my Raspberry Pi with the most minimal configuration to conserve precious system resources.</p>
<h2 id="get-started">Get started</h2>
<p>To begin, create a new operating system drive for the Pi. This requires two things: an OS image file and a microSD card.</p>
<h3 id="raspberry-pi-os-image-file">Download the Raspberry Pi OS image file</h3>
<p>While several operating systems are available, I chose to stick to the officially supported OS.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on Raspberry Pi</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/resources/what-raspberry-pi?src=raspberry_pi_resource_menu1&amp;intcmp=701f2000000h4RcAAI">What is Raspberry Pi?</a></li>
<li><a href="https://opensource.com/downloads/raspberry-pi-guide?src=raspberry_pi_resource_menu2&amp;intcmp=701f2000000h4RcAAI">eBook: Guide to Raspberry Pi</a></li>
<li><a href="https://opensource.com/downloads/getting-started-raspberry-pi-cheat-sheet?src=raspberry_pi_resource_menu3&amp;intcmp=701f2000000h4RcAAI">Getting started with Raspberry Pi cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/kubernetes-raspberry-pi?src=raspberry_pi_resource_menu6&amp;intcmp=701f2000000h4RcAAI">eBook: Running Kubernetes on your Raspberry Pi</a></li>
<li><a href="https://www.redhat.com/en/resources/data-intensive-applications-hybrid-cloud-blueprint-detail?src=raspberry_pi_resource_menu7&amp;intcmp=701f2000000h4RcAAI">Whitepaper: Data-intensive intelligent applications in a hybrid cloud blueprint</a></li>
<li><a href="https://www.redhat.com/en/topics/edge-computing?src=raspberry_pi_resource_menu5&amp;intcmp=701f2000000h4RcAAI">Understanding edge computing</a></li>
<li><a href="https://opensource.com/tags/raspberry-pi?src=raspberry_pi_resource_menu4&amp;intcmp=701f2000000h4RcAAI">Our latest on Raspberry Pi </a></li>
</ul></div>
</div>
</div>
</div>
<p>The first step is to download the newest OS image file from the official <a href="https://www.raspberrypi.org/software/operating-systems" target="_blank">Raspberry Pi OS</a> site to a computer you can use to write to a microSD card. Three different images are offered, and I chose the Raspberry Pi OS Lite. It is the smallest OS and includes only the essential files required for a base OS, so it will consume the least amount of disk space and system RAM. (When I downloaded the OS, the release date was August 20, 2020, but it has been updated since then. I do not expect any major differences, but as always, I recommend reading the release notes.)</p>
<h3 id="the-microsd-card">Write the OS to the microSD Card</h3>
<p>The second step is to write the downloaded OS image file to the microSD card. My card was used previously, and when I inserted it into my Linux desktop, it automatically mounted its two existing partitions. I couldn't write the image until I unmounted these partitions. To do so, I had to determine their path with the <code>lsblk</code> command, which identified the device as <code>/dev/mmcblk0</code>:</p>
<pre><code class="language-bash"># lsblk -p</code></pre><p>I then unmounted the partitions with the <code>umount</code> command:</p>
<pre><code class="language-bash"># umount /dev/mmcblk0p2
# umount /dev/mmcblk0p1</code></pre><p>Once the partitions are unmounted, write the image file to the microSD card. Although there are many graphical image-writing tools available, I used the venerable <code>dd</code> command:</p>
<pre><code class="language-bash"># dd bs=4M if=/home/alan/Downloads/raspios/2020-08-20-raspios-buster-armhf-lite.img of=/dev/mmcblk0 status=progress conv=fsync</code></pre><h3 id="boot-the-pi">Boot the Pi</h3>
<p>You just need a monitor, keyboard, and power adapter to access the Raspberry Pi. I also have an Ethernet cable for network connectivity, which I prefer over wireless—especially for a dedicated server.</p>
<p>Insert the microSD card and power on the Pi. Once it boots, log in with the default credentials: user <code>pi</code> and password <code>raspberry</code>.</p>
<h2 id="set-the-configuration">Configure the OS</h2>
<p>Take the following steps to minimize your installation, disk space, and memory usage as much as possible. I recommend spending time to research each configuration to be as correct as possible. There are often several ways to apply a configuration, and configuration files and directives can be deprecated. Always review a product's documentation to ensure you're not applying an outdated configuration.</p>
<h3 id="run-raspi-config">Run raspi-config</h3>
<p>The main configuration program in Raspberry Pi OS is called raspi-config. Run it immediately after logging in:</p>
<pre><code class="language-bash"># raspi-config</code></pre><p>
<article class="media media--type-image media--view-mode-full" title="Raspberry Pi config main window"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/raspi-config-main.png" width="600" height="396" alt="Raspberry Pi config main window" title="Raspberry Pi config main window" /></div>
      
  </article></p>
<p>It presents an option to expand the root filesystem to use all of the available space on the microSD card. After taking this option, reboot and log in again.</p>
<p>Verify that the card's full capacity is being used with the <code>df</code> command:</p>
<pre><code class="language-bash"># df -h</code></pre><p>If you need to configure other options, run <code>raspi-config</code> again. Some of these will vary according to your requirements or preferences. Go through all of them just to be sure you don't miss anything. I recommend the following changes for best performance. (I will skip the sections where I did not make any changes.)</p>
<ul><li><strong>System options:</strong> You can set the hostname, preferably using a fully qualified domain name (FQDN). You can also change your password here, which is always highly recommended.</li>
<li><strong>Interface options:</strong> Enable SSH.</li>
<li><strong>Performance options:</strong> Reduce GPU memory to the lowest setting (16MB).</li>
<li><strong>Localization options:</strong> Choose your time zone, location, and keyboard type.</li>
<li><strong>Advanced options:</strong> This section contains the Expand Filesystem option to expand the root filesystem. If you didn't do this above, be sure to do it here so that you have access to all storage available on the microSD card.</li>
<li><strong>Update:</strong> Entering the Update section immediately checks for an update to the raspi-config tool. If an update is available, it will be downloaded and applied. Otherwise, raspi-config will re-launch after a few seconds.</li>
</ul><p>Once you complete these configurations in raspi-config, select <strong>Finish</strong> to exit the tool.</p>
<h3 id="manual-configurations">Manual configurations</h3>
<p>There are several other changes that I recommend. They are all manual changes that require editing certain configuration files.</p>
<h4 id="configure-static-ip">Configure static IP</h4>
<p>Generally, it is best to configure a server with a static IP address. To configure the IP and your default gateway (router) and domain name service (DNS) addresses, begin by identifying the network interface device with the <code>ip</code> command:</p>
<pre><code class="language-bash"># ip link
1: lo: &lt;LOOPBACK,UP,LOWER_UP&gt; mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
2: eth0: &lt;BROADCAST,MULTICAST,UP,LOWER_UP&gt; mtu 1500 qdisc pfifo_fast state UP mode DEFAULT group default qlen 1000
    link/ether b8:27:eb:48:3f:46 brd ff:ff:ff:ff:ff:ff</code></pre><p>You also need to know the IP address of your default gateway and one or more DNS servers. Add this information to the file <code>/etc/dhcpcd.conf</code> (<em>I strongly suggest making a backup of this file before making changes)</em>:</p>
<pre><code class="language-bash"># cd /etc
# cp -a dhcpcd.conf dhcpcd.conf.original</code></pre><p>Edit the file as shown:</p>
<pre><code class="language-bash"># vi dhcpcd.conf

# static IP configuration:
interface eth0
static ip_address=192.168.1.5/24
static routers=192.168.1.1
static domain_name_servers=192.168.1.3 192.168.1.4</code></pre><h4 id="disable-ipv6">Disable IPv6</h4>
<p>Unless you specifically need to use IPv6, you might prefer to disable it. Do this by creating two new files that include a one-line directive instructing the Linux kernel not to use IPv6.</p>
<p>First, create the file <code>/etc/sysctl.d/disable-ipv6.conf</code> with the line<br /><br /><code>net.ipv6.conf.all.disable_ipv6 = 1</code>:</p>
<pre><code class="language-bash"># cd /etc/sysctl.d
# echo "net.ipv6.conf.all.disable_ipv6 = 1" &gt; disable-ipv6.conf</code></pre><p>Then create the file <code>/etc/modprobe.d/blacklist-ipv6.conf</code> with the line <code>blacklist ipv6</code>:</p>
<pre><code class="language-bash"># cd /etc/modprobe.d
# echo "blacklist ipv6" &gt; blacklist-ipv6.conf</code></pre><h4 id="disable-wifi-bluetooth-and-audio">Disable WiFi, Bluetooth, and audio</h4>
<p>My server's specific purpose will not need Bluetooth or audio. Also, since it's connected with Ethernet, it will not use wireless (WiFi). Unless you plan to use them, disable them with the following steps.</p>
<p>Make the following changes to the file <code>/boot/config.txt</code> <em>(again, I suggest making a backup of this file)</em>:</p>
<pre><code class="language-bash"># cd /boot
# cp -a config.txt config.txt.original</code></pre><p>Add the following two directives to the bottom of the file to disable Bluetooth and WiFi:</p>
<ul><li><code>dtoverlay=disable-bt</code></li>
<li><code>dtoverlay=disable-wifi</code></li>
</ul><p>These echo commands will do the trick:</p>
<pre><code class="language-bash"># cd /boot
# echo "dtoverlay=disable-bt" &gt;&gt; config.txt
# echo "dtoverlay=disable-wifi" &gt;&gt; config.txt</code></pre><p>To disable audio, change the parameter <code>dtparam=audio</code> to <code>off</code>. You can do this with a short <code>sed</code> command:</p>
<pre><code class="language-bash"># sed -i '/dtparam=audio/c dtparam=audio=off' config.txt</code></pre><p>The last step is to disable the WiFi service. Use the <code>systemctl mask</code> command:</p>
<pre><code class="language-bash">systemctl mask wpa_supplicant.service</code></pre><p>You can disable a couple of other services if you won't need them:</p>
<ul><li><strong>Disable modem service:</strong><br /><pre><code class="language-bash">systemctl disable hciuart</code></pre></li>
<li><strong>Disable Avahi-daemon:</strong><br /><pre><code class="language-bash">systemctl disable avahi-daemon.service</code></pre></li>
</ul><h2 id="final-steps">Final steps</h2>
<ul><li><strong>Check your memory usage:</strong><br /><pre><code class="language-bash"># free -h</code></pre><p>	I was astonished: My OS only uses 30MB of RAM.</p></li>
<li><strong>Create personal accounts:</strong> It is advisable to create user accounts for any individuals who will log into this server. You can assign them to the sudo group to allow them to issue administrative commands. For example, to give a user named George an account:<br /><pre><code class="language-bash"># adduser george
# usermod -a -G adm,sudo,users george</code></pre></li>
<li><strong>Get updates:</strong> This is an important step. Apply updates to get the latest fixes to the Raspberry Pi OS:<br /><pre><code class="language-bash"># apt update
# apt full-upgrade</code></pre></li>
<li><strong>Reboot:</strong> It's a good idea to reboot your new server:<br /><pre><code class="language-bash"># systemctl reboot</code></pre></li>
<li><strong>Install Cockpit:</strong> You can install <a href="https://cockpit-project.org/" target="_blank">Cockpit</a>, also known as the Linux Web Console, on Raspberry Pi OS. It provides an HTML-based interface for managing and monitoring your server remotely. I recently wrote about <a href="https://opensource.com/article/20/11/cockpit-server-management">getting started with Cockpit</a>. Install it with:<br /><pre><code class="language-bash"># apt install cockpit</code></pre></li>
</ul><p>Now my Raspberry Pi is ready to host a server. I could use it for a <a href="https://opensource.com/article/17/3/building-personal-web-server-raspberry-pi-3">web server</a>, a <a href="https://opensource.com/article/19/6/raspberry-pi-vpn-server">VPN server</a>, a game server such as <a href="https://github.com/minetest" target="_blank">Minetest</a>, or (as I did) an <a href="https://opensource.com/article/18/2/block-ads-raspberry-pi">ad blocker based on Pi-Hole</a>.</p>
<h2 id="keep-old-hardware-alive">Keep old hardware alive</h2>
<p>Regardless of what hardware you have available, carefully minimizing and controlling your operating system and packages can keep your resource usage low so that you can get the most out of it. This also improves security by reducing the number of services and packages available to would-be mal-actors trying to exploit a vulnerability.</p>
<p>So, before you decommission older hardware, consider all the possibilities for how it can continue to be used.</p>
