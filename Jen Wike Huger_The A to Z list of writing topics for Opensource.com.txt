<p>Would you like to write for us?</p>
<p>Opensource.com is looking for stories, tutorials, and opinion peices on the topics and ideas you see below. From personal experiences in open source and Linux to more technical tutorials on everything from Git to the command line to open hardware, Opensource.com is a home for a wide range of open stories.</p>
<p>To submit your idea or draft for review, <a href="https://opensource.com/how-submit-article">contact us</a>. </p>
<p>To learn more about our writing guidelines check out our <a href="https://opensource.com/writers" title="writers and contributors">resources for contributors</a>.</p>
<ul><li><b>Apache </b>stories and open source project updates.</li>
<li><b>Big data</b> tools, stories, communities, and news.</li>
<li><b>Command-line </b>tips and tricks for Linux.</li>
<li><b>Content management system </b>tutorials and updates on Drupal, ezPublish, Joomla, TYPO3, WordPress, and others.</li>
<li><b>DevOps</b> best practices, case studies, and "getting started" stories.</li>
<li><b>Education</b> tools, solutions, and resources for teachers, parents, and students.</li>
<li><strong>FOSS</strong> project introductions, updates, and opinions.</li>
<li><b>Geek</b> culture stories.</li>
<li><b>Hardware</b> projects, maker culture, new products, howtos, and tutorials.</li>
<li><b>Humanitarian</b> open source project stories, news, communities, and updates.</li>
<li><strong>IoT</strong> (Internet of Things) devices and tutorials for home automation and more.</li>
<li><strong>Javascript</strong> frameworks.</li>
<li><strong>Kernel</strong>, the Linux kernel that is. Tell us about what it's like to work on the Linux kernel.</li>
<li><strong>Linux</strong> projects, tutorials, stories. Anything and everything Linux.</li>
<li><strong>Maker</strong> movement articles and projects.</li>
<li><strong>Music</strong> applications and programs.</li>
<li><strong>Networking </strong>news and tutorials.</li>
<li><strong>Open organization </strong>stories focused on management, teams, tools, and philosophy.</li>
<li><strong>3D Printing</strong> howtos, projects, and ideas.</li>
<li><strong>Python</strong> projects and stories from the community.</li>
<li><strong>Perl</strong> programming tips and tricks</li>
<li><strong>Q&amp;A</strong> articles with your favorite open source developers, leaders, innovators, makers, and more.</li>
<li><strong>Raspberry Pi</strong> projects and tutorials.</li>
<li><b>Science</b> programs, journals, news, and tools for researchers and in the classroom.</li>
<li><strong>Sysadmin</strong> howtos, tutorials, and tools.</li>
<li><strong>Tutorials</strong> on everything from software to hardware.</li>
<li><strong>UX</strong> design thinking and ideas for how to improve.</li>
<li><strong>Video editing</strong> tools, programs, tutorials, and opinions.</li>
<li><strong>Web</strong> tools, innovations, development solutions, and more.</li>
<li><strong>X</strong> open source alternatives for Y</li>
<li><strong>Your Linux or open source story</strong>. Tell us how you found Linux/open source, what you love about it, and what tools you use in your daily experience.</li>
<li><strong>Z</strong>zzzzz.... What passion projects keep you up at night? Tell us about the open source projects you work on in your personal time, the open hardware projects you build on nights and weekends, the open source communities you spend time with when you aren't in the office, and the new open source skills you're picking up in your "off" hours.</li>
</ul>