<p>When you're writing an application or configuring one for a server, eventually, you will need to store persistent information. Sometimes, a configuration file, such as an INI or <a href="https://www.redhat.com/sysadmin/yaml-tips" target="_blank">YAML</a> file<span style="color:#e74c3c;"> </span>will do. Other times, a custom file format designed in XML or JSON or similar is better.</p>
<p>But sometimes you need something that can validate input, search through information quickly, make connections between related data, and generally handle your users' work adeptly. That's what a database is designed to do, and <a href="https://mariadb.org/" target="_blank">MariaDB</a> (a fork of <a href="https://www.mysql.com/" target="_blank">MySQL</a> by some of its original developers) is a great option. I use MariaDB in this article, but the information applies equally to MySQL.</p>
<p>It's common to interact with a database through programming languages. For this reason, there are <a href="https://en.wikipedia.org/wiki/SQL" target="_blank">SQL</a> libraries for Java, Python, Lua, PHP, Ruby, C++, and many others. However, before using these libraries, it helps to have an understanding of what's happening with the database engine and why your choice of database is significant. This article introduces MariaDB and the <code>mysql</code> command to familiarize you with the basics of how a database handles data.</p>
<p>If you don't have MariaDB yet, follow the instructions in my article about <a href="https://opensource.com/article/20/10/install-mariadb-and-mysql-linux" target="_blank">installing MariaDB on Linux</a>. If you're not on Linux, use the instructions provided on the MariaDB <a href="https://mariadb.org/download" target="_blank">download page</a>.</p>
<h2 id="interact-with-mariadb">Interact with MariaDB</h2>
<p>You can interact with MariaDB using the <code>mysql</code> command. First, verify that your server is up and running using the <code>ping</code> subcommand, entering your MariaDB password when prompted:</p>
<pre><code class="language-bash">$ mysqladmin -u root -p ping
Enter password:
mysqld is alive</code></pre><p>To make exploring SQL easy, open an interactive MariaDB session:</p>
<pre><code class="language-bash">$ mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.
Commands end with ; or \g.
[...]
Type 'help;' or '\h' for help.
Type '\c' to clear the current input statement.

MariaDB [(none)]&gt;</code></pre><p>This places you in a MariaDB subshell, and your prompt is now a MariaDB prompt. Your usual Bash commands don't work here. You must use MariaDB commands. To see a list of MariaDB commands, type <code>help</code> (or just <code>?</code>). These are administrative commands for your MariaDB shell, so they're useful for customizing your shell, but they aren't part of the SQL language.</p>
<h2 id="learn-sql-basics">Learn SQL basics</h2>
<p>The <a href="https://publications.opengroup.org/c449" target="_blank">Structured Query Language (SQL)</a> is named after what it provides: a method to inquire about the contents of a database in a predictable and consistent syntax in order to receive useful results. SQL reads a lot like an ordinary English sentence, if a little robotic. For instance, if you've signed into a database server and you need to understand what you have to work with, type <code>SHOW DATABASES;</code> and press Enter for the results.</p>
<p>SQL commands are terminated with a semicolon. If you forget the semicolon, MariaDB assumes you want to continue your query on the next line, where you can either do so or terminate the query with a semicolon.</p>
<pre><code class="language-sql">MariaDB [(none)]&gt; SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| test               |
+--------------------+
4 rows in set (0.000 sec)</code></pre><p>This shows there are four databases present: information_schema, mysql, performance_schema, and test. To issue queries to a database, you must select which database you want MariaDB to use. This is done with the MariaDB command <code>use</code>. Once you choose a database, your MariaDB prompt changes to reflect the active database.</p>
<pre><code class="language-sql">MariaDB [(none)]&gt; use test;
MariaDB [(test)]&gt; </code></pre><h3 id="show-database-tables">Show database tables</h3>
<p>Databases contain <em>tables</em>, which can be visualized in the same way a spreadsheet is: as a series of rows (called <em>records</em> in a database) and columns. The intersection of a row and a column is called a <em>field</em>.</p>
<p>To see the tables available in a database (you can think of them as tabs in a multi-sheet spreadsheet), use the SQL keyword <code>SHOW</code> again:</p>
<pre><code class="language-sql">MariaDB [(test)]&gt; SHOW TABLES;
empty set</code></pre><p>The <code>test</code> database doesn't have much to look at, so use the <code>use</code> command to switch to the <code>mysql</code> database.</p>
<pre><code class="language-sql">MariaDB [(test)]&gt; use mysql;
MariaDB [(mysql)]&gt; SHOW TABLES;

+---------------------------+
| Tables_in_mysql           |
+---------------------------+
| column_stats              |
| columns_priv              |
| db                        |
[...]
| time_zone_transition_type |
| transaction_registry      |
| user                      |
+---------------------------+
31 rows in set (0.000 sec)</code></pre><p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Linux resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux commands cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Free online course: RHEL Technical Overview</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux networking cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">SELinux cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux common commands cheat sheet</a></li>
<li><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">What are Linux containers?</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Our latest Linux articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>There are a lot more tables in this database! The <code>mysql</code> database is the system management database for this MariaDB instance. It contains important data, including an entire user structure to manage database privileges. It's an important database, and you don't always have to interact with it directly, but it's not uncommon to manipulate it in SQL scripts. It's also useful to understand the <code>mysql</code> database when you're learning MariaDB because it can help demonstrate some basic SQL commands.</p>
<h3 id="examine-a-table">Examine a table</h3>
<p>The last table listed in this instance's <code>mysql</code> database is titled <code>user</code>. This table contains data about users permitted to access the database. Right now, there's only a root user, but you can add other users with varying privileges to control whether each user can view, update, or create data. To get an idea of all the attributes a MariaDB user can have, you can view column headers in a table:</p>
<pre><code class="language-sql">&gt; SHOW COLUMNS IN user;
MariaDB [mysql]&gt; SHOW columns IN user;
+-------------+---------------+------+-----+----------+
| Field       | Type          | Null | Key | Default  |
+-------------+---------------+------+-----+----------+
| Host        | char(60)      | NO   | PRI |          |
| User        | char(80)      | NO   | PRI |          |
| Password    | char(41)      | NO   |     |          |
| Select_priv | enum('N','Y') | NO   |     | N        |
| Insert_priv | enum('N','Y') | NO   |     | N        |
| Update_priv | enum('N','Y') | NO   |     | N        |
| Delete_priv | enum('N','Y') | NO   |     | N        |
| Create_priv | enum('N','Y') | NO   |     | N        |
| Drop_priv   | enum('N','Y') | NO   |     | N        |
[...]
47 rows in set (0.001 sec)</code></pre><h3 id="create-a-new-user">Create a new user</h3>
<p>Whether you need help from a fellow human to administer a database or you're setting up a database for a computer to use (for example, in a WordPress, Drupal, or Joomla installation), it's common to need an extra user account within MariaDB. You can create a MariaDB user either by adding it to the <code>user</code> table in the <code>mysql</code> database, or you can use the SQL keyword <code>CREATE</code> to prompt MariaDB to do it for you. The latter features some helper functions so that you don't have to generate all the information manually:</p>
<pre><code class="language-sql">&gt; CREATE USER 'tux'@'localhost' IDENTIFIED BY 'really_secure_password';</code></pre><h3 id="view-table-fields">View table fields</h3>
<p>You can view fields and values in a database table with the <code>SELECT</code> keyword. In this example, you created a user called <code>tux</code>, so select the columns in the <code>user</code> table:</p>
<pre><code class="language-sql">&gt; SELECT user,host FROM user;
+------+------------+
| user | host       |
+------+------------+
| root | localhost  |
[...]
| tux  | localhost  |
+------+------------+
7 rows in set (0.000 sec)</code></pre><h3 id="grant-privileges-to-a-user">Grant privileges to a user</h3>
<p>By looking at the column listing on the <code>user</code> table, you can explore a user's status. For instance, the new user <code>tux</code> doesn't have permission to do anything with the database. Using the <code>WHERE</code> statement, you can view only the record for <code>tux</code>:</p>
<pre><code class="language-sql">&gt; SELECT user,select_priv,insert_priv,update_priv FROM user WHERE user='tux';
+------+-------------+-------------+-------------+
| user | select_priv | insert_priv | update_priv |
+------+-------------+-------------+-------------+
| tux  | N           | N           | N           |
+------+-------------+-------------+-------------+</code></pre><p>Use the <code>GRANT</code> command to modify user permissions:</p>
<pre><code class="language-sql">&gt; GRANT SELECT on *.* TO 'tux'@'localhost';
&gt; FLUSH PRIVILEGES;</code></pre><p>Verify your change:</p>
<pre><code class="language-sql">&gt; SELECT user,select_priv,insert_priv,update_priv FROM user WHERE user='tux';
+------+-------------+-------------+-------------+
| user | select_priv | insert_priv | update_priv |
+------+-------------+-------------+-------------+
| tux  | Y           | N           | N           |
+------+-------------+-------------+-------------+</code></pre><p>User <code>tux</code> now has privileges to select records from all tables.</p>
<h2 id="create-a-custom-database">Create a custom database</h2>
<p>So far, you've interacted just with the default databases. Most people rarely interact much with the default databases outside of user management. Usually, you create a database and populate it with tables full of custom data.</p>
<h3 id="create-a-mariadb-database">Create a MariaDB database</h3>
<p>You may already be able to guess how to create a new database in MariaDB. It's a lot like creating a new user:</p>
<pre><code class="language-sql">&gt; CREATE DATABASE example;
Query OK, 1 row affected (0.000 sec)
&gt; SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| example            |
[...]</code></pre><p>Make this new database your active one with the <code>use</code> command:</p>
<pre><code class="language-sql">&gt; use example;</code></pre><h3 id="create-a-table">Create a table</h3>
<p>Creating a table is more complex than creating a database because you must define column headings. MariaDB provides many convenience functions for you to use when creating columns, including data type definitions, automatic incrementing options, constraints to avoid empty values, automated timestamps, and more.</p>
<p>Here's a simple table to describe a set of users:</p>
<pre><code class="language-sql">&gt; CREATE table IF NOT EXISTS member (
    -&gt; id INT auto_increment PRIMARY KEY,
    -&gt; name varchar(128) NOT NULL,
    -&gt; startdate TIMESTAMP DEFAULT CURRENT_TIMESTAMP);
Query OK, 0 rows affected (0.030 sec)</code></pre><p>This table provides a unique identifier to each row by using an auto-increment function. It contains a field for a user's name, which cannot be empty (or <code>null</code>), and generates a timestamp when the record is created.</p>
<p>Populate this table with some sample data using the <code>INSERT</code> SQL keyword:</p>
<pre><code class="language-sql">&gt; INSERT INTO member (name) VALUES ('Alice');
Query OK, 1 row affected (0.011 sec)
&gt; INSERT INTO member (name) VALUES ('Bob');
Query OK, 1 row affected (0.011 sec)
&gt; INSERT INTO member (name) VALUES ('Carol');
Query OK, 1 row affected (0.011 sec)
&gt; INSERT INTO member (name) VALUES ('David');
Query OK, 1 row affected (0.011 sec)</code></pre><p>Verify the data in the table:</p>
<pre><code class="language-sql">&gt; SELECT * FROM member;
+----+-------+---------------------+
| id | name  | startdate           |
+----+-------+---------------------+
|  1 | Alice | 2020-10-03 15:25:06 |
|  2 | Bob   | 2020-10-03 15:26:43 |
|  3 | Carol | 2020-10-03 15:26:46 |
|  4 | David | 2020-10-03 15:26:51 |
+----+-------+---------------------+
4 rows in set (0.000 sec)</code></pre><h3 id="add-multiple-rows-at-once">Add multiple rows at once</h3>
<p>Now create a second table:</p>
<pre><code class="language-sql">&gt; CREATE table IF NOT EXISTS linux (
    -&gt; id INT auto_increment PRIMARY KEY,
    -&gt; distro varchar(128) NOT NULL,
Query OK, 0 rows affected (0.030 sec)</code></pre><p>Populate it with some sample data, this time using a little <code>VALUES</code> shortcut so you can add multiple rows in one command. The <code>VALUES</code> keyword expects a list in parentheses, but it can take multiple lists separated by commas:</p>
<pre><code class="language-sql">&gt; INSERT INTO linux (distro)
 -&gt; VALUES ('Slackware'), ('RHEL'),('Fedora'),('Debian');
Query OK, 4 rows affected (0.011 sec)
Records: 4  Duplicates: 0  Warnings: 0
&gt; SELECT * FROM linux;
+----+-----------+
| id | distro    |
+----+-----------+
|  1 | Slackware |
|  2 | RHEL      |
|  3 | Fedora    |
|  4 | Debian    |
+----+-----------+</code></pre><h2 id="create-relationships-between-tables">Create relationships between tables</h2>
<p>You now have two tables, but there's no relationship between them. They each contain independent data, but you might need to associate a member of the first table to a specific item listed in the second.</p>
<p>To do that, you can create a new column for the first table that corresponds to something in the second. Because both tables were designed with unique identifiers (the auto-incrementing <code>id</code> field), the easiest way to connect them is to use the <code>id</code> field of one as a selector for the other.</p>
<p>Create a new column in the first table to represent a value in the second table:</p>
<pre><code class="language-sql">&gt; ALTER TABLE member ADD COLUMN (os INT);
Query OK, 0 rows affected (0.012 sec)
Records: 0  Duplicates: 0  Warnings: 0
&gt; DESCRIBE member;
DESCRIBE member;
+-----------+--------------+------+-----+---------+------+
| Field     | Type         | Null | Key | Default | Extra|
+-----------+--------------+------+-----+---------+------+
| id        | int(11)      | NO   | PRI | NULL    | auto_|
| name      | varchar(128) | NO   |     | NULL    |      |
| startdate | timestamp    | NO   |     | cur[...]|      |
| os        | int(11)      | YES  |     | NULL    |      |
+-----------+--------------+------+-----+---------+------+</code></pre><p>Using the unique IDs of the <code>linux</code> table, assign a distribution to each member. Because the records already exist, use the <code>UPDATE</code> SQL keyword rather than <code>INSERT</code>. Specifically, you want to select one row and then update the value of one column. Syntactically, this is expressed a little in reverse, with the update happening first and the selection matching last:</p>
<pre><code class="language-sql">&gt; UPDATE member SET os=1 WHERE name='Alice';
Query OK, 1 row affected (0.007 sec)
Rows matched: 1  Changed: 1  Warnings: 0</code></pre><p>Repeat this process for the other names in the <code>member</code> table to populate it with data. For variety, assign three different distributions across the four rows (doubling up on one).</p>
<h3 id="join-tables">Join tables</h3>
<p>Now that these two tables relate to one another, you can use SQL to display the associated data. There are many kinds of joins in databases, and you can try them all once you know the basics. Here's a basic join to correlate the values found in the <code>os</code> field of the <code>member</code> table to the <code>id</code> field of the <code>linux</code> table:</p>
<pre><code class="language-sql">SELECT * FROM member JOIN linux ON member.os=linux.id;
+----+-------+---------------------+------+----+-----------+
| id | name  | startdate           | os   | id | distro    |
+----+-------+---------------------+------+----+-----------+
|  1 | Alice | 2020-10-03 15:25:06 |    1 |  1 | Slackware |
|  2 | Bob   | 2020-10-03 15:26:43 |    3 |  3 | Fedora    |
|  4 | David | 2020-10-03 15:26:51 |    3 |  3 | Fedora    |
|  3 | Carol | 2020-10-03 15:26:46 |    4 |  4 | Debian    |
+----+-------+---------------------+------+----+-----------+
4 rows in set (0.000 sec)</code></pre><p>The <code>os</code> and <code>id</code> fields form the join.</p>
<p>In a graphical application, you can imagine that the <code>os</code> field might be set by a drop-down menu, the values for which are drawn from the contents of the <code>distro</code> field of the <code>linux</code> table. By using separate tables for unique but related sets of data, you ensure the consistency and validity of data, and thanks to SQL, you can associate them dynamically later.</p>
<h2 id="learn-more"><a href="https://opensource.com/downloads/mariadb-mysql-cheat-sheet">Download the MariaDB and MySQL cheat sheet</a></h2>
<p>MariaDB is an enterprise-grade database. It's designed and proven to be a robust, powerful, and fast database engine. Learning it is a great step toward using it to do things like managing web applications or programming language libraries. As a quick reference when you're using MariaDB, <a href="https://opensource.com/downloads/mariadb-mysql-cheat-sheet">download our MariaDB and MySQL cheat sheet</a>. </p>
