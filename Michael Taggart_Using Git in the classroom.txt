<p>In my advanced programming classes I've discovered that middle school students are capable of far more complex operations than we often suspect. In many cases, they're wholly capable of using industry-standard tools to produce remarkable work.</p>
<!--break--><p>In our class, one of the central tools for our work is <a href="https://git-scm.com/" target="_blank">Git</a>. It's a version control system designed by none other than <a href="https://en.wikipedia.org/wiki/Linus_Torvalds" target="_blank">Linus Torvalds</a>, the creator of the Linux kernel. Looking at the front page of the Git site should give you an idea how well-established it is in the industry. It has as much place in a computer science class as any other tool we might use—even at the middle school level.</p>
<p>We'll talk about Git's features more deeply in later sections, but the big kahuna is this: syncing to a remote repository and tracking changes of code by multiple users. This is the technology that enables <a href="https://github.com" target="_blank">GitHub</a> to be the incredible library of open source projects that it is. The same features that enable GitHub to be GitHub also allow teachers to manage multiple students' projects easily, and even handle collaborative work fairly and clearly.</p>
<p>GUI helpers for Git abound (GitHub even offers <a href="https://desktop.github.com/" target="_blank">their own</a>), but we have found that the plain-ol' command line is easiest to use. What's more, getting comfortable with Git and the command line in middle school pays dividends in the long run.</p>
<h2>Getting started with Git</h2>
<p>On Mac and Windows, installing the desktop app linked above is an easy way to get started. This will also install the command line tools that undergird the Git system. Again, I <strong>highly</strong> recommend using the command line over GUI tools. Here's why.</p>
<h2>Embrace the keyboard</h2>
<p>Students <strong>love</strong> learning the command line. Like programming languages, the command line seems like a secret power, like arcane knowledge. With proper scaffolding, it can be as simple as teaching any other user interface.</p>
<p>Yes, the command prompt can be scary:</p>
<p><code class="language-bash">$ you-could-enter-anything-here </code></p>
<p>It's the computer version of looking long into the abyss—it looks into you. But consider this: the interface language is a human language. Just words. That's something that, in truth, is <em>more</em> familiar than the arcane iconography employed by most UIs. With a little practice, I predict you and your students will have terminal windows open at all times and use them whenever you can. There's an old saying about GUIs: they make simple things simple and complex things impossible. Once you've learned <code>touch</code>, <code>mkdir</code>, and <code>cd</code>, see whether the GUI or CLI takes more time to get things done.</p>
<p>And not for nothing, but mastering the CLI provides a sense of accomplishment for students. They're proud to know it, and that kind of self-esteem can go a long way.</p>
<p>The steps in this article will all be done using CLI examples.</p>
<h2>Your first repo</h2>
<p>Time for the Git equivalent of <em>Hello, world!</em> Let's make a new directory to use as our Git repository:</p>
<p><code class="language-bash">mkdir gitdemo</code><br /><code class="language-bash">cd gitdemo</code></p>
<p>Now we're inside our new folder. Time to make it a proper Git repo:</p>
<p><code class="language-bash">git init </code></p>
<p>You'll see Initialized empty Git repository in <code>/path/to/your/repo/.git/</code>. What's that <code>.git</code>? If you list all files in your directory (<code>ls -a</code>), you'll see a new hidden <code>.git/</code> directory. That's where Git stores the information about this new repository. Time to add some files.</p>
<p><code class="language-bash">touch new.txt</code><br /><code class="language-bash">echo "Hello, World!" &gt; new.txt </code></p>
<p>You'll have a new file, <code>new.txt</code> that now has one line of text in it: <em>Hello, world!</em> Pretty cool how we could do that from the command line without opening a text editor, huh?</p>
<p>But this isn't just any old folder; it's <strong>Git repository</strong>! Git has tracked that we have a new file. Enter the following command:</p>
<p><code class="language-bash">git status </code></p>
<p>We'll get back the following:</p>
<pre class="language-bash">
On branch master

Initial commit

Untracked files:
(use "git add <file>..." to include in what will be committed)

new.txt

nothing added to commit but untracked files present (use "git add" to track)</file></pre><p>Here's a point-by-point translation:</p>
<ul><li>We can have multiple versions of this folder, and you're looking at one called <em>master</em>.</li>
<li>No commits (saves) have been made yet.</li>
<li>I see some files in this folder you haven't told me to care about yet. Here they are...</li>
</ul><p>Committing in Git is a little more nuanced than just hitting save. Git lets us choose what files we track inside a repository. It also lets us stage files to be saved into its history, rather than just saving everything all the time. This might seem like a hassle, but over time the advantages of such control reveal themselves.</p>
<p>So before we commit <code>test.txt</code> to the ANNALS OF HISTORY, we have to <em>stage</em> it.</p>
<p><code class="language-bash">git add new.txt </code></p>
<p>No output after that, but running git <code>status</code> again yields:</p>
<pre class="language-bash">
On branch master

Initial commit

Changes to be committed:
(use "git rm --cached <file>..." to unstage)

new file: new.txt </file></pre><p>Okay, excellent. Git knows about our file now. Time to commit our changes to Git's history.</p>
<p><code class="language-bash">git commit -m "Add new.txt" </code></p>
<p>The <code>-m</code> flag provides a commit message. Such a message is required for all commits. If you don't provide one with the <code>-m</code> flag, Git will kick you into a command line text editor, which can be unnerving when you're not used to it. I recommend using the <code>-m</code> flag for now.</p>
<p>After the commit command, you'll see output like this:</p>
<pre class="language-bash">
[master (root-commit) c6c1180]
Add new.txt 1 file changed, 1 insertion(+)
create mode 100644 new.txt 
</pre><p>Translation:</p>
<ul><li>Our first commit! Also we're on the master branch. I've made a unique id for this commit.</li>
<li>Here's what changed in this commit.</li>
<li>I made one file.</li>
</ul><p>We're almost done! But most of your commits won't be initial commits, right? They'll be updates of repos you've already worked with. So let's make some changes.</p>
<p><code class="language-bash">echo "Foobar!" &gt;&gt; new.txt </code></p>
<p>This adds a new line (again, no text editor needed) to our <code>new.txt</code>. A quick run of <code>git status</code> reveals:</p>
<pre class="language-bash">
On branch master
Changes not staged for commit:
(use "git add <file>..." to update what will be committed)
(use "git checkout -- <file>..." to discard changes in working directory)

modified: new.txt 

no changes added to commit (use "git add" and/or "git commit -a") </file></file></pre><p>Look at that! <code>modified: new.txt</code>. Git sees changes to our file. If you're curious exactly <em>what</em> has changed, you can run <code>git diff new.txt</code>. That yields something like:</p>
<pre>
diff --git a/new.txt b/new.txt
index 8ab686e..d8eeaac 100644
--- a/new.txt
+++ b/new.txt @@ -1 +1,2 @@
Hello, World!
+Foobar! 
</pre><p>Notice the <code>+</code> around the added line. This seems like a reasonable change, so we're going to commit it. Don't forget to <code>git add new.txt</code> or <code>git add -A</code> to add that file or all files, respectively, to the staging area. Another call to <code>git commit</code> with an appropriate commit message gives us a total of two commits in our history. Go ahead and see what Git is tracking for you with <code>git log</code>.</p>
<p>Helpful, no? Git's functionality goes much deeper, but you've just gone through a fairly standard Git workflow for developers.</p>
<h2>Going remote</h2>
<p>Using Git to manage local projects is very helpful, but the technology really shines when paired with remote repositories like GitHub or Bitbucket. So let's go ahead and do that. I've chosen to use GitHub for this article, since that's the preeminent remote repository service right now. I do like Bitbucket for personal projects, but there's a serious benefit to being on the same platform as other developers. Think of it like social media for programmers.</p>
<p>You can set up a GitHub account for this if you like, but you won't need it, since I've already made a repo for you. You're going to connect to my remote, online version of the <code>gitdemo</code> repository and download my changes, which will then merge with yours.</p>
<p><strong>SECURITY NOTE:</strong> We're all friends here in the FOSS community. And we download code from other computers all the time. But what I'm about to ask you to do requires a certain amount of trust, and I don't think you should do so blindly. Please feel free to review this <a href="https://github.com/mttaggart/gitdemo" target="_blank">tiny repository</a> to assure yourself that I am not attempting to infect your machine with malicious code. Or any code, for that matter.</p>
<p>With that out of the way, let's set up your local repository to look at mine for upstream changes by entering:</p>
<p><code class="language-bash">git remote add upstream https://</code><code>github.com/mttaggart/gitdemo </code></p>
<p>No output means it worked. We're now ready to download (fetch) and merge the files on GitHub with your local repository. We can do both at the same time with <code>git pull</code>, or do both steps separately with <code>git fetch</code> and <code>git merge</code>. For simplicity's sake, let's pull:</p>
<p><code class="language-bash">git pull upstream master </code></p>
<p>Hey, what's that master thing? No worries; that just means we're on the <em>master</em> branch of the repository, the main branch.</p>
<p>I guess that'd be the trunk. Anyway, you see this output:</p>
<pre>
Merge made by the 'recursive' strategy.
README.md | 1 +
1 file changed, 1 insertion(+)
create mode 100644 README.md 
</pre><p>Looks like you have a new file there! Go check out <code>README.md</code> and see what's up. If you want a nice visualization of what just occurred, go ahead and run the following:</p>
<p><code class="language-bash">git log --graph</code> <code class="language-bash"># q quits, j and k scroll </code></p>
<p>You can see that there was a merge of two branches—remote and local, resulting in the current state of affairs.</p>
<p>If you and I were working together on this repository and you then made some changes you need to commit to our shared GitHub repository, you'd simply <code>git push</code> to upload your changes to GitHub, merging them with what was there previously.</p>
<p>"Great Taggart, now I can use Git. How does that make me a better teacher?" The secret is thinking outside of code.</p>
<h2>Git portfolios</h2>
<p>Lots of educators talk about meaning digital portfolios for students, but few actually pull the trick off. It's tough—how do you represent so many different documents? How do you keep track of what should and shouldn't be included? How do you ensure that the portfolio is built on a platform students will be able to access, even after they graduate or leave the school?</p>
<p>Git, Git, Git, HTML, and Git.</p>
<p>Imagine having students maintain a directory of documents (ideally Open Document Format, or even better, HTML) that they curate throughout their school career. Perhaps every quarter they make some decisions about what work they're proudest of. They add these files to a Git repository, commit, and push to a remote repo. It needn't be GitHub if privacy is a concern. Projects like <a href="https://gitlab.com" target="_blank">GitLab</a> allow schools to create their own Git remote repositories without relying on third parties.</p>
<p>With the tracking and versioning features that developers rely on for clean code, students get a nice timeline of their work history. They can even create <em>releases</em> of their portfolio at different points in the school career to demonstrate growth. I realize Git was never intended for this purpose, but I believe it to be a possible solution to the portfolio problem.</p>
<h2>Computer science with Git</h2>
<p>I run my middle school programming classes with Git/GitHub. I realize that GitHub has a <a href="https://classroom.github.com/" target="_blank">rather nice solution</a> for managing multiple student repos, but I decided to roll my own—in part because the school already uses a learning management system that can track assignments, and in part to force myself to learn Git well enough that I'd be confident to use it to run the class.</p>
<p><strong>Here's how I did it:</strong></p>
<h3>1. Create the upstream repo</h3>
<p><em>Upstream</em> is the repository where you submit <em>stub</em> or starter code for your students to work from. I also put my Markdown-formatted lesson objectives in this repo. Each lesson gets its own folder. Every week, a new commit brings a new project.</p>
<h3>2. Set up student accounts</h3>
<p>GitHub's <a href="https://classroom.github.com/" target="_blank" title="Classroom for GitHub">Classroom</a> solution allows educators to create private repositories for student work—a major advantage if privacy is a concern. Alternatively, you can just allow students to create personal public accounts on GitHub or Bitbucket. If the students own the account from the outset, they've gotten an early start on their career as developers. What's more, creating the account could be an opportunity to discuss online safety, digital citizenship, and intellectual property.</p>
<h3>3. Fork upstream</h3>
<p>After students create their accounts, they should <em>fork</em> your upstream repository.</p>
<p>Forking creates a personal copy of your code (open source is great, isn't it?) that students can change and extend on their own. You can add as little or as much to upstream as you like; students will take it and run from there.</p>
<h3>4. Clone/pull student repos</h3>
<p><code>git clone</code> grabs a remote repository and downloads a local copy to your computer, complete with the repo's commit history. I have a folder in which I've <code>git clone</code>d all of my students' repos. Then, using a little Bash script I wrote, I pull all the repos as once, creating a combined log file of each student's last two commits. Their commits tell me what they've worked on since last I checked, so I know to go back and look at old lessons if they've improved their solutions.</p>
<h2>Git: Many problems, one solution</h2>
<p>Surely, dear educator, there is a tremendous learning curve to implementing something like Git into a classroom—many times that if considering Git as a portfolio tool. 'Twas always thus, though. No major change in education practice, even education technology practice, comes without considerable difficulty. As open source advocates <em>and</em> education advocates, we should strive for solutions that are effective, fair, and accessible. Although unfamiliar to the non-programmers, Git as a technology meets all three criteria. Its future is assuredly bright, and I hope you consider exploring its potential use in education.</p>
