<p>Both the <code>su</code> and the <code>sudo</code> commands allow users to perform system administration tasks that are not permitted for non-privileged users—that is, everyone but the root user. Some people prefer the <code>sudo</code> command: For example, <a href="https://opensource.com/users/seth" target="_blank">Seth Kenlon</a> recently published "<a href="https://opensource.com/article/22/5/use-sudo-linux">5 reasons to use sudo on Linux</a>", in which he extols its many virtues.</p>
<p>I, on the other hand, am partial to the <code>su</code> command and prefer it to <code>sudo</code> for most of the system administration work I do. In this article, I compare the two commands and explain why I prefer <code>su</code> over <code>sudo</code> but still use both.</p>
<p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">More Linux resources</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux commands cheat sheet">Linux commands cheat sheet</a></div>
              <div class="field__item"><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Advanced Linux commands cheat sheet">Advanced Linux commands cheat sheet</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Free online course: RHEL technical overview">Free online course: RHEL technical overview</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux networking cheat sheet">Linux networking cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="SELinux cheat sheet">SELinux cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux common commands cheat sheet">Linux common commands cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="What are Linux containers?">What are Linux containers?</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Register for your free Red Hat account">Register for your free Red Hat account</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Our latest Linux articles">Our latest Linux articles</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<h2>Historical perspective of sysadmins</h2>
<p>The <code>su</code> and <code>sudo</code> commands were designed for a different world. Early Unix computers required full-time system administrators, and they used the root account as their only administrative account. In this ancient world, the person entrusted with the root password would log in as root on a teletype machine or CRT terminal such as the DEC VT100, then perform the administrative tasks necessary to manage the Unix computer.</p>
<p>The root user would also have a non-root account for non-root activities such as writing documents and managing their personal email. There were usually many non-root user accounts on those computers, and none of those users needed total root access. A user might need to run one or two commands as root, but very infrequently. Many sysadmins log in as root to work as root and log out of our root sessions when finished. Some days require staying logged in as root all day long. Most sysadmins rarely use <code>sudo</code> because it requires typing more than necessary to run essential commands.</p>
<p>These tools both provide escalated privileges, but the way they do so is significantly different. This difference is due to the distinct use cases for which they were originally intended.</p>
<h2>sudo</h2>
<p>The original intent of <code>sudo</code> was to enable the root user to delegate to one or two non-root users access to one or two specific privileged commands they need regularly. The <code>sudo</code> command gives non-root users temporary access to the elevated privileges needed to perform tasks such as adding and deleting users, deleting files that belong to other users, installing new software, and generally any task required to administer a modern Linux host.</p>
<p>Allowing the users access to a frequently used command or two that requires elevated privileges saves the sysadmin a lot of requests from users and eliminates the wait time. The <code>sudo</code> command does not switch the user account to become root; most non-root users should never have full root access. In most cases, <code>sudo</code> lets a user issue one or two commands then allows the privilege escalation to expire. During this brief time interval, usually configured to be 5 minutes, the user may perform any necessary administrative tasks that require elevated privileges. Users who need to continue working with elevated privileges but are not ready to issue another task-related command can run the <code>sudo -v</code> command to revalidate the credentials and extend the time for another 5 minutes.</p>
<p>Using the <code>sudo</code> command does have the side effect of generating log entries of commands used by non-root users, along with their IDs. The logs can facilitate a problem-related postmortem to determine when users need more training. (You thought I was going to say something like "assign blame," didn't you?)</p>
<h2>su</h2>
<p>The <code>su</code> command is intended to allow a non-root user to elevate their privilege level to that of root—in fact, the non-root user becomes the root user. The only requirement is that the user know the root password. There are no limits on this because the user is now logged in as root.</p>
<p>No time limit is placed on the privilege escalation provided by the <b>su</b> command. The user can work as root for as long as necessary without needing to re-authenticate. When finished, the user can issue the <b>exit</b> command to revert from root back to their own non-root account.</p>
<h2>Controversy and change</h2>
<p>There has been some recent disagreement about the uses of <code>su</code> versus <code>sudo</code>.</p>
<blockquote><p><i>Real [Sysadmins] don't use sudo.</i> —Paul Venezia</p>
</blockquote>
<p>Venezia contends in his <a href="http://www.infoworld.com/t/unix/nine-traits-the-veteran-unix-admin-276?page=0,0&amp;source=fssr" target="_blank">InfoWorld article</a> that <code>sudo</code> is used as an unnecessary prop for many people who act as sysadmins. He does not spend much time defending or explaining this position; he just states it as a fact. And I agree with him—for sysadmins. We don't need the training wheels to do our jobs. In fact, they get in the way.</p>
<p>However,</p>
<blockquote><p><i>The times they are a'changin.'</i> —Bob Dylan</p>
</blockquote>
<p>Dylan was correct, although he was not singing about computers. The way computers are administered has changed significantly since the advent of the one-person, one-computer era. In many environments, the user of a computer is also its administrator. This makes it necessary to provide some access to the powers of root for those users.</p>
<p>Some modern distributions, such as Ubuntu and its derivatives, are configured to use the <code>sudo</code> command exclusively for privileged tasks. In those distros, it is impossible to log in directly as the root user or even to <code>su</code> to root, so the <code>sudo</code> command is required to allow non-root users any access to root privileges. In this environment, all system administrative tasks are performed using <code>sudo</code>.</p>
<p>This configuration is possible by locking the root account and adding the regular user account(s) to the wheel group. This configuration can be circumvented easily. Try a little experiment on any Ubuntu host or VM. Let me stipulate the setup here so you can reproduce it if you wish. I installed Ubuntu 16.04 LTS1 and installed it in a VM using VirtualBox. During the installation, I created a non-root user, student, with a simple password for this experiment.</p>
<p>Log in as the user student and open a terminal session. Look at the entry for root in the <code>/etc/shadow</code> file, where the encrypted passwords are stored.</p>
<pre>
<code class="language-bash">student@ubuntu1:~$ cat /etc/shadow
cat: /etc/shadow: Permission denied</code></pre><p>Permission is denied, so we cannot look at the <code>/etc/shadow</code> file. This is common to all distributions to prevent non-privileged users from seeing and accessing the encrypted passwords, which would make it possible to use common hacking tools to crack those passwords.</p>
<p>Now let's try to <code>su -</code> to root.</p>
<pre>
<code class="language-bash">student@ubuntu1:~$ su -
Password: &lt;Enter root password – but there isn't one&gt;
su: Authentication failure</code></pre><p>This fails because the root account has no password and is locked out. Use the <code>sudo</code> command to look at the <code>/etc/shadow</code> file.</p>
<pre>
<code class="language-bash">student@ubuntu1:~$ sudo cat /etc/shadow
[sudo] password for student: &lt;enter the student password&gt;
root:!:17595:0:99999:7:::
&lt;snip&gt;
student:$6$tUB/y2dt$A5ML1UEdcL4tsGMiq3KOwfMkbtk3WecMroKN/:17597:0:99999:7:::
&lt;snip&gt;</code></pre><p>I have truncated the results to show only the entry for the root and student users. I have also shortened the encrypted password so the entry will fit on a single line. The fields are separated by colons (<code>:</code>) and the second field is the password. Notice that the password field for root is a <i>bang</i>, known to the rest of the world as an exclamation point (<code>!</code>). This indicates that the account is locked and that it cannot be used.</p>
<p>Now all you need to do to use the root account as a proper sysadmin is to set up a password for the root account.</p>
<pre>
<code class="language-bash">student@ubuntu1:~$ sudo su -
[sudo] password for student: &lt;Enter password for student&gt;
root@ubuntu1:~# passwd root
Enter new UNIX password: &lt;Enter new root password&gt;
Retype new UNIX password: &lt;Re-enter new root password&gt;
passwd: password updated successfully
root@ubuntu1:~#</code></pre><p>Now you can log in directly on a console as root or <code>su</code> directly to root instead of using <code>sudo</code> for each command. Of course, you could just use <code>sudo su -</code> every time you want to log in as root, but why bother?</p>
<p>Please do not misunderstand me. Distributions like Ubuntu and their up- and downstream relatives are perfectly fine, and I have used several of them over the years. When using Ubuntu and related distros, one of the first things I do is set a root password so that I can log in directly as root. Other distributions, like Fedora and its relatives, now provide some interesting choices during installation. The first Fedora release where I noticed this was Fedora 34, which I have installed many times while writing an upcoming book.</p>
<p>One of those installation options can be found on the page to set the root password. The new option allows the user to choose "Lock root account" in the way an Ubuntu root account is locked. There is also an option on this page that allows remote SSH login to this host as root using a password, but that only works when the root account is unlocked. The second option is on the page that allows the creation of a non-root user account. One of the options on this page is "Make this user administrator." When this option is checked, the user ID is added to a special group called the wheel group, which authorizes members of that group to use the <code>sudo</code> command. Fedora 36 even mentions the wheel group in the description of that checkbox.</p>
<p>More than one non-root user can be set as an administrator. Anyone designated as an administrator using this method can use the <code>sudo</code> command to perform all administrative tasks on a Linux computer. Linux only allows the creation of one non-root user during installation, so other new users can be added to the wheel group when created. Existing users can be added to the wheel group by the root user or another administrator directly by using a text editor or the <code>usermod</code> command.</p>
<p>In most cases, today's administrators need to do only a few essential tasks such as adding a new printer, installing updates or new software, or deleting software that is no longer needed. These GUI tools require a root or administrative password and will accept the password from a user designated as an administrator.</p>
<h2>How I use su and sudo on Linux</h2>
<p>I use both <code>su</code> and <code>sudo</code>. They each have an important place in my sysadmin toolbox.</p>
<p>I can't lock the root account because I need to use it to run my <a href="https://opensource.com/article/20/10/first-day-ansible">Ansible</a> playbooks and the <a href="https://opensource.com/article/17/1/rsync-backup-linux">rsbu</a> Bash program I wrote to perform backups. Both of these need to be run as root, and so do several other administrative Bash scripts I have written. I use the <code>su</code> command to switch users to the root user so I can perform these and many other common tasks. Elevating my privileges to root using <code>su</code> is especially helpful when performing problem determination and resolution. I really don't want a <code>sudo</code> session timing out on me while I am in the middle of my thought process.</p>
<p>I use the <code>sudo</code> command for tasks that need root privilege when a non-root user needs to perform them. I set the non-root account up in the sudoers file with access to only those one or two commands needed to complete the tasks. I also use <code>sudo</code> myself when I need to run only one or two quick commands with escalated privileges.</p>
<p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">More for sysadmins</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://www.redhat.com/sysadmin/?intcmp=7013a000002CxqpAAC" data-analytics-category="resource list" data-analytics-text="Enable Sysadmin blog">Enable Sysadmin blog</a></div>
              <div class="field__item"><a href="https://go.redhat.com/automated-enterprise-ebook-20180920?intcmp=7013a000002CxqpAAC" data-analytics-category="resource list" data-analytics-text="The Automated Enterprise: A guide to managing IT with automation">The Automated Enterprise: A guide to managing IT with automation</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/ansible-quickstart?intcmp=7013a000002CxqpAAC" data-analytics-category="resource list" data-analytics-text="eBook: Ansible automation for Sysadmins">eBook: Ansible automation for Sysadmins</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/engage/system-administrator-guide-s-202107300146?intcmp=7013a000002CxqpAAC" data-analytics-category="resource list" data-analytics-text="Tales from the field: A system administrator's guide to IT automation">Tales from the field: A system administrator's guide to IT automation</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/kubernetes-sysadmin?intcmp=7013a000002CxqpAAC" data-analytics-category="resource list" data-analytics-text="eBook: A guide to Kubernetes for SREs and sysadmins">eBook: A guide to Kubernetes for SREs and sysadmins</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Register for your free Red Hat account">Register for your free Red Hat account</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/sysadmin?intcmp=7013a000002CxqpAAC" data-analytics-category="resource list" data-analytics-text="Latest sysadmin articles">Latest sysadmin articles</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<h2>Conclusions</h2>
<p>The tools you use don't matter nearly as much as getting the job done. What difference does it make if you use vim or Emacs, systemd or SystemV, RPM or DEB, <code>sudo</code> or <code>su</code>? The bottom line here is that you should use the tools with which you are most comfortable and that work best for you. One of the greatest strengths of Linux and open source is that there are usually many options available for each task we need to accomplish.</p>
<p>Both <code>su </code>and <code>sudo</code> have strengths, and both can be secure when applied properly for their intended use cases. I choose to use both <code>su</code> and <code>sudo</code> mostly in their historical roles because that works for me. I prefer <code>su</code> for most of my own work because it works best for me and my workflow.</p>
<p>Share how you prefer to work in the comments!</p>
<hr /><p>This article is taken from Chapter 19 of my book <i>The Linux Philosophy for Sysadmins</i> (Apress, 2018) and is republished with permission.</p>
