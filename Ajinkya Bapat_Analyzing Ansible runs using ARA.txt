<p><a href="https://www.ansible.com/" target="_blank">Ansible</a> is a versatile platform that has become popular for managing servers and server configurations. Today, Ansible is used heavily to deploy and test through continuous integration (CI).</p>
<p>In the world of automated continuous integration, it’s not uncommon to have hundreds, if not thousands, of jobs running every day for testing, building, compiling, deploying, and more.</p>
<h2>The Ansible Run Analysis (ARA) tool</h2>
<p>Ansible runs generate a large amount of console data, and keeping up with high volumes of Ansible output in the context of CI is challenging. The Ansible Run Analysis (ARA) tool makes this verbose output readable and more representative of the job status and debug information. ARA organizes recorded playbook data so you can search and find what you’re interested in as quickly and as easily as possible.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on Ansible</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/downloads/ansible-quickstart?intcmp=701f2000000h4RcAAI">A quickstart guide to Ansible</a></li>
<li><a href="https://opensource.com/downloads/ansible-cheat-sheet?intcmp=701f2000000h4RcAAI">Ansible cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/do007-ansible-essentials-simplicity-automation-technical-overview?intcmp=701f2000000h4RcAAI">Free online course: Ansible essentials</a></li>
<li><a href="https://docs.ansible.com/ansible/latest/intro_installation.html?intcmp=701f2000000h4RcAAI">Download and install Ansible</a></li>
<li><a href="https://go.redhat.com/automated-enterprise-ebook-20180920?intcmp=701f2000000h4RcAAI">eBook: The automated enterprise</a></li>
<li><a href="https://www.ansible.com/resources/ebooks/ansible-for-devops?intcmp=701f2000000h4RcAAI">eBook: Ansible for DevOps</a></li>
<li><a href="https://www.ansible.com/resources/ebooks?intcmp=701f2000000h4RcAAI">Free Ansible eBooks</a></li>
<li><a href="https://opensource.com/tags/ansible?intcmp=701f2000000h4RcAAI">Latest Ansible articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>Note that ARA doesn't run your playbooks for you; rather, it integrates with Ansible as a callback plugin wherever it is. A callback plugin enables adding new behaviors to Ansible when responding to events. It can perform custom actions in response to Ansible events such as a play starting or a task completing on a host.</p>
<p>Compared to <a href="https://www.ansible.com/products/awx-project" target="_blank">AWX</a> and <a href="https://www.ansible.com/products/tower" target="_blank">Tower</a>, which are tools that control the entire workflow, with features like inventory management, playbook execution, editing features, and more, the scope of ARA is comparatively narrow: It records data and provides an intuitive interface. It is a relatively simple application that is easy to install and configure.</p>
<h3>Installation</h3>
<p>There are two ways to install ARA on your system:</p>
<ul><li>Using the Ansible role hosted on your <a href="https://github.com/AjinkyaBapat/Ansible-Run-Analyser" target="_blank">GitHub account</a>. Clone the repo and do:</li>
</ul><pre>
<code class="language-text">ansible-playbook Playbook.yml</code></pre><p>If the playbook run is successful, you will get:</p>
<pre>
<code class="language-text">TASK [ara : Display ara UI URL] ************************
   ok: [localhost] =&gt; {}
   "msg": "Access playbook records at http://YOUR_IP:9191" </code></pre><p>Note: It picks the IP address from <code>ansible_default_ipv4</code> fact gathered by Ansible. If there is no such fact gathered, replace it with your IP in <code>main.yml</code> file in the <code>roles/ara/tasks/</code> folder.</p>
<ul><li>ARA is an open source project available on <a href="https://github.com/dmsimard/ara" target="_blank">GitHub</a> under the Apache v2 license. Installation instructions are in the Quickstart chapter. The <a href="http://ara.readthedocs.io/en/latest/" target="_blank">documentation</a> and <a href="http://ara.readthedocs.io/en/latest/faq.html" target="_blank">FAQs</a> are available on <a href="http://ara.readthedocs.io/en/latest/" target="_blank">readthedocs.io</a>.</li>
</ul><h3>What can ARA do?</h3>
<p>The image below shows the ARA landing page launched from the browser:</p>
<p></p>
<article class="media media--type-image media--view-mode-full" title="ara landing page"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/life-uploads/ara_landing_page.png" width="1298" height="637" alt="ara landing page" title="ara landing page" /></div>
      
  </article><p>It provides summaries of task results per host or per playbook:</p>
<p><article class="media media--type-image media--view-mode-full" title="task summaries"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/life-uploads/task_summaries.png" width="1301" height="641" alt="task summaries" title="task summaries" /></div>
      
  </article></p>
<p>It allows you to filter task results by playbook, play, host, task, or status:</p>
<p><font color="#333333"><font size="3"></font></font></p>
<article class="media media--type-image media--view-mode-full" title="playbook runs filtered by hosts"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/life-uploads/playbook_filtered_by_hosts.png" width="1301" height="638" alt="playbook runs filtered by hosts" title="playbook runs filtered by hosts" /></div>
      
  </article><p></p>
<p>With ARA, you can easily drill down from the summary view to find the results you’re interested in, whether it’s a particular host or a specific task:</p>
<p><article class="media media--type-image media--view-mode-full" title="summary of each task"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/life-uploads/summary_of_each_task.png" width="1301" height="637" alt="summary of each task" title="summary of each task" /></div>
      
  </article></p>
<p>ARA supports recording and viewing multiple runs in the same database.</p>
<p><article class="media media--type-image media--view-mode-full" title="show gathered facts"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/life-uploads/showing_gathered_facts.png" width="1301" height="636" alt="show gathered facts" title="show gathered facts" /></div>
      
  </article></p>
<h3>Wrapping up</h3>
<p>ARA is a useful resource that has helped me get more out of Ansible run logs and outputs. I highly recommend it to all Ansible ninjas out there.</p>
<p>Feel free to share this, and please let me know about your experience using ARA in the comments.</p>
<hr /><p>
<strong>[See our related story, <a href="https://opensource.com/article/18/2/tips-success-when-getting-started-ansible" target="_blank">Tips for success when getting started with Ansible</a>.]</strong></p>
