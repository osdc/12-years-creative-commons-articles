<p><a href="https://www.freedos.org/" target="_blank">FreeDOS</a>, the open source implementation of DOS, provides a lightweight operating system for running legacy applications on modern hardware (or in an emulator) and for updating hardware vendor fails with a Linux-compatible firmware flasher. Getting familiar with FreeDOS is not only a fun throwback to the computing days of the past, it's an investment into gaining useful computing skills. In this article, I'll look at some of the essential commands you need to know to work on a FreeDOS system.</p>
<h2 id="essential-directory-and-file-commands">Essential directory and file commands</h2>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Great Content</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=7016000000127cYAAQ">Free online course: RHEL technical overview</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=7016000000127cYAAQ">Learn Advanced Linux Commands</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Download Cheat Sheets</a></li>
<li><a href="https://opensource.com/alternatives?intcmp=7016000000127cYAAQ">Find an Open Source Alternative</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=7016000000127cYAAQ">Read Top Linux Content</a></li>
<li><a href="https://opensource.com/resources?intcmp=7016000000127cYAAQ">Check out open source resources</a></li>
</ul></div>
</div>
</div>
</div>
<p>FreeDOS uses directories to organize files on a hard drive. That means you need to use directory commands to create a structure to store your files and find the files you've stored there. The commands you need to manage your directory structure are relatively few:</p>
<ul><li><code>MD</code> (or <code>MKDIR</code>) creates a new directory or subdirectory.</li>
<li><code>RD</code> (or <code>RMDIR</code>) removes (or deletes) a directory or subdirectory.</li>
<li><code>CD</code> (or <code>CHDIR</code>) changes from the current working directory to another directory.</li>
<li><code>DELTREE</code> erases a directory, including any files or subdirectories it contains.</li>
<li><code>DIR</code> lists the contents of the current working directory.</li>
</ul><p>Because working with directories is central to what FreeDOS does, all of these (except DELTREE) are internal commands contained within COMMAND.COM. Therefore, they are loaded into RAM and ready for use whenever you boot (even from a boot disk). The first three commands have two versions: a two-letter short name and a long name. There is no difference in practice, so I'll use the short form in this article.</p>
<h2 id="make-a-directory-with-md">Make a directory with MD</h2>
<p>FreeDOS's <code>MD</code> command creates a new directory or subdirectory. (Actually, since the <em>root</em> is the main directory, all directories are technically subdirectories, so I'll refer to <em>subdirectories</em> in all examples.) An optional argument is the path to the directory you want to create, but if no path is included, the subdirectory is created in the current working subdirectory.</p>
<p>For example, to create a subdirectory called <code>letters</code> in your current location:</p>
<pre><code class="language-bash">C:\HOME\&gt;MD LETTERS</code></pre><p>This creates the subdirectory <code>C:\letters</code>.</p>
<p>By including a path, you can create a subdirectory anywhere:</p>
<pre><code class="language-bash">C:\&gt;MD C:\HOME\LETTERS\LOVE</code></pre><p>This has the same result as moving into <code>C:\HOME\LETTERS</code> first and then creating a subdirectory there:</p>
<pre><code class="language-bash">C:\CD HOME\LETTERS
C:\HOME\LETTERS\&gt;MD LOVE
C:\HOME\LETTERS\&gt;DIR
LOVE</code></pre><p>A path specification cannot exceed 63 characters, including backslashes.</p>
<h2 id="remove-a-directory-with-rd">Remove a directory with RD</h2>
<p>FreeDOS's <code>RD</code> command removes a subdirectory. The subdirectory must be empty. If it contains files or other subdirectories, you get an error message. This has an optional path argument with the same syntax as <code>MD</code>.</p>
<p>You cannot remove your current working subdirectory. To do that, you must <code>CD</code> to the parent subdirectory and then remove the undesired subdirectory.</p>
<h2 id="delete-files-and-directories-with-deltree">Delete files and directories with DELTREE</h2>
<p>The <code>RD</code> command can be a little confusing because of safeguards FreeDOS builds into the command. That you cannot delete a subdirectory that has contents, for instance, is a safety measure. <code>DELTREE</code> is the solution.</p>
<p><code>DELTREE</code> deletes an entire subdirectory "tree" (a subdirectory), plus all of the files it contains, plus all of the subdirectories those contain, and all of the files <em>they</em> contain, and so on, all in one easy command. Sometimes it can be a little <em>too</em> easy because it can wipe out so much data so quickly. It ignores file attributes, so you can wipe out hidden, read-only, and system files without knowing it.</p>
<p>You can even wipe out multiple trees by specifying them in the command. This would wipe out both of these subdirectories in one command:</p>
<pre><code class="language-bash">C:\&gt;DELTREE C:\FOO C:\BAR</code></pre><p>This is one of those commands where you really ought to think twice before you use it. It has its place, definitely. I can still remember how tedious it was to go into each subdirectory, delete the individual files, check each subdirectory for contents, delete each subdirectory one at a time, then jump up one level and repeat the process. <code>DELTREE</code> is a great timesaver when you need it. But I would never use it for ordinary maintenance because one false move can do so much damage.</p>
<h2 id="format-a-hard-drive">Format a hard drive</h2>
<p>The <code>FORMAT</code> command can also be used to prepare a blank hard drive to have files written to it. This formats the <code>D:</code> drive:</p>
<pre><code class="language-bash">C:\&gt;FORMAT D:</code></pre><h2 id="copy-files">Copy files</h2>
<p>The <code>COPY</code> command, as the name implies, copies files from one place to another. The required arguments are the file to be copied and the path and file to copy it to. Switches include:</p>
<ul><li><code>/Y</code> prevents a prompt when a file is being overwritten.</li>
<li><code>/-Y</code> requires a prompt when a file is being overwritten.</li>
<li><code>/V</code> verifies the contents of the copy.</li>
</ul><p>This copies the file <code>MYFILE.TXT</code> from the working directory on <code>C:</code> to the root directory of the <code>D:</code> drive and renames it <code>EXAMPLE.TXT</code>:</p>
<pre><code class="language-bash">C:\&gt;COPY MYFILE.TXT D:\EXAMPLE.TXT</code></pre><p>This copies the file <code>EXAMPLE.TXT</code> from the working directory on <code>C:</code> to the <code>C:\DOCS\</code> directory and then verifies the contents of the file to ensure that the copy is complete:</p>
<pre><code class="language-bash">C:\&gt;COPY EXAMPLE.TXT C:\DOCS\EXAMPLE.TXT /V</code></pre><p>You can also use the <code>COPY</code> command to combine and append files. This combines the two files <code>MYFILE1.TXT</code> and <code>MYFILE2.TXT</code> and places them in a new file called <code>MYFILE3.TXT</code>:</p>
<pre><code class="language-bash">C:\&gt;COPY MYFILE1.TXT+MYFILE2.TXT MYFILE3.TXT</code></pre><h2 id="copy-directories-with-xcopy">Copy directories with XCOPY</h2>
<p>The <code>XCOPY</code> command copies entire directories, along with all of their subdirectories and all of the files contained in those subdirectories. Arguments are the files and path to be copied and the destination to copy them to. Important switches are:</p>
<ul><li><code>/S</code> copies all files in the current directory and any subdirectory within it.</li>
<li><code>/E</code> copies subdirectories, even if they are empty. This option must be used with the <code>/S</code> option.</li>
<li><code>/V</code> verifies the copies that were made.</li>
</ul><p>This is a very powerful and useful command, particularly for backing up directories or an entire hard drive.</p>
<p>This command copies the entire contents of the directory <code>C:\DOCS</code>, including all subdirectories and their contents (except empty subdirectories) and places them on drive <code>D:</code> in the directory <code>D:\BACKUP\DOCS\</code>:</p>
<pre><code class="language-bash">C:\&gt;XCOPY C:\DOCS D:\BACKUP\DOCS\ /S</code></pre><h2 id="using-freedos">Using FreeDOS</h2>
<p>FreeDOS is a fun, lightweight, open source operating system. It provides lots of great utilities to enable you to get work done on it, whether you're using it to update the firmware of your motherboard or to give new life to an old computer. Learn the basics of FreeDOS. You might be surprised at how versatile it is.</p>
<hr /><p><em>Some of the information in this article was previously published in <a href="https://www.ahuka.com/dos-lessons-for-self-study-purposes/dos-lesson-8-format-copy-diskcopy-xcopy/" target="_blank">DOS lesson 8: Format; copy; diskcopy; Xcopy</a>; <a href="https://www.ahuka.com/dos-lessons-for-self-study-purposes/dos-lesson-10-directory-commands/" target="_blank">DOS lesson 10: Directory commands</a> (both CC BY-SA 4.0); and <a href="https://allaboutdosdirectoires.blogspot.com/" target="_blank">How to work with DOS</a>.</em></p>
