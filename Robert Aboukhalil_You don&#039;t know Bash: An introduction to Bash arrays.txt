<p id="the-weird-wondrous-world-of-bash-arrays">Although software engineers regularly use the command line for many aspects of development, arrays are likely one of the more obscure features of the command line (although not as obscure as the regex operator <code>=~</code>). But obscurity and questionable syntax aside, <a href="https://opensource.com/article/17/7/bash-prompt-tips-and-tricks">Bash</a> arrays can be very powerful.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Programming and development</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ&amp;src=programming_resource_menu4">Red Hat Developers Blog</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Programming cheat sheets</a></li>
<li><a href="https://www.redhat.com/en/services/training/learning-subscription?intcmp=7016000000127cYAAQ">Try for free: Red Hat Learning Subscription</a></li>
<li><a href="https://opensource.com/downloads/bash-programming-guide?intcmp=7016000000127cYAAQ">eBook: An introduction to programming with Bash</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/bash-shell-cheat-sheet?intcmp=7016000000127cYAAQ">Bash Shell Scripting Cheat Sheet</a></li>
<li><a href="https://developers.redhat.com/e-books/modernizing-enterprise-java?intcmp=7016000000127cYAAQ">eBook: Modernizing Enterprise Java</a></li>
</ul></div>
</div>
</div>
</div>

<h2 id="wait-but-why">Wait, but why?</h2>
<p>Writing about Bash is challenging because it's remarkably easy for an article to devolve into a manual that focuses on syntax oddities. Rest assured, however, the intent of this article is to avoid having you RTFM.</p>
<h3 id="a-real-actually-useful-example">A real (actually useful) example</h3>
<p>To that end, let's consider a real-world scenario and how Bash can help: You are leading a new effort at your company to evaluate and optimize the runtime of your internal data pipeline. As a first step, you want to do a parameter sweep to evaluate how well the pipeline makes use of threads. For the sake of simplicity, we'll treat the pipeline as a compiled C++ black box where the only parameter we can tweak is the number of threads reserved for data processing: <code>./pipeline --threads 4</code>.</p>
<h2 id="the-basics">The basics</h2>
<p>The first thing we'll do is define an array containing the values of the <code>--threads</code> parameter that we want to test:</p>
<pre><code class="language-bash">allThreads=(1 2 4 8 16 32 64 128)</code></pre><p class="language-bash">In this example, all the elements are numbers, but it need not be the case—arrays in Bash can contain both numbers and strings, e.g., <code>myArray=(1 2 "three" 4 "five")</code> is a valid expression. And just as with any other Bash variable, make sure to leave no spaces around the equal sign. Otherwise, Bash will treat the variable name as a program to execute, and the <code>=</code> as its first parameter!</p>
<p>Now that we've initialized the array, let's retrieve a few of its elements. You'll notice that simply doing <code>echo $allThreads</code> will output only the first element.</p>
<p>To understand why that is, let's take a step back and revisit how we usually output variables in Bash. Consider the following scenario:</p>
<pre><code class="language-bash">type="article"
echo "Found 42 $type"</code></pre><p>Say the variable <code>$type</code> is given to us as a singular noun and we want to add an <code>s</code> at the end of our sentence. We can't simply add an <code>s</code> to <code>$type</code> since that would turn it into a different variable, <code>$types</code>. And although we could utilize code contortions such as <code>echo "Found 42 "$type"s"</code>, the best way to solve this problem is to use curly braces: <code>echo "Found 42 ${type}s"</code>, which allows us to tell Bash where the name of a variable starts and ends (interestingly, this is the same syntax used in JavaScript/ES6 to inject variables and expressions in <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals" target="_blank">template literals</a>).</p>
<p>So as it turns out, although Bash variables don't generally require curly brackets, they are required for arrays. In turn, this allows us to specify the index to access, e.g., <code>echo ${allThreads[1]}</code> returns the second element of the array. Not including brackets, e.g.,<code>echo $allThreads[1]</code>, leads Bash to treat <code>[1]</code> as a string and output it as such.</p>
<p>Yes, Bash arrays have odd syntax, but at least they are zero-indexed, unlike some other languages (I'm looking at you, <code>R</code>).</p>
<h2 id="looping-through-arrays">Looping through arrays</h2>
<p>Although in the examples above we used integer indices in our arrays, let's consider two occasions when that won't be the case: First, if we wanted the <code>$i</code>-th element of the array, where <code>$i</code> is a variable containing the index of interest, we can retrieve that element using: <code>echo ${allThreads[$i]}</code>. Second, to output all the elements of an array, we replace the numeric index with the <code>@</code> symbol (you can think of <code>@</code> as standing for <code>all</code>): <code>echo ${allThreads[@]}</code>.</p>
<h3 id="looping-through-array-elements">Looping through array elements</h3>
<p>With that in mind, let's loop through <code>$allThreads</code> and launch the pipeline for each value of <code>--threads</code>:</p>
<pre><code class="language-bash">for t in ${allThreads[@]}; do
  ./pipeline --threads $t
done</code></pre><h3 class="language-bash">Looping through array indices</h3>
<p>Next, let's consider a slightly different approach. Rather than looping over array <em>elements</em>, we can loop over array <em>indices</em>:</p>
<pre><code class="language-bash">for i in ${!allThreads[@]}; do
  ./pipeline --threads ${allThreads[$i]}
done</code></pre><p class="language-bash">Let's break that down: As we saw above, <code>${allThreads[@]}</code> represents all the elements in our array. Adding an exclamation mark to make it <code>${!allThreads[@]}</code> will return the list of all array indices (in our case 0 to 7). In other words, the <code>for</code> loop is looping through all indices <code>$i</code> and reading the <code>$i</code>-th element from <code>$allThreads</code> to set the value of the <code>--threads</code> parameter.</p>
<p>This is much harsher on the eyes, so you may be wondering why I bother introducing it in the first place. That's because there are times where you need to know both the index and the value within a loop, e.g., if you want to ignore the first element of an array, using indices saves you from creating an additional variable that you then increment inside the loop.</p>
<h2 id="populating-arrays">Populating arrays</h2>
<p>So far, we've been able to launch the pipeline for each <code>--threads</code> of interest. Now, let's assume the output to our pipeline is the runtime in seconds. We would like to capture that output at each iteration and save it in another array so we can do various manipulations with it at the end.</p>
<h3 id="some-useful-syntax">Some useful syntax</h3>
<p>But before diving into the code, we need to introduce some more syntax. First, we need to be able to retrieve the output of a Bash command. To do so, use the following syntax: <code>output=$( ./my_script.sh )</code>, which will store the output of our commands into the variable <code>$output</code>.</p>
<p>The second bit of syntax we need is how to append the value we just retrieved to an array. The syntax to do that will look familiar:</p>
<pre><code class="language-bash">myArray+=( "newElement1" "newElement2" )</code></pre><h3 id="the-parameter-sweep">The parameter sweep</h3>
<p>Putting everything together, here is our script for launching our parameter sweep:</p>
<pre><code class="language-bash">allThreads=(1 2 4 8 16 32 64 128)
allRuntimes=()
for t in ${allThreads[@]}; do
  runtime=$(./pipeline --threads $t)
  allRuntimes+=( $runtime )
done</code></pre><p class="language-bash">And voilà!</p>
<h2 id="what-else-you-got">What else you got?</h2>
<p>In this article, we covered the scenario of using arrays for parameter sweeps. But I promise there are more reasons to use Bash arrays—here are two more examples.</p>
<h3 id="log-alerting">Log alerting</h3>
<p>In this scenario, your app is divided into modules, each with its own log file. We can write a cron job script to email the right person when there are signs of trouble in certain modules:<code class="prism  language-bash"> </code></p>
<pre><code class="language-bash"># List of logs and who should be notified of issues
logPaths=("api.log" "auth.log" "jenkins.log" "data.log")
logEmails=("jay@email" "emma@email" "jon@email" "sophia@email")

# Look for signs of trouble in each log
for i in ${!logPaths[@]};
do
  log=${logPaths[$i]}
  stakeholder=${logEmails[$i]}
  numErrors=$( tail -n 100 "$log" | grep "ERROR" | wc -l )

  # Warn stakeholders if recently saw &gt; 5 errors
  if [[ "$numErrors" -gt 5 ]];
  then
    emailRecipient="$stakeholder"
    emailSubject="WARNING: ${log} showing unusual levels of errors"
    emailBody="${numErrors} errors found in log ${log}"
    echo "$emailBody" | mailx -s "$emailSubject" "$emailRecipient"
  fi
done</code></pre><h3 id="api-queries">API queries</h3>
<p>Say you want to generate some analytics about which users comment the most on your Medium posts. Since we don't have direct database access, SQL is out of the question, but we can use APIs!</p>
<p>To avoid getting into a long discussion about API authentication and tokens, we'll instead use <a href="https://github.com/typicode/jsonplaceholder" target="_blank">JSONPlaceholder</a>, a public-facing API testing service, as our endpoint. Once we query each post and retrieve the emails of everyone who commented, we can append those emails to our results array:</p>
<pre><code class="language-bash">endpoint="https://jsonplaceholder.typicode.com/comments"
allEmails=()

# Query first 10 posts
for postId in {1..10};
do
  # Make API call to fetch emails of this posts's commenters
  response=$(curl "${endpoint}?postId=${postId}")

  # Use jq to parse the JSON response into an array
  allEmails+=( $( jq '.[].email' &lt;&lt;&lt; "$response" ) )
done</code></pre><p class="language-bash">Note here that I'm using the <a href="https://stedolan.github.io/jq/" target="_blank"><span style="color:#3498db;"><code>jq</code></span> tool</a> to parse JSON from the command line. The syntax of <code>jq</code> is beyond the scope of this article, but I highly recommend you look into it.</p>
<p>As you might imagine, there are countless other scenarios in which using Bash arrays can help, and I hope the examples outlined in this article have given you some food for thought. If you have other examples to share from your own work, please leave a comment below.</p>
<h2 id="but-wait-theres-more">But wait, there's more!</h2>
<p>Since we covered quite a bit of array syntax in this article, here's a summary of what we covered, along with some more advanced tricks we did not cover:</p>
<table><thead><tr><th>Syntax</th>
<th>Result</th>
</tr></thead><tbody><tr><td><code>arr=()</code></td>
<td>Create an empty array</td>
</tr><tr><td><code>arr=(1 2 3)</code></td>
<td>Initialize array</td>
</tr><tr><td><code>${arr[2]}</code></td>
<td>Retrieve third element</td>
</tr><tr><td><code>${arr[@]}</code></td>
<td>Retrieve all elements</td>
</tr><tr><td><code>${!arr[@]}</code></td>
<td>Retrieve array indices</td>
</tr><tr><td><code>${#arr[@]}</code></td>
<td>Calculate array size</td>
</tr><tr><td><code>arr[0]=3</code></td>
<td>Overwrite 1st element</td>
</tr><tr><td><code>arr+=(4)</code></td>
<td>Append value(s)</td>
</tr><tr><td><code>str=$(ls)</code></td>
<td>Save <code>ls</code> output as a string</td>
</tr><tr><td><code>arr=( $(ls) )</code></td>
<td>Save <code>ls</code> output as an array of files</td>
</tr><tr><td><code>${arr[@]:s:n}</code></td>
<td>Retrieve n elements <code>starting at index s</code></td>
</tr></tbody></table><h2 id="one-last-thought">One last thought</h2>
<p>As we've discovered, Bash arrays sure have strange syntax, but I hope this article convinced you that they are extremely powerful. Once you get the hang of the syntax, you'll find yourself using Bash arrays quite often.</p>
<h3 id="bash-or-python">Bash or Python?</h3>
<p>Which begs the question: <em>When should you use Bash arrays instead of other scripting languages such as Python?</em></p>
<p>To me, it all boils down to dependencies—if you can solve the problem at hand using only calls to command-line tools, you might as well use Bash. But for times when your script is part of a larger Python project, you might as well use Python.</p>
<p>For example, we could have turned to Python to implement the parameter sweep, but we would have ended up just writing a wrapper around Bash:</p>
<pre><code class="language-bash">import subprocess

all_threads = [1, 2, 4, 8, 16, 32, 64, 128]
all_runtimes = []

# Launch pipeline on each number of threads
for t in all_threads:
  cmd = './pipeline --threads {}'.format(t)

  # Use the subprocess module to fetch the return output
  p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
  output = p.communicate()[0]
  all_runtimes.append(output)</code></pre><p>Since there's no getting around the command line in this example, using Bash directly is preferable.</p>
<h3 id="time-for-a-shameless-plug">Time for a shameless plug</h3>
<p>This article is based on a talk I gave at <a href="https://conferences.oreilly.com/oscon/oscon-or" target="_blank">OSCON</a>, where I presented the live-coding workshop <a href="https://conferences.oreilly.com/oscon/oscon-or/public/schedule/detail/67166" target="_blank"><em>You Don't Know Bash</em></a>. No slides, no clickers—just me and the audience typing away at the command line, exploring the wondrous world of Bash.</p>
<p><em>This article originally appeared on <a href="https://medium.com/@robaboukhalil/the-weird-wondrous-world-of-bash-arrays-a86e5adf2c69" target="_blank">Medium</a> and is republished with permission.</em></p>
