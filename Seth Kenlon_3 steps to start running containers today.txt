<p>Whether you're interested in them as part of your job, for future job opportunities, or just out of interest in new technology, containers can seem pretty overwhelming to even an experienced systems administrator. So how do you actually get started with containers? And what's the path from containers to <a href="https://opensource.com/tags/kubernetes">Kubernetes</a>? Also, why is there a path from one to the other at all? As you might expect, the best place to start is the beginning.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Linux Containers</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/topics/containers/whats-a-linux-container?utm_campaign=containers&amp;intcmp=70160000000h1s6AAA">What are Linux containers?</a></li>
<li><a href="https://developers.redhat.com/blog/2016/01/13/a-practical-introduction-to-docker-container-terminology/?utm_campaign=containers&amp;intcmp=70160000000h1s6AAA">An introduction to container terminology</a></li>
<li><a href="https://opensource.com/downloads/containers-primer?utm_campaign=containers&amp;intcmp=70160000000h1s6AAA">Download: Containers Primer</a></li>
<li><a href="https://www.redhat.com/en/resources/oreilly-kubernetes-operators-automation-ebook?utm_campaign=containers&amp;intcmp=70160000000h1s6AAA">Kubernetes Operators: Automating the container orchestration platform</a></li>
<li><a href="https://www.redhat.com/en/engage/kubernetes-containers-architecture-s-201910240918?utm_campaign=containers&amp;intcmp=70160000000h1s6AAA">eBook: Kubernetes patterns for designing cloud-native apps</a></li>
<li><a href="https://www.redhat.com/en/topics/containers/what-is-kubernetes?utm_campaign=containers&amp;intcmp=70160000000h1s6AAA">What is Kubernetes?</a></li>
</ul></div>
</div>
</div>
</div>

<h2>1. Understanding containers</h2>
<p>On second thought, starting at the beginning arguably dates back to early BSD and their special chroot jails, so skip ahead to the middle instead.</p>
<p>Not so very long ago, the Linux kernel introduced <em>cgroups</em>, which enables you to "tag" processes with something called a <em>namespace</em>. When you group processes together into a namespace, those processes act as if nothing outside that namespace exists. It's as if you've put those processes into a sort of container. Of course, the container is virtual, and it exists inside your computer. It runs on the same kernel, RAM, and CPU that the rest of your operating system is running on, but you've contained the processes.</p>
<p>Pre-made containers get distributed with just what's necessary to run the application it contains. With a container engine, like <a href="https://www.redhat.com/sysadmin/podman-guides-2020">Podman</a>, Docker, or CRI-O, you can run a containerized application without installing it in any traditional sense. Container engines are often cross-platform, so even though containers run Linux, you can launch containers on Linux, macOS, or Windows.</p>
<p>More importantly, you can run more than one container of the same application when there's high demand for it.</p>
<p>Now that you know what a container is. The next step is to run one.</p>
<p><strong>[ Get the cheat sheet: <a href="https://enterprisersproject.com/cheat-sheet-what-s-difference-between-pod-cluster-and-container">What’s the difference between a pod, a cluster, and a container?</a> ]</strong></p>
<h2>2. Run a container</h2>
<p>Before running a container, you should have a reason for running a container. You can make up a reason, but it's helpful for that reason to interest you, so you're inspired actually to use the container you run. After all, running a container but never using the application it provides only proves that you're not noticing any failures, but using the container demonstrates that it works.</p>
<p>I recommend WordPress as a start. It's a popular web application that's easy to use, so you can test drive the app once you've got the container running. While you can easily set up a WordPress container, there are many configuration options, which can lead you to discover more container options (like running a database container) and how containers communicate.</p>
<p>I use Podman, which is a friendly, convenient, and daemonless container engine. If you don't have Podman available, you can use the Docker command instead. Both are great open source container engines, and their syntax is identical (just type <code>docker</code> instead of <code>podman</code>). Because Podman doesn't run a daemon, it requires more setup than Docker, but the ability to run rootless daemonless containers is worth it.</p>
<p>If you're going with Docker, you can skip down to the <a href="#wp">WordPress subheading</a>. Otherwise, open a terminal to install and configure Podman:</p>
<pre><code class="language-bash">$ sudo dnf install podman</code></pre><p>Containers spawn many processes, and normally only the root user has permission to create thousands of process IDs. Add some extra process IDs to your user by creating a file called <code>/etc/subuid</code> and defining a suitably high start UID with a suitable large number of permitted PIDs:</p>
<pre><code class="language-bash">seth:200000:165536</code></pre><p>Do the same for your group in a file called <code>/etc/subgid</code>. In this example, my primary group is <code>staff</code> (it may be <code>users</code> for you, or the same as your username, depending on how you've configured your system.)</p>
<pre><code class="language-bash">staff:200000:165536</code></pre><p>Finally, confirm that your user is also permitted to manage thousands of namespaces:</p>
<pre><code class="language-bash">$ sysctl --all --pattern user_namespaces
user.max_user_namespaces = 28633</code></pre><p>If your user doesn't have permission to manage at least 28,000 namespaces, increase the number by creating the file <code>/etc/sysctl.d/userns.conf</code> and enter:</p>
<pre><code class="language-bash">user.max_user_namespaces=28633</code></pre><h3><a id="wp" name="wp"></a>Running WordPress as a container</h3>
<p>Now, whether you're using Podman or Docker, you can pull a WordPress container from a container registry online and run it. You can do all this with a single Podman command:</p>
<pre><code class="language-bash">$ podman run --name mypress \
-p 8080:80 -d wordpress</code></pre><p>Give Podman a few moments to find the container, copy it from the internet, and start it up.</p>
<p>Start a web browser once you get a terminal prompt back and navigate to <code>localhost:8080</code>. WordPress is running, waiting for you to set it up.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="WordPress running in a container"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/podman-wordpress.jpg" width="765" height="539" alt="WordPress running in a container" title="WordPress running in a container" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Seth Kenlon, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>It doesn't take long to reach your next hurdle, though. WordPress uses a database to keep track of data, so you need to provide it with a database where it can store its information.</p>
<p>Before continuing, stop and remove the WordPress container:</p>
<pre><code class="language-bash">$ podman stop mypress
$ podman rm mypress</code></pre><h2>3. Run containers in a pod</h2>
<p>Containers are, by design and, as their name suggests, self-contained. An application running in a container isn't supposed to interact with applications or infrastructure outside of its container. So when one container requires another container to function, one solution is to put those two containers inside a bigger container called a <em>pod</em>. A pod ensures that its containers can share important namespaces to communicate with one another.</p>
<p>Create a new pod, providing a name for the pod and which ports you want to be able to access:</p>
<pre><code class="language-bash">$ podman pod create \
--name wp_pod \
--publish 8080:80</code></pre><p>Confirm that the pod exists:</p>
<pre><code class="language-bash">$ podman pod list
POD ID        NAME     STATUS    INFRA ID      # OF CONTAINERS
100e138a29bd  wp_pod   Created   22ace92df3ef   1</code></pre><h3>Add a container to a pod</h3>
<p>Now that you have a pod for your interdependent containers, you launch each container by specifying a pod for it to run in.</p>
<p>First, launch a database. You can make up your own credentials as long as you use those same credentials when connecting to the database from WordPress.</p>
<pre><code class="language-bash">$ podman run --detach \
--pod wp_pod \
--restart=always \
-e MYSQL_ROOT_PASSWORD="badpassword0" \
-e MYSQL_DATABASE="wp_db" \
-e MYSQL_USER="tux" \
-e MYSQL_PASSWORD="badpassword1" \
--name=wp_db mariadb</code></pre><p>Next, launch the WordPress container into the same pod:</p>
<pre><code class="language-bash">$ podman run --detach \
--restart=always --pod=wp_pod \
-e WORDPRESS_DB_NAME="wp_db" \
-e WORDPRESS_DB_USER="tux" \
-e WORDPRESS_DB_PASSWORD="badpassword1" \
-e WORDPRESS_DB_HOST="127.0.0.1" \
--name mypress wordpress</code></pre><p>Now launch your favorite web browser and navigate to <code>localhost:8080</code>.</p>
<p>This time, the setup goes as expected. WordPress connects to the database because you've passed those environment variables while launching the container.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="WordPress setup"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/wordpress-setup.jpg" width="1459" height="887" alt="WordPress setup" title="WordPress setup" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Seth Kenlon, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>After you've created a user account, you can log in to see the WordPress dashboard.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="WordPress dashboard running in a container"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/wordpress-welcome.jpg" width="1281" height="760" alt="WordPress dashboard running in a container" title="WordPress running in a container" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Seth Kenlon, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<h2>Next steps</h2>
<p>You've created two containers, and you've run them in a pod. You know enough now to run services in containers on your own server. If you want to move to the cloud, containers are, of course, well-suited for that. With tools like Kubernetes and OpenShift, you can automate the process of launching <a href="https://enterprisersproject.com/article/2020/9/pod-cluster-container-what-is-difference" target="_blank">containers and pods on a cluster</a>. If you're thinking about taking the next step, read <a href="https://enterprisersproject.com/article/2019/11/kubernetes-3-ways-get-started" target="_blank">3 ways to get started with Kubernetes</a> by Kevin Casey, and give the Minikube tutorial he mentions a try.</p>
