<p>The ICRA 2023 conference will take place from May 29 to June 2, 2023 in London. It brings together the world’s top robotics researchers from academia and industry. During this event, several competitions are organized to foster new achievements in robotics. Among them, the wrestling competition will confront the best algorithms controlling intelligent humanoid robots in a wrestling game. Although the competition takes place in simulation, these little 3D guys are true digital twins of existing real robots named NAO. So their AI could transfer into the body of their real counterparts in a pretty straightforward way.</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2023-02/boxing-ring.webp" width="768" height="432" alt="Robots competing in a boxing ring" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Olivier Michel, CC BY-SA 4.0)</p>
</div>
      
  </article><h2>Digital twin of a real robot</h2>
<p>Each robot is equipped with a number of sensors and motors. These devices are available from the programming interface. The controller program can retrieve their measurements, such as camera images, process them, and send motor commands to drive the robot on its way to floor its opponent. Each participant can submit several versions of its controller program. For each new submission, a series of games is run in the cloud. Everyone can view them immediately as 3D animations from the web page of the leaderboard.</p>
<h2>All shots allowed</h2>
<p>The goal for the little guys on the ring is to floor their opponent in less than three minutes. If none succeed, the robot who covered the largest area wins. During the game, all shots are allowed, including using a stick or throwing a little plastic duck. Because these guys are sometimes so clunky, watching the games can be very entertaining.</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2023-02/robot-parts.webp" width="1000" height="933" alt="Robot parts with labels" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Olivier Michel, CC BY-SA 4.0)</p>
</div>
      
  </article><h2>100% open source software</h2>
<p>The complete competition infrastructure runs on GitHub and fully relies on open source software, including the Webots robot simulator. The participant workflow is straightforward for any software developer and can be easily re-used to implement a similar competition.</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2023-02/pipeline.webp" width="981" height="904" alt="The pipeline moves from a developer's local Git repo to the official remote Git repo, which is pushed to the simulation server. The simulation is published to the web server, making the competition viewable by the world." /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Olivier Michel, CC BY-SA 4.0)</p>
</div>
      
  </article><h2>Open to anyone</h2>
<p>Although it requires some basic skills in software development, participation is open to anyone. The robots can be programmed in <a href="https://opensource.com/article/17/10/python-101">Python</a>, <a href="https://opensource.com/tags/rust" target="_blank">Rust</a>, <a href="https://opensource.com/downloads/guide-c-programming">C</a>, <a href="https://opensource.com/article/20/12/learn-c-game">C++</a>, <a href="https://opensource.com/article/19/12/java-resources">Java</a> or ROS. There’s no limitation on the software libraries or programming languages that can be used.</p>
<h2>Save the date</h2>
<p>If you want to participate, you can register until May 23rd, 2023. However, you’re encouraged to register as early as possible to increase your chance of success.</p>
<ul><li><strong>January 16th, 2023</strong>: Registration opens and qualification games start</li>
<li><strong>May 23rd, 2023</strong>: Selection of the best 32 teams</li>
<li><strong>May 30th, 2023</strong>: 1/16 finals</li>
<li><strong>May 31th, 2023</strong>: 1/8 finals</li>
<li><strong>June 1st, 2023</strong>: 1/4 finals</li>
<li><strong>June 2nd, 2023</strong>: Semifinals, third place game, and final</li>
</ul><p>The finals will take place during the ICRA 2023 conference in London, and will be broadcasted online in real time. Remote participation will be possible.</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2023-02/DIVISIONS.webp" width="922" height="796" alt="Divisions and heats for the robot competition." /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Olivier Michel, CC BY-SA 4.0)</p>
</div>
      
  </article><p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">Open science and sustainability</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://www.redhat.com/en/creating-chris?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Video series: ChRIS (ChRIS Research Integration System)">Video series: ChRIS (ChRIS Research Integration System)</a></div>
              <div class="field__item"><a href="https://research.redhat.com/?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Explore Red Hat Research projects">Explore Red Hat Research projects</a></div>
              <div class="field__item"><a href="https://opensource.com/article/22/12/open-source-sustainability?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="6 articles to inspire open source sustainability">6 articles to inspire open source sustainability</a></div>
              <div class="field__item"><a href="https://opensource.com/article/22/4/how-linux-saves-earth?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="How Linux rescues slow computers (and the planet)">How Linux rescues slow computers (and the planet)</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text=" Register for your free Red Hat account"> Register for your free Red Hat account</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/science?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Latest articles about open science">Latest articles about open science</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/education?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Latest articles about open education">Latest articles about open education</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/sustainability?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Latest articles about sustainability">Latest articles about sustainability</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<h2>Grand prize</h2>
<p>The winner of the competition will receive one Ether crypto-currency. Although its value was USD 1,659 on February 3rd 2023, it may be completely different on June 2.</p>
<h2>Fostering the development of open science</h2>
<p>The competition is organized by Cyberbotics Ltd., a Swiss-based company developing the open source Webots robot simulator. It aims at promoting the use of open source software tools, including cloud-based simulations, to achieve incremental progress in robotics research. By relying on open source software, running in virtual machines in the cloud, research results in robotics become easily reproducible by anyone. This allows researchers to build their own research on top of the results achieved by their colleagues, paving the way to incremental research in the spirit of open science.</p>
<p>Here are relevant links for participation:</p>
<ul><li><a href="https://github.com/cyberbotics/wrestling">GitHub home of the competition</a></li>
<li><a href="https://webots.cloud/competition">Website hosting the competition</a></li>
<li><a href="https://www.icra2023.org/">ICRA 2023 conference</a></li>
<li><a href="https://github.com/cyberbotics/webots">Webots robot simulator</a></li>
<li><a href="https://cyberbotics.com/">Cyberbotics Ltd.</a></li>
</ul><p>Hope to see you there!</p>
