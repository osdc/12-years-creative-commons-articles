<p>A method in Java (called a "function" in many other programming languages) is a portion of code that's been grouped together and labeled for reuse. Methods are useful because they allow you to perform the same action or series of actions without rewriting the same code, which not only means less work for you, it means less code to maintain and debug when something goes wrong.</p>
<p>A method exists within a class, so the standard Java boilerplate code applies:</p>
<pre class="highlight">
<code class="language-java" data-lang="java">package com.opensource.example;

public class Example {
  // code here
}</code></pre><p>A package definition isn't strictly necessary in a simple one-file application like this, but it's a good habit to get into, and most IDEs enforce it.</p>
<p>By default, Java looks for a <code>main</code> method to run in a class. Methods can be made public or private, and static or non-static, but the main method must be public and static for the Java compiler to recognize and utilize it. When a method is public, it's able to be executed from outside the class. To call the <code>Example</code> class upon start of the program, its <code>main</code> method must be accessible, so set it to <code>public</code>.</p>
<p>Here's a simple demonstration of two methods: one <code>main</code> method that gets executed by default when the <code>Example</code> class is invoked, and one <code>report</code> method that accepts input from <code>main</code> and performs a simple action.</p>
<p>To mimic arbitrary data input, I use an if-then statement that chooses between two strings, based on when you happen to start the application. In other words, the <code>main</code> method first sets up some data (in real life, this data could be from user input, or from some other method elsewhere in the application), and then "calls" the <code>report</code> method, providing the processed data as input:</p>
<pre class="highlight">
<code class="language-java" data-lang="java">package com.opensource.example;

public class Example {
  public static void main(String[] args) {
    // generate some data
    long myTime = System.currentTimeMillis();
    String weather;

    if ( myTime%2 == 0 ) {
      weather = "party";
    } else {
      weather = "apocalypse";
    }

    // call the other method
    report(weather);
  }

  private static void report(String day) {
    System.out.printf("Welcome to the zombie %s\n", day);
  }
}</code></pre><p>Run the code:</p>
<pre class="highlight">
<code class="language-bash" data-lang="bash">$ java ./Example.java
Welcome to the zombie apocalypse
$ java ./Example.java
Welcome to the zombie party</code></pre><p>Notice that there are two different results from the same <code>report</code> method. In this simple demonstration, of course, there's no need for a second method. The same result could have been generated from the if-then statement that mimics the data generation. But when a method performs a complex task, like resizing an image into a thumbnail and then generating a widget on screen using that resized image, then the "expense" of an additional component makes a lot of sense.</p>
<p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">More on Java</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://developers.redhat.com/topics/enterprise-java/?intcmp=7013a000002Cxq6AAC" data-analytics-category="resource list" data-analytics-text="What is enterprise Java programming?">What is enterprise Java programming?</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/engage/open-source-alternative-s-202104291240?intcmp=7013a000002Cxq6AAC" data-analytics-category="resource list" data-analytics-text="An open source alternative to Oracle JDK">An open source alternative to Oracle JDK</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/java-cheat-sheet?intcmp=7013a000002Cxq6AAC" data-analytics-category="resource list" data-analytics-text="Java cheat sheet">Java cheat sheet</a></div>
              <div class="field__item"><a href="https://developers.redhat.com/products/openjdk/overview?intcmp=7013a000002Cxq6AAC" data-analytics-category="resource list" data-analytics-text="Red Hat build of OpenJDK">Red Hat build of OpenJDK</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/services/training/do092-developing-cloud-native-applications-microservices-architectures?intcmp=7013a000002Cxq6AAC" data-analytics-category="resource list" data-analytics-text="Free online course: Developing cloud-native applications with microservices">Free online course: Developing cloud-native applications with microservices</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Register for your free Red Hat account">Register for your free Red Hat account</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/java?intcmp=7013a000002Cxq6AAC" data-analytics-category="resource list" data-analytics-text="Fresh Java articles">Fresh Java articles</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<h2 id="_benefits_of_methods">When to use a Java method</h2>
<p>It can be difficult to know when to use a method and when to just send data into a <a href="https://opensource.com/article/20/1/javastream">Java Stream</a> or loop. If you're faced with that decision, the answer is usually to use a method. Here's why:</p>
<ul><li>Methods are cheap. They don't add processing overhead to your code.</li>
<li>Methods reduce the line count of your code.</li>
<li>Methods are specific. It's usually easier to find a method called <code>resizeImage</code> than it is to find code that's hidden in a loop somewhere in the function that loads images from the drive.</li>
<li>Methods are reusable. When you first write a method, you may <em>think</em> it's only useful for one task within your application. As your application grows, however, you may find yourself using a method you thought you were "done" with.</li>
</ul><h2 id="_the_difference_between_functional_and_object_oriented_programming">Functional vs. object-oriented programming</h2>
<p>Functional programming utilizes methods as the primary construct for performing tasks. You create a method that accepts one kind of data, processes that data, and outputs new data. String lots of methods together, and you have a dynamic and capable application. Programming languages like C and <a href="https://opensource.com/article/22/11/lua-worth-learning" target="_blank">Lua</a> are examples of this style of coding.</p>
<p>The other way to think of accomplishing tasks with code is the object-oriented model, which Java uses. In object-oriented programming, methods are components of a template. Instead of sending data from method to method, you create objects with the option to alter them through the use of their methods.</p>
<p>Here's the same simple zombie apocalypse demo program from an object-oriented perspective. In the functional approach, I used one method to generate data and another to perform an action with that data. The object-oriented equivalent is to have a class that represents a work unit. This example application presents a message-of-the-day to the user, announcing that the day brings either a zombie party or a zombie apocalypse. It makes sense to program a "day" object, and then to query that day to learn about its characteristics. As an excuse to demonstrate different aspects of object-oriented construction, the new sample application will also count how many zombies have shown up to the party (or apocalypse).</p>
<p>Java uses one file for each class, so the first file to create is <code>Day.java</code>, which serves as the Day object:</p>
<pre class="highlight">
<code class="language-java" data-lang="java">package com.opensource.example;

import java.util.Random;

// Class
public class Day {
    public static String weather;
    public int count;

// Constructor
  public Day() {
    long myTime = System.currentTimeMillis();

    if ( myTime%2 == 0 ) {
      weather = "paradise";
    } else {
      weather = "apocalypse";
    }
  }

// Methods
  public String report() {
      return weather;
  }

  public int counter() {
    Random rand = new Random();
    count = count + rand.nextInt(100);

    return(count);
    }
}</code></pre><p>In the <code>Class</code> section, two fields are created: <code>weather</code> and <code>count</code>. Weather is static. Over the course of a day (in this imaginary situation), weather doesn't change. It's either a party or an apocalypse, and it lasts all day. The number of zombies, however, increases over the course of a day.</p>
<p>In the <code>Constructor</code> section, the day's weather is determined. It's done as a <a href="https://opensource.com/article/19/6/what-java-constructor">constructor</a> because it's meant to only happen once, when the class is initially invoked.</p>
<p>In the <code>Methods</code> section, the <code>report</code> method only returns the weather report as determined and set by the constructor. The <code>counter</code> method, however, generates a random number and adds it to the current zombie count.</p>
<p>This class, in other words, does three very different things:</p>
<ul><li>Represents a "day" as defined by the application.</li>
<li>Sets an unchanging weather report for the day.</li>
<li>Sets an ever-increasing zombie count for the day.</li>
</ul><p>To put all of this to use, create a second file:</p>
<pre class="highlight">
<code class="language-java" data-lang="java">package com.opensource.example;

public class Example {
  public static void main(String[] args) {
    Day myDay = new Day();
    String foo = myDay.report();
    String bar = myDay.report();

    System.out.printf("Welcome to a zombie %s\n", foo);
    System.out.printf("Welcome to a zombie %s\n", bar);
    System.out.printf("There are %d zombies out today.\n", myDay.counter());
    System.out.printf("UPDATE: %d zombies. ", myDay.counter());
    System.out.printf("UPDATE: %d zombies. ", myDay.counter());
  }
}</code></pre><p>Because there are now two files, it's easiest to use a Java IDE to run the code, but if you don't want to use an IDE, you can create your own <a href="https://opensource.com/article/21/8/fastjar">JAR file</a>. Run the code to see the results:</p>
<pre class="highlight">
<code class="language-text" data-lang="text">Welcome to a zombie apocalypse
Welcome to a zombie apocalypse
There are 35 zombies out today.
UPDATE: 67 zombies. UPDATE: 149 zombies.</code></pre><p>The "weather" stays the same regardless of how many times the <code>report</code> method is called, but the number of zombies on the loose increases the more you call the <code>counter</code> method.</p>
<h2 id="_methods">Java methods</h2>
<p>Methods (or functions) are important constructs in programming. In Java, you can use them either as part of a single class for functional-style coding, or you can use them across classes for object-oriented code. Both styles of coding are different perspectives on solving the same problem, so there's no right or wrong decision. Through trial and error, and after a little experience, you learn which one suits a particular problem best.</p>
