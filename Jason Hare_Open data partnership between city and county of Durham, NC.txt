<p><em>Co-authored by Greg Marrow, Chief Information Officer at Durham County Government</em></p>
<p><em>Co-authored by Kerry Goode, Chief Information Officer for the City of Durham<br /></em></p>
<p>Open data has found the most innovation at the local government level. While not taking away from the efforts of <a href="http://www.data.gov/" target="_blank">data.gov</a> and the state initiatives, local data has more impact on the day to day lives of civil society. A wealth of city and county public data exists, but accessing it can sometimes be time consuming. Now, thanks to a new local government partnership, open data in Durham is just months away from becoming a reality.</p>
<!--break-->
<p>The City of Durham and Durham County governments in North Carolina are <a href="http://durhamnc.gov/Pages/NNDetails.aspx?detailId=911" target="_blank" title="City of Durham and Durham County Government Begin Open Data Partnership">embarking on an open data partnership</a> that will lay the groundwork for businesses, non-profits, journalists, universities, and residents to access and use the wealth of public data available between the two government organizations, while becoming even more transparent to the residents of Durham.</p>
<p>Durham City and County is taking a social sustainability approach toward their open data intiative. There are several categories of data that fit in with the assessment, gap analysis and open data roadmap toward creating a sustainable Durham:</p>
<h2>City/County services and quality of life</h2>
<ul><li>Economy</li>
<li>Education</li>
<li>Energy</li>
<li>Environment</li>
<li>Finance</li>
<li>Fire and Emergency Response</li>
<li>Governance</li>
<li>Health</li>
<li>Recreation</li>
<li>Safety</li>
<li>Shelter</li>
<li>Solid Waste</li>
<li>Telecommunication and Innovation</li>
<li>Transportation</li>
<li>Urban Planning</li>
<li>Wastewater</li>
<li>Water and Sanitation</li>
</ul><h3>Why are these metrics important?</h3>
<ul><li>Effective city governance with performance measurement</li>
<li>Transparency, bridging the divide between civil society</li>
<li>And government</li>
<li>Guides city management and sustainability planning</li>
<li>Facilitates learning across cities, globally and locally</li>
<li>Comparative analysis for policy development</li>
<li>Open data, third party verified data</li>
</ul><h2>Durham leaders voice opinions</h2>
<p>"This is a unique collaborative effort that demonstrates a regional commitment," said Durham County Manager Wendell Davis. "There will be shared costs, a common platform and portal, and a combined effort by both organizations to reach into the community to bring about business development and new community benefits and services."</p>
<p>"Durham is uniquely positioned to be a very successful site that demonstrates the business and social potential for open data," said Durham City Manager Thomas J. Bonfield. "Our community's entrepreneurial energy and creativity gives us an advantage to innovate new ways of doing business by making information readily accessible."</p>
<p>"Between the city and county, a lot of information and data exists in many areas, from transportation to public health," said City of Durham Mayor Bill Bell. "This information has always been publicly available, but open data makes it easier to use by developers, marketers, community enrichment agencies and the general public."</p>
<h2>The furture of open data in Durham</h2>
<p>Plans are underway now for a joint work plan and project framework, with a projected site launch date of summer 2015. </p>
<p>Over the past two years, open data hackathons have been conducted at American Tobacco Campus leading to the development of applications that provide important information for consumers. Code for America, advocates for open data, has already worked with the city and county to deliver an application tied to the health inspection reports for local restaurants.</p>
<p>Kerry Goode, the City of Durham CIO and Greg Marrow, the County of Durham CIO, will be co-presenters at 2015's Triangle Open Data Day. There they will discuss open data, sustainability and it's impact of residents of the City and County of Durham. The city-county partnership joins other local government open data efforts that include <a href="http://www.wakegov.com/data/Pages/default.aspx" target="_blank" title="Wake County Open Data" style="font-size: 0.75em; line-height: 1.4; background-color: transparent;">Wake County</a> and the <a href="https://data.raleighnc.gov/" target="_blank" title="City of Raleigh Open Data" style="font-size: 0.75em; line-height: 1.4; background-color: transparent;">City of Raleigh</a>. 2015 is the the Year of Open Data in the Research Triangle Park!</p>
