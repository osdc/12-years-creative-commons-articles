<p>Opensource.com is an online publication that collects and shares the stories of people all over the globe who are using open source. And we want to help share your story with the open source community and beyond.</p>
<h2>Ready to contribute?</h2>
<p>If you think you have a great story to tell, experience to share, or advice to give on one of these (or more!) topics, email us at <a href="mailto:open@opensource.com" target="_blank">open@opensource.com</a> and we will review your idea. If it is a good fit, we will move you forward through the editorial process to get your article published on Opensource.com. Guides for writers can be found at <a href="https://opensource.com/writers" target="_blank">Opensource.com/writers</a>.</p>
<p>Maybe you've been thinking about contributing but you'd like to see what we're working on this year. What our plans are. So, we've created a public-facing list that includes our monthly topics and weekly columns: <a href="https://opensource.com/resources/editorial-calendar" target="_blank">2015 Opensource.com Editorial Calendar</a></p>
<h2>2015 monthly themes</h2>
<p> </p>
<table border="1" cellpadding="8" cellspacing="5"><tbody><tr><td>January</td>
<td>Open Source Careers</td>
<td>Submission deadline: January 16</td>
</tr><tr><td>February</td>
<td>Beginners to Open Source<br />
			Speaker interviews for SCALE13X</td>
<td>Submission deadline: February 6</td>
</tr><tr><td>March</td>
<td>Open Education</td>
<td>Submission deadline: March 6</td>
</tr><tr><td>April</td>
<td>Speaker interviews for POSSCON and ApacheCon</td>
<td>Submission deadline: March 25</td>
</tr><tr><td>May</td>
<td>Open Hardware<br />
			Speaker interviews for OpenStack</td>
<td>
<p>Submission deadline: May 8<br />
			Submission deadline: April 22</p>
</td>
</tr><tr><td>June</td>
<td>Open Government &amp; Open Data</td>
<td>Submission deadline: May 29</td>
</tr></tbody></table><p> </p>
<h2>Weekly columns</h2>
<table border="1" cellpadding="8" cellspacing="5"><tbody><tr><td>
<h4>The Open CMS</h4>
<p>A column about open source content management systems (CMS) such as Drupal, Joomla, Plone, WordPress, and others.</p>
</td>
<td><a href="mailto:open@opensource.com">Contact Robin Muilwijk to contribute</a></td>
<td>We are still gathering contributors for this new column</td>
</tr><tr><td>
<h4>Easy DevOps</h4>
<p>A column about making DevOps practical—along with the tools, processes, culture, successes, and glorious/inglorious failures.</p>
</td>
<td><a href="mailto:open@opensource.com">Contact Greg DeKoenigsberg to contribute</a></td>
<td><a href="https://opensource.com/tags/easy-devops-column" target="_blank">View the column</a></td>
</tr><tr><td>
<h4>Humanitarian FOSS</h4>
<p>A column about projects and stories from the people who develop, maintain, and implement humanitarian free and open source software projects and tools.</p>
</td>
<td><a href="mailto:open@opensource.com">Contact Jen Wike Huger to contribute</a></td>
<td><a href="https://opensource.com/tags/hfoss-column" target="_blank">View the column</a></td>
</tr><tr><td>
<h4>The Open Hardware Connection</h4>
<p>A column about the growing open hardware community and the fantastic projects coming from makers and tinkers around the world.</p>
</td>
<td><a href="mailto:open@opensource.com">Contact Jason Hibbets to contribute</a></td>
<td><a href="https://opensource.com/tags/open-hardware-column" target="_blank">View the column</a></td>
</tr></tbody></table><p> </p>
<h2>Should I contribute?</h2>
<p>Not sure if you're skilled enough to write for us? We're here to help and offer editorial services that include copy editing, revisions, SEO, and promotion. Guides for writers can be found at <a href="https://opensource.com/writers" target="_blank">Opensource.com/writers</a>.</p>
<p>Maybe you're not sure what to write about but you use open source (software, hardware, tools, data, access, etc) in some way or you believe the open source way is just a better way. I gave a short talk last year that shows you how Opensource.com is building a premier storytelling platform. And this is a good way for you to understand how you might fit into that, as a writer, as a sharer, as a fellow member of the growing open source community. Feel free to ask us questions, we'd love to hear from you!</p>
<p>So, what does it mean to <em>use</em> open source? A few examples are:</p>
<ul><li dir="ltr">using open source software tools to get work done</li>
<li dir="ltr">using open data to create apps the public can use</li>
<li dir="ltr">using materials that are open access for scientific research</li>
</ul><p> </p>
<h2>What is Opensource.com all about?</h2>
<p>At Opensource.com we feel we're doing an essential job as an online publication that shares open source knowledge, news, and stories that achieve two things: we inform the open source community (both project-based and at-large) and we educate everyone else out there who are not aware of their options beyond proprietary and closed.</p>
<p>As of today, we publish an average of three new articles a day (Monday - Friday) and one new article on the weekend (Saturday - Sunday). We write and maintain resource pages on topics that are big in open source, like: What is open source? What is Docker? What is OpenStack? and What is open hardware? Every month, we publish articles focused on a particular topic of interest. And weekly, we publish articles as part of our columns. See our 2015 plans in the table above as well as on the <a href="https://opensource.com/resources/editorial-calendar" target="_blank">2015 Opensource.com Editorial Calendar</a>. For some open source conferences, we interview some of the speakers before their talks so that our readers can find out more about them, their work, and what they plan to share at these great events for the community.</p>
<p>Opensource.com also has a great Community Moderator program that consists of 13 members (and growing!) who are dedicated to helping write, curate, and promote open source stories on the site. You can find out more about the whole team on our staff page.</p>
