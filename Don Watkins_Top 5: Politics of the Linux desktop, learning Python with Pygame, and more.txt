<p>This week we want to know if you still have the installation media for your first Linux computer. We also look at three open source alternatives to Microsoft Publisher, if Pygame could be the best way to learn Python, open source trends for 2018, and more.</p>
<p></p><center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/hrpL7mO1BB8" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen=""></iframe><p></p></center>
<h2>Top 5 articles</h2>
<h3>5. <a href="https://opensource.com/article/17/11/linux-install-disk-poll">How old is your oldest Linux install disk?</a></h3>
<p>Editor Jason Baker wants to know how old your oldest Linux install disk is. There's a certain nostalgia attached to the physical relics of your early days with computing, particularly if you managed to save your first Linux boot disk. Be sure to vote in our poll and share your thoughts in the comments.</p>
<h3>4. <a href="https://opensource.com/alternatives/microsoft-publisher">3 open source alternatives to Microsoft Publisher</a></h3>
<p>Editor Jason Baker wants to know how you design for print. The world of proprietary software has brought us a number of great products, but did you know that you can get great results with Scribus, LibreOffice, and Markup too? </p>
<h3>3. <a href="https://opensource.com/article/17/11/pygame">Why Python and Pygame are a great pair for beginning programmers</a></h3>
<p>Craig Oda is president and co-founder of the Tokyo Linux User Group. In this article, he presents three reasons why Pygame might be the best solution when you are learning to program with Python.</p>
<h3>2. <a href="https://opensource.com/article/17/11/10-open-source-technology-trends-2018">10 open source technology trends for 2018</a></h3>
<p>Sreejith Omanakuttan, open source project lead at Fingent, gives a great tour of his forecast for open source technology trends in the next year. See if you agree with his predictions and share your own in the comments.</p>
<h3>1. <a href="https://opensource.com/article/17/11/politics-linux-desktop">The politics of the Linux desktop</a></h3>
<p>Mike Bursell of Red Hat has been around open source since 1997. He wants to know why you would use anything other than a Linux desktop. He says, "I believe in open source. We have a number of very, very good desktop-compatible distributions these days, and most of the time they just work." Be sure to leave your thoughts in the comments. </p>
<h3>Honorable mention: <a href="https://opensource.com/article/17/11/black-friday-3d-printer-giveaway">3D Printer giveaway</a></h3>
<p>There's still time to enter the Opensource.com Holiday 3D Printer giveaway! One grand prize winner will receive a <a href="https://www.lulzbot.com/store/printers/lulzbot-taz-6">LulzBot Taz 6</a> 3D printer and a lucky second prize winner will receive a <a href="https://www.lulzbot.com/store/printers/lulzbot-mini">LulzBot Mini</a> 3D printer. Enter by this Sunday, December 3, at 11:59 p.m. Eastern Time for a chance to win.</p>
