<p>Regardless of how organized I resolve to be, it seems there are always times when I just can't locate a file. Sometimes it's because I can't remember the name of the file in the first place. Other times, I know the name, but I can't recall where I decided to save it. There are even times when I need a file that I didn't create in the first place. No matter what the quandary, though, I know that on a <a href="https://opensource.com/article/19/7/what-posix-richard-stallman-explains" target="_self">POSIX system</a>, I always have the <code>find</code> command.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Linux resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux commands cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Free online course: RHEL Technical Overview</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux networking cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">SELinux cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux common commands cheat sheet</a></li>
<li><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">What are Linux containers?</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Our latest Linux articles</a></li>
</ul></div>
</div>
</div>
</div>

<h2>Installing find</h2>
<p>The <code>find</code> command is defined by the <a href="https://pubs.opengroup.org/onlinepubs/9699919799.2018edition/" target="_blank">POSIX specification</a>, which creates the open standard by which POSIX systems (including Linux, BSD, and macOS) are measured. Simply put, you already have <code>find</code> installed as long as you're running Linux, BSD, or macOS.</p>
<p>However, not all <code>find</code> commands are exactly alike. The GNU <code>find</code> command, for instance, has features that the BSD or Busybox or Solaris <code>find</code> command might not have or does have but implements differently. This article uses GNU <code>find</code> from the <a href="https://www.gnu.org/software/findutils/" target="_blank">findutils</a> package because it's readily available and pretty popular. Most commands demonstrated in this article work with other implementations of <code>find</code>, but should you try a command on a platform other than Linux and get unexpected results, try downloading and installing the GNU version.</p>
<h2>Find a file by name</h2>
<p>You can locate a file by its filename by providing the full file name or parts of the file name using regular expressions. The <code>find</code> command requires the path to the directory you want to search in, options to specify what attribute you're searching (for instance, -<code>name</code> for a case-sensitive file name), and then the search string. By default, your search string is treated literally: The <code>find</code> command searches for a filename that is exactly the string you enter between quotes unless you use regular expression syntax.</p>
<p>Assume your Documents directory contains four files: <code>Foo</code>, <code>foo</code>, <code>foobar.txt</code>, and <code>foo.xml</code>. Here's a literal search for a file with the name "foo":</p>
<pre><code class="language-bash">$ find ~ -name "foo"
/home/tux/Documents/examples/foo</code></pre><p>You can broaden your search by making it case-insensitive with the <code>-iname</code> option:</p>
<pre><code class="language-bash">$ find ~ -iname "foo"
/home/tux/Documents/examples/foo
/home/tux/Documents/examples/Foo</code></pre><h2>Wildcards</h2>
<p>You can use basic shell wildcard characters to broaden your search. For instance, the asterisk (<code>*</code>) represents any number of characters:</p>
<pre><code class="language-bash">$ find ~ -iname "foo*"
/home/tux/Documents/examples/foo
/home/tux/Documents/examples/Foo
/home/tux/Documents/examples/foo.xml
/home/tux/Documents/examples/foobar.txt</code></pre><p>A question mark (<code>?</code>) represents a single character:</p>
<pre><code class="language-bash">$ find ~ -iname "foo*.???"
/home/tux/Documents/examples/foo.xml
/home/tux/Documents/examples/foobar.txt</code></pre><p>This isn't regular expression syntax, so the dot (<code>.</code>) represents a literal dot in this example.</p>
<h2>Regular expressions</h2>
<p>You can also use regular expressions. As with <code>-iname</code> and <code>-name</code>, there's both a case-sensitive and a case-insensitive option. Unlike the <code>-name</code> and <code>-iname</code> options, though, a <code>-regex</code> and <code>-iregex</code> search is applied to the <em>whole path</em>, not just the file name. That means if you search for <code>foo</code>, you get no results because <code>foo</code> doesn't match <code>/home/tux/Documents/foo</code>. Instead, you must either search for the entire path, or else use a wildcard sequence at the beginning of your string:</p>
<pre><code class="language-bash">$ find ~ -iregex ".*foo"
/home/tux/Documents/examples/foo
/home/tux/Documents/examples/Foo</code></pre><h2>Find a file modified within the last week</h2>
<p>To find a file you last modified last week, use the <code>-mtime</code> option along with a (negative) number of days in the past:</p>
<pre><code class="language-bash">$ find ~ -mtime -7
/home/tux/Documents/examples/foo
/home/tux/Documents/examples/Foo
/home/tux/Documents/examples/foo.xml
/home/tux/Documents/examples/foobar.txt</code></pre><h2>Find a file modified within a range of days</h2>
<p>You can combine <code>-mtime</code> options to locate a file within a range of days. For the first <code>-mtime</code> argument, provide the most recent number of days you could have modified the file, and for the second, give the greatest number of days. For instance, this search looks for files with modification times more than one day in the past, but no more than seven:</p>
<pre><code class="language-bash">$ find ~ -mtime +1 -mtime -7</code></pre><h2>Limit a search by file type</h2>
<p>It's common to optimize the results of <code>find</code> by specifying the file type you're looking for. You shouldn't use this option if you're not sure what you're looking for, but if you know you're looking for a file and not a directory, or a directory but not a file, then this can be a great filter to use. The option is <code>-type</code>, and its arguments are a letter code representing a few different kinds of data. The most common are:</p>
<ul><li><code>d</code> - directory</li>
<li><code>f</code> - file</li>
<li><code>l</code> - symbolic link</li>
<li><code>s</code> - socket</li>
<li><code>p</code> - named pipe (used for FIFO)</li>
<li><code>b</code> - block special (usually a hard drive designation)</li>
</ul><p>Here are some examples:</p>
<pre><code class="language-bash">$ find ~ -type d -name "Doc*"
/home/tux/Documents
$ find ~ -type f -name "Doc*"
/home/tux/Downloads/10th-Doctor.gif
$ find /dev -type b -name "sda*"
/dev/sda
/dev/sda1</code></pre><h2>Adjust scope</h2>
<p>The <code>find</code> command is recursive by default, meaning that it searches for results in the directories of directories contained in directories (and so on). This can get overwhelming in a large filesystem, but you can use the <code>-maxdepth</code> option to control how deep into your folder structure you want <code>find</code> to descend:</p>
<pre><code class="language-bash">$ find /usr -iname "*xml" | wc -l
15588
$ find /usr -maxdepth 2 -iname "*xml" | wc -l
15</code></pre><p>You can alternately set the minimum depth of recursion with <code>-mindepth</code>:</p>
<pre><code class="language-bash">$ find /usr -mindepth 8 -iname "*xml" | wc -l
9255</code></pre><h2>Download the cheat sheet</h2>
<p>This article only covers the basic functions of <code>find</code>. It's a great tool for searching through your system, but it's also a really useful front-end for the powerful <a href="https://opensource.com/article/18/5/gnu-parallel" target="_self">Parallel</a> command. There are many reasons to learn <code>find</code>, so <strong><a href="https://opensource.com/downloads/linux-find-cheat-sheet">download our free <code>find</code> cheat sheet</a></strong> to help you learn more about the command.</p>
