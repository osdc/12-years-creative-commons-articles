<p>Today marks the release of OpenStack's seventeenth release, <a href="https://releases.openstack.org/queens/" target="_blank">Queens</a>. After a 26-week release schedule, Queens brings into the fold new projects and new features, including strong container integration, support for vGPUs, and many advancements around NFV, edge computing, and machine learning applications. For an overview of many of the great new things you'll find in this release, see the official <a href="https://www.openstack.org/news/view/371/openstack-queens-release-expands-support-for-gpus-and-containers-to-meet-edge-nfv-and-machine-learning-workload-demands" target="_blank">press release</a>.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on OpenStack</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/resources/what-is-openstack?intcmp=7016000000127cYAAQ">What is OpenStack?</a></li>
<li><a href="https://opensource.com/resources/openstack/tutorials?intcmp=7016000000127cYAAQ">OpenStack tutorials</a></li>
<li><a href="https://opensource.com/tags/openstack?intcmp=7016000000127cYAAQ">More OpenStack articles</a></li>
<li><a href="https://www.rdoproject.org/?intcmp=7016000000127cYAAQ">The RDO Project</a></li>
</ul></div>
</div>
</div>
</div>
<p>Meanwhile, OpenStack's development community is gathered this week in Dublin, Ireland for the twice-a-year <a href="https://www.openstack.org/ptg" target="_blank">Project Teams Gathering</a> (PTG). As developers seek to better understand and implement the needs of the OpenStack user community, users themselves can begin exploring what Queens has to offer and thinking about what their organization's strategy is for implementing new features as they begin to land in their OpenStack distribution of choice.</p>
<p>If your team works regularly with OpenStack, you know that there's a lot of information to keep track of. OpenStack is a huge effort, encompassing dozens of individual projects, and even more related tools you may need to be familiar with. Whether you're a developer, an architect, or a cloud administrator, there's a lot to stay on top of.</p>
<p>Fortunately for you, there are plenty of resources out there to help you. There's great project documentation, forums, mailing lists, IRC channels, numerous books and guides, as well as certification and training programs. On top of this, many OpenStack community members also write up their own guides, tips, and tutorials. Every month we bring together some of the best of this new community-created content in one easy place. Here's what we found this month.</p>
<ul style="list-style-type:disc;"><li>
<p>Let's start with something simple. Ever uploaded an image to Glance, only to release you made a mistake, then gone to delete it and found that the file was locked? Adam Young ran into this problem, and he detailed what happened and how he quickly fixed it in <a href="http://adam.younglogic.com/2018/02/deleting-an-image-on-rdo/" target="_blank">this</a> short blog post.</p>
</li>
<li>
<p>If you're developing code for OpenStack, it's important that you make sure that adequate tests exist in Tempest to make sure your code runs smoothly in a variety of environments and different conditions. For simple code changes, this may be simple. But what happens if the project you're working on is large enough to span multiple OpenStack services? This <a href="https://andreafrittoli.me/2018/01/25/cross-service-tempest-plugin-devstack/" target="_blank">multiple</a> <a href="https://andreafrittoli.me/2018/01/30/cross-service-tempest-testing-the-tempest-plugin/" target="_blank">part</a> guide to cross-service Tempest testing might help.</p>
</li>
<li>
<p>OpenStack makes use of the Keystone project to handle authentication of users to the various services and resources available in its cloud. But what happens if you want to build a Kubernetes cluster on top of your OpenStack cloud: do you have to create a whole new authentication system? Nope, you can use Keystone to authenticate your users in Kubernetes as well, and <a href="https://cloudblog.switch.ch/2018/02/02/openstack-keystone-authentication-for-your-kubernetes-cluster/" target="_blank">here's how</a>.</p>
</li>
<li>
<p>One of the great new features of today's OpenStack Queens release is volume multi-attach: the ability to use a single block storage device with multiple servers in your OpenStack cluster. Learn more about how to get started using it and get an overview of how it works in this <a href="http://superuser.openstack.org/articles/meet-volume-multi-attach-great-new-feature-openstack-queens/" target="_blank">demo video</a>.</p>
</li>
<li>
<p>Gnocchi is a tool which can be used for gathering telemetry data within OpenStack. But how can you use it to run aggregate queries against multiple servers in your cloud? Lars Kellogg-Stedman walks through <a href="http://blog.oddbit.com/2018/02/26/grouping-aggregation-queries-i/" target="_blank">an example</a> of querying how much memory was used by each individual project, on average, running in an OpenStack cloud.</p>
</li>
</ul><hr /><p>That's all we've got for this time around. But if you're looking for more, we have an entire collection of past <a href="https://opensource.com/resources/openstack-tutorials" target="_blank">OpenStack guides, how-tos, and tutorials</a> where you can find almost four years of archived community-created help. Have you come across a great new piece that we missed? Let us know in the comments, and we can consider it for future articles like this one. Or if you've got tutorial of your own to share, consider <a href="https://opensource.com/how-submit-article" target="_blank">submitting your article</a> to Opensource.com.</p>
