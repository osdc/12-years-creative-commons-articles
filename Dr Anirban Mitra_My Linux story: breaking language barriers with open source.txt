<p>My open source journey started rather late in comparison to many of my peers and colleagues.</p>
<p>I was pursuing a post-graduate degree in medicine in 2000 when I managed to fulfill a dream I’d had since high school—to buy my own PC. Before that, my only exposure to computers was through occasional access in libraries or cyber cafés, which charged exorbitant prices for access at that time. So I saved up portions of my grad student stipend and managed to buy a Pentium III 550 Mhz with 128MB RAM, and as came standard in most computers in India at that time, a pirated version of Windows 98.</p>
<p>There was no Internet access in my hostel room. I had to go to the nearby cyber café, download software there, and then carry around dozens of floppy discs.</p>
<p>As happy as I was finally owning my own computer, it bothered me that I could not write in my mother tongue, Bangla. I came across resources provided by CDAC, a government agency that provided Indian language tools based on ISCII, an older national standard upon which the Unicode standard of Indic language was based. It was difficult to learn the keyboard layouts.</p>
<h2 id="my-first-contribution">My first contribution</h2>
<p>Soon, I came across a software called <a href="http://www.yudit.org/" target="_blank">Yudit</a>, which offered phonetic typing of Indic language using the standard QWERTY keyboard. It was with Yudit that I first came across terms like open source and free software, GNU, and Linux. Yudit allowed me to translate UI elements into Bengali too, and when I submitted the translations to the developer, he gladly incorporated them into the next version and credited me in the README of the software.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Linux resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux commands cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Free online course: RHEL Technical Overview</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux networking cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">SELinux cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux common commands cheat sheet</a></li>
<li><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">What are Linux containers?</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Our latest Linux articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>This was exciting for me, as I was seeing, for the very first time, an application user element in my mother tongue. Moreover, I had been able to contribute to the development of a software despite having almost zero knowledge of coding. I went on to create an ISCII-to-Unicode converter for Yudit, which can also be used for transliteration between various Indian languages. I also bought a Linux magazine that came with a free live CD of Knoppix, and that’s how I got a feel for the Linux desktop.</p>
<p>Another issue I faced was the lack of availability of Unicode-compliant OpenType Bangla font. The font I used was shareware, and I was supposed to pay a license fee for it. I thought, “Why not try my hand at developing it myself?” In the process, I came in contact with Bangla speakers scattered worldwide who were trying to enable Bangla in the Linux operating system, via <code>bengalinux.org</code> (later renamed Ankur group).</p>
<p>I joined their mailing list, and we discussed among ourselves and the authorities the various flaws in the Unicode and OpenType specifications of Bangla, which were then corrected in due course. I contributed by converting legacy Bangla fonts into OpenType Unicode-compliant fonts, translating UI, and so on. That group also came out with the world’s first Live Linux CD with a Bangla user interface.</p>
<p>In 2003, I had moved to a place where I did not have access to the Internet; I could only connect to the group on Sundays when I came to Kolkata. By that time, Bangla localization of Linux had become a mainstream thing. Some of our volunteers joined Red Hat to work on translation and font development. I also became busy in my medical practice and had little time left for open source development.</p>
<p>Now, I feel more comfortable using Linux to do my daily work than any other operating system. I also feel proud to be associated with a project which allows people to communicate in their own language. It also brought computing power to a population who were for a long time considered to be on the other side of the “digital divide” because they did not speak English. Bangla is actually one of the most widely spoken languages in the world, and this project removed a major barrier to access for a large chunk of the global population.</p>
<h2 id="joining-open-source">Joining open source</h2>
<p>Joining in on the open source movement is easy. Take the initiative to do something that is useful to yourself, and then think about how it could be useful to others. The key is to keep it freely available, and it can add untold value to the world.</p>
