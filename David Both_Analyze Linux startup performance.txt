<p>Part of the system administrator's job is to analyze the performance of systems and to find and resolve problems that cause poor performance and long startup times. Sysadmins also need to check other aspects of systemd configuration and usage.</p>
<p>The systemd init system provides the <code>systemd-analyze</code> tool that can help uncover performance problems and other important systemd information. In a previous article, <a href="https://opensource.com/article/20/7/systemd-calendar-timespans"><em>Analyzing systemd calendar and timespans</em></a>, I used <code>systemd-analyze</code> to analyze timestamps and timespans in systemd timers, but this tool has many other uses, some of which I will explore in this article.</p>
<h2 id="startup-overview">Startup overview</h2>
<p>The Linux startup sequence is a good place to begin exploring because many <code>systemd-analyze</code> tool functions are targeted at startup. But first, it is important to understand the difference between boot and startup. The boot sequence starts with the BIOS power-on self test (POST) and ends when the kernel is finished loading and takes control of the host system, which is the beginning of startup and the point when the systemd journal begins.</p>
<p>In the second article in this series, <a href="https://opensource.com/article/20/5/systemd-startup?utm_campaign=intrel"><em>Understanding systemd at startup on Linux</em></a>, I discuss startup in a bit more detail with respect to what happens and in what sequence. In this article, I want to examine the startup sequence to look at the amount of time it takes to go through startup and which tasks take the most time.</p>
<p>The results I'll show below are from my primary workstation, which is much more interesting than a virtual machine's results. This workstation consists of an ASUS TUF X299 Mark 2 motherboard, an Intel i9-7960X CPU with 16 cores and 32 CPUs (threads), and 64GB of RAM. Some of the commands below can be run by a non-root user, but I will use root in this article to prevent having to switch between users.</p>
<p>There are several options for examining the startup sequence. The simplest form of the <code>systemd-analyze</code> command displays an overview of the amount of time spent in each of the main sections of startup, the kernel startup, loading and running <code>initrd</code> (i.e., initial ramdisk, a temporary system image that is used to initialize some hardware and mount the <code>/</code> [root] filesystem), and userspace (where all the programs and daemons required to bring the host up to a usable state are loaded). If no subcommand is passed to the command, <code>systemd-analyze time</code> is implied:</p>
<pre>
<code class="language-bash">[root@david ~]$ systemd-analyze 
Startup finished in 53.921s (firmware) + 2.643s (loader) + 2.236s (kernel) + 4.348s (initrd) + 10.082s (userspace) = 1min 13.233s 
graphical.target reached after 10.071s in userspace
[root@david ~]#</code></pre><p>The most notable data in this output is the amount of time spent in firmware (BIOS): almost 54 seconds. This is an extraordinary amount of time, and none of my other physical systems take anywhere near as long to get through BIOS.</p>
<p>My System76 Oryx Pro laptop spends only 8.506 seconds in BIOS, and all of my home-built systems take a bit less than 10 seconds. After some online searches, I found that this motherboard is known for its inordinately long BIOS boot time. My motherboard never "just boots." It always hangs, and I need to do a power off/on cycle, and then BIOS starts with an error, and I need to press F1 to enter BIOS configuration, from where I can select the boot drive and finish the boot. This is where the extra time comes from.</p>
<p>Not all hosts show firmware data. My unscientific experiments lead me to believe that this data is shown only for Intel generation 9 processors or above. But that could be incorrect.</p>
<p>This overview of the boot startup process is interesting and provides good (though limited) information, but there is much more information available about startup, as I'll describe below.</p>
<h2 id="assigning-blame">Assigning blame</h2>
<p>You can use <code>systemd-analyze blame</code> to discover which systemd units take the most time to initialize. The results are displayed in order by the amount of time they take to initialize, from most to least:</p>
<pre>
<code class="language-bash">[root@david ~]$ systemd-analyze blame                                                                         
       5.417s NetworkManager-wait-online.service                                                       
       3.423s dracut-initqueue.service                                                                 
       2.715s systemd-udev-settle.service                                                              
       2.519s fstrim.service                                                                           
       1.275s udisks2.service                                                                          
       1.271s smartd.service                                                                           
        996ms upower.service                                                                           
        637ms lvm2-monitor.service                                                                     
        533ms lvm2-pvscan@8:17.service                                                                 
        520ms dmraid-activation.service                                                                
        460ms vboxdrv.service                                                                          
        396ms initrd-switch-root.service
&lt;SNIP – removed lots of entries with increasingly small times&gt;</code></pre><p>Because many of these services start in parallel, the numbers may add up to significantly more than the total given by <code>systemd-analyze time</code> for everything after the BIOS. All of these are small numbers, so I cannot find any significant savings here.</p>
<p>The data from this command can provide indications about which services you might consider to improve boot times. Services that are not used can be disabled. There does not appear to be any single service that is taking an excessively long time during this startup sequence. You may see different results for each boot and startup.</p>
<h2 id="critical-chains">Critical chains</h2>
<p>Like the critical path in project management, a <em>critical chain</em> shows the time-critical chain of events that take place during startup. These are the systemd units you want to look at if startup is slow, as they are the ones that would cause delays. This tool does not display all the units that start, only those in this critical chain of events:</p>
<pre>
<code class="language-bash">[root@david ~]# systemd-analyze critical-chain 
The time when unit became active or started is printed after the "@" character.
The time the unit took to start is printed after the "+" character.

graphical.target @10.071s
└─lxdm.service @10.071s
  └─plymouth-quit.service @10.047s +22ms
    └─systemd-user-sessions.service @10.031s +7ms
      └─remote-fs.target @10.026s
        └─remote-fs-pre.target @10.025s
          └─nfs-client.target @4.636s
            └─gssproxy.service @4.607s +28ms
              └─network.target @4.604s
                └─NetworkManager.service @4.383s +219ms
                  └─dbus-broker.service @4.434s +136ms
                    └─dbus.socket @4.369s
                      └─sysinit.target @4.354s
                        └─systemd-update-utmp.service @4.345s +9ms
                          └─auditd.service @4.301s +42ms
                            └─systemd-tmpfiles-setup.service @4.254s +42ms
                              └─import-state.service @4.233s +19ms
                                └─local-fs.target @4.229s
                                  └─Virtual.mount @4.019s +209ms
                                    └─systemd-fsck@dev-mapper-vg_david2\x2dVirtual.service @3.742s +274ms
                                      └─local-fs-pre.target @3.726s
                                        └─lvm2-monitor.service @356ms +637ms
                                          └─dm-event.socket @319ms
                                            └─-.mount
                                              └─system.slice
                                                └─-.slice
[root@david ~]#</code></pre><p>The numbers preceded with <code>@</code> show the absolute number of seconds since startup began when the unit becomes active. The numbers preceded by <code>+</code> show the amount of time it takes for the unit to start.</p>
<h2 id="system-state">System state</h2>
<p>Sometimes you need to determine the system's current state. The <code>systemd-analyze dump</code> command dumps a <em>massive</em> amount of data about the current system state. It starts with a list of the primary boot timestamps, a list of each systemd unit, and a complete description of the state of each:</p>
<pre>
<code class="language-bash">[root@david ~]# systemd-analyze dump
Timestamp firmware: 1min 7.983523s
Timestamp loader: 3.872325s
Timestamp kernel: Wed 2020-08-26 12:33:35 EDT
Timestamp initrd: Wed 2020-08-26 12:33:38 EDT
Timestamp userspace: Wed 2020-08-26 12:33:42 EDT
Timestamp finish: Wed 2020-08-26 16:33:56 EDT
Timestamp security-start: Wed 2020-08-26 12:33:42 EDT
Timestamp security-finish: Wed 2020-08-26 12:33:42 EDT
Timestamp generators-start: Wed 2020-08-26 16:33:42 EDT
Timestamp generators-finish: Wed 2020-08-26 16:33:43 EDT
Timestamp units-load-start: Wed 2020-08-26 16:33:43 EDT
Timestamp units-load-finish: Wed 2020-08-26 16:33:43 EDT
Timestamp initrd-security-start: Wed 2020-08-26 12:33:38 EDT
Timestamp initrd-security-finish: Wed 2020-08-26 12:33:38 EDT
Timestamp initrd-generators-start: Wed 2020-08-26 12:33:38 EDT
Timestamp initrd-generators-finish: Wed 2020-08-26 12:33:38 EDT
Timestamp initrd-units-load-start: Wed 2020-08-26 12:33:38 EDT
Timestamp initrd-units-load-finish: Wed 2020-08-26 12:33:38 EDT
-&gt; Unit system.slice:
        Description: System Slice
        Instance: n/a
        Unit Load State: loaded
        Unit Active State: active
        State Change Timestamp: Wed 2020-08-26 12:33:38 EDT
        Inactive Exit Timestamp: Wed 2020-08-26 12:33:38 EDT
        Active Enter Timestamp: Wed 2020-08-26 12:33:38 EDT
        Active Exit Timestamp: n/a
        Inactive Enter Timestamp: n/a
        May GC: no
&lt;SNIP – Deleted a bazillion lines of output&gt;</code></pre><p>On my main workstation, this command generated a stream of 49,680 lines and about 1.66MB. This command is very fast, so you don't need to wait for the results.</p>
<p> </p>
<div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Linux resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux commands cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Free online course: RHEL Technical Overview</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux networking cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">SELinux cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux common commands cheat sheet</a></li>
<li><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">What are Linux containers?</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Our latest Linux articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>I do like the wealth of detail provided for the various connected devices, such as storage. Each systemd unit has a section with details such as modes for various runtimes, cache, and log directories, the command line used to start the unit, the process ID (PID), the start timestamp, as well as memory and file limits.</p>
<p> </p>
<p>The man page for <code>systemd-analyze</code> shows the <code>systemd-analyze --user dump</code> option, which is intended to display information about the internal state of the user manager. This fails for me, and internet searches indicate that there may be a problem with it. In systemd, <code>--user</code> instances are used to manage and control the resources for the hierarchy of processes belonging to each user. The processes for each user are part of a control group, which I'll cover in a future article.</p>
<h2 id="analytic-graphs">Analytic graphs</h2>
<p>Most pointy-haired-bosses (PHBs) and many good managers find pretty graphs easier to read and understand than the text-based system performance data I usually prefer. Sometimes, though, even I like a good graph, and <code>systemd-analyze</code> provides the capability to display startup data in an <a href="https://en.wikipedia.org/wiki/Scalable_Vector_Graphics" target="_blank">SVG</a> vector graphics chart.</p>
<p>The following command generates a vector graphics file that displays the events that take place during boot and startup. It only takes a few seconds to generate this file:</p>
<pre>
<code class="language-bash">[root@david ~]# systemd-analyze plot &gt; /tmp/bootup.svg</code></pre><p>This command creates an SVG, which is a text file that defines a series of graphic vectors that applications, including Image Viewer, Ristretto, Okular, Eye of Mate, LibreOffice Draw, and others, use to generate a graph. These applications process SVG files to create an image.</p>
<p>I used LibreOffice Draw to render a graph. The graph is huge, and you need to zoom in considerably to make out any detail. Here is a small portion of it:</p>
<p> </p>
<article class="align-center media media--type-image media--view-mode-full" title="The bootup.svg file displayed in LibreOffice Draw."><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/bootup.svg-graph.png" width="675" height="437" alt="The bootup.svg file displayed in LibreOffice Draw." title="The bootup.svg file displayed in LibreOffice Draw." /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(David Both, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article><p>The bootup sequence is to the left of the zero (0) on the timeline in the graph, and the startup sequence is to the right of zero. This small portion shows the kernel, <code>initrd</code>, and the processes <code>initrd </code>started.</p>
<p>This graph shows at a glance what started when, how long it took to start up, and the major dependencies. The critical path is highlighted in red.</p>
<p>Another command that generates graphical output is <code>systemd-analyze dot</code>. It generates textual dependency graph descriptions in <a href="https://en.wikipedia.org/wiki/DOT_(graph_description_language)" target="_blank">DOT</a> format. The resulting data stream is then piped through the <code>dot</code> utility, which is part of a family of programs that can be used to generate vector graphic files from various types of data. These SVG files can also be processed by the tools listed above.</p>
<p>First, generate the file. This took almost nine minutes on my primary workstation:</p>
<pre>
<code class="language-bash">[root@david ~]# time systemd-analyze dot | dot -Tsvg &gt; /tmp/test.svg
   Color legend: black     = Requires
                 dark blue = Requisite
                 dark grey = Wants
                 red       = Conflicts
                 green     = After

real    8m37.544s
user    8m35.375s
sys     0m0.070s
[root@david ~]#</code></pre><p>I won't reproduce the output here because the resulting graph is pretty much spaghetti. But you should try it and view the result to see what I mean.</p>
<h2 id="conditionals">Conditionals</h2>
<p>One of the more interesting, yet somewhat generic, capabilities I discovered while reading the <code>systemd-analyze(1)</code> man page is the <code>condition</code> subcommand. (Yes—I do read the man pages, and it is amazing what I have learned this way!) This <code>condition</code> subcommand can be used to test the conditions and asserts that can be used in systemd unit files.</p>
<p>It can also be used in scripts to evaluate one or more conditions—it returns a zero (0) if all are met or a one (1) if any condition is not met. In either case, it also spews text about its findings.</p>
<p>The example below, from the man page, is a bit complex. It tests for a kernel version between 4.0 and 5.1, that the host is running on AC power, that the system architecture is anything but ARM, and that the directory <code>/etc/os-release</code> exists. I added the <code>echo $?</code> statement to print the return code.</p>
<pre>
<code class="language-bash">[root@david ~]# systemd-analyze condition 'ConditionKernelVersion = ! &lt;4.0' \
                    'ConditionKernelVersion = &gt;=5.1' \
                    'ConditionACPower=|false' \
                    'ConditionArchitecture=|!arm' \
                    'AssertPathExists=/etc/os-release' ; \
echo $?
test.service: AssertPathExists=/etc/os-release succeeded.
Asserts succeeded.
test.service: ConditionArchitecture=|!arm succeeded.
test.service: ConditionACPower=|false failed.
test.service: ConditionKernelVersion=&gt;=5.1 succeeded.
test.service: ConditionKernelVersion=!&lt;4.0 succeeded.
Conditions succeeded.
0
[root@david ~]#</code></pre><p>The list of conditions and asserts starts around line 600 on the <code>systemd.unit(5)</code> man page.</p>
<h2 id="listing-configuration-files">Listing configuration files</h2>
<p>The <code>systemd-analyze</code> tool provides a way to send the contents of various configuration files to <code>STDOUT</code>, as shown here. The base directory is <code>/etc/</code>:</p>
<pre>
<code class="language-bash">[root@david ~]# systemd-analyze cat-config systemd/system/display-manager.service
# /etc/systemd/system/display-manager.service
[Unit]
Description=LXDM (Lightweight X11 Display Manager)
#Documentation=man:lxdm(8)
Conflicts=getty@tty1.service
After=systemd-user-sessions.service getty@tty1.service plymouth-quit.service livesys-late.service
#Conflicts=plymouth-quit.service

[Service]
ExecStart=/usr/sbin/lxdm
Restart=always
IgnoreSIGPIPE=no
#BusName=org.freedesktop.lxdm

[Install]
Alias=display-manager.service
[root@david ~]#</code></pre><p>This is a lot of typing to do nothing more than a standard <code>cat</code> command does. I find the next command a tiny bit helpful. It can search out files with the specified pattern within the standard systemd locations:</p>
<pre>
<code class="language-bash">[root@david ~]# systemctl cat backup*
# /etc/systemd/system/backup.timer
# This timer unit runs the local backup program
# (C) David Both
# Licensed under GPL V2
#

[Unit]
Description=Perform system backups
Requires=backup.service

[Timer]
Unit=backup.service
OnCalendar=*-*-* 00:15:30

[Install]
WantedBy=timers.target


# /etc/systemd/system/backup.service
# This service unit runs the rsbu backup program
# By David Both
# Licensed under GPL V2
#

[Unit]
Description=Backup services using rsbu
Wants=backup.timer

[Service]
Type=oneshot
Environment="HOME=/root"
ExecStart=/usr/local/bin/rsbu -bvd1
ExecStart=/usr/local/bin/rsbu -buvd2

[Install]
WantedBy=multi-user.target

[root@david ~]#</code></pre><p>Both of these commands preface the contents of each file with a comment line containing the file's full path and name.</p>
<h2 id="unit-file-verification">Unit file verification</h2>
<p>After creating a new unit file, it can be helpful to verify that its syntax is correct. This is what the <code>verify</code> subcommand does. It can list directives that are spelled incorrectly and call out missing service units:</p>
<pre>
<code class="language-bash">[root@david ~]# systemd-analyze verify /etc/systemd/system/backup.service</code></pre><p>Adhering to the Unix/Linux philosophy that "silence is golden," a lack of output messages means that there are no errors in the scanned file.</p>
<h2 id="security">Security</h2>
<p>The <code>security</code> subcommand checks the security level of specified services. It only works on service units and not on other types of unit files:</p>
<pre>
<code class="language-bash">[root@david ~]# systemd-analyze security display-manager 
  NAME                                                        DESCRIPTION                                                     &gt;
✗ PrivateNetwork=                                             Service has access to the host's network                        &gt;
✗ User=/DynamicUser=                                          Service runs as root user                                       &gt;
✗ CapabilityBoundingSet=~CAP_SET(UID|GID|PCAP)                Service may change UID/GID identities/capabilities              &gt;
✗ CapabilityBoundingSet=~CAP_SYS_ADMIN                        Service has administrator privileges                            &gt;
✗ CapabilityBoundingSet=~CAP_SYS_PTRACE                       Service has ptrace() debugging abilities                        &gt;
✗ RestrictAddressFamilies=~AF_(INET|INET6)                    Service may allocate Internet sockets                           &gt;
✗ RestrictNamespaces=~CLONE_NEWUSER                           Service may create user namespaces                              &gt;
✗ RestrictAddressFamilies=~…                                  Service may allocate exotic sockets                             &gt;
✗ CapabilityBoundingSet=~CAP_(CHOWN|FSETID|SETFCAP)           Service may change file ownership/access mode/capabilities unres&gt;
✗ CapabilityBoundingSet=~CAP_(DAC_*|FOWNER|IPC_OWNER)         Service may override UNIX file/IPC permission checks            &gt;
✗ CapabilityBoundingSet=~CAP_NET_ADMIN                        Service has network configuration privileges                    &gt;
✗ CapabilityBoundingSet=~CAP_SYS_MODULE                       Service may load kernel modules
&lt;SNIP&gt;
✗ CapabilityBoundingSet=~CAP_SYS_TTY_CONFIG                   Service may issue vhangup()                                     &gt;
✗ CapabilityBoundingSet=~CAP_WAKE_ALARM                       Service may program timers that wake up the system              &gt;
✗ RestrictAddressFamilies=~AF_UNIX                            Service may allocate local sockets                              &gt;

→ Overall exposure level for backup.service: 9.6 UNSAFE ?
lines 34-81/81 (END)</code></pre><p>Yes, the emoji is part of the output. But, of course, many services need pretty much complete access to everything in order to do their work. I ran this program against several services, including my own backup service; the results may differ, but the bottom line seems to be mostly the same.</p>
<p>This tool would be very useful for checking and fixing userspace service units in security-critical environments. I don't think it has much to offer for most of us.</p>
<h2 id="final-thoughts">Final thoughts</h2>
<p>This powerful tool offers some interesting and amazingly useful options. Much of what this article explores is about using <code>systemd-analyze</code> to provide insights into Linux's startup performance using systemd. It can also analyze other aspects of systemd.</p>
<p>Some of these tools are of limited use, and a couple should be forgotten completely. But most can be used to good effect when resolving problems with startup and other systemd functions.</p>
<h2 id="resources">Resources</h2>
<p>There is a great deal of information about systemd available on the internet, but much is terse, obtuse, or even misleading. In addition to the resources mentioned in this article, the following webpages offer more detailed and reliable information about systemd startup. This list has grown since I started this series of articles to reflect the research I have done.</p>
<ul><li>The <a href="https://man7.org/linux/man-pages/man5/systemd.unit.5.html" target="_blank">systemd.unit(5) manual page</a> contains a nice list of unit file sections and their configuration options along with concise descriptions of each.</li>
<li>The Fedora Project has a good, practical <a href="https://docs.fedoraproject.org/en-US/quick-docs/understanding-and-administering-systemd/index.html" target="_blank">guide to systemd</a>. It has pretty much everything you need to know in order to configure, manage, and maintain a Fedora computer using systemd.</li>
<li>The Fedora Project also has a good <a href="https://fedoraproject.org/wiki/SysVinit_to_Systemd_Cheatsheet" target="_blank">cheat sheet</a> that cross-references the old SystemV commands to comparable systemd ones.</li>
<li>Red Hat documentation contains a good description of the <a href="https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_basic_system_settings/managing-services-with-systemd_configuring-basic-system-settings#Managing_Services_with_systemd-Unit_File_Structure" target="_blank">Unit file structure</a> as well as other important information.  </li>
<li>For detailed technical information about systemd and the reasons for creating it, check out Freedesktop.org's <a href="https://www.freedesktop.org/wiki/Software/systemd/" target="_blank">description of systemd</a>.</li>
<li><a href="http://Linux.com" target="_blank">Linux.com</a>'s "More systemd fun" offers more advanced systemd <a href="https://www.linux.com/training-tutorials/more-systemd-fun-blame-game-and-stopping-services-prejudice/" target="_blank">information and tips</a>.</li>
</ul><p>There is also a series of deeply technical articles for Linux sysadmins by Lennart Poettering, the designer and primary developer of systemd. These articles were written between April 2010 and September 2011, but they are just as relevant now as they were then. Much of everything else good that has been written about systemd and its ecosystem is based on these papers.</p>
<ul><li><a href="http://0pointer.de/blog/projects/systemd.html" target="_blank">Rethinking PID 1</a></li>
<li><a href="http://0pointer.de/blog/projects/systemd-for-admins-1.html" target="_blank">systemd for Administrators, Part I</a></li>
<li><a href="http://0pointer.de/blog/projects/systemd-for-admins-2.html" target="_blank">systemd for Administrators, Part II</a></li>
<li><a href="http://0pointer.de/blog/projects/systemd-for-admins-3.html" target="_blank">systemd for Administrators, Part III</a></li>
<li><a href="http://0pointer.de/blog/projects/systemd-for-admins-4.html" target="_blank">systemd for Administrators, Part IV</a></li>
<li><a href="http://0pointer.de/blog/projects/three-levels-of-off.html" target="_blank">systemd for Administrators, Part V</a></li>
<li><a href="http://0pointer.de/blog/projects/changing-roots" target="_blank">systemd for Administrators, Part VI</a></li>
<li><a href="http://0pointer.de/blog/projects/blame-game.html" target="_blank">systemd for Administrators, Part VII</a></li>
<li><a href="http://0pointer.de/blog/projects/the-new-configuration-files.html" target="_blank">systemd for Administrators, Part VIII</a></li>
<li><a href="http://0pointer.de/blog/projects/on-etc-sysinit.html" target="_blank">systemd for Administrators, Part IX</a></li>
<li><a href="http://0pointer.de/blog/projects/instances.html" target="_blank">systemd for Administrators, Part X</a></li>
<li><a href="http://0pointer.de/blog/projects/inetd.html" target="_blank">systemd for Administrators, Part XI</a></li>
</ul>