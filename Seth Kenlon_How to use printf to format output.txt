<p>When I started learning Unix, I was introduced to the <code>echo</code> command pretty early in the process. Likewise, my initial <a href="https://opensource.com/resources/python">Python</a> lesson involved the <code>print</code> function. Picking up C++ and <a href="https://opensource.com/resources/python">Java</a> introduced me to <code>cout</code> and <code>systemout</code>. It seemed every language proudly had a convenient one-line method of producing output and advertised it like it was going out of style.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Programming and development</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ&amp;src=programming_resource_menu4">Red Hat Developers Blog</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Programming cheat sheets</a></li>
<li><a href="https://www.redhat.com/en/services/training/learning-subscription?intcmp=7016000000127cYAAQ">Try for free: Red Hat Learning Subscription</a></li>
<li><a href="https://opensource.com/downloads/bash-programming-guide?intcmp=7016000000127cYAAQ">eBook: An introduction to programming with Bash</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/bash-shell-cheat-sheet?intcmp=7016000000127cYAAQ">Bash Shell Scripting Cheat Sheet</a></li>
<li><a href="https://developers.redhat.com/e-books/modernizing-enterprise-java?intcmp=7016000000127cYAAQ">eBook: Modernizing Enterprise Java</a></li>
</ul></div>
</div>
</div>
</div>
<p>But once I turned the first page of intermediate lessons, I met <code>printf</code>, a cryptic, mysterious, and surprisingly flexible function. In going against the puzzling tradition of hiding <code>printf</code> from beginners, this article aims to introduce to the world the humble <code>printf</code> function and explain how it can be used in nearly any language.</p>
<h2 id="a-brief-history-of-printf">A brief history of printf</h2>
<p>The term <code>printf</code> stands for "print formatted" and may have first appeared in the <a href="https://opensource.com/article/20/6/algol68">Algol 68</a> programming language. Since its inclusion in C, <code>printf</code> has been reimplemented in C++, Java, Bash, PHP, and quite probably in whatever your favorite (post-C) language happens to be.</p>
<p>It's clearly popular, and yet many people seem to regard its syntax to be complex, especially compared to alternatives such as <code>echo</code> or <code>print</code> or <code>cout</code>. For example, here's a simple echo statement in Bash:</p>
<pre><code class="language-bash">$ echo hello
hello
$</code></pre><p>Here's the same result using <code>printf</code> in Bash:</p>
<pre><code class="language-bash">$ printf "%s\n" hello
hello
$</code></pre><p>But you get a lot of features for that added complexity, and that's exactly why <code>printf</code> is well worth learning.</p>
<h2 id="printf-output">printf output</h2>
<p>The main concept behind <code>printf</code> is its ability to format its output based on style information <em>separate</em> from the content. For instance, there is a collection of special sequences that <code>printf</code> recognizes as special characters. Your favorite language may have greater or fewer sequences, but common ones include:</p>
<ul><li><code>\n</code>: New line</li>
<li><code>\r</code>: Carriage return</li>
<li><code>\t</code>: Horizontal tab</li>
<li><code>\NNN</code>: A specific byte with an octal value containing one to three digits</li>
</ul><p>For example:</p>
<pre><code class="language-bash">$ printf "\t\123\105\124\110\n"
     SETH
$</code></pre><p>In this Bash example, <code>printf</code> renders a tab character followed by the ASCII characters assigned to a string of four octal values. This is terminated with the control sequence to produce a new line (<code>\n</code>).</p>
<p>Attempting the same thing with <code>echo</code> produces something a little more literal:</p>
<pre><code class="language-bash">$ echo "\t\123\105\124\110\n"
\t\123\105\124\110\n
$</code></pre><p>Using Python's <code>print</code> function for the same task reveals there's more to Python's <code>print</code> command than you might expect:</p>
<pre><code class="language-python">&gt;&gt;&gt; print("\t\123\n")
        S

&gt;&gt;&gt;</code></pre><p>Obviously, Python's <code>print</code> incorporates traditional <code>printf</code> features as well as the features of a simple <code>echo</code> or <code>cout</code>.</p>
<p>These examples contain nothing more than literal characters, though, and while they're useful in some situations, they're probably the least significant thing about <code>printf</code>. The true power of <code>printf</code> lies in format specification.</p>
<h2 id="format-output-with-printf">Format output with printf</h2>
<p>Format specifiers are characters preceded by a percent sign (<code>%</code>).<br /><br />
Common ones include:</p>
<ul><li><code>%s</code>: String</li>
<li><code>%d</code>: Digit</li>
<li><code>%f</code>: Floating-point number</li>
<li><code>%o</code>: A number in octal</li>
</ul><p>These are placeholders in a <code>printf</code> statement, which you can replace with a value you provide somewhere else in your <code>printf</code> statement. Where these values are provided depends on the language you're using and its syntax, but here's a simple example in Java:</p>
<pre><code class="language-java">string var="hello\n";
system.out.printf("%s", var);</code></pre><p>This, wrapped in appropriate boilerplate code and executed, renders:</p>
<pre><code class="language-text">$ ./example
hello
$</code></pre><p>It gets even more interesting, though, when the content of a variable changes. Suppose you want to update your output based on an ever-increasing number:</p>
<pre><code class="language-java">#include &lt;stdio.h&gt;

int main() {
  int var=0;
  while ( var &lt; 100) {
    var++;
  printf("Processing is %d%% finished.\n", var);
  }
  return 0;
}</code></pre><p>Compiled and run:</p>
<pre><code class="language-text">Processing is 1% finished.
[...]
Processing is 100% finished.</code></pre><p>Notice that the double <code>%%</code> in the code resolves to a single printed <code>%</code> symbol.</p>
<h2 id="limiting-decimal-places-with-printf">Limiting decimal places with printf</h2>
<p>Numbers can get complex, and <code>printf</code> offers many formatting options. You can limit how many decimal places are printed using the <code>%f</code> for floating-point numbers. By placing a dot (<code>.</code>) along with a limiter number between the percent sign and the <code>f</code>, you tell <code>printf</code> how many decimals to render. Here's a simple example written in Bash for brevity:</p>
<pre><code class="language-bash">$ printf "%.2f\n" 3.141519
3.14
$</code></pre><p>Similar syntax applies to other languages. Here's an example in C:</p>
<pre><code class="language-c">#include &lt;math.h&gt;
#include &lt;stdio.h&gt;

int main() {
  fprintf(stdout, "%.2f\n", 4 * atan(1.0));
  return 0;
}</code></pre><p>For three decimal places, use <code>.3f</code>, and so on.</p>
<h2 id="adding-commas-to-a-number-with-printf">Adding commas to a number with printf</h2>
<p>Since big numbers can be difficult to parse, it's common to break them up with a comma. You can have <code>printf</code> add commas as needed by placing an apostrophe (<code>'</code>) between the percent sign and the <code>d</code>:</p>
<pre><code class="language-bash">$ printf "%'d\n" 1024
1,024
$ printf "%'d\n" 1024601
1,024,601
$</code></pre><h2 id="add-leading-zeros-with-printf">Add leading zeros with printf</h2>
<p>Another common use for <code>printf</code> is to impose a specific format upon numbers in file names. For instance, if you have 10 sequential files on a computer, the computer may sort <code>10.jpg</code> before <code>1.jpg</code>, which is probably not your intent. When writing to a file programmatically, you can use <code>printf</code> to form the file name with leading zero characters. Here's an example in Bash for brevity:</p>
<pre><code class="language-bash">$ printf "%03d.jpg\n" {1..10}
001.jpg
002.jpg
[...]
010.jpg</code></pre><p>Notice that a maximum of 3 places are used in each number.</p>
<h2 id="using-printf">Using printf</h2>
<p>As you can tell from these <code>printf</code> examples, including control characters, especially <code>\n</code>, can be tedious, and the syntax is relatively complex. This is the reason shortcuts like <code>echo</code> and <code>cout</code> were developed. However, if you use <code>printf</code> every now and again, you'll get used to the syntax, and it will become second nature. I don't see any reason <code>printf</code> should be your <em>first</em> choice for printing statements during everyday activities, but it's a great tool to be comfortable enough with that it won't slow you down when you need it.</p>
<p>Take some time to learn <code>printf</code> in your language of choice, and use it when you need it. It's a powerful tool you won't regret having at your fingertips.</p>
