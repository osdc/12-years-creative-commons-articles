<p>Krusader is a simple, powerful, dual-panel file manager for the KDE Plasma Desktop and other Linux desktops. You can think of it as a power-user's alternative to <a href="https://opensource.com/article/22/12/linux-file-manager-dolphin" target="_blank">Dolphin</a>, and it follows the tradition of "commander" style file managers (such as <a href="https://opensource.com/article/22/12/linux-file-manager-midnight-commander" target="_blank">Midnight Commander</a>.) Krusader includes archive handling, mounted file system support, a file system search, directory synchronization, batch renaming, and much more.</p>
<h2 id="_install_krusader">Install Krusader</h2>
<p>On Linux, your distribution may package Krusader in its software repository. If so, you can use your package manager to install. For example, on Debian and Debian-based systems:</p>
<pre class="highlight">
<code class="language-bash" data-lang="bash">$ sudo apt install krusader</code></pre><p>If your distribution doesn't offer Krusader, you can download it from <a href="https://krusader.org/">krusader.org</a>.</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-12/krusader.webp" width="882" height="555" alt="Krusader is a dual-panel file manager" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Seth Kenlon, CC BY-SA 4.0)</p>
</div>
      
  </article><h2 id="_putting_you_in_control">Putting you in control</h2>
<p>It sounds aspirational, but Krusader truly is whatever file manager you want it to be. Its default presentation is pretty specific: it's a dual-pane file manager in the style of Midnight Commander but with the powerful underpinnings of the KDE Framework. As great as that is for many users, it doesn't do the application justice. Krusader is <em>extremely</em> configurable, to the point that you can use it as a single-pane file manager if you prefer.</p>
<p>Krusader is designed with the theory that by providing all users <em>all</em> of the tools, any single user can hide unwanted features and just use what they want. With so much available to you, it can admittedly take time to find what you like. However, the more you use Krusader, the more you find the features that make your life easier.</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-12/krusader-minimal.webp" width="676" height="521" alt="Krusader is a configurable file manager." /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Seth Kenlon, CC BY-SA 4.0)</p>
</div>
      
  </article><h2 id="_krusader_desktop">Krusader desktop</h2>
<p>There's more to Krusader than its configuration options, though. Krusader isn't really just a file manager. I think of it as a desktop in a window. If you're running a window manager instead of a full desktop, you know how much work it can be to assemble the features of a desktop.</p>
<p>For instance, a window manager doesn't have a device manager. When you attach a USB thumbdrive to your computer, a window manager doesn't alert you that a new device has been detected, and it doesn't offer to mount the file system.</p>
<p>Krusader has a device manager. You can mount and unmount file systems with the <strong>MountMan</strong> tool in the <strong>Tools</strong> window.</p>
<p>Krusader also has a disk usage monitor, so if you're wondering where all your hard drive space went, you can get a graphical report on what files are using up your space.</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-12/krusader-disk-usage.webp" width="880" height="490" alt="Krusader features a disk usage monitor." /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Seth Kenlon, CC BY-SA 4.0)</p>
</div>
      
  </article><p>Krusader supports archive formats, too, so you don't need to install an archive utility. Krusader can interact natively with <strong>ace</strong>, <strong>arj</strong>, <strong>bzip2</strong>, <strong>deb</strong>, <strong>gzip</strong>, <strong>iso</strong>, <strong>lha</strong>, <strong>rar</strong>, <strong>rpm</strong>, <strong>tar</strong>, <strong>zip</strong>, <strong>7-zip</strong>.</p>
<p>But wait, there's more. Because Krusader is built on top of the KDE Framework, it also understands all the KIO modules the Plasma Desktop can use. For instance, to quickly go to a <a href="https://opensource.com/article/21/12/file-sharing-linux-samba">Samba share</a>, you can use the <code>smb://</code> prefix in your URL, and you can log in to a remote system over SSH using <code>fish://</code> prefix.</p>
<p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">More Linux resources</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux commands cheat sheet">Linux commands cheat sheet</a></div>
              <div class="field__item"><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Advanced Linux commands cheat sheet">Advanced Linux commands cheat sheet</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Free online course: RHEL technical overview">Free online course: RHEL technical overview</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux networking cheat sheet">Linux networking cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="SELinux cheat sheet">SELinux cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux common commands cheat sheet">Linux common commands cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="What are Linux containers?">What are Linux containers?</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Register for your free Red Hat account">Register for your free Red Hat account</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Our latest Linux articles">Our latest Linux articles</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<h2 id="_custom_actions">Custom actions</h2>
<p>Krusader also features the <strong>Useractions</strong> menu, a place where you can define custom actions. An action can be an arbitrary command, or it can a hook into built-in Krusader functions.</p>
<p>To hook into Krusader, you use <em>placeholders</em>, which are keywords you can use in your commands.</p>
<p>A placeholder begins with a percent sign (<code>%</code>), then a <em>panel indicator</em>. There are five possible panel indicators:</p>
<ul><li>
<p><strong>a</strong>: active panel</p>
</li>
<li>
<p><strong>o</strong>: other panel (the one that's not active)</p>
</li>
<li>
<p><strong>l</strong>: left panel</p>
</li>
<li>
<p><strong>r</strong>: right panel</p>
</li>
<li>
<p><strong>_</strong>: panel not applicable</p>
</li>
</ul><p>For instance, suppose you want to create an action to convert a selection of images to the <a href="https://opensource.com/article/20/4/webp-image-compression">webp format</a>. The command you might use for this in a terminal is:</p>
<pre class="highlight">
<code class="language-bash" data-lang="bash">$ convert image_001.png image_001.webp</code></pre><p>But in Krusader, the name of the image must be variable, because it won't always be <code>image_001</code> that you want to convert. You need a placeholder.</p>
<p>Here's the command in Krusader:</p>
<pre class="highlight">
<code class="language-bash" data-lang="bash">convert %aList("Selected")% %aCurrent(0,0)%.webp</code></pre><p>This action invokes the <code>convert</code> command from ImageMagick and executes it on the current selection (whether it's one file or multiple files). Its destination is the current panel (the <code>0,0</code> in parentheses disables some optional features) with the file extension of <code>.webp</code>. These are the placeholders I find myself using the most:</p>
<ul><li>
<p><code>List</code>: selected items or a list of the <em>first parameter</em> (for instance, a list of paths)</p>
</li>
<li>
<p><code>Current</code>: the current item</p>
</li>
<li>
<p><code>Path</code>: the panel's path</p>
</li>
<li>
<p><code>Count</code>: the number of <em>first parameter</em> (for instance, the number of items selected in a <code>List</code>)</p>
</li>
<li>
<p><code>Filter</code>: the panel's filter mask</p>
</li>
<li>
<p><code>Select</code>: set what is selected</p>
</li>
<li>
<p><code>Goto</code>: change the panel's path</p>
</li>
<li>
<p><code>Ask</code>: get user input</p>
</li>
<li>
<p><code>Clipboard</code>: manipulate the clipboard</p>
</li>
<li>
<p><code>Each</code>: split a command into a list, and execute each command one after the other</p>
</li>
</ul><p>There are about 20 placeholders to learn, and they're all listed in the <strong>Krusader handbook</strong>, available from the <strong>Help</strong> menu.</p>
<h2 id="_krusader_is_powerful">Krusader is powerful</h2>
<p>Krusader has everything you need. It's configurable enough that you can emphasize the parts you want to use, or hide the parts you don't use often. Whether you use Krusader as a humble file manager or the all-encompassing interface to your Linux system, it's a satisfyingly powerful application, and you owe it to yourself to give it a try.</p>
