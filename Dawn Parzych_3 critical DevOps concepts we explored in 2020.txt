<p>Looking back through Opensource.com's articles about <a href="https://opensource.com/resources/devops">DevOps</a> in 2020, there was a bit of something for everyone—from people starting the DevOps journey to seasoned DevOps veterans. The articles focused on testing, software methodologies, and DevOps' most important part: the people. Here are the top 10 DevOps articles of 2020.</p>
<h2 id="test-driven-development">Test-driven development</h2>
<p>Alex Bunardzic shared <a href="https://opensource.com/users/alex-bunardzic">many articles about testing</a> this year, and I narrowed the list down to the top three. If you are interested in testing (and I would argue that anybody involved in writing or supporting software should have at least a passing interest in testing), check out these articles.</p>
<p>In <em><a href="https://opensource.com/article/20/1/test-driven-development">How to get started with test-driven development</a></em>, Alex uses an analogy to describe the fundamentals of test-driven development (TDD) and explain dependencies and how to eliminate them for testing purposes.</p>
<p>If you're going to practice TDD, you need to understand how and when to write a unit test. Alex shares his best practices in <a href="https://opensource.com/article/20/2/automate-unit-tests"><em>Writing unit test after writing code considered harmful in test-driven development</em></a>.</p>
<p>Testing is about determining if code is working as designed, and <a href="https://opensource.com/article/20/7/code-tdd"><em>What does it mean for code to "work"?</em></a> explores how to define what working code looks like from the perspective of observable behavior.</p>
<h2 id="software-methodologies">Software methodologies</h2>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More DevOps resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/resources/devops?intcmp=7013a00000263HlAAI">What is DevOps?</a></li>
<li><a href="https://opensource.com/downloads/devops-hiring-guide?intcmp=7013a00000263HlAAI">The ultimate DevOps hiring guide</a></li>
<li><a href="https://opensource.com/downloads/devops-monitoring-guide?intcmp=7013a00000263HlAAI">DevOps monitoring tools guide</a></li>
<li><a href="https://opensource.com/downloads/devsecops?intcmp=7013a00000263HlAAI">Getting started with DevSecOps</a></li>
<li><a href="https://enterprisersproject.com/cheat-sheet-devops-glossary?intcmp=7013a00000263HlAAI">Download the DevOps glossary</a></li>
<li><a href="https://www.ansible.com/resources/ebooks/ansible-for-devops?intcmp=7013a00000263HlAAI">eBook: Ansible for DevOps</a></li>
<li><a href="https://opensource.com/tags/devops?src=devops_resource_menu2">Latest DevOps articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>People sometimes ask, "How does 'concept x' relate to DevOps?" Both <a href="https://opensource.com/article/20/2/devops-vs-agile"><em>DevOps vs. Agile: What's the difference?</em></a> by Tonya Brown and <a href="https://opensource.com/article/20/4/kanban-devops"><em>How does kanban relate to DevOps</em></a><em>?</em> by Willy-Peter Schaub address these questions head-on. Willy explains the four pivotal kanban practices then dives into how they are similar to DevOps. Tonya highlights similarities and differences between DevOps and agile. If you're starting out on your DevOps journey, these articles will help you understand how DevOps is similar to and different from your current practices.</p>
<p>And speaking of starting out, Sameer S Paradkar's article <a href="https://opensource.com/article/20/2/devops-beginners"><em>A beginner's guide to everything DevOps</em></a> should be bookmarked and shared with anyone new to the practice. He covers everything from how DevOps addresses development and operations challenges, to the differences between traditional IT processes and DevOps, to understanding the DevOps lifecycle. To top it off, Sameer ends with a handy cheat sheet to open source DevOps tools.</p>
<h2 id="people">People</h2>
<p>The people are the most important part of DevOps. They are the ones writing the software, supporting it, building automation, and so much more. Finding the right people is more important than implementing the right tools.</p>
<p>The right mindset is required to navigate DevOps. In <a href="https://opensource.com/article/20/6/devops-mindset"><em>10 tips for maintaining a DevOps mindset for distributed teams</em></a>, Willy-Peter Shaub shares how that mindset helps in the new era of remote work:</p>
<blockquote><p>"A healthy DevOps mindset navigates through different paths of continuous improvement wherein disruption, discipline, and guardrails are the norm. What no one anticipated is the radical disruption we are all experiencing due to the pandemic, and the impact it has on our DevOps and personal mindset, our workflows, and the ceremonies of kanban and agile teams."</p>
</blockquote>
<p>This mindset is crucial as teams evolve how we collaborate, communicate, and deliver software for the remote world.</p>
<p>Yet, it's not the only thing you need to be successful in DevOps. Josh Atwell shared his list of key skills for DevOps success in <a href="https://opensource.com/article/20/3/devops-relationships"><em>How to be the right person for DevOps</em></a>. It may come as a surprise, but none of the skills are related to programming languages or tools. Being the right person for DevOps requires communication, selflessness, and self-care, Josh says.</p>
<p>Even the right mindset and attitude won't make everything perfect; there will still be challenges. One of the challenges people face today is burnout. People are getting tired of being remote, and they are looking for things to return to "normal." In <a href="https://opensource.com/article/20/1/devops-burnout-solution"><em>DevOps is a solution to burnout worth investing in</em></a>, I discuss how changing underlying processes and cultures can help reduce or prevent burnout.</p>
<p>Finally, if you're looking for a career change in 2021, consider the role of scrum master. Tonya Brown outlined a day in the life of a scrum master in <a href="https://opensource.com/article/20/7/scrum-master"><em>What does a scrum master do?</em></a> If you like removing impediments and coaching people on solving problems, this may be a role for you.</p>
<hr /><p>What DevOps topics are you interested in reading about? Please share your ideas in the comments, and if you have knowledge to share, please <a href="https://opensource.com/how-submit-article">consider writing about it for Opensource.com</a>.</p>
