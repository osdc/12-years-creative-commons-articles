<p><a href="https://flutter.dev/" target="_blank">Flutter</a> is a popular project among mobile developers around the world. The framework has a massive, friendly community of enthusiasts, which continues to grow as Flutter helps programmers take their projects into the mobile space.</p>
<p>This tutorial is meant to help you start doing mobile development with Flutter. After reading it, you'll know how to quickly install and set up the framework to start coding for smartphones, tablets, and other platforms.</p>
<p>This how-to assumes you have <a href="https://developer.android.com/studio" target="_blank">Android Studio</a> installed on your computer and some experience working with it.</p>
<h2 id="what-is-flutter">What is Flutter?</h2>
<p>Flutter enables developers to build apps for several platforms, including:</p>
<ul><li>Android</li>
<li>iOS</li>
<li>Web (in beta)</li>
<li>macOS (in development)</li>
<li>Linux (in development)</li>
</ul><p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Great Content</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=7016000000127cYAAQ">Free online course: RHEL technical overview</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=7016000000127cYAAQ">Learn Advanced Linux Commands</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Download Cheat Sheets</a></li>
<li><a href="https://opensource.com/alternatives?intcmp=7016000000127cYAAQ">Find an Open Source Alternative</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=7016000000127cYAAQ">Read Top Linux Content</a></li>
<li><a href="https://opensource.com/resources?intcmp=7016000000127cYAAQ">Check out open source resources</a></li>
</ul></div>
</div>
</div>
</div>
<p>Support for macOS and Linux is in early development, while web support is expected to be released soon. This means that you can try out its capabilities now (as I'll describe below).</p>
<h2 id="install-flutter">Install Flutter</h2>
<p>I'm using Ubuntu 18.04, but the installation process is similar with other Linux distributions, such as Arch or Mint.</p>
<h3 id="install-with-snapd">Install with snapd</h3>
<p>To install Flutter on Ubuntu or similar distributions using <a href="https://snapcraft.io/docs/getting-started" target="_blank">snapd</a>, enter this in a terminal:</p>
<pre><code class="language-bash">$ sudo snap install flutter --classic

$ sudo snap install flutter –classic
flutter 0+git.142868f from flutter Team/ installed</code></pre><p>Then launch it using the <code>flutter</code> command. Upon the first launch, the framework downloads to your computer:</p>
<pre><code class="language-bash">$ flutter
Initializing Flutter
Downloading https://storage.googleapis.com/flutter_infra[...]</code></pre><p>Once the download is finished, you'll see a message telling you that Flutter is initialized:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Flutter initialized"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/flutter1_initialized.png" width="675" height="215" alt="Flutter initialized" title="Flutter initialized" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Vitaly Kuprenko, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<h3 id="install-manually">Install manually</h3>
<p>If you don't have snapd or your distribution isn't Ubuntu, the installation process will be a little bit different. In that case, <a href="https://flutter.dev/docs/get-started/install/linux" target="_blank">download</a> the version of Flutter recommended for your operating system.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Install Flutter manually"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/flutter2_manual-install.png" width="675" height="156" alt="Install Flutter manually" title="Install Flutter manually" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Vitaly Kuprenko, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>Then extract it to your home directory.</p>
<p>Open the <code>.bashrc</code> file in your home directory (or <code>.zshrc</code> if you use the <a href="https://opensource.com/article/19/9/getting-started-zsh">Z shell</a>) in your favorite text editor. Because it's a hidden file, you must first enable showing hidden files in your file manager or open it from a terminal with:</p>
<pre><code class="language-bash">$ gedit ~/.bashrc &amp;</code></pre><p>Add the following line to the end of the file:</p>
<pre><code class="language-bash">export PATH="$PATH:~/flutter/bin"</code></pre><p>Save and close the file. Keep in mind that if you extracted Flutter somewhere other than your home directory, the <a href="https://opensource.com/article/17/6/set-path-linux">path to Flutter SDK</a> will be different.</p>
<p>Close your terminal and then open it again so that your new configuration loads. Alternatively, you can source the configuration with:</p>
<pre><code class="language-bash">$ . ~/.bashrc</code></pre><p>If you don't see an error, then everything is fine.</p>
<p>This installation method is a little bit harder than using the <code>snap</code> command, but it's pretty versatile and lets you install the framework on almost any distribution.</p>
<h3 id="check-the-installation">Check the installation</h3>
<p>To check the result, enter the following in the terminal:</p>
<pre><code class="language-bash">flutter doctor -v</code></pre><p>You'll see information about installed components. Don't worry if you see errors. You haven't installed any IDE plugins for working with Flutter SDK yet.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Checking Flutter installation with the doctor command"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/flutter3_doctor.png" width="675" height="464" alt="Checking Flutter installation with the doctor command" title="Checking Flutter installation with the doctor command" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Vitaly Kuprenko, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<h2 id="install-ide-plugins">Install IDE plugins</h2>
<p>You should install plugins in your <a href="https://www.redhat.com/en/topics/middleware/what-is-ide">integrated development environment (IDE)</a> to help it interface with the Flutter SDK, interact with devices, and build code.</p>
<p>The three main IDE tools that are commonly used for Flutter development are IntelliJ IDEA (Community Edition), Android Studio, and VS Code (or <a href="https://opensource.com/article/20/6/open-source-alternatives-vs-code">VSCodium</a>). I'm using Android Studio in this tutorial, but the steps are similar to how they work on IntelliJ IDEA (Community Edition) since they're built on the same platform.</p>
<p>First, launch <strong>Android Studio</strong>. Open <strong>Settings</strong> and go to the <strong>Plugins</strong> pane, and select the <strong>Marketplace</strong> tab. Enter <strong>Flutter</strong> in the search line and click <strong>Install</strong>.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Flutter plugins"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/flutter4_plugins.png" width="675" height="456" alt="Flutter plugins" title="Flutter plugins" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Vitaly Kuprenko, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>You'll probably see an option to install the <strong>Dart</strong> plugin; agree to it. If you don't see the Dart option, then install it manually by repeating the steps above. I also recommend using the <strong>Rainbow Brackets</strong> plugin, which makes code navigation easier.</p>
<p>That's it! You've installed all the plugins you need. You can check by entering a familiar command in the terminal:</p>
<pre><code class="language-bash">flutter doctor -v</code></pre><p>
<article class="align-center media media--type-image media--view-mode-full" title="Checking Flutter plugins with the doctor command"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/flutter5_plugincheck.png" width="675" height="444" alt="Checking Flutter plugins with the doctor command" title="Checking Flutter plugins with the doctor command" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Vitaly Kuprenko, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<h2 id="build-your-hello-world-application">Build your "Hello World" application</h2>
<p>To start a new project, create a Flutter project:</p>
<ol><li>Select <strong>New -&gt; New Flutter project</strong>.<br /><br /><article class="align-center media media--type-image media--view-mode-full" title="Creating a new Flutter plugin"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/flutter6_newproject.png" width="675" height="510" alt="Creating a new Flutter plugin" title="Creating a new Flutter plugin" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Vitaly Kuprenko, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></li>
<li>In the window, choose the type of project you want. In this case, you need <strong>Flutter Application</strong>.</li>
<li>Name your project <strong>hello_world</strong>. Note that you should use a merged name, so use an underscore instead of a space. You may also need to specify the path to the SDK.<br /><br /><article class="align-center media media--type-image media--view-mode-full" title="Naming a new Flutter plugin"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/flutter7_projectname.png" width="675" height="495" alt="Naming a new Flutter plugin" title="Naming a new Flutter plugin" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Vitaly Kuprenko, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></li>
<li>Enter the package name.</li>
</ol><p>You've created a project! Now you can launch it on a device or by using an emulator.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Device options in Flutter"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/flutter8_launchflutter.png" width="319" height="120" alt="Device options in Flutter" title="Device options in Flutter" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Vitaly Kuprenko, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>Select the device you want and press <strong>Run</strong>. In a moment, you will see the result.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Flutter demo on mobile device"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/flutter9_demo.png" width="365" height="675" alt="Flutter demo on mobile device" title="Flutter demo on mobile device" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Vitaly Kuprenko, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>Now you can start working on an <a href="https://opensource.com/article/18/6/flutter">intermediate project</a>.</p>
<h2 id="try-flutter-for-web">Try Flutter for web</h2>
<p>Before you install Flutter components for the web, you should know that Flutter's support for web apps is pretty raw at the moment. So it's not a good idea to use it for complicated projects yet.</p>
<p>Flutter for web is not active in the basic SDK by default. To switch it on, go to the beta channel. To do this, enter the following command in the terminal:</p>
<pre><code class="language-bash">flutter channel beta</code></pre><p>
<article class="align-center media media--type-image media--view-mode-full" title="flutter channel beta output"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/flutter10_beta.png" width="675" height="217" alt="flutter channel beta output" title="flutter channel beta output" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Vitaly Kuprenko, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>Next, upgrade Flutter according to the beta branch by using the command:</p>
<pre><code class="language-bash">flutter upgrade</code></pre><p>
<article class="align-center media media--type-image media--view-mode-full" title="flutter upgrade output"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/flutter11_upgrade.png" width="675" height="190" alt="flutter upgrade output" title="flutter upgrade output" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Vitaly Kuprenko, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>To make Flutter for web work, enter:</p>
<pre><code class="language-bash">flutter config --enable-web</code></pre><p>Restart your IDE; this helps Android Studio index the new IDE and reload the list of devices. You should see several new devices:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Flutter for web device options"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/flutter12_new-devices.png" width="331" height="167" alt="Flutter for web device options" title="Flutter for web device options" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Vitaly Kuprenko, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>Selecting <strong>Chrome</strong> launches an app in the browser, while <strong>Web Server</strong> gives you the link to your web app, which you can open in any browser.</p>
<p>Still, it's not time to rush into development because your current project doesn't support the web. To improve it, open the terminal in the project's root and enter:</p>
<pre><code class="language-bash">flutter create</code></pre><p>This command recreates the project, adding web support. The existing code won't be deleted.</p>
<p>Note that the tree has changed and now has a "web" directory:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="File tree with web directory"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/flutter13_tree.png" width="324" height="372" alt="File tree with web directory" title="File tree with web directory" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Vitaly Kuprenko, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>Now you can get to work. Select <strong>Chrome</strong> and press <strong>Run</strong>. In a moment, you'll see the browser window with your app.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Flutter web app demo"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/flutter14_webapp.png" width="675" height="610" alt="Flutter web app demo" title="Flutter web app demo" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Vitaly Kuprenko, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>Congratulations! You've just launched a project for the browser and can continue working with it as with any other website.</p>
<p>All of this comes from the same codebase because Flutter makes it possible to write code for both mobile platforms and the web with little to no changes.</p>
<h2 id="do-more-with-flutter">Do more with Flutter</h2>
<p>Flutter is a powerful tool for mobile development, and moreover, it's an important evolutionary step toward cross-platform development. Learn it, use it, and deliver your apps to all the platforms!</p>
