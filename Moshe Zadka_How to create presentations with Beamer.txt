<p><a href="https://www.overleaf.com/learn/latex/Beamer" target="_blank">Beamer</a> is a LaTeX package for generating presentation slide decks. One of its nicest features is that it can take advantage of LaTeX's powerful typesetting system and all the other packages in its ecosystem. For example, I often use LaTeX's <a href="https://www.overleaf.com/learn/latex/Code_listing" target="_blank">listings</a> package in Beamer presentations that include code.</p>
<h2 id="starting-a-presentation">Starting a presentation</h2>
<p>To begin a Beamer document, enter:</p>
<pre><code class="language-text">\documentclass{beamer}</code></pre><p>As you would with any other LaTeX document, add any packages you want to use. For example, to use the <strong>listings</strong> package, enter:</p>
<pre><code class="language-text">\usepackage{listings}</code></pre><p>Place all content inside the <strong>document </strong>environment:</p>
<pre><code class="language-text">\begin{document}</code></pre><p>Beamer documents are usually a sequence of <strong>frame </strong>environments. Frames that contain code should be marked <strong>fragile</strong>:</p>
<pre><code class="language-text">\begin{frame}[fragile]</code></pre><p>Begin your frames with a title:</p>
<pre><code class="language-text">\frametitle{Function to Do Stuff}</code></pre><h2 id="testing-your-code-before-you-present-it">Testing your code before you present it</h2>
<p>One of the worst feelings in the world is giving a talk and realizing, as you walk through the code, that there is a glaring bug in it—maybe a misspelled keyword or an unclosed brace.</p>
<p>The solution is to <em>test</em> code that is presented. In most presentation environments, this means creating a separate file, writing tests, then copying and pasting.</p>
<p>However, with Beamer, there is a better way. Imagine you have a file named <strong>do_stuff.py</strong> that contains code. You can write tests for the <strong>do_stuff.py</strong> code in a second file, which you call <strong>test_do_stuff.py</strong>, and can exercise it with, say, <a href="https://docs.pytest.org/en/latest/" target="_blank">pytest</a>. However, most of the lines in <strong>do_stuff.py</strong> lack pedagogic value, like defining helper functions.</p>
<p>To simplify things for your audience, you can import just the lines you want to talk about into the frame in your presentation :</p>
<pre><code class="language-text">\lstinputlisting[
    language=Python,
    firstline=8,
    lastline=15
]{do_stuff.py}</code></pre><p>Since you will be talking through those lines (from 8 to 15), you don't need any other content on the slide. Close the frame:</p>
<pre><code class="language-text">\end{frame}</code></pre><p>On the next slide, you want to show a usage example for the <strong>do_stuff()</strong> function you just presented:</p>
<pre><code class="language-text">\begin{frame}[fragile]
\frametitle{Calling Function to Do Stuff}
\lstinputlisting[
    language=Python,
    firstline=17,
    lastline=19
]{do_stuff.py}
\end{frame}</code></pre><p>You use the same file, but this time you show the lines that <em>call</em> the function. Finally, close the document:</p>
<pre><code class="language-text">\end{document}</code></pre><p>Assuming you have an appropriate Python file in <strong>do_stuff.py</strong>, this will produce a short two-slide presentation.</p>
<p>Beamer also supports necessary features such as progressive revelation, showing only one bullet at a time to prevent the audience from being distracted by reading ahead.": <strong>\pause</strong> inside a list will divide bullets into pages:</p>
<pre><code class="language-text">\begin{frame}
Remember:
\begin{itemize}
\item This will show up on the first slide. \pause
\item This will show up on the
      second slide, as well as the preceding point. \pause
\item Finally, on the third slide,
       all three bullets will show up.
\end{frame}</code></pre><h2 id="creating-handouts">Creating handouts</h2>
<p>My favorite feature in Beamer is that you can set it to <em>ignore</em> everything outside a frame with <strong>\documentclass[ignorenonframetext]{beamer}</strong>. When I prepare a presentation, I leave off the top (where the document class is declared) and auto-generate two versions of it: one with Beamer that ignores all text outside any frame, which I use for my presentation, and one with a header like:</p>
<pre><code class="language-text">\documentclass{article}
\usepackage{beamerarticle}</code></pre><p>which generates a handout—a PDF that has all the frames <em>and</em> all the text between them.</p>
<p>When a conference organizer asks me to publish my slides, I include the original slide deck as a reference, but the main thing I like people to have is the handout, which has all the explanatory text that I don't want to include on the slide deck itself.</p>
<p>When creating presentation slides, people often wonder whether it's better to optimize their materials for the presentation or for people who want to read them afterward. Fortunately, Beamer provides the best of both worlds.</p>
