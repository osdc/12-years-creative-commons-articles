<p>This is an exciting time for HR—the use of analytics is predictably changing the conversations and the perception around HR as a function. Most organizations are convinced of the role people analytics can play in making HR a strategic partner to the C-suite, thus firmly involved in the agenda of business transformation. While people analytics has been one of the most crucial tools to usher in this change, the unintended effect is that is has opened up a big debate about transparency vs. privacy in the workplace.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Learn about open organizations</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://theopenorganization.org">Download resources</a></li>
<li><a href="https://theopenorganization.community">Join the community</a></li>
<li><a href="https://opensource.com/open-organization/resources/open-org-definition?src=too_resource_menu3a">What is an open organization?</a></li>
<li><a href="https://opensource.com/open-organization/resources/open-org-maturity-model?src=too_resource_menu4a">How open is your organization?</a></li>
</ul></div>
</div>
</div>
</div>

<p>We know that the application of people analytics, whether it be descriptive or predictive, shows insights into peoples' behaviors, connections, and performance hitherto unknown. There have been many studies showing that organizations that create an analytics-driven (fact-based) culture outperform their competition. Done right, we know that analytics delivers insights to make better decisions.</p>
<p>But when does <em>too much</em> cross the line and violate our privacy? People analytics is a relatively new discipline, without regulation to help guide us on these issues. When does <em>too much</em> slip from grey into black? Who should have access to the people data? What analyses will the data be used for? How will the findings be shared and applied, and with whom? All-important questions for both organizations and employees—especially when you start segmenting the data into categories of age, tenure, gender, race, performance, background, test scores, and other such sensitive information. Analytics has the potential to generate business value through talent outcomes. However, analytics professionals often struggle to find balance and answer to the complex set of privacy challenges.</p>
<p>According to a study by the <a href="https://www.conference-board.org/">Conference Board</a>, 63% of employees' lack confidence and believe that their employer is tracking sensitive data about them privately, and 72% believe their companies are not telling them what data they are collecting.</p>
<p>In this atmosphere of mistrust in the workplace, people analytics professionals need to move towards bringing greater transparency in the process. The challenge for the HR professional today is to link the existing talent systems to the culture necessary for developing talent that brings value to the organization. Greater buy-in will only happen when employees are convinced that the data generated will be used for improving efficiency, effectiveness and employee experience.</p>
<p></p><div class="embedded-callout-text callout-float-right">In this atmosphere of mistrust in the workplace, people analytics professionals need to move towards bringing greater transparency in the process.</div>
<p>Analytics professionals need to combine both privacy and transparency to drive the best in individual performance and build capabilities and behaviors that ensure better performance. Transparency will reinforce the need to become a fact-based organization that uses data for decision making and will be crucial in strengthening the case for people analytics.</p>
<p>Here are three things you can do if you are planning to implement people analytics in the organization but need help in tackling questions about privacy.</p>
<p><strong>1. Have a strategic plan around People Management supported by analytics </strong><br /><br />
Ensure that you have answers to strategic challenges for the organization; for example answer questions like what skills the organization will need to focus on in the future. When you have data backed answers to questions of critical skill that are going to be needed in the future given the changing market dynamics and workforce trend data, more employees will see the merit in people analytics.</p>
<p><strong>2. Build employee capabilities to win the confidence </strong><br /><br />
Use analytics to create the performance management framework and develop a work plan that builds capabilities required to support organizational changes and strategic initiatives. For example, what is the correlation between skill and performance, how to match the right skills for specific business challenges, improve productivity through simplification of processes, managing attrition, etc. can be answered using people analytics effectively.</p>
<p><strong>3. Use analytics to drive transformation initiatives </strong><br /><br />
Help redirect HR conversations from tactical to strategic ones by revolutionizing HR processes and decision making. By using people analytics in restructuring HR processes, a culture of higher efficiency and productivity could be developed that will ultimately impact employee experience. The resulting transformation mandate will help the organization develop a sustainable people analytics capability.</p>
<p>In the end, when people analytics is well applied within the broader framework of ethical, fair business environment, it has a higher ability to drive positive change. It can move both the organization and its employees in a growth trajectory often in complementary ways.</p>
<p>I hope you'll <a href="http://genepease.com/">join me</a> as we continue to explore this important issue.</p>
<p><em><u>This article <a href="http://genepease.com/people-analytics-privacy-vs-transparency/">originally appeared at Genepease.com</a> and is republished here with the author's permission. Copyright Gene Pease.</u></em></p>
