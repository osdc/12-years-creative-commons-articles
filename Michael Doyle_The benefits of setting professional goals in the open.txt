<p>Self-assessments—reflection of your current abilities, and identification of areas that need focus to create future professional opportunities—are integral to personal development. In open organizations, self-assessments are most effective when they're <em>transparent</em> and <em>collaborative</em>. I'd like to share the simple process I followed when opening up my own self-assessment, so you'll have some ideas you can take forward to start your own collaborative skills assessment.</p>
<h2>From inspiration to action</h2>
<p>In 2017, a colleague inspired me when he who took four months off work to hike the Appalachian Trail. When he returned and shared his experience, he said something that made me stop and think: He said, "I now know what it's like to set a big goal and achieve it by working hard at it every day."</p>
<p>This made me want to do the same thing—set a big goal for myself and work at it to achieve it (not necessarily hike the Appalachian Trail). "Great idea," I thought, "but where do I start?"</p>
<p>To answer that question I decided to create a development plan for myself, because it seemed that doing this would uncover a purposeful big goal I could work towards. The funny thing is that using a development plan to identify a big goal did not immediately lead to an answer. Instead it produced another question: <em>How do I create a personal development plan?</em></p>
<p>To tackle that, I decided to meet with a colleague who has a wealth of experience in the development planning process. During the course of an hour, via video, across continents and hemispheres, he helped me understand how to think about creating a development plan. He pointed out that development planning is simply the process of truly understanding where you are <em>now</em> and where you want to be <em>in the future</em>. Only then can you define the actions you need to take to get there.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Learn about open organizations</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://theopenorganization.org">Download resources</a></li>
<li><a href="https://theopenorganization.community">Join the community</a></li>
<li><a href="https://opensource.com/open-organization/resources/open-org-definition?src=too_resource_menu3a">What is an open organization?</a></li>
<li><a href="https://opensource.com/open-organization/resources/open-org-maturity-model?src=too_resource_menu4a">How open is your organization?</a></li>
</ul></div>
</div>
</div>
</div>

<p><em>A-ha</em>, I thought, <em>I now have my first step: Assess where I am now</em>. But along came another question: <em>What's the best way to assess my own skills in an open organization?</em></p>
<h2>The power of including others</h2>
<p><em>The Open Organization</em> underscores the power of including others in your decision making. I figured that if I was going to do an assessment of my own skills and make some decisions about my strengths and areas for opportunity, then I should leverage the power of others in that process to expand my thinking and validate my findings.</p>
<p>I sent an email to my local office explaining what I was trying to achieve, and I asked for anyone interested in assessing their own skills to come and join me for a skills assessment workshop. Lo and behold, a few people actually took me up on the offer.</p>
<p>The workshop's objective was to identify two behaviors participants considered current strengths and two behaviors they'd like to develop. I invited the participants to split into groups of two and set a timed exercise for the first person to sort a list of defined behaviors into three groups:</p>
<ol><li>Current strength</li>
<li>Neutral (neither a strength nor an area to develop)</li>
<li>Opportunity for development</li>
</ol><p>At the end of this exercise, we reset the timer and instructed the first person to choose two behaviors in current strengths and two behaviors in opportunity for development. We encouraged the second person to contribute by asking non-judgemental, open questions to the first person to help them with their thinking, questions like:</p>
<ul><li>Where can you see large groups of similar behaviors? What does that say to you?</li>
<li>What behavior, if you focussed on it, would propel you the fastest in the next 12 months?</li>
<li>How could you leverage your strengths to boost a behavior you want to improve?</li>
<li>Which behaviors excite you? Where do you feel energy coming from?</li>
<li>Are there any strengths you're overusing to compensate for an area you're not focussing on? What would happen if you focused developing that underdeveloped behavior?</li>
</ul><p>Including others in this way was a powerful part of the self assessment workshop. One participant summed it up perfectly:</p>
<blockquote><p>"Taking the time to assess your skills really pays off when you do this with another colleague— especially, someone who works with you and can give you valuable feedback."</p></blockquote>
<p>Once we'd identified potential behaviors we thought were strengths (and some behaviors we wanted to develop) we then needed a mechanism to validate our choices. I was talking to a director in the organization about this point, and she gave me some great advice. She said, "Sit down with a colleague who knows you really well and show them your choices. You'll be able to tell quickly by their reaction if they are the right behaviors." When I tried this, it worked exactly as she had described; my colleague's reactions and words helped me validate my choices.</p>
<p>Next, I need to create some <em>goals</em> around the behaviors I'm looking to develop, then gather additional feedback on those.</p>
<h2>A journey, one step at a time</h2>
<p>Personal development is a journey. But like all journeys, it happens one step at a time. If you don't take that first step in assessing your own skills, then you'll never embark on the journey.</p>
<p>If you're looking to get started on your development plan—or even if you're currently executing one—identify ways you could include others to get feedback or inform your next choice of action.</p>
<p>If you're a manager looking to support development in your team, share this collaborative self-assessment approach with them and listen to their thoughts and feedback. Think about how you could support your team by breaking down the development planning process into smaller milestones that you can discuss at your regular one-on-ones.</p>
<p><em><a href="https://opensource.com/open-organization/resources/newsletter">Subscribe to our weekly newsletter</a> to learn more about open organizations.</em></p>
