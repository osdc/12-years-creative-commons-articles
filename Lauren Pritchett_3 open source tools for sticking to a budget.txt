<p>In light of changing economical times in many countries around the world right now, you may find yourself wanting to revamp or improve your financial situation and your understanding of it. Or, perhaps one of your New Year's resolutions was to start budgeting? You are not alone.</p>
<p>The best way to stick to that resolution is to keep track of the money you are spending and making on a regular basis. The problem is, many popular personal finance apps out there are proprietary. Are you looking for an open source alternative to Quicken, Mint, or You Need a Budget? Whether you are new to open source software or new to budgeting in general, one of these tools will suit your needs and comfort level. </p>
<h2>LibreOffice Calc</h2>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Great Content</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=7016000000127cYAAQ">Free online course: RHEL technical overview</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=7016000000127cYAAQ">Learn Advanced Linux Commands</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Download Cheat Sheets</a></li>
<li><a href="https://opensource.com/alternatives?intcmp=7016000000127cYAAQ">Find an Open Source Alternative</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=7016000000127cYAAQ">Read Top Linux Content</a></li>
<li><a href="https://opensource.com/resources?intcmp=7016000000127cYAAQ">Check out open source resources</a></li>
</ul></div>
</div>
</div>
</div>
<p>I recommend using LibreOffice Calc to any open source newcomer who wants to start budgeting. If you've used proprietary spreadsheets like Google Sheets or Microsoft Excel, the LibreOffice Calc template will be very familiar. In this <a href="https://opensource.com/article/20/3/libreoffice-open-source-budget">budgeting tutorial</a>, author Jess Weichler has done a lot of the work for you by including a handy, downloadable template. The template already has expense categories like utility bills, groceries, dining out, and more, but you have the freedom to customize it to your lifestyle. In a second article, she shows you how to <a href="https://opensource.com/article/20/3/libreoffice-templates">create your own templates</a>. </p>
<h2>HomeBank</h2>
<p>HomeBank is another great option for open source users of all levels. HomeBank is free, yet it has plenty of analysis and transaction features like its proprietary counterparts. In fact, you can export and import Quicken files to HomeBank, which makes transitioning to this open source app a breeze. Finally, you can use a tool to help you make wise decisions about your money without spending more money. Learn how to get started in Jessica Cherry's <a href="https://opensource.com/article/20/2/open-source-homebank">tutorial</a>. </p>
<h2>GnuCash</h2>
<p>Like the other budgeting tools mentioned here, GnuCash can be used on Windows, macOS, and Linux. There is a slew of documentation available, but our very own Don Watkins guides you through the steps of setting up GnuCash on Linux in <a href="https://opensource.com/article/20/2/gnucash">this tutorial</a>. Not only is GnuCash a great option for taking control of your personal finances, but it also has features like invoicing to help you manage your small business. </p>
<h2>Bonus: OpenTaxSolver</h2>
<p>The dreaded tax season can be a stressful time for many Americans. Many folks purchase TurboTax or use an accountant or tax service to do their taxes. Contrary to popular belief, open source tax preparation software exists! Author Jessica Cherry did the belaboring research and introduces our readers to OpenTaxSolver in <a href="https://opensource.com/article/20/2/open-source-taxes">this article</a>. To use OpenTaxSolver successfully, you'll need keen attention to detail, but you won't have to worry about doing the complicated math. </p>
<p>Which open source budgeting app will you try? Do you have a favorite tool that I didn't mention in this list? Share your thoughts in the comments. </p>
