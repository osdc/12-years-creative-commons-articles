<p>Running applications with containers and orchestrating their lifecycles with <a href="https://kubernetes.io/" target="_blank">Kubernetes</a> has transformed how teams manage large-scale deployments. Containers are typically used to perform a short-lived task, such as sending a message, or running an application that does not need to store information, such as a web server. A new container can be created that's unaware of any work that previously occurred—in other words, be "stateless"—and this is completely fine for most applications.</p>
<p>The important part of a database system is that it maintains state: a process, while it is running, can modify the state of the data on disk, and that state is maintained even after the process terminates. In other words, when you use a database system, your data must always be stored on disk whether or not your system is running.</p>
<p>On the surface, this is at odds with the "serverless" world: it's important for a database to maintain state even after a process terminates. However, if an application is able to provide Kubernetes with additional knowledge about how to maintain the state beyond the lifecycle of a pod, not only can databases take advantage of Kubernetes' orchestration features, but suddenly teams have the ability to run their own database-as-a-service platforms.</p>
<h2 id="kubernetes-operators--postgresql">Kubernetes Operators &amp; PostgreSQL</h2>
<p>A <a href="https://coreos.com/operators/" target="_blank">Kubernetes Operator</a> is a type of application that lets developers provide additional context to Kubernetes on how to manage a stateful application. An Operator lets developers provide more application-specific knowledge to properly maintain the full lifecycle of a stateful application and ensure critical assets, such as data, remain safe and accessible.</p>
<p>With a database, like the popular open source <a href="https://www.postgresql.org/" target="_blank">PostgreSQL</a> database, an Operator can help with actions including:</p>
<ul><li><strong>Provisioning</strong>: allocating disk space where the data will be permanently stored</li>
<li><strong>Scaling</strong>: safely creating a replica (or copy) of the database that is consistently kept up-to-date</li>
<li><strong>High availability</strong>: ensuring applications can always read and write to the database even if a node becomes unavailable</li>
<li><strong>User management</strong>: enabling user access and remembering permissions to specific databases within an environment</li>
</ul><p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on Kubernetes</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/topics/containers/what-is-kubernetes?intcmp=7016000000127cYAAQ">What is Kubernetes?</a></li>
<li><a href="https://www.redhat.com/en/engage/kubernetes-containers-storage-s-201911201051?intcmp=7016000000127cYAAQ">eBook: Storage Patterns for Kubernetes</a></li>
<li><a href="https://www.redhat.com/en/engage/openshift-storage-testdrive-20170718?intcmp=7016000000127cYAAQ">Test drive OpenShift hands-on</a></li>
<li><a href="https://opensource.com/kubernetes-dump-truck?intcmp=7016000000127cYAAQ">eBook: Getting started with Kubernetes</a></li>
<li><a href="https://www.redhat.com/en/resources/managing-containers-kubernetes-openshift-technology-detail?intcmp=7016000000127cYAAQ">An introduction to enterprise Kubernetes</a></li>
<li><a href="https://enterprisersproject.com/article/2017/10/how-explain-kubernetes-plain-english?intcmp=7016000000127cYAAQ">How to explain Kubernetes in plain terms</a></li>
<li><a href="https://opensource.com/downloads/kubernetes-raspberry-pi?intcmp=7016000000127cYAAQ">eBook: Running Kubernetes on your Raspberry Pi homelab</a></li>
<li><a href="https://opensource.com/downloads/kubernetes-cheat-sheet?intcmp=7016000000127cYAAQ">Kubernetes cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/kubernetes-sysadmin?intcmp=7016000000127cYAAQ">eBook: A guide to Kubernetes for SREs and sysadmins</a></li>
<li><a href="https://opensource.com/tags/kubernetes?intcmp=7016000000127cYAAQ">Latest Kubernetes articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>Additional operating requirements include using a particular version of the database software, using a particular storage system or hardware profile, or deploying the databases to particular server nodes in a cluster.</p>
<p><a href="https://www.crunchydata.com/" target="_blank">Crunchy Data's</a> open source <a href="https://github.com/CrunchyData/postgres-operator" target="_blank">Crunchy PostgreSQL Operator</a> is an Operator for PostgreSQL that is used in many production environments. It provides a simple yet powerful <a href="https://access.crunchydata.com/documentation/postgres-operator/3.5.0/operator-cli/" target="_blank">command-line interface</a> that lets users deploy their own <a href="https://blog.openshift.com/running-your-own-database-as-a-service-with-the-crunchy-postgresql-operator/" target="_blank">database-as-a-service system</a> on any Kubernetes-enabled platform.</p>
<p>For example, with the <strong>pgo create</strong> command, which is used to provision a database, you can set up a distributed, high-availability PostgreSQL cluster with full disaster recovery support along with a database monitoring sidecar powered by <a href="https://github.com/CrunchyData/pgmonitor" target="_blank">pgMonitor</a>. In other words, in lieu of a complicated multi-step process or having to write your own scripts, you can create the type of PostgreSQL system required for production from a single command.</p>
<p>While this may seem to be overkill if you are managing a handful of database clusters, the value of an Operator scales significantly if you need to support hundreds or thousands of different clusters. Having a standardized set of commands with the ability to flexibly deploy clusters for different workloads both eases the administration burden and provides more options for how a team can develop and deploy workloads into production.</p>
<h2 id="open-source-as-a-service">Open source as a service</h2>
<p>While many large cloud providers provide various open source platforms "as a service," this has the effect of locking people into a particular cloud infrastructure. Using Kubernetes and an open source Operator like the Crunchy PostgreSQL Operator lets users choose which cloud environments they want to run their stateful services (have Kubelet, will travel!), all while managing them from a standardized interface.</p>
<hr /><p><em>Jonathan Katz will present <a href="https://www.socallinuxexpo.org/scale/17x/presentations/operating-postgresql-kubernetes-scale" target="_blank">Operating PostgreSQL with Kubernetes at Scale</a> at the 17th annual Southern California Linux Expo (<a href="https://www.socallinuxexpo.org/" target="_blank">SCaLE 17x</a>) March 7-10 in Pasadena, Calif.</em></p>
