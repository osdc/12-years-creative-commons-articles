<p>The opensouce.com community is growing fast, and we're trying to  figure out who we are and what we care about. The more we know about  ourselves, the more relevant our content and discussions will be.</p>
<p>These polls aren't scientific, but they will give us a useful  snapshot of of our growing community, so we can plan better for the  future.</p>
<p>Feel free to tell us more about you in the comments.</p>
