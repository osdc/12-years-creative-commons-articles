<p>Communication is complicated in any organization. But in open organizations, where communication crisscrosses hierarchical boundaries and functional silos all the time, it's even more complicated.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Learn about open organizations</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://theopenorganization.org">Download resources</a></li>
<li><a href="https://theopenorganization.community">Join the community</a></li>
<li><a href="https://opensource.com/open-organization/resources/open-org-definition?src=too_resource_menu3a">What is an open organization?</a></li>
<li><a href="https://opensource.com/open-organization/resources/open-org-maturity-model?src=too_resource_menu4a">How open is your organization?</a></li>
</ul></div>
</div>
</div>
</div>

<p>So learning how to communicate with stakeholders sitting in all areas of the organization is exceedingly important. The truth is that you'll need to adapt your approach as you make connections across teams and departments.</p>
<p>In this video, I offer some advice on adapting your communication styles when you're speaking to high-level executives, middle managers, and operational staff. Each of these groups wants something different from you—and knowing about those differing needs can help you succeed in connecting with them.</p>
<p>This the first installment of a new open organization video series I'm launching. If you have feedback on ways I can make future videos more useful, please let me know.</p>
<p><iframe allow="autoplay; encrypted-media" allowfullscreen="" src="https://www.youtube.com/embed/ZrrahvcQUMY?rel=0" width="560" height="315" frameborder="0"></iframe></p>
