<p>We are proud to announce that Red Hat is now accepting nominations for the <a href="http://www.redhat.com/en/about/women-in-open-source" target="_blank">Women in Open Source Award</a>. This award is the first of its kind, created to shine a spotlight on women making important contributions to an open source project, to the open source community, or through the use of open source methodology.</p>
<!--break-->
<h2>Who to nominate</h2>
<p>The Women in Open Source Award celebrates all different kinds of contributions to open source, including:</p>
<ul><li>Code and programming</li>
<li>Quality assurance, bug triage, and other quality-related contributions</li>
<li>Involvement in open hardware</li>
<li>System administration and infrastructure contributions</li>
<li>Design, artwork, user-experience (UX), and marketing</li>
<li>Documentation, tutorials, and other forms of communication</li>
<li>Translation and other internationalization contributions</li>
<li>Open content</li>
<li>Community advocacy and management</li>
<li>Intellectual property advocacy and legal reform</li>
<li>Open source methodology</li>
</ul><h3>Women can qualify for one of two tracks:</h3>
<ul><li>Academic award: open to women enrolled in college or university</li>
<li>Community award: open to all other women</li>
</ul><p>Both award tracks recognize the contributions of a woman in open source. With the Academic award, Red Hat wants to inspire students to get involved in open source and to promote <a href="https://opensource.com/life/14/2/open-source-can-empower-women" target="_blank" title="Open source can empower women">the benefits of careers in open source</a> and technology. The award is open to women and anyone who currently identifies as a woman. Self-nomination is encouraged. Red Hat employees are not eligible to win, although they can nominate others for the award.</p>
<h2>What winners receive</h2>
<p>The Women in Open Source Academic Award winner will receive:</p>
<ul><li>$2,500 stipend, with suggested use to support open source project or efforts</li>
<li>Feature article here on Opensource.com</li>
</ul><p>The Women in Open Source Community Award winner will receive:</p>
<ul><li>Ticket, flight, and hotel accommodations for the <a href="http://www.redhat.com/summit/" target="_blank">Red Hat Summit</a>, June 23-26, 2015, in Boston</li>
<li>$2,500 stipend, with suggested use to support open source project or efforts</li>
<li>Feature article here on Opensource.com</li>
<li>Speaking opportunity at future Red Hat Women's Leadership Community event</li>
</ul><p><a href="http://www.redhat.com/en/about/women-in-open-source" target="_blank">Nominations</a> are open through November 21. Qualified judges from Red Hat will narrow down the nominees to a subset of finalists for both the Academic and Community awards. The public will vote to determine the winner from the finalists selected. Winners will be announced in June during an awards ceremony at the<a href="http://www.redhat.com/summit/"> </a><a href="http://www.redhat.com/summit/" target="_blank">2015</a> Red Hat Summit in Boston, Massachusetts.</p>
<p>Think about women you know who make valuable contributions to open source. This is a great way to recognize someone for her hard work. <a href="http://www.redhat.com/en/about/women-in-open-source" target="_blank">Nominate someone today</a>.</p>
