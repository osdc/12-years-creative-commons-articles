<p>JupyterLab is the next-generation web-based <a href="https://jupyter.org/" target="_blank">Jupyter</a> user interface. It allows you to work with <a href="https://opensource.com/article/18/3/getting-started-jupyter-notebooks">Jupyter Notebooks</a>, as well as editors, terminals, and more, to produce interactive documents for data science, statistical modeling, data visualization, and more.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Python Resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/topics/middleware/what-is-ide?intcmp=7016000000127cYAAQ">What is an IDE?</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-python-37-beginners?intcmp=7016000000127cYAAQ">Cheat sheet: Python 3.7 for beginners</a></li>
<li><a href="https://opensource.com/resources/python/gui-frameworks?intcmp=7016000000127cYAAQ">Top Python GUI frameworks</a></li>
<li><a href="https://opensource.com/downloads/7-essential-pypi-libraries?intcmp=7016000000127cYAAQ">Download: 7 essential PyPI libraries</a></li>
<li><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ">Red Hat Developers</a></li>
<li><a href="https://opensource.com/tags/python?intcmp=7016000000127cYAAQ">Latest Python content</a></li>
</ul></div>
</div>
</div>
</div>
<p>It has native viewers for PDF, CSV, JSON, images, and more. It is also extensible to support other formats.</p>
<p>JupyterLab's left sidebar has tabs for using it as a file manager, a Jupyter kernel manager, or a Jupyter Notebook metadata editor.</p>
<p>Writing code in Jupyter Notebooks enables an interactive development experience. You can write code, see the results, and modify the code—all without restarting your process or losing your in-memory data. This is a great fit for exploratory programming when you are not sure what your end result will look like.</p>
<p>Exploration is common in data science; after all, science is the process of finding out answers not known before. But exploration is not limited to data science. Jupyter works well for system diagnostics and automation where you don't know the answer or solution in advance. Whenever feedback is useful for the next step, whether it is image manipulation, analyzing your exercise data, or writing games, Jupyter's bias toward exploration can be helpful.</p>
<p>Jupyter and JupyterLab are great tools, so this JupyterLab cheat sheet will make it easier for you to get started.</p>
<h2 class="rtecenter" id="download-our-jupyterlab-cheat-sheet-today"><a href="https://opensource.com/downloads/jupyterlab-cheat-sheet">Download the JupyterLab cheat sheet</a> </h2>
