<p>Like many people, I've been exploring lots of new TV shows during the pandemic. I recently discovered a British game show called <em><a href="https://en.wikipedia.org/wiki/Countdown_%28game_show%29" target="_blank">Countdown</a></em>, where contestants play two types of games: a <em>words</em> game, where they try to make the longest word out of a jumble of letters, and a <em>numbers</em> game, where they calculate a target number from a random selection of numbers. Because I enjoy mathematics, I've found myself drawn to the numbers game.</p>
<p>The numbers game can be a fun addition to your next family game night, so I wanted to share my own variation of it. You start with a collection of random numbers, divided into "small" numbers from 1 to 10 and "large" numbers that are 15, 20, 25, and so on until 100. You pick any combination of six numbers from both large and small numbers.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Linux resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux commands cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Free online course: RHEL Technical Overview</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux networking cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">SELinux cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux common commands cheat sheet</a></li>
<li><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">What are Linux containers?</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Our latest Linux articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>Next, you generate a random "target" number between 200 and 999. Then use simple arithmetic operations with your six numbers to try to calculate the target number using each "small" and "large" number no more than once. You get the highest number of points if you calculate the target number exactly and fewer points if you can get within 10 of the target number.</p>
<p>For example, if your random numbers were 75, 100, 2, 3, 4, and 1, and your target number was 505, you might say <em>2+3=5</em>, <em>5×100=500</em>, <em>4+1=5</em>, and <em>5+500=505</em>. Or more directly: (<strong>2</strong>+<strong>3</strong>)×<strong>100</strong> + <strong>4</strong> + <strong>1</strong> = <strong>505</strong>.</p>
<h2 id="randomize-lists-on-the-command-line">Randomize lists on the command line</h2>
<p>I've found the best way to play this game at home is to pull four "small" numbers from a pool of 1 to 10 and two "large" numbers from multiples of five from 15 to 100. You can use the Linux command line to create these random numbers for you.</p>
<p>Let's start with the "small" numbers. I want these to be in the range of 1 to 10. You can generate a sequence of numbers using the Linux <code>seq</code> command. You can run <code>seq</code> a few different ways, but the simplest form is to provide the starting and ending numbers for the sequence. To generate a list from 1 to 10, you might run this command:</p>
<pre><code class="language-bash">$ seq 1 10
1
2
3
4
5
6
7
8
9
10</code></pre><p>To randomize this list, you can use the Linux <code><strong>shuf</strong></code> ("shuffle") command. <code><strong>shuf</strong></code> will randomize the order of whatever you give it, usually a file. For example, if you send the output of the <code><strong>seq</strong></code> command to the <code><strong>shuf</strong></code> command, you will receive a randomized list of numbers between 1 and 10:</p>
<pre><code class="language-bash">$ seq 1 10 | shuf
3
6
8
10
7
4
5
2
1
9</code></pre><p>To select just four random numbers from a list of 1 to 10, you can send the output to the <code>head</code> command, which prints out the first few lines of its input. Use the <code>-4</code> option to specify that <code>head</code> should print only the first four lines:</p>
<pre><code class="language-bash">$ seq 1 10 | shuf | head -4
6
1
8
4</code></pre><p>Note that this list is different from the earlier example because <code>shuf</code> will generate a random order every time.</p>
<p>Now you can take the next step to generate the random list of "large" numbers. The first step is to generate a list of possible numbers starting at 15, incrementing by five, until you reach 100. You can generate this list with the Linux <code>seq</code> command. To increment each number by five, insert another option for the <code>seq</code> command to indicate the <em>step</em>:</p>
<pre><code class="language-bash">$ seq 15 5 100
15
20
25
30
35
40
45
50
55
60
65
70
75
80
85
90
95
100</code></pre><p>And just as before, you can randomize this list and select two of the "large" numbers:</p>
<pre><code class="language-bash">$ seq 15 5 100 | shuf | head -2
75
40</code></pre><h2 id="generate-a-random-number-with-bash">Generate a random number with Bash</h2>
<p>I suppose you could use a similar method to select the game's target number from the range 200 to 999. But the simplest solution to generate a single random value is to use the <code>RANDOM</code> variable directly in Bash. When you reference this built-in variable, Bash generates a large random number. To put this in the range of 200 to 999, you need to put the random number into the range 0 to 799 first, then add 200.</p>
<p>To put a random number into a specific range starting at 0, you can use the <strong>modulo</strong> arithmetic operation. Modulo calculates the <em>remainder</em> after dividing two numbers. If I started with 801 and divided by 800, the result is 1 <em>with a remainder of</em> 1 (the modulo is 1). Dividing 800 by 800 gives 1 <em>with a remainder of</em> 0 (the modulo is 0). And dividing 799 by 800 results in 0 <em>with a remainder of</em> 799 (the modulo is 799).</p>
<p>Bash supports arithmetic expansion with the <code>$(( ))</code> construct. Between the double parentheses, Bash will perform arithmetic operations on the values you provide. To calculate the modulo of 801 divided by 800, then add 200, you would type:</p>
<pre><code class="language-bash">$ echo $(( 801 % 800 + 200 ))
201</code></pre><p>With that operation, you can calculate a random target number between 200 and 999:</p>
<pre><code class="language-bash">$ echo $(( RANDOM % 800 + 200 ))
673</code></pre><p>You might wonder why I used <code>RANDOM</code> instead of <code>$RANDOM</code> in my Bash statement. In arithmetic expansion, Bash will automatically expand any variables within the double parentheses. You don't need the <code>$</code> on the <code>$RANDOM</code> variable to reference the value of the variable because Bash will do it for you.</p>
<h2 id="playing-the-numbers-game">Playing the numbers game</h2>
<p>Let's put all that together to play the numbers game. Generate two random "large" numbers, four random "small" values, and the target value:</p>
<pre><code class="language-bash">$ seq 15 5 100 | shuf | head -2
75
100
$ seq 1 10 | shuf | head -4
4
3
10
2
$ echo $(( RANDOM % 800 + 200 ))
868</code></pre><p>My numbers are <strong>75</strong>, <strong>100</strong>, <strong>4</strong>, <strong>3</strong>, <strong>10</strong>, and <strong>2</strong>, and my target number is <strong>868</strong>.</p>
<p>I can get close to the target number if I do these arithmetic operations using each of the "small" and "large" numbers no more than once:</p>
<pre><code class="language-text">10×75 = 750
750+100 = 850

and:

4×3 = 12
850+12 = 862
862+2 = 864</code></pre><p>That's only four away—not bad! But I found this way to calculate the exact number using each random number no more than once:</p>
<pre><code class="language-text">4×2 = 8
8×100 = 800

and:

75-10+3 = 68
800+68 = 868</code></pre><p>Or I could perform <em>these</em> calculations to get the target number exactly. This uses only five of the six random numbers:</p>
<pre><code class="language-text">4×3 = 12
75+12 = 87

and:

87×10 = 870
870-2 = 868</code></pre><p>Give the <em>Countdown</em> numbers game a try, and let us know how well you did in the comments.</p>
