<p>Nadia Eghbal's 2016 <a href="https://www.fordfoundation.org/work/learning/research-reports/roads-and-bridges-the-unseen-labor-behind-our-digital-infrastructure/" target="_blank">Ford Foundation report</a> on open source software infrastructure shed light on the problem of public code being built and maintained in large part by unpaid volunteers. Since then, the industry has been eager to understand how and why maintainers do this and what we can collectively do to support them better.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Great Content</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=7016000000127cYAAQ">Free online course: RHEL technical overview</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=7016000000127cYAAQ">Learn Advanced Linux Commands</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Download Cheat Sheets</a></li>
<li><a href="https://opensource.com/alternatives?intcmp=7016000000127cYAAQ">Find an Open Source Alternative</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=7016000000127cYAAQ">Read Top Linux Content</a></li>
<li><a href="https://opensource.com/resources?intcmp=7016000000127cYAAQ">Check out open source resources</a></li>
</ul></div>
</div>
</div>
</div>
<p>I work for a company, Tidelift, that supports the work of open source maintainers. We care deeply about understanding the reality of maintainer life—good, bad, and otherwise—to advance the global conversation and provide better-targeted support for maintainers (in addition to paying them).</p>
<p>As part of this effort, we launched our <a href="https://tidelift.az1.qualtrics.com/jfe/form/SV_9SRzXPe4ulOEpRc" target="_blank">first, all-encompassing survey</a> for people who actively maintain one or more open source projects. It includes questions such as:</p>
<ul><li>Do you get paid for open source maintenance work, and if so, by whom?</li>
<li>How has the COVID-19 pandemic impacted your project maintenance work?</li>
<li>What do you enjoy most about being a maintainer, and what do you dislike most?</li>
<li>Have you ever considered quitting, and if so, why?</li>
</ul><p>Plus many others! If you are as curious as we are about the answers and want to know what other maintainers are thinking:</p>
<ul><li>Maintainers, <a href="https://tidelift.az1.qualtrics.com/jfe/form/SV_9SRzXPe4ulOEpRc" target="_blank">please take the survey and share your thoughts</a>.</li>
<li>If you know open source maintainers, please share the survey with them. The more data we collect, the better picture we'll have into the lives of open source maintainers in 2021.</li>
</ul><blockquote class="twitter-tweet"><p dir="ltr" lang="en" xml:lang="en">We want to learn more about the reality of maintainers–good, bad, and otherwise.<br /><br /><br /><br />
First survey of its kind aimed at the whole of open source, it'll help us provide better targeted supports for maintainers (on top of, y'know, paying them).<br /><br /><br /><br />
RT for reach? ?️<a href="https://t.co/t4JX5gfHK3">https://t.co/t4JX5gfHK3</a> <a href="https://t.co/ZLgcWvvmEz">pic.twitter.com/ZLgcWvvmEz</a></p>
<p>— Josh Simmons (@joshsimmons) <a href="https://twitter.com/joshsimmons/status/1364665328899289088?ref_src=twsrc%5Etfw">February 24, 2021</a></p></blockquote>
<script async="" src="https://platform.twitter.com/widgets.js" charset="utf-8"></script><p>It's a more in-depth survey than some, so we are compensating maintainers for their time with one of these two "way-better-than-average" thank you gifts:</p>
<ul><li>If you are a maintainer working as a "Lifter" for Tidelift or you apply to "lift" a project and are accepted as a new Lifter, we'll send you a pair of custom one-of-a-kind Tidelift Chuck Taylor Shoes (whoa!). Learn more about <a href="https://tidelift.com/about/lifter" target="_blank">how lifting a project works here</a>.</li>
<li>If you are an open source maintainer not working with Tidelift, fill out the survey and get a pair of snuggly and snazzy argyle socks.</li>
</ul><p><a href="https://tidelift.az1.qualtrics.com/jfe/form/SV_9SRzXPe4ulOEpRc" target="_blank">Maintainers, please complete the survey by March 12, 2021</a>—and thank you for what you do!</p>
