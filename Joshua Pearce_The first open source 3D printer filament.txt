<p>Until very recently, 3D printing companies sold only proprietary hardware and materials (including plastic filament) to print with them. This plastic was sold at exorbitant prices (up to hundreds of U.S. dollars per kg), even if it was a common material like ABS (the plastic used for Lego blocks). These companies were following the path of traditional desktop printing companies that rake in large profits selling toner and ink. Some companies still attempt to extort their customers by threatening warranty loss if they use filament from other manufacturers.</p>
<p>Fortunately, along with the radical price declines and more rapid innovation that came with the release of the open <a href="http://reprap.org/" target="_blank">RepRap 3D printer</a>, there was an explosion of filament manufacturers that provided a wide array of new filaments for these machines.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Explore open hardware </p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/resources/what-open-hardware?src=open_hardware_resources_menu1">What is open hardware?</a></li>
<li><a href="https://opensource.com/resources/what-raspberry-pi?src=open_hardware_resources_menu2">What is Raspberry Pi?</a></li>
<li><a href="https://opensource.com/resources/what-arduino?src=open_hardware_resources_menu3">What is an Arduino?</a></li>
<li><a href="https://opensource.com/tags/hardware?src=open_hardware_resources_menu4">Our latest open hardware articles</a></li>
</ul></div>
</div>
</div>
</div>

<p>Open source RepRap 3D printers and their commercial derivatives now dominate the desktop 3D printing market. This was a great start, but speaking as a material scientist, the fact that all of the filaments were proprietary was a major problem. Most people do not know what they are printing with, which is a challenge as we begin to rely more and more on <a href="https://www.academia.edu/31327768/Emergence_of_Home_Manufacturing_in_the_Developed_World_Return_on_Investment_for_Open-Source_3-D_Printers" target="_blank">distributed manufacturing of our own products</a>. Even if you make the filament yourself with a <a href="http://www.appropedia.org/Recyclebot" target="_blank">recyclebot</a> from <a href="http://www.academia.edu/2643418/Distributed_Recycling_of_Waste_Polymer_into_RepRap_Feedstock" target="_blank">waste plastic</a>, you only know the basic polymer, not anything about additives, coloring agents, etc.</p>
<p class="rtecenter">
<article class="align-center media media--type-image media--view-mode-full" title="Recyclebot"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/recyclebot-2.jpg" width="700" height="540" alt="Recyclebot" title="Recyclebot" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p><sup>Michigan Tech student preparing a vertical recyclebot for filament manufacturing from waste plastic. <a href="http://www.appropedia.org/File:Recyclebot-2.jpg" target="_blank" rel="ugc">Shan Zhong</a>, CC-by-sa</sup></p>
</div>
      
  </article></p>
<p>A detailed list and OpenSCAD source code for <a href="https://www.academia.edu/11229348/Polymer_recycling_codes_for_distributed_manufacturing_with_3-D_printers" target="_blank">polymer recycling codes</a> have been developed for 3D printer users to integrate directly into their own designs for eventual recycling. For the detailed recycling code system to work perfectly, you still need to know what you are printing with.</p>
<p class="rtecenter">
<article class="align-center media media--type-image media--view-mode-full" title="advanced recycling code for 3D printer manufacturers"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/pla-symb.jpg" width="393" height="400" alt="advanced recycling code for 3D printer manufacturers" title="advanced recycling code for 3D printer manufacturers" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>An example of an advanced recycling code for 3D printer manufacturers that enables symbols to be embedded into the plastic products. The new system allows for far more granularity than the measly seven resin ID codes currently used by the U.S. recycling industry. <a href="http://www.appropedia.org/User:J.M.Pearce" target="_blank" rel="ugc">J.M.Pearce</a>, GNU Free Documentation License</sup></p>
</div>
      
  </article></p>
<p>Fortunately, IC3D has set a precedent with <a href="https://consumables.ic3dprinters.com/shop/filament/ic3d-abs-3d-printer-filament/" target="_blank">IC3D ABS</a>, the first Open Source Hardware Association (<a href="http://certificate.oshwa.org/certification-directory/" target="_blank">OSHWA) certified</a> filament for 3D printing. Just as open source produced rapid innovation in 3D printers, IC3D's filament promises to advance material consistency, precision, diversity, and quality across the industry. To see exactly what goes into its filament and how it's processed, you can follow the complete source code on <a href="https://github.com/IC3DPrinters/filament-extrusion/blob/master/Open%20Source%20Filament/IC3D%20Open%20Source%20ABS%203D%20Printing%20Filament%20Documentation%20Rev0.pdf" target="_blank">GitHub</a>.</p>
<p>While it is unlikely individuals will recreate IC3D's industrial line, those developing the next variants of smaller open source recyclebots can learn a lot from IC3D. Innovations from the community will feed back to IC3D as it moves to more advanced materials, which will benefit the entire 3D printing community.</p>
<p class="rtecenter">
<article class="align-center media media--type-image media--view-mode-full" title="Different colors of IC3D filament"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/filament_66_crop.jpg" width="700" height="322" alt="Different colors of IC3D filament" title="Different colors of IC3D filament" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>IC3D's OSHWA certified open source filament, <a href="https://consumables.ic3dprinters.com/" target="_blank" rel="ugc">IC3D</a>.</sup></p>
</div>
      
  </article></p>
<p>What makes IC3D's release particularly interesting is that it is now possible to use a completely libre 3D printing toolchain to <a href="https://opensource.com/article/17/3/how-to-create-consumer-goods-open-hardware" target="_blank">cut the costs of commercial products</a>. You can use free and open source software like <a href="http://www.openscad.org/" target="_blank">OpenSCAD</a> or <a href="https://freecadweb.org/" target="_blank">FreeCAD</a> to make your design and slice it with an open slicer (e.g., <a href="https://ultimaker.com/en/products/cura-software" target="_blank">Cura</a> or <a href="http://slic3r.org/" target="_blank">Slic3r</a>) running on your open source laptop with your <a href="https://distrowatch.com/search.php?status=All" target="_blank">favorite version of Linux</a>. Then you can use an open source 3D printer controller like <a href="https://github.com/mtu-most/franklin" target="_blank">Franklin</a> or <a href="http://www.pronterface.com/" target="_blank">Printrun</a> to print out your masterpiece on your open source 3D printer (e.g., a <a href="https://www.lulzbot.com/" target="_blank">Lulzbot</a> or <a href="http://www.prusa3d.com/" target="_blank">Prusa</a>) using IC3D's filament.</p>
<p>IC3D created this open source filament because it fits its mission "to enable curious tinkerers and inspire young inventors with the high-quality 3D printing gear they will enjoy, while exploring and expressing their inner creativity to the world."</p>
<p>For those who appreciate quality filament as well as the value of open source, by choosing IC3D ABS filament you can help support innovation and freedom. I look forward to seeing IC3D make the rest of its filament line open source and begin to work on advanced composite and high-performance filaments. Hopefully, we will also see other filament manufacturers follow IC3D's lead, also, as its value becomes clear.</p>
