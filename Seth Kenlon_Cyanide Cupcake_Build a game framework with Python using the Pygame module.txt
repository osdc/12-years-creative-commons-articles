<p>In my <a href="https://opensource.com/article/17/10/python-101" target="_blank">first article in this series</a>, I explained how to use Python to create a simple, text-based dice game. You also used the Turtle module to generate some simple graphics. In this article, you start using a module called Pygame to create a game with graphics.</p>
<p>The Turtle module is included with Python by default. Anyone who's got Python installed also has Turtle. The same is not true for an advanced module like Pygame. Because it's a specialized code library, you must install Pygame yourself. Modern Python development uses the concept of virtual <em>environments</em>, which provides your Python code its own space to run in, and also helps manage which code libraries your application uses. This ensures that when you pass your Python application to another user to play, you know exactly what they need to install to make it work.</p>
<p>You can manage your Python virtual environment manually, or you can let your IDE help you. For now, you can let PyCharm do all the work. If you're not using PyCharm, <a href="https://opensource.com/article/19/4/managing-python-packages" target="_blank">read László Kiss Kollár's article about managing Python packages</a>.</p>
<h2>Getting started with Pygame</h2>
<p>Pygame is a library, or <em>Python module</em>. It's a collection of common code that prevents you from having to reinvent the wheel with every new game you write. You've already used the Turtle module, and you can imagine how complex that could have been if you'd had to write the code to create a pen before drawing with it. Pygame offers similar advantages, but for video games.</p>
<p>A video game needs a setting, a world in which it takes place. In Pygame, there are two different ways to create your setting:</p>
<ul><li>Set a background color</li>
<li>Set a background image</li>
</ul><p>Either way, your background is only an image or a color. Your video game characters can't interact with things in the background, so don't put anything too important back there. It's just set dressing.</p>
<h2>Setting up your first Pygame script</h2>
<p>To start a new Python project, you would normally create a new folder on your computer and place all your game files go into this directory. It's vitally important that you keep all the files needed to run your game inside of your project folder.</p>
<p>PyCharm (or whatever IDE you use) can do this for you.</p>
<p>To create a new project in PyCharm, navigate to the <strong>File</strong> menu and select <strong>New Project</strong>. In the window that appears, enter a name for your project (such as <strong>game_001</strong>.) Notice that this project is saved into a special <strong>PycharmProjects</strong> folder in your home directory. This ensures that all the files your game needs stays in one place.</p>
<p>When creating a new project, let PyCharm create a new environment using Virtualenv, and accept all defaults. Enable the option to create a <strong>main.py</strong> file (and to set up some important defaults.)</p>
<p><article class="media media--type-image media--view-mode-full" title="New project settings in PyCharm"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/pycharm-new-project_0.jpg" width="675" height="360" alt="New project settings in PyCharm" title="New project settings in PyCharm" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>After you've clicked the <strong>Create</strong> button, a new project appears in your PyCharm window. The project consists of a virtual environment (the <strong>venv</strong> directory listed in the left column) and a demo file called <strong>main.py</strong>.</p>
<p>Delete all the contents of <strong>main.py</strong> so you can enter your own custom code. A Python script starts with the file type, your name, and the license you want to use. Use an open source license so your friends can improve your game and share their changes with you:</p>
<pre><code class="language-python">#!/usr/bin/env python3
# by Seth Kenlon

## GPLv3
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see &lt;http://www.gnu.org/licenses/&gt;.</code></pre><p>Then tell Python what modules you want to use. In this code sample, you don't have to type the # character or anything after it on each line. The # character in Python represents a <em>comment</em>, notes in code left for yourself and other coders.</p>
<pre><code class="language-python">import pygame  # load pygame keywords
import sys     # let  python use your file system
import os      # help python identify your OS</code></pre><p>Notice that PyCharm doesn't understand what Pygame is, and underlines it to mark it as an error. This is because Pygame, unlike sys and os, isn't included with Python by default. You need to include Pygame in your project directory before you can use it successfully in code. To include it, hover your mouse over the word <strong>pygame</strong> until you see a notification popup explaining the error.</p>
<p>Click the <strong>Install package pygame</strong> link in the alert box, and wait for PyCharm to install Pygame into your virtual environment.</p>
<p>Once it's installed, the error disappears.</p>
<p>Without an IDE like PyCharm, you can install Pygame into your virtual environment manually using the <strong>pip </strong>command.</p>
<h2>Code sections</h2>
<p>Because you'll be working a lot with this script file, it helps to make sections within the file so you know where to put stuff. You do this with block comments, which are comments that are visible only when looking at your source code. Create four blocks in your code. These are all comments that Python ignores, but they're good placeholders for you as you follow along with these lessons. I still use placeholders when I code, because it helps me organize and plan.</p>
<pre><code class="language-python">'''
Variables
'''

# put variables here


'''
Objects
'''

# put Python classes and functions here


'''
Setup
'''

# put run-once code here


'''
Main Loop
'''

# put game loop here</code></pre><p>Next, set the window size for your game. Keep in mind that not everyone has a big computer screen, so it's best to use a screen size that fits on "most" people's computers.</p>
<p>There is a way to toggle full-screen mode, the way many modern video games do, but since you're just starting out, keep it simple and just set one size.</p>
<pre><code class="language-python">'''
Variables
'''
worldx = 960
worldy = 720</code></pre><p>The Pygame engine requires some basic setup before you can use it in a script. You must set the frame rate, start its internal clock, and start (using the keyword <code>init</code>, for <em>initialize</em>) Pygame.</p>
<p>Add these variables:</p>
<pre><code class="language-python">fps   = 40  # frame rate
ani   = 4   # animation cycles</code></pre><p>Add instructions to start Pygame's internal clock in the Setup section:</p>
<pre><code class="language-python">'''
Setup
'''

clock = pygame.time.Clock()
pygame.init()</code></pre><p>Now you can set your background.</p>
<h2>Setting the background</h2>
<p>Before you continue, open a graphics application and create a background for your game world. Save it as <code>stage.png</code> inside a folder called <code>images</code> in your project directory. If you need a starting point, you can download a set of <a href="https://opensource.com/article/20/1/what-creative-commons" target="_blank">Creative Commons</a> backgrounds from <a href="https://kenney.nl/assets/background-elements-redux" target="_blank">kenny.nl</a>.</p>
<p>There are several free graphic applications you can use to create, resize, or modify graphics for your games.</p>
<ul><li><a href="https://pinta-project.com/pintaproject/pinta/releases" target="_blank">Pinta</a> is a basic, easy to learn paint application.</li>
<li><a href="http://krita.org" target="_blank">Krita</a> is a professional-level paint materials emulator that can be used to create beautiful images. If you're very interested in creating art for video games, you can even purchase a series of online <a href="https://gumroad.com/l/krita-game-art-tutorial-1" target="_blank">game art tutorials</a>.</li>
<li><a href="http://inkscape.org" target="_blank">Inkscape</a> is a vector graphics application. Use it to draw with shapes, lines, splines, and Bézier curves.</li>
</ul><p>Your graphic doesn't have to be complex, and you can always go back and change it later. Once you have a background, add this code in the setup section of your file:</p>
<pre><code class="language-python">world = pygame.display.set_mode([worldx,worldy])
backdrop = pygame.image.load(os.path.join('images','stage.png'))
backdropbox = world.get_rect()</code></pre><p>If you're just going to fill the background of your game world with a color, all you need is:</p>
<pre><code class="language-python">world = pygame.display.set_mode([worldx, worldy])</code></pre><p>You also must define a color to use. In your setup section, create some color definitions using values for red, green, and blue (RGB).</p>
<pre><code class="language-python">'''
Variables
'''

BLUE  = (25, 25, 200)
BLACK = (23, 23, 23)
WHITE = (254, 254, 254)</code></pre><h2>Look out for errors</h2>
<p>PyCharm is strict, and that's pretty typical for programming. Syntax is paramount! As you enter your code into PyCharm, you see warnings and errors. The warnings are yellow and the errors are red.</p>
<p>PyCharm can be over-zealous sometimes, though, so it's usually safe to ignore warnings. You may be violating the "Python style", but these are subtle conventions that you'll learn in time. Your code will still run as expected.</p>
<p>Errors, on the other hand, prevent your code (and sometimes PyCharm) from doing what you expect. For instance, PyCharm is very insistent that there's a newline character at the end of the last line of code, so you <strong>must</strong> press <strong>Enter</strong> or <strong>Return</strong> on your keyboard at the end of your code. If you don't, PyCharm quietly refuses to run your code.</p>
<h2>Running the game</h2>
<p>At this point, you could theoretically start your game. The problem is, it would only last for a millisecond.</p>
<p>To prove this, save and then run your game.</p>
<p>If you are using IDLE, run your game by selecting <code>Run Module</code> from the Run menu.</p>
<p>If you are using PyCharm, click the <code>Run file</code> button in the top right toolbar.</p>
<p><article class="media media--type-image media--view-mode-full" title="Run button"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/pycharm-button-run_0.jpeg" width="304" height="219" alt="Run button" title="Run button" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>You can also run a Python script straight from a Unix terminal or a Windows command prompt, as long as you're in your virtual environment.</p>
<p>However you launch it, don't expect much, because your game only lasts a few milliseconds right now. You can fix that in the next section.</p>
<h2>Looping</h2>
<p>Unless told otherwise, a Python script runs once and only once. Computers are very fast these days, so your Python script runs in less than a second.</p>
<p>To force your game to stay open and active long enough for someone to see it (let alone play it), use a <code>while</code> loop. To make your game remain open, you can set a variable to some value, then tell a <code>while</code> loop to keep looping for as long as the variable remains unchanged.</p>
<p>This is often called a "main loop," and you can use the term <code>main</code> as your variable. Add this anywhere in your Variables section:</p>
<pre><code class="language-python">main = True</code></pre><p>During the main loop, use Pygame keywords to detect if keys on the keyboard have been pressed or released. Add this to your main loop section:</p>
<pre><code class="language-python">'''
Main loop
'''

while main:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            try:
                sys.exit()
            finally:
                main = False

        if event.type == pygame.KEYDOWN:
            if event.key == ord('q'):
                pygame.quit()
            try:
                sys.exit()
            finally:
                main = False
</code></pre><p>Be sure to press <strong>Enter</strong> or <strong>Return</strong> after the final line of your code so there's an empty line at the bottom of your file.</p>
<p>Also in your main loop, refresh your world's background.</p>
<p>If you are using an image for the background:</p>
<pre><code class="language-python">world.blit(backdrop, backdropbox)</code></pre><p>If you are using a color for the background:</p>
<pre><code class="language-python">world.fill(BLUE)</code></pre><p>Finally, tell Pygame to refresh everything on the screen and advance the game's internal clock.</p>
<pre><code class="language-python">    pygame.display.flip()
    clock.tick(fps)</code></pre><p>Save your file, and run it again to see the most boring game ever created.</p>
<p>To quit the game, press <code>q</code> on your keyboard.</p>
<h2>Freeze your Python environment</h2>
<p>PyCharm is managing your code libraries, but your users aren't going to run your game from PyCharm. Just as you save your code file, you also need to preserve your virtual environment.</p>
<p>Go to the <strong>Tools</strong> menu and select <strong>Sync Python Requirements</strong>. This saves your library dependencies to a special file called <strong>requirements.txt</strong>. The first time you sync your requirements, PyCharm prompts you to install a plugin and to add dependencies. Click to accept these offers.</p>
<p><article class="media media--type-image media--view-mode-full" title="Requirements"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/pycharm-requirements.jpg" width="485" height="214" alt="Requirements" title="Requirements" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>A <strong>requirements.txt</strong> is generated for you, and placed into your project directory.</p>
<h2>Code</h2>
<p>Here's what your code should look like so far:</p>
<pre><code class="language-python">#!/usr/bin/env python3
# by Seth Kenlon

# GPLv3
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see &lt;http://www.gnu.org/licenses/&gt;.

import pygame
import sys
import os

'''
Variables
'''

worldx = 960
worldy = 720
fps = 40  # frame rate
ani = 4   # animation cycles
main = True

BLUE = (25, 25, 200)
BLACK = (23, 23, 23)
WHITE = (254, 254, 254)


'''
Objects
'''

# put Python classes and functions here


'''
Setup
'''

clock = pygame.time.Clock()
pygame.init()
world = pygame.display.set_mode([worldx, worldy])
backdrop = pygame.image.load(os.path.join('images', 'stage.png'))
backdropbox = world.get_rect()


'''
Main Loop
'''

while main:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            try:
                sys.exit()
            finally:
                main = False

        if event.type == pygame.KEYDOWN:
            if event.key == ord('q'):
                pygame.quit()
            try:
                sys.exit()
            finally:
                main = False
    world.blit(backdrop, backdropbox)
    pygame.display.flip()
    clock.tick(fps)</code></pre><h2>What to do next</h2>
<p>In the <a href="https://opensource.com/article/17/12/program-game-python-part-3-spawning-player">next article</a> of this series, I'll show you how to add to your currently empty game world, so start creating or finding some graphics to use!</p>
