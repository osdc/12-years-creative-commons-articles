<p>Some data is ephemeral, stored in RAM, and only significant while an application is running. But some data is meant to be persistent, stored on a hard drive for later use. When you program, whether you're working on a simple script or a complex suite of tools, it's common to need to read and write files. Sometimes a file may contain configuration options, and other times the file is the data that your user is creating with your application. Every language handles this task a little differently, and this article demonstrates how to handle data files with Lua.</p>
<h2 id="installing-lua">Installing Lua</h2>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Programming and development</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ&amp;src=programming_resource_menu4">Red Hat Developers Blog</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Programming cheat sheets</a></li>
<li><a href="https://www.redhat.com/en/services/training/learning-subscription?intcmp=7016000000127cYAAQ">Try for free: Red Hat Learning Subscription</a></li>
<li><a href="https://opensource.com/downloads/bash-programming-guide?intcmp=7016000000127cYAAQ">eBook: An introduction to programming with Bash</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/bash-shell-cheat-sheet?intcmp=7016000000127cYAAQ">Bash Shell Scripting Cheat Sheet</a></li>
<li><a href="https://developers.redhat.com/e-books/modernizing-enterprise-java?intcmp=7016000000127cYAAQ">eBook: Modernizing Enterprise Java</a></li>
</ul></div>
</div>
</div>
</div>
<p>If you're on Linux, you can install Lua from your distribution's software repository. On macOS, you can install Lua from <a href="https://opensource.com/article/20/11/macports">MacPorts</a> or <a href="https://opensource.com/article/20/6/homebrew-mac">Homebrew</a>. On Windows, you can install Lua from <a href="https://opensource.com/article/20/3/chocolatey">Chocolatey</a>.</p>
<p>Once you have Lua installed, open your favorite text editor and get ready to code.</p>
<h2 id="readinf-a-file-with-lua">Reading a file with Lua</h2>
<p>Lua uses the <code>io</code> library for data input and output. The following example creates a function called <code>ingest</code> to read data from a file and then parses it with the <code>:read</code> function. When opening a file in Lua, there are several modes you can enable. Because I just need to read data from this file, I use the <code>r</code> (for "read") mode:</p>
<pre><code class="language-c">function ingest(file)
   local f = io.open(file, "r")
   local lines = f:read("*all")
   f:close()
   return(lines)
end

myfile=ingest("example.txt")
print(myfile)</code></pre><p>In the code, notice that the variable <code>myfile</code> is created to trigger the <code>ingest</code> function, and therefore, it receives whatever that function returns. The <code>ingest</code> function returns the lines (from a variable intuitively called <code>lines</code>) of the file. When the contents of the <code>myfile</code> variable are printed in the final step, the lines of the file appear in the terminal.</p>
<p>If the file <code>example.txt</code> contains configuration options, then I would write some additional code to parse that data, probably using another Lua library depending on whether the configuration was stored as an INI file or YAML file or some other format. If the data were an SVG graphic, I'd write extra code to parse XML, probably using an SVG library for Lua. In other words, the data your code reads can be manipulated once it's loaded into memory, but all that's required to load it is the <code>io</code> library.</p>
<h2 id="writing-data-to-a-file-with-lua">Writing data to a file with Lua</h2>
<p>Whether you're storing data your user is creating with your application or just metadata about what the user is doing in an application (for instance, game saves or recent songs played), there are many good reasons to store data for later use. In Lua, this is achieved through the <code>io</code> library by opening a file, writing data into it, and closing the file:</p>
<pre><code class="language-c">function exgest(file)
   local f = io.open(file, "a")
   io.output(f)
   io.write("hello world\n")
   io.close(f)
end

exgest("example.txt")</code></pre><p>To read data from the file, I open the file in <code>r</code> mode, but this time I use <code>a</code> (for "append") to write data to the end of the file. Because I'm writing plain text into a file, I added my own newline character (<code>\n</code>). Often, you're not writing raw text into a file, and you'll probably use an additional library to write a specific format instead. For instance, you might use an INI or YAML library to help write configuration files, an XML library to write XML, and so on.</p>
<h2 id="file-modes">File modes</h2>
<p>When opening files in Lua, there are some safeguards and parameters to define how a file should be handled. The default is <code>r</code>, which permits you to read data only:</p>
<ul><li><strong>r</strong> for read only</li>
<li><strong>w</strong> to overwrite or create a new file if it doesn't already exist</li>
<li><strong>r+ </strong>to read and overwrite</li>
<li><strong>a</strong> to append data to a file or make a new file if it doesn't already exist</li>
<li><strong>a+</strong> to read data, append data to a file, or make a new file if it doesn't already exist</li>
</ul><p>There are a few others (<code>b</code> for binary formats, for instance), but those are the most common. For the full documentation, refer to the excellent Lua documentation on <a href="http://lua.org/manual" target="_blank">Lua.org/manual</a>.</p>
<h2>Lua and files</h2>
<p>Like other programming languages, Lua has plenty of library support to access a filesystem to read and write data. Because Lua has a consistent and simple syntax, it's easy to perform complex processing on data in files of any format. Try using Lua for your next software project, or as an API for your C or C++ project.</p>
<p> </p>
