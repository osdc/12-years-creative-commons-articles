<p>The Raspberry Pi camera module is a great accessory for the Pi—it's great quality, and can capture still photos and record video in full HD (1080p). The original 5-megapixel camera module was <a href="https://www.raspberrypi.org/blog/camera-board-available-for-sale/" target="_blank">released</a> in 2013, and a new 8-megapixel version was <a href="https://www.raspberrypi.org/blog/new-8-megapixel-camera-board-sale-25/" target="_blank">released</a> in April this year. Both versions are compatible with all Raspberry Pi models. There are also two variations—a regular visible light camera, and an infra-red camera—both available for US$ 25.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on Raspberry Pi</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/resources/what-raspberry-pi?src=raspberry_pi_resource_menu1&amp;intcmp=701f2000000h4RcAAI">What is Raspberry Pi?</a></li>
<li><a href="https://opensource.com/downloads/raspberry-pi-guide?src=raspberry_pi_resource_menu2&amp;intcmp=701f2000000h4RcAAI">eBook: Guide to Raspberry Pi</a></li>
<li><a href="https://opensource.com/downloads/getting-started-raspberry-pi-cheat-sheet?src=raspberry_pi_resource_menu3&amp;intcmp=701f2000000h4RcAAI">Getting started with Raspberry Pi cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/kubernetes-raspberry-pi?src=raspberry_pi_resource_menu6&amp;intcmp=701f2000000h4RcAAI">eBook: Running Kubernetes on your Raspberry Pi</a></li>
<li><a href="https://www.redhat.com/en/resources/data-intensive-applications-hybrid-cloud-blueprint-detail?src=raspberry_pi_resource_menu7&amp;intcmp=701f2000000h4RcAAI">Whitepaper: Data-intensive intelligent applications in a hybrid cloud blueprint</a></li>
<li><a href="https://www.redhat.com/en/topics/edge-computing?src=raspberry_pi_resource_menu5&amp;intcmp=701f2000000h4RcAAI">Understanding edge computing</a></li>
<li><a href="https://opensource.com/tags/raspberry-pi?src=raspberry_pi_resource_menu4&amp;intcmp=701f2000000h4RcAAI">Our latest on Raspberry Pi </a></li>
</ul></div>
</div>
</div>
</div>

<p>The camera module is high spec and much better quality than a basic USB webcam. Its feature-packed firmware fully utilizes the power of the VideoCore GPU in the <a href="https://www.raspberrypi.org/help/faqs/#generalSoCdefined" target="_blank">Raspberry Pi SoC</a>, allowing recording 1080p video at 30fps, 720p at 60fps, and VGA resolution (640x480) at 90fps—perfect for slow-motion playback.</p>
<h2>Get started</h2>
<p>First, with the Pi switched off, you'll need to connect the camera module to the Raspberry Pi's camera port, then start up the Pi and ensure the software is enabled. Locate the camera port on your Raspberry Pi and connect the camera:</p>
<p class="rtecenter"><img alt="" class="media-image attr__typeof__foaf:Image img__fid__318626 img__view_mode__default attr__format__default attr__field_file_image_alt_text[und][0][value]__pi camera attr__field_file_image_title_text[und][0][value]__pi attr__field_file_image_caption[und][0][value]__&amp;lt;p&amp;gt;&amp;lt;a  data-cke-saved-href=&amp;quot;https://www.raspberrypi.org/&amp;quot; href=&amp;quot;https://www.raspberrypi.org/&amp;quot; target=&amp;quot;_blank&amp;quot;&amp;gt;The Raspberry Pi Foundation&amp;lt;/a&amp;gt;.&amp;amp;nbsp;&amp;lt;a  data-cke-saved-href=&amp;quot;https://creativecommons.org/licenses/by-sa/4.0/&amp;quot; href=&amp;quot;https://creativecommons.org/licenses/by-sa/4.0/&amp;quot; target=&amp;quot;_blank&amp;quot;&amp;gt;CC BY-SA 4.0&amp;lt;/a&amp;gt;.&amp;amp;nbsp;&amp;lt;/p&amp;gt; attr__field_file_image_caption[und][0][format]__panopoly_wysiwyg_text attr__field_folder[und]__9402" src="https://opensource.com/sites/default/files/resize/f1-520x373.jpg" style="width: 520px; height: 373px;" title="" typeof="foaf:Image" width="520" height="373" /></p>
<p><sup>Dave Jones, CC BY-SA</sup></p>
<p>Ensure the camera software is enabled in the Raspberry Pi Configuration tool:</p>
<p class="rtecenter"><img alt="screenshot" class="media-image attr__typeof__foaf:Image img__fid__318631 img__view_mode__default attr__format__default attr__field_file_image_alt_text[und][0][value]__screenshot attr__field_file_image_title_text[und][0][value]__screenshot attr__field_file_image_caption[und][0][value]__&amp;lt;p&amp;gt;&amp;lt;a  data-cke-saved-href=&amp;quot;https://www.raspberrypi.org/&amp;quot; href=&amp;quot;https://www.raspberrypi.org/&amp;quot; target=&amp;quot;_blank&amp;quot;&amp;gt;The Raspberry Pi Foundation&amp;lt;/a&amp;gt;.&amp;amp;nbsp;&amp;lt;a  data-cke-saved-href=&amp;quot;https://creativecommons.org/licenses/by-sa/4.0/&amp;quot; href=&amp;quot;https://creativecommons.org/licenses/by-sa/4.0/&amp;quot; target=&amp;quot;_blank&amp;quot;&amp;gt;CC BY-SA 4.0&amp;lt;/a&amp;gt;.&amp;amp;nbsp;&amp;lt;/p&amp;gt; attr__field_file_image_caption[und][0][format]__panopoly_wysiwyg_text attr__field_folder[und]__9402" height="449" src="https://opensource.com/sites/default/files/f2.png" title="screenshot" typeof="foaf:Image" width="517" /></p>
<p>Test your camera by opening a terminal window and entering <code>raspistill -k</code>. This will show you a camera preview on the monitor. If you're connected via SSH or VNC, this will be shown on the Pi's monitor, not yours. Press <i>Ctrl</i> + <i>C</i> to exit the preview.</p>
<h2>Python</h2>
<p>Although you can control the camera using the command-line interface <code>raspistill</code>, using the Python picamera module is much easier and allows you to change the camera controls dynamically in real time—ideal for projects.</p>
<p>Open the Python 3 editor, <a href="https://wiki.python.org/moin/IDLE" target="_blank">IDLE</a>, create a new file and type the following code:</p>
<pre>
from picamera import PiCamera
from time import sleep

camera = PiCamera()

camera.start_preview()
sleep(3)
camera.capture('/home/pi/Desktop/image.jpg')
camera.stop_preview()
</pre><p>Now run the code and it should show the preview for three seconds before capturing a photo. The photo will be saved on your desktop, and you should see an icon with a thumbnail appear right away. Double-click the icon on your desktop to see the picture.</p>
<p>You can manipulate the camera object in various ways. You can alter the brightness and contrast with values between 0 and 100: <code> camera.brightness = 70 camera.contrast = 40 </code> You can add text to the image with: <code> camera.</code></p>
<p><code>annotate_text = “Hello world"</code></p>
<p>You can alter the image effect with:</p>
<p><code>camera.image_effect = “colorswap"</code></p>
<p>Also try out effects, such as <i>sketch</i>, <i>negative</i>, and <i>emboss</i>. A list of effects is provided in <code>camera.</code></p>
<p><code>IMAGE_EFFECTS</code>, which you can loop over and makes a great demo:</p>
<pre>
camera.start_preview()
for effect in camera.IMAGE_EFFECTS:
    camera.image_effect = effect
    camera.annotate_text = effect
    sleep(5)
camera.stop_preview()
</pre><p>There are many more attributes you can alter, such as resolution, zoom, ISO, white-balance modes, and exposure modes. See the <a href="http://picamera.readthedocs.io" target="_blank">picamera documentation</a> for more details.</p>
<h2>Video</h2>
<p>Recording video is just as easy—simply use the methods <code>start_recording()</code> and <code>stop_recording()</code>:</p>
<pre>
camera.start_preview()
camera.start_recording('/home/pi/video.h264')
sleep(10)
camera.stop_recording()
camera.stop_preview()
</pre><p>Then play back using omxplayer. Note the video may play back at a higher frame rate than was recorded.</p>
<h2>Infrared</h2>
<p>The Raspberry Pi infrared camera (Pi NoIR) was made especially because people were buying the regular camera and taking it apart to remove the infrared filter—with varying success—so the Foundation decided to produce a special camera without the infrared filter. The API works exactly the same, and in visible light, pictures will appear mostly normal, but they can also see infrared light, allowing capturing and recording at night.</p>
<p class="rtecenter"><img alt="Pi camera" class="media-image attr__typeof__foaf:Image img__fid__318641 img__view_mode__default attr__format__default attr__field_file_image_alt_text[und][0][value]__Pi camera attr__field_file_image_title_text[und][0][value]__Pi attr__field_file_image_caption[und][0][value]__&amp;lt;p&amp;gt;&amp;lt;a  data-cke-saved-href=&amp;quot;https://www.raspberrypi.org/&amp;quot; href=&amp;quot;https://www.raspberrypi.org/&amp;quot; target=&amp;quot;_blank&amp;quot;&amp;gt;The Raspberry Pi Foundation&amp;lt;/a&amp;gt;.&amp;amp;nbsp;&amp;lt;a  data-cke-saved-href=&amp;quot;https://creativecommons.org/licenses/by-sa/4.0/&amp;quot; href=&amp;quot;https://creativecommons.org/licenses/by-sa/4.0/&amp;quot; target=&amp;quot;_blank&amp;quot;&amp;gt;CC BY-SA 4.0&amp;lt;/a&amp;gt;.&amp;amp;nbsp;&amp;lt;/p&amp;gt; attr__field_file_image_caption[und][0][format]__panopoly_wysiwyg_text attr__field_folder[und]__9402" height="416" src="https://opensource.com/sites/default/files/f4_nuttall.png" title="Pi camera" typeof="foaf:Image" width="520" /></p>
<p>This is great for wildlife cameras, such as the <a href="http://naturebytes.org/" target="_blank">Naturebytes kit</a>, projects like the <a href="https://www.raspberrypi.org/learning/infrared-bird-box/" target="_blank">infrared bird box</a>, and various security camera projects. The IR camera has even been used to <a href="https://www.raspberrypi.org/blog/penguin-lifelines/" target="_blank">monitor penguins in Antarctica</a>.</p>
<p>Also the camera can be used to <a href="https://www.raspberrypi.org/blog/whats-that-blue-thing-doing-here/" target="_blank">monitor the health of green plants</a>.</p>
<h2>Pi Zero</h2>
<p>When the <a href="https://opensource.com/business/15/11/raspberry-pi-zero" target="_blank">$5 Pi Zero was announced</a> last year, it did not feature a camera connector due to its bare bones minimalist nature; however, last month a <a href="https://www.raspberrypi.org/blog/zero-grows-camera-connector/" target="_blank">new version of the Zero</a> was announced, which added a camera port.</p>
<p class="rtecenter"><img alt="" class="media-image attr__typeof__foaf:Image img__fid__318646 img__view_mode__default attr__format__default attr__field_file_image_alt_text[und][0][value]__ attr__field_file_image_title_text[und][0][value]__ attr__field_file_image_caption[und][0][value]__&amp;lt;p&amp;gt;&amp;lt;a  data-cke-saved-href=&amp;quot;https://www.raspberrypi.org/&amp;quot; href=&amp;quot;https://www.raspberrypi.org/&amp;quot; target=&amp;quot;_blank&amp;quot;&amp;gt;The Raspberry Pi Foundation&amp;lt;/a&amp;gt;.&amp;amp;nbsp;&amp;lt;a  data-cke-saved-href=&amp;quot;https://creativecommons.org/licenses/by-sa/4.0/&amp;quot; href=&amp;quot;https://creativecommons.org/licenses/by-sa/4.0/&amp;quot; target=&amp;quot;_blank&amp;quot;&amp;gt;CC BY-SA 4.0&amp;lt;/a&amp;gt;.&amp;amp;nbsp;&amp;lt;/p&amp;gt; attr__field_file_image_caption[und][0][format]__panopoly_wysiwyg_text attr__field_folder[und]__9402" height="390" src="https://opensource.com/sites/default/files/f6_nuttall.png" title="" typeof="foaf:Image" width="520" /></p>
<p>The connector is smaller than the regular one. In fact, the same connector is used on the compute module, but a cable can be used to connect a camera. Both spins—visible and infrared—and both versions (V1 and V2) work with the new Pi Zero.</p>
<h2>More ideas</h2>
<p>There's plenty more to read up on what you can do with the camera module, and why not tie in with some GPIO for more physical computing projects?</p>
<ul><li>See the <a href="http://picamera.readthedocs.io" target="_blank">picamera documentation</a></li>
<li>Work through the <a href="https://www.raspberrypi.org/learning/getting-started-with-picamera/" target="_blank">Getting Started with Picamera</a> resource.</li>
<li>Try using <a href="http://gpiozero.readthedocs.io" target="_blank">GPIO Zero</a> in combination with Picamera (add a button or a motion sensor).</li>
<li>Build a robot with sensors and a camera.</li>
<li>Use the <a href="https://www.raspberrypi.org/learning/getting-started-with-the-twitter-api/" target="_blank">Twitter API</a> to <a href="https://www.raspberrypi.org/learning/tweeting-babbage/" target="_blank">post your photos to Twitter</a>.</li>
<li>Make an <a href="https://www.raspberrypi.org/learning/infrared-bird-box/" target="_blank">infrared bird box</a>. Also check out the <a href="http://naturebytes.org/" target="_blank">Naturebytes</a> wildlife camera kit.</li>
<li>Make a <a href="https://www.raspberrypi.org/learning/parent-detector/" target="_blank">parent detector</a>.</li>
<li>Set up for <a href="https://github.com/raspberrypilearning/timelapse-setup" target="_blank">time-lapse</a> or <a href="https://www.raspberrypi.org/learning/push-button-stop-motion/" target="_blank">stop motion</a>.</li>
</ul>