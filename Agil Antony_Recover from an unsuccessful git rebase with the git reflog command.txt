<p>The <a href="https://opensource.com/article/22/4/manage-git-commits-rebase-i-command" rel="noopener" target="_blank">git rebase</a> command allows you to adjust the history of your Git repository. It's a useful feature, but of course, mistakes can be made. As is usually the case with Git, you can repair your error and restore your repository to a former state. To recover from an unsuccessful rebase, use the <code>git reflog</code> command.</p>
<h2 id="_git_reflog">Git reflog</h2>
<p>Suppose you perform this interactive rebase:</p>
<pre>
<code data-lang="bash">$ git rebase -i HEAD~20</code></pre><p>In this context, <code>~20</code> means to rebase the last 20 commits.</p>
<p>Unfortunately, in this imaginary scenario, you mistakenly squashed or dropped some commits you didn't want to lose. You've already completed the rebase, but this is Git, so of course, you can recover your lost commits.</p>
<h2 id="_review_your_history_with_reflog">Review your history with reflog</h2>
<p> Run the <code>git reflog</code> command to collect data and view a history of your interactions with your repository. This is an example for my demonstration repository, however, the result will vary depending on your actual repository:</p>
<pre>
<code data-lang="bash">$ git reflog
222967b (HEAD -&gt; main) HEAD@{0}: rebase (finish): returning to refs/heads/main
222967b (HEAD -&gt; main) HEAD@{1}: rebase (squash): My big rebase
c388f0c HEAD@{2}: rebase (squash): # This is a combination of 20 commits
56ee04d HEAD@{3}: rebase (start): checkout HEAD~20
0a0f875 HEAD@{4}: commit: An old good commit
[...]</code></pre><h2 id="_find_the_last_good_commit">Find the last good commit</h2>
<p>In this example, <code>HEAD@{3}</code> represents the start of your rebase. You can tell because its description is <code>rebase (start)</code>.</p>
<p>The commit just under it, <code>0a0f875 HEAD@{4}</code>, is the tip of your Git branch before you executed your incorrect rebase. Depending on how old and active your repository is, there are likely more lines below this one, but assume this is the commit you want to restore.</p>
<p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">More on Git</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://opensource.com/resources/what-is-git?intcmp=7013a000002CxqLAAS" data-analytics-category="resource list" data-analytics-text="What is Git?">What is Git?</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-git?intcmp=7013a000002CxqLAAS" data-analytics-category="resource list" data-analytics-text="Git cheat sheet">Git cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-markdown?intcmp=7013a000002CxqLAAS" data-analytics-category="resource list" data-analytics-text="Markdown cheat sheet">Markdown cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/git-tricks-tips?intcmp=7013a000002CxqLAAS" data-analytics-category="resource list" data-analytics-text="eBook: Git tips and tricks">eBook: Git tips and tricks</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Register for your free Red Hat account">Register for your free Red Hat account</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/git?intcmp=7013a000002CxqLAAS" data-analytics-category="resource list" data-analytics-text="New Git articles">New Git articles</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<h2 id="_restore_the_commit">Restore the commit</h2>
<p>To recover the commit you accidentally squashed and all of its parent commits, including those accidentally squashed or dropped, use <code>git checkout</code>. In this example, <code>HEAD@{4}</code> is the commit you need to restore, so that's the one to check out:</p>
<pre>
<code data-lang="bash">$ git checkout HEAD@{4}</code></pre><p>With your good commit restored, you can create a new branch using <code>git checkout -b <em>&lt;branch_name&gt;</em></code> as usual. Replace <code><em>&lt;branch_name&gt;</em></code> with your desired branch name, such as <code>test-branch</code>.</p>
<h2 id="_git_version_control">Git version control</h2>
<p>Git's purpose is to track versions, and its default setting is usually to preserve as much data about your work as feasible. Learning to use new Git commands makes many of its most powerful features available and safeguards your work.</p>
