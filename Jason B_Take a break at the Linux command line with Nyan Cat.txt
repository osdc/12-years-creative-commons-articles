<p>We're now on day six of the Linux command-line toys advent calendar, where we explore some of the fun, entertaining, and in some cases, utterly useless toys available for your Linux terminal. All are available under an open source license.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>The Linux Terminal</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/life/17/10/top-terminal-emulators?intcmp=7016000000127cYAAQ">Top 7 terminal emulators for Linux</a></li>
<li><a href="https://opensource.com/article/17/2/command-line-tools-data-analysis-linux?intcmp=7016000000127cYAAQ">10 command-line tools for data analysis in Linux</a></li>
<li><a href="https://opensource.com/downloads/advanced-ssh-cheat-sheet?intcmp=7016000000127cYAAQ">Download Now: SSH cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=7016000000127cYAAQ">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://opensource.com/tags/command-line?intcmp=7016000000127cYAAQ">Linux command line tutorials</a></li>
</ul></div>
</div>
</div>
</div>
<p>Will they all be unique? Yes. Will they all be unique to you? I don't know, but, chances are you'll find at least one new toy to play with by the time our advent calendar is done.</p>
<p>Today's selection is a continuation on the <a href="https://opensource.com/article/18/12/linux-toy-lolcat">theme</a> we started yesterday: cats and rainbows. Wait, there's more cat-related rainbow fun to be had at the Linux command line? You bet there is.</p>
<p>So let's make a throwback all the way to 2011's <a href="https://en.wikipedia.org/wiki/Nyan_Cat" target="_blank">Nyan Cat</a> with a command-line toy call, not surprisingly, <strong>nyancat</strong>. Did you miss the cultural phenomenon that was Nyan Cat? Watch the embed below, I'll wait.</p>
<p><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" frameborder="0" height="500" src="https://www.youtube.com/embed/QH2-TGUlwu4" width="737"></iframe></p>
<p>Now, let's recreate that amazing experience in your terminal. <strong>Nyancat</strong> is packaged for many distributions (Arch, Debian, Gentoo, Ubuntu, etc.) but not for mine (Fedora), but compiling from source was simple.</p>
<p>Per a great reminder from a reader, this would be a good point in the series for me to mention: be careful about installing applications from untrusted sources or compiling and running any piece code you find online, just because you find them in an article like this one. If you're not sure, use appropriate precautions, especially if you're on a production machine.</p>
<p>I cloned the source with:</p>
<pre><code class="language-bash">git clone https://github.com/klange/nyancat.git</code></pre><p>Then I used <strong>make</strong><strong> </strong>to compile, and ran the application with <strong>./nyancat</strong>.</p>
<p>This launched straight into a <strong>nyancat</strong> experience complete with a counter of how long I had been enjoying the <strong>nyancat</strong> magic for.</p>
<p><article class="media media--type-image media--view-mode-full" title="Linux toy: nyancat"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/linux-toy-nyancat-animated.gif" width="600" height="408" alt="Linux toy: nyancat" title="Linux toy: nyancat" /></div>
      
  </article></p>
<p>You can find the source for <strong>nyancat</strong> <a href="https://github.com/klange/nyancat" target="_blank">on GitHub</a> under an <a href="http://en.wikipedia.org/wiki/University_of_Illinois/NCSA_Open_Source_License" target="_blank">NCSA open source license</a>.</p>
<p>The command-line version of Nyan Cat used to be <a href="http://nyancat.dakko.us/" target="_blank">accessible by a public Telnet server</a> (or, for even more pun, with <a href="http://netcat.sourceforge.net/" target="_blank">netcat</a>) so that you didn't even have to install it, but sadly was shut down due to bandwidth limitations. Nevertheless, the <a href="http://nyancat.dakko.us/" target="_blank">gallery</a> from the old Telnet server running Nyan Cat on a variety of old devices is well-worth checking out, and maybe you'd like to do the community a favor by launching your own public mirror and letting the author know so that they may share it with the public yet again?</p>
<p>Do you have a favorite command-line toy that you think I ought to profile? The calendar for this series is mostly filled out but I've got a few spots left. Let me know in the comments below, and I'll check it out. If there's space, I'll try to include it. If not, but I get some good submissions, I'll do a round-up of honorable mentions at the end.</p>
<p>Check out yesterday's toy, <a href="https://opensource.com/article/18/12/linux-toy-lolcat">Bring some color to your Linux terminal with lolcat</a>, and check back tomorrow for another!</p>
