import csv
import argparse
from sys import argv

def getOptions():
    args = argv[1:]
    parser = argparse.ArgumentParser(description="Parses command.",argument_default=argparse.SUPPRESS)

    parser.add_argument('input', type=str, nargs='?',help='Input file')

    options = parser.parse_args(args)
    opts = vars(options)
    
    return opts

def doTheThing():
    print("doing the thing on this file:")
    opts = getOptions()
    print(opts['input'])
    
    with open(opts['input'], 'r') as csv_file:
        reader = csv.reader(csv_file)
            
        # 0title,1Body,2"Authored by",3Co-authors,4Credits
        for row in reader:
            if (len(row) < 5):
                print("Less than 5!! Detected " + str(len(row)) + " → " + row[0])
                print(len(row))
                try:
                    print(row[1])
                    print(row[2])
                    print(row[3])
                    print(row[4])                
                except:
                    pass
                
            print(len(row))
            try:
                if row[3] == "No co-authors":
                    row[3] = ''
                else:
                    row[3] = row[3] + "_"
            except:
                row[3] = ''

            try:
                if row[4] == "No credited authors":
                    row[4] = ''
                else:
                    row[4] = row[4] + "_"
            except:
                row[4] = ''

            filename=row[2].replace("/", "-") + "_" + row[3] + row[0].replace("/", "-") + ".txt"
            with open("./articles/" + filename.strip(' '), 'w') as f:
                f.write(row[1])
            f.close()

        csv_file.close()

if __name__ == "__main__":
    doTheThing()
