<p>Last week, Red Hatter Máirín Duffy gave us a run down of her <a title="Duffy's favorite open source creative tools" href="https://opensource.com/life/12/9/tour-through-open-source-creative-tools" target="_blank">favorite open source creative tools</a>. This week, we turn the table and ask you which application you favor in every day practice.</p>
<!--break-->
<p>Did one of these tools make or break a big campaign or project? Perhaps you impressed yourself, your lunch group, or your boss?</p>
<p>Share your stories about using creative design tools with us and we'll see if your story is dazzling enough to be featured on our homepage next week.</p>
