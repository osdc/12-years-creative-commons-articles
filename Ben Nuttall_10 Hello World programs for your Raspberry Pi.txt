<p>"Hello world" is the beginning of everything when it comes to computing and programming. It's the first thing you learn in a new programming language, and it's the way you test something out or check to see if something's working because it's usually the simplest way of testing simple functionality.</p>
<p>Warriors of programming language wars often cite their own language's "hello world" against that of another, saying theirs is <i>shorter</i> or <i>more concise</i> or <i>more explicit</i> or something. Having a nice simple readable "hello world" program makes for a good intro for beginners learning your language, library, framework, or tool.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on Raspberry Pi</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/resources/what-raspberry-pi?src=raspberry_pi_resource_menu1&amp;intcmp=701f2000000h4RcAAI">What is Raspberry Pi?</a></li>
<li><a href="https://opensource.com/downloads/raspberry-pi-guide?src=raspberry_pi_resource_menu2&amp;intcmp=701f2000000h4RcAAI">eBook: Guide to Raspberry Pi</a></li>
<li><a href="https://opensource.com/downloads/getting-started-raspberry-pi-cheat-sheet?src=raspberry_pi_resource_menu3&amp;intcmp=701f2000000h4RcAAI">Getting started with Raspberry Pi cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/kubernetes-raspberry-pi?src=raspberry_pi_resource_menu6&amp;intcmp=701f2000000h4RcAAI">eBook: Running Kubernetes on your Raspberry Pi</a></li>
<li><a href="https://www.redhat.com/en/resources/data-intensive-applications-hybrid-cloud-blueprint-detail?src=raspberry_pi_resource_menu7&amp;intcmp=701f2000000h4RcAAI">Whitepaper: Data-intensive intelligent applications in a hybrid cloud blueprint</a></li>
<li><a href="https://www.redhat.com/en/topics/edge-computing?src=raspberry_pi_resource_menu5&amp;intcmp=701f2000000h4RcAAI">Understanding edge computing</a></li>
<li><a href="https://opensource.com/tags/raspberry-pi?src=raspberry_pi_resource_menu4&amp;intcmp=701f2000000h4RcAAI">Our latest on Raspberry Pi </a></li>
</ul></div>
</div>
</div>
</div>
<p>I thought it would be cool to create a list of as many different "hello world" programs as possible that can be run on the <a href="https://opensource.com/resources/raspberry-pi">Raspberry Pi</a> using its Raspbian operating system, but without installing any additional software than what comes bundled when you download it from the Raspberry Pi website. I've created a <a href="https://github.com/bennuttall/hello-world-raspberry-pi" target="_blank">GitHub repository</a> of these programs, and I've explained 10 of them for you here.</p>
<h2>1. Scratch</h2>
<p><a href="https://opensource.com/sitewide-search?search_api_views_fulltext=scratch">Scratch</a> is a graphical block-based programming environment designed for kids to learn programming skills without having to type or learn the synax of a programming language. The "hello world" for Scratch is simple—and very visual!</p>
<p>1. Open <strong>Scratch 2</strong> from the main menu.</p>
<p>2. Click <strong>Looks</strong>.</p>
<p>3. Drag a <strong>say Hello!</strong> block into the workspace on the right.</p>
<p>4. Change the text to <code>Hello world</code>.</p>
<p><article class="media media--type-image media--view-mode-full" title="Hello world, Scratch program"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/scratch2-1.png" width="124" height="49" alt="Hello world, Scratch program" title="Hello world, Scratch program" /></div>
      
  </article></p>
<p>5. Click on the block to run the code.</p>
<p><article class="media media--type-image media--view-mode-full" title="Scratch logo"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/scratch2-2.png" width="204" height="181" alt="Scratch logo" title="Scratch logo" /></div>
      
  </article></p>
<h2>2. Python</h2>
<p><a href="https://opensource.com/tags/python">Python</a> is a powerful and professional language that's also great for beginners— and it's lots of fun to learn. Because one of Python's main objectives was to be readable and stick to simple English, its "hello world" program is as simple as possible.</p>
<ol><li>Open <strong>Thonny Python IDE</strong> from the main menu.</li>
<li>Enter the following code:<br /><pre><code class="language-python">print("Hello world")</code></pre></li>
<li>Save the file as <code>hello3.py</code>.</li>
<li>Click the <strong>Run</strong> button.</li>
</ol><p><article class="media media--type-image media--view-mode-full" title="Python hello world screenshot"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/python3-1.png" width="694" height="199" alt="Python hello world screenshot" title="Python hello world screenshot" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<h2>3. Ruby/Sonic Pi</h2>
<p><a href="https://opensource.com/sitewide-search?search_api_views_fulltext=ruby">Ruby</a> is another powerful language that's friendly for beginners. <a href="http://sonic-pi.net/" target="_blank">Sonic Pi</a>, the live coding music synth, is built on top of Ruby, so what users actually type is a form of Ruby.</p>
<ol><li>Open <strong>Sonic Pi</strong> from the main menu.</li>
<li>Enter the following code:<br /><pre><code class="language-bash">puts "Hello world"</code></pre></li>
<li>Press <strong>Run</strong>.</li>
</ol><p><article class="media media--type-image media--view-mode-full" title="Sonic Pi hello world message"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/sonic-pi-1.png" width="264" height="262" alt="Sonic Pi hello world message" title="Sonic Pi hello world message" /></div>
      
  </article></p>
<p>Unfortunately, "hello world" does not do Sonic Pi justice in the slightest, but after you've finished this article you should check out its creator <a href="https://www.youtube.com/watch?v=KJPdbp1An2s" target="_blank">Sam Aaron live coding</a>, and see the <a href="http://sonic-pi.net/" target="_blank">tutorials on the Sonic Pi website</a>.</p>
<p>Alternatively, to using the Sonic Pi application for this example, you can write Ruby code in a text editor and run it in the terminal:</p>
<ol><li>Open <strong>Text Editor</strong> from the main menu.</li>
<li>Enter the following code:<br /><pre><code class="language-bash">puts "Hello world"</code></pre></li>
<li>Save the file as <code>hello.rb</code> in the home directory.</li>
<li>Open <strong>Terminal</strong> from the main menu.</li>
<li>Run the following command:<br /><pre><code class="language-bash">ruby hello.rb</code></pre></li>
</ol><p><article class="media media--type-image media--view-mode-full" title="Ruby hello world message"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/ruby-1.png" width="697" height="142" alt="Ruby hello world message" title="Ruby hello world message" /></div>
      
  </article></p>
<h2>4. JavaScript</h2>
<p>This is a bit of a cheat as I just make use of client-side <a href="https://opensource.com/tags/javascript">JavaScript</a> within the web browser using the Web Inspector console, but it still counts!</p>
<ol><li>Open <strong>Chromium Web Browser</strong> from the main menu.</li>
<li>Right-click the empty web page and select <strong>Inspect</strong> from the context menu.</li>
<li>Click the <strong>Console</strong> tab.</li>
<li>Enter the following code:<br /><pre><code class="language-python">console.log("Hello world")</code></pre></li>
<li>Press <strong>Enter</strong> to run.</li>
</ol><p><article class="media media--type-image media--view-mode-full" title="javascript hello world message"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/javascript-1.png" width="556" height="168" alt="javascript hello world message" title="javascript hello world message" /></div>
      
  </article></p>
<p>You can also install NodeJS on the Raspberry Pi, and write server-side JavaScript, but that's not available in the standard Raspbian image.</p>
<h2>5. Bash</h2>
<p><a href="https://opensource.com/sitewide-search?search_api_views_fulltext=bash">Bash</a> (Bourne Again Shell) is the default Unix shell command language in most Linux distributions, including Raspbian. You can enter Bash commands directly into a terminal window, or script them into a file and execute the file like a programming script.</p>
<ol><li>Open <strong>Text Editor</strong> from the main menu.</li>
<li>Enter the following code:<br /><pre><code class="language-bash">echo "Hello world"</code></pre></li>
<li>Save the file as <code>hello.sh</code> in the home directory.</li>
<li>Open <strong>Terminal</strong> from the main menu.</li>
<li>Run the following command:<br /><pre><code class="language-bash">bash hello.sh</code></pre></li>
</ol><p><article class="media media--type-image media--view-mode-full" title="Bash hello world message"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/bash-1.png" width="675" height="127" alt="Bash hello world message" title="Bash hello world message" /></div>
      
  </article></p>
<p>Note you'd usually see a "hashbang" at the top of the script (<code>#!/bin/bash</code>), but because I'm calling this script directly using the <code>bash</code> command, it's not necessary (and I'm trying to keep all these examples as short as possible).</p>
<p>You'd also usually make the file executable with <code>chmod +x</code>, but again, this is not necessary as I'm executing with <code>bash</code>.</p>
<h3>6. Java</h3>
<p><a href="https://opensource.com/tags/java">Java</a> is a popular language in industry, and is commonly taught to undergraduates studying computer science. I learned it at university and have tried to avoid touching it since then. Apparently, now I do (very small amounts of) it for fun...</p>
<ol><li>Open <strong>Text Editor</strong> from the main menu.</li>
<li>Enter the following code:<br /><pre><code class="language-java">public class Hello {
        public static void main(String[] args) {
            System.out.println("Hello world");
        }
    }
       
</code></pre></li>
<li>Save the file as <code>Hello.java</code> in the home directory.</li>
<li>Open <strong>Terminal</strong> from the main menu.</li>
<li>Run the following commands:<br /><pre><code class="language-bash">javac Hello.java
java Hello</code></pre></li>
</ol><p><article class="media media--type-image media--view-mode-full" title="java hello world message"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/java-1.png" width="684" height="148" alt="java hello world message" title="java hello world message" /></div>
      
  </article></p>
<p>I could <i>almost</i> remember the "hello world" for Java off the top of my head, but not quite. I always forget where the <code>String[] args</code> bit goes, but it's obvious when you think about it...</p>
<h3>7. C</h3>
<p>C is a fundamental low-level programming language. It's what many programming languages are written in. It's what operating systems are written in. See for yourself&amp;mdash:take a look at the source for <a href="https://github.com/python/cpython/" target="_blank">Python</a> and <a href="https://github.com/torvalds/linux" target="_blank">the Linux kernel</a>. If that looks a bit hazy, get started with "hello world":</p>
<ol><li>Open <strong>Text Editor</strong> from the main menu.</li>
<li>Enter the following code:<br /><pre><code class="language-text">#include &lt;stdio.h&gt;

int main() {
    printf("Hello world\n");
}</code></pre></li>
<li>Save the file as <code>hello.c</code> in the home directory.</li>
<li>Open <strong>Terminal</strong> from the main menu.</li>
<li>Run the following commands:<br /><pre><code class="language-bash">gcc -o hello hello.c
./hello</code></pre></li>
</ol><p><article class="media media--type-image media--view-mode-full" title="C hello world message"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/c-1.png" width="689" height="147" alt="C hello world message" title="C hello world message" /></div>
      
  </article></p>
<p>Note that in the previous examples, only one command was required to run the code (e.g., <code>python3 hello.py</code> or <code>ruby hello.rb</code>) because these languages are interpreted rather than compiled. (Actually Python is compiled at runtime but that's a minor detail.) C code is compiled into byte code and the byte code is executed.</p>
<p>If you're interested in learning C, the Raspberry Pi Foundation publishes a book <a href="https://www.raspberrypi.org/magpi/issues/essentials-c-v1/" target="_blank">Learning to code with C</a> written by one of its engineers. You can buy it in print or download for free.</p>
<h3>8. C++</h3>
<p>C's younger bother, C++ (that's C incremented by one...) is another fundamental low-level language, with more advanced language features included, such as classes. It's popular in a range of uses, including game development, and chunks of your operating system will be written in C++ too.</p>
<ol><li>Open <strong>Text Editor</strong> from the main menu.</li>
<li>Enter the following code:<br /><pre><code class="language-text">#include &lt;iostream&gt;
using namespace std;

int main() {
    cout &lt;&lt; "Hello world\n";
}</code></pre></li>
<li>Save the file as <code>hello.cpp</code> in the home directory.</li>
<li>Open <strong>Terminal</strong> from the main menu.</li>
<li>Run the following commands:<br /><pre><code class="language-bash">g++ -o hellopp hello.cpp
./hellocpp</code></pre></li>
</ol><p><article class="media media--type-image media--view-mode-full" title="C++ hello world message"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/cpp-1.png" width="696" height="162" alt="C++ hello world message" title="C++ hello world message" /></div>
      
  </article></p>
<p>Readers familiar with C/C++ will notice I have not included the main function return values in my examples. This is intentional as to remove boilerplate, which is not strictly necessary.</p>
<h3>9. Perl</h3>
<p><a href="https://opensource.com/tags/perl">Perl</a> gets a lot of stick for being hard to read, but nothing much gets in the way of understanding its "hello world" program. So far, so good!</p>
<ol><li>Open <strong>Text Editor</strong> from the main menu.</li>
<li>Enter the following code:<br /><pre><code class="language-bash">print "Hello world\n"</code></pre></li>
<li>Save the file as <code>hello.pl</code> in the home directory.</li>
<li>Open <strong>Terminal</strong> from the main menu.</li>
<li>Run the following command:<br /><pre><code class="language-bash">perl hello.pl</code></pre></li>
</ol><p><article class="media media--type-image media--view-mode-full" title="Perl hello world message"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/perl-1.png" width="696" height="147" alt="Perl hello world message" title="Perl hello world message" /></div>
      
  </article></p>
<p>Again, I learned Perl at university, but unlike Java, I have managed to <i>successfully</i> avoid using it.</p>
<h3>10. Python extras: Minecraft and the Sense HAT emulator</h3>
<p>So that's nine different programming languages covered, but let's finish with a bit more Python. The popular computer game Minecraft is available for Raspberry Pi, and comes bundled with Raspbian. A Python library allows you to communicate with your Minecraft world, so open Minecraft and a Python editor side-by-side for some fun hacking your virtual world with code.</p>
<ol><li>Open <a href="https://opensource.com/life/15/5/getting-started-minecraft-pi"><strong>Minecraft Pi</strong></a> from the main menu.</li>
<li>Create and enter a Minecraft world.</li>
<li>Press <strong>Tab</strong> to release your focus from the Minecraft window.</li>
<li>Open <strong>Thonny Python IDE</strong> from the main menu.</li>
<li>Enter the following code:<br /><pre><code class="language-python">from mcpi.minecraft import Minecraft

mc = Minecraft.create()

mc.postToChat("Hello world")</code></pre></li>
<li>Save the file as <code>hellomc.py</code>.</li>
<li>Click the <strong>Run</strong> button.</li>
</ol><p><article class="media media--type-image media--view-mode-full" title="Minecraft hello world"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/mcpi-1.png" width="876" height="534" alt="Minecraft hello world" title="Minecraft hello world" /></div>
      
  </article></p>
<p>Read more about hacking Minecraft with Python in my article <a href="https://opensource.com/life/15/5/getting-started-minecraft-pi">Getting started with Minecraft Pi</a>.</p>
<p>Finally, let's look at the <a href="https://opensource.com/life/16/9/coding-raspberry-pi-web-emulator">Sense HAT Emulator</a>. This tool provides a graphical representation of the <a href="https://opensource.com/life/15/10/exploring-raspberry-pi-sense-hat">Sense HAT</a>, an add-on board for Raspberry Pi made especially to go to space for reasons explained in <a href="https://opensource.com/education/15/4/uk-students-compete-chance-have-their-raspberry-pi-code-run-space">this article</a>.</p>
<p>The <code>sense_emu</code> Python library is identical to the <code>sense_hat</code> library except that its commands get executed in the emulator rather than on a physical piece of hardware. Because the Sense HAT includes an 8x8 LED display, we can use its <code>show_message</code> function to write "hello world".</p>
<ol><li>Open another tab in Thonny and enter the following code:<br /><pre><code class="language-python">from sense_emu import SenseHat

sense = SenseHat()

sense.show_message("Hello world")</code></pre></li>
<li>Save the file as <code>sense.py</code>.</li>
<li>Click the <strong>Run</strong> button.</li>
</ol><p><article class="media media--type-image media--view-mode-full" title="sense hat hello world"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/sense-emu-1.png" width="737" height="428" alt="sense hat hello world" title="sense hat hello world" /></div>
      
  </article></p>
<h3>More</h3>
<p>That's it! I hope you learned something new, and have fun trying out new "hello world" programs on your Raspberry Pi!</p>
<p>You can find more on the <a href="https://github.com/bennuttall/hello-world-raspberry-pi">GitHub repository</a> —and feel free to suggest more in an issue, or send me a pull request with your contribution. If you have any other interesting "hello world" programs you want to share—related to Raspberry Pi or not—please share them in the comments below.</p>
