<p>In this edition of our open source news roundup, we take a look at GitHub and its efforts to protect open source in the EU, a novel idea for sustainable open source, the next FIDO2 security key being open source, and more.</p>
<h2>GitHub goes to Brussels</h2>
<p>GitHub’s Abby Vollmer writes about <a href="https://blog.github.com/2018-10-23-github-goes-to-brussels/">GitHub visiting Brussels</a> last week, together with OpenForum Europe and Red Hat. GitHub hosted an event with the intent of informing developers and EU policymakers about open source and copyright.</p>
<p>The purpose of this event was to raise awareness about the <a href="https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:52016PC0593">EU Copyright Directive</a> and the possible effect it could have on open source software in Europe. In her article, Abby further shares the status of open source in the negotiations around this directive.</p>
<h2>Open source hardware could defend against the next generation of hacking</h2>
<p>Joshua M. Pearce from Michigan's Technological University <a href="https://theconversation.com/open-source-hardware-could-defend-against-the-next-generation-of-hacking-104473">starts his article</a> with an interesting analogy about trust. Joshua explains his choice of keeping a secret document in a safe, and how the company who built the safe published its designs in the open.</p>
<p>He continues to write about computer hardware, and compares this to the safe and its mechanisms kept secret by the manufacturer. By doing so, he asks whether we as a customer should learn from lessons in open source. </p>
<blockquote><p>Open-source software isn’t inherently or automatically more secure. But it creates more possibilities, and market pressure, for improving security.</p>
</blockquote>
<p>Open source hardware does provide the customer with a choice. People would be able to review the published designs and therefore its security. Joshua ends his article saying companies and governments can also be expected to adopt policies that favor open source hardware.</p>
<h2>Top open source projects 2018</h2>
<p><a href="https://www.zdnet.com/article/top-open-source-projects-2018-vscode-react-native-tensorflow/">Over at ZDNet</a> you can read up on part of GitHub's Octoverse report, and the top open source projects for 2018. The top 5 includes Vscode, React-native, TensorFlow, Angular-CLI, and Azure Docs. As Larry Dignan says in his article, this shows a fast growth in areas such as machine learning, 3D printing, and data analysis.</p>
<p><a href="https://octoverse.github.com/">The State of Octoverse</a> tells us that 31M+ developers from 2.1M+ organizations contributed 200M+ pull requests to 96M+ repositories. The report has detailed information broken in sections such as People, Projects, and Platform.</p>
<h2>How a donation model could show us a sustainable future for open source</h2>
<p>Matt Asay <a href="https://www.techrepublic.com/article/how-one-companys-donation-model-could-show-us-a-sustainable-future-for-open-source/#ftag=RSS56d97e7">writes for TechRepublic</a> about a company called Citus Data, who has a new idea to support open source through what is basically a donation model. Citus Data announced it will be giving 1% of its equity to the PostgreSQL organization.</p>
<p>Many organizations use open source solutions without contributing back. As the product of Citus Data depends fully on Postgres, their <a href="https://pledge1percent.org/">1% Pledge</a> makes a great example of making open source sustainable.</p>
<h2>In other news</h2>
<ul><li><a href="https://interestingengineering.com/arduino-wows-crowd-at-maker-faire-rome-2018">Arduino Wows Crowd at Maker Faire Rome 2018</a></li>
<li><a href="https://www.infoworld.com/article/3313342/open-source-tools/how-azure-became-the-place-for-open-source-in-the-cloud.html">How Azure became the place for open source in the cloud</a></li>
<li><a href="https://mobileidworld.com/new-fido2-security-key-will-be-open-source-910152/">New FIDO2 Security Key Will Be Open Source</a></li>
<li><a href="https://techcrunch.com/2018/10/16/mongodb-switches-up-its-open-source-license/">MongoDB switches up its open-source license</a></li>
<li><a href="https://www.linux.com/news/top-13-linux-and-open-source-conferences-2019-0">The Top 13 Linux and Open Source Conferences in 2019</a></li>
<li><a href="https://variety.com/2018/digital/news/academy-software-foundation-sony-pictures-warner-bros-1202989227/">Sony Pictures, Warner Bros. Join Academy Software Foundation</a></li>
<li><a href="https://www.zdnet.com/article/ibm-open-sources-mac-sysadmin-software/#ftag=RSSbaffb68">IBM open-sources Mac sysadmin software</a></li>
</ul><p><em>Thanks, as always, to Opensource.com staff members and moderators for their help this week. Make sure to check out <a href="https://opensource.com/resources/conferences-and-events-monthly">our event calendar</a> to see what's happening next week in open source.</em></p>
