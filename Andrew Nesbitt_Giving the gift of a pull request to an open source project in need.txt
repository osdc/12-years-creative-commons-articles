<p>On December 1st, <a href="https://24pullrequests.com/" target="_blank">24 Pull Requests</a> will be opening its virtual doors once again, asking you to give the gift of a pull request to an open source project in need.  Six years ago, inspired by <a href="https://24ways.org/" target="_blank">24 Ways</a> (an advent calendar for web geeks), I decided an advent calendar was a great way to motivate people to contribute to projects. Last year more than 16,000 pull requests were made by nearly 3,000 contributors through the site. And they're not all by programmers.</p>
<p>Often the contribution with the most impact might be an improvement to technical documentation, some tests, or even better—guidance for other contributors. The 24 Pull Requests website, for example, started off as a single html page and has received almost 900 pull requests over the years to turn it into the site it is today.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Programming and development</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ&amp;src=programming_resource_menu4">Red Hat Developers Blog</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Programming cheat sheets</a></li>
<li><a href="https://www.redhat.com/en/services/training/learning-subscription?intcmp=7016000000127cYAAQ">Try for free: Red Hat Learning Subscription</a></li>
<li><a href="https://opensource.com/downloads/bash-programming-guide?intcmp=7016000000127cYAAQ">eBook: An introduction to programming with Bash</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/bash-shell-cheat-sheet?intcmp=7016000000127cYAAQ">Bash Shell Scripting Cheat Sheet</a></li>
<li><a href="https://developers.redhat.com/e-books/modernizing-enterprise-java?intcmp=7016000000127cYAAQ">eBook: Modernizing Enterprise Java</a></li>
</ul></div>
</div>
</div>
</div>
<p>Contributing to open source is a great way to hone your skills, build your profile, make new friends, and become a part of the wider open source community. In my view, there's no reason why a designer, editor, or anyone else should miss out on that. Also, 24 Pull Requests is a great way for new contributors to do <a href="https://twitter.com/_notwaving/status/805016973168046080" target="_blank">their first pull requests</a>. </p>
<p>You can see a list of events on the <a href="https://24pullrequests.com/events" target="_blank">24 Pull Requests site</a>. Last year we had seven events, so check back as the list grows. <a href="https://codebar.io/" target="_blank">Codebar</a> and <a href="https://twitter.com/ladiesofcode" target="_blank">Ladies of Code</a> have been running 24 Pull Request events in London for the past four years, and <a href="https://twitter.com/codebar" target="_blank">codebar</a> organizer Kimberly Cook wrote up a report on <a href="https://medium.com/the-codelog/24-pull-requests-2016-ef7e28d2e051" target="_blank">24 Pull Requests 2016</a>.</p>
<p>This year we want to encourage groups to get together to hack on projects. If there's not an event organized near you yet, you're only a click away from listing and hosting your own. If you can't get involved in a project or event directly, you can always <a href="https://24pullrequests.com/projects/new" target="_blank">suggest a project for 24 Pull Requests-ers to contribute to</a>.</p>
<p>So if you're looking to get into open source, or you know a project in need of attention, you can get involved by signing in with your GitHub account via the <a href="https://24pullrequests.com/" target="_blank">24 Pull Requests website</a>.</p>
<p>Happy holidays and happy hacking!</p>
