<p>Free and open source software (FOSS) plays an indispensable role in developing countries. As it is often a substitute for more expensive proprietary software, it can impact  the economy and progress of a country, like India, in a very positive way.</p>
<p><!--break--></p>
<p>A survey report on the ‘<a href="http://www.google.com/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;source=web&amp;cd=1&amp;ved=0CF4QFjAA&amp;url=http://www.iimb.ernet.in/%7Erahulde/RD_FOSSRep2009.pdf&amp;ei=aOczUP3zDMPI6wHqyoC4CA&amp;usg=AFQjCNG8Es2gbKJuLi9FO-Yoc1rcy0luUQ&amp;sig2=qP5HuKamiS9i4lsMRZjCbg&amp;cad=rja">Economic Impact of FOSS in India</a>’ by a team at IIM – Bangalore gave some interesting results. The study is based on 20 case studies from various Indian governmental departments and educational institutions with FOSS being used as an  operating system, server, or application.</p>
<p>Benefits realized by a few organizations:</p>
<ul><li>
<p>In Kerala (an Indian state), the government replaced Windows software with FOSS on 50,000 desktops in school across the state. They saved nearly $10.2 million USD.</p>
</li>
<li>
<p>The Life Insurance Corporation (LIC), one of the largest insurers in India, replaced their entire IT structure of 3,500 servers and 30,000 desktops with FOSS. They saved $8.75 million USD.</p>
</li>
<li>
<p>The New India Assurance Company, with IT infrastructure of 1,500 servers and 7,000 desktops, saved $16.67 million USD by adopting FOSS.</p>
</li>
</ul><p>Ultimately, this study shows that by replacing approximately 50 percent of an Indian company's proprietary software with open source software, it can save close to Rs. 10,000 crore (which is $1.8 billion USD).</p>
<p>But there's more than just the benefit of cost savings; an Indian company can escape from vendor lock-in and modify at their convenience. The ability to change and add new software remains a prevailing benefit of FOSS, and the custom of sharing helps employees become more innovative and contribute to the success and evolution of their organization. Open source software is a truly a valuable substitute in India, with many helpful attributes.</p>
<p>However, FOSS implementation requires increased awareness and encouragement among its users. Government organizations (and other businesses) must take  concrete steps for the successful procurement of FOSS—emphasizing IT adoption at all levels. And for there to be adoption, officials and staff must learn as much as they can about the subject. I hope to see further awareness and understanding brought about by government sponsored FOSS education and training courses in the near future.</p>
