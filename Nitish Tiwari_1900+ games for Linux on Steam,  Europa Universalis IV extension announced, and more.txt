<p>Hello, open gaming fans! In this week's edition, we take a look at 1900+ games available for Linux on Steam, extension of popular Europa Universalis IV announced, and new games out for Linux.</p>
<p><strong>Open gaming roundup for February 27 - March 4, 2016</strong></p>
<p><!--break--></p>
<h2>1900+ games available for Linux on Steam</h2>
<p>The total number of Linux games available on the Steam platform crossed 1900 recently. Not only is this a great news for the open source community as a whole, but also for Linux game developers specifically. As the ecosystem around Linux games grows, development will have better incentives, and that in turn will bring us more games.</p>
<p>Take a look at the <a href="http://store.steampowered.com/search/?term=#sort_by=Released_DESC&amp;category1=998&amp;os=linux&amp;page=1" target="_blank">1900+ games on Steam</a>.</p>
<h2>Extension of Europa Universalis IV announced</h2>
<p>Paradox Development Studio's best-selling historical grand strategy game <em>Europa Universalis IV</em> will have a new extension: Mare Nostrum. For those of you wondering about the name of the game, <em>mare nostrum</em> was the old Roman term for the Mediterranean, meaning <em>our sea.</em> Mare Nostrum, therefore, takes its name from the major changes made to the naval game in Europa Universalis. Read more in the announcement on <a href="http://linuxgamenews.com/post/140351329300/europa-universalis-iv-expansion-mare-nostrum#.Vtn27JMrJ-V" target="_blank">Linux Game News</a>.</p>
<h2>New games out for Linux</h2>
<h3>Balrum</h3>
<p><a href="http://store.steampowered.com/app/424250/" target="_blank">Balrum</a> is an old-school, hybrid turn-based, open world indie RPG with deep tactical combat. Explore a huge living fantasy world with dozens of side quests next to an epic main quest. As a player you'll control a young man whose peaceful life is suddenly interrupted by events which will change his life forever.</p>
<p><iframe allowfullscreen="" frameborder="0" height="293" src="https://www.youtube.com/embed/ghfEYjaMWM4" width="520"></iframe></p>
<h3>That Dragon, Cancer</h3>
<p><a href="http://store.steampowered.com/app/419460/" target="_blank">That Dragon, Cancer</a> is an immersive, narrative that retells Joel Green’s 4-year fight against cancer through about two hours of poetic, imaginative gameplay that explores themes of faith, hope, and love.</p>
<p><iframe allowfullscreen="" frameborder="0" height="293" src="https://www.youtube.com/embed/60ZCaupyHhc" width="520"></iframe></p>
