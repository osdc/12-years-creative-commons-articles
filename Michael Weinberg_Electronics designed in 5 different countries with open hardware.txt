<p>The Open Source Hardware Association's <a href="https://certification.oshwa.org/list.html" target="_blank">Hardware Registry</a> lists hardware from 29 different countries on five continents, demonstrating the broad, international footprint of certified open source hardware.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Open source hardware map"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/opensourcehardwaremap.jpg" width="650" height="488" alt="Open source hardware map" title="Open source hardware map" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>Michael Weinberg, CC BY 4.0</p>
</div>
      
  </article></p>
<p>In some ways, this international reach shouldn't be a surprise. Like many other open source communities, the open source hardware community is built on top of the internet, not grounded in any specific geographical location. The focus on documentation, sharing, and openness makes it easy for people in different places with different backgrounds to connect and work together to develop new hardware. Even the community-developed open source hardware <a href="https://www.oshwa.org/definition/" target="_blank">definition</a> has been translated into 11 languages from the original English.</p>
<p>Even if you're familiar with the international nature of open source hardware, it can still be refreshing to step back and remember what it means in practice. While it may not surprise you that there are many certifications from the United States, Germany, and India, some of the other countries boasting certifications might be a bit less expected. Let's look at six such projects from five of those countries.</p>
<h2 id="bulgaria">Bulgaria</h2>
<p>Bulgaria may have the highest per-capita open source hardware certification rate of any country on earth. That distinction is mostly due to the work of two companies: <a href="http://anavi.technology/" target="_blank">ANAVI Technology</a> and <a href="https://www.olimex.com/" target="_blank">Olimex</a>.</p>
<p>ANAVI focuses mostly on IoT projects built on top of the Raspberry Pi and ESP8266. The concept of "creator contribution" means that these projects can be certified open source even though they are built upon non-open bases. That is because all of ANAVI's work to develop the hardware on top of these platforms (ANAVI's "creator contribution") has been open sourced in compliance with the certification requirements.</p>
<p>The <a href="https://certification.oshwa.org/bg000001.html" target="_blank">ANAVI Light pHAT</a> was the first piece of Bulgarian hardware to be certified by OSHWA. The Light pHAT makes it easy to add a 12V RGB LED strip to a Raspberry Pi.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="ANAVI-Light-pHAT"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/anavi-light-phat.png" width="559" height="282" alt="ANAVI-Light-pHAT" title="ANAVI-Light-pHAT" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup><a href="http://anavi.technology/images/anavi-light-phat.jpg" target="_blank" rel="ugc">ANAVI-Light-pHAT</a></sup></p>
</div>
      
  </article></p>
<p>Olimex's first OSHWA certification was for the <a href="https://certification.oshwa.org/bg000010.html" target="_blank">ESP32-PRO</a>, a highly connectable IoT board built around an ESP32 microcontroller.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Olimex ESP32-PRO"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/olimex-esp32-pro.png" width="347" height="496" alt="Olimex ESP32-PRO" title="Olimex ESP32-PRO" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup><a href="https://www.olimex.com/Products/IoT/ESP32/ESP32-PRO/open-source-hardware" target="_blank" rel="ugc">Olimex ESP32-PRO</a></sup></p>
</div>
      
  </article></p>
<h2 id="china">China</h2>
<p>While most people know China is a hotbed for hardware development, fewer realize that it is also the home to a thriving <em>open source</em> hardware culture. One of the reasons is the tireless advocacy of Naomi Wu (also known as <a href="https://www.youtube.com/channel/UCh_ugKacslKhsGGdXP0cRRA" target="_blank">SexyCyborg</a>). It is fitting that the first piece of certified hardware from China is one she helped develop: the <a href="https://certification.oshwa.org/cn000001.html" target="_blank">sino:bit</a>. The sino:bit is designed to help introduce students to programming and includes China-specific features like a LED matrix big enough to represent Chinese characters.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="sino:bit"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/sinobit.png" width="650" height="650" alt="sino:bit" title="sino:bit" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p><a href="https://github.com/sinobitorg/hardware" target="_blank" rel="ugc">Sino:bit</a> image by Naomi Wu, <a href="https://opensource.org/licenses/MIT" target="_blank" rel="ugc">MIT License</a></p>
</div>
      
  </article></p>
<h2>Mexico</h2>
<p>Mexico has also produced a range of certified open source hardware. A recent certification is the <a href="https://certification.oshwa.org/mx000003.html" target="_blank">Meow Meow</a>, a capacitive touch interface from <a href="https://electroniccats.com/" target="_blank">Electronic Cats</a>. Meow Meow makes it easy to use a wide range of objects—bananas are always a favorite—as controllers for your computer.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Meow Meow"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/meowmeow.png" width="600" height="600" alt="Meow Meow" title="Meow Meow" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup><a href="https://electroniccats.com/producto/meowmeow/" target="_blank" rel="ugc">Meow Meow</a></sup></p>
</div>
      
  </article></p>
<h2 id="saudi-arabia">Saudi Arabia</h2>
<p>Saudi Arabia jumped into open source hardware earlier this year with the <a href="https://certification.oshwa.org/sa000001.html" target="_blank">M1 Rover</a>. The robot is an unmanned vehicle that you can build (and build upon). It is compatible with a number of different packages designed for specific purposes, so you can customize it for a wide range of applications.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="M1-Rover "><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/m1-rover.png" width="650" height="488" alt="M1-Rover " title="M1-Rover " /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><a href="https://www.hackster.io/AhmedAzouz/m1-rover-362c05" target="_blank" rel="ugc"><sup>M1-Rover</sup></a></p>
</div>
      
  </article></p>
<h2 id="sri-lanka">Sri Lanka</h2>
<p>This project from Sri Lanka is part of a larger effort to improve traffic flow in urban areas. The team behind the <a href="https://certification.oshwa.org/lk000001.html" target="_blank">Traffic Wave Disruptor</a> read research about how many traffic jams are caused by drivers slamming on their brakes when they drive too close to the car in front of them, producing a ripple of rapid breaking on the road behind them. This stop/start effect can be avoided if cars maintain a consistent, optimal distance from one another. If you reduce the stop/start pattern, you also reduce the number of traffic jams.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Traffic Wave Disruptor"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/traffic-wave-disruptor.png" width="650" height="433" alt="Traffic Wave Disruptor" title="Traffic Wave Disruptor" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><a href="https://github.com/Aightm8/Traffic-wave-disruptor" target="_blank" rel="ugc"><sup>Traffic Wave Disruptor</sup></a></p>
</div>
      
  </article></p>
<p>But how can drivers know if they are keeping an optimal distance? The prototype Traffic Wave Disruptor aims to give drivers feedback when they fail to keep optimal spacing. Wider adoption could help increase traffic flow without building new highways nor reducing the number of cars using them. </p>
<hr /><p>You may have noticed that all the hardware featured here is based on electronics. In next month's open source hardware column, we will take a look at open source hardware for the outdoors, away from batteries and plugs. Until then, <a href="https://certification.oshwa.org/" target="_blank">certify</a> your open source hardware project (especially if your country is not yet on the registry). It might be featured in a future column.</p>
