<p>When a team of designers works together on a project, one of the most important goals is consistency. Whether you need a consistent look because of a corporate identity or just for visual cohesion, the look and layout of pages and screens must be reasonably similar within any given project. It's hard enough to do this as a solo artist, and it gets more complex with added contributors. It becomes a monumental task when several mock-ups from a design team must be approved by a client, signed off by an accessibility expert, and then translated by a team of programmers. It's difficult but manageable with the right tool, and the open source tool for the job is <a href="https://opensource.com/article/21/9/open-source-design" target="_self">Penpot</a>.</p>
<p>Penpot is an online design workspace where designers can create or import graphical elements, create mock-ups, and share those mock-ups with clients and collaborators. It is open source and relies on open formats like SVG, which means your contributors have lots of choices in what tools they use to contribute assets. To try Penpot, go to <a href="http://penpot.app" target="_blank">Penpot.app</a> and create an account.</p>
<h2>Getting started with Penpot</h2>
<p>After you log in, you're presented with the <strong>Projects</strong> page. There are some demo files here, designed to provide you with an overview of the Penpot interface. A project in Penpot can have multiple pages. Select a page to view in the <strong>Pages</strong> panel.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Penpot pages"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/penpot-panel-pages.jpg" width="306" height="331" alt="Penpot pages" title="Penpot pages" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Seth Kenlon, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</p>
</div>
      
  </article></p>
<p>A page in Penpot isn't like a page in the real world. A Penpot page is an infinite canvas. Middle-click and drag to move the canvas.</p>
<p>To zoom in and out, press <strong>Ctrl</strong> on your keyboard and then use your mouse wheel or do the equivalent action on the trackpad or tablet you use.</p>
<p>Zoom is an especially useful convention because Penpot is often used to design content for screens that are intended to scroll, not paginate.</p>
<h2>Create an artboard</h2>
<p>An infinite canvas means you can use the same workspace to display several different options for the same page type. You can design a light and dark theme, a mobile and desktop version, and then several different iterations of each. Because everything's on the canvas, you need a way to define an area for your design, and in the terminology of design apps, that's an <em>artboard</em>. An artboard is a container (think of it as a <strong>div</strong>, in CSS terms) for a mock-up. It's the part of the canvas you'll show someone when they ask to see your design ideas.</p>
<p>To create an artboard, click the Artboard icon in the left toolbar, or just press <strong>A</strong> on your keyboard.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Artboard button"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/penpot-button-artboard.jpg" width="292" height="288" alt="Artboard button" title="Artboard button" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Seth Kenlon, CC BY-SA 4.0)</sup></p>
</div>
      
  </article></p>
<p>Click and drag on the canvas to create an artboard. If you're designing for a specific screen or page size, you can represent it with your artboard. If you need very specific sizes, you can adjust your artboard's dimensions in the properties panel on the right side of the Penpot interface.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Penpot properties panel"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/penpot-panel-artboard.jpg" width="351" height="409" alt="Penpot properties panel" title="Penpot properties panel" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Seth Kenlon, CC BY-SA 4.0)</sup></p>
</div>
      
  </article></p>
<p>You can perform basic maths in the text fields, too. For example, if you know your design size is 1920 by 1080, but you need to allow for scrolling down the equivalent of five screens, enter <code>1080*5</code> as the height, and Penpot converts it to 5400 for you.</p>
<h2>Drawing in Penpot</h2>
<p>Penpot primarily uses SVG (Scalable Vector Graphics) for its design elements. SVG is a structured and open format that's easy for web developers to translate into code. Thanks to this feature, drawing in Penpot is as easy as <a href="https://opensource.com/article/21/12/linux-draw-inkscape" target="_self">drawing in Inkscape</a> and, just as importantly, it's easy to align, duplicate, re-use, and export for use in any other application.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Designing a mobile app in Penpot"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/penpot-design.jpg" width="1067" height="676" alt="Designing a mobile app in Penpot" title="Designing a mobile app in Penpot" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Seth Kenlon, CC BY-SA 4.0)</sup></p>
</div>
      
  </article></p>
<p>In Penpot, you have these drawing tools:</p>
<ul><li>Rectangle (<strong>R</strong>): draw rectangles, and adjust for rounded corners and fills in the properties panel</li>
<li>Ellipse (<strong>E</strong>): draw ellipses and circles</li>
<li>Text (<strong>T</strong>): create text elements</li>
<li>Image (<strong>Shift+K</strong>): import a bitmap image</li>
<li>Curve (<strong>Shift+C</strong>): a pen with Bezier curves</li>
<li>Path (<strong>P</strong>): a pen tool</li>
</ul><p>You can adjust all design elements in the properties panel on the right of the interface. Most everything that can be adjusted also maps to a CSS property, so in a way, you're designing for the web in a graphical interface, meaning it's that much easier for the development team to translate design to implementation accurately.</p>
<h2>Assets</h2>
<p>One of the easiest ways to ensure consistency in design is to re-use assets. The fewer times a designer has to draw essentially the same button, the fewer opportunities there are for a rounded corner to be forgotten or the size to be wrong. Build a library of design elements and make those the vocabulary of your designers.</p>
<p>Creating a component library for your project is easy. Once you've designed an element, right-click on it, and select <strong>Create Component</strong>. The item is added to your project's Assets library.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Penpot assets"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/penpot-assets.jpg" width="243" height="422" alt="Penpot assets" title="Penpot assets" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Seth Kenlon, CC BY-SA 4.0)</sup></p>
</div>
      
  </article></p>
<p>You can access your asset library by pressing <strong>Alt+I</strong> on your keyboard or clicking the Assets icon in the lower-left corner of the interface.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Asset icon"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/penpot-button-assets.jpg" width="408" height="234" alt="Asset icon" title="Asset icon" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Seth Kenlon, CC BY-SA 4.0)</sup></p>
</div>
      
  </article></p>
<h2>Comments and feedback</h2>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Great Content</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=7016000000127cYAAQ">Free online course: RHEL technical overview</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=7016000000127cYAAQ">Learn Advanced Linux Commands</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Download Cheat Sheets</a></li>
<li><a href="https://opensource.com/alternatives?intcmp=7016000000127cYAAQ">Find an Open Source Alternative</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=7016000000127cYAAQ">Read Top Linux Content</a></li>
<li><a href="https://opensource.com/resources?intcmp=7016000000127cYAAQ">Check out open source resources</a></li>
</ul></div>
</div>
</div>
</div>
<p>A healthy feedback loop is vital for collaboration, and one way to provide feedback asynchronously is the Comment (<strong>C</strong>) tool. Using the Comment tool, collaborators on a project can safely post questions and observations anywhere in the workspace. Comments are threaded, so a conversation happens within one comment block, and a comment thread can be deleted when an issue has been resolved.</p>
<h2>Prototype</h2>
<p>Designing for interactive media is different from designing a magazine ad or a billboard. Things change based on viewer choices and application status. To capture the different states your design may go through as a user interacts with it, you can build a <em>prototype</em>, with no coding required.</p>
<p>A prototype lets you set any design element as a trigger to detect a click or mouse event and to take an action such as navigating to another artboard or opening an overlay. Your app won't actually work, but it'll look and act like it works.</p>
<h2>Using open source for design</h2>
<p>Penpot is an amazing and fun tool, not just as a design and prototyping platform but also as an online SVG illustration app. Use Penpot to mock-up an application so you can understand user interaction before writing code, use it to encourage design contribution, and enable collaboration between teams.</p>
<p>If you want to get started, but aren't sure where to begin, try opening <a href="https://github.com/penpot/penpot-files/blob/main/samples/webinar_12012021.penpot" target="_blank">this great demo project</a> in Penpot to see how components are stored and connected.</p>
