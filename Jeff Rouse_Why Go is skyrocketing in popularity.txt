<p>The <a href="https://golang.org/" target="_blank">Go programming language,</a> sometimes referred to as Google's golang, is making strong gains in popularity. While languages such as Java and C continue to dominate programming, new models have emerged that are better suited to modern computing, particularly in the cloud. Go's increasing use is due, in part, to the fact that it is a lightweight, open source language suited for today's microservices architectures. Container darling Docker and Google's container orchestration product <a href="https://opensource.com/sitewide-search?search_api_views_fulltext=Kubernetes">Kubernetes</a> are built using Go. Go is also gaining ground in data science, with strengths that data scientists are looking for in overall performance and the ability to go from "the analyst's laptop to full production."</p>
<p>As an engineered language (rather than something that evolved over time), Go benefits developers in multiple ways, including garbage collection, native concurrency, and many other native capabilities that reduce the need for developers to write code to handle memory leaks or networked apps. Go also provides many other features that fit well with microservices architectures and data science.</p>
<p>Because of this, Go is being adopted by interesting companies and projects. Recently an API for <a href="https://www.tensorflow.org/" target="_blank">Tensorflow</a> has been added, and products like <a href="http://www.pachyderm.io/" target="_blank">Pachyderm</a> (next-gen data processing, versioning, and storage) are being built using Go. Heroku's <a href="https://github.com/heroku/force" target="_blank">Force.com</a> and parts of <a href="https://www.cloudfoundry.org/" target="_blank">Cloud Foundry</a> were also written in Go. More names are being added regularly.</p>
<h2>Rising popularity and usage</h2>
<p>In the September 2017 TIOBE Index for Go, you can clearly see the incredible jump in Go popularity since 2016, including being named TIOBE's Programming Language Hall of Fame winner for 2016, as the programming language with the highest rise in ratings in a year. It currently stands at #17 on the monthly list, up from #19 a year ago, and up from #65 two years ago.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="TIOBE Index for Go"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/tiobe_index_for_go.png" width="470" height="413" alt="TIOBE Index for Go" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>The Stack Overflow Survey 2017 also shows signs of Go's rise in popularity. Stack Overflow's comprehensive survey of 64,000 developers tries to get at developers' preferences by asking about the "Most Loved, Dreaded, and Wanted Languages." This list is dominated by newer languages like Mozilla's Rust, Smalltalk, Typescript, Apple's Swift, and Google's Go. But for the third year in a row Rust, Swift, and Go made the top five "most loved" programming languages.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Most Loved, Dreaded, and Wanted Languages"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/stackoverflow_most_loved.png" width="470" height="327" alt="Most Loved, Dreaded, and Wanted Languages" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<h2>Go advantages</h2>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Programming and development</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ&amp;src=programming_resource_menu4">Red Hat Developers Blog</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Programming cheat sheets</a></li>
<li><a href="https://www.redhat.com/en/services/training/learning-subscription?intcmp=7016000000127cYAAQ">Try for free: Red Hat Learning Subscription</a></li>
<li><a href="https://opensource.com/downloads/bash-programming-guide?intcmp=7016000000127cYAAQ">eBook: An introduction to programming with Bash</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/bash-shell-cheat-sheet?intcmp=7016000000127cYAAQ">Bash Shell Scripting Cheat Sheet</a></li>
<li><a href="https://developers.redhat.com/e-books/modernizing-enterprise-java?intcmp=7016000000127cYAAQ">eBook: Modernizing Enterprise Java</a></li>
</ul></div>
</div>
</div>
</div>
<p>Some programming languages were hacked together over time, whereas others were created academically. Still others were designed in a different age of computing with different problems, hardware, and needs. Go is an explicitly engineered language intended to solve problems with existing languages and tools while natively taking advantage of modern hardware architectures. It has been designed not only with teams of developers in mind, but also long-term maintainability.</p>
<p>At its core, Go is pragmatic. In the real world of IT, complex, large-scale software is written by large teams of developers. These developers typically have varying skill levels, from juniors up to seniors. Go is easy to become functional with and appropriate for junior developers to work on.</p>
<p>Also, having a language that encourages readability and comprehension is extremely useful. The mixture of duck typing (via interfaces) and convenience features such as "<strong>:=</strong>" for short variable declarations give Go the feel of a dynamically typed language while retaining the positives of a strongly typed one.</p>
<p>Go's native garbage collection removes the need for developers to do their own memory management, which helps negate two common issues:</p>
<ul><li>First, many programmers have come to expect that memory management will be done for them.</li>
<li>Second, memory management requires different routines for different processing cores. Manually attempting to account for each configuration can significantly increase the risk of introducing memory leaks.</li>
</ul><p>Go's native concurrency is a boon for network applications that live and die on concurrency. From APIs to web servers to web app frameworks, Go projects tend to focus on networking, distributed functions, and/or services for which Go's goroutines and channels are well suited.</p>
<h2>Suited for data science</h2>
<p>Extracting business value from large datasets is quickly becoming a competitive advantage for companies and a very active area in programming, encompassing specialties like artificial intelligence, machine learning, and more. Go has multiple strengths in these areas of data science, which is increasing its use and popularity.</p>
<ul><li>Superior error handling and easier debugging are helping it gain popularity over Python and R, the two most commonly used data science languages.</li>
<li>Data scientists are typically not programmers. Go helps with both prototyping and production, so it ends up being a more robust language for putting data science solutions into production.</li>
<li>Performance is fantastic, which is critical given the explosion in big data and the rise of GPU databases. Go does not have to call in C/C++ based optimizations for performance gains, but gives you the ability to do so.</li>
</ul><h2>Seeds of Go's expansion</h2>
<p>Software delivery and deployment have changed dramatically. Microservices architectures have become key to unlocking application agility. Modern apps are designed to be cloud-native and to take advantage of loosely coupled cloud services offered by cloud platforms.</p>
<p>Go is an explicitly engineered programming language, specifically designed with these new requirements in mind. Written expressly for the cloud, Go has been growing in popularity because of its mastery of concurrent operations and the beauty of its construction.</p>
<p>Not only is Google supporting Go, but other companies are aiding in market expansion, as well. For example, Go code is supported and expanded with enterprise-level distributions such as <a href="https://www.activestate.com/activego" target="_blank">ActiveState's ActiveGo</a>. As an open source movement, the <a href="https://golang.org/" target="_blank">golang.org</a> site and annual <a href="https://www.gophercon.com/" target="_blank">GopherCon</a> conferences form the basis of a strong, modern open source community that allows new ideas and new energy to be brought into Go's development process.</p>
