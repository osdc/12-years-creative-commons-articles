<p>When writing about open source software, I prefer to show a few screenshots to help demonstrate what I'm talking about. As the old saying goes, a picture is worth a thousand words. If you can show a thing, that's often better than merely trying to describe it.</p>
<p>There are a few ways you can take screenshots in Linux. Here are three methods I use to capture screenshots on Linux:</p>
<h2 id="gnome">1. GNOME</h2>
<p>GNOME has a great built-in screenshot tool. Just hit the <strong>PrtScr</strong> key on your keyboard, and GNOME displays a screenshot dialog:</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-07/screenshot-gnome.png" width="1300" height="975" alt="Image of GNOME screenshot tool" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Jim Hall, CC BY-SA 40)</p>
</div>
      
  </article><p>The default action is to grab a screenshot of a region. This is an incredibly useful way to crop a screenshot as you make it. Just move the highlight box to where you need it, and use the "grab" corners to change the size. Or select one of the other icons to take a screenshot of the entire screen, or just a single window on your system. Click the circle icon to take the screenshot, similar to the "take photo" button on mobile phones. The GNOME screenshot tool saves screenshots in a Screenshots folder inside your Pictures folder.</p>
<h2 id="gimp">2. GIMP</h2>
<p>If you need more options for screenshots, you can grab a screenshot using GIMP, the popular image editor. To take a screenshot, go to <strong>File </strong>and choose the <strong>Create </strong>submenu, and then choose <strong>Screenshot</strong>.</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-07/gimp-screenshot.png" width="457" height="575" alt="Image of the GIMP screenshot menu" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Jim Hall, CC BY-SA 40)</p>
</div>
      
  </article><p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">More Linux resources</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux commands cheat sheet">Linux commands cheat sheet</a></div>
              <div class="field__item"><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Advanced Linux commands cheat sheet">Advanced Linux commands cheat sheet</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Free online course: RHEL technical overview">Free online course: RHEL technical overview</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux networking cheat sheet">Linux networking cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="SELinux cheat sheet">SELinux cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux common commands cheat sheet">Linux common commands cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="What are Linux containers?">What are Linux containers?</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Register for your free Red Hat account">Register for your free Red Hat account</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Our latest Linux articles">Our latest Linux articles</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<p>The dialog allows you to take a screenshot of a single window, the entire screen, or just a region. I like that this tool lets you set a delay: how long until you select the window, and how long after that to take the screenshot. I use this feature a lot when I want to grab a screenshot of a menu action, so I have enough time to go to the window and open the menu.</p>
<p>GIMP opens the screenshot as a new image, which you can edit and save to your preferred location.</p>
<h2 id="firefox">3. Firefox</h2>
<p>If you need to take a screenshot of a website, try Firefox's built-in screenshot utility. Right-click anywhere in the web page body, and select <strong>Take Screenshot</strong> from the menu:</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-07/firefox-screenshot_cropped_0.png" width="441" height="331" alt="Image of screenshot utility" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Jim Hall, CC BY-SA 40)</p>
<p>​</p>
</div>
      
  </article><p>Firefox switches to a modal display and prompts you to click or drag on the page to select a region, or use one of the icons to save a copy of the full page or just what's visible in the browser:</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-07/firefox-screenshot_1.png" width="1340" height="878" alt="Image of Firefox modal display" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Jim Hall, CC BY-SA 40)</p>
</div>
      
  </article><p>As you move your mouse around the screen, you may notice that Firefox highlights certain areas. These are block elements on the page, such as a  <code>&lt;div&gt; </code>or another block element. Click on the element to take a screenshot of it. Firefox saves the screenshot to your <strong>Downloads</strong> folder, or wherever you have set as your "download" location.</p>
<p>If you're trying to document a process, a screenshot can save you a lot of time. Try using one of these methods to take a screenshot on Linux.</p>
