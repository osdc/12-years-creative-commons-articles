<p>Last year, I brought you 19 days of new (to you) productivity tools for 2019. This year, I'm taking a different approach: building an environment that will allow you to be more productive in the new year, using tools you may or may not already be using.</p>
<h2 id="access-your-rss-feeds-and-podcasts-with-newsboat">Access your RSS feeds and podcasts with Newsboat</h2>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on productivity</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/downloads/collaboration-tools-ebook">5 open source collaboration tools</a></li>
<li><a href="https://opensource.com/downloads/organization-tools">6 open source tools for staying organized</a></li>
<li><a href="https://opensource.com/downloads/desktop-tools">7 open source desktop tools</a></li>
<li><a href="https://opensource.com/tags/tools">The latest on open source tools</a></li>
</ul></div>
</div>
</div>
</div>
<p>RSS news feeds are an exceptionally handy way to keep up to date on various websites. In addition to Opensource.com, I follow the annual <a href="https://sysadvent.blogspot.com/" target="_blank">SysAdvent</a> sysadmin tools feed, some of my favorite authors, and several webcomics. RSS readers allow me to "batch up" my reading, so I'm not spending every day on a bunch of different websites.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Newsboat"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/productivity_12-1.png" width="675" height="363" alt="Newsboat" title="Newsboat" /></div>
      
  </article></p>
<p><a href="https://newsboat.org" target="_blank">Newsboat</a> is a terminal-based RSS feed reader that looks and feels a lot like the email program <a href="http://mutt.org/" target="_blank">Mutt</a>. It makes news reading easy and has a lot of nice features.</p>
<p>Installing Newsboat is pretty easy since it is included with most distributions (and Homebrew on MacOS). Once it is installed, adding the first feed is as easy as adding the URL to the <strong>~/.newsboat/urls</strong> file. If you are migrating from another feed reader and have an OPML file export of your feeds, you can import that file with:</p>
<pre><code class="language-text">newsboat -i &lt;/path/to/my/feeds.opml&gt;</code></pre><p>After you've added your feeds, the Newsboat interface is <em>very</em> familiar, especially if you've used Mutt. You can scroll up and down with the arrow keys, check for new items in a feed with <strong>r</strong>, check for new items in all feeds with <strong>R</strong>, press <strong>Enter</strong> to open a feed and select an article to read.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Newsboat article list"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/productivity_12-2.png" width="675" height="363" alt="Newsboat article list" title="Newsboat article list" /></div>
      
  </article></p>
<p>You are not limited to just the local URL list, though. Newsboat is also a client for news reading services like <a href="https://tt-rss.org/" target="_blank">Tiny Tiny RSS</a>, ownCloud and Nextcloud News, and a few Google Reader successors. Details on that and a whole host of other configuration options are covered in <a href="https://newsboat.org/releases/2.18/docs/newsboat.html" target="_blank">Newsboat's documentation</a>.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Reading an article in Newsboat"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/productivity_12-3.png" width="675" height="363" alt="Reading an article in Newsboat" title="Reading an article in Newsboat" /></div>
      
  </article></p>
<h3 id="podcasts">Podcasts</h3>
<p>Newsboat also provides <a href="https://newsboat.org/releases/2.18/docs/newsboat.html#_podcast_support" target="_blank">podcast support</a> through Podboat, an included application that facilitates downloading and queuing podcast episodes. While viewing a podcast feed in Newsboat, press <strong>e</strong> to add the episode to your download queue. All the information will be stored in a queue file in the <strong>~/.newsboat</strong> directory. Podboat reads this queue and downloads the episode(s) to your local drive. You can do this from the Podboat user interface (which looks and acts like Newsboat), or you can tell Podboat to download them all with <strong>podboat -a</strong>. As a podcaster and podcast listener, I think this is <em>really</em> handy.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Podboat"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/productivity_12-4.png" width="675" height="363" alt="Podboat" title="Podboat" /></div>
      
  </article></p>
<p>Overall, Newsboat has some really great features and is a nice, lightweight alternative to web-based or desktop apps.</p>
