<p>The Java Development Kit (JDK) provides binaries, tools, and compilers for the development of Java applications. One helpful tool included is jconsole.</p>
<p>To demonstrate, I will use the WildFly J2EE application server, which is part of the JBOSS open source application server project. First, I start up a standalone instance.</p>
<pre><code class="language-bash">~/wildfly/24.0.1/bin$ ./standalone.sh
=========================================================================

  JBoss Bootstrap Environment

  JBOSS_HOME: /home/alan/wildfly/24.0.1

  JAVA: /usr/lib/jvm/java-11-openjdk-11.0.11.0.9-5.fc34.x86_64/bin/java
</code></pre><p>Now, in another terminal, I type <code>jconsole</code>.</p>
<pre>$ jconsole</pre><p>Upon launching, jconsole lists local instances. Select Local Process, then select the name of the process and click Connect. That is all it takes to connect and begin using jconsole with a running Java Virtual Machine (JVM).</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="new connection"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/jconsole_new_connection_local.png" width="600" height="433" alt="jconsole new connection screen with local processes" title="new connection" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup><font face="Arial, sans-serif"><font size="2">(Alan Formy-Duvall, <font color="#000000"><a href="https://creativecommons.org/licenses/by-sa/4.0/deed.ast" rel="ugc">CC BY-SA 4.0</a>)</font></font></font></sup></p>
</div>
      
  </article></p>
<h2>Overview</h2>
<p>The Java Monitoring and Management Console shows the process identifier (PID) at the top of the dashboard. The Overview tab has four graphs to show the vitals for Heap Memory Usage, Threads, Classes, and CPU Usage.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="tab overview"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/jconsole_tab_overview.png" width="600" height="501" alt="jconsole dashboard showing heap memory usage, threads, classes, and CPU usage" title="tab overview" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup><font face="Arial, sans-serif"><font size="2"><font color="#000000"><font face="Arial, sans-serif"><font size="2">(Alan Formy-Duvall, <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.ast" rel="ugc">CC BY-SA 4.0</a>)</font></font></font></font></font></sup></p>
</div>
      
  </article></p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on Java</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/topics/enterprise-java/?intcmp=7013a000002Cxq6AAC">What is enterprise Java programming?</a></li>
<li><a href="https://developers.redhat.com/products/openjdk/overview?intcmp=7013a000002Cxq6AAC">Red Hat build of OpenJDK</a></li>
<li><a href="https://opensource.com/downloads/java-cheat-sheet?intcmp=7013a000002Cxq6AAC">Java cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/do092-developing-cloud-native-applications-microservices-architectures?intcmp=7013a000002Cxq6AAC">Free online course: Developing cloud-native applications with microservices architectures</a></li>
<li><a href="https://opensource.com/tags/java?intcmp=7013a000002Cxq6AAC">Fresh Java articles</a></li>
</ul></div>
</div>
</div>
</div>

<p>The tabs along the top provide more detailed views of each area.</p>
<h2>Memory</h2>
<p>The Memory tab displays graphs of various aspects of the memory being used by the JVM. The amount of the server system memory allocated to the JVM is called the heap. This screen also provides details about usage by the internal components of the heap, such as the Eden Space, Old Gen, and the Survivor Space. You can manually request a garbage collection action as well.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="memory tab"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/jconsole_tab_memory.png" width="600" height="511" alt="jconsole memory tab" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><font face="Arial, sans-serif"><font size="2"><font color="#000000"><sup><font face="Arial, sans-serif"><font size="2">(Alan Formy-Duvall, <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.ast" rel="ugc">CC BY-SA 4.0</a>)</font></font></sup></font></font></font></p>
</div>
      
  </article></p>
<h2>Threads</h2>
<p>The Threads tab shows how many threads are running. You can also manually check for deadlocks.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="threads tab"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/jconsole_tab_threads.png" width="600" height="511" alt="jconsole thread dashboard showing number of threads over time and a scrolling list of threads" title="threads tab" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><font face="Arial, sans-serif"><font size="2"><font color="#000000"><sup><font face="Arial, sans-serif"><font size="2">(Alan Formy-Duvall, <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.ast" rel="ugc">CC BY-SA 4.0</a>)</font></font></sup></font></font></font></p>
</div>
      
  </article></p>
<h2>Classes</h2>
<p>The classes tab tells you how many classes are loaded and how many have been unloaded.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="classes tab"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/jconsole_tab_classes.png" width="600" height="501" alt="jconsole classes tab showing number of loaded classes over time" title="classes tab" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><font face="Arial, sans-serif"><font size="2"><font color="#000000"><sup><font face="Arial, sans-serif"><font size="2">(Alan Formy-Duvall, <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.ast" rel="ugc">CC BY-SA 4.0</a>)</font></font></sup></font></font></font></p>
</div>
      
  </article></p>
<h2>VM Summary</h2>
<p>The VM Summary tab provides many details about the application and the host system. You can learn which operating system and architecture you are on, the total amount of system memory, the number of CPUs, and even swap space.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="VMsummary tab "><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/jconsole_tab_vm-summary.png" width="600" height="511" alt="jconsole VMsummary tab " title="VMsummary tab " /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><font face="Arial, sans-serif"><font size="2"><font color="#000000"><sup><font face="Arial, sans-serif"><font size="2">(Alan Formy-Duvall, <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.ast" rel="ugc">CC BY-SA 4.0</a>)</font></font></sup></font></font></font></p>
</div>
      
  </article></p>
<p>Further details about the JVM shown in the summary include current and maximum heap size and information about the garbage collectors in use. The bottom pane lists all of the arguments passed to the JVM.</p>
<h2>MBeans</h2>
<p>The last tab, MBeans, lets you drill down through all of the MBeans to view attributes and values for each.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="MBeans tab"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/jconsole_tab_mbeans.png" width="600" height="511" alt="MBeans tab" title="MBeans tab" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><font face="Arial, sans-serif"><font size="2"><font color="#000000"><sup><font face="Arial, sans-serif"><font size="2">(Alan Formy-Duvall, <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.ast" rel="ugc">CC BY-SA 4.0</a>)</font></font></sup></font></font></font></p>
</div>
      
  </article></p>
<h2>Conclusion</h2>
<p>Java has been around a long time, and it continues to power millions of systems worldwide. Plenty of development environments and monitoring systems are available, but having a tool like jconsole included in the base kit can be highly valuable.</p>
