<p>When you're patching Linux machines these days, I could forgive you for asking, "How hard can it be?" Sure, a <strong>yum update -y</strong> will sort it for you in a flash.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Animation of updating Linux"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/quick_update.gif" width="600" height="337" alt="Animation of updating Linux" title="Animation of updating Linux" /></div>
      
  </article></p>
<p>But for those of us working with more than a handful of machines, it's not that simple. Sometimes an update can create unintended consequences across many machines, and you're left wondering how to put things back the way they were. Or you might think, "Should I have applied the critical patch on its own and saved myself a lot of pain?"</p>
<p>Faced with these sorts of challenges in the past led me to build a way to cherry-pick the updates needed and automate their application.</p>
<h2 id="a-flexible-idea">A flexible idea</h2>
<p>Here's an overview of the process:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Overview of the Ansible patch process"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/patch_process.png" width="455" height="455" alt="Overview of the Ansible patch process" title="Overview of the Ansible patch process" /></div>
      
  </article></p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on Ansible</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/downloads/ansible-quickstart?intcmp=701f2000000h4RcAAI">A quickstart guide to Ansible</a></li>
<li><a href="https://opensource.com/downloads/ansible-cheat-sheet?intcmp=701f2000000h4RcAAI">Ansible cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/do007-ansible-essentials-simplicity-automation-technical-overview?intcmp=701f2000000h4RcAAI">Free online course: Ansible essentials</a></li>
<li><a href="https://docs.ansible.com/ansible/latest/intro_installation.html?intcmp=701f2000000h4RcAAI">Download and install Ansible</a></li>
<li><a href="https://go.redhat.com/automated-enterprise-ebook-20180920?intcmp=701f2000000h4RcAAI">eBook: The automated enterprise</a></li>
<li><a href="https://www.ansible.com/resources/ebooks/ansible-for-devops?intcmp=701f2000000h4RcAAI">eBook: Ansible for DevOps</a></li>
<li><a href="https://www.ansible.com/resources/ebooks?intcmp=701f2000000h4RcAAI">Free Ansible eBooks</a></li>
<li><a href="https://opensource.com/tags/ansible?intcmp=701f2000000h4RcAAI">Latest Ansible articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>This system doesn't permit machines to have direct access to vendor patches. Instead, they're selectively subscribed to repositories. Repositories contain only the patches that are required––although I'd encourage you to give this careful consideration so you don't end up with a proliferation (another management overhead you'll not thank yourself for creating).</p>
<p>Now patching a machine comes down to 1) The repositories it's subscribed to and 2) Getting the "thumbs up" to patch. By using variables to control both subscription and permission to patch, we don't need to tamper with the logic (the plays); we only need to alter the data.</p>
<p>Here is an <a href="https://github.com/phips/ansible-patching/blob/master/roles/patching/tasks/main.yml" target="_blank">example Ansible role</a> that fulfills both requirements. It manages repository subscriptions and has a simple variable that controls running the patch command.</p>
<pre><code class="language-yaml">---
# tasks file for patching

- name: Include OS version specific differences
  include_vars: "{{ ansible_distribution }}-{{ ansible_distribution_major_version }}.yml"

- name: Ensure Yum repositories are configured
  template:
    src: template.repo.j2
    dest: "/etc/yum.repos.d/{{ item.label }}.repo"
    owner: root
    group: root
    mode: 0644
  when: patching_repos is defined
  loop: "{{ patching_repos }}"
  notify: patching-clean-metadata

- meta: flush_handlers

- name: Ensure OS shipped yum repo configs are absent
  file:
    path: "/etc/yum.repos.d/{{ patching_default_repo_def }}"
    state: absent

# add flexibility of repos here
- name: Patch this host
  shell: 'yum update -y'
  args:
    warn: false
  when: patchme|bool
  register: result
  changed_when: "'No packages marked for update' not in result.stdout"</code></pre><h2 id="scenarios">Scenarios</h2>
<p>In our fictitious, large, globally dispersed environment (of four hosts), we have:</p>
<ul><li>Two web servers</li>
<li>Two database servers</li>
<li>An application comprising one of each server type</li>
</ul><p>OK, so this number of machines isn't "enterprise-scale," but remove the counts and imagine the environment as multiple, tiered, geographically dispersed applications. We want to patch elements of the stack across server types, application stacks, geographies, or the whole estate.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Example patch groups"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/patch_groups.png" width="455" height="455" alt="Example patch groups" title="Example patch groups" /></div>
      
  </article></p>
<p>Using only changes to variables, can we achieve that flexibility? Sort of. Ansible's <a href="https://docs.ansible.com/ansible/2.3/playbooks_variables.html#variable-precedence-where-should-i-put-a-variable" target="_blank">default behavior</a> for hashes is to overwrite. In our example, the <strong>patching_repos</strong> variable for the <strong>db1</strong> and <strong>web1</strong> hosts are overwritten because of their later occurrence in our inventory. Hmm, a bit of a pickle. There are two ways to manage this:</p>
<ol><li>Multiple inventory files</li>
<li><a href="https://docs.ansible.com/ansible/2.3/intro_configuration.html#sts=hash_behaviour" target="_blank">Change the variable behavior</a></li>
</ol><p>I chose number one because it maintains clarity. Once you start merging variables, it's hard to find where a hash appears and how it's put together. Using the default behavior maintains clarity, and it's the method I'd encourage you to stick with for your own sanity.</p>
<h2 id="get-on-with-it-then">Get on with it then</h2>
<p>Let's run the play, focusing only on the database servers.</p>
<p><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/HVnoLzno8gc" width="560"></iframe></p>
<p>Did you notice the final step—<strong>Patch this host</strong>—says <strong>skipping</strong>? That's because we didn't set <a href="https://github.com/phips/ansible-patching/blob/master/roles/patching/defaults/main.yml#L4" target="_blank">the controlling variable</a> to do the patching. What we have done is set up the repository subscriptions to be ready.</p>
<p>So let's run the play again, limiting it to the web servers and tell it to do the patching. I ran this example with verbose output so you can see the yum updates happening.</p>
<p><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/rczTrcQg06o" width="560"></iframe></p>
<p>Patching an application stack requires another inventory file, as mentioned above. Let's rerun the play.</p>
<p><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/KdlUg9gGHrM" width="560"></iframe></p>
<p>Patching hosts in the European geography is the same scenario as the application stack, so another inventory file is required.</p>
<p><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/Y8Pg431RyZU" width="560"></iframe></p>
<p>Now that all the repository subscriptions are configured, let's just patch the whole estate. Note the <strong>app1</strong> and <strong>emea</strong> groups don't need the inventory here––they were only being used to separate the repository definition and setup. Now, <strong>yum update -y</strong> patches everything. If you didn't want to capture those repositories, they could be configured as <strong>enabled=0</strong>.</p>
<p><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/2tqvv9HF5sY" width="560"></iframe></p>
<h2 id="conclusion">Conclusion</h2>
<p>The flexibility comes from how we group our hosts. Because of default hash behavior, we need to think about overlaps—the easiest way, to my mind at least, is with separate inventories.</p>
<p>With regard to repository setup, I'm sure you've already said to yourself, "Ah, but the cherry-picking isn't that simple!" There is additional overhead in this model to download patches, test that they work together, and bundle them with dependencies in a repository. With complementary tools, you could automate the process, and in a large-scale environment, you'd have to.</p>
<p>Part of me is drawn to just applying full patch sets as a simpler and easier way to go; skip the cherry-picking part and apply a full set of patches to a "standard build." I've seen this approach applied to both Unix and Windows estates with enforced quarterly updates.</p>
<p>I’d be interested in hearing your experiences of patching regimes, and the approach proposed here, in the comments below or <a href="https://twitter.com/thismarkp" target="_blank">via Twitter</a>.</p>
