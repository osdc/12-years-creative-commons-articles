<p>Join us later today for an #OpenOrgChat about open organizations and non-profits! As usual, we'll gather around the #OpenOrgChat hashtag at 2 p.m. Eastern (14:00 ET/19:00 UTC).</p>
<p>Follow <a href="https://twitter.com/openorgbook" target="_blank">OpenOrgBook</a> and the chat's <a href="https://www.hashtracking.com/streams/openorgbook/openorgchat" target="_blank">live stream</a> for updates!</p>
<h3>This week's special guests</h3>
<ul><li>Matt Thompson (<a href="https://twitter.com/OpenMatt" target="_blank">@openmatt</a>)</li>
<li>E.G. Nadhan (<a href="https://twitter.com/NadhanEG" target="_blank">@NadhanEG</a>)</li>
<li>David Egts (<a href="https://twitter.com/davidegts" target="_blank">@davidegts</a>)</li>
<li>Karen Sandler (<a href="https://twitter.com/o0karen0o" target="_blank">@o0karen0o</a>)</li>
<li>Leslie Hawthorn (<a href="https://twitter.com/lhawthorn" target="_blank">@lhawthorn</a>)</li>
<li>Danese Cooper (<a href="https://twitter.com/divadanese" target="_blank">@divadanese</a>)</li>
</ul><h3>Some questions we'll explore</h3>
<ul><li>What makes a union between OpenOrg thinking and non-profit work so appealing?</li>
<li>How does an organization's status as non-profit affect organizational culture?</li>
<li>What can open organizations learn from successful non-profits?</li>
<li>What are your most effective strategies for aligning passion and purpose (surely @openmatt has some ideas)?</li>
<li>What challenges do non-profits encounter when they try to open up (I bet @lhawthorn and @divadanese have stories)?</li>
<li>What unique value do non-profits bring to open source communities specifically? (we're looking at you @o0karen0o)</li>
<li>What books would you recommend to someone interested in OpenOrg thinking and non-profit work?</li>
</ul><h3>#OpenOrgChat Twitter chat</h3>
<p><a class="twitter-timeline" data-widget-id="636969512437444608" href="https://twitter.com/search?q=openorgchat">Tweets about openorgchat</a> </p>
<script>
<!--//--><![CDATA[// ><!--
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
//--><!]]>
</script>