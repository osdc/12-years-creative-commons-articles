<p>In 1972, Dennis Ritchie was at Bell Labs, where a few years earlier, he and his fellow team members invented Unix. After creating an enduring OS (still in use today), he needed a good way to program those Unix computers so that they could perform new tasks. It seems strange now, but at the time, there were relatively few programming languages; Fortran, Lisp, <a href="https://opensource.com/article/20/6/algol68">Algol</a>, and B were popular but insufficient for what the Bell Labs researchers wanted to do. Demonstrating a trait that would become known as a primary characteristic of programmers, Dennis Ritchie created his own solution. He called it C, and nearly 50 years later, it's still in widespread use.</p>
<h2 id="why-you-should-learn-c">Why you should learn C</h2>
<p>Today, there are many languages that provide programmers more features than C. The most obvious one is C++, a rather blatantly named language that built upon C to create a nice object-oriented language. There are many others, though, and there's a good reason they exist. Computers are good at consistent repetition, so anything predictable enough to be built into a language means less work for programmers. Why spend two lines recasting an <code>int</code> to a <code>long</code> in C when one line of C++ (<code>long x = long(n);</code>) can do the same?</p>
<p>And yet C is still useful today.</p>
<p>First of all, C is a fairly minimal and straightforward language. There aren't very advanced concepts beyond the basics of programming, largely because C is literally one of the foundations of modern programming languages. For instance, C features arrays, but it doesn't offer a dictionary (unless you write it yourself). When you learn C, you learn the building blocks of programming that can help you recognize the improved and elaborate designs of recent languages.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Programming and development</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ&amp;src=programming_resource_menu4">Red Hat Developers Blog</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Programming cheat sheets</a></li>
<li><a href="https://www.redhat.com/en/services/training/learning-subscription?intcmp=7016000000127cYAAQ">Try for free: Red Hat Learning Subscription</a></li>
<li><a href="https://opensource.com/downloads/bash-programming-guide?intcmp=7016000000127cYAAQ">eBook: An introduction to programming with Bash</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/bash-shell-cheat-sheet?intcmp=7016000000127cYAAQ">Bash Shell Scripting Cheat Sheet</a></li>
<li><a href="https://developers.redhat.com/e-books/modernizing-enterprise-java?intcmp=7016000000127cYAAQ">eBook: Modernizing Enterprise Java</a></li>
</ul></div>
</div>
</div>
</div>
<p>Because C is a minimal language, your applications are likely to get a boost in performance that they wouldn't see with many other languages. It's easy to get caught up in the race to the bottom when you're thinking about how fast your code executes, so it's important to ask whether you <em>need</em> more speed for a specific task. And with C, you have less to obsess over in each line of code, compared to, say, Python or Java. C is fast. There's a good reason the Linux kernel is written in C.</p>
<p>Finally, C is easy to get started with, especially if you're running Linux. You can already run C code because Linux systems include the GNU C library (<code>glibc</code>). To write and build it, all you need to do is install a compiler, open a text editor, and start coding.</p>
<h2 id="getting-started-with-c">Getting started with C</h2>
<p>If you're running Linux, you can install a C compiler using your package manager. On Fedora or RHEL:</p>
<pre><code class="language-bash">$ sudo dnf install gcc</code></pre><p>On Debian and similar:</p>
<pre><code class="language-bash">$ sudo apt install build-essential </code></pre><p>On macOS, you can <a href="https://opensource.com/article/20/6/homebrew-mac">install Homebrew</a> and use it to install <a href="https://gcc.gnu.org/" target="_blank">GCC</a>:</p>
<pre><code class="language-bash">$ brew install gcc</code></pre><p>On Windows, you can install a minimal set of GNU utilities, GCC included, with <a href="https://opensource.com/article/20/8/gnu-windows-mingw">MinGW</a>.</p>
<p>Verify you've installed GCC on Linux or macOS:</p>
<pre><code class="language-bash">$ gcc --version
gcc (GCC) x.y.z
Copyright (C) 20XX Free Software Foundation, Inc.</code></pre><p>On Windows, provide the full path to the EXE file:</p>
<pre><code class="language-bash">PS&gt; C:\MinGW\bin\gcc.exe --version
gcc.exe (MinGW.org GCC Build-2) x.y.z
Copyright (C) 20XX Free Software Foundation, Inc.</code></pre><h2 id="c-syntax">C syntax</h2>
<p>C isn't a scripting language. It's compiled, meaning that it gets processed by a C compiler to produce a binary executable file. This is different from a scripting language like <a href="https://opensource.com/resources/what-bash">Bash</a> or a hybrid language like <a href="https://opensource.com/resources/python">Python</a>.</p>
<p>In C, you create <em>functions</em> to carry out your desired task. A function named <code>main</code> is executed by default.</p>
<p>Here's a simple "hello world" program written in C:</p>
<pre><code class="language-c">#include &lt;stdio.h&gt;

int main() {
  printf("Hello world");
  return 0;
}</code></pre><p>The first line includes a <em>header file</em>, essentially free and very low-level C code that you can reuse in your own programs, called <code>stdio.h</code> (standard input and output). A function called <code>main</code> is created and populated with a rudimentary print statement. Save this text to a file called <code>hello.c</code>, then compile it with GCC:</p>
<pre><code class="language-bash">$ gcc hello.c --output hello</code></pre><p>Try running your C program:</p>
<pre><code class="language-c">$ ./hello
Hello world$</code></pre><h3 id="return-values">Return values</h3>
<p>It's part of the Unix philosophy that a function "returns" something to you after it executes: nothing upon success and something else (an error message, for example) upon failure. These return codes are often represented with numbers (integers, to be precise): 0 represents nothing, and any number higher than 0 represents some non-successful state.</p>
<p>There's a good reason Unix and Linux are designed to expect silence upon success. It's so that you can always plan for success by assuming no errors nor warnings will get in your way when executing a series of commands. Similarly, functions in C expect no errors by design.</p>
<p>You can see this for yourself with one small modification to make your program appear to fail:</p>
<pre><code class="language-c">include &lt;stdio.h&gt;

int main() {
  printf("Hello world");
  return 1;
}</code></pre><p>Compile it:</p>
<pre><code class="language-bash">$ gcc hello.c --output failer</code></pre><p>Now run it using a built-in Linux test for success. The <code>&amp;&amp;</code> operator executes the second half of a command only upon success. For example:</p>
<pre><code class="language-bash">$ echo "success" &amp;&amp; echo "it worked"
success
it worked</code></pre><p>The <code>||</code> test executes the second half of a command upon <em>failure</em>.</p>
<pre><code class="language-bash">$ ls blah || echo "it did not work"
ls: cannot access 'blah': No such file or directory
it did not work</code></pre><p>Now try your program, which does <em>not</em> return 0 upon success; it returns 1 instead:</p>
<pre><code class="language-bash">$ ./failer &amp;&amp; echo "it worked"
String is: hello</code></pre><p>The program executed successfully, yet did not trigger the second command.</p>
<h3 id="variables-and-types">Variables and types</h3>
<p>In some languages, you can create variables without specifying what <em>type</em> of data they contain. Those languages have been designed such that the interpreter runs some tests against a variable in an attempt to discover what kind of data it contains. For instance, Python knows that <code>var=1</code> defines an integer when you create an expression that adds <code>var</code> to something that is obviously an integer. It similarly knows that the word <code>world</code> is a string when you concatenate <code>hello</code> and <code>world</code>.</p>
<p>C doesn't do any of these investigations for you; you must define your variable type. There are several types of variables, including integers (int), characters (char), float, and Boolean.</p>
<p>You may also notice there's no string type. Unlike Python and Java and Lua and many others, C doesn't have a string type and instead sees strings as an array of characters.</p>
<p>Here's some simple code that establishes a <code>char</code> array variable, and then prints it to your screen using <a href="https://opensource.com/article/20/8/printf">printf</a> along with a short message:</p>
<pre><code class="language-c">#include &lt;stdio.h&gt;

int main() {
   char var[6] = "hello";
   printf("Your string is: %s\r\n",var);</code></pre><p>You may notice that this code sample allows six characters for a five-letter word. This is because there's a hidden terminator at the end of the string, which takes up one byte in the array. You can run the code by compiling and executing it:</p>
<pre><code class="language-bash">$ gcc hello.c --output hello
$ ./hello
hello</code></pre><h2 id="functions">Functions</h2>
<p>As with other languages, C functions take optional parameters. You can pass parameters from one function to another by defining the type of data you want a function to accept:</p>
<pre><code class="language-c">#include &lt;stdio.h&gt;

int printmsg(char a[]) {
   printf("String is: %s\r\n",a);
}

int main() {
   char a[6] = "hello";
   printmsg(a);
   return 0;
}</code></pre><p>The way this code sample breaks one function into two isn't very useful, but it demonstrates that <code>main</code> runs by default and how to pass data between functions.</p>
<h2 id="conditionals">Conditionals</h2>
<p>In real-world programming, you usually want your code to make decisions based on data. This is done with <em>conditional</em> statements, and the <code>if</code> statement is one of the most basic of them.</p>
<p>To make this example program more dynamic, you can include the <code>string.h</code> header file, which contains code to examine (as the name implies) strings. Try testing whether the string passed to the <code>printmsg</code> function is greater than 0 by using the <code>strlen</code> function from the <code>string.h</code> file:</p>
<pre><code class="language-c">#include &lt;stdio.h&gt;
#include &lt;string.h&gt;

int printmsg(char a[]) {
  size_t len = strlen(a);
  if ( len &gt; 0) {
    printf("String is: %s\r\n",a);
  }
}

int main() {
   char a[6] = "hello";
   printmsg(a);
   return 1;
}</code></pre><p>As implemented in this example, the sample condition will never be untrue because the string provided is always "hello," the length of which is always greater than 0. The final touch to this humble re-implementation of the <code>echo</code> command is to accept input from the user.</p>
<h2 id="command-arguments">Command arguments</h2>
<p>The <code>stdio.h</code> file contains code that provides two arguments each time a program is launched: a count of how many items are contained in the command (<code>argc</code>) and an array containing each item (<code>argv</code>). For example, suppose you issue this imaginary command:</p>
<pre><code class="language-bash">$ foo -i bar</code></pre><p>The <code>argc</code> is three, and the contents of <code>argv</code> are:</p>
<ul><li><code>argv[0] = foo</code></li>
<li><code>argv[1] = -i</code></li>
<li><code>argv[2] = bar</code></li>
</ul><p>Can you modify the example C program to accept <code>argv[2]</code> as the string instead of defaulting to <code>hello</code>?</p>
<h2 id="imperative-programming">Imperative programming</h2>
<p>C is an imperative programming language. It isn't object-oriented, and it has no class structure. Using C can teach you a lot about how data is processed and how to better manage the data you generate as your code runs. Use C enough, and you'll eventually be able to write libraries that other languages, such as Python and Lua, can use.</p>
<p>To learn more about C, you need to use it. Look in <code>/usr/include/</code> for useful C header files, and see what small tasks you can do to make C useful to you. As you learn, use our <a href="https://opensource.com/downloads/c-programming-cheat-sheet">C cheat sheet</a> by <a href="https://opensource.com/users/jim-hall">Jim Hall</a> of FreeDOS. It's got all the basics on one double-sided sheet, so you can immediately access all the essentials of C syntax while you practice.</p>
<h2 class="rtecenter"><a href="https://opensource.com/downloads/c-programming-cheat-sheet">Download our C cheat sheet today!</a></h2>
