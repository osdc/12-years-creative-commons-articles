<p>Here on the Life channel, we've realized there are a lot of stories about everyday life that are using open source principles—collaboration, participation, sharing, transparency—what we call the open source way. But we can't find them all. And we certainly can't tell them all. That's where you come in.</p>
<!--break--><p>
We've recently posted helpful pages in the Life channel that <a href="https://opensource.com/community/16/8/editorial-writing-topics" title="highlight some of the stories">highlight some of the stories</a> we're looking for help with or working on writing. We've also outlined the broader topics we are interested in telling on the site. Things like health, art, kids, music, fashion, science, and many other topics.</p>
<p><a href="https://opensource.com/contact" title="contact">Contact</a> the life channel moderators if you:</p>
<ul><li>stumble upon a story that should be told</li>
<li>know someone we should interview</li>
<li>want to write <a href="https://opensource.com/community/16/8/editorial-writing-topics" title="writing topics">one the stories we need help with</a></li>
<li>want to write <a href="https://opensource.com/community/16/8/editorial-writing-topics" title="writing topics">on a topic we're interested in</a></li>
</ul><p>
We're excited about finding new stories highlighting how the open source way is being applied to our everyday lives.</p>
