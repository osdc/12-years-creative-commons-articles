<p><a href="https://byobu.org/" target="_blank">Byobu</a> is a text-based window manager and terminal multiplexer. It's similar to <a href="http://www.gnu.org/software/screen/" target="_blank">GNU Screen</a> but more modern and more intuitive. It also works on most Linux, BSD, and Mac distributions.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on Raspberry Pi</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/resources/what-raspberry-pi?src=raspberry_pi_resource_menu1&amp;intcmp=701f2000000h4RcAAI">What is Raspberry Pi?</a></li>
<li><a href="https://opensource.com/downloads/raspberry-pi-guide?src=raspberry_pi_resource_menu2&amp;intcmp=701f2000000h4RcAAI">eBook: Guide to Raspberry Pi</a></li>
<li><a href="https://opensource.com/downloads/getting-started-raspberry-pi-cheat-sheet?src=raspberry_pi_resource_menu3&amp;intcmp=701f2000000h4RcAAI">Getting started with Raspberry Pi cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/kubernetes-raspberry-pi?src=raspberry_pi_resource_menu6&amp;intcmp=701f2000000h4RcAAI">eBook: Running Kubernetes on your Raspberry Pi</a></li>
<li><a href="https://www.redhat.com/en/resources/data-intensive-applications-hybrid-cloud-blueprint-detail?src=raspberry_pi_resource_menu7&amp;intcmp=701f2000000h4RcAAI">Whitepaper: Data-intensive intelligent applications in a hybrid cloud blueprint</a></li>
<li><a href="https://www.redhat.com/en/topics/edge-computing?src=raspberry_pi_resource_menu5&amp;intcmp=701f2000000h4RcAAI">Understanding edge computing</a></li>
<li><a href="https://opensource.com/tags/raspberry-pi?src=raspberry_pi_resource_menu4&amp;intcmp=701f2000000h4RcAAI">Our latest on Raspberry Pi </a></li>
</ul></div>
</div>
</div>
</div>
<p>Byobu allows you to maintain multiple terminal windows, connect via SSH (secure shell), disconnect, reconnect, and even let other people access it, all while keeping the session alive.</p>
<p>For example, if you are SSH'd into a Raspberry Pi or server and run (for example) <strong>sudo apt update &amp;&amp; sudo apt upgrade</strong>—and lose your internet connection while it is running, your command will be lost to the void. However, if you start a byobu session first, it will continue running and, when you reconnect, you will find it's been running happily without your eyes on it.</p>
<figure class="image media-wysiwyg-align-center file-default media-element"><img alt="the byobu logo" data-delta="1" data-fid="466146" data-media-element="1" src="https://opensource.com/sites/default/files/uploads/byobu.png" title="byobu screen" typeof="foaf:Image" width="640" height="640" /><figcaption>The byobu logo is a fun play on screens.</figcaption></figure><p>Byobu is named for a Japanese term for decorative, multi-panel screens that serve as folding room dividers, which I think is quite fitting.</p>
<p>To install byobu on Debian/Raspbian/Ubuntu:</p>
<p><strong>sudo apt install byobu</strong></p>
<p>Then enable it:</p>
<p><strong>byobu-enable</strong></p>
<p>Now drop out of your SSH session and log back in—you'll land in a byobu session. Run a command like <strong>sudo apt update</strong> and close the window (or enter the escape sequence (<a href="https://www.google.com/search?client=ubuntu&amp;channel=fs&amp;q=Enter-tilde-dot&amp;ie=utf-8&amp;oe=utf-8" target="_blank"><strong>Enter</strong>+<strong>~</strong>+<strong>.</strong></a>) and log back in. You'll see the update running just as you left it.</p>
<p>There are a <em>lot</em> of features I don't use regularly or at all. The most common ones I use are:</p>
<ul><li><strong>F2</strong> – New window</li>
<li><strong>F3/F4</strong> – Navigate between windows</li>
<li><strong>Ctrl</strong>+<strong>F2</strong> – Split pane vertically</li>
<li><strong>Shift</strong>+<strong>F2</strong> – Split pane horizontally</li>
<li><strong>Shift</strong>+<strong>Left arrow/Shift</strong>+<strong>Right arrow</strong> – Navigate between splits</li>
<li><strong>Shift</strong>+<strong>F11</strong> – Zoom in (or out) on a split</li>
</ul><p>You can learn more by watching this video:</p>
<p><iframe allowfullscreen="" height="315" src="https://www.youtube.com/embed/NawuGmcvKus" width="560"></iframe></p>
<h2 id="how-were-using-byobu">How we're using byobu</h2>
<p>Byobu has been great for the maintenance of <a href="https://opensource.com/article/20/1/piwheels">piwheels</a>, the convenient, pre-compiled Python packages for Raspberry Pi. We have a horizontal split showing the piwheels monitor in the top half and the syslog entries scrolled in real time on the bottom half. Then, if we want to do something else, we switch to another window. It's particularly handy when we're investigating something collaboratively, as I can see what my colleague Dave types (and correct his typos) while we chat in IRC.</p>
<p>I have byobu enabled on my home and work servers, so when I log into either machine, everything is as I left it—multiple jobs running, a window left in a particular directory, running a process as another user, that kind of thing.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="byobu screenshot"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/byobu-screenshot.png" width="675" height="382" alt="byobu screenshot" title="byobu screenshot" /></div>
      
  </article></p>
<p>Byobu is handy for development on Raspberry Pis, too. You can launch it on the desktop, run a command, then SSH in and attach yourself to the session where that command is running. Just note that enabling byobu won't change what the terminal launcher does. Just run <strong>byobu</strong> to launch it.</p>
<hr /><p><em>This article originally appeared on Ben Nuttall's <a href="https://tooling.bennuttall.com/byobu/" target="_blank">Tooling blog</a> and is reused with permission.</em></p>
