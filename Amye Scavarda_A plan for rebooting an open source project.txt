<p>Once in a while, you will have an open source project that needs care and feeding. Maybe you're just stepping into an existing project, and you need to figure out where the project is and where it needs to go. It needs a reboot. What do you do? How do you begin?</p>
<p>When I was in this situation, I was thrown into the deep end with a security issue. I spent a good eight months getting to know the community as we resolved that issue and other legacy infrastructure issues. After about a year, I finally felt like I had a good outline of the community and its challenges and strengths. I realized afterward that it would've been smarter if I had first invested time to review the project's status and create a strategy for how we tackled its needs.</p>
<p>Here are tools to put into your toolbox if you, too, find yourself in a similar predicament.</p>
<h2>A simple review and strategy framework</h2>
<ol><li>Ask yourself this question: <em>What do I know about this community?</em>
<ul><li>Set a timer for five minutes and, in that time, write down everything you think you know about the project.</li>
</ul></li>
</ol><ol start="2"><li>On another sheet of paper, create four quadrants by drawing two lines in a <strong>+</strong> shape.
<ul><li>Along one axis, write <em>Technical and Political</em>.</li>
<li>Along the other axis, write <em>Internal and External</em>.</li>
<li>Go back to the list you created in step 1. Write the items where each exists in the quadrants.</li>
</ul></li>
</ol><ol start="3"><li>Next, build a timeline of the things you need to do.
<ul><li>Get another sheet of paper and divide it into three columns. Label the columns <em>30</em>, <em>60</em>, and <em>90</em>.</li>
<li>Under each heading, write down what you need to do in the next 30 days, the next 60, and the next 90.</li>
<li>What pieces in your quadrant need to be there?</li>
<li>Don't worry if you're not sure you can accomplish everything in this timeframe. I'm almost three years into this, and I still have things on my original 30-60-90 list of things to do. (I had such vision!)</li>
</ul></li>
</ol><ol start="4"><li>Now you need to ask: <em>What pieces are missing?</em>
<ul><li>Get another sheet of paper, set a timer for five minutes, and write down every missing thing you can think of.</li>
<li>Note that some of the things that you think are missing may have some history behind them that you aren't aware of.</li>
<li>Also, consider that one of the things that may have been missing before is <em>you</em>.</li>
</ul></li>
</ol><p>By combining the <em>things you know</em> on the quadrant paper, the <em>things you need to do</em> on the 30-60-90 paper, and the <em>things that are missing</em>, you pretty much have your marching orders to calibrate your project.</p>
<ul><li>Your <em>quadrant paper</em> is your survey paper.</li>
<li>Your <em>30-60-90 paper</em> is your priorities.</li>
<li>Your <em>what's missing</em> paper is your gap analysis.</li>
</ul><h2>Other ideation suggestions</h2>
<p>Want to take your brainstorming process further? Try asking yourself these questions:</p>
<ol><li>If this project was a perfume, how would you describe it?</li>
<li>What would be the one position you'd hire for (if you could)?</li>
<li>Your project is a color. What color is it?</li>
</ol><p>Be creative here, because the more words you use, the more depth and color your plan will have.</p>
<h2>Extra credit</h2>
<p>There's one more thing you can do to strengthen your plan: Figure out what you need to focus on.</p>
<ul><li>Get another sheet of paper, and in just a few minutes write down all your priorities for the project (10-15 is a good goal).</li>
<li>Now, pick out your top five priorities from that list.</li>
<li>Everything else becomes your "do not do this at all costs" list because they will weaken your focus on your top five.</li>
</ul><p>Figuring out your project's status takes more than just time and several sheets of paper, but this is a useful structure for making your plan actionable and manageable. Keep your lists and come back to them in a few years; you might be surprised at how much you've accomplished.</p>
<hr /><p><em>This article was based on Amye Scavarda's talk, <a href="http://sched.co/Djsh" target="_blank">Rebooting an Open Source Project</a>, at the <a href="https://events.linuxfoundation.org/events/open-source-leadership-summit-2018/" target="_blank">Open Leadership Summit</a>, which was held March 6-8 in Sonoma Valley, California.</em></p>
