<p>The best way to ensure student success is to give them agency and access to the best tools available. We highlighted those tools and practices in <a href="https://opensource.com/" target="_blank">Opensource.com</a> this year, and the results were amazing.</p>
<p>As usual, open source continues to inspire innovation in both theory and practice. Our authors covered a broad range of topics within the education paradigm. We had something for everyone, including projects that could easily be applied in other settings. Here are a few highlights:</p>
<ul><li>
<p>We learned how students at Centennial Senior Public School in Brampton, Ontario, were <a href="https://opensource.com/article/18/1/new-linux-computers-classroom" target="_blank">repurposing computers with Linux</a>.</p>
</li>
<li>
<p>We learned how to <a href="https://opensource.com/article/18/3/computer-lab-school-raspberry-pi" target="_blank">create a computer lab</a> for less than $1500 using the Raspberry Pi platform.</p>
</li>
<li>
<p>We learned how to make literature reviews easier and more accessible <a href="https://opensource.com/article/18/6/open-source-literature-review-tools" target="_blank">using open source tools</a>.</p>
</li>
<li>
<p>We offered our readers a wealth of enrichment options, including <a href="https://opensource.com/article/18/6/fiction-book-list" target="_blank">12 fiction books</a> for Linux and open source fans.</p>
</li>
</ul><p>In addition, we shared a review and an invitation to read <a href="https://opensource.com/article/18/5/books-kids-linux-open-source" target="_blank">15 books</a>, including <em>Getting Started with Raspberry Pi</em> by Carrie Anne Philbin, <em>Coding Games in Scratch</em> by Jon Woodcock, <em>Python for Kids</em> by Jason Briggs, and a dozen more. Book recommendations and reviews proved to be very popular this year.</p>
<p>Python programming is immensely popular with emerging young programmers. <a href="https://opensource.com/article/18/8/getting-started-mu-python-editor-beginners" target="_blank">Getting started with Mu, a Python editor for beginners</a> provided a great segue into programming for younger students.</p>
<p>Our top-performing article highlighted the work of 14-year-old Python developer Joshua Lowe, who invited us to <a href="https://opensource.com/article/18/8/edublocks" target="_blank">Learn Python programming the easy way with EduBlocks</a>.</p>
<h2 class="rtecenter">Here is the entire list of reader favorites from 2018</h2>
