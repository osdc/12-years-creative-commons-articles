<p>In this week's edition of our open source news roundup, we cover the release of Linux kernel 4.14 LTS, Linux powering all 500 of the world's supercomputers, why banks favor open source software, how the Pentagon is going open source, and how the latest release of Firefox saves your processor.</p>
<p></p><center><strong>Open source news roundup for November 12-25, 2017</strong></center>
<h2>Linux dominates supercomputing</h2>
<p>
  It has been a long time coming, but now Linux is operating on all 500 of the world's supercomputers. <a href="http://www.zdnet.com/article/linux-totally-dominates-supercomputers/" target="_blank">According to ZDNet's Steven J. Vaughn-Nichols</a>, the last two non-Linux systems, a pair of computers running AIX, were recently shuttered.
</p>
<h2>Linux kernel 4.14 LTS</h2>
<p>
  Linus Torvalds released version 4.14 of the kernel November 12. Though it was released a week late, this is a long-term stable release and will be supported for about two years. Read more about the features of the new release <a href="https://www.linux.com/news/2017/11/lts-linux-kernel-414-no-regressions" target="_blank">here</a>.
</p>
<h2>
  Pentagon prepares for big open source push<br /></h2>
<p>
  The National Defense Authorization Act of 2018 is the bill that funds the Pentagon and all that is dependent on it. An amendment to this bill <a href="https://www.theverge.com/2017/11/14/16649042/pentagon-department-of-defense-open-source-software" target="_blank">sponsored jointly by Senators Mike Rounds (R-SD) and Elizabeth Warren (D-MA)</a> would allow the Pentagon to rely heavily on open source software.
</p>
<h2>
  Introducing Firefox Quantum<br /></h2>
<p>
  Mozilla <a href="https://blog.mozilla.org/blog/2017/11/14/introducing-firefox-quantum/" target="_blank">announced</a> the release of Firefox Quantum. It's twice as fast as its previous release and uses much less memory, according to Mozilla. There's also a new CSS engine that takes advantage of today's multi-core processors.
</p>
<h2>Banks favor open source</h2>
<p>
  A recent article on <a href="https://www.americanbanker.com/news/why-chase-capital-one-barclays-add-to-open-source-projects" target="_blank">American Banker</a> says bankers are increasingly turning to open source projects. The consensus is that open source software and the collaboration inherent in its development provides a system for the development of stable and secure software. Banks are coming to realize <a href="https://en.wikipedia.org/wiki/Linus%27s_Law" target="_blank">Linus's Law</a>, that "given enough eyes, all bugs are shallow."
</p>
<h3>
  Other news<br /></h3>
<ul><li><a href="https://techcrunch.com/2017/11/15/facebook-open-sources-open-r-distributed-networking-software/?ncid=mobilenavtrend" target="_blank">Facebook open sources Open/R networking software.</a></li>
<li><a href="https://thejournal.com/articles/2017/11/14/google-contest-exposes-students-to-open-source-coding.aspx" target="_blank">Google contest exposes students to open source coding.</a></li>
<li><a href="https://www.linux.com/news/why-and-how-set-open-source-strategy" target="_blank">How and why to set and open source strategy?</a></li>
<li><a href="https://www.raspberrypi.org/blog/raspberry-pi-brazil/" target="_blank">Raspberry Pi's new Blue Board</a></li>
<li><a href="https://flipboard.com/@flipboard/-pip-is-a-retro-games-console-for-kids-t/f-b310512d15%2Ftechcrunch.com" target="_blank">Pip for Raspberry Pi</a></li>
<li><a href="https://www.geeky-gadgets.com/libre-tritium-sbc-mini-pc-16-11-2017/">A mini Linux and Android computer for $9</a></li>
</ul>