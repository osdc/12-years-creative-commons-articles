<p>If you want a home security system to tell you if someone is lurking around your property, you don't need an expensive, proprietary solution from a third-party vendor. You can set up your own system using a Raspberry Pi, a passive infrared (PIR) motion sensor, and an LTE modem that will send SMS messages whenever it detects movement.</p>
<h2 id="prerequisites">Prerequisites</h2>
<p>You will need:</p>
<ul><li>A Raspberry Pi with Ethernet connection and Raspberry Pi OS</li>
<li>An HC-SR501 PIR motion sensor</li>
<li>1 red LED</li>
<li>1 green LED</li>
<li>An LTE modem (I used the Teltonika <a href="https://teltonika-networks.com/product/trm240/" target="_blank">TRM240</a>)</li>
<li>A SIM card</li>
</ul><h3 id="the-pir-motion-sensor">The PIR motion sensor</h3>
<p>The PIR motion sensor will sense motion when something that emits infrared rays (e.g., a human, animal, or anything that emits heat) moves in the range of the sensor's field or reach. PIR motion sensors are low power and inexpensive, so they're used in many products that detect motion. They can't say how many people are in the area and how close they are to the sensor; they just detect motion.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="PIR sensor"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/pir-sensor.jpg" width="650" height="607" alt="PIR sensor" title="PIR sensor" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Lukas Janenas, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on Raspberry Pi</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/resources/what-raspberry-pi?src=raspberry_pi_resource_menu1&amp;intcmp=701f2000000h4RcAAI">What is Raspberry Pi?</a></li>
<li><a href="https://opensource.com/downloads/raspberry-pi-guide?src=raspberry_pi_resource_menu2&amp;intcmp=701f2000000h4RcAAI">eBook: Guide to Raspberry Pi</a></li>
<li><a href="https://opensource.com/downloads/getting-started-raspberry-pi-cheat-sheet?src=raspberry_pi_resource_menu3&amp;intcmp=701f2000000h4RcAAI">Getting started with Raspberry Pi cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/kubernetes-raspberry-pi?src=raspberry_pi_resource_menu6&amp;intcmp=701f2000000h4RcAAI">eBook: Running Kubernetes on your Raspberry Pi</a></li>
<li><a href="https://www.redhat.com/en/resources/data-intensive-applications-hybrid-cloud-blueprint-detail?src=raspberry_pi_resource_menu7&amp;intcmp=701f2000000h4RcAAI">Whitepaper: Data-intensive intelligent applications in a hybrid cloud blueprint</a></li>
<li><a href="https://www.redhat.com/en/topics/edge-computing?src=raspberry_pi_resource_menu5&amp;intcmp=701f2000000h4RcAAI">Understanding edge computing</a></li>
<li><a href="https://opensource.com/tags/raspberry-pi?src=raspberry_pi_resource_menu4&amp;intcmp=701f2000000h4RcAAI">Our latest on Raspberry Pi </a></li>
</ul></div>
</div>
</div>
</div>
<p>A PIR sensor has two potentiometers, one for setting the delay time and another for sensitivity. The delay time adjustment sets how long the output should remain high after detecting motion; it can be anywhere from five seconds to five minutes. The sensitivity adjustment sets the detection range, which can be anywhere from three to seven meters.</p>
<h3 id="the-lte-modem">The LTE modem</h3>
<p>Long-term evolution (LTE) is a standard for wireless broadband communication based on the GSM/EDGE and UMTS/HSPA technologies. The LTE modem I'm using is a USB device that can add 3G or 4G (LTE) cellular connectivity to a Raspberry PI computer. In this project, I'm not using the modem for cellular connectivity, but to send messages to my phone when motion is detected. I can control the modem with serial communication and AT commands; the latter send messages from the modem to my phone number.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="LTE modem"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/modem.jpg" width="675" height="449" alt="LTE modem" title="LTE modem" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Lukas Janenas, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<h2 id="how-to-set-up-your-home-security-system">How to set up your home security system</h2>
<h3 id="step-1-install-the-software">Step 1: Install the software</h3>
<p>First, install the necessary software on your Raspberry Pi. In the Raspberry Pi's terminal, enter:</p>
<pre><code class="language-bash">sudo apt install python3 python3-gpiozero python-serial -y</code></pre><h3 id="step-2-set-up-the-modem">Step 2: Set up the modem</h3>
<p>Insert your SIM card into your LTE modem by following these <a href="https://wiki.teltonika-networks.com/view/TRM240_SIM_Card" target="_blank">instructions</a> for the TRM240. Make sure to mount the antenna on the modem for a better signal.</p>
<h3 id="step-3-connect-the-modem-to-the-raspberry-pi">Step 3: Connect the modem to the Raspberry Pi</h3>
<p>Connect the LTE modem to one of the Raspberry Pi's USB ports, then wait for the device to boot up. You should see four new USB ports in the <code>/dev</code> directory. You can check them by executing this command in the terminal:</p>
<pre><code class="language-bash">ls /dev/ttyUSB*</code></pre><p>You should now see these devices:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="USB ports output"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/deviceoutput.png" width="442" height="37" alt="USB ports output" title="USB ports output" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Lukas Janenas, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>You will use the <strong>ttyUSB2</strong> port to communicate with the device by sending AT commands.</p>
<h3 id="step-4-connect-the-sensors-to-the-raspberry-pi">Step 4: Connect the sensors to the Raspberry Pi</h3>
<ol><li>
<p><strong>Connect the PIR sensor:</strong><br /><br />
	Connect the <strong>VCC</strong> and <strong>GND</strong> pins to the respective pins on the Raspberry Pi, and connect the motion sensor's output pin to the <strong>8 pin</strong> on the Raspberry Pi. See below for a schematic on these connections, and you can learn more about <a href="https://gpiozero.readthedocs.io/en/stable/recipes.html#pin-numbering" target="_blank">Raspberry Pi pin numbering</a> in the GPIO Zero docs.</p>
</li>
<li>
<p><strong>Connect the LED lights:</strong><br /><br />
	If you want indicator LEDs to light up when motion is detected, connect the cathode (short leg, flat side) of the LED to a ground pin; connect the anode (longer leg) to a current-limiting resistor; and connect the other side of the resistor to a GPIO pin (the limiting resistor can be placed either side of the LED). Connect the red LED to the <strong>38 pin</strong> on the board and the green LED to the <strong>40 pin</strong>.</p>
</li>
</ol><p>Note that this step is optional. If you don't want indicator LEDs when motion is detected, delete the LED sections from the example code below.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Raspberry Pi wiring diagram"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/wiring.png" width="650" height="523" alt="Raspberry Pi wiring diagram" title="Raspberry Pi wiring diagram" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Lukas Janenas, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<h3 id="step-5-launch-the-program">Step 5: Launch the program</h3>
<p>Using the terminal (or any text editor), create a file named <code>motion_sensor.py</code>, and paste in the <a href="#code">example code</a> below.</p>
<p>Find and change these fields:</p>
<ul><li><code>phone_number</code></li>
<li><code>message</code></li>
</ul><p>If you used different pins to connect the sensors, make sure to change the code accordingly.</p>
<p>When everything is set and ready to go, start the program from the terminal by entering:</p>
<pre><code class="language-bash">python3 motion_sensor.py</code></pre><p>If you don't have the required privileges to start the program, try this command:</p>
<pre><code class="language-bash">sudo python3 motion_sensor.py</code></pre><p>
<article class="align-center media media--type-image media--view-mode-full" title="Motion sensor hardware"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/motionssensorsetup.jpg" width="675" height="312" alt="Motion sensor hardware" title="Motion sensor hardware" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Lukas Janenas, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<h2 id="example-code"><a id="code" name="code"></a>Example code</h2>
<pre><code class="language-python">from gpiozero import MotionSensor, LED
from time import sleep, time
from sys import exit
import serial
import threading


# Raspberry Pi GPIO pin config
sensor = MotionSensor(14)
green = LED(21)
red = LED(20)

# Modem configuration
device = '/dev/ttyUSB2'
message = '&lt;message&gt;'
phone_number = '&lt;phone_number&gt;'
sms_timeout = 120 # min seconds between SMS messages


def setup():
    port.close()

    try:
        port.open()
    except serial.SerialException as e:
        print('Error opening device: ' + str(e))
        return False

    # Turn off echo mode
    port.write(b'ATE0 \r')
    if not check_response('OK', 10):
        print('Failed on ATE0')
        return False

    # Enter SMS text mode
    port.write(b'AT+CMGF=1 \r')
    if not check_response('OK', 6):
        print('Failed on CMGF')
        return False

    # Switch character set to 'international reference alphabet'
    # Note: this still doesn't support all characters
    port.write(b'AT+CSCS="IRA" \r')
    if not check_response('OK', 6):
        print('Failed on CSCS')
        return False

    return True


def check_response(string, amount):
    result = ''

    try:
        result = port.read(amount).decode()
    except:
        return False

    if not string in result:
        try:
            # Write 'ESC' to exit SMS input mode, just in case
            port.write(b'\x1B \r')
        except:
            return False

    return string in result


def send_sms():
    global currently_sending, last_msg_time
    currently_sending = True

    try:
        port.write('AT+CMGS="{}" \r'.format(phone_number).encode())
        if not check_response('&gt;', 6):
            print('Failed on CMGS')
            currently_sending = False
            return

        # Write the message terminated by 'Ctrl+Z' or '1A' in ASCII
        port.write('{}\x1A \r'.format(message).encode())

        while True:
            result = port.readline().decode()

            if 'OK' in result:
                print('&gt; SMS sent successfully')
                last_msg_time = time() 
                currently_sending = False
                return

            if 'ERROR' in result:
                print('&gt; Failed to send SMS [{}]'.format(result.rstrip()))
                currently_sending = False
                return
    except:
        # Initiate setup if the got while the program was running
        setup()
        currently_sending = False


def on_motion():
    print('Motion detected!')
    green.off()
    red.on()

    if time() - last_msg_time &gt; sms_timeout and not currently_sending:
        print('&gt; Sending SMS...')
        threading.Thread(target=send_sms).start()


def no_motion():
    green.on()
    red.off()


print('* Setting up...')
green.on()
red.on()

port = serial.Serial()
port.port = device
port.baudrate = 115200
port.timeout = 2

last_msg_time = 0
currently_sending = False

if not setup():
    print('* Retrying...')
    if not setup():
        print('* Try restarting the modem')
        exit(1)

print('* Do not move, setting up the PIR sensor...')
sensor.wait_for_no_motion()

print('* Device ready! ', end='', flush=True)
green.on()
red.off()

sensor.when_motion = on_motion
sensor.when_no_motion = no_motion
input('Press Enter or Ctrl+C to exit\n\n')</code></pre><p> </p>
