<p><em>This is a summary of Jessica McKellar's talk about Python in the Enterprise at the <a href="http://www.allthingsopen.org/speakers.html" target="_blank" title="All Things Open conference 2013">All Things Open conference</a> this year</em>.<em> She is on the Board of Directors for the <a href="http://python.org/psf/" target="_blank" title="PSF">Python Software Foundation</a> and an active leader of the <a href="http://www.meetup.com/bostonpython/?trax_also_in_algorithm2=original&amp;traxDebug_also_in_algorithm2_picked=original" target="_blank" title="Boston Python User Group page">Boston</a> Python User Group.</em></p>
<hr /><p>Python, the programming language, is an open source, volunteer-driven project. Historically viewed as a <a href="https://en.wikipedia.org/wiki/Scripting_language" target="_blank" title="scripting language">scripting language</a> (think: slow), the Python of today has developed into a robust and responsive language for the enterprise and other open initiatives around the world—with a Foundation to boot that reinvests money into the community and works to attract newcomers.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Programming and development</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ&amp;src=programming_resource_menu4">Red Hat Developers Blog</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Programming cheat sheets</a></li>
<li><a href="https://www.redhat.com/en/services/training/learning-subscription?intcmp=7016000000127cYAAQ">Try for free: Red Hat Learning Subscription</a></li>
<li><a href="https://opensource.com/downloads/bash-programming-guide?intcmp=7016000000127cYAAQ">eBook: An introduction to programming with Bash</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/bash-shell-cheat-sheet?intcmp=7016000000127cYAAQ">Bash Shell Scripting Cheat Sheet</a></li>
<li><a href="https://developers.redhat.com/e-books/modernizing-enterprise-java?intcmp=7016000000127cYAAQ">eBook: Modernizing Enterprise Java</a></li>
</ul></div>
</div>
</div>
</div>

<p>Jessica McKellar shared so many gains for Python community at the All Things Open conference <a href="http://www.atoschedule.com/events/python-for-the-enterprise" target="_blank" title="Python for the enterprise">on October 23</a> that I'm not sure where to start. So, I'll begin with the recent release of Python 3.4. Aside from new features including library modules and other major improvements, the gold star of achievement goes to their score on the Coverity Software Integrity Report: 3. This is almost unheard of for a large, open source project. (<a href="http://www.allthingsopen.org/files/mckellar.pdf" style="letter-spacing: 0px;" target="_blank" title="full presentation, slides">More here</a> on the new release.)</p>
<h3>Users of Python</h3>
<p>Sizeable installations with high traffic use Python (to name a few):</p>
<ul><li>Reddit (whole site)</li>
<li>YouTube (just about everything)</li>
<li>Many projects at Google</li>
<li>NASA (glue language for many platforms)</li>
<li>NYSE transaction system</li>
<li>Rackspace ("CORE" CRM and ERP platform)</li>
</ul><h3>Python adoption </h3>
<p>Two of the <em>most likely to be disrupted </em>fields right now are health and science. We are already seeing innovation in the development of health sensors (hardware) and tracking (software), and the call for open access for scientific research gets louder everyday. Python is becoming an increasingly greater part of this conversation, as it is used to process high performance data and popular hardware devices choose it as their primary language (like, the Raspberry Pi). Even the gaming industry is jumping on board; many role-playing games are now entirely written in Python (it's not just the glue!).</p>
<p>Universities are also adopting Python (<a href="http://www.allthingsopen.org/files/mckellar.pdf" target="_blank" title="full presentation, slides">slide 31</a> shows 43 programs using it out of the top three most-used programming languages). Jessica explains that educators are understanding that by teaching Python to their students, they are investing in a language that can get them hired.</p>
<p><img alt="Universities using Python" class="mceItem" height="389" src="https://opensource.com/sites/default/files/resize/images/life-uploads/edu.python.mckellar-520x389.PNG" title="Universities using Python" width="520" /></p>
<p>Python is seeing high adoption rates across the board (see: <a href="https://github.com/trending?l=python" target="_blank" title="trending Python repos on GitHub">trending repositories on GitHub</a>). User groups and conferences around the world are working to onboard newbies, educate beginners, and strengthen the communtiy of experienced developers and friends of Python. One of my favorite slides in Jessica's presentation is the graph by PyPL (popularity of programming language index) that charts 11 of the most popular languages from 2004 to 2013 (<a href="http://www.allthingsopen.org/files/mckellar.pdf" target="_blank" title="full presentation, slides">slide 30</a>). The results show Python's transformation and growth over the past nine years, which Jessica atrributes to the following innate characteristics of the language and community of developers:</p>
<ul><li>Able to get work done with only a few lines of code</li>
<li>High standards of quality</li>
<li>High coding, testing, and documentation standards</li>
</ul><p><img alt="Popularity of Python graph" class="mceItem" height="389" src="https://opensource.com/sites/default/files/resize/images/life-uploads/graphpython.mckellar_0-520x389.PNG" title="Popularity of Python graph" width="520" /></p>
<p><a href="http://www.allthingsopen.org/files/mckellar.pdf" target="_blank" title="full presentation, slides">Download full presentation (PDF slides).</a></p>
<hr /><p style="text-align: center;"><em><strong>What project are using Python for?</strong></em></p>
