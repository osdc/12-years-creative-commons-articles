<p>If you're part of a team producing live meeting or conferences, chances are you'd like to record speakers' presentations and make them available on the web. Fortunately, this is easy and relatively inexpensive today, thanks to open source software and readily available hardware. <a href="https://opensource.com/article/17/9/equipment-recording-presentations">Part 1</a> of this series on recording live presentations covered the equipment you need. Now it's time to prepare your software to capture video, and part 3 will explain how to record a presentation and troubleshoot anything that goes wrong.</p>
<p>This project uses two cross-platform, free and open source software packages: <a href="https://obsproject.com/" target="_blank">Open Broadcaster Software</a> (OBS) presentation capture software, which is used to do live recordings, and <a href="https://inkscape.org/en/" target="_blank">Inkscape</a> vector graphics editor software, which is used to create a mask for OBS with information about the talk, logo, background, and transparent areas for the video inputs. Download and install both from their respective websites or repositories/packet managers.</p>
<p>General notes:</p>
<ul><li>Plug all your devices together before starting your recording computer so the system loads drivers during startup and is more stable.</li>
<li>Before starting OBS, close all other applications and dedicate your computer to the recording. Stable recording needs all of the computer's power.</li>
<li>If you aren't live streaming, disable networking.</li>
<li>Remove unnecessary USB devices so your USB ports are dedicated to your recording devices.</li>
<li>Check that the USB sound card is detected.</li>
</ul><p>Now that you have everything plugged in, let's dive into the software with the methods we used at EclipseCon France 2017.</p>
<h2>Create a mask with Inkscape</h2>
<p>The first step is to create a mask for the speaker recordings that displays the speaker, the slides, and the conference information on one screen. This is how it will look when it's finished.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="EclipseCon video"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/obs-1.png" width="600" height="475" alt="EclipseCon video" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>It is simple to do with a few Inkscape tricks. First, open a new document in Inkscape.</p>
<h3>Create the mask base</h3>
<p>In the menu, select <em>File/Document properties</em>, and change the document size to 1280x720 pixels.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Setting custom screen size"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/obs-12_maskbasesize.png" width="452" height="120" alt="Setting custom screen size" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<ul><li>Create one 1280x720 pixel rectangle (the same size as the document), fill it with a gray color, without contour (no stroke), and center it exactly in the document.</li>
<li>Create two 4:3 rectangles (e.g.,640x480 pixels), and fill them in black without contour.</li>
<li>Using <em>Ctrl</em>+<em>Click</em> to keep the ratio, resize them to create one big and one small rectangle. Align them as shown in the image below.</li>
<li>Select both with <em>Shift</em>+<em>Click</em>, and select <em>Path/Combine</em> in the menu.</li>
</ul><p><article class="align-center media media--type-image media--view-mode-full" title="Drawing video boxes"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/obs13_inkscape-newdoc.png" width="600" height="413" alt="Drawing video boxes" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>Now, combine them to create the transparent areas for the audio/video capture:</p>
<ul><li>Select all with <em>Ctrl</em>+<em>A</em>.</li>
<li>In the menu, click on <em>Path/Object to Path</em>.</li>
<li>Click on <em>Difference</em>.</li>
</ul><p>Now, you have two transparent areas. To check, create a simple black rectangle, move it to the background, and look:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Checking transparency"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/obs14_inkscape-2.png" width="600" height="413" alt="Checking transparency" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>Remove the black square, and you have the base of your mask for OBS.</p>
<p>To add a background texture to the mask:</p>
<ul><li>Create a picture in your favorite picture editor, or download a free image from <a href="https://unsplash.com/photos/gILHZBtL_JA" target="_blank">Unsplash</a> or <a href="https://pixabay.com/" target="_blank">Pixabay</a> so you are clear with the license (such as this <a href="https://unsplash.com/photos/gILHZBtL_JA" target="_blank">image</a> by Britt Felton).</li>
<li>Import the picture in Inkscape.</li>
<li>Resize it to be a little bit larger than the mask document (e.g., 1366x768).</li>
<li>Select the picture and, in the menu, select <em>Object / Pattern / Object to Pattern</em>.</li>
<li>Select the mask and click on the gray area.</li>
<li>In the <em>Fill and Strok</em>e menu, click on the <em>Fill</em> tab.</li>
</ul><p><article class="align-center media media--type-image media--view-mode-full" title="Selecting fill option"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/obs15_inkscape-fill.png" width="337" height="118" alt="Selecting fill option" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<ul><li>Select the pattern you want to use from the list.</li>
</ul><p><article class="align-center media media--type-image media--view-mode-full" title="Choosing pattern"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/obs16_inkscape-pattern.png" width="328" height="55" alt="Choosing pattern" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<ul><li>You can move the pattern around in the mask. Double click on the mask to see the nodes of the mask and the nodes of the pattern. You can set the beginning of the pattern, rotate it, etc.</li>
</ul><p><article class="align-center media media--type-image media--view-mode-full" title="Adding the pattern"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/obs17_inkscape-maskpattern.png" width="600" height="413" alt="Adding the pattern" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>Adding text and images, such as a company name and/or logo, information about the event, a social media channel or website, etc., is a smart way to brand your video. Think about what you would like viewers to see (in addition to the video) while watching the presentation, and insert them on the mask.</p>
<p>Finally, export your mask so you can use it in OBS.</p>
<ul><li>In the menu, click on <em>File / Export PNG image</em>.</li>
<li>Click on <em>Page</em>, then check the image size (make sure it's still 1280x720) and set the path and filename to save the mask. And don't forget to click on <em>Export</em>, or nothing will happen.</li>
</ul><p><article class="align-center media media--type-image media--view-mode-full" title="Export PNG"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/obs18_exportpng.png" width="339" height="464" alt="Export PNG" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>Before you close Inkscape, don't forget to save the file, in case you want to modify it or to use to create new masks.</p>
<p>You should have a 1280x720 pixel PNG image file that looks something like this:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Mask template"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/obs19_template.png" width="600" height="338" alt="Mask template" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>The checkerboard areas are transparent. OBS manages PNG transparency very well, as you will see below.</p>
<p>These basic steps can be use to create other types of masks, such as:</p>
<ul><li>The same mask, but with a 16:9 ratio for the speaker input.</li>
<li>A mask to display only one video source (e.g., only the laptop output or only the webcam) rather than two, if you need to show something bigger.</li>
<li>Intro, outro, and "please wait" masks.</li>
<li>Title templates to add a message or the speaker's name and presentation title. You can add a simple message directly in OBS, or you can prepare your titles in advance in Inkscape.</li>
</ul><h2>OBS setup</h2>
<p>Now that the mask is finished, it's time to configure OBS for a good quality 720p video recording.</p>
<p>Let's start with a basic OBS configuration for recording live presentations. Start OBS and click on <em>Settings</em>.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="OBS settings"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/obs-20_settings.png" width="135" height="151" alt="OBS settings" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>Select <em>Output</em> from the left-hand menu.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Output settings"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/obs-21_outputsettings.png" width="600" height="490" alt="Output settings" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>Change the audio quality to 192khz at minimum and set your recording path (where you want to save the recording). The other default options are fine for a Core i5 processor. </p>
<p>In terms of <em>Recording Quality</em>, note that the more you compress your file, the more power you need, but the smaller the output file. A medium compression produces larger files, but requires less power. Keep in mind that a Core i5 CPU is not ultra-powerful. With this configuration, expect files to be around 800MB for a 40-minute talk.</p>
<p>You have a choice among FLV, MOV, MP4, MKV, and others for <em>Recording Format</em>. MKV is the safest container to use, because if OBS or the operating system crashes, you can recover the recording (up until the crash). You can also upload MKV directly to YouTube.</p>
<p>If you need an MP4 for post-editing, in OBS's menu, find <em>File / Remux Recordings</em>, browse and open the MKV file, click on Remux, and it will generate a MP4 file with no decrease in quality. Simple, easy, and very fast.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Remux"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/obs-22_remux.png" width="491" height="152" alt="Remux" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>Next, select <em>Audio</em> from the left-hand menu.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Audio settings"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/obs-23_audiosettings.png" width="600" height="490" alt="Audio settings" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>We'll maintain the typical configuration of recording audio in two 48KHz tracks (stereo). This makes it easy to read the recording, upload it to the web, remux it in MP4, or import it in an editor.</p>
<p>By default, we will disable all audio sources that OBS can detect and add our USB sound card manually in the <em>Audio</em> settings. This ensures that we record only the speaker (and avoid the common mistake of accidentally recording system sounds or the webcam's microphone).</p>
<p>Video is our last configuration. Again, select it from the left-hand menu.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Video settings"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/obs-24_videosettings.png" width="600" height="490" alt="Video settings" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>The goal is to record a good 720p video. The resolution for a 16:9, 720p video is 1280x720 pixels.</p>
<p>FPS (frames per second) is a standard that changes depending on your location. In North America, it's usually 30fps (or 29.97), and in Europe it's usually 25fps. Cinema standard has been 24fps for years. It's up to you, but keep in mind that 25fps will do the job very well and generate a smaller file than 30fps.</p>
<p>Additional settings requirements depend on your target:</p>
<ul><li><a href="https://support.google.com/youtube/answer/1722171?hl=en" target="_blank">YouTube</a> encoding and formatting recommendations</li>
<li><a href="https://vimeo.com/help/compression" target="_blank">Vimeo</a> video compression guidelines</li>
</ul><p>It's a good practice to record with a better quality than required, then compress your video afterward. This way you have good material for other uses.</p>
<p>After OBS is configured, it will keep those settings whenever it starts (until they're changed). A configuration can be saved as a profile and new ones can be created and saved; this is a good way to manage multiple configurations.</p>
<h2>Setting up the video and mask inputs</h2>
<p>By default, OBS starts with an empty Scene in the left column. One scene is enough for this use, but it needs to be prepared before recording. Let's do it with a concrete example: recording talks at EclipseCon France.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="OBS menu"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/obs-25_scenes.png" width="600" height="132" alt="OBS menu" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>In each <em>Scene</em>, you can add <em>Sources</em>. <em>Sources</em> can be anything: a screen, a window, a webcam, streaming via VLC, a webpage, a picture, an audio source, etc.</p>
<p>Click on <strong><em>+</em></strong> in the <em>Sources</em> column to see this menu:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Video sources"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/obs-26_sources.png" width="298" height="323" alt="Video sources" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>Add the speaker laptop input. At EclipseCon France, we used Epiphan devices, so I will also use an Epiphan in this tutorial.</p>
<p>First, check that the USB webcam and the Epiphan device are plugged in (no joke, please check again). Then, connect the speaker's laptop's VGA output and the projector cable to the VGA splitter (see the image at the end of <a href="https://opensource.com/article/17/8/equipment-recording-presentations">Part 1</a> for the proper setup). When the projector displays the speaker laptop, the Epiphan device should be ready and visible in the list of devices. To add it:</p>
<ul><li>Click on <em><strong>+</strong></em> in the <em>Sources</em> column.</li>
<li>Select <em>Video Capture Device</em> (V4L2)</li>
<li>In <em>Create New</em>, add a name like <em>Epiphan</em> or <em>Speaker Laptop</em>. Make sure the name is obvious; you don't want to be live and not sure what you see. Click on <em>OK</em>.</li>
</ul><p><article class="align-center media media--type-image media--view-mode-full" title="Naming Epiphan source"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/obs-27_setsource.png" width="352" height="342" alt="Naming Epiphan source" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<ul><li>In the new window, click on <em>Device</em> and select the Epiphan. You should see a preview. A green screen will be visible for a second, then you should see the same thing on the projector:</li>
</ul><p><article class="align-center media media--type-image media--view-mode-full" title="Device properties"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/obs-28_sourceproperties.png" width="600" height="506" alt="Device properties" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>Keep the default configuration (it should work fine), and click on <em>OK</em>.</p>
<p>Note: If the preview stays green, change the screen resolution on the laptop to enable the Epiphan device to handle it. Try a smaller resolution or another form factor (e.g., 16:9 to 4:3).</p>
<p>Then, do the same with the USB webcam. If you are using a laptop, you will probably see the integrated webcam in the device list, just select the right one.</p>
<ul><li>Give it a clear name, like <em>Webcam</em> or <em>Room View</em>.</li>
<li>Configure the webcam resolution to 640x480 pixels.</li>
<li>If possible, set the frame rate to 25fps (or the frame you are using in the OBS configuration). OBS requires less CPU power when the frame rates are the same.</li>
</ul><p><article class="align-center media media--type-image media--view-mode-full" title="Webcam properties"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/obs-29_cameraproperties.png" width="600" height="507" alt="Webcam properties" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>You should have something like this:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Resizing video boxes"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/obs-30_editing.png" width="600" height="475" alt="Resizing video boxes" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>The final step is to add the mask and change the size and position of the video inputs to match the transparent areas:</p>
<ul><li>Click on <strong><em>+</em></strong> in the <em>Sources</em> column and select <em>Image</em>.</li>
</ul><p><article class="align-center media media--type-image media--view-mode-full" title="Selecting Mask source"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/obs-31_imagesource.png" width="300" height="324" alt="Selecting Mask source" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<ul><li>Name it <em>Mask</em> or something like that and click on <em>OK</em>.</li>
</ul><p><article class="align-center media media--type-image media--view-mode-full" title="Naming Mask source"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/obs-32_imagemask.png" width="352" height="342" alt="Naming Mask source" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<ul><li>Browse to find the PNG file you exported from Inkscape.</li>
</ul><p><article class="align-center media media--type-image media--view-mode-full" title="Mask properties"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/obs-33_maskproperties.png" width="600" height="507" alt="Mask properties" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<ul><li>In the preview, check that the transparent areas of the mask are dark gray, like OBS's background. If it is white, there's a problem with your export from Inkscape; try the export again. If not, click on <em>OK</em>.</li>
</ul><p>You should now have something like this:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="EclipseCon video"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/obs-1.png" width="600" height="475" alt="EclipseCon video" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>The last step is to resize and position the <em>Sources</em>:</p>
<ul><li>Click on <em>Epiphan</em> in the <em>Sources</em> column.</li>
<li>There will be a red frame around the preview in the monitoring area. Move and resize it to fit its place. Do not hesitate to oversize the frame to hide black borders or compensate inside the input frame. <em>Ctrl</em>+<em>Click</em> provides more precision and control.</li>
<li>Do the same with the webcam.</li>
<li>Check that the <em>Mask</em> is at the top of the <em>Sources</em> list. You can change the order using the up and down arrows (circled below).</li>
</ul><p><article class="align-center media media--type-image media--view-mode-full" title="Reordering sources"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/obs-35_videoisready.png" width="600" height="475" alt="Reordering sources" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>Other options include creating other scenes and configuring transitions and changing between them during the live presentation. Test your scenes each time to be sure that everything is working well before recording each presentation. We had only one scene at EclipseCon France, which decreases the number of things to check before each talk. Recording a live presentation can be stressful, so try to keep things as simple as possible.</p>
<p>Now that the video is ready, it's time to configure the audio.</p>
<h2>Configure the audio input</h2>
<p>At EclipseCon, we used a wireless microphones, so I will use one for this example. Check that (yes, check again):</p>
<ul><li>The microphone output is connected to the sound card input.</li>
<li>The input gain on the sound card is not on mute.</li>
<li>The microphone transmitter receiver output levels are not too low, not too loud, and not on mute. If you set the levels too high, the sound will be saturated or truncated, resulting in a poor signal quality. Use the level meters or the peak level diodes to check that you never hit the maximum level.</li>
<li>The battery levels are good; if any doubt, change them.</li>
<li>You have spare batteries for your devices.</li>
</ul><p>Keep in mind that it is better if the microphone is set a little bit low rather than having a distorted voice because the level was set too high.</p>
<p>Add the audio input in OBS by clicking on <strong><em>+</em></strong> in the <em>Source</em> column.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Audio input"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/obs-36_audioinput.png" width="299" height="322" alt="Audio input" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>There are two choices:</p>
<ul><li>To add just the USB sound card dedicated to OBS, select <em>Audio Capture Device (ALSA)</em>.</li>
<li>To use the main operating system sound card, use <em>Audio Input Capture (PulseAudio)</em>. In this case, the sound card must be selected as the input source in the sound preferences.</li>
</ul><p><article class="align-center media media--type-image media--view-mode-full" title="Add audio source"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/obs-37_audiosource.png" width="352" height="342" alt="Add audio source" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<ul><li>Name the audio source and click <em>OK</em>.</li>
<li>Select your device in the list.</li>
</ul><p><article class="align-center media media--type-image media--view-mode-full" title="Select audio device"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/obs-38_selectaudiodevice.png" width="600" height="161" alt="Select audio device" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<ul><li>Set the sample rate at 48000 Hz and click <em>OK</em>.</li>
</ul><p><article class="align-center media media--type-image media--view-mode-full" title="Set sample rate"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/obs-39_samplerate.png" width="600" height="70" alt="Set sample rate" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<ul><li>There is now an input in the <em>Mixer</em> column.</li>
</ul><p><article class="align-center media media--type-image media--view-mode-full" title="Audio mixer"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/obs-40_audiomixer.png" width="213" height="181" alt="Audio mixer" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<ul><li>Click on the settings symbol next to <em>Mixer</em>.</li>
</ul><p><article class="align-center media media--type-image media--view-mode-full" title="Advanced audio properties"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/obs-41_advancedaudioproperties.png" width="600" height="104" alt="Advanced audio properties" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<ul><li>Check the <em>Downmix to Mono</em> box, as there's only one microphone and the voice should be in the center.</li>
<li>Use <em>Sync Offset (ms)</em> to synchronize the speaker voice with what's shown on the webcam. In our case, this is not necessary, as the speaker is not near the webcam and is very small in the composition, so any discrepancy will not be noticeable.</li>
<li>Keep two tracks to create a standard stereo MKV recording. This creates a file that's easy to use in an editor, upload to the web, or remux into a stereo MP4 file.</li>
<li>Click <em>Close</em>.</li>
</ul><p>Add a noise suppression filter and a limiter on the audio input to record a clean sound. Noise suppression will remove the background noise from the microphone input (this is very useful, particularly with an inexpensive sound card like the Behringer). The limiter will prevent a hand clap or something hitting the microphone from damaging the audio track.</p>
<ul><li>Click on the settings symbol next to the audio input in the <em>Mixer</em> column and select <em>Filter</em>.</li>
</ul><p><article class="align-center media media--type-image media--view-mode-full" title="Audio filter"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/obs-42_audiofilter.png" width="305" height="118" alt="Audio filter" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<ul><li>Click on the <strong><em>+</em></strong> at the bottom and add <em>Noise Suppressio</em>n.</li>
</ul><p><article class="align-center media media--type-image media--view-mode-full" title="Noise Suppression"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/obs-43_noisesuppression.png" width="540" height="250" alt="Noise Suppression" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<ul><li>Keep the default parameter at -30dB.</li>
<li>Repeat the steps and add the <em>Compressor</em>.</li>
<li>Configure it as a fast limiter with the maximum ratio, fast <em>Attack</em> and <em>Release</em> times, and a <em>Threshold</em> slightly below 0dB (0dB is the maximum level for a digital audio signal).</li>
</ul><p><article class="align-center media media--type-image media--view-mode-full" title="Audio compression"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/obs-44_audiocompressor.png" width="633" height="303" alt="Audio compression" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<ul><li>Click <em>Close</em>.</li>
</ul><h2>Sound check</h2>
<p>It's time for a sound check.</p>
<p>Unfortunately, it is not yet possible to monitor audio directly with OBS in the Linux version, so use the sound card's monitoring features. Fortunately, most sound cards include a way to listen to the inputs, so you can be sure that the sound coming from the microphone is good.</p>
<p>The Mac and Windows versions of OBS enable software audio monitoring. I don't know why it's not included in the Linux version, but I can't wait to have it. (If a contributor can tell us why in a comment, that would be awesome, and maybe we can find some help.)</p>
<p>For the sound check, ask the presenters to use the microphone like they would present a slide. On the sound card, turn the gain of the microphone input so you can hear the speaker. Listen carefully, to make sure the voice is clear. If necessary, adjust the output level of the microphone receiver until you have an input level at maximum <sup>3</sup>/<sub>4</sub> of the view meter in OBS.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Setting max audio level"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/obs-45_audiomax.png" width="217" height="88" alt="Setting max audio level" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>This should ensure that the voice is never distorted, even if there is an issue during the presentation. If you feel the voice is too low after the recording, it can be improved by using an audio editor, since the recording quality will be good.</p>
<h2>Coming next</h2>
<p>Now it's time to record. <a href="https://opensource.com/article/17/9/recording-and-troubleshooting-live-presentations" target="_blank">Part 3 of this series</a> explains how to do that and what to do if something goes wrong.</p>
