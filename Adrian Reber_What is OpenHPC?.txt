<p><a href="https://insidehpc.com/hpc-basic-training/what-is-hpc/" target="_blank">High performance computing (HPC)</a>—the aggregation of computers into clusters to increase computing speed and power—relies heavily on the software that connects and manages the various nodes in the cluster. Linux is the dominant HPC operating system, and many HPC sites expand upon the operating system's capabilities with different scientific applications, libraries, and other tools.</p>
<p>As HPC began developing, that there was considerable duplication and redundancy among the HPC sites compiling HPC software became apparent, and sometimes dependencies between the different software components made installations cumbersome. The <a href="https://openhpc.community/" target="_blank">OpenHPC</a> project was created in response to these issues. OpenHPC is a community-based effort to solve common tasks in HPC environments by providing documentation and building blocks that can be combined by HPC sites according to their needs. </p>
<p>The project's mission and vision are:</p>
<p><strong>Mission</strong>: To provide a reference collection of open source HPC software components and best practices, lowering barriers to deployment, advancement, and use of modern HPC methods and tools.</p>
<p><strong>Vision</strong>: OpenHPC components and best practices will enable and accelerate innovation and discoveries by broadening access to state-of-the-art, open source HPC methods and tools in a consistent environment, supported by a collaborative, worldwide community of HPC users, developers, researchers, administrators, and vendors.</p>
<h2>OpenHPC history</h2>
<p>OpenHPC started at <a href="http://www.isc-hpc.com/id-2015.html" target="_blank">International Supercomputing 2015</a> (ISC'15) with a <a href="https://en.wikipedia.org/wiki/Birds_of_a_feather_(computing)" target="_blank">birds of a feather</a> (BoF) discussion on the merits of a community effort towards a HPC software repository and management framework. At <a href="http://sc15.supercomputing.org/" target="_blank">Supercomputing 2015</a> (SC'15), OpenHPC 1.0 was released and the Linux Foundation <a href="https://www.linuxfoundation.org/press-release/high-performance-computing-leaders-unite-to-develop-open-source-framework/" target="_blank">announced</a> the Open HPC Collaborative Project's technical leadership, founding members, and formal governance structure.</p>
<p>Since then, the growing number of <a href="http://openhpc.community/about-us/participants/" target="_blank">OpenHPC project members</a> have collaborated on regular updates of OpenHPC's software packages and documentation, with v1.1.1 released in June 2016 at <a href="http://www.isc-hpc.com/id-2016.html" target="_blank">ISC'16</a>. The newest, v1.3.2, was released in September 2017.</p>
<h2>What is OpenHPC?</h2>
<p>OpenHPC provides a collection of prebuilt software components common in HPC environments, such as provisioning tools, resource managers, I/O libraries, development tools, and a variety of scientific libraries. It supports different Linux distributions and architectures and provides a package repository for CentOS 7 and SUSE Linux Enterprise Server (SLES) 12.</p>
<p>One key tenet of the project is that no one is ever required to install the entire OpenHPC stack; rather, users can pick and choose the components that are relevant to their HPC needs.</p>
<p>OpenHPC v1.2 and later include builds for x86_64 and aarch64 (tech preview); <a href="https://github.com/openhpc/ohpc/blob/obs/OpenHPC_1.3.2_Factory/README.md" target="_blank">v.1.3.2</a> adds CentOS 7.3 and SLES 12 SP2 builds. In addition to the provided software packages, OpenHPC includes installation guides for different provisioning systems (<a href="http://warewulf.lbl.gov/" target="_blank">Warewulf</a>, <a href="https://xcat.org/" target="_blank">xCAT</a>) as well as different resource managers (<a href="https://slurm.schedmd.com/" target="_blank">Slurm</a>, <a href="http://pbspro.org/" target="_blank">PBS Professional</a>).</p>
<p>The software and documentation are maintained by the OpenHPC project members and external collaborators in a <a href="https://github.com/openhpc/ohpc/" target="_blank">Git repository</a> hosted at GitHub. To ensure that the software and documentation works as described, OpenHPC has a <a href="http://test.openhpc.community:8080/" target="_blank">continuous integration system</a> that verifies the software using bare metal installs and integration test suites.</p>
<p>OpenHPC takes into consideration that HPC sites often rely on multiple compilers that may have partially compatible or even incompatible <a href="https://en.wikipedia.org/wiki/Application_binary_interface" target="_blank">Application Binary Interfaces (ABIs)</a> and <a href="https://en.wikipedia.org/wiki/Message_Passing_Interface" target="_blank">Message Passing Interfaces (MPIs)</a>. With the adoption of hierarchical build configurations, OpenHPC delivers unique builds of software packages for each desired compiler/MPI permutation. Software packages that have specific compiler and MPI dependencies have those added to the package name in the following manner:</p>
<pre>
<code class="language-bash" target="_blank">package-&lt;compiler&gt;-&lt;mpi&gt;-ohpc</code></pre><p>By using <a href="https://en.wikipedia.org/wiki/Environment_Modules_(software)" target="_blank">environment modules</a> to load the corresponding compiler and MPI family, the environment variables will point to the user's compiler and MPI choices.</p>
<p>Supported compiler families are different versions of the <a href="https://gcc.gnu.org/" target="_blank">GNU Compiler Collection</a> or the Intel compiler, and supported MPI families are <a href="https://www.open-mpi.org/" target="_blank">Open MPI</a>, <a href="https://www.mpich.org/" target="_blank">MPICH</a>, <a href="http://mvapich.cse.ohio-state.edu/" target="_blank">MVAPICH2</a>, and Intel MPI. Using those options, the resulting package name could be:</p>
<pre>
<code class="language-bash" target="_blank">fftw-gnu7-openmpi-ohpc</code></pre><h2>OpenHPC software packages</h2>
<p>OpenHPC divides its software packages into different functional areas, and each includes multiple components:</p>
<ul><li>Provisioning: Warewulf, xCAT</li>
<li>Resource management: Slurm, Munge, PBS Professional</li>
<li>Runtimes: OpenMP, OCR, Singularity</li>
<li>Numerical/scientific libraries: GSL, FFTW, Metis, PETSc, Trilinos, Hypre, SuperLU, Mumps, OpenBLAS, Scalapack</li>
<li>I/O libraries: HDF5, NETCDF, Adios</li>
<li>Compilers: GCC, LLVM</li>
<li>MPI families: MVAPICH2, Open MPI, MPICH</li>
<li>Performance tools: PAPI, IMB, mpiP, pdtoolkit, TAU, Scalasca, ScoreP, SIONLib</li>
</ul><p>The next release (1.3.3) is planned for November 2017. As with every new release, new packages will be added and existing packages will be updated. Latest version can always be obtained from <a href="https://openhpc.community/downloads/" target="_blank">openhpc.community/downloads/</a>.</p>
<h2>Additional resources</h2>
<p>OpenHPC is a collaborative, community effort that is open for contributions. If you would like to learn more about OpenHPC and how you can contribute, please visit the following resources:</p>
<ul><li><a href="https://openhpc.community/" target="_blank">OpenHPC site</a></li>
<li><a href="https://github.com/openhpc/ohpc" target="_blank">GitHub site</a></li>
<li><a href="https://build.openhpc.community/" target="_blank">Build service and community package repository</a></li>
<li>"<a href="https://github.com/openhpc/ohpc/files/619162/HPCSYSPROS16_OpenHPC.pdf" target="_blank">Cluster Computing with OpenHPC</a>" (academic article)</li>
<li>Please visit the <a href="http://iebms.heiexpo.com/iebms/oep/oep_p2_details.aspx?oc=34&amp;ct=OEP&amp;eventid=5017&amp;OrderNbr=9064" target="_blank">OpenHPC booth</a> and come to our <a href="http://sc17.supercomputing.org/presentation/?id=bof131&amp;sess=sess373" target="_blank">BoF</a> at <a href="https://sc17.supercomputing.org/" target="_blank">SC'17</a> November 13-16</li>
</ul>