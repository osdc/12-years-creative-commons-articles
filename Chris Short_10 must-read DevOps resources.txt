<p>Continuous learning is a key component of DevOps and digital transformation. If you are not learning from your day-to-day operations, your operational mindset will never change. If you don't put effort into pushing the limits of each team member's mind, your team will never reach its fullest potential. To quote Andrew Clay Shafer: "You are either building a learning organization, or you will be losing to someone who is."</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More DevOps resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/resources/devops?intcmp=7013a00000263HlAAI">What is DevOps?</a></li>
<li><a href="https://opensource.com/downloads/devops-hiring-guide?intcmp=7013a00000263HlAAI">The ultimate DevOps hiring guide</a></li>
<li><a href="https://opensource.com/downloads/devops-monitoring-guide?intcmp=7013a00000263HlAAI">DevOps monitoring tools guide</a></li>
<li><a href="https://opensource.com/downloads/devsecops?intcmp=7013a00000263HlAAI">Getting started with DevSecOps</a></li>
<li><a href="https://enterprisersproject.com/cheat-sheet-devops-glossary?intcmp=7013a00000263HlAAI">Download the DevOps glossary</a></li>
<li><a href="https://www.ansible.com/resources/ebooks/ansible-for-devops?intcmp=7013a00000263HlAAI">eBook: Ansible for DevOps</a></li>
<li><a href="https://opensource.com/tags/devops?src=devops_resource_menu2">Latest DevOps articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>There are many ways to advance a continuous learning initiative in your organization, such as starting a book club or creating a reading list. Here are ten must-read DevOps resources to start your journey.</p>
<p><article class="align-center media media--type-video media--view-mode-full"><div class="field field--name-field-media-oembed-video field--type-string field--label-visually_hidden">
    <div class="field__label visually-hidden">Remote video URL</div>
              <div class="field__item"><iframe src="https://opensource.com/media/oembed?url=https%3A//www.youtube.com/watch%3Fv%3DIbnj-YZTypU&amp;max_width=0&amp;max_height=0&amp;hash=0TJA3lVpQ-5DBsRe3HTFv1KefEuOicgZW-E8C7dMR-c" frameborder="0" allowtransparency="" width="200" height="113" class="media-oembed-content" title="DevOps README.md—Chris Short"></iframe>
</div>
          </div>

  </article></p>
<hr /><table border="0" cellpadding="8" cellspacing="12" style="width: 500px;"><tbody><tr><td width="180">
<article class="align-left media media--type-image media--view-mode-full" title="The Phoenix Project book cover"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/life-uploads/phoenix_project_0.jpg" width="154" height="229" alt="The Phoenix Project cover" title="The Phoenix Project book cover" /></div>
      
  </article></td>
<td>
<h2><a href="https://itrevolution.com/book/the-phoenix-project//" target="blank_">The Phoenix Project</a></h2>
<p><b>by Kevin Behr, George Spafford, and Gene Kim</b></p>
<p>The quintessential start to any DevOps knowledge journey, this book takes you through the transformation of a broken IT organization toward a DevOps culture. Written as a novel, it is an easy read. Give a copy to senior engineers, C-suite types, and brand-new IT staff; they will be able to relate.</p>
</td>
</tr></tbody></table><hr /><table border="0" cellpadding="8" cellspacing="12" style="width: 500px;"><tbody><tr><td width="180">
<article class="media media--type-image media--view-mode-full" title="Devops handbook book cover"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/life-uploads/devops_handbook.jpg" width="145" height="229" alt="Devops handbook cover" title="Devops handbook book cover" /></div>
      
  </article></td>
<td>
<h2><a href="https://itrevolution.com/book/the-devops-handbook/" target="blank_">The DevOps Handbook</a></h2>
<p><b>by Gene Kim, Jez Humble, John Willis, and Patrick Debois</b></p>
<p>Far from just a bunch of code samples, this book is a guide to implementing specific examples to bring DevOps into an organization. After reading <em>The Phoenix Project</em>, this is the next logical step in your reading list—it offers years of experiences in one useful book.</p>
</td>
</tr></tbody></table><hr /><table border="0" cellpadding="8" cellspacing="12" style="width: 500px;"><tbody><tr><td width="180">
<p><article class="align-left media media--type-image media--view-mode-full" title="12 Factor App book cover"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/12-factor-cover.png" width="140" height="211" alt="12 Factor App book cover" title="12 Factor App book cover" /></div>
      
  </article></p>
</td>
<td>
<h2><a href="https://12factor.net/" target="blank_">The Twelve-Factor App</a></h2>
<p><b>by Adam Wiggins</b></p>
<p>With this e-book, Heroku cofounder Adam Wiggins changed how the world thinks about software-as-a-service. Design principles outlined here have become de facto standards for implementing software, and it includes excellent guidance for refactoring software and greenfield projects. The e-book is free and updated regularly.</p>
</td>
</tr></tbody></table><hr /><table border="0" cellpadding="8" cellspacing="12" style="width: 500px;"><tbody><tr><td width="180">
<article class="media media--type-image media--view-mode-full" title="Continuous delivery book cover"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/life-uploads/continuous_delivery.jpg" width="174" height="229" alt="Continuous delivery book cover" title="Continuous delivery book cover" /></div>
      
  </article></td>
<td>
<h2><a href="http://www.informit.com/store/continuous-delivery-reliable-software-releases-through-9780321601919" target="blank_">Continuous Delivery</a></h2>
<p><b>by Jez Humble and David Farley</b></p>
<p>This book focuses on how to deploy software faster, with an emphasis on automating tasks and automating first (because you won't have time to go back and automate later). When people say, "Shift left," they are talking about executing earlier in pipelines described in this book. This is a DevOps must-read.</p>
</td>
</tr></tbody></table><hr /><table border="0" cellpadding="8" cellspacing="12" style="width: 500px;"><tbody><tr><td width="180">
<article class="media media--type-image media--view-mode-full" title="Site Reliability Engineering book cover"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/life-uploads/sitereliabilityengineering.jpg" width="178" height="229" alt="Site Reliability Engineering book cover" title="Site Reliability Engineering book cover" /></div>
      
  </article></td>
<td>
<h2><a href="https://landing.google.com/sre/book.html" target="blank_">Site Reliability Engineering</a></h2>
<p><b>Edited by Betsy Beyer, Chris Jones, Jennifer Petoff, and Niall Murphy</b></p>
<p>I like to think of this collection of essays from the good folks running things at Google as a reference for how to do things the right way at enormous scale. The book won't offer solutions for every organization; most probably don't have Google-sized problems. Running your gigabyte-sized database across distributed infrastructure optimizes for availability. But it will cost you more than a handful of geographically diverse replicas.</p>
</td>
</tr></tbody></table><hr /><table border="0" cellpadding="8" cellspacing="12" style="width: 500px;"><tbody><tr><td width="180">
<article class="media media--type-image media--view-mode-full" title="Enterprise Devops Playbook cover"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/life-uploads/enterprise_devops_handbook.jpg" width="153" height="229" alt="Enterprise Devops Playbook cover" title="Enterprise Devops Playbook cover" /></div>
      
  </article></td>
<td>
<h2><a href="http://www.oreilly.com/webops-perf/free/enterprise-devops-playbook.csp" target="blank_">Enterprise DevOps Playbook</a></h2>
<p><b>by Bill Ott, Jimmy Pham, and Haluk Saker</b></p>
<p>This book serves as a roadmap for building a successful DevOps organization, addressing hiring, culture, and learning. While it might be missing a few components, it offers ways to bring DevOps to your current organization. (Yes, enterprise and DevOps can work together.)</p>
</td>
</tr></tbody></table><hr /><table border="0" cellpadding="8" cellspacing="12" style="width: 500px;"><tbody><tr><td width="180">
<article class="media media--type-image media--view-mode-full" title="Lean Enterprise book cover"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/life-uploads/lean_enterprise.jpg" width="149" height="229" alt="Lean Enterprise book cover" title="Lean Enterprise book cover" /></div>
      
  </article></td>
<td>
<h2><a href="http://shop.oreilly.com/product/0636920030355.do" target="blank_">Lean Enterprise</a></h2>
<p><b>by Jez Humble, Barry O'Reilly, and Joanne Molesky</b></p>
<p>This book helps readers become big-picture, business-minded change agents. An all-phase guide to planning, organizing, implementing, and measuring the lean business, it's great for leaders and managers alike. Regardless of how successful your organization might be, this book will change how you look at things.</p>
</td>
</tr></tbody></table><hr /><table border="0" cellpadding="8" cellspacing="12" style="width: 580px;"><tbody><tr><td width="180px">
<p><article class="media media--type-image media--view-mode-full" title="The Open Org Guide toIT Culture Change cover"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/theopenorg_guidetoitculturechange_1_0_front_cover_final_sm.jpg" width="145" height="217" alt="The Open Org Guide toIT Culture Change cover" title="The Open Org Guide toIT Culture Change cover" /></div>
      
  </article></p>
</td>
<td>
<h2><a href="https://opensource.com/open-organization/17/6/open-org-it-culture-now-available" target="blank_">The Open Organization Guide to IT Culture Change</a></h2>
<p><b>by The Open Organization</b></p>
<p>Most people agree that culture is the most critical component of DevOps. This collection of essays from people across the DevOps spectrum is a community-produced companion to Jim Whitehurst's 2015 book, <em>The Open Organization</em>. This is an accessible, inspirational book from a diverse group of authors. The chapter I most recommend is "Assuming Positive Intent when Working across Teams," by Jonas Rosland.</p>
</td>
</tr></tbody></table><hr /><table border="0" cellpadding="8" cellspacing="12" style="width: 500px;"><tbody><tr><td width="180">
<article class="media media--type-image media--view-mode-full" title="Upside of Stress book cover"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/life-uploads/upside_of_stress.jpg" width="150" height="229" alt="Upside of Stress book cover" title="Upside of Stress book cover" /></div>
      
  </article></td>
<td>
<h2><a href="https://smile.amazon.com/gp/product/1583335617/" target="blank_">The Upside of Stress: Why Stress Is Good for You, and How to Get Good at It</a></h2>
<p><b>by Kelly McGonigal, PhD</b></p>
<p>This book helps you learn to manage and embrace stress as a good thing. It explains how to turn stress-induced adrenaline, angst of the unknown, and feelings of nervousness into an opportunity to thrive rather than something to fight through. In fact, it suggests that stress can even make you happier when handled appropriately. How much better would your life be if stress turns out to be good for you?</p>
</td>
</tr></tbody></table><hr /><h2><a href="http://classics.mit.edu/Tzu/artwar.html" target="_blank">The Art of War</a></h2>
<p><b>by Sun Tzu</b></p>
<p>In DevOps, you <em>should not</em> have adversaries. But in business you will, and chances are, many of those adversaries have read <em>The Art of War</em>. Applying the principles of war from a Chinese military treatise from the 5th century BC to DevOps may seem like a stretch, but if you think of your next outage as your enemy and your pipeline as your supply line, Sun Tzu's ancient work starts shaping into an interesting strategy guide for almost any situation.</p>
<hr /><p>Are there other resources you think would help folks on their DevOps journey? Feel free to submit a pull request to the <a href="https://github.com/chris-short/DevOps-README.md" target="_blank">DevOps-README.md</a> GitHub repository or add a comment below.</p>
