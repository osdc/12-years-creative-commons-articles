<p>In this week's top 5, we take a look at maps, robots, and more!</p>
<p></p><center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/ng_fDg1zbR0" frameborder="0" allowfullscreen=""></iframe><p></p></center>
<h2>This week's top articles</h2>
<h3>5. <a href="https://opensource.com/article/17/8/upgrade-your-home-six-open-hardware-projects">6 hardware projects for upgrading your home</a></h3>
<p>When you make your house a little smarter, you’re going to want to use open hardware. Editor Alex Sanchez shares some projects that you can do yourself on your old house.</p>
<h3>4. <a href="https://opensource.com/article/17/8/twitter-bot">7 open source Twitter bots to follow</a></h3>
<p>Do you like Twitter, but wish it wasn’t so full of humans? Editor Jason Baker shares some bots you might want to follow or use as inspiration to make your own.</p>
<h3>3. <a href="https://opensource.com/article/17/8/linux-guitar-amp">How to make a low-cost guitar amp with Linux</a></h3>
<p>Are you having trouble getting your music to go to 11? Seth Kenlon shows you how to turn your computer into a rock and roll machine.</p>
<h3>2. <a href="https://opensource.com/article/17/8/raspberry-pi-twitter-bot">Make your own Twitter bot with Python and Raspberry Pi</a></h3>
<p>Community moderator Ben Nuttall shares how you can use a Raspberry Pi and the twython library to write your own Twitter bot. Now you can remind your friends to take out their trash cans every week.</p>
<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr" xml:lang="en">Okay, <a href="https://twitter.com/Jen_Scan">@Jen_Scan</a>, you wanted a trash reminder. Here it is.</p>
<p>— Ben Cotton (@FunnelFiasco) <a href="https://twitter.com/FunnelFiasco/status/895788107752243200">August 10, 2017</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><h3>1. <a href="https://opensource.com/article/17/8/openstreetmap">13 amazing maps to celebrate 13 years of OpenStreetMap</a></h3>
<p>Editor Jason Baker shares how this community mapping project has done a lot over the years. And the best part is that no one will yell at you to roll the maps.</p>
