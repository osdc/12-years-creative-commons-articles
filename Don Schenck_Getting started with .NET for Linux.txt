<p>When you know a software developer's preferred operating system, you can often guess what programming language(s) they use. If they use Windows, the language list includes C#, JavaScript, and TypeScript. A few legacy devs may be using Visual Basic, and the bleeding-edge coders are dabbling in F#. Even though you can use Windows to develop in just about any language, most stick with the usuals.</p>
<p>If they use Linux, you get a list of open source projects: Go, Python, Ruby, Rails, Grails, Node.js, Haskell, Elixir, etc. It seems that as each new language—Kotlin, anyone?—is introduced, Linux picks up a new set of developers.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Programming and development</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ&amp;src=programming_resource_menu4">Red Hat Developers Blog</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Programming cheat sheets</a></li>
<li><a href="https://www.redhat.com/en/services/training/learning-subscription?intcmp=7016000000127cYAAQ">Try for free: Red Hat Learning Subscription</a></li>
<li><a href="https://opensource.com/downloads/bash-programming-guide?intcmp=7016000000127cYAAQ">eBook: An introduction to programming with Bash</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/bash-shell-cheat-sheet?intcmp=7016000000127cYAAQ">Bash Shell Scripting Cheat Sheet</a></li>
<li><a href="https://developers.redhat.com/e-books/modernizing-enterprise-java?intcmp=7016000000127cYAAQ">eBook: Modernizing Enterprise Java</a></li>
</ul></div>
</div>
</div>
</div>
<p>So leave it to Microsoft (Microsoft?!?) to throw a wrench into this theory by making the .NET framework, coined <em>.NET Core</em>, open source and available to run on any platform. Windows, Linux, MacOS, and even a television OS: Samsung's Tizen. Add in Microsoft's other .NET flavors, including Xamarin, and you can add the iOS and Android operating systems to the list. (Seriously? I can write a Visual Basic app to run on my TV? What strangeness is this?)</p>
<p>Given this situation, it's about time Linux developers get comfortable with .NET Core and start experimenting, perhaps even building production applications. Pretty soon you'll meet that person: "I use Linux … I write C# apps." Brace yourself: .NET is coming.</p>
<h2>How to install .NET Core on Linux</h2>
<p>The list of Linux distributions on which you can run .NET Core includes Red Hat Enterprise Linux (RHEL), Ubuntu, Debian, Fedora, CentOS, Oracle, and SUSE.</p>
<p>Each distribution has its own <a href="https://www.microsoft.com/net/core" target="_blank">installation instructions</a>. For example, consider Fedora 26:</p>
<p>Step 1: Add the dotnet product feed.</p>
<pre>
<code class="language-bash">	sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
	sudo sh -c 'echo -e "[packages-microsoft-com-prod]\nname=packages-microsoft-com-prod \nbaseurl=https://packages.microsoft.com/yumrepos/microsoft-rhel7.3-prod\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" &gt; /etc/yum.repos.d/dotnetdev.repo'
</code></pre><p>Step 2: Install the .NET Core SDK.</p>
<pre>
<code class="language-bash">	sudo dnf update
	sudo dnf install libunwind libicu compat-openssl10
	sudo dnf install dotnet-sdk-2.0.0
</code></pre><h2>Creating the Hello World console app</h2>
<p>Now that you have .NET Core installed, you can create the ubiquitous "Hello World" console application before learning more about .NET Core. After all, you're a developer: You want to create and run some code <em>now</em>. Fair enough; this is easy. Create a directory, move into it, create the code, and run it:</p>
<pre>
<code class="language-bash">mkdir helloworld &amp;&amp; cd helloworld
dotnet new console
dotnet run
</code></pre><p>You’ll see the following output:</p>
<pre>
<code class="language-bash">$ dotnet run
Hello World!
</code></pre><h2>What just happened?</h2>
<p>Let's take what just happened and break it down. We know what the <strong>mkdir</strong> and <strong>cd</strong> did, but after that?</p>
<h3>dotnew new console</h3>
<p>As you no doubt have guessed, this created the "Hello World!" console app. The key things to note are: The project name matches the directory name (i.e., "helloworld"); the code was build using a template (console application); and the project's dependencies were automatically retrieved by the <strong>dotnet restore</strong> command, which pulls from <a href="https://www.nuget.org/" target="_blank">nuget.org</a>.</p>
<p>If you view the directory, you'll see these files were created:</p>
<pre>
<code class="language-bash">Program.cs
helloworld.csproj
</code></pre><p><strong>Program.cs</strong> is the C# console app code. Go ahead and take a look inside (you already did ... I know ... because you're a developer), and you'll see what's going on. It's all very simple.</p>
<p><strong>Helloworld.csproj</strong> is the MSBuild-compatible project file. In this case there's not much to it. When you create a web service or website, the project file will take on a new level of significance.</p>
<h3>dotnet run</h3>
<p>This command did two things: It built the code, and it ran the newly built code. Whenever you invoke <strong>dotnet run</strong>, it will check to see if the *.csproj file has been altered and will run the <strong>dotnet restore</strong> command. It will also check to see if any source code has been altered and will, behind the scenes, run the <strong>dotnet build</strong> command which—you guessed it—builds the executable. Finally, it will run the executable.</p>
<p>Sort of.</p>
<h2>Where is my executable?</h2>
<p>Oh, it's right there. Just run <strong>which dotnet</strong> and you'll see (on RHEL): </p>
<pre>
<code class="language-bash">/opt/rh/rh-dotnet20/root/usr/bin/dotnet</code></pre><p>That's your executable.</p>
<p>Sort of.</p>
<p>When you create a dotnet application, you're creating an assembly … a library … yes, you're creating a DLL. If you want to see what is created by the <strong>dotnet build</strong> command, take a peek at <strong>bin/Debug/netcoreapp2.0/</strong>. You'll see <strong>helloworld.dll</strong>, some JSON configuration files, and a <strong>helloworld.pdb</strong> (debug database) file. You can look at the JSON files to get some idea as to what they do (you already did … I know … because you're a developer).</p>
<p>When you run <strong>dotnet run</strong>, the process that runs is dotnet. That process, in turn, invokes your DLL file and it becomes your application.</p>
<h2>It's portable</h2>
<p>Here's where .NET Core really starts to depart from the Windows-only .NET Framework: The DLL you just created will run on any system that has .NET Core installed, whether it be Linux, Windows, or MacOS. It's portable. In fact, it is literally called a "portable application."</p>
<h2>Forever alone</h2>
<p>What if you want to distribute an application and don't want to ask the user to install .NET Core on their machine? (Asking that is sort of rude, right?) Again, .NET Core has the answer: the standalone application.</p>
<p>Creating a standalone application means you can distribute the application to any system and it will run, <em>without the need to have .NET Core installed</em>. This means a faster and easier installation. It also means you can have multiple applications running different versions of .NET Core on the same system. It also seems like it would be useful for, say, running a microservice inside a Linux container. Hmmm…</p>
<h2>What's the catch?</h2>
<p>Okay, there is a catch. For now. When you create a standalone application using the <strong>dotnet publish</strong> command, your DLL is placed into the target directory along with all of the .NET bits necessary to run your DLL. That is, you may see 50 files in the directory. This is going to change soon. An already-running-in-the-lab initiative, .NET Native, will soon be introduced with a future release of .NET Core. This will build one executable with all the bits included. It's just like when you are compiling in the Go language, where you specify the target platform and you get one executable; .NET will do that as well.</p>
<p>You do need to build once for each target, which only makes sense. You simply include a <a href="https://docs.microsoft.com/en-us/dotnet/core/rid-catalog" target="_blank">runtime identifier</a> and build the code, like this example, which builds the release version for RHEL 7.x on a 64-bit processor:</p>
<pre>
<code class="language-bash">dotnet publish -c Release -r rhel.7-x64</code></pre><h2>Web services, websites, and more</h2>
<p>So much more is included with the .NET Core templates, including support for F# and Visual Basic. To get a starting list of available templates that are built into .NET Core, use the command <strong>dotnet new --help</strong>.</p>
<p>Hint: .NET Core templates can be created by third parties. To get an idea of some of these third-party templates, check out <a href="https://github.com/dotnet/templating/wiki/Available-templates-for-dotnet-new" target="_blank">these templates</a>, then let your mind start to wander…</p>
<p>Like most command-line utilities, contextual help is always at hand by using the <strong>--help</strong> command switch. Now that you've been introduced to .NET Core on Linux, the help function and a good web search engine are all you need to get rolling.</p>
<h2>Other resources</h2>
<p>Ready to learn more about .NET Core on Linux? Check out these resources:</p>
<ul><li><a href="https://developers.redhat.com/topics/dotnet/" target="_blank">Redhatloves.net</a></li>
<li><a href="https://www.microsoft.com/net" target="_blank">Dot.net</a></li>
<li><a href="https://live.asp.net/" target="_blank">Live.asp.net</a></li>
<li><em><a href="https://developers.redhat.com/promotions/dot-net-core/" target="_blank">Transitioning to .NET Core on Red Hat Enterprise Linux</a></em></li>
</ul>