<p>One way to make Python applications installable on Debian-based operating systems (such as Debian or <a href="https://opensource.com/article/19/12/pantheon-linux-desktop">Elementary OS</a>) is by using the <a href="https://dh-virtualenv.readthedocs.io/en/latest/" target="_blank">dh_virtualenv</a> tool. It builds a <strong>.deb</strong> package that wraps a Python virtual environment around an application and deploys it upon installing.</p>
<p>In this article, I will explain how to use it with the example of building a package containing the <a href="https://opensource.com/article/19/8/getting-started-httpie">HTTPie</a> tool to test HTTP APIs from the command line without having to activate a virtual environment.</p>
<h2>Packaging with dh_virtualenv</h2>
<p>First, you need to install the tools that dh_virtualenv needs. dh_virtualenv's <a href="https://dh-virtualenv.readthedocs.io/en/1.1/tutorial.html" target="_blank">documentation</a> provides all of the installation options. On my Debian-based system, I entered:</p>
<pre><code class="language-bash">apt-get install dh-virtualenv devscripts</code></pre><p>While the <a href="http://man.he.net/man1/devscripts" target="_blank">devscripts</a> package is not required, it will simplify doing the subsequent operations.</p>
<p>Now, create a directory to keep the sources. Since this is a local, unofficial, packaging of HTTPie, I called it <strong>myhttp</strong>. Next, let's create some files inside <strong>myhttp</strong> to provide metadata to the Debian build system.</p>
<p>First, create the <strong>debian/control</strong> file:</p>
<pre><code class="language-text">Source: myhttp
Section: python
Priority: extra
Maintainer: Jan Doe &lt;jandoe@example.org&gt;
Build-Depends: debhelper (&gt;= 9), python3.7, dh-virtualenv (&gt;= 0.8)
Standards-Version: 3.9.5

Package: myhttp
Architecture: any
Pre-Depends: dpkg (&gt;= 1.16.1), python3.7, ${misc:Pre-Depends}
Depends: ${misc:Depends}
Description: http client
 Useful for doing stuff</code></pre><p>So what is all this information about? As the <a href="https://www.debian.org/doc/manuals/maint-guide/dreq.en.html#control">Debian documentation</a> puts it:</p>
<blockquote><p>"Lines 1–7 are the control information for the source package. Lines 9–13 are the control information for the binary package."</p>
</blockquote>
<p>Here's my take:</p>
<ul><li>the <strong>section</strong> value is mostly meaningless for our case, but needs to be there. It's meaningful to provide information to the guided UI installer, which is not relevant for this package.</li>
<li>The extra <strong>Priority</strong> value is the right priority for 3rd party packages like this one.</li>
<li>It is highly recommended to put real contact details in the <strong>Maintainer</strong> field. It does not have to be your personal e-mail, though -- "Infrastructure Team &lt;<a href="mailto:infra-team-list@company.example.com">infra-team-list@company.example.com</a>&gt;", for example, if the package is maintained by the team and you would like issues to be sent to the team's mail alias.</li>
<li>The <strong>build-depends</strong> field indicates that you need debhelper, python, and dh-virtualenv to build the package: the package build process will make sure those dependencies are installed at package build time.</li>
<li>The <strong>standards version</strong> is mostly for human consumption. It indicates which guide you are following. This guide is based on the official documentation of dh-virtualenv, which is based on the 3.9.5 guide from Debian. It is almost always the best choice to name the binary package and the source package the same.</li>
<li>The <strong>Architecture</strong> field should be <strong>Any</strong> because a virtual environment might include some architecture-specific files: otherwise, the field would be better chosen as <strong>all</strong>.</li>
<li>Keep the <strong>pre-depends</strong> list as-is: pre-depends is a pretty strict form of dependencies, and it is rare that you need anything more than the minimum suggested here. The dependencies are usually calculated accurately by the build system, so there is no reason to specify them manually.</li>
<li>If your package is mostly for internal use, then the <strong>Description</strong> might only specify minimal information and a link to the company wiki; otherwise, more details might be useful.</li>
</ul><p>Then create the <strong>debian/compat</strong> file, which <a href="https://www.debian.org/doc/manuals/maint-guide/dother.en.html#compat">exists mostly for historical purposes</a>:</p>
<pre><code class="language-bash">$ echo "9" &gt; debian/compat</code></pre><p>Next, create the changelog to tell package users what has changed since the last release. The easiest way is to use <strong>dch --create</strong> to create a template and then fill in the values.</p>
<p>Filled in, it looks like:</p>
<pre><code class="language-text">myhttp (2.0.0-1) stable; urgency=medium

  * Initial release.

 -- Jan Doe &lt;jandoe@example.org&gt;  Fri, 27 Mar 2020 01:09:22 +0000</code></pre><p>Now you need to tell the tool to install HTTPie, but which version?</p>
<p>Create a <strong>requirements.in</strong> file that has loose versions:</p>
<pre><code class="language-text">httpie</code></pre><p>In general, the loose requirements file will only contain direct dependencies of your project and will specify minimum versions if needed. It is not always necessary to specify the minimum versions: the tools are usually biased towards tightening the dependencies towards "latest version possible". In the case where your Debian package corresponds to one internal Python package, a common case in internal applications, the loose requirements file will look similar: just one line with the name of the package.</p>
<p>Then use <strong>pip-compile</strong> (which is available by installing the PyPI package <strong>pip-tools</strong>):</p>
<pre><code class="language-bash">$ pip-compile requirements.in &gt; requirements.txt</code></pre><p>This will produce a strict dependency file called <strong>requirements.txt</strong>:</p>
<pre><code class="language-text">#
# This file is autogenerated by pip-compile
# To update, run:
#
#    pip-compile requirements.in
#
certifi==2019.11.28       # via requests
chardet==3.0.4            # via requests
httpie==2.0.0             # via -r requirements.in
idna==2.9                 # via requests
pygments==2.6.1           # via httpie
requests==2.23.0          # via httpie
urllib3==1.25.8           # via requests</code></pre><p>Finally, write a <strong>debian/rules</strong> file for creating the package. Since dh_virtualenv does all the hard work, the rules file is simple:</p>
<pre><code class="language-text">#!/usr/bin/make -f

%:
	dh $@ --with python-virtualenv --python /usr/bin/python3.7</code></pre><p>Be sure to specify the Python interpreter. By default, it will use the interpreter in <strong>/usr/bin/python</strong>, which is Python 2, but you should use a <a href="https://opensource.com/article/19/11/end-of-life-python-2">supported version of Python</a>.</p>
<p>The writing is finished; all that's left is to build the package:</p>
<pre><code class="language-bash">$ debuild -b -us -uc</code></pre><p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Linux resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux commands cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Free online course: RHEL Technical Overview</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux networking cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">SELinux cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux common commands cheat sheet</a></li>
<li><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">What are Linux containers?</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Our latest Linux articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>This will produce a file in the parent directory with a name like <strong>myhttp_2.0.0-1_amd64.deb</strong>. This file can be installed on any compatible operating system.</p>
<p>In general, it's best to build Debian packages that are intended for a specific platform, such as Debian 10.0, on the same platform.</p>
<p>You can store this Debian package in a repository and install it on all relevant systems with, for example, <a href="https://opensource.com/resources/what-ansible">Ansible</a>.</p>
<h2 id="conclusion">Conclusion</h2>
<p>Packaging applications for Debian-based operating systems is a multi-step process. Using dh_virtualenv will make the process straightforward.</p>
