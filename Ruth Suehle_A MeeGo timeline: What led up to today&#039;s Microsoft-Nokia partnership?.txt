<p>Today the landscape changed for open source and smartphones when Microsoft and Nokia announced a partnership making Windows Phone 7 the platform for Nokia smartphones. For those who don't follow the mobile device industry (and perhaps just learned the word "MeeGo" this morning), or for those who are just trying to keep track, here's a timeline of what happened leading up to today's announcement:</p>
<p><strong>Nov. 2005:</strong> First version of Maemo platform, based on Debian, released by Nokia for smartphones and tablets</p>
<p><strong> July 2007:</strong> Intel launched Moblin.org, short for "mobile Linux," for an operating system and stack for mobile devices</p>
<p><strong> April 2009:</strong> Intel turned Moblin over to the Linux Foundation</p>
<p><strong>Feb. 2010:</strong> Intel and Nokia hold press conference at Mobile World Congress to say Maemo and Moblin would merge into MeeGo</p>
<p><strong>April 2010: </strong><a href="http://www.techradar.com/news/computing-components/processors/intel-meego-exists-because-microsoft-let-us-down-684665#ixzz1DfNObv9H">James Reinders of Intel explains the project</a>, "Microsoft hasn't been quite as aggressive as we might have hoped at supporting Atom, especially in the embedded space and that's why we came up with our platform Moblin - which is now MeeGo."</p>
<p><strong>May 2010:</strong> MeeGo 1.0 released</p>
<p><strong>Sept. 2010: </strong>Stephen Elop, formerly of Microsoft (and before that Juniper Networks and Adobe) joins Nokia as president and CEO</p>
<p><strong>Sept. 2010:</strong> Anssi Vanjoki, EVP and GM of Mobile Solutions at Nokia, <a href="http://www.engadget.com/2010/09/23/anssi-vanjoki-on-quitting-nokia-i-didn-t-become-the-ceo-it-is/">announces he's leaving the company</a></p>
<p><strong> Oct. 2010:</strong> Ari Jaaksi, VP of Nokia's MeeGo Devices, resigns</p>
<p><strong>Feb. 9, 2011:</strong> Nokia kills plan for MeeGo smartphone; speech leaks in which Chief Executive <a href="http://www.guardian.co.uk/technology/blog/2011/feb/09/nokia-burning-platform-memo-elop">Stephen Elop compares Nokia to a man considering jumping off a burning oil platform</a></p>
<p><strong>Feb. 10, 2011:</strong> <a href="http://www.engadget.com/2011/02/11/nokia-execs-given-the-boot-in-microsoft-centered-reorganization/">Nokia creates separate divisions</a> for Smart Devices and Mobile Phones; Mobile Solutions head Alberto Torres steps down from management</p>
<p><strong>Feb. 11, 2011 </strong></p>
<ul><li><a href="http://conversations.nokia.com/2011/02/11/open-letter-from-ceo-stephen-elop-nokia-and-ceo-steve-ballmer-microsoft/">Microsoft and Nokia announce partnership</a> making Windows Phone 7 the platform for Nokia smartphones </li>
<li>Intel says that they see MeeGo as more than just a phone OS, will continue to support it; "While we are disappointed with Nokia’s decision, <a href="http://blog.laptopmag.com/intel-were-not-blinking-on-meego">Intel is not blinking on MeeGo.</a> We remain committed and welcome Nokia’s continued contribution to MeeGo open source." </li>
<li>Reports come that 1,000+ employees may have <a href="http://thenextweb.com/eu/2011/02/11/more-than-a-thousand-employees-walk-out-of-nokia-offices/">walked out of Nokia offices</a> in Finland</li>
<p> (Nokia states that they are simply taking advantage of flextime benefits after a major announcement)</p></ul><p>Mobile World Congress starts in Barcelona in three days. Reports indicate that:</p>
<ul><li>Elop and Steve Ballmer will present on the details of the new Microsoft/Nokia deal</li>
<li>Nokia will talk about the future of MeeGo</li>
<li>Intel will demonstrate where MeeGo is being used in segments outside of smartphones and on MeeGo's ecosystem of support</li>
</ul><p><br />Additions or corrections? Leave comments below.</p>
