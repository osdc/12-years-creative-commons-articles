<p>When you write source code, you're writing code for humans to read. Computers can't execute source code until the code is compiled into <em>machine language</em>, a generic term referring to any number of languages required by a specific machine. Normally, if you compile code on Linux, it runs on Linux, and if you compile code on Windows, it runs on Windows, and so on. However, Java is different. It doesn't target an actual machine. It targets something called the Java Virtual Machine (JVM), and so it can run on any machine.</p>
<p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">More on Java</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://developers.redhat.com/topics/enterprise-java/?intcmp=7013a000002Cxq6AAC" data-analytics-category="resource list" data-analytics-text="What is enterprise Java programming?">What is enterprise Java programming?</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/engage/open-source-alternative-s-202104291240?intcmp=7013a000002Cxq6AAC" data-analytics-category="resource list" data-analytics-text="An open source alternative to Oracle JDK">An open source alternative to Oracle JDK</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/java-cheat-sheet?intcmp=7013a000002Cxq6AAC" data-analytics-category="resource list" data-analytics-text="Java cheat sheet">Java cheat sheet</a></div>
              <div class="field__item"><a href="https://developers.redhat.com/products/openjdk/overview?intcmp=7013a000002Cxq6AAC" data-analytics-category="resource list" data-analytics-text="Red Hat build of OpenJDK">Red Hat build of OpenJDK</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/services/training/do092-developing-cloud-native-applications-microservices-architectures?intcmp=7013a000002Cxq6AAC" data-analytics-category="resource list" data-analytics-text="Free online course: Developing cloud-native applications with microservices">Free online course: Developing cloud-native applications with microservices</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Register for your free Red Hat account">Register for your free Red Hat account</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/java?intcmp=7013a000002Cxq6AAC" data-analytics-category="resource list" data-analytics-text="Fresh Java articles">Fresh Java articles</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<p>Java source code gets compiled into bytecode which is run by a JVM installed on a computer. The JVM is an execution engine, but it's not one you usually interact with directly. It runs quietly, processing Java bytecode. Most people don't need to think or even know about the JVM, but it can be useful to understand how the JVM works so you can debug and optimize Java code. For example:</p>
<ul><li>
<p>In the production environment, you might find a deployed application needs a performance boost.</p>
</li>
<li>
<p>If something goes wrong in an application you've written, both the developer and end-user have options to debug the problem.</p>
</li>
<li>
<p>Should you want to know the details of the Java Development Kit (JDK) being used to develop or run a Java application, you can get those details by querying the JVM.</p>
</li>
</ul><p>This article introduces some basic JVM parameters to help in these scenarios…</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-03/java-jvm-parameters.jpg" width="391" height="271" alt="Jvm parameters" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Jayashree Huttanagoudar CC BY-SA 4.0)</p>
</div>
      
  </article><h2 id="whats-the-difference-between-a-jvm-jdk-and-jre">What's the difference between a JVM, JDK, and JRE?</h2>
<p>Java has a lot of J-acronyms, including JVM, JDK, and JRE.</p>
<ul><li>
<p>A Java Developer Kit (JDK) is accessed by programmers who need development libraries to use in their code.</p>
</li>
<li>
<p>The Java Runtime Environment (JRE) is employed by people who want to run a Java application.</p>
</li>
<li>
<p>The Java Virtual Machine (JVM) is the component that runs Java bytecode.</p>
</li>
</ul><p>The JDK contains both a JRE and a JVM, but some Java distributions provide an alternate download containing a JRE (including a JVM).</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-03/jdk.jpg" width="331" height="201" alt="JDK" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Jayashree Huttanagoudar CC BY-SA 4.0)</p>
</div>
      
  </article><p>Java is open source, so different companies build and distribute JDKs. You can install more than one on your system, which can be helpful when you're working on or using different Java projects, some of which might use an old JDK.</p>
<p>To list the JDKs on your Linux system, you can use the alternatives command:</p>
<pre>
<code class="language-java">$ alternatives --config java
There are 2 programs that provide java.
Selection Command
-----------------------------------------------
*+ 1 java-11-openjdk.x86_64 (/usr/lib/jvm/java-11-openjdk-11.0.13.0.8-2.fc35.x86_64/bin/java)
2 java-1.8.0-openjdk.x86_64 (/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.312.b07-2.fc35.x86_64/jre/bin/java)

Enter to keep the current selection[+], or type selection number:</code></pre><p>To switch between available JDKs, run the command again:</p>
<pre>
<code class="language-java">$ sudo alternatives --config java</code></pre><p>Another option is to use <a href="https://opensource.com/%5Bhttps%3A//opensource.com/article/22/3/manage-java-versions-sdkman%5D%28https%3A//opensource.com/article/22/3/manage-java-versions-sdkman%29">SDKMan</a>, which helps you download, update, and manage the JDKs on your system.</p>
<h2 id="what-is-jvm-tuning">What is JVM tuning?</h2>
<p>Tuning a JVM is the process of adjusting JVM parameters to improve the performance of the Java application. It also helps to diagnose application failure.</p>
<p>In general, it's important to consider these points before tuning:</p>
<ul><li>
<p><strong>Cost</strong> : Sometimes, improving the hardware running your code can improve an application's performance. That might seem like a "cheat" but consider how much time you're willing to spend tuning the JVM parameters. Sometimes, an application requires more memory to perform as desired, and no amount of software hacking will change that.</p>
</li>
<li>
<p><strong>Desired Outcome</strong>: Stability is more important than performance in the long run. If your tuning affects the stability, it's probably better to wisely choose your tuning parameters.</p>
</li>
<li>
<p><strong>Underlying issues</strong> : Sometimes, the issue could be an underlying issue with the host operating system. Before tuning the JVM, ensure that the JVM's platform is working as expected.</p>
</li>
<li>
<p><strong>Memory leaks</strong>: If you find yourself using Garbage Collection (GC) tuning parameters, there are likely memory leaks that need to get fixed in the application code.</p>
</li>
</ul><h2 id="types-of-jvm-parameters">Types of JVM Parameters</h2>
<p>JVM parameters are grouped under three categories: Standard options, Non-standard, and Advanced.</p>
<h3 id="standard-options">Standard options</h3>
<p>All JVM implementations support standard options. Run the 'java' command in a terminal to see a list of standard options.</p>
<pre>
<code class="language-java">$ java
Usage: java [options] &lt;mainclass&gt; [args...]
       	(to execute a class)
   or  java [options] -jar &lt;jarfile&gt; [args...]
       	(to execute a jar file)

 where options include:

	-cp &lt;class search path of directories and zip/jar files&gt;
	-classpath &lt;class search path of directories and zip/jar files&gt;
	--class-path &lt;class search path of directories and zip/jar files&gt;
         	A : separated list of directories, JAR archives,
          	and ZIP archives to search for class files.
	--enable-preview
         	allow classes to depend on preview features of this release

To specify an argument for a long option, you can use --&lt;name&gt;=&lt;value&gt; or
--&lt;name&gt; &lt;value&gt;.</code></pre><p>These are all standard options included with any JVM, and you can safely use them as you use any <a href="https://opensource.com/%5Bhttps%3A//opensource.com/article/21/8/linux-terminal%5D%28https%3A//opensource.com/article/21/8/linux-terminal%29">command-line option</a>. For example, to validate command options for configuration, and create a VM and load a main class without executing the main class, use:</p>
<pre>
<code class="language-java">$ java --dry-run &lt;classfile&gt;</code></pre><h3 id="non-standard-options">Non-standard options</h3>
<p>Non-standard options start with <code>-X</code>. These are for general purpose use and are specific to a particular implementation of JVM. To list these options:<br />
 </p>
<pre>
<code class="language-java">$ java -X
-Xbatch disable background compilation
-Xbootclasspath/a:&lt;directories and zip/jar files separated by :&gt;
append to end of bootstrap class path
-Xinternalversion
displays more detailed JVM version information than the
-version option
-Xloggc:&lt;file&gt; log GC status to a file with time stamps
[...]</code></pre><p>These extra options are subject to change without notice and are not supported by all JVM implementations.</p>
<p>A JVM built by Microsoft may have different options than one built by Red Hat, and so on.</p>
<p>To get detailed JVM version information, use the following option:<br />
 </p>
<pre>
<code class="language-java">$ java -Xinternalversion --version
OpenJDK 64-Bit Server VM (11.0.13+8) for linux-amd64 JRE (11.0.13+8), built on Nov 8 2021 00:00:00 by "mockbuild" with gcc 11.2.1 20210728 (Red Hat 11.2.1-1)</code></pre><p>To get the property setting use:</p>
<pre>
<code class="language-java">$ java -XshowSettings:properties --version</code></pre><h3 id="advanced-options">Advanced options</h3>
<p>These options are not for casual use and are used for tuning the specific areas of the Hotspot VM. These options are subject to change, and there is no guarantee that all JVM implementations will support it.</p>
<p>These options start with -XX. To list these options, use the following command:</p>
<pre>
<code class="language-java">$ java -XX:+UnlockDiagnosticVMOptions -XX:+PrintFlagsFinal -version</code></pre><p>For example, to trace the class loading then use the below command:</p>
<pre>
<code class="language-java">$ java -XX:+TraceClassLoading Hello</code></pre><p>The Hello.java has:</p>
<pre>
<code class="language-java">$ cat Hello. java
public class Hello {
  public static void main(String[] args) {
    System.out.println("Inside Hello World!");
  }
}
 </code></pre><p>Another common problem you might face is OOM (Out Of Memory) errors, which can happen without much debug information. To solve such a problem, you might use the debug option -XX:+HeapDumpOnOutOfMemoryError, which creates a .hprof file with debug information.<br />
 </p>
<pre>
<code>$ cat TestClass. java
import java.util.ArrayList;
import java.util.List;

public class TestClass {
  public static void main(String[] args) {
    List&lt;Object&gt; list = new ArrayList&lt;Object&gt;();
    for (int i = 0; i &lt; 1000; i++) {
      list.add(new char[1000000]);
    }
  }
}
$ Javac TestClass.java
$ java -XX:+HeapDumpOnOutOfMemoryError -Xms10m -Xmx1g TestClass
java.lang.OutOfMemoryError: java heap space
Dumping heap to java_pid444496.hprof ...
Heap dump file created [1018925828 bytes in 1.442 secs]
Exception in thread "main" java.lang.OutOfMemoryError: java heap space
at TestClass.main(TestClass.Java:8)</code></pre><p><a href="https://opensource.com/%5Bhttps%3A//docs.oracle.com/javase/7/docs/technotes/tools/share/jhat.html%5D%28https%3A//docs.oracle.com/javase/7/docs/technotes/tools/share/jhat.html%29">There are tools</a> to look at this .hprof file to understand what went wrong.</p>
<h2 id="conclusion">Conclusion</h2>
<p>By understanding and using JVM and JVM parameters, both developers and end users can diagnose failures and improve the performance of a Java application. The next time you're working with Java, take a moment to look at the options available to you.</p>
