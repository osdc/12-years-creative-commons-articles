<p>In prior years, this annual series covered individual apps. This year, we are looking at all-in-one solutions in addition to strategies to help in 2021. Welcome to day 15 of 21 Days of Productivity in 2021.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on productivity</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/downloads/collaboration-tools-ebook">5 open source collaboration tools</a></li>
<li><a href="https://opensource.com/downloads/organization-tools">6 open source tools for staying organized</a></li>
<li><a href="https://opensource.com/downloads/desktop-tools">7 open source desktop tools</a></li>
<li><a href="https://opensource.com/tags/tools">The latest on open source tools</a></li>
</ul></div>
</div>
</div>
</div>
<p>Staying productive also means (to some extent) being organized enough to find my notes and reference them on demand. This is a challenge not just for myself but for a lot of people I talk to.</p>
<p>Over the years, I have used either individually or some combination of digital notes in an app, paper notes, sticky notes, digital sticky notes, word processor documents, plain text files, and a bunch of other formats I am forgetting. That makes not only finding notes difficult but knowing where to put them an even bigger challenge.</p>
<p><article class="align-center media media--type-image media--view-mode-full"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/day15-image1.jpg" width="800" height="600" alt="Stacks of paper notes on a desk" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter">Piles of notes Photo by Jessica Cherry 2021 <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC-BY</a></p>
</div>
      
  </article></p>
<p>And then there is the most important thing about taking notes: A note has no value at all if you can't find it later. Knowing that the note with the information you need exists <em>somewhere</em> in one of the places you keep notes isn't helpful at all.</p>
<p>How did I fix that for myself? It was, as they say, a process, and I hope it is a process that works for others as well.</p>
<p>I started by looking at the kinds of notes I was taking. Do different subjects need to be stored in different ways? Since I handwrite notes for my podcasts and use plain text notes for almost everything else, I needed two different ways to maintain them. For the handwritten notes, I put them all in a binder that I could reference easily.</p>
<p><article class="align-center media media--type-image media--view-mode-full"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/day15-image2.png" width="600" height="800" alt="Man holding a binder full of notes" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter">Over three years of notes</p>
</div>
      
  </article></p>
<p>To store my digital notes, I needed to pull them all into one place. The tool needed to be accessible from multiple devices, have a useful search function, and be able to export or share my notes. I chose <a href="https://joplinapp.org/" target="_blank">Joplin</a> after trying many, many different options. Joplin lets me write notes in markdown, has a pretty good search function, has applications for all the OSs (including mobile), and supports several different ways to sync between devices. As a bonus, it has folders <em>and</em> tags, so I can group my notes together in ways that make sense to me.</p>
<p><article class="align-center media media--type-image media--view-mode-full"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/day15-image3.png" width="800" height="463" alt="Organized Joplin notes management page" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter">My Joplin</p>
</div>
      
  </article></p>
<p>It took me some time to get everything where I wanted it, but in the end, it really was worth it. Now I can find the notes I take and not have them scattered across my office, different machines, and various services.</p>
