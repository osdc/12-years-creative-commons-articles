<p>Like most developers, I search and read source code all day long. Personally, I've never gotten used to integrated development environments (IDEs), and for years, I mainly used <strong>grep</strong> and copy/pasted file names to open Vi(m).</p>
<p>Eventually, I came up with this script, slowly refining it as needed.</p>
<p>Its dependencies are <a href="https://www.vim.org/" target="_blank">Vim</a> and <a href="https://linux.die.net/man/1/rlwrap" target="_blank">rlwrap</a>, and it is open source under the Apache 2.0 license. To use the script,<a href="https://opensource.com/article/17/6/set-path-linux"> put it in your PATH</a>, and run it inside a directory of text files with:</p>
<pre><code class="language-text">grepgitvi &lt;grep options&gt; &lt;grep/vim search pattern&gt;</code></pre><p>It will return a numbered list of search results, prompt you for the number of the result you want to use, and open Vim with that result. After you exit Vim, it will show the list again in a loop until you enter anything other than a result number. You can also use the Up and Down arrow keys to select a file; this makes it easier (for me) to find which results I've already looked at.</p>
<p>It's simple and primitive compared to modern IDEs, or even to more sophisticated uses of Vim, but that's what does the job for me.</p>
<h2 id="the-script">The script</h2>
<pre><code class="language-bash">#!/bin/bash

# grepgitvi - grep source files, interactively open vim on results
# Doesn't really have to do much with git, other than ignoring .git
#
# Copyright Yedidyah Bar David 2019
#
# SPDX-License-Identifier: Apache-2.0
#
# Requires vim and rlwrap
# 
# Usage: grepgitvi &lt;grep options&gt; &lt;grep/vim pattern&gt;
#

TMPD=$(mktemp -d /tmp/grepgitvi.XXXXXX)
UNCOLORED=${TMPD}/uncolored
COLORED=${TMPD}/colored

RLHIST=${TMPD}/readline-history

[ -z "${DIRS}" ] &amp;&amp; DIRS=.

cleanup() {
	rm -rf "${TMPD}"
}

trap cleanup 0

find ${DIRS} -iname .git -prune -o \! -iname "*.min.css*" -type f -print0 &gt; ${TMPD}/allfiles

cat ${TMPD}/allfiles | xargs -0 grep --color=always -n -H "$@" &gt; $COLORED
cat ${TMPD}/allfiles | xargs -0 grep -n -H "$@" &gt; $UNCOLORED

max=`cat $UNCOLORED | wc -l`
pat="${@: -1}"

inp=''
while true; do
	echo "============================ grep results ==============================="
	cat $COLORED | nl
	echo "============================ grep results ==============================="
	prompt="Enter a number between 1 and $max or anything else to quit: "
	inp=$(rlwrap -H $RLHIST bash -c "read -p "$prompt" inp; echo \$inp")
	if ! echo "$inp" | grep -q '^[0-9][0-9]*$' || [ "$inp" -gt "$max" ]; then
		break
	fi

	filename=$(cat $UNCOLORED | awk -F: "NR==$inp"' {print $1}')
	linenum=$(cat $UNCOLORED | awk -F: "NR==$inp"' {print $2-1}')
	vim +:"$linenum" +"norm zz" +/"${pat}" "$filename"
done</code></pre><p> </p>
