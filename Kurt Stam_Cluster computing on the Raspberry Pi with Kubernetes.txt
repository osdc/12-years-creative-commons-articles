<p>
  Ever wanted to make your very own cloud? Now you can! All it takes is some cheap open source hardware and open source software. For about $200, I was able to set up four <a href="https://www.raspberrypi.org/products/raspberry-pi-2-model-b/" target="_blank">Raspberry Pi 2</a>s with the <a href="http://kubernetes.io/" target="_blank">Kubernetes</a> cloud operating system using <a href="http://fabric8.io/" target="_blank">Fabric8</a>.
</p>
<div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on Raspberry Pi</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/resources/what-raspberry-pi?src=raspberry_pi_resource_menu1&amp;intcmp=701f2000000h4RcAAI">What is Raspberry Pi?</a></li>
<li><a href="https://opensource.com/downloads/raspberry-pi-guide?src=raspberry_pi_resource_menu2&amp;intcmp=701f2000000h4RcAAI">eBook: Guide to Raspberry Pi</a></li>
<li><a href="https://opensource.com/downloads/getting-started-raspberry-pi-cheat-sheet?src=raspberry_pi_resource_menu3&amp;intcmp=701f2000000h4RcAAI">Getting started with Raspberry Pi cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/kubernetes-raspberry-pi?src=raspberry_pi_resource_menu6&amp;intcmp=701f2000000h4RcAAI">eBook: Running Kubernetes on your Raspberry Pi</a></li>
<li><a href="https://www.redhat.com/en/resources/data-intensive-applications-hybrid-cloud-blueprint-detail?src=raspberry_pi_resource_menu7&amp;intcmp=701f2000000h4RcAAI">Whitepaper: Data-intensive intelligent applications in a hybrid cloud blueprint</a></li>
<li><a href="https://www.redhat.com/en/topics/edge-computing?src=raspberry_pi_resource_menu5&amp;intcmp=701f2000000h4RcAAI">Understanding edge computing</a></li>
<li><a href="https://opensource.com/tags/raspberry-pi?src=raspberry_pi_resource_menu4&amp;intcmp=701f2000000h4RcAAI">Our latest on Raspberry Pi </a></li>
</ul></div>
</div>
</div>
</div>
<p>
  Fabric8 is an open source DevOps and integration platform that works out of the box on any Kubernetes or <a href="https://www.openshift.com/" target="_blank">OpenShift</a> environment and provides continuous delivery, management, ChatOps, and a Chaos Monkey. In short, it's the best framework there is to develop and manage your microservices on. (Full disclosure: I work on the Red Hat Fabric8 project.)
</p>
<p>
  Never before was there a better match between a software architecture and the hardware it runs on. The Pis are completely silent (no fan) and have a quite powerful quad-core CPU, while the microservices architecture makes each process relatively small so it can run on the 1GB RAM memory constraint of each node. You can simply add more Pis if you need more computing power. Lastly, it's <em>just plain fun</em> to play with the hardware instead of logging into a remote virtual machine at Amazon.
</p>
<h2>1. Required hardware</h2>
<p>
  To build this four-Pi setup I used:
</p>
<ul><li>4 Raspberry Pi 2s</li>
<li>4 16GB MicroSD cards (Class 10)</li>
<li>1 60W power supply with USB outlets</li>
<li>4 short USB to Micro USB cables (for powering the Pis)</li>
<li>4 short Cat 5 network cables</li>
<li>1 longer Cat 5 network cable to hook into your network</li>
<li>1 network hub (Mine is an old five-port, 10/100MBps I dusted off)</li>
<li>LEGOs (Trust me, it feels good to build your own!)</li>
</ul><p>
  It's important to get <strong>Class 10 MicroSD cards</strong>, as this is the one component that greatly affects the speed of the system. You can easily get away with a smaller 20W power supply for four Pis, but I'm planning on adding more later so I got a bigger one. The network port on the Pi is 100MBps, so you can use your old hub.
</p>
<h2>2. Flashing the MicroSD cards</h2>
<p>
  Now we need to get the operating system onto the MicroSD Cards. I'm using a <a href="https://hub.docker.com/u/hypriot/" target="_blank">Hypriot image</a>. I <a href="http://kurtstam.blogspot.com/2015/03/pi-oneering-on-raspberry-pi-2-part-1.html" target="_blank">tested a bunch of others</a>, including Pidora, Redsleeve/CentOS, and Arch Linux, but concluded that Hypriot is the best for me at this point (mostly because of the native Docker support and the image being small). You can download the latest Hypriot image <a href="http://blog.hypriot.com/downloads/" target="_blank">here</a>.
</p>
<ol><li>Insert the memory card in the card reader. Do not mount it. If it automounts, unmount the card. Then write the image to the card. On my OS X machine, the command for doing so is:<br /><pre>sudo dd bs=1m if=hypriot-rpi-20151103-224349.img of=/dev/diskn<pre></pre></pre></li>

Replace 'n' with the number of disk found using the disk utility: On Mac OS X 10.8.x Mountain Lion, "Verify Disk" (before unmounting) will display the BSD name as "/dev/disk2s1" or similar. Please double check or else you maybe formatting a disk you did not intend to format!

  <li>Wait. This may take 10 minutes or so.</li>
  <li>Do this for all four MicroSD cards. You can work on your LEGO casing while waiting.</li>
</ol><h2>3. Power up and add DNS</h2>

<p>
  You can now power up the first Pi. By default, use <strong>root/hypriot</strong> to log in. You will need to set up DNS for each of the Pis. If you don't want to hook up a monitor and keyboard to figure out its IP and MAC address, then you can <a href="https://kurtstam.github.io/2015/07/14/Turn-your-Raspberry-Pi-2-into-a-Hotspot.html" target="_blank">use nmap</a>. If you have a DNS service, provision it there. Otherwise, name the Pis in <strong>/etc/hosts</strong>. I named my Pis rpi-master, rpi-node-1, rpi-node-2, and rpi-node-3. After a quick reboot of each Pi, you'll see the names reflected in the cmd prompt. Note that <strong>if you skip the DNS step, your Kubernetes cluster will not work</strong>. I added the following to my <strong>/etc/hosts</strong> files on all machines:
</p>

<pre>192.168.1.9     rpi-master
192.168.1.21    rpi-node-1
192.168.1.18    rpi-node-2
192.168.1.23    rpi-node-3</pre><h2>4. Add swap (optional)</h2>
<p>
  I added 1GB of swap space to my rpi-master because it runs a few more Kubernetes components. Some people seem concerned that frequent writes to the MicroSD card will make it fail quickly. I therefore decided to set the <strong>swappiness</strong> such that it only uses the swap as a last resort.
</p>
<pre>dd if=/dev/zero of=/swap/swapfile bs=1M count=1024
mkswap /swap/swapfile
swapon /swap/swapfile</pre><p>
  Now you can check with <strong>top</strong> that you have a 1GB of swap space.
</p>
<table border="0"><tr><td><small>KiB Mem:</small></td>
<td><small>947468 total,</small></td>
<td><small>913096 used,</small></td>
<td><small>34372 free,</small></td>
<td><small>69884 buffers</small></td>
</tr><tr><td><small>KiB Swap:</small></td>
<td><small>1048572 total,</small></td>
<td><small>484 used,</small></td>
<td><small>1048088 free,</small></td>
<td><small>667248 cached</small></td>
</tr></table><p>
  To make the swap permanent between reboots, add
</p>
<pre>/swap/swapfile none swap sw 0 0</pre><p>
  to your <strong>/etc/fstab</strong> file. Finally, in /etc/sysctl.conf, I've set the swappiness to 1:
</p>
<pre>vm.swappiness = 1</pre><p>
  "1" means it will only use swap when RAM use exceeds 99%.
</p>
<h2>
  5. Install Kubernetes<br /></h2>
<p>
  These instructions are to install the plain vanilla Kubernetes from Google. I had some issues compiling OpenShift 3 due to some 64-bit dependencies, and I will get back to that later. I would like go support both. All the code I'm using is checked in under <a href="https://github.com/Project31" target="_blank">https://github.com/Project31</a>. If you want to compile your own Kubernetes binaries, check out <a href="https://kurtstam.github.io/2015/12/04/How-to-Compile-Kubernetes-for-Raspberry-Pi-ARM.html" target="_blank">my blog post</a> about it.
</p>
<h3>5.1. Install Kubernetes master</h3>
<p>
  The Kubernetes master runs the Kubernetes REST API, a scheduler, kubernetes-proxy, and a replication controller. It also uses etcd as a key-value store that is replicated with between other Kubernetes masters. For more details, please see the <a href="http://kubernetes.io/v1.1/docs/design/architecture.html" target="_blank">Kubernetes documentation</a>.
</p>
<p></p><center><br /><img src="https://opensource.com/sites/default/files/kubernetes_master_diagram.png" alt="Diagram of Kubernetes master components" width="520" height="308" /><br /><sup>Kubernetes master components.</sup></center>
<p>
  To install these components onto the master, log in to the master node and run:
</p>
<pre>git clone git@github.com:Project31/kubernetes-installer-rpi.git
cd kubernetes-install-rpi
./build-master.sh</pre><p>Verify the running Docker containers using <strong>docker ps</strong>:</p>
<table border="0"><tr><td><small>CONTAINER ID</small></td>
<td><small>IMAGE</small></td>
<td><small>COMMAND</small></td>
<td><small>CREATED</small></td>
<td><small>STATUS</small></td>
<td><small>NAMES</small></td>
</tr><tr><td><small>d598e486daf5</small></td>
<td><small>hyperkube</small></td>
<td><small>"/hyperkube proxy --m"</small></td>
<td><small>...</small></td>
<td><small>...</small></td>
<td><small>k8s_kube-proxy...</small></td>
</tr><tr><td><small>026c19a67f86</small></td>
<td><small>hyperkube</small></td>
<td><small>"/hyperkube scheduler"</small></td>
<td><small>...</small></td>
<td><small>...</small></td>
<td><small>k8s_kube-scheduler...</small></td>
</tr><tr><td><small>8f615b87cfda</small></td>
<td><small>hyperkube</small></td>
<td><small>"/hyperkube controlle"</small></td>
<td><small>...</small></td>
<td><small>...</small></td>
<td><small>k8s_kube-controller-manager....</small></td>
</tr><tr><td><small>a737d9927c03</small></td>
<td><small>hyperkube</small></td>
<td><small>"/hyperkube apiserver"</small></td>
<td><small>...</small></td>
<td><small>...</small></td>
<td><small>k8s_kube-apiserver....</small></td>
</tr><tr><td><small>0207a21ce18d</small></td>
<td><small>etcd</small></td>
<td><small>"etcd --data-dir=/var"</small></td>
<td><small>...</small></td>
<td><small>...</small></td>
<td><small>k8s_etcd...</small></td>
</tr><tr><td><small>a4174bf7cb98</small></td>
<td><small>.../pause:0.8.0</small></td>
<td><small>"/pause"</small></td>
<td><small>...</small></td>
<td><small>...</small></td>
<td><small>k8s_POD...</small></td>
</tr></table><p>and the now set export <strong>KUBERNETES_MASTER=http:/<b></b>/rpi-master:8080</strong> so your <strong>kubectl</strong> can connect the Kubernetes REST API on the master. Now let take a look at the running pods using <strong>kubectl get</strong> pods</p>
<table border="0"><tr><td><small>NAME</small></td>
<td><small>READY</small></td>
<td><small>STATUS</small></td>
<td><small>RESTARTS</small></td>
<td><small>AGE</small></td>
</tr><tr><td><small>kube-controller-rpi-master</small></td>
<td><small>5/5</small></td>
<td><small>Running</small></td>
<td><small>0</small></td>
<td><small>10s</small></td>
</tr></table><p>
  We can see that the <strong>kube-controller-rpi-master</strong> pod contains runs five Docker containers with the Kubernetes services mentioned above.
</p>
<h3>5.2. Install Kubernetes node</h3>
<p>
  The Kubernetes node only runs the Kubernetes proxy and the pods.
</p>
<p></p><center><br /><img src="https://opensource.com/sites/default/files/kubernetes_clientcomponents_diagram.png" alt="Kubernetes client components diagram" width="520" height="315" /><br /><sup>Kubernetes client components.</sup></center>
<p>
  To install these components on a node, log in to the node and run:
</p>
<pre>git clone git@github.com:Project31/kubernetes-installer-rpi.git
cd kubernetes-install-rpi</pre><p>
  Now edit the <strong>kube-procy.yaml</strong> and set <strong>--master=http:/<b></b>/rpi-master:8080</strong> to your Kubernetes master. Then edit the <strong>kubelet.service</strong> file and set your master's Kubernetes REST endpoint there as well (which in my case is http:/<b></b>/192.168.1.9:8080).
</p>
<p>
  Now you can run the install:
</p>
<pre>./build-worker.sh</pre><p>
  And verify our proxy came up using <strong>docker ps</strong>:
</p>
<table border="0"><tr><td><small>CONTAINER ID</small></td>
<td><small>IMAGE</small></td>
<td><small>COMMAND</small></td>
<td><small>CREATED</small></td>
<td><small>STATUS</small></td>
<td><small>NAMES</small></td>
</tr><tr><td><small>cf4a9a2d7f35</small></td>
<td><small>hyperkube</small></td>
<td><small>"/hyperkube proxy --m"</small></td>
<td><small>40 seconds ago</small></td>
<td><small>Up 37 seconds</small></td>
<td><small>k8s_kube-proxy...</small></td>
</tr><tr><td><small>d9f8f937df4d</small></td>
<td><small>gcr.io/go...</small></td>
<td><small>"/pause"</small></td>
<td><small>43 seconds ago</small></td>
<td><small>Up 40 seconds</small></td>
<td><small>k8s_POD.e4cc..</small></td>
</tr></table><p>
  The proxy is running! Set export <strong>KUBERNETES_MASTER=http:/<b></b>/rpi-master:8080</strong> so your <strong>kubectl</strong> can connect the Kubernetes REST API on the master. Now verify the nodes are all registered:
</p>
<pre>kubectl get nodes</pre><table border="0"><tr><td><small>NAME</small></td>
<td><small>LABELS</small></td>
<td><small>STATUS</small></td>
</tr><tr><td><small>rpi-master</small></td>
<td><small>kubernetes.io/hostname=rpi-master</small></td>
<td><small>Ready</small></td>
</tr><tr><td><small>rpi-node-1</small></td>
<td><small>kubernetes.io/hostname=rpi-node-1</small></td>
<td><small>Ready</small></td>
</tr><tr><td><small>rpi-node-2</small></td>
<td><small>kubernetes.io/hostname=rpi-node-2</small></td>
<td><small>Ready</small></td>
</tr><tr><td><small>rpi-node-3</small></td>
<td><small>kubernetes.io/hostname=rpi-node-3</small></td>
<td><small>Ready</small></td>
</tr></table><p>
  Yay, it worked!
</p>
<h2>6. Open Docker for remote connections</h2>
<p>
  We need to fix up the Docker configuration on the master so that it accepts remote connections and we can deploy something to it. Open the <strong>/etc/default/docker</strong> file for editing and set the <strong>DOCKER_OPTS</strong>:
</p>
<pre>DOCKER_OPTS="-H tcp://192.168.1.9:2375 -H unix:///var/run/docker.sock --storage-driver=overlay -D"</pre><p>
Specify the IP address of machine, or you can user '0.0.0.0' to bind to all interfaces. Now we can remotely push Docker images to the master.
</p>
<h2>7. Deploy a simple service</h2>
<p>
  Let's deploy a simple service and scale to two pods to make sure things are working correctly:
</p>
<pre>kubectl -s http://localhost:8080 run httpd --image=hypriot/rpi-busybox-httpd --port=80
kubectl scale --replicas=2 rc httpd
kubectl get pods -o wide</pre><p>
  We see that even though we executed the command on the master, it started a pod on node-2 and node-3:
</p>
<table border="0"><tr><td><small>NAME</small></td>
<td><small>READY</small></td>
<td><small>STATUS</small></td>
<td><small>RESTARTS</small></td>
<td><small>AGE</small></td>
<td><small>NODE</small></td>
</tr><tr><td><small>httpd-4v1qw</small></td>
<td><small>1/1</small></td>
<td><small>Running</small></td>
<td><small>0</small></td>
<td><small>9m</small></td>
<td><small>rpi-node-2</small></td>
</tr><tr><td><small>httpd-qxcxu</small></td>
<td><small>0/1</small></td>
<td><small>Pending</small></td>
<td><small>0</small></td>
<td><small>16s</small></td>
<td><small>rpi-node-3</small></td>
</tr><tr><td><small>kube-controller-rpi-master</small></td>
<td><small>5/5</small></td>
<td><small>Running</small></td>
<td><small>0</small></td>
<td><small>1d</small></td>
<td><small>rpi-master</small></td>
</tr><tr><td><small>kube-system-rpi-node-1</small></td>
<td><small>1/1</small></td>
<td><small>Running</small></td>
<td><small>0</small></td>
<td><small>33m</small></td>
<td><small>rpi-node-1</small></td>
</tr><tr><td><small>kube-system-rpi-node-2</small></td>
<td><small>1/1</small></td>
<td><small>Running</small></td>
<td><small>0</small></td>
<td><small>43m</small></td>
<td><small>rpi-node-2</small></td>
</tr><tr><td><small>kube-system-rpi-node-3</small></td>
<td><small>1/1</small></td>
<td><small>Running</small></td>
<td><small>0</small></td>
<td><small>53m</small></td>
<td><small>rpi-node-3</small></td>
</tr></table><h2>
  8. Conclusion<br /></h2>
<p>
  First of all, it works! The Kubernetes master may not have much memory left to run services, but the nodes, on the other hand, have almost an entire 1GB available for Docker containers. If you ever need more computing power, you can simply add nodes. The extremely low price of this hardware platform, combined with the Kubernetes cloud OS and the Fabric8 DevOps and iPaaS capabilities make this truly disruptive technology. It really is a cloud in a box for just $200.
</p>
