<p>Imagine a world where your software works perfectly for you. It meets your needs, does things your way, and is the ideal tool to achieve great things toward your goals.</p>
<p>Open source software stems from these roots. Many projects are built by engineers that have a problem and build a solution to solve it. Then they openly share their solution with others to use and improve.</p>
<p>Unfortunately, building software is hard. Not everyone has the expertise to build software that works perfectly for their needs. And if the software developers building applications don't fully understand users' needs and how they do their job, the solutions they build may not meet the users' needs and may accidentally create a lot of gaps.</p>
<p>I recently encountered an example of this at my dentist's office. As I was waiting in the chair, I couldn't help but notice how long the dental hygienist was taking to punch around in the software to get ready for the dentist's examination. So I asked her, "Do you feel like this software meets your needs?" She replied, "No! It's so complex, and what I actually need is never where I need it!"</p>
<p>Part of the problem is that the people who built this software are probably not dentists, dental hygienists, or have any experience with a dentist other than as a patient in the chair. Many software companies have roles to help fill those gaps—product managers, product and market analysts, researchers, and others are meant to help gather requirements that tell development teams what to build.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Great Content</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=7016000000127cYAAQ">Free online course: RHEL technical overview</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=7016000000127cYAAQ">Learn Advanced Linux Commands</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Download Cheat Sheets</a></li>
<li><a href="https://opensource.com/alternatives?intcmp=7016000000127cYAAQ">Find an Open Source Alternative</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=7016000000127cYAAQ">Read Top Linux Content</a></li>
<li><a href="https://opensource.com/resources?intcmp=7016000000127cYAAQ">Check out open source resources</a></li>
</ul></div>
</div>
</div>
</div>
<p>But we all know about learning a new domain. There's learning it from the outside, casually understanding it; then there's learning it from the inside, living it day-to-day, and really knowing it as an expert. The closer the development team can get to the real, "living it" experience, the better decisions they will make when they build software for specific users and domains.</p>
<p>Open source has an ethos of sharing the work being done. Now I think it is time to evolve open source to the next level: sharing work that <em>needs to be</em> done. Domain experts (i.e., the eventual users) need to share information about what they need to do their job so that open source developers can build software to meet their needs.</p>
<p>This might sound overwhelming if you're not a software developer. Contributing to open source means that you need to write code, understand <a href="https://opensource.com/resources/what-is-git">Git</a>, or cross other technical hurdles, right? Not necessarily; contributing your domain expertise is an important part of open source development.</p>
<p>And you can use the tools you already have. You're already on social media. You probably have access to a word processor of some kind. And you have a work environment that can be shared openly (even in COVID times, thanks to technology).</p>
<h2 id="three-steps-to-opening-your-job">Three steps to opening your job</h2>
<p>The following steps can help you open your job so software developers can learn your domain, understand your job and pain points, and build software that works better for you.</p>
<h3 id="invite-software-builders-into-your-work-environment">1. Invite software builders into your work environment</h3>
<p>An ethnographic study is where parts of the software team shadow you in your job. You'll be asked to do everything you normally do on a workday, just as you always do it. Like a "fly on the wall," the software team is there to observe and understand how you do what you do. They might debrief with you throughout the day to get more insights into why you did certain things. Overall, this is meant to be non-invasive for you but very informative for the team.</p>
<h3 id="share-writeups-about-your-job-with-software-builders">2. Share writeups about your job with software builders</h3>
<p>Far too often in the software industry, we focus on the software, not its use. As an example, a software requirement may come in as: "Show the history of dental work done to a tooth." This assumes that the software is required to do this job and that this history is useful no matter where it's shown in the software.</p>
<p>Instead, developers need to back up and make sure they understand the job functions thoroughly. For example, a requirement could say: "A patient has a tooth with a cavity that was found during routine cleaning. While the patient is still there, the dentist needs to know what else has been done to the tooth to be able to prescribe a solution to the patient." By knowing the job that needs to be done, the software developer can build software that enables easy selection of the tooth in question, mid-exam, with gloves on. Therefore, this information should be available while in the "routine cleaning" view.</p>
<p>Writing up your intent and context while you're using the software, rather than the tasks you do, can help software developers build to meet your true needs.</p>
<p>If you want to go deeper, look at the <a href="https://www.google.com/books/edition/The_Jobs_To_Be_Done_Playbook/1vHRDwAAQBAJ?hl=en&amp;gbpv=1&amp;printsec=frontcover" target="_blank">Jobs To Be Done</a> (JTBD) framework as a way to convey your expertise to software teams.</p>
<h3 id="use-social-media-to-share-videos-explanations-and-more-about-your-job">3. Use social media to share videos, explanations, and more about your job</h3>
<p>Use the social media platform of your choice (e.g., YouTube, Reddit, etc.) to share information about your job. Recording and sharing a "day in the life" video openly could greatly help development teams really understand what you do and how you do it so that they can create software that serves your needs.</p>
<h2>Open opportunities</h2>
<p>Open your job, and you'll open opportunities for software developers to build better software! Let's bring open source to the next level together by sharing more about what you need.</p>
