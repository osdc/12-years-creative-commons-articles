<p>The Opensource.com team has been fascinated by the <a href="https://opensource.com/life/15/10/exploring-raspberry-pi-sense-hat" target="_blank">Raspberry Pi Sense HAT</a>, a low-cost addon for the Raspberry Pi that enables astronauts and citizen scientists alike to easily collect measurements from a variety of sensors to conduct science experiments or just have fun.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on Raspberry Pi</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/resources/what-raspberry-pi?src=raspberry_pi_resource_menu1&amp;intcmp=701f2000000h4RcAAI">What is Raspberry Pi?</a></li>
<li><a href="https://opensource.com/downloads/raspberry-pi-guide?src=raspberry_pi_resource_menu2&amp;intcmp=701f2000000h4RcAAI">eBook: Guide to Raspberry Pi</a></li>
<li><a href="https://opensource.com/downloads/getting-started-raspberry-pi-cheat-sheet?src=raspberry_pi_resource_menu3&amp;intcmp=701f2000000h4RcAAI">Getting started with Raspberry Pi cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/kubernetes-raspberry-pi?src=raspberry_pi_resource_menu6&amp;intcmp=701f2000000h4RcAAI">eBook: Running Kubernetes on your Raspberry Pi</a></li>
<li><a href="https://www.redhat.com/en/resources/data-intensive-applications-hybrid-cloud-blueprint-detail?src=raspberry_pi_resource_menu7&amp;intcmp=701f2000000h4RcAAI">Whitepaper: Data-intensive intelligent applications in a hybrid cloud blueprint</a></li>
<li><a href="https://www.redhat.com/en/topics/edge-computing?src=raspberry_pi_resource_menu5&amp;intcmp=701f2000000h4RcAAI">Understanding edge computing</a></li>
<li><a href="https://opensource.com/tags/raspberry-pi?src=raspberry_pi_resource_menu4&amp;intcmp=701f2000000h4RcAAI">Our latest on Raspberry Pi </a></li>
</ul></div>
</div>
</div>
</div>

<p>So we decided to try one out ourselves. We grabbed a side room at our offices in Red Hat Tower and spent an hour or two learning what it can do.</p>
<p>First step, attaching the device. Easy enough!</p>
<p><img alt="sense-hat-attached.jpg" src="https://opensource.com/sites/default/files/resize/images/life-uploads/sense-hat-attached-520x390.jpg" style="width: 520px; height: 390px;" width="520" height="390" /></p>
<p>Then, it was time to try out some of the functionality. The Raspberry Pi Sense HAT provides a variety of ways for testing out interactivity and sensing the environment, including electronics for:</p>
<ul><li>Accelerometer (movement)</li>
<li>Barometer (pressure)</li>
<li>Gyroscope (rotation)</li>
<li>Hygrometer (humidity)</li>
<li>Joystick (basic input)</li>
<li>LED matrix (basic output)</li>
<li>Magnetometer (direction)</li>
<li>Thermometer (temperature)</li>
</ul><h2>Experiments</h2>
<p>We got started by testing out the humidity and temperature sensors. After borrowing and slightly modifying <a href="https://github.com/bennuttall/sense-hat-examples/blob/master/python/humidity.py" target="_blank">some code</a> from Ben Nuttall's GitHub repository, we were able to display a graph of the humidity in our room on screen the Sense HAT's screen.</p>
<pre>
<code class="language-python">from sense_hat import SenseHat

sense = SenseHat()

while True:
    humidity = sense.humidity
    humidity_value = 64 * humidity / 100
    print(humidity)</code></pre><p>A further extension of this code let us display the results on the Sense HAT's tiny screen, but with several of us gathered in the room, it was simply easier to display the output of our terminal window on the room's projector. Next, we tested the accelerometer.</p>
<p>Using the Creative Commons <a href="https://www.raspberrypi.org/learning/magic-8-ball/worksheet/" target="_blank">instructions</a> on the Raspberry Pi, we designed a Magic 8 Ball. Except my team wasn't content with the default responses, and instead, decided to make it a Magic 8 Ball that answers questions the way that I do.</p>
<pre>
<code class="language-python">import random
import time
from sense_hat import SenseHat

sh = SenseHat()

sh.show_message("Ask a question &amp; shake", scroll_speed=(0.06))
time.sleep(3)

replies = ['No.',
	   'That is a great question...',
	   'It depends...',
	   'Actually it is complicated...',
	   'Let me think about that.'
        ]

while True:
    x, y, z = sh.get_accelerometer_raw().values()

    x = abs(x)
    y = abs(y)
    z = abs(z)

if x &gt; 2 or y &gt; 2 or z &gt; 2 :
    sh.show_message(random.choice(replies))
else:
    sh.clear()</code></pre><p>Unfortunately for me, that worked too.</p>
<blockquote class="twitter-tweet" data-lang="en"><p dir="ltr" lang="en" xml:lang="en">Just made a magic eight ball that spouts <a href="https://twitter.com/jehb">@jehb</a>-isms when we shake it.<br />
h/t <a href="https://twitter.com/Raspberry_Pi">@Raspberry_Pi</a> for the how-to! <a href="https://t.co/b4o0CWBfox">pic.twitter.com/b4o0CWBfox</a></p>
<p>— Alex Sanchez (@_alxsanchez) <a href="https://twitter.com/_alxsanchez/status/710869145060032512">March 18, 2016</a></p></blockquote>
<script async="" src="//platform.twitter.com/widgets.js" charset="utf-8"></script><h2>What's next?</h2>
<p>Now that we've learned to use the Sense Hat, what's next? Sure, it's a fun toy, but it's also a working scientific instrument. Why not try out some 'real' science? Here are some of the projects we hope to try out next.</p>
<ul><li>Testing the humidity every few milliseconds is fun, but not all that useful. Let's build a data logger to record the humidity over time to a file, so we know how damp (or not) the air in our office is over the course of a week.</li>
<li>Next, let's take the concept and extend it a little further. Using the thermometer just as we did the humidity meter, let's use a little Python to have our Raspberry Pi tweet at us when the conference room gets too cool for taste, to remind us to bring a sweater!</li>
<li>Ever wonder how 'active' you are at your desk, as you figet and shift your weight around throughout the day? Why not use the accelerometer and a chair-mounted Pi to figure out just how many times you move across your workday?</li>
</ul><p>Have you tried experimenting with the Raspberry Pi Sense HAT? What have you built with it? Let us know in the comments below!</p>
<ul></ul>