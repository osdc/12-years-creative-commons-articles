<p>In this week's Top 5, we highlight machines, machine learning, and learning. Plus there's a look at two of the most popular Linux desktop environments. Be sure to vote for your favorite in our <a href="https://opensource.com/article/17/5/linux-desktop-environment-poll">poll</a>.</p>
<p><iframe allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/L3NlCkwG5Fs" width="560"></iframe></p>
<h2>Top 5 articles of the week</h2>
<p><strong>5. <a href="https://opensource.com/article/17/5/7-cool-kde-tweaks-will-improve-your-life">7 cool KDE tweaks that will change your life</a> </strong></p>
<p>In this article, Community Moderator Seth Kenlon shares seven ways to customize your KDE desktop. If you're a KDE user like me, you'll want to check this out.</p>
<p><strong>4. <a href="https://opensource.com/article/17/5/how-linux-higher-education">5 myths busted: Using open source in higher education</a></strong></p>
<p>Have you ever heard someone say, "It's impossible to do X with Linux"? This is the story of how Kyle R. Conway busted the myths about open source and used Linux to finish his PhD in fine arts.</p>
<p><strong>3. <a href="https://opensource.com/article/17/5/reasons-gnome">11 reasons to use the GNOME 3 desktop environment for Linux</a></strong></p>
<p>Desktop environments are a personal choice. In this article, Community Moderator David Both shares what he loved after giving GNOME 3 a try.</p>
<p><strong>2. <a href="https://opensource.com/article/17/5/upgrade-your-car-these-5-arduino-projects">5 Arduino projects to upgrade your car</a></strong></p>
<p>Cars aren't just for analogies anymore. Opensource.com's Alex Sanchez shares five projects that you can use to make your ride a little sweeter.</p>
<p><strong>1. <a href="https://opensource.com/article/17/5/python-machine-learning-introduction">Get started with machine learning using Python</a></strong></p>
<p>Machine learning is an in-demand skill to add to your resume. Dr. Michael Garbade walks through steps for wading into machine learning with the help of Python.</p>
<h2>Honorable mention</h2>
<p><strong><a href="https://opensource.com/article/17/5/using-openshot-video-editor">Tinkering with OpenShot for video editing</a></strong></p>
<p>Want to edit your own videos with open source tools? Jeff Macharyas gives an introduction to the OpenShot video editing program. Maybe you can make your own Top 5 video!</p>
