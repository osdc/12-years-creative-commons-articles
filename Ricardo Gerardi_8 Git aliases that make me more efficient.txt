<p>The excellent article <em><a href="https://opensource.com/article/20/10/advanced-git-tips">7 Git tricks that changed my life</a></em> inspired me to write about another Git feature that's had a major impact on my experience using Git on the command line: aliases.</p>
<p>Defining Git aliases to serve as substitutes for commands provides two major benefits:</p>
<ul><li>It simplifies long commands that have many options, making them shorter and easier to remember.</li>
<li>It shortens frequently used commands so that you can work more efficiently.</li>
</ul><h2 id="how-to-define-and-use-aliases">How to define and use aliases</h2>
<p>To define a Git alias, use the <code>git config</code> command with the alias and the command you want to substitute. For example, to create the alias <code>p</code> for <code>git push</code>:</p>
<pre><code class="language-bash">$ git config --global alias.p 'push'
</code></pre><p>You can use an alias by providing it as an argument to <code>git</code>, just like any other command:</p>
<pre><code class="language-bash">$ git p
</code></pre><p>To see all your aliases, list your configuration with <code>git config</code>:</p>
<pre><code class="language-bash">$ git config --global -l
user.name=ricardo
user.email=ricardo@example.com
alias.p=push</code></pre><p>You can also define aliases with your favorite shell, such as Bash or Zsh. However, defining aliases using Git offers several features that you don't get using the shell. First, it allows you to use aliases across different shells with no additional configuration. It also integrates with Git's autocorrect feature, so Git can suggest aliases as alternatives when you mistype a command. Finally, Git saves your aliases in the user configuration file, allowing you to transfer them to other machines by copying a single file.</p>
<p>Regardless of the method you use, defining aliases improves your overall experience with Git. For more information about defining Git aliases, take a look at the <a href="https://git-scm.com/book/en/v2/Git-Basics-Git-Aliases" target="_blank">Git Book</a>.</p>
<h2 id="useful-git-aliases">8 useful Git aliases</h2>
<p>Now that you know how to create and use an alias, take a look at some useful ones.</p>
<h3 id="git-status">1. Git status</h3>
<p>Git command line users often use the <code>status</code> command to see changed or untracked files. By default, this command provides verbose output with many lines, which you may not want or need. You can use a single alias to address both of these components: Define the alias <code>st</code> to shorten the command with the option <code>-sb</code> to output a less verbose status with branch information:</p>
<pre><code class="language-bash">$ git config --global alias.st 'status -sb'
</code></pre><p>If you use this alias on a clean branch, your output looks like this:</p>
<pre><code class="language-bash">$  git st
## master</code></pre><p>Using it on a branch with changed and untracked files produces this output:</p>
<pre><code class="language-bash">$ git st
## master
 M test2
?? test3</code></pre><h3 id="git-log---oneline">2. Git log --oneline</h3>
<p>Create an alias to display your commits as single lines for more compact output:</p>
<pre><code class="language-bash">$ git config --global alias.ll 'log --oneline'
</code></pre><p>Using this alias provides a short list of all commits:</p>
<pre><code class="language-bash">$ git ll
33559c5 (HEAD -&gt; master) Another commit
17646c1 test1</code></pre><h3 id="git-last-commit">3. Git last commit</h3>
<p>This shows details about the most recent commit you made. This extends an example in the Git Book's chapter on <a href="https://git-scm.com/book/en/v2/Git-Basics-Git-Aliases" target="_blank">Aliases</a>:</p>
<pre><code class="language-bash">$ git config --global alias.last 'log -1 HEAD --stat'
</code></pre><p>Use it to see the last commit:</p>
<pre><code class="language-bash">$ git last
commit f3dddcbaabb928f84f45131ea5be88dcf0692783 (HEAD -&gt; branch1)
Author: ricardo &lt;ricardo@example.com&gt;
Date:   Tue Nov 3 00:19:52 2020 +0000

    Commit to branch1

 test2 | 1 +
 test3 | 0
 2 files changed, 1 insertion(+)</code></pre><h3 id="git-commit">4. Git commit</h3>
<p>You use <code>git commit</code> a lot when you're making changes to a Git repository. Make the <code>git commit -m</code> command more efficient with the <code>cm</code> alias:</p>
<pre><code class="language-bash">$ git config --global alias.cm 'commit -m'
</code></pre><p>Because Git aliases expand commands, you can provide additional parameters during their execution:</p>
<pre><code class="language-bash">$ git cm "A nice commit message"
[branch1 0baa729] A nice commit message
 1 file changed, 2 insertions(+)</code></pre><h3 id="git-remote">5. Git remote</h3>
<p>The <code>git remote -v</code> command lists all configured remote repositories. Shorten it with the alias <code>rv</code>:</p>
<pre><code class="language-bash">$ git config --global alias.rv 'remote -v'
</code></pre><h3 id="git-diff">6. Git diff</h3>
<p>The <code>git diff</code> command displays differences between files in different commits or between a commit and the working tree. Simplify it with the <code>d</code> alias:</p>
<pre><code class="language-bash">$ git config --global alias.d 'diff'
</code></pre><p>The standard <code>git diff</code> command works fine for small changes. But for more complex ones, an external tool such as <code>vimdiff</code> makes it more useful. Create the alias <code>dv</code> to display diffs using <code>vimdiff</code> and use the <code>-y</code> parameter to skip the confirmation prompt:</p>
<pre><code class="language-bash">$ git config --global alias.dv 'difftool -t vimdiff -y'
</code></pre><p>Use this alias to display <code>file1</code> differences between two commits:</p>
<pre><code class="language-bash">$ git dv 33559c5 ca1494d file1
</code></pre><p>
<article class="align-center media media--type-image media--view-mode-full" title="vim-diff results"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/vimdiff.png" width="675" height="357" alt="vim-diff results" title="vim-diff results" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Ricardo Gerardi, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<h3 id="git-config-list">7. Git config list</h3>
<p>The <code>gl</code> alias makes it easier to list all user configurations:</p>
<pre><code class="language-bash">$ git config --global alias.gl 'config --global -l'
</code></pre><p>Now you can see all defined aliases (and other configuration options):</p>
<pre><code class="language-bash">$ git gl
user.name=ricardo
user.email=ricardo@example.com
alias.p=push
alias.st=status -sb
alias.ll=log --oneline
alias.last=log -1 HEAD --stat
alias.cm=commit -m
alias.rv=remote -v
alias.d=diff
alias.dv=difftool -t vimdiff -y
alias.gl=config --global -l
alias.se=!git rev-list --all | xargs git grep -F</code></pre><h3 id="git-search-commit">8. Git search commit</h3>
<p>Git alias allows you to define more complex aliases, such as executing external shell commands, by prefixing them with the <code>!</code> character. You can use this to execute custom scripts or more complex commands, including shell pipes.</p>
<p>For example, define the <code>se</code> alias to search within your commits:</p>
<pre><code class="language-bash">$ git config --global alias.se '!git rev-list --all | xargs git grep -F'
</code></pre><p>Use this alias to search for specific strings in your commits:</p>
<pre><code class="language-bash">$ git se test2
0baa729c1d683201d0500b0e2f9c408df8f9a366:file1:test2
ca1494dd06633f08519ec43b57e25c30b1c78b32:file1:test2</code></pre><h2 id="optimize-your-efficiency">Autocorrect your aliases</h2>
<p>A cool benefit of using Git aliases is its native integration with the autocorrect feature. If you make a mistake, by default Git suggests commands that are similar to what you typed, including aliases. For example, if you type <code>ts</code> instead of <code>st</code> for <code>status</code>, Git will suggest the correct alias:</p>
<pre><code class="language-bash">$ git ts
git: 'ts' is not a git command. See 'git --help'.

The most similar command is
        st</code></pre><p>If you have autocorrect enabled, Git will automatically execute the correct command:</p>
<pre><code class="language-bash">$ git config --global help.autocorrect 20
$ git ts
WARNING: You called a Git command named 'ts', which does not exist.
Continuing in 2.0 seconds, assuming that you meant 'st'.
## branch1
?? test4</code></pre><h2>Optimize Git commands</h2>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on Git</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/resources/what-is-git?intcmp=7013a000002CxqLAAS">What is Git?</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-git?intcmp=7013a000002CxqLAAS">Git cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-markdown?intcmp=7013a000002CxqLAAS">Markdown cheat sheet</a></li>
<li><a href="https://opensource.com/tags/git?intcmp=7013a000002CxqLAAS">New Git articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>Git alias is a useful feature that improves your efficiency by optimizing the execution of common and repetitive commands. Git allows you to define as many aliases as you want, and some users define many. I prefer to define aliases for just my most used commands—defining too many makes it harder to memorize them and may require me to look them up to use them.</p>
<p>For more about aliases, including other useful ones, see the <a href="https://git.wiki.kernel.org/index.php/Aliases" target="_blank">Git Wiki's Aliases page</a>.</p>
