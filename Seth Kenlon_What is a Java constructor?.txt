<p>Java is (disputably) the undisputed heavyweight in open source, cross-platform programming. While there are many <a href="https://opensource.com/resources/python">great</a> <a href="https://opensource.com/resources/python">cross-platform</a> <a href="https://opensource.com/article/17/4/pyqt-versus-wxpython">frameworks</a>, few are as unified and direct as <a href="https://opensource.com/resources/java">Java</a>.</p>
<p>Of course, Java is also a pretty complex language with subtleties and conventions all its own. One of the most common questions about Java relates to <strong>constructors</strong>: What are they and what are they used for?</p>
<p>Put succinctly: a constructor is an action performed upon the creation of a new <strong>object</strong> in Java. When your Java application creates an instance of a class you have written, it checks for a constructor. If a constructor exists, Java runs the code in the constructor while creating the instance. That's a lot of technical terms crammed into a few sentences, but it becomes clearer when you see it in action, so make sure you have <a href="https://openjdk.java.net/install/index.html">Java installed</a> and get ready for a demo.</p>
<h2 id="life-without-constructors">Life without constructors</h2>
<p>If you're writing Java code, you're already using constructors, even though you may not know it. All classes in Java have a constructor because even if you haven't created one, Java does it for you when the code is compiled. For the sake of demonstration, though, ignore the hidden constructor that Java provides (because a default constructor adds no extra features), and take a look at life without an explicit constructor.</p>
<p>Suppose you're writing a simple Java dice-roller application because you want to produce a pseudo-random number for a game.</p>
<p>First, you might create your dice class to represent a physical die. Knowing that you play a lot of <a href="https://opensource.com/article/19/5/free-rpg-day">Dungeons and Dragons</a>, you decide to create a 20-sided die. In this sample code, the variable <strong>dice</strong> is the integer 20, representing the maximum possible die roll (a 20-sided die cannot roll more than 20). The variable <strong>roll</strong> is a placeholder for what will eventually be a random number, and <strong>rand</strong> serves as the random seed.</p>
<pre><code class="language-java">import java.util.Random;

public class DiceRoller {
  private int dice = 20;
  private int roll;
  private Random rand = new Random();</code></pre><p>Next, create a function in the <strong>DiceRoller</strong> class to execute the steps the computer must take to emulate a die roll: Take an integer from <strong>rand</strong> and assign it to the <strong>roll</strong> variable, add 1 to account for the fact that Java starts counting at 0 but a 20-sided die has no 0 value, then print the results.</p>
<pre><code class="language-java">public void Roller() {
	roll = rand.nextInt(dice);
	roll += 1;
	System.out.println (roll);
}</code></pre><p>Finally, spawn an instance of the <strong>DiceRoller</strong> class and invoke its primary function, <strong>Roller</strong>:</p>
<pre><code class="language-java"> // main loop
 public static void main (String[] args) {
	 System.out.printf("You rolled a ");

	 DiceRoller App = new DiceRoller();
	 App.Roller();
 }
}</code></pre><p>As long as you have a Java development environment installed (such as <a href="https://openjdk.java.net/" target="_blank">OpenJDK</a>), you can run your application from a terminal:</p>
<pre><code class="language-java">$ java dice.java
   You rolled a 12</code></pre><p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Programming and development</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ&amp;src=programming_resource_menu4">Red Hat Developers Blog</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Programming cheat sheets</a></li>
<li><a href="https://www.redhat.com/en/services/training/learning-subscription?intcmp=7016000000127cYAAQ">Try for free: Red Hat Learning Subscription</a></li>
<li><a href="https://opensource.com/downloads/bash-programming-guide?intcmp=7016000000127cYAAQ">eBook: An introduction to programming with Bash</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/bash-shell-cheat-sheet?intcmp=7016000000127cYAAQ">Bash Shell Scripting Cheat Sheet</a></li>
<li><a href="https://developers.redhat.com/e-books/modernizing-enterprise-java?intcmp=7016000000127cYAAQ">eBook: Modernizing Enterprise Java</a></li>
</ul></div>
</div>
</div>
</div>
<p>In this example, there is no explicit constructor. It's a perfectly valid and legal Java application, but it's a little limited. For instance, if you set your game of Dungeons and Dragons aside for the evening to play some Yahtzee, you would need 6-sided dice. In this simple example, it wouldn't be that much trouble to change the code, but that's not a realistic option in complex code. One way you could solve this problem is with a constructor.</p>
<h2 id="constructors-in-action">Constructors in action</h2>
<p>The <strong>DiceRoller</strong> class in this example project represents a virtual dice factory: When it's called, it creates a virtual die that is then "rolled." However, by writing a custom constructor, you can make your Dice Roller application ask what kind of die you'd like to emulate.</p>
<p>Most of the code is the same, with the exception of a constructor accepting some number of sides. This number doesn't exist yet, but it will be created later.</p>
<pre><code class="language-java">import java.util.Random;

public class DiceRoller {
	private int dice;  
	private int roll;
	private Random rand = new Random();

  // constructor 
  public DiceRoller(int sides) {
	dice = sides; 
  }</code></pre><p>The function emulating a roll remains unchanged:</p>
<pre><code class="language-java">public void Roller() {
	roll = rand.nextInt(dice);
	roll += 1;
	System.out.println (roll);
}</code></pre><p>The main block of code feeds whatever arguments you provide when running the application. Were this a complex application, you would parse the arguments carefully and check for unexpected results, but for this sample, the only precaution taken is converting the argument string to an integer type:</p>
<pre><code class="language-java"> public static void main (String[] args) {
	 System.out.printf("You rolled a ");
	 DiceRoller App = new DiceRoller( Integer.parseInt(args[0]) );
	 App.Roller();
 }
}</code></pre><p>Launch the application and provide the number of sides you want your die to have:</p>
<pre><code class="language-java">$ java dice.java 20
    You rolled a 10
    $ java dice.java 6
    You rolled a 2
    $ java dice.java 100
    You rolled a 44</code></pre><p>The constructor has accepted your input, so when the class instance is created, it is created with the <strong>sides</strong> variable set to whatever number the user dictates.</p>
<p>Constructors are powerful components of programming. Practice using them to unlock the full potential of Java.</p>
