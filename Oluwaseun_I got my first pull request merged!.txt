<p><meta charset="utf-8" /></p>
<p>Words cannot express my joy when I got the notification about the merge below, and I owe it to my current engineering school, <a href="https://www.altschoolafrica.com/" target="_blank">AltSchool Africa</a>.</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-09/successfulmerge.png" width="880" height="182" alt="successful merge message" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Awosise Oluwaseun, CC BY-SA 4.0)</p>
</div>
      
  </article><p>Before this, I had been introduced to open source many times, told about its importance in the tech space, and even attended open source conferences (e.g., OSCAFest). I had all the instant excitement to start, but imposter syndrome set in on opening GitHub to create something.</p>
<p>Fast forward to Monday, the 8th of August, 2022, when I watched Bolaji's video on contributing to open source. I felt pumped again, but I wanted to apply what I learned, so I noted some steps.</p>
<p>The steps:</p>
<ol><li>I made up my mind I was going to contribute to a project.</li>
<li>I was focused on a site (<a href="https://goodfirstissues.com/" target="_blank">good first issue</a>) to pick my first project from, which I filtered to suit my skill level. I kept opening the next page till I found one.</li>
<li>I made sure I was equipped with the required <a href="https://docs.github.com/en/pull-requests/collaborating-with-pull-requests/getting-started/about-collaborative-development-models" target="_blank">Git and GitHub</a> knowledge to complete the project.</li>
</ol><p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">Our favorite resources about open source</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-git?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Git cheat sheet">Git cheat sheet</a></div>
              <div class="field__item"><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Advanced Linux commands cheat sheet">Advanced Linux commands cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/alternatives?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Open source alternatives">Open source alternatives</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Free online course: RHEL technical overview">Free online course: RHEL technical overview</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Register for your free Red Hat account">Register for your free Red Hat account</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Check out more cheat sheets">Check out more cheat sheets</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<h2>The project</h2>
<p>After long hours searching for projects, I finally found one titled, <a href="https://github.com/mdn/content/issues/19334" target="_blank">Ensure no missing alt attributes</a>. I was to give descriptive alt values to images from the site. Alt values in images help to improve the accessibility of the site such that screen readers can provide a detailed description of the image to, say, a visually impaired person. Easy right? Yes, but if I didn't make up my mind to get the first contribution, I wouldn't find it, and open source would continue to be a myth to me.</p>
<p>I was still pumped until I discovered it was from <a href="https://developer.mozilla.org/en-US/" target="_blank">MDN</a>. Wait, MDN? As in Mozilla developer? Will they merge my contribution even with how seemingly easy it looks? <a href="https://opensource.com/article/20/9/imposter-syndrome" target="_blank">Imposter syndrome</a> set in.</p>
<p>Upon checking the issue, I saw that people were already contributing. I summoned my courage and started reading about it. Taking my time to read and understand the project and how I needed to approach the issue was another challenge I had to overcome.</p>
<p>The project is as easy as you try to understand it.</p>
<p>So, I picked two images to begin with. I gave alt values to them, committed my changes, then made a pull request. The time between when I made the pull request and when I got the approval mail was full of self-doubts. Should I close the pull request? This is MDN. Well, it's not coding... What if I don't get merged? I might never contribute again. All it took to clear all of the doubts were the emails I got from my reviewer below:</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-09/approved.png" width="879" height="366" alt="Email of approved pull request" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Awosise Oluwaseun, CC BY-SA 4.0)</p>
</div>
      
  </article><article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-09/merged_0.png" width="838" height="415" alt="mail showing that pull request has been merged" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Awosise Oluwaseun, CC BY-SA 4.0)</p>
</div>
      
  </article><article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-09/thanks.png" width="880" height="466" alt="congratulatory mail on contributing and merging of pull request" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Awosise Oluwaseun, CC BY-SA 4.0)</p>
</div>
      
  </article><p>I was indeed delighted, and this inspired me to check for more. It gave me the courage I needed to request additional issues to solve.</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-09/next.png" width="748" height="387" alt="Mail of issue assignment" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Awosise Oluwaseun, CC BY-SA 4.0)</p>
</div>
      
  </article><h2>Summary</h2>
<p>A few lessons I'd love you to take home from this article are:</p>
<ul><li>Open source is for all. Do you see that typo on that site you just visited? You helping to correct it is a way of contributing.</li>
<li>No skillset is too small. A basic understanding of HTML was what I needed to contribute.</li>
<li>Only you can stop yourself from contributing.</li>
<li>The first contribution is all you need to get the ball rolling.</li>
</ul><p>I hope you have been able to pick something from my story and apply it today. This is another space I'd like to keep contributing to, so see you in my next article, and happy open sourcing!</p>
<hr /><p><em>This article originally appeared on <a href="https://dev.to/jhhornn/i-got-my-first-pull-request-merged-3ei9" target="_blank">I got my first Pull Request merged!</a> and is republished with permission.</em></p>
