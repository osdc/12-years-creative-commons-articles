<p>The humble <strong>mv</strong> command is one of those useful tools you find on every POSIX box you encounter. Its job is clearly defined, and it does it well: Move a file from one place in a file system to another. But Linux is nothing if not flexible, and there are other options for moving files. Using different tools can provide small advantages that fit perfectly with a specific use case.</p>
<p>Before straying too far from <strong>mv</strong>, take a look at this command’s default results. First, create a directory and generate some files with permissions set to 777:</p>
<pre><code class="language-bash">$ mkdir example
$ touch example/{foo,bar,baz}
$ for i in example/*; do ls /bin &gt; "${i}"; done 
$ chmod 777 example/*
</code></pre><p>You probably don't think about it this way, but files exist as entries, called index nodes (commonly known as <strong>inodes</strong>), in a <a href="https://opensource.com/article/18/11/partition-format-drive-linux#what-is-a-filesystem" target="_blank">filesystem</a>. You can see what inode a file occupies with the <a href="https://opensource.com/article/19/7/master-ls-command" target="_blank">ls command</a> and its <strong>--inode</strong> option:</p>
<pre><code class="language-bash">$ ls --inode example/foo
7476868 example/foo
</code></pre><p>As a test, move that file from the example directory to your current directory and then view the file’s attributes:</p>
<pre><code class="language-text">$ mv example/foo .
$ ls -l -G -g --inode
7476868 -rwxrwxrwx. 1 29545 Aug  2 07:28 foo
</code></pre><p>As you can see, the original file—along with its existing permissions—has been "moved", but its inode has not changed.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>The Linux Terminal</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/life/17/10/top-terminal-emulators?intcmp=7016000000127cYAAQ">Top 7 terminal emulators for Linux</a></li>
<li><a href="https://opensource.com/article/17/2/command-line-tools-data-analysis-linux?intcmp=7016000000127cYAAQ">10 command-line tools for data analysis in Linux</a></li>
<li><a href="https://opensource.com/downloads/advanced-ssh-cheat-sheet?intcmp=7016000000127cYAAQ">Download Now: SSH cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=7016000000127cYAAQ">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://opensource.com/tags/command-line?intcmp=7016000000127cYAAQ">Linux command line tutorials</a></li>
</ul></div>
</div>
</div>
</div>
<p>That’s the way the <strong>mv</strong> tool is programmed to move a file: Leave the inode unchanged (unless the file is being moved to a different filesystem), and preserve its ownership and permissions.</p>
<p>Other tools provide different options.</p>
<h2 id="copy-and-remove">Copy and remove</h2>
<p>On some systems, the move action is a true move action: Bits are removed from one point in the file system and reassigned to another. This behavior has largely fallen out of favor. Move actions are now either attribute reassignments (an inode now points to a different location in your file organization) or amalgamations of a copy action followed by a remove action.<br /><br />
The philosophical intent of this design is to ensure that, should a move fail, a file is not left in pieces.</p>
<p>The <strong>cp</strong> command, unlike <strong>mv</strong>, creates a brand new data object in your filesystem. It has a new inode location, and it is subject to your active umask. You can mimic a move using the <strong>cp</strong> and <strong>rm</strong> (or <a href="https://gitlab.com/trashy" target="_blank">trash</a> if you have it) commands:</p>
<pre><code class="language-bash">$ cp example/foo .
$ ls -l -G -g --inode
7476869 -rwxrwxr-x. 29545 Aug  2 11:58 foo
$ trash example/foo
</code></pre><p>The new <strong>foo</strong> file in this example got 775 permissions because the location’s umask specifically excludes write permissions:</p>
<pre><code class="language-text">$ umask
0002
</code></pre><p>For more information about umask, read Alex Juarez’s article about <a href="https://opensource.com/article/19/8/linux-permissions-101#umask">file permissions</a>.</p>
<h2 id="cat-and-remove">Cat and remove</h2>
<p>Similar to a copy and remove, using the <a href="https://opensource.com/article/19/2/getting-started-cat-command" target="_blank">cat</a> (or <strong>tac</strong>, for that matter) command assigns different permissions when your "moved" file is created. Assuming a fresh test environment with no <strong>foo</strong> in the current directory:</p>
<pre><code class="language-text">$ cat example/foo &gt; foo
$ ls -l -G -g --inode
7476869 -rw-rw-r--. 29545 Aug 8 12:21 foo
$ trash example/foo
</code></pre><p>This time, a new file was created with no prior permissions set. The result is entirely subject to the umask setting, which blocks no permission bit for the user and group (the executable bit is not granted for new files regardless of umask), but it blocks the write (value two) bit from others. The result is a file with 664 permission.</p>
<h2 id="rsync">Rsync</h2>
<p>The <strong>rsync</strong> command is a robust multipurpose tool to send files between hosts and file system locations. This command has many options available to it, including the ability to make its destination mirror its source.</p>
<p>You can copy and then remove a file with <strong>rsync</strong> using the <strong>--remove-source-files</strong> option, along with whatever other option you choose to perform the synchronization (a common, general-purpose one is <strong>--archive</strong>):</p>
<pre><code class="language-bash">$ rsync --archive --remove-source-files example/foo .
$ ls example
bar  baz
$ ls -lGgi
7476870 -rwxrwxrwx. 1 seth users 29545 Aug 8 12:23 foo
</code></pre><p>Here you can see that file permission and ownership was retained, the timestamp was updated, and the source file was removed.</p>
<p><strong>A word of warning:</strong> Do not confuse this option for <strong>--delete</strong>, which removes files from your <em>destination</em> directory. Misusing <strong>--delete</strong> can wipe out most of your data, and it’s recommended that you avoid this option except in a test environment.</p>
<p>You can override some of these defaults, changing permission and modification settings:</p>
<pre><code class="language-bash">$ rsync --chmod=666 --times \
--remove-source-files example/foo .
$ ls example
bar  baz
$ ls -lGgi
7476871 -rw-rw-r--. 1 seth users 29545 Aug 8 12:55 foo
</code></pre><p>Here, the destination’s umask is respected, so the <strong>--chmod=666</strong> option results in a file with 664 permissions.</p>
<p>The benefits go beyond just permissions, though. The <strong>rsync</strong> command has <a href="https://opensource.com/article/19/5/advanced-rsync" target="_blank">many</a> useful <a href="https://opensource.com/article/17/1/rsync-backup-linux" target="_blank">options</a> (not the least of which is the <strong>--exclude</strong> flag so you can exempt items from a large move operation) that make it a more robust tool than the simple <strong>mv</strong> command. For example, to exclude all backup files while moving a collection of files:</p>
<pre><code class="language-bash">$ rsync --chmod=666 --times \
--exclude '*~' \
--remove-source-files example/foo .</code></pre><h2 id="setting-permissions-with-the-install-command">Set permissions with install</h2>
<p>The <strong>install</strong> command is a copy command specifically geared toward developers and is mostly invoked as part of the install routine of software compiling. It’s not well known among users (and I do often wonder why it got such an intuitive name, leaving mere acronyms and pet names for package managers), but <strong>install</strong> is actually a useful way to put files where you want them.</p>
<p>There are many options for the <strong>install</strong> command, including <strong>--backup</strong> and <strong>--compare</strong> command (to avoid "updating" a newer copy of a file).</p>
<p>Unlike <strong>cp</strong> and <strong>cat</strong>, but exactly like <strong>mv</strong>, the <strong>install</strong> command can copy a file while preserving its timestamp:</p>
<pre><code class="language-text">$ install --preserve-timestamp example/foo .
$ ls -l -G -g --inode
7476869 -rwxr-xr-x. 1 29545 Aug  2 07:28 foo
$ trash example/foo
</code></pre><p>Here, the file was copied to a new inode, but its <strong>mtime</strong> did not change. The permissions, however, were set to the <strong>install</strong> default of <strong>755</strong>.</p>
<p>You can use <strong>install</strong> to set the file’s permissions, owner, and group:</p>
<pre><code class="language-text">$ install --preserve-timestamp \
--owner=skenlon \
--group=dialout \
--mode=666 example/foo .
$ ls -li
7476869 -rw-rw-rw-. 1 skenlon dialout 29545 Aug  2 07:28 foo
$ trash example/foo
</code></pre><h2 id="moving-copying-and-removing">Move, copy, and remove</h2>
<p>Files contain data, and the really important files contain <em>your</em> data. Learning to manage them wisely is important, and now you have the toolkit to ensure that your data is handled in exactly the way you want.</p>
<p>Do you have a different way of managing your data? Tell us your ideas in the comments.</p>
