<p>In the <a href="https://opensource.com/article/19/3/why-devops-most-important-tech-strategy-today">first part of this article</a>, we explained why <a href="https://opensource.com/resources/devops" target="_blank">DevOps</a> is the most important strategy for modern technology organizations. To learn more about how DevOps is being used, we launched the <a href="https://lastcallmedia.com/drupal-community-devops-survey" target="_blank">2019 Drupal Community DevOps Survey</a>, which is collecting data until we present the results at <a href="https://events.drupal.org/seattle2019/sessions/devops-why-how-and-what" target="_blank">DrupalCon</a> on April 10. The survey asks Drupal community members to assess their teams across 24 DevOps dimensions using a Likert scale.</p>
<p>Each dimension in the survey is scored, averaged across all respondents, and then rolled up into an overall score. Each DevOps dimension is also scored and can be considered individually for the value it has to the team and how the team might start or improve on it. Even though the survey is still in progress as we're writing this, the results (which are updated as responses come in) already seem to be stabilizing across the Drupal community.</p>
<p> </p>
<article class="align-left media media--type-image media--view-mode-full" title="66% graphic"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/66_percent_small.png" width="125" height="112" alt="66% graphic" title="66% graphic" /></div>
      
  </article><p>The Drupal community has been floating somewhere in the mid-60s on its use of DevOps practices. While the real value is for a team to track its high-level improvement over time, it could be understood superficially as meaning the Drupal community feels like it is consistent with DevOps practices a bit more than some of the time.</p>
<h2 id="a-quick-win-opportunity-releasing-code-quickly">A quick-win opportunity: Releasing code quickly</h2>
<p>If we scan the results for a high impact, easy win, we might choose something like:</p>
<blockquote><p>How often is it safe and possible to release code to a production environment at any time?</p>
</blockquote>
<p> </p>
<article class="align-center media media--type-image media--view-mode-full" title="83% of survey respondents say codebase is in a deployable state throughout its lifecycle"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/83_percent_lifecycle.png" width="650" height="96" alt="83% of survey respondents say codebase is in a deployable state throughout its lifecycle" title="83% of survey respondents say codebase is in a deployable state throughout its lifecycle" /></div>
      
  </article><p>At 83%, most teams are well on their way and don't have much further to go on this high-impact dimension of DevOps. This means that, for many, this is a solved problem—and with some quick learning and initiative, any team could be well on its way to consistency on this dimension.</p>
<div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More DevOps resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/resources/devops?intcmp=7013a00000263HlAAI">What is DevOps?</a></li>
<li><a href="https://opensource.com/downloads/devops-hiring-guide?intcmp=7013a00000263HlAAI">The ultimate DevOps hiring guide</a></li>
<li><a href="https://opensource.com/downloads/devops-monitoring-guide?intcmp=7013a00000263HlAAI">DevOps monitoring tools guide</a></li>
<li><a href="https://opensource.com/downloads/devsecops?intcmp=7013a00000263HlAAI">Getting started with DevSecOps</a></li>
<li><a href="https://enterprisersproject.com/cheat-sheet-devops-glossary?intcmp=7013a00000263HlAAI">Download the DevOps glossary</a></li>
<li><a href="https://www.ansible.com/resources/ebooks/ansible-for-devops?intcmp=7013a00000263HlAAI">eBook: Ansible for DevOps</a></li>
<li><a href="https://opensource.com/tags/devops?src=devops_resource_menu2">Latest DevOps articles</a></li>
</ul></div>
</div>
</div>
</div>
<h3 id="why-it's-valuable-to-be-consistent">Why it's valuable to be consistent</h3>
<p>We've likely all had the experience of releasing some kind of critical bug to production. Breaking production eventually is inevitable, and DevOps prioritizes moving quickly over avoiding breakage at any cost. In this case, "moving quickly" means being able to release a hotfix right away to fix the issue. This is impossible if you have changes on your master branch that you aren't 100% sure are ready to go.</p>
<h3 id="how-to-start">How to start</h3>
<p>It's hard to beat the <a href="https://datasift.github.io/gitflow/IntroducingGitFlow.html" target="_blank">GitFlow</a> model for ensuring production can be fixed at any time. GitFlow keeps unreleased work on a separate branch (typically called "develop") until it is deployed. It is merged to the main branch (typically "master") only when it is deployed. This leaves master always clean for a hotfix and allows you to follow an abbreviated version of the normal release process during a hotfix.</p>
<p>Getting started is going to mean learning about version control and using it (if you aren't already—nearly 100% of Drupal respondents report consistently using version control in their work). From there, it's also important to get as much of your site's configuration into code, for example, Features for D7 or Configuration Management in D8 so that all work can be pulled around for review by others on the team. Finally, implementing basic continuous integration (CI) for running your test suite, along with continuous delivery (CD) to a preview environment for visual review, will decouple your ongoing feature development from the production pipeline and allow for more strategic deployments of ready branches or hotfixes as needed without jeopardizing the consistency of the master branch.</p>
<p>Fortunately, many services exist for working with Drupal in this way and can be learned and implemented to a basic degree without too much effort.</p>
<h2 id="another-quick-win-opportunity-security">Another quick-win opportunity: Security</h2>
<p>Another quick win in this 2019 Drupal Community DevOps Survey results set could be:</p>
<blockquote><p>How often are security considerations made as early as possible rather than being left to the end of the process?</p>
</blockquote>
<p> </p>
<article class="align-center media media--type-image media--view-mode-full" title="68% say security is integrated with design and testing"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/68_percent_security.png" width="650" height="95" alt="68% say security is integrated with design and testing" title="68% say security is integrated with design and testing" /></div>
      
  </article><p>Security can be part of the process while determining how features will be developed. It can also be tested like everything else. And it is clearly a high-impact improvement and should be easy to gain traction, as it starts with a mindset shift that can be learned and practiced.</p>
<h3 id="why-it's-valuable-to-be-consistent-1">Why it's valuable to be consistent</h3>
<p>When people think of security issues, they tend to think of a single "hole" an attacker slips through to cause a breach. Modern web applications have hundreds or thousands of these tiny security holes, every one of which could be the source of a security issue that could bankrupt the company. Instead of thinking of security as a boolean (secure/not secure) value, think of it as a scale. To move toward the secure end of the scale, you need to prevent or catch security issues before they make it to production.</p>
<p>To accomplish this, you will need security consciousness at every stage of the process.</p>
<h3 id="how-to-start-1">How to start</h3>
<p>On the business end, consider the types of actions you want users to be able to perform. In architecture, design for defense-in-depth. In development, educate yourself on how to use the security mechanisms provided by the framework. In QA, learn how to exploit an application and actively attempt to do so. Considering security at every stage of the process will reduce (but never eliminate) the volume of security issues the team produces.</p>
<p>In Drupal, it's important to ask questions about things like user accounts, forms, integrations, secure libraries and frameworks, and security updates. Following are some questions, using user accounts as example responses, to ask as early as possible:</p>
<ul><li>What are we working on? <em>Users can log in for special access to features.</em></li>
<li>What can go wrong? <em>The user's role is misconfigured to allow too much access, causing either business loss or technical disruption.</em></li>
<li>What can we do about it? <em>We can test the roles for misconfiguration on every commit using automated testing and continuous integration.</em></li>
<li>Did we do a good job? Do these tests cover everything? What more could be done?</li>
</ul><p>For more ideas, see <a href="https://threatmodelingbook.com/" target="_blank"><em>Threat Modeling Designing for Security</em></a>.</p>
<h3 id="how-to-improve">How to improve</h3>
<p>There are ways to automate some types of security checks. They will be specific to your environment, but some ideas that have worked well include:</p>
<ul><li>Test for XSS vulnerabilities by automatically filling your data structures with "malicious" strings then testing for whether those strings are escaped in your output.</li>
<li>Monitor your third-party dependencies for insecure libraries and alert when a security update is available. (Bonus: automate a pull request for updating the library.)</li>
<li>Include SQL injection strings (e.g., drop tables) when filling out forms during automated testing.</li>
</ul><p>In Drupal, automation offers some chances to do some incredibly robust automated tests with very little effort, if you're clever about it. For example, we can get full-site XSS coverage just by injecting XSS strings at entity load and checking various pages for leakage. There's also a lot that can be done with permissions, like verifying that nobody but the admin user has "dangerous" permissions.</p>
<p>These kinds of tests are possible because of the Drupal CMS, and we should be working as a community to make them accessible and usable to any team who wants them. Having this level of robust automated testing in Drupal enables us to respond quickly in the event of a zero-day vulnerability, since these tests will catch such issues before deployment that would have been disastrous had the release gone out.</p>
<h2 id="a-bigger-challenge-test-coverage">A bigger challenge: Test coverage</h2>
<p>The Drupal community's DevOps dimension that needed the most attention was:</p>
<p> </p>
<article class="align-center media media--type-image media--view-mode-full" title="34% say the majority of primary business value has test coverage and tests are run when code is committed"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/34_percent_testing.png" width="650" height="98" alt="34% say the majority of primary business value has test coverage and tests are run when code is committed" title="34% say the majority of primary business value has test coverage and tests are run when code is committed" /></div>
      
  </article><p>When features are developed, tests can be written to verify whether they are providing business value. How often are the tests written in a way that allows them to be run in a CI environment?</p>
<p>You'll likely need some pretty good momentum going on your team to tackle this one, but it is well worth it.</p>
<h3 id="how-to-start-2">How to start</h3>
<p>When determining which features need to be tested, consider which ones would cause unplanned work if they broke. For example, if you are creating a recipe database and one of your features is a recipe-entry form, you would want to test that recipes can be entered and the form can be submitted because, without that feature, your application would no longer provide value to your users.</p>
<p>Once your tests are written, they will provide the most value if they are run in a CI system on every commit rather than on a schedule or in an ad-hoc way. This shortens the time it takes for a developer to become aware of a breakage they introduced—the goal is that they become aware of the breakage before they move to the next task so they won't have to switch contexts to come back to fix it.</p>
<h3 id="how-to-improve-1">How to improve</h3>
<p>Even if you have an excellent manual-review process, including functional testing by a human, it will quickly become impossible to manually check every feature of your application. Instead, you should be looking to automated testing to cover (at minimum) your business-critical features. The value of this testing will multiply with the number of developers on the project, the number of commits pushed daily, and the number of business-critical features you have.</p>
<p>There are many options when it comes to testing, but here are two types that people generally use:</p>
<ul><li><strong>Unit:</strong> Exhaustive tests of a single "unit" of code (function, class, etc.). We often do less of this kind of testing for an application but leverage it for particularly complicated or important bits of code. Also, unit testing can force good design practices.</li>
<li><strong>Functional:</strong> Tests of a piece of functionality as seen by a user, such as user log in. This type of testing can cover many different code paths, but typically it doesn't cover them exhaustively. This is a good choice for applications, and it is typically available using "behavioral" frameworks like Behat or Cucumber.</li>
</ul><p>For Drupal, be sure to learn about <a href="https://gitlab.com/weitzman/drupal-test-traits" target="_blank">Drupal Test Traits</a>, which is a collection of traits "for testing Drupal sites that have user content (versus unpopulated sites)" by <a href="http://weitzman.github.io/" target="_blank">Moshe Weitzman</a>, <a href="https://www.drupal.org/u/jibran" target="_blank">Jibran Ijaz</a>, and <a href="https://github.com/rbayliss" target="_blank">Rob Bayliss</a>. DTT was built to make functional testing easy because it operates on your installed site, so you don't need to worry about mocking up a site install from scratch with every test. It also provides tests that naturally evolve with your site since it uses the configuration you have today, not the configuration you wrote into the test.</p>
<h2 id="conclusion">Conclusion</h2>
<p>Having an understanding of the context and history of DevOps will mean the difference between following and leading in an industry. If there's still time, please participate in the <a href="https://lastcallmedia.com/drupal-community-devops-survey" target="_blank">Drupal Community DevOps Survey</a> so we can include your input in the results. </p>
<hr /><p><em>Rob</em><em> Bayliss and Kelly Albrecht will present the results of the 2019 Drupal Community DevOps Survey in their presentation <a href="https://events.drupal.org/seattle2019/sessions/devops-why-how-and-what" target="_blank">DevOps: Why, How, and What</a> and host a follow-up <a href="https://events.drupal.org/seattle2019/bofs/devops-getting-started" target="_blank">Birds of a Feather discussion</a> at <a href="https://events.drupal.org/seattle2019" target="_blank">DrupalCon 2019</a> in Seattle, April 8-12.</em></p>
<hr />