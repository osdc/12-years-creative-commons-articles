<p>I've been ripping CDs to my computer for a long time now. Over that time, I've used several different tools for ripping, and I have observed that each tool seems to have a different take on tagging, specifically, what metadata to save with the music data. By "observed," I mean that music players seem to sort albums in a funny order, they split tracks in one physical directory into two albums, or they create other sorts of frustrating irritations.</p>
<p>I've also learned that some of the tags are pretty obscure, and many music players and tag editors don't show them. Even so, they may use them for sorting or displaying music in some edge cases, like where the player separates all the music files containing tag XYZ into a different album from all the files not containing that tag.</p>
<p>So if the tagging applications and music players don't show the "weirdo" tags—but are somehow affected by them—what can you do?</p>
<h2 id="metaflac-to-the-rescue">Metaflac to the rescue!</h2>
<p>I have been meaning to get familiar with <strong><a href="https://xiph.org/flac/documentation_tools_metaflac.html" target="_blank">metaflac</a></strong>, the open source command-line metadata editor for <a href="https://xiph.org/flac/index.html" target="_blank">FLAC files</a>, which is my open source music file format of choice. Not that there is anything wrong with great tag-editing software like <a href="https://wiki.gnome.org/Apps/EasyTAG" target="_blank">EasyTAG</a>, but the old saying "if all you have is a hammer…" comes to mind. Also, from a practical perspective, my home and office stereo music needs are met by small, dedicated servers running <a href="https://www.armbian.com/" target="_blank">Armbian</a> and <a href="https://www.musicpd.org/" target="_blank">MPD</a>, with the music files stored locally, running a very stripped-down, music-only headless environment, so a command-line metadata management tool would be quite useful.</p>
<p>The screenshot below shows the typical problem created by my long-term ripping program: Putumayo's wonderful compilation of Colombian music appears as two separate albums, one containing a single track, the other containing the remaining 11:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Album with incorrect tags"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/music-tags1_before.png" width="350" height="187" alt="Album with incorrect tags" title="Album with incorrect tags" /></div>
      
  </article></p>
<p>I used metaflac to generate a list of all the tags for all of the FLAC files in the directory containing those tracks:</p>
<pre><code class="language-text">rm -f tags.txt
for f in *.flac; do
	echo $f &gt;&gt; tags.txt
	metaflac --export-tags-to=tags.tmp "$f"
	cat tags.tmp &gt;&gt; tags.txt
	rm tags.tmp
done</code></pre><p>I saved this as an executable shell script (see my colleague <a href="https://opensource.com/users/dboth">David Both</a>'s wonderful series of columns on Bash shell scripting, <a href="https://opensource.com/article/19/10/programming-bash-loops">particularly the one on loops</a>). Basically, what I'm doing here is creating a file, <em>tags.txt</em>, containing the filename (the <strong>echo </strong>command) followed by all its flags, followed by the next filename, and so forth. Here are the first few lines of the result:</p>
<pre><code class="language-text">A Guapi.flac
TITLE=A Guapi
ARTIST=Grupo Bahia
ALBUMARTIST=Various Artists
ALBUM=Putumayo Presents: Colombia
DATE=2001
TRACKTOTAL=12
GENRE=Latin Salsa
MUSICBRAINZ_ALBUMARTISTID=89ad4ac3-39f7-470e-963a-56509c546377
MUSICBRAINZ_ALBUMID=6e096386-1655-4781-967d-f4e32defb0a3
MUSICBRAINZ_ARTISTID=2993268d-feb6-4759-b497-a3ef76936671
DISCID=900a920c
ARTISTSORT=Grupo Bahia
MUSICBRAINZ_DISCID=RwEPU0UpVVR9iMP_nJexZjc_JCc-
COMPILATION=1
MUSICBRAINZ_TRACKID=8a067685-8707-48ff-9040-6a4df4d5b0ff
ALBUMARTISTSORT=50 de Joselito, Los
Cumbia Del Caribe.flac</code></pre><p>After a bit of investigation, it turns out I ripped a number of my Putumayo CDs at the same time, and whatever software I was using at the time seems to have put the MUSICBRAINZ_ tags on all but one of the files. (A bug? Probably; I see this on a half-dozen albums.) Also, with respect to the sometimes unusual sorting, note the ALBUMARTISTSORT tag moved the Spanish article "Los" to the end of the artist name, after a comma.</p>
<p>I used a simple <strong>awk </strong>script to list all the tags reported in the <em>tags.txt</em> file:</p>
<pre><code class="language-bash">awk -F= 'index($0,"=") &gt; 0 {print $1}' tags.txt | sort -u</code></pre><p>This split all lines into fields using <strong>=</strong> as the field separator and prints the first field of lines containing an equals sign. The results are passed through sort with the <strong>-u</strong> flag, which eliminates all duplication in the output (see my colleague Seth Kenlon's great <a href="https://opensource.com/article/19/10/get-sorted-sort">article on the <strong>sort</strong> utility</a>). For this specific <em>tags.txt</em> file, the output is:</p>
<pre><code class="language-text">ALBUM
ALBUMARTIST
ALBUMARTISTSORT
ARTIST
ARTISTSORT
COMPILATION
DATE
DISCID
GENRE
MUSICBRAINZ_ALBUMARTISTID
MUSICBRAINZ_ALBUMID
MUSICBRAINZ_ARTISTID
MUSICBRAINZ_DISCID
MUSICBRAINZ_TRACKID
TITLE
TRACKTOTAL</code></pre><p>Sleuthing around a bit, I found that the MUSICBRAINZ_ flags appear on all but one FLAC file, so I used the metaflac command to delete those flags:</p>
<pre><code class="language-text">for f in *.flac; do metaflac --remove-tag MUSICBRAINZ_ALBUMARTISTID "$f"; done
for f in *.flac; do metaflac --remove-tag MUSICBRAINZ_ALBUMID "$f"; done
for f in *.flac; do metaflac --remove-tag MUSICBRAINZ_ARTISTID "$f"; done
for f in *.flac; do metaflac --remove-tag MUSICBRAINZ_DISCID "$f"; done
for f in *.flac; do metaflac --remove-tag MUSICBRAINZ_TRACKID "$f"; done</code></pre><p>Once that's done, I can rebuild the MPD database with my music player. Here are the results:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Album with correct tags"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/music-tags2_after.png" width="350" height="155" alt="Album with correct tags" title="Album with correct tags" /></div>
      
  </article></p>
<p>And, there we are—all 12 tracks together in one album.</p>
<p>So, yeah, I'm lovin' metaflac a whole bunch. I expect I'll be using it more often as I try to wrangle the last bits of weirdness in my music collection's music tags. It's highly recommended!</p>
<h2 id="and-the-music">And the music</h2>
<p>I've been spending a few evenings listening to Odario Williams' program <em>After Dark</em> on CBC Music. (CBC is Canada's public broadcasting corporation.) Thanks to Odario, one of the albums I've really come to enjoy is <a href="https://burlingtonpac.ca/events/kevin-fox/" target="_blank"><em>Songs for Cello and Voice</em> by Kevin Fox</a>. Here he is, covering the Eurythmics tune "<a href="https://www.youtube.com/watch?v=uyN66XI1zp4" target="_blank">Sweet Dreams (Are Made of This)</a>."</p>
<p><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/uyN66XI1zp4" width="560"></iframe></p>
<p>I bought this on CD, and now it's on my music server with its tags properly organized!</p>
