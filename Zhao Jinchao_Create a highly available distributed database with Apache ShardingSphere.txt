<p>Modern business systems must be highly available, reliable, and stable in the digital age. As the cornerstone of the current business system, databases are supposed to embrace high availability.</p>
<p>High availability (HA) allows databases to switch services between primary and secondary database nodes. HA automatically selects a primary, picking the best node when the previous one crashes.</p>
<h2>MySQL high availability</h2>
<p>There are plenty of <a href="https://www.mysql.com/products/enterprise/high_availability.html" target="_blank">MySQL high availability options</a>, each with pros and cons. Below are several common high availability options:</p>
<ul><li><a href="https://github.com/openark/orchestrator" target="_blank">Orchestrator</a> is a MySQL HA and replication topology management tool written in Go. Its advantage lies in its support for manual adjustment of the primary-secondary topology, automatic failover, and automatic or manual recovery of primary nodes through a graphical web console. However, the program needs to be deployed separately and has a steep learning curve due to its complex configurations.</li>
<li><a href="https://www.percona.com/blog/2016/09/02/mha-quickstart-guide/" target="_blank">MHA</a> is another mature solution. It provides primary/secondary switching and failover capabilities. The good thing about it is that it can ensure the least data loss in the switching process and works with semi-synchronous and asynchronous replication frameworks. However, only the primary node is monitored after MHA starts, and MHA doesn't provide the load balancing feature for the read database.</li>
<li><a href="https://dev.mysql.com/doc/refman/8.0/en/group-replication.html" target="_blank">MGR</a> implements group replication based on the distributed Paxos protocol to ensure data consistency. It is an official HA component provided by MySQL, and no extra deployment program is required. Instead, users only need to install the MGR plugin on each data source node. The tool features high consistency, fault tolerance, scalability, and flexibility.</li>
</ul><h2>Apache ShardingSphere high availability</h2>
<p>Apache ShardingSphere's architecture actually separates storage from computing. The storage node represents the underlying database, such as MySQL, PostgreSQL, openGauss, etc., while compute node refers to <a href="https://shardingsphere.apache.org/document/current/en/quick-start/shardingsphere-jdbc-quick-start/" target="_blank">ShardingSphere-JDBC</a> or <a href="https://shardingsphere.apache.org/document/current/en/quick-start/shardingsphere-proxy-quick-start/" target="_blank">ShardingSphere-Proxy</a>.</p>
<p>Accordingly, the <a href="https://shardingsphere.apache.org/document/current/en/features/ha/" target="_blank">high availability solutions</a> for storage nodes and compute nodes are different. Stateless compute nodes need to perceive the changes in storage nodes. They also need to set up separate load balancers and have the capabilities of service discovery and request distribution. Stateful storage nodes must provide data synchronization, connection testing, primary node election, and so on.</p>
<p>Although ShardingSphere doesn't provide a database with high availability, it can help users integrate database HA solutions such as primary-secondary switchover, faults discovery, traffic switching governance, and so on with the help of the database HA and through its capabilities of database discovery and dynamic perception.</p>
<p>When combined with the primary-secondary flow control feature in distributed scenarios, ShardingSphere can provide better high availability read/write splitting solutions. It will be easier to operate and manage ShardingSphere clusters using <a href="https://opensource.com/article/21/9/distsql" target="_blank">DistSQL</a>'s dynamic high availability adjustment rules to get primary/secondary nodes' information.</p>
<p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">More on edge computing</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://www.redhat.com/en/topics/edge-computing?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Understanding edge computing">Understanding edge computing</a></div>
              <div class="field__item"><a href="https://opensource.com/article/21/2/linux-edge-computing?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Why Linux is critical to edge computing">Why Linux is critical to edge computing</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/kubernetes-raspberry-pi?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="eBook: Running Kubernetes on your Raspberry Pi">eBook: Running Kubernetes on your Raspberry Pi</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/engage/automated-enterprise-ebook-20171115?intcmp=701f2000000h4RcAAI" data-analytics-category="resource list" data-analytics-text="Download now: The automated enterprise eBook">Download now: The automated enterprise eBook</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/home-automation-ebook?intcmp=701f2000000h4RcAAI" data-analytics-category="resource list" data-analytics-text="eBook: A practical guide to home automation using open source tools">eBook: A practical guide to home automation using open source tools</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/engage/automation-at-edge-20220727?intcmp=701f2000000h4RcAAI" data-analytics-category="resource list" data-analytics-text="eBook: 7 examples of automation on the edge">eBook: 7 examples of automation on the edge</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/topics/edge-computing/what-is-edge-machine-learning?intcmp=701f2000000h4RcAAI" data-analytics-category="resource list" data-analytics-text="What is edge machine learning?">What is edge machine learning?</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text=" Register for your free Red Hat account"> Register for your free Red Hat account</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/edge-computing?intcmp=701f2000000h4RcAAI" data-analytics-category="resource list" data-analytics-text="The latest on edge">The latest on edge</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<h3>Best practices</h3>
<p>Apache ShardingSphere adopts a plugin-oriented architecture so that you can use all its enhanced capabilities independently or together. Its high availability function is often used with read/write splitting to distribute query requests to the secondary databases according to the load balancing algorithm to ensure system HA, relieve primary database pressure, and improve business system throughput.</p>
<p>Note that ShardingSphere HA implementation leans on its distributed governance capability. Therefore, it can only be used under the cluster mode for the time being. Meanwhile, read/write splitting rules are revised in ShardingSphere 5.1.0. Please refer to the official documentation about <a href="https://shardingsphere.apache.org/document/current/cn/user-manual/shardingsphere-jdbc/yaml-config/rules/readwrite-splitting/" target="_blank">read/write splitting</a> for details.</p>
<p>Consider the following HA+read/write splitting configuration with ShardingSphere <a href="https://shardingsphere.apache.org/document/current/cn/user-manual/shardingsphere-proxy/distsql/syntax/ral/" target="_blank">DistSQL RAL</a> statements as an example. The example begins with the configuration, requirements, and initial SQL.</p>
<h3>Configuration</h3>
<pre>
<code class="language-text">schemaName: database_discovery_db

dataSources:
  ds_0:
    url: jdbc:mysql://127.0.0.1:1231/demo_primary_ds?serverTimezone=UTC&amp;useSSL=false
    username: root
    password: 123456
    connectionTimeoutMilliseconds: 3000
    idleTimeoutMilliseconds: 60000
    maxLifetimeMilliseconds: 1800000
    maxPoolSize: 50
    minPoolSize: 1
  ds_1:
    url: jdbc:mysql://127.0.0.1:1232/demo_primary_ds?serverTimezone=UTC&amp;useSSL=false
    username: root
    password: 123456
    connectionTimeoutMilliseconds: 3000
    idleTimeoutMilliseconds: 60000
    maxLifetimeMilliseconds: 1800000
    maxPoolSize: 50
    minPoolSize: 1
  ds_2:
    url: jdbc:mysql://127.0.0.1:1233/demo_primary_ds?serverTimezone=UTC&amp;useSSL=false
    username: root
    password: 123456
    connectionTimeoutMilliseconds: 3000
    idleTimeoutMilliseconds: 50000
    maxLifetimeMilliseconds: 1300000
    maxPoolSize: 50
    minPoolSize: 1

rules:
  - !READWRITE_SPLITTING
    dataSources:
      replication_ds:
        type: Dynamic
        props:
          auto-aware-data-source-name: mgr_replication_ds
  - !DB_DISCOVERY
    dataSources:
      mgr_replication_ds:
        dataSourceNames:
          - ds_0
          - ds_1
          - ds_2
        discoveryHeartbeatName: mgr-heartbeat
        discoveryTypeName: mgr
    discoveryHeartbeats:
      mgr-heartbeat:
        props:
          keep-alive-cron: '0/5 * * * * ?'
    discoveryTypes:
      mgr:
        type: MGR
        props:
          group-name: b13df29e-90b6-11e8-8d1b-525400fc3996</code></pre><h3>Requirements</h3>
<ul><li>ShardingSphere-Proxy 5.1.0 (Cluster mode + HA + dynamic read/write splitting rule)</li>
<li>Zookeeper 3.7.0</li>
<li>MySQL MGR cluster</li>
</ul><h3>SQL script</h3>
<pre>
<code class="language-sql">CREATE TABLE `t_user` (
  `id` int(8) NOT NULL,
  `mobile` char(20) NOT NULL,
  `idcard` varchar(18) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;</code></pre><p>First, view the primary-secondary relationship:</p>
<pre>
<code class="language-sql">mysql&gt; SHOW READWRITE_SPLITTING RULES;
+----------------+-----------------------------+------------------------+------------------------+--------------------+---------------------+
| name           | auto_aware_data_source_name | write_data_source_name | read_data_source_names | load_balancer_type | load_balancer_props |
+----------------+-----------------------------+------------------------+------------------------+--------------------+---------------------+
| replication_ds | mgr_replication_ds          | ds_0                   | ds_1,ds_2              | NULL               |                     |
+----------------+-----------------------------+------------------------+------------------------+--------------------+---------------------+
1 row in set (0.09 sec)</code></pre><p>You can also look at the secondary database state:</p>
<pre>
<code class="language-sql">mysql&gt; SHOW READWRITE_SPLITTING READ RESOURCES;
+----------+---------+
| resource | status  |
+----------+---------+
| ds_1     | enabled |
| ds_2     | enabled |
+----------+---------+</code></pre><p>The results above show that the primary database is currently <strong>ds_0</strong>, while secondary databases are <strong>ds_1</strong> and <strong>ds_2</strong>.</p>
<p>Next, test <strong>INSERT</strong>:</p>
<pre>
<code class="language-sql">mysql&gt; INSERT INTO t_user(id, mobile, idcard) value (10000, '13718687777', '141121xxxxx');
Query OK, 1 row affected (0.10 sec)</code></pre><p>View the ShardingSphere-Proxy log and see if the route node is the primary database ds_0.</p>
<pre>
<code class="language-text">[INFO ] 2022-02-28 15:28:21.495 [ShardingSphere-Command-2] ShardingSphere-SQL - Logic SQL: INSERT INTO t_user(id, mobile, idcard) value (10000, '13718687777', '141121xxxxx')
[INFO ] 2022-02-28 15:28:21.495 [ShardingSphere-Command-2] ShardingSphere-SQL - SQLStatement: MySQLInsertStatement(setAssignment=Optional.empty, onDuplicateKeyColumns=Optional.empty)
[INFO ] 2022-02-28 15:28:21.495 [ShardingSphere-Command-2] ShardingSphere-SQL - Actual SQL: ds_0 ::: INSERT INTO t_user(id, mobile, idcard) value (10000, '13718687777', '141121xxxxx')</code></pre><p>Now test <strong>SELECT</strong> (repeat it twice):</p>
<pre>
<code class="language-sql">mysql&gt; SELECT id, mobile, idcard FROM t_user WHERE id = 10000;
</code></pre><p>View the ShardingSphere-Proxy log and see if the route node is <strong>ds_1</strong> or <strong>ds_2</strong>.</p>
<pre>
<code class="language-text">[INFO ] 2022-02-28 15:34:07.912 [ShardingSphere-Command-4] ShardingSphere-SQL - Logic SQL: SELECT id, mobile, idcard FROM t_user WHERE id = 10000
[INFO ] 2022-02-28 15:34:07.913 [ShardingSphere-Command-4] ShardingSphere-SQL - SQLStatement: MySQLSelectStatement(table=Optional.empty, limit=Optional.empty, lock=Optional.empty, window=Optional.empty)
[INFO ] 2022-02-28 15:34:07.913 [ShardingSphere-Command-4] ShardingSphere-SQL - Actual SQL: ds_1 ::: SELECT id, mobile, idcard FROM t_user WHERE id = 10000
[INFO ] 2022-02-28 15:34:21.501 [ShardingSphere-Command-4] ShardingSphere-SQL - Logic SQL: SELECT id, mobile, idcard FROM t_user WHERE id = 10000
[INFO ] 2022-02-28 15:34:21.502 [ShardingSphere-Command-4] ShardingSphere-SQL - SQLStatement: MySQLSelectStatement(table=Optional.empty, limit=Optional.empty, lock=Optional.empty, window=Optional.empty)
[INFO ] 2022-02-28 15:34:21.502 [ShardingSphere-Command-4] ShardingSphere-SQL - Actual SQL: ds_2 ::: SELECT id, mobile, idcard FROM t_user WHERE id = 10000</code></pre><h2>Switch to the primary database</h2>
<p>Close the primary database <strong>ds_0</strong>:</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-12/close-master.png" width="832" height="320" alt="Close primary database" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Zhao Jinchao, CC BY-SA 4.0)</p>
</div>
      
  </article><p>View whether the primary database has changed and if the secondary database state is correct through DistSQL:</p>
<pre>
<code class="language-text">[INFO ] 2022-02-28 15:34:07.912 [ShardingSphere-Command-4] ShardingSphere-SQL - Logic SQL: SELECT id, mobile, idcard FROM t_user WHERE id = 10000
[INFO ] 2022-02-28 15:34:07.913 [ShardingSphere-Command-4] ShardingSphere-SQL - SQLStatement: MySQLSelectStatement(table=Optional.empty, limit=Optional.empty, lock=Optional.empty, window=Optional.empty)
[INFO ] 2022-02-28 15:34:07.913 [ShardingSphere-Command-4] ShardingSphere-SQL - Actual SQL: ds_1 ::: SELECT id, mobile, idcard FROM t_user WHERE id = 10000
[INFO ] 2022-02-28 15:34:21.501 [ShardingSphere-Command-4] ShardingSphere-SQL - Logic SQL: SELECT id, mobile, idcard FROM t_user WHERE id = 10000
[INFO ] 2022-02-28 15:34:21.502 [ShardingSphere-Command-4] ShardingSphere-SQL - SQLStatement: MySQLSelectStatement(table=Optional.empty, limit=Optional.empty, lock=Optional.empty, window=Optional.empty)
[INFO ] 2022-02-28 15:34:21.502 [ShardingSphere-Command-4] ShardingSphere-SQL - Actual SQL: ds_2 ::: SELECT id, mobile, idcard FROM t_user WHERE id = 10000</code></pre><p>Now, <strong>INSERT</strong> another line of data:</p>
<pre>
<code class="language-sql">mysql&gt; INSERT INTO t_user(id, mobile, idcard) value (10001, '13521207777', '110xxxxx');
Query OK, 1 row affected (0.04 sec)</code></pre><p>View the ShardingSphere-Proxy log and see if the route node is the primary database <strong>ds_1</strong>:</p>
<pre>
<code class="language-text">[INFO ] 2022-02-28 15:40:26.784 [ShardingSphere-Command-6] ShardingSphere-SQL - Logic SQL: INSERT INTO t_user(id, mobile, idcard) value (10001, '13521207777', '110xxxxx')
[INFO ] 2022-02-28 15:40:26.784 [ShardingSphere-Command-6] ShardingSphere-SQL - SQLStatement: MySQLInsertStatement(setAssignment=Optional.empty, onDuplicateKeyColumns=Optional.empty)
[INFO ] 2022-02-28 15:40:26.784 [ShardingSphere-Command-6] ShardingSphere-SQL - Actual SQL: ds_1 ::: INSERT INTO t_user(id, mobile, idcard) value (10001, '13521207777', '110xxxxx')</code></pre><p>Finally, test <strong>SELECT</strong>(repeat it twice):</p>
<pre>
<code class="language-sql">mysql&gt; SELECT id, mobile, idcard FROM t_user WHERE id = 10001;</code></pre><p>View the ShardingSphere-Proxy log and see if the route node is <strong>ds_2</strong>:</p>
<pre>
<code class="language-text">[INFO ] 2022-02-28 15:42:00.651 [ShardingSphere-Command-7] ShardingSphere-SQL - Logic SQL: SELECT id, mobile, idcard FROM t_user WHERE id = 10001
[INFO ] 2022-02-28 15:42:00.651 [ShardingSphere-Command-7] ShardingSphere-SQL - SQLStatement: MySQLSelectStatement(table=Optional.empty, limit=Optional.empty, lock=Optional.empty, window=Optional.empty)
[INFO ] 2022-02-28 15:42:00.651 [ShardingSphere-Command-7] ShardingSphere-SQL - Actual SQL: ds_2 ::: SELECT id, mobile, idcard FROM t_user WHERE id = 10001
[INFO ] 2022-02-28 15:42:02.148 [ShardingSphere-Command-7] ShardingSphere-SQL - Logic SQL: SELECT id, mobile, idcard FROM t_user WHERE id = 10001
[INFO ] 2022-02-28 15:42:02.149 [ShardingSphere-Command-7] ShardingSphere-SQL - SQLStatement: MySQLSelectStatement(table=Optional.empty, limit=Optional.empty, lock=Optional.empty, window=Optional.empty)
[INFO ] 2022-02-28 15:42:02.149 [ShardingSphere-Command-7] ShardingSphere-SQL - Actual SQL: ds_2 ::: SELECT id, mobile, idcard FROM t_user WHERE id = 10001</code></pre><h2>Release the secondary databases</h2>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-12/release-secondary.png" width="832" height="496" alt="Release the secondary database" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Zhao Jinchao, CC BY-SA 4.0)</p>
</div>
      
  </article><p>View the latest primary-secondary relationship changes through DistSQL. The state of the <strong>ds_0</strong> node is recovered as enabled, while <strong>ds_0</strong> is integrated to <strong>read_data_source_names</strong>:</p>
<pre>
<code class="language-sql">mysql&gt; SHOW READWRITE_SPLITTING RULES;
+----------------+-----------------------------+------------------------+------------------------+--------------------+---------------------+
| name           | auto_aware_data_source_name | write_data_source_name | read_data_source_names | load_balancer_type | load_balancer_props |
+----------------+-----------------------------+------------------------+------------------------+--------------------+---------------------+
| replication_ds | mgr_replication_ds          | ds_1                   | ds_0,ds_2              | NULL               |                     |
+----------------+-----------------------------+------------------------+------------------------+--------------------+---------------------+
1 row in set (0.01 sec)

mysql&gt; SHOW READWRITE_SPLITTING READ RESOURCES;
+----------+---------+
| resource | status  |
+----------+---------+
| ds_0     | enabled |
| ds_2     | enabled |
+----------+---------+
2 rows in set (0.00 sec)</code></pre><h2>Wrap up</h2>
<p>Database high availability is critical in today's business environments, and Apache ShardingSphere can help provide the necessary reliability. Based on the above example, you now know more about ShardingSphere's high availability and dynamic read/write splitting. Use this example as the basis for your own configurations. </p>
