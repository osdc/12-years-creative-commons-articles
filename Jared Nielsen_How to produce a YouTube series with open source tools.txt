<p>
  <a href="http://csedweek.org" target="_blank">Computer Science Education Week</a> is December 7-13. To mark the occasion, <a href="http://www.dototot.com/" target="_blank">Dototot</a> is launching a new series of <em>Hello World</em> videos covering the basics of computer science. The 10 episodes follow Unique ID, the highly intelligent robot host of <em>The Hello World Program</em>, on adventures exploring a range of topics, from binary to artificial intelligence. The new videos incorporate a wide range of media, from traditional hand-drawn animation and stop-motion to <a href="https://opensource.com/resources/what-arduino" target="_blank">Arduino</a>-powered robots and 3D CG.
</p>
<iframe width="520" height="315" src="https://www.youtube.com/embed/bJKltx0MFmI" frameborder="0" allowfullscreen=""></iframe><p>
  Many of our most memorable educational experiences involved hands-on activities. As we developed <em>The Hello World Program</em>, we sought to integrate the crafts we learned as youth, stitching together analog and digital media. Papercraft and programming may seem worlds apart, but the underlying principles of procedural problem-solving, conceptual realization, and critical thinking are the same. Not everyone needs to be a computer scientist, but in this day and age, we think everyone should understand the basics. Our goal is to make the fundamentals of computer science accessible and entertaining to kids of all ages.
</p>
<p>
  We initially set out to produce <em>The Hello World Program</em> entirely with free and open source software, which presented us with a variety of challenges. Not all professional media production software has a FOSS equivalent, requiring a bit of flexibility and ingenuity on our part. We advocate FOSS because it enables anyone of any age, background, or budget to create professional media with minimal equipment and a handful of craft supplies.
</p>
<h2>
  From RMS to Aramis<br /></h2>
<article class="media media--type-image media--view-mode-full"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/life-uploads/dototot_craftworks_rms_aramis.png" width="520" height="260" /></div>
      
  </article><div align="center"><sup>Richard Stallman photo by <a href="https://commons.wikimedia.org/wiki/File:Richard_Stallman_-_F%C3%AAte_de_l%27Humanit%C3%A9_2014_-_010.jpg" target="_blank">Thesupermat</a>. <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank">CC BY-SA 4.0</a>. Aramis photo courtesy of dototot.com. <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/" target="_blank">CC BY-NC-SA 4.0</a>.</sup></div>
<p>
  Whether it's prototyping a puppet or programming a platform, both are iterative processes requiring incremental testing during development. We always start every project with pencil and paper sketches. Here you can see our first design attempts for developing Aramis, one of the stars of <em>Superusers: The Legendary GNU/Linux Show</em>.
</p>
<iframe width="520" height="315" src="https://www.youtube.com/embed/5qAooVa5dfA" frameborder="0" allowfullscreen=""></iframe><article class="media media--type-image media--view-mode-full"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/life-uploads/dototot_craftworks_puppet_pattern.png" width="520" height="347" /></div>
      
  </article><div align="center"><sup>Courtesy of dototot.com. <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/" target="_blank">CC BY-NC-SA 4.0</a>.</sup></div>
<p>
  After a lot of trial and error, we scan our sketch patterns and import them into Inkscape, where we create a final design. <a href="http://www.inkscape.org" target="_blank">Inkscape</a> provides a user-friendly vector graphics editor for a variety of design situations from print media to websites and animations. It is similar to Adobe Illustrator, only free and open source.
</p>
<h2>
  Art + Math = 3D CGI<br /></h2>
<div align="center">
<article class="media media--type-image media--view-mode-full"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/life-uploads/dototot_bios.gif" width="500" height="500" /></div>
      
  </article><p><sup>Courtesy of dototot.com. <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/" target="_blank">CC BY-NC-SA 4.0</a>.</sup></p></div>

<p>
  We consider drawing to be the most important skill a maker needs to know. You don't have to be good, you just have to do it (but the secret is, the more you do it, the better you get). Even when modeling in 3D, we begin with a sketch. This is BIOS, a new character inspired by the Cheshire Cat and appearing in the forthcoming episode, <em>What's Inside My Computer?</em>, designed and rendered in Blender. <a href="http://blender.org/" target="_blank">Blender</a> is a remarkably powerful, free and open source 3D modeling and animation program. It can also be used as a video editor and compositor, and even comes equipped with its own game engine. It rivals proprietary 3D animation applications in terms of capabilities, but has a notoriously steep learning curve.
</p>
<iframe width="520" height="315" src="https://www.youtube.com/embed/IeEQcEJuvso" frameborder="0" allowfullscreen=""></iframe><p>
  We don't do all of our sketching with pencil and paper, though. We recently fell in love with <a href="https://krita.org/" target="_blank">Krita</a>. The drawings in our Mighty Boosh-inspired animation are an even split of Krita .pngs and hand-drawn illustrations in marker on vellum, photographed and white-balanced in GIMP. <a href="http://www.gimp.org" target="_blank">GIMP</a> is easily the most popular FOSS image manipulation program. It is every bit as powerful as Photoshop, but generally requires a few extra steps to achieve the same results Photoshop users can with the click of a button. It can be used as a digital painting canvas, but the excellent <a href="http://www.krita.org" target="_blank">Krita</a> is better suited for that.
</p>
<h2>
  Puppets: The original robot<br /></h2>
<div align="center">
<article class="media media--type-image media--view-mode-full"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/life-uploads/dototot_craftpunk.gif" width="500" height="500" /></div>
      
  </article><p><sup>Courtesy of dototot.com. <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/" target="_blank">CC BY-NC-SA 4.0</a>.</sup></p></div>

<p>
  For the soon-to-be-released episode, <em>Powers of 2</em>, we designed and built robot puppets (rather than puppet robots), using Arduinos, servos, LEDs, and cardboard. We modeled these robots on Daft Punk and refer to them as "Craft Punk." We mocked up the heads in Blender and unwrapped, exported, and printed the 3D meshes to trace and cut in cardboard, which we then assembled with a lot of wire and hot glue.
</p>
<article class="media media--type-image media--view-mode-full"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/life-uploads/dototot_craftworks_craftpunk_blender.png" width="520" height="317" /></div>
      
  </article><div align="center"><sup>Courtesy of dototot.com. <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/" target="_blank">CC BY-NC-SA 4.0</a>.</sup></div>
<p>
  The electronics of each robot are an Arduino, 30 or so LEDS, and two servos, one for the head and one for the hand. The script is relatively simple, consisting of a half-dozen functions combining Arduino basics such as sweeps for the LEDs and servos.
</p>
<div align="center">
<article class="media media--type-image media--view-mode-full"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/life-uploads/dototot_bangalter.gif" width="500" height="500" /></div>
      
  </article><p><sup>Courtesy of dototot.com. <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/" target="_blank">CC BY-NC-SA 4.0</a>.</sup></p></div>

<h2>
  Material design<br /></h2>
<div align="center">
<article class="media media--type-image media--view-mode-full"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/life-uploads/dototot_link_0.gif" width="400" height="225" /></div>
      
  </article><p><sup>Courtesy of dototot.com. <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/" target="_blank">CC BY-NC-SA 4.0</a>.</sup></p></div>

<p>
  Our HTML series, <em>Daisy's Web Development Diary</em>, is a video diary hosted by Daisy the fiery fox, featuring stop-motion papercraft animations. Because building a web page with blocky HTML elements and adjusting the style with CSS is like highly technical digital scrapbooking, the papercraft aesthetic was a natural fit for the series.
</p>
<p>
  Our upcoming programming episode celebrates the amazing <a href="https://en.wikipedia.org/wiki/Grace_Hopper" target="_blank">Grace Hopper</a> in the form of a stop-motion animated origami grasshopper. After schooling hosts Unique ID and Guido on the human condition, Grace hops through a series of surreal, construction paper landscapes.
</p>
<article class="media media--type-image media--view-mode-full"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/life-uploads/grasshopper_0.gif" width="520" height="293" /></div>
      
  </article><div align="center"><sup>Courtesy of dototot.com. <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/" target="_blank">CC BY-NC-SA 4.0</a>.</sup></div>
<p>
  These scenes were shot with multiple origami grasshoppers suspended on wire structures that were later masked in <a href="https://www.lwks.com/" target="_blank">Lightworks</a>.
</p>
<article class="media media--type-image media--view-mode-full"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/life-uploads/dototot_grace_hopper_rig_0.jpg" width="520" height="341" /></div>
      
  </article><div align="center"><sup>Courtesy of dototot.com. <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/" target="_blank">CC BY-NC-SA 4.0</a>.</sup></div>
<p>
  Stop-motion animation mimics the illusion of motion created in videos by staging each and every frame. Because this process is so simple, it is a very accessible medium. You only need a camera and some objects to animate, however your camera must have manual controls for every setting to maintain a consistent exposure. Additionally, lights and a tripod are almost a necessity. Even the slightest change in position, lighting, or exposure will cause a dramatic change in the finished video.
</p>
<p>
  These potential pitfalls of stop-motion animation make traditional hand-drawn animation even more accessible. The only equipment necessary to produce pencil test animations is a scanner and a stack of printer paper. This process is extremely labor intensive, as each frame must be painstakingly drawn by hand.
</p>
<article class="media media--type-image media--view-mode-full"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/life-uploads/quickfox.gif" width="520" height="196" /></div>
      
  </article><div align="center"><sup>Courtesy of dototot.com. <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/" target="_blank">CC BY-NC-SA 4.0</a>.</sup></div>
<p>
  Digital 2D animation software eliminates the need to create animations frame-by-frame with keyframes that automatically modify elements over time. We use <a href="http://www.synfig.org" target="_blank">Synfig</a>, a cross-platform FOSS animation studio, to create cutout style animations. Our Synfig animations are a hybrid of digital and analog mediums, incorporating vector graphics created in Inkscape and scanned puppets and textures.
</p>
<article class="media media--type-image media--view-mode-full"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/life-uploads/dototot_synfig.jpg" width="520" height="278" /></div>
      
  </article><div align="center"><sup>Courtesy of dototot.com. <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/" target="_blank">CC BY-NC-SA 4.0</a>.</sup></div>
<h2>Building your own FOSS toolkit</h2>
<p>
  Everything we have demonstrated is easily accessible to a creative individual. All you really need to make digital animations is access to a computer. If you intend on doing any video production or stop-motion animation, you will need a few more pieces of equipment. We recommend a DSLR camera for the versatility it affords. DSLRs can be used for video, stop-motion animation, and still photography. You can get by without a tripod and lights, but everything will look 200% better with them–even if your "light kit" consists of cheap, hardware store shop lights.
</p>
<article class="media media--type-image media--view-mode-full"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/life-uploads/dototot_greenscreen_garage.jpg" width="520" height="347" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>dototot.com. <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/" target="_blank" rel="ugc">CC BY-NC-SA 4.0</a>.</p>
</div>
      
  </article><div align="center"><sup>Courtesy of dototot.com. <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/" target="_blank">CC BY-NC-SA 4.0</a>.</sup></div>
<p>
  There's a good chance you already have a decent flatbed scanner built into your printer, so don't worry about seeking out something better. Scanners are great for capturing textures and drawings for use in digital animations, and can even be repurposed for stop-motion animation.
</p>
<p>
  You may have noticed that we didn't discuss video editing above. We quickly encountered difficulty finding a professional-grade FOSS NLE and settled on <a href="http://www.lwks.com" target="_blank">Lightworks</a>, the Academy and Emmy-award-winning video editor, which, while a great package, has yet to live up to its promise of open source. The free version of Lightworks is nearly the same as the pro version, but with considerable limitations on export options. There are a number of free and open source video editors on Linux, but none of them offer the necessary features to make them viable competition.
</p>
<h2>
  Engaging education<br /></h2>
<p>
  Visit our site, <a href="http://dototot.com/" target="_blank">dototot.com</a>, for tutorials on the topics covered here. Follow us on Twitter <a href="http://twitter.com/dotototdotcom" target="_blank">@dotototdotcom</a>, like us <a href="http://facebook.com/dototot" target="_blank">on Facebook</a>, and <a href="http://eepurl.com/bHJ7yH" target="_blank">sign up for our monthly newsletter</a>. While you're at it, subscribe to <a href="https://www.youtube.com/user/helloworldprogram" target="_blank"><em>The Hello World Program</em></a> on YouTube for educational and entertaining adventures exploring the inner workings of computers.
</p>
