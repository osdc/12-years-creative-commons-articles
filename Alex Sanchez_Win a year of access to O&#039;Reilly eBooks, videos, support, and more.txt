<p><strong>Congratulations to our winner, Christopher Stenger! <a href="https://opensource.com/email-newsletter">Sign up for our email newsletter</a> to find out about future giveaways.</strong></p>
<p><a href="https://opensource.com/tags/oscon">OSCON 2018</a> happens <strong>this week</strong> in Portland, Oregon! To celebrate, we're giving away a one-year subscription to <a href="https://www.safaribooksonline.com/" target="_blank">O'Reilly Safari</a>, a US $399/year membership that gives users access to thousands of technology and business ebooks, videos, live online trainings, and real-time support from experts.</p>
<p>For almost 40 years, O'Reilly Media has provided technology and business training, knowledge, and insight to help companies succeed. As a SaaS learning platform, Safari delivers highly topical and comprehensive technology and business learning solutions to millions of users across enterprise, consumer, and university channels.</p>
<p>So what are you waiting for? Enter by <strong>this Sunday, July 22 at 11:59 p.m. Eastern Time (ET)</strong> for a chance to win!</p>
<p></p><center>
<p><a href="https://opensource.com/2018-summer-oreilly-safari-sweepstakes-entry-form" style="display: block; width: 200px; background: #69BE28; padding: 10px; text-align: center; border-radius: 2px; color: white; font-weight: bold; text-decoration: none;">ENTER NOW</a></p>
<p></p></center>
<p>P.S. Be sure to check out our <a href="https://opensource.com/tags/oscon">series of articles</a> authored by OSCON 2018 speakers.</p>
