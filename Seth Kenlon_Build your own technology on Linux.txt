<p>In 2021, there are more reasons why people love Linux than ever before. In this series, I'll share 21 different reasons to use Linux. Linux empowers its users to build their own tools.</p>
<p>There's a persistent myth that tech companies must "protect" their customers from the many features of their technology. Sometimes, companies put restrictions on their users for fear of unexpected breakage, and other times they expect users to pay extra to unlock features. I love an operating system that protects me from stupid mistakes, but I want to know without a doubt that there's a manual override switch somewhere. I want to be able to control my own experience on my own computer. Whether I'm using Linux for work or my hobbies, that's precisely what it does for me. It puts me in charge of the technology I've chosen to use.</p>
<h2 id="customizing-your-tools">Customizing your tools</h2>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Linux resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux commands cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Free online course: RHEL Technical Overview</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux networking cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">SELinux cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux common commands cheat sheet</a></li>
<li><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">What are Linux containers?</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Our latest Linux articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>It's hard for a business, even when its public face is the image of a "quirky revolutionary," to deal with the fact that reality is actually quite diverse. Literally everybody on the planet uses a computer differently than the next person. We all have our habits; we have artifacts of good and bad computer training; we have our interest levels, our distractions, and our individual goals.</p>
<p>No matter what a company anticipates, there's no way to construct the ideal environment to please each and every potential user. And it's a tall order to expect a business to even attempt that. You might even think it's an unreasonable demand to have a custom tool for every individual.</p>
<p>But we're living in the future, it's a high-tech world, and the technology that empowers users to design and use their own tools has been around for decades. You can witness early Unix users stringing together commands on old <a href="https://archive.org/details/UNIX1985" target="_blank"><em>Computer Chronicles</em></a> episodes way back in 1985. Today, you see it in spreadsheet applications, possibly the most well-used and malleable (and unintentional) prototype engines available. From business forms to surveys to games to video encoder frontends, I've seen everyday users claiming to have "no programming skills" design spreadsheets that rival applications developed and sold by software companies. This is the kind of creativity that technology should foster and encourage, and I think it's the way computing is heading the more that <em>open source</em> principles become an expectation.</p>
<p>Today, Linux delivers the same power: the power to construct your own utilities and offer them to other users in a portable and adaptable format. Whether you work in Bash, Python, or LibreOffice Calc, Linux invites you to build tools that make your life easier.</p>
<h2 id="services">Services</h2>
<p>I believe one of the missing components of the modern computing experience is connectedness. That seems like a crazy thing to assert in the 21st century when we have social networks that claim to bring people together like never before. But social networks have always felt like more like a chaperoned prom than a casual hangout. You go to a place where you're expected to socialize, and you do what's expected of you, but deep down, you'd rather just invite your friends over to watch some movies and play some games.</p>
<p>The deficiency of modern computing platforms is that this casual level of sharing our digital life isn't easy. In fact, it's really difficult on most computers. While we're still a long away from a great selection of sharable applications, Linux is nevertheless built for sharing. It doesn't try to block your path when you open a port to invite your friends to connect to a shared application like <a href="https://opensource.com/article/20/3/drawpile">Drawpile</a> or <a href="https://opensource.com/article/18/5/maptool">Maptool</a>. On the contrary, it has <a href="https://opensource.com/article/19/7/make-linux-stronger-firewalls">tools specifically to make sharing <em>easy</em></a>.</p>
<h2 id="stand-back-im-doing-science">Stand back; I'm doing science!</h2>
<p>Linux is a platform for makers, creators, and developers. It's part of its core tenet to let its users explore and to ensure that the user remains in control of their system. Linux offers you an open source environment and an <a href="https://www.redhat.com/en/about/open-studio" target="_blank">open studio</a>. All you have to do is take advantage of it.</p>
