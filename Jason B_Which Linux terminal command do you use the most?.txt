<p>A few weeks back I stumbled across this <a href="https://www.reddit.com/r/linux/comments/8vzr3y/whats_the_most_frequent_terminal_command_you_use/">Reddit thread</a> with a short Bash snippet telling me how to get a list of my most-used terminal commands.</p>
<pre><code class="language-bash">history | awk '{CMD[$2]++;count++;}END { for (a in CMD)print CMD[a] " " CMD[a]/count*100 "% " a; }' | grep -v "./" | column -c3 -s " " -t | sort -nr | nl | head -n10</code></pre><p>The details aren't terribly important, but basically, it takes the output of the history command, totals it up, and tells you what your top ten commands have been and what percentage of the time each had been used. You could pretty easily make some modifications to the snippet to get a longer list, or change the formatting a bit. Here's mine from my fairly-recently-refreshed laptop.</p>
<pre><code class="language-text">     1	138  25.0909%   sudo
     2	59   10.7273%   ls
     3	47   8.54545%   cd
     4	20   3.63636%   git
     5	19   3.45455%   grep
     6	13   2.36364%   vi
     7	10   1.81818%   lpstat
     8	9    1.63636%   find
     9	8    1.45455%   ps
    10	8    1.45455%   htop</code></pre><p>The only item in there that surprised me was lpstat (I had been fighting with a printer configuration recently). These numbers weren't terribly big, and some of the details were hidden behind sudo (likely a lot of those were dnf updates). So curious, I ran the same command on my desktop and on a couple of virtual machines I'd used for different purposes and got very different results.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>The Linux Terminal</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/life/17/10/top-terminal-emulators?intcmp=7016000000127cYAAQ">Top 7 terminal emulators for Linux</a></li>
<li><a href="https://opensource.com/article/17/2/command-line-tools-data-analysis-linux?intcmp=7016000000127cYAAQ">10 command-line tools for data analysis in Linux</a></li>
<li><a href="https://opensource.com/downloads/advanced-ssh-cheat-sheet?intcmp=7016000000127cYAAQ">Download Now: SSH cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=7016000000127cYAAQ">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://opensource.com/tags/command-line?intcmp=7016000000127cYAAQ">Linux command line tutorials</a></li>
</ul></div>
</div>
</div>
</div>
<p>On my desktop, I spend a considerable amount more time using ssh. And in the virtual machines, systemctl and docker both make the list, and vi climbs to the top as I spend a lot of time editing configuration files.</p>
<p>Your top ten probably shares something about you, too. Are you a developer? A power user? Do you work a lot with remote systems? What's your favorite text editor? If you're willing, share your top ten list in the comments below, or perhaps just the top tool. Did your list surprise you? Why or why not?</p>
