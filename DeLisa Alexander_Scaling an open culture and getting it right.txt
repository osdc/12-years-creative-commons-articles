<p>An open organization's culture—the unique ways that you work together as a community—can create a strategic competitive advantage. Red Hat, my home for the last 15 years, was born out of the Linux community. For years, maintaining our open and collaborative culture was relatively simple: We just hired as many open source contributors as we could find, people who already embodied the principles of openness like transparency and collaboration. As the leader of the People team (our HR organization), my job was mostly to make sure we didn't mess the culture up!</p>
<p>These days, though, we've grown beyond 10,000 associates. The size of our organization and the scope of our ambitions mean that we need to hire more and more people who don't necessarily have extensive experience with doing things the open source way.</p>
<p>
</p><div class="embedded-callout-menu callout-float-left">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Learn about open organizations</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://theopenorganization.org">Download resources</a></li>
<li><a href="https://theopenorganization.community">Join the community</a></li>
<li><a href="https://opensource.com/open-organization/resources/open-org-definition?src=too_resource_menu3a">What is an open organization?</a></li>
<li><a href="https://opensource.com/open-organization/resources/open-org-maturity-model?src=too_resource_menu4a">How open is your organization?</a></li>
</ul></div>
</div>
</div>
</div>

<p>As we looked at the market opportunities ahead of us, we also started to see that we, as Red Hatters, needed to open our own minds to the possibility that there were some areas where our culture might be holding us back. Some of the behaviors embedded in our culture weren't necessarily things we needed to encourage or sustain into the future.</p>
<p>Last year, we kicked off a project we dubbed "Scaling Our Culture For The Future." In this new article series, I will share our journey of culture sustainment and transformation from the past six months—and as we move through the phases of this project in the coming year.</p>
<h2>3 forces that shape our culture</h2>
<p>Research shows that in most organizations, you learn about two forces that shape an organization’s culture: the behavior of your leaders and your "people" systems and processes.</p>
<p>But for Red Hat, and perhaps for other open organizations, I've found that three forces shape our culture:</p>
<p class="rtecenter"><img alt="open_culture_influences.png" src="https://opensource.com/sites/default/files/images/business-uploads/open_culture_influences.png" style="width:489px;height:299px;" width="489" height="299" /></p>
<p class="rtecenter"><sup>Image courtesy of the Red Hat People Team (CC-BY-SA)</sup></p>
<ol><li><strong>Open source influence.</strong> As I said above, we hire lots of people from open source communities, and they join us with the expectation that our company will work in the transparent, collaborative, meritocratic ways that are typical of open source projects. They work and collaborate in these communities every day. You can see this influence at work in things like our <a href="https://github.com/opensourceway/open-decision-framework">Open Decision Framework</a> and the <a href="https://github.com/red-hat-people-team/red-hat-multiplier">Red Hat Multiplier</a>, as well as in the ways that we work together, both inside our company and in open source communities.</li>
<li><strong>Leadership behaviors. </strong>In an open organization, <a href="https://opensource.com/open-organization/17/1/developing-open-leaders">leadership is about influence more than a job title</a>. This means that each and every Red Hatter has the potential to lead and influence others. Of course, many of our managers and our executives have a lot of influence, and their behavior matters a great deal, particularly in challenging times. But in an open organization, how each and every associate behaves matters, particularly those associates who others admire and look up to. Broadly speaking, we look at leadership as any behavior that enables others, and the organization, to grow and contribute.</li>
<li><strong>Systems and processes.</strong> This may not be intuitive, but your people systems, tools, and processes, such as the ones used for performance reviews, promotions, and hiring, can have a big impact on your culture. If you get them wrong, your culture will suffer for it. If you get them right—or at least, good enough—they can help reinforce and strengthen the culture.</li>
</ol><p>That last one took me a few years to accept—because after all when people don’t like our systems or processes, they generally just avoid using them! Nevertheless, we recently kicked off projects to revamp two HR systems and processes that were creating frustration for our associates (our performance and development tool, and our associate referral process). The feedback we received helped cement in my mind the importance of getting these kinds of systems and processes right—and how passionate people are about them.</p>
<p>In the coming year, I’ll share more about how we are approaching these two projects, and the impact they’re having on on our culture.</p>
