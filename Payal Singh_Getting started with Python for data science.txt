<p>Whether you're a budding data science enthusiast with a math or computer science background or an expert in an unrelated field, the possibilities data science offers are within your reach. And you don't need expensive, highly specialized enterprise software—the open source tools discussed in this article are all you need to get started.</p>
<p><a href="https://www.python.org/" target="_blank">Python</a>, its machine-learning and data science libraries (<a href="https://pandas.pydata.org/" target="_blank">pandas</a>, <a href="https://keras.io/" target="_blank">Keras</a>, <a href="https://www.tensorflow.org/" target="_blank">TensorFlow</a>, <a href="http://scikit-learn.org/stable/" target="_blank">scikit-learn</a>, <a href="https://www.scipy.org/" target="_blank">SciPy</a>, <a href="http://www.numpy.org/" target="_blank">NumPy</a>, etc.), and its extensive list of visualization libraries (<a href="https://matplotlib.org/" target="_blank">Matplotlib</a>, <a href="https://matplotlib.org/api/pyplot_api.html" target="_blank">pyplot</a>, <a href="https://plot.ly/" target="_blank">Plotly</a>, etc.) are excellent FOSS tools for beginners and experts alike. Easy to learn, popular enough to offer community support, and armed with the latest emerging techniques and algorithms developed for data science, these comprise one of the best toolsets you can acquire when starting out.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Python Resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/topics/middleware/what-is-ide?intcmp=7016000000127cYAAQ">What is an IDE?</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-python-37-beginners?intcmp=7016000000127cYAAQ">Cheat sheet: Python 3.7 for beginners</a></li>
<li><a href="https://opensource.com/resources/python/gui-frameworks?intcmp=7016000000127cYAAQ">Top Python GUI frameworks</a></li>
<li><a href="https://opensource.com/downloads/7-essential-pypi-libraries?intcmp=7016000000127cYAAQ">Download: 7 essential PyPI libraries</a></li>
<li><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ">Red Hat Developers</a></li>
<li><a href="https://opensource.com/tags/python?intcmp=7016000000127cYAAQ">Latest Python content</a></li>
</ul></div>
</div>
</div>
</div>
<p>Many of these Python libraries are built on top of each other (known as <em>dependencies</em>), and the basis is the <a href="http://www.numpy.org/" target="_blank">NumPy</a> library. Designed specifically for data science, NumPy is often used to store relevant portions of datasets in its ndarray datatype, which is a convenient datatype for storing records from relational tables as <code>csv</code> files or in any other format, and vice-versa. It is particularly convenient when scikit functions are applied to multidimensional arrays. SQL is great for querying databases, but to perform complex and resource-intensive data science operations, storing data in ndarray boosts efficiency and speed (but make sure you have ample RAM when dealing with large datasets). When you get to using pandas for knowledge extraction and analysis, the almost seamless conversion between DataFrame datatype in pandas and ndarray in NumPy creates a powerful combination for extraction and compute-intensive operations, respectively.</p>
<p>For a quick demonstration, let’s fire up the Python shell and load an open dataset on crime statistics from the city of Baltimore in a pandas DataFrame variable, and view a portion of the loaded frame:</p>
<pre>
<code class="language-text">&gt;&gt;&gt;  import   pandas as  pd
&gt;&gt;&gt;  crime_stats   =  pd.read_csv('BPD_Arrests.csv')
&gt;&gt;&gt;  crime_stats.head()
</code></pre><p>
<article class="media media--type-image media--view-mode-full" title="Crime statistics"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/life-uploads/crime_stats_chart.jpg" width="1061" height="314" alt="Crime stats " title="Crime statistics" /></div>
      
  </article></p>
<p>We can now perform most of the queries on this pandas DataFrame that we can with SQL in databases. For instance, to get all the unique values of the "Description" attribute, the SQL query is:</p>
<pre>
<code class="language-text">$  SELECT  unique(“Description”)   from   crime_stats;</code></pre><p>This same query written for a pandas DataFrame looks like this:</p>
<pre>
<code class="language-text">&gt;&gt;&gt;  crime_stats['Description'].unique()
['COMMON   ASSAULT'   'LARCENY'   'ROBBERY   - STREET'   'AGG.   ASSAULT'
'LARCENY   FROM   AUTO'   'HOMICIDE'   'BURGLARY'   'AUTO   THEFT'
'ROBBERY   - RESIDENCE'   'ROBBERY   - COMMERCIAL'   'ROBBERY   - CARJACKING'
'ASSAULT   BY  THREAT'   'SHOOTING'   'RAPE'   'ARSON']
</code></pre><p>which returns a NumPy array (ndarray):</p>
<pre>
<code class="language-text">&gt;&gt;&gt;  type(crime_stats['Description'].unique())
&lt;class   'numpy.ndarray'&gt;
</code></pre><p>Next let’s feed this data into a neural network to see how accurately it can predict the type of weapon used, given data such as the time the crime was committed, the type of crime, and the neighborhood in which it happened:</p>
<pre>
<code class="language-text">&gt;&gt;&gt;  from   sklearn.neural_network   import   MLPClassifier
&gt;&gt;&gt;  import   numpy   as np
&gt;&gt;&gt;
&gt;&gt;&gt;  prediction   =  crime_stats[[‘Weapon’]]
&gt;&gt;&gt;  predictors   =  crime_stats['CrimeTime',   ‘CrimeCode’,   ‘Neighborhood’]
&gt;&gt;&gt;
&gt;&gt;&gt;  nn_model   =  MLPClassifier(solver='lbfgs',   alpha=1e-5,   hidden_layer_sizes=(5,
2),   random_state=1)
&gt;&gt;&gt;
&gt;&gt;&gt;predict_weapon   =  nn_model.fit(prediction,   predictors)
</code></pre><p>Now that the learning model is ready, we can perform several tests to determine its quality and reliability. For starters, let’s feed a training set data (the portion of the original dataset used to train the model and not included in creating the model):</p>
<pre>
<code class="language-text">&gt;&gt;&gt;  predict_weapon.predict(training_set_weapons)
array([4,   4,   4,   ..., 0,   4,   4])
</code></pre><p>As you can see, it returns a list, with each number predicting the weapon for each of the records in the training set. We see numbers rather than weapon names, as most classification algorithms are optimized with numerical data. For categorical data, there are techniques that can reliably convert attributes into numerical representations. In this case, the technique used is Label Encoding, using the LabelEncoder function in the sklearn preprocessing library: <code>preprocessing.LabelEncoder()</code>. It has a function to transform and inverse transform data and their numerical representations. In this example, we can use the <code>inverse_transform</code> function of LabelEncoder() to see what Weapons 0 and 4 are:</p>
<pre>
<code class="language-text">&gt;&gt;&gt;  preprocessing.LabelEncoder().inverse_transform(encoded_weapons)
array(['HANDS',   'FIREARM',   'HANDS',   ..., 'FIREARM',   'FIREARM',   'FIREARM']
</code></pre><p>This is fun to see, but to get an idea of how accurate this model is, let's calculate several scores as percentages:</p>
<pre>
<code class="language-text">&gt;&gt;&gt;  nn_model.score(X,   y)
0.81999999999999995
</code></pre><p>This shows that our neural network model is ~82% accurate. That result seems impressive, but it is important to check its effectiveness when used on a different crime dataset. There are other tests, like correlations, confusion, matrices, etc., to do this. Although our model has high accuracy, it is not very useful for general crime datasets as this particular dataset has a disproportionate number of rows that list ‘FIREARM’ as the weapon used. Unless it is re-trained, our classifier is most likely to predict ‘FIREARM’, even if the input dataset has a different distribution.</p>
<p>It is important to clean the data and remove outliers and aberrations before we classify it. The better the preprocessing, the better the accuracy of our insights. Also, feeding the model/classifier with too much data to get higher accuracy (generally over ~90%) is a bad idea because it looks accurate but is not useful due to <a href="https://www.kdnuggets.com/2014/06/cardinal-sin-data-mining-data-science.html" target="_blank">overfitting</a>.</p>
<p><a href="http://jupyter.org/" target="_blank">Jupyter notebooks</a> are a great interactive alternative to the command line. While the CLI is fine for most things, Jupyter shines when you want to run snippets on the go to generate visualizations. It also formats data better than the terminal.</p>
<p><a href="https://machinelearningmastery.com/best-machine-learning-resources-for-getting-started/" target="_blank">This article</a> has a list of some of the best free resources for machine learning, but plenty of additional guidance and tutorials are available. You will also find many open datasets available to use, based on your interests and inclinations. As a starting point, the datasets maintained by <a href="https://www.kaggle.com/" target="_blank">Kaggle</a>, and those available at state government websites are excellent resources.</p>
<p><em>Payal Singh will be presenting at SCaLE16x this year, March 8-11 in Pasadena, California. To attend and get 50% of your ticket, <a href="https://register.socallinuxexpo.org/reg6/">register</a> using promo code <strong>OSDC</strong></em></p>
