<p>Open source web browsers have come a long way since Microsoft dominated the web browser market with its closed source Internet Explorer (IE). For many years, IE was the standard browser for Microsoft's Windows operating system, while Safari (also closed source) was the default browser for MacOS. Then Mozilla's introduction of Firefox, followed by Google's release of Chrome, sparked a revolution in open source internet browsers. Those two are extremely well known but are not the only open source browsers available.</p>
<p>This article introduces seven open source browsers, summarizes their features, and shares how you can contribute to them.</p>
<table border="1"><thead><tr><th>Name / Link to Contribute</th>
<th>License</th>
<th>Supported OSes</th>
</tr></thead><tbody><tr><td><a href="https://github.com/brave" target="_blank">Brave</a></td>
<td>MPL 2.0</td>
<td>All</td>
</tr><tr><td><a href="https://www.chromium.org/Home" target="_blank">Chrome/Chromium</a></td>
<td>BSD</td>
<td>All</td>
</tr><tr><td><a href="https://developer.mozilla.org/en-US/docs/Mozilla/Developer_guide/Introduction" target="_blank">Firefox</a></td>
<td>MPL 2.0</td>
<td>All</td>
</tr><tr><td><a href="https://kde.org/applications/internet/org.kde.konqueror/development" target="_blank">Konqueror</a></td>
<td>GPL</td>
<td>Linux</td>
</tr><tr><td><a href="https://github.com/kurtchen/Lynx" target="_blank">Lynx</a></td>
<td>GPL</td>
<td>Linux, Windows, DOS</td>
</tr><tr><td><a href="https://github.com/midori-browser/core" target="_blank">Midori</a></td>
<td>LGPL</td>
<td>Linux, Windows</td>
</tr></tbody></table><h2 id="brave">Brave</h2>
<p>The <a href="https://brave.com" target="_blank">Brave browser</a> was created with the goal of blocking all but user-approved advertisements and website trackers. <a href="https://en.wikipedia.org/wiki/Brendan_Eich" target="_blank">Brendan Eich</a>, the creator of JavaScript and a co-founder of the Mozilla Foundation, leads the Brave project as the CEO and a co-founder.</p>
<p><strong>Pros of Brave:</strong></p>
<ul><li>No ads or web trackers</li>
<li>Speed</li>
<li>Security</li>
<li>Chromium <a href="https://support.brave.com/hc/en-us/articles/360017909112-How-can-I-add-extensions-to-Brave-">extension support</a></li>
<li>Bugs are tracked in <a href="https://community.brave.com/c/legacy/qa" target="_blank">Brave QA central</a></li>
</ul><p><strong>Cons of Brave:</strong></p>
<ul><li>The opt-in micro-payment system to support content creators has an unclear pathway to get your payments to your intended recipient</li>
</ul><p>You can find Brave's source code (available under the Mozilla Public License) in its extensive <a href="https://github.com/brave" target="_blank">GitHub repositories</a> (there are 140 repos as of this writing).</p>
<h2 id="google-chromechromium">Chrome/Chromium</h2>
<p><a href="https://www.google.com/chrome/" target="_blank">Google Chrome</a>, inarguably, is the most <a href="https://www.statista.com/statistics/544400/market-share-of-internet-browsers-desktop/" target="_blank">widely used</a> internet browser—open source or otherwise. I remember when Google introduced the first version of Chrome. Mozilla Firefox, which came out much earlier, was riding a wave of popularity. The first version of Chrome was so slow, buggy, and disappointing, which led me to think it wouldn't be successful. But, boy, I was wrong. Chrome got better and better, and the browser eventually surpassed Firefox's browser market share. Google Chrome is still known as a "memory hog" due to its heavy random access memory (RAM) utilization. Regardless, Chrome is by far the most popular browser, and it's loved by many due to its simplicity and speed.</p>
<p><strong>Pros of Google Chrome/Chromium:</strong></p>
<ul><li>Simplicity</li>
<li>Speed</li>
<li>Many useful built-in features</li>
</ul><p><strong>Cons of Google Chrome/Chromium:</strong></p>
<ul><li>Heavy memory usage</li>
<li>Chrome (not Chromium) has proprietary code</li>
</ul><p>Chromium, which is the open source project behind the Chrome browser, is available under the Berkeley Software Distribution (BSD) license. Note that the Chrome browser also has some closed source code. To contribute, visit the <a href="https://chromium.googlesource.com/chromium/src/+/master/docs/contributing.md" target="_blank">Contributing to Chromium</a> page.</p>
<h2 id="mozilla-firefox">Firefox</h2>
<p>Although Chrome is now the most popular browser, <a href="https://www.mozilla.org/firefox/" target="_blank">Mozilla Firefox</a> is the one that started the whole open source web browser sensation. Before Mozilla Firefox, Internet Explorer seemed to be undefeatable. But the birth of Firefox shook that belief. One interesting bit of trivia is that its co-founder <a href="https://en.wikipedia.org/wiki/Blake_Ross" target="_blank">Blake Ross</a> was only 19 years old when Firefox was released.</p>
<p><strong>Pros of Mozilla Firefox:</strong></p>
<ul><li>Security</li>
<li>Many extensions are available</li>
<li>Uniform user experience across different systems</li>
</ul><p><strong>Cons of Mozilla Firefox:</strong></p>
<ul><li>Heavy memory usage</li>
<li>Some HTML5 compatibility issue</li>
</ul><p>Firefox's source code is available under the Mozilla Public License (MPL), and it maintains comprehensive guidelines on <a href="https://developer.mozilla.org/en-US/docs/Mozilla/Developer_guide/Introduction" target="_blank">how to contribute</a>.</p>
<h2 id="konqueror">Konqueror</h2>
<p><a href="https://kde.org/applications/internet/org.kde.konqueror" target="_blank">Konqueror</a> may not be the most well-known internet browser, and that is okay because it is responsible for KHTML, the browser engine forked by Apple and then Google for the Safari and Chrome browsers (and subsequently used by Brave, Vivaldi, and several other browsers). Today, Konqueror can use either its native KHTML engine or the Chromium fork. Konqueror is maintained by the international <a href="https://kde.org" target="_blank">KDE</a> free software community, and it's easy to find on most Linux desktops.</p>
<p><strong>Pros of Konqueor:</strong></p>
<ul><li>Pre-installed on many Linux desktops</li>
<li>Fast and efficient</li>
<li>Built-in ad-blocker and pop-up blocker</li>
<li>Customizable URL shortcuts</li>
<li>Doubles as a file manager, man page viewer, and much more</li>
</ul><p><strong>Cons of Konqueror:</strong></p>
<ul><li>Primarily runs in Linux</li>
<li>Requires several KDE libraries to be installed</li>
</ul><p>Konqueror's source code is available under the GNU Public License (GPL). You can find its detailed <a href="https://docs.kde.org/stable5/en/applications/konqueror/index.html" target="_blank">documentation</a> and <a href="https://kde.org/applications/internet/org.kde.konqueror/development" target="_blank">source code</a> on the KDE website.</p>
<h2 id="lynx">Lynx</h2>
<p>Ah, <a href="http://lynx.browser.org/" target="_blank">Lynx</a>. Lynx is a unique browser as it is entirely text-based. It is also the oldest web browser still in use and still under development. You might think, "who would use a text-based browser?" But it works, and there is a big community supporting this special open source browser.</p>
<p><strong>Pros of Lynx:</strong></p>
<ul><li>Extremely lightweight</li>
<li>Extremely minimal</li>
<li>Extremely secure</li>
<li>Supports DOS and Windows</li>
<li>Ideal for testing and safe browsing</li>
</ul><p><strong>Cons of Lynx:</strong></p>
<ul><li>Nothing but text</li>
</ul><p>Lynx's source code is available under the GNU Public License (GPL) and maintained on <a href="https://github.com/kurtchen/Lynx" target="_blank">GitHub</a>.</p>
<h2 id="midori">Midori</h2>
<p>If you hear "Midori," you might think of a green-hued cocktail. But the <a href="https://www.midori-browser.org/" target="_blank">Midori browser</a> is an open source, lightweight browser. If you want a simple and lightweight browser, Midori might be an interesting one to look at. But note that there is no stable release for this browser, and it is known to be quite buggy.</p>
<p><strong>Pros of Midori:</strong></p>
<ul><li>Simple</li>
<li>Lightweight</li>
</ul><p><strong>Cons of Midori:</strong></p>
<ul><li>Still no stable release</li>
<li>Buggy</li>
<li>Almost no extensions</li>
<li>No process isolation</li>
</ul><p>Midori's source code is available under the GNU Lesser General Public License (LGPL) and maintained on <a href="https://github.com/midori-browser/core" target="_blank">GitHub</a>.</p>
<hr /><p>Do you know another open source browser that I should have mentioned on this list? Please share it in the comments.</p>
