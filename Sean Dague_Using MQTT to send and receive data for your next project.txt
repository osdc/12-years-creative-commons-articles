<p>Last November we bought an electric car, and it raised an interesting question: When should we charge it? I was concerned about having the lowest emissions for the electricity used to charge the car, so this is a specific question: What is the rate of CO2 emissions per kWh at any given time, and when during the day is it at its lowest?</p>
<h2 id="finding-the-data">Finding the data</h2>
<p>I live in New York State. About 80% of our electricity comes from in-state generation, mostly through natural gas, hydro dams (much of it from Niagara Falls), nuclear, and a bit of wind, solar, and other fossil fuels. The entire system is managed by the <a href="http://www.nyiso.com/public/index.jsp" target="_blank">New York Independent System Operator</a> (NYISO), a not-for-profit entity that was set up to balance the needs of power generators, consumers, and regulatory bodies to keep the lights on in New York.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Programming and development</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ&amp;src=programming_resource_menu4">Red Hat Developers Blog</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Programming cheat sheets</a></li>
<li><a href="https://www.redhat.com/en/services/training/learning-subscription?intcmp=7016000000127cYAAQ">Try for free: Red Hat Learning Subscription</a></li>
<li><a href="https://opensource.com/downloads/bash-programming-guide?intcmp=7016000000127cYAAQ">eBook: An introduction to programming with Bash</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/bash-shell-cheat-sheet?intcmp=7016000000127cYAAQ">Bash Shell Scripting Cheat Sheet</a></li>
<li><a href="https://developers.redhat.com/e-books/modernizing-enterprise-java?intcmp=7016000000127cYAAQ">eBook: Modernizing Enterprise Java</a></li>
</ul></div>
</div>
</div>
</div>
<p>Although there is no official public API, as part of its mission, NYISO makes <a href="http://www.nyiso.com/public/markets_operations/market_data/reports_info/index.jsp" target="_blank">a lot of open data</a> available for public consumption. This includes reporting on what fuels are being consumed to generate power, at five-minute intervals, throughout the state. These are published as CSV files on a public archive and updated throughout the day. If you know the number of megawatts coming from different kinds of fuels, you can make a reasonable approximation of how much CO2 is being emitted at any given time.</p>
<p>We should always be kind when building tools to collect and process open data to avoid overloading those systems. Instead of sending everyone to their archive service to download the files all the time, we can do better. We can create a low-overhead event stream that people can subscribe to and get updates as they happen. We can do that with <a href="http://mqtt.org/" target="_blank">MQTT</a>. The target for my project (<a href="http://ny-power.org/#" target="_blank">ny-power.org</a>) was inclusion in the <a href="https://www.home-assistant.io" target="_blank">Home Assistant</a> project, an open source home automation platform that has hundreds of thousands of users. If all of these users were hitting this CSV server all the time, NYISO might need to restrict access to it.</p>
<h2 id="what-is-mqtt">What is MQTT?</h2>
<p>MQTT is a publish/subscribe (pubsub) wire protocol designed with small devices in mind. Pubsub systems work like a message bus. You send a message to a topic, and any software with a subscription for that topic gets a copy of your message. As a sender, you never really know who is listening; you just provide your information to a set of topics and listen for any other topics you might care about. It's like walking into a party and listening for interesting conversations to join.</p>
<p>This can make for extremely efficient applications. Clients subscribe to a narrow selection of topics and only receive the information they are looking for. This saves both processing time and network bandwidth.</p>
<p>As an open standard, MQTT has many open source implementations of both clients and servers. There are client libraries for every language you could imagine, even a library you can embed in Arduino for making sensor networks. There are many servers to choose from. My go-to is the <a href="https://mosquitto.org/" target="_blank">Mosquitto</a> server from Eclipse, as it's small, written in C, and can handle tens of thousands of subscribers without breaking a sweat.</p>
<h2 id="why-i-like-mqtt">Why I like MQTT</h2>
<p>Over the past two decades, we've come up with tried and true models for software applications to ask questions of services. Do I have more email? What is the current weather? Should I buy this thing now? This pattern of "ask/receive" works well much of the time; however, in a world awash with data, there are other patterns we need. The MQTT pubsub model is powerful where lots of data is published inbound to the system. Clients can subscribe to narrow slices of data and receive updates instantly when that data comes in.</p>
<p>MQTT also has additional interesting features, such as "last-will-and-testament" messages, which make it possible to distinguish between silence because there is no relevant data and silence because your data collectors have crashed. MQTT also has retained messages, which provide the last message on a topic to clients when they first connect. This is extremely useful for topics that update slowly.</p>
<p>In my work with the Home Assistant project, I've found this message bus model works extremely well for heterogeneous systems. If you dive into the Internet of Things space, you'll quickly run into MQTT everywhere.</p>
<h2 id="our-first-mqtt-stream">Our first MQTT stream</h2>
<p>One of NYSO's CSV files is the real-time fuel mix. Every five minutes, it's updated with the fuel sources and power generated (in megawatts) during that time period.</p>
<p>The CSV file looks something like this:</p>
<table><thead><tr><th>Time Stamp</th>
<th>Time Zone</th>
<th>Fuel Category</th>
<th>Gen MW</th>
</tr></thead><tbody><tr><td>05/09/2018 00:05:00</td>
<td>EDT</td>
<td>Dual Fuel</td>
<td>1400</td>
</tr><tr><td>05/09/2018 00:05:00</td>
<td>EDT</td>
<td>Natural Gas</td>
<td>2144</td>
</tr><tr><td>05/09/2018 00:05:00</td>
<td>EDT</td>
<td>Nuclear</td>
<td>4114</td>
</tr><tr><td>05/09/2018 00:05:00</td>
<td>EDT</td>
<td>Other Fossil Fuels</td>
<td>4</td>
</tr><tr><td>05/09/2018 00:05:00</td>
<td>EDT</td>
<td>Other Renewables</td>
<td>226</td>
</tr><tr><td>05/09/2018 00:05:00</td>
<td>EDT</td>
<td>Wind</td>
<td>41</td>
</tr><tr><td>05/09/2018 00:05:00</td>
<td>EDT</td>
<td>Hydro</td>
<td>3229</td>
</tr><tr><td>05/09/2018 00:10:00</td>
<td>EDT</td>
<td>Dual Fuel</td>
<td>1307</td>
</tr><tr><td>05/09/2018 00:10:00</td>
<td>EDT</td>
<td>Natural Gas</td>
<td>2092</td>
</tr><tr><td>05/09/2018 00:10:00</td>
<td>EDT</td>
<td>Nuclear</td>
<td>4115</td>
</tr><tr><td>05/09/2018 00:10:00</td>
<td>EDT</td>
<td>Other Fossil Fuels</td>
<td>4</td>
</tr><tr><td>05/09/2018 00:10:00</td>
<td>EDT</td>
<td>Other Renewables</td>
<td>224</td>
</tr><tr><td>05/09/2018 00:10:00</td>
<td>EDT</td>
<td>Wind</td>
<td>40</td>
</tr><tr><td>05/09/2018 00:10:00</td>
<td>EDT</td>
<td>Hydro</td>
<td>3166</td>
</tr></tbody></table><p>The only odd thing in the table is the dual-fuel category. Most natural gas plants in New York can also burn other fossil fuel to generate power. During cold snaps in the winter, the natural gas supply gets constrained, and its use for home heating is prioritized over power generation. This happens at a low enough frequency that we can consider dual fuel to be natural gas (for our calculations).</p>
<p>The file is updated throughout the day. I created a simple data pump that polls for the file every minute and looks for updates. It publishes any new entries out to the MQTT server into a set of topics that largely mirror this CSV file. The payload is turned into a JSON object that is easy to parse from nearly any programming language.</p>
<pre><code class="language-text">ny-power/upstream/fuel-mix/Hydro {"units": "MW", "value": 3229, "ts": "05/09/2018 00:05:00"}
ny-power/upstream/fuel-mix/Dual Fuel {"units": "MW", "value": 1400, "ts": "05/09/2018 00:05:00"}
ny-power/upstream/fuel-mix/Natural Gas {"units": "MW", "value": 2144, "ts": "05/09/2018 00:05:00"}
ny-power/upstream/fuel-mix/Other Fossil Fuels {"units": "MW", "value": 4, "ts": "05/09/2018 00:05:00"}
ny-power/upstream/fuel-mix/Wind {"units": "MW", "value": 41, "ts": "05/09/2018 00:05:00"}
ny-power/upstream/fuel-mix/Other Renewables {"units": "MW", "value": 226, "ts": "05/09/2018 00:05:00"}
ny-power/upstream/fuel-mix/Nuclear {"units": "MW", "value": 4114, "ts": "05/09/2018 00:05:00"}</code></pre><p>This direct reflection is a good first step in turning open data into open events. We'll be converting this into a CO2 intensity, but other applications might want these raw feeds to do other calculations with them.</p>
<h2 id="mqtt-topics">MQTT topics</h2>
<p>Topics and topic structures are one of MQTT's major design points. Unlike more "enterprisey" message buses, in MQTT topics are not preregistered. A sender can create topics on the fly, the only limit being that they are less than 220 characters. The <code>/</code> character is special; it's used to create topic hierarchies. As we'll soon see, you can subscribe to slices of data in these hierarchies.</p>
<p>Out of the box with Mosquitto, every client can publish to any topic. While it's great for prototyping, before going to production you'll want to add an access control list (ACL) to restrict writing to authorized applications. For example, my app's tree is accessible to everyone in read-only format, but only clients with specific credentials can publish to it.</p>
<p>There is no automatic schema around topics nor a way to discover all the possible topics that clients will publish to. You'll have to encode that understanding directly into any application that consumes the MQTT bus.</p>
<p>So how should you design your topics? The best practice is to start with an application-specific root name, in our case, <code>ny-power</code>. After that, build a hierarchy as deep as you need for efficient subscription. The <code>upstream</code> tree will contain data that comes directly from an upstream source without any processing. Our <code>fuel-mix</code> category is a specific type of data. We may add others later.</p>
<h2 id="subscribing-to-topics">Subscribing to topics</h2>
<p>Subscriptions in MQTT are simple string matches. For processing efficiency, only two wildcards are allowed:</p>
<ul><li><code>#</code> matches everything recursively to the end</li>
<li><code>+</code> matches only until the next <code>/</code> character</li>
</ul><p>It's easiest to explain this with some examples:</p>
<pre><code class="language-text">ny-power/#  - match everything published by the ny-power app
ny-power/upstream/#  - match all raw data
ny-power/upstream/fuel-mix/+  - match all fuel types
ny-power/+/+/Hydro - match everything about Hydro power that's
       nested 2 deep (even if it's not in the upstream tree)</code></pre><p>A wide subscription like <code>ny-power/#</code> is common for low-volume applications. Just get everything over the network and handle it in your own application. This works poorly for high-volume applications, as most of the network bandwidth will be wasted as you drop most of the messages on the floor.</p>
<p>To stay performant at higher volumes, applications will do some clever topic slides like <code>ny-power/+/+/Hydro</code> to get exactly the cross-section of data they need.</p>
<h2 id="adding-our-next-layer-of-data">Adding our next layer of data</h2>
<p>From this point forward, everything in the application will work off existing MQTT streams. The first additional layer of data is computing the power's CO2 intensity.</p>
<p>Using the 2016 <a href="https://www.eia.gov/" target="_blank">U.S. Energy Information Administration</a> numbers for total emissions and total power by fuel type in New York, we can come up with an <a href="https://github.com/IBM/ny-power/blob/master/src/nypower/calc.py#L1-L60" target="_blank">average emissions rate</a> per megawatt hour of power.</p>
<p>This is encapsulated in a dedicated microservice. This has a subscription on <code>ny-power/upstream/fuel-mix/+</code>, which matches all upstream fuel-mix entries from the data pump. It then performs the calculation and publishes out to a new topic tree:</p>
<pre><code class="language-text">ny-power/computed/co2 {"units": "g / kWh", "value": 152.9486, "ts": "05/09/2018 00:05:00"}</code></pre><p>In turn, there is another process that subscribes to this topic tree and archives that data into an <a href="https://www.influxdata.com/" target="_blank">InfluxDB</a> instance. It then publishes a 24-hour time series to <code>ny-power/archive/co2/24h</code>, which makes it easy to graph the recent changes.</p>
<p>This layer model works well, as the logic for each of these programs can be distinct from each other. In a more complicated system, they may not even be in the same programming language. We don't care, because the interchange format is MQTT messages, with well-known topics and JSON payloads.</p>
<h2 id="consuming-from-the-command-line">Consuming from the command line</h2>
<p>To get a feel for MQTT in action, it's useful to just attach it to a bus and see the messages flow. The <code>mosquitto_sub</code> program included in the <code>mosquitto-clients</code> package is a simple way to do that.</p>
<p>After you've installed it, you need to provide a server hostname and the topic you'd like to listen to. The <code>-v</code> flag is important if you want to see the topics being posted to. Without that, you'll see only the payloads.</p>
<pre><code class="language-text">mosquitto_sub -h mqtt.ny-power.org -t ny-power/# -v</code></pre><p>Whenever I'm writing or debugging an MQTT application, I always have a terminal with <code>mosquitto_sub</code> running.</p>
<h2 id="mqtt-directly-from-the-web">Accessing MQTT directly from the web</h2>
<p>We now have an application providing an open event stream. We can connect to it with our microservices and, with some command-line tooling, it's on the internet for all to see. But the web is still king, so it's important to get it directly into a user's browser.</p>
<p>The MQTT folks thought about this one. The protocol specification is designed to work over three transport protocols: <a href="https://en.wikipedia.org/wiki/Transmission_Control_Protocol" target="_blank">TCP</a>, <a href="https://en.wikipedia.org/wiki/User_Datagram_Protocol" target="_blank">UDP</a>, and <a href="https://en.wikipedia.org/wiki/WebSocket" target="_blank">WebSockets</a>. WebSockets are supported by all major browsers as a way to retain persistent connections for real-time applications.</p>
<p>The Eclipse project has a JavaScript implementation of MQTT called <a href="https://www.eclipse.org/paho/" target="_blank">Paho</a>, which can be included in your application. The pattern is to connect to the host, set up some subscriptions, and then react to messages as they are received.</p>
<pre><code class="language-javascript">// ny-power web console application
var client = new Paho.MQTT.Client(mqttHost, Number("80"), "client-" + Math.random());

// set callback handlers
client.onMessageArrived = onMessageArrived;

// connect the client
client.reconnect = true;
client.connect({onSuccess: onConnect});

// called when the client connects
function onConnect() {
    // Once a connection has been made, make a subscription and send a message.
    console.log("onConnect");
    client.subscribe("ny-power/computed/co2");
    client.subscribe("ny-power/archive/co2/24h");
    client.subscribe("ny-power/upstream/fuel-mix/#");
}

// called when a message arrives
function onMessageArrived(message) {
    console.log("onMessageArrived:"+message.destinationName + message.payloadString);
    if (message.destinationName == "ny-power/computed/co2") {
        var data = JSON.parse(message.payloadString);
        $("#co2-per-kwh").html(Math.round(data.value));
        $("#co2-units").html(data.units);
        $("#co2-updated").html(data.ts);
    }
    if (message.destinationName.startsWith("ny-power/upstream/fuel-mix")) {
        fuel_mix_graph(message);
    }
    if (message.destinationName == "ny-power/archive/co2/24h") {
        var data = JSON.parse(message.payloadString);
        var plot = [
            {
                x: data.ts,
                y: data.values,
                type: 'scatter'
            }
        ];
        var layout = {
            yaxis: {
                title: "g CO2 / kWh",
            }
        };
        Plotly.newPlot('co2_graph', plot, layout);
    }</code></pre><p>This application subscribes to a number of topics because we're going to display a few different kinds of data. The <code>ny-power/computed/co2</code> topic provides us a topline number of current intensity. Whenever we receive that topic, we replace the related contents on the site.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="NY ISO Grid CO2 Intensity"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/mqtt_nyiso-co2intensity.png" width="700" height="450" alt="NY ISO Grid CO2 Intensity" title="NY ISO Grid CO2 Intensity" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>NY ISO Grid CO<sub>2</sub> Intensity graph from <a href="http://ny-power.org/#" target="_blank" rel="ugc">/ny-power.org/#</a></sup></p>
</div>
      
  </article></p>
<p>The <code>ny-power/archive/co2/24h</code> topic provides a time series that can be loaded into a <a href="https://plot.ly/" target="_blank">Plotly</a> line graph. And <code>ny-power/upstream/fuel-mix</code> provides the data needed to provide a nice bar graph of the current fuel mix.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Fuel mix on NYISO grid"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/mqtt_nyiso_fuel-mix.png" width="700" height="450" alt="Fuel mix on NYISO grid" title="Fuel mix on NYISO grid" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>Fuel mix on NYISO grid, <a href="http://ny-power.org/#" target="_blank" rel="ugc">ny-power.org</a>.</sup></p>
</div>
      
  </article></p>
<p>This is a dynamic website that is not polling the server. It is attached to the MQTT bus and listening on its open WebSocket. The webpage is a pub/sub client just like the data pump and the archiver. This one just happens to be executing in your browser instead of a microservice in the cloud.</p>
<p>You can see the page in action at <a href="http://ny-power.org" target="_blank">http://ny-power.org</a>. That includes both the graphics and a real-time MQTT console to see the messages as they come in.</p>
<h2 id="diving-deeper">Diving deeper</h2>
<p>The entire ny-power.org application is <a href="https://github.com/IBM/ny-power" target="_blank">available as open source on GitHub</a>. You can also check out <a href="https://developer.ibm.com/code/patterns/use-mqtt-stream-real-time-data/">this architecture overview</a> to see how it was built as a set of Kubernetes microservices deployed with <a href="https://helm.sh/" target="_blank">Helm</a>. You can see another interesting MQTT application example with <a href="https://developer.ibm.com/code/patterns/deploy-serverless-multilingual-conference-room/">this code pattern</a> using MQTT and OpenWhisk to translate text messages in real time.</p>
<p>MQTT is used extensively in the Internet of Things space, and many more examples of MQTT use can be found at the <a href="https://www.home-assistant.io/">Home Assistant</a> project.</p>
<p>And if you want to dive deep into the protocol, <a href="http://mqtt.org/">mqtt.org</a> has all the details for this open standard.</p>
<hr /><p><em>To learn more, attend Sean Dague's talk, <a href="https://conferences.oreilly.com/oscon/oscon-or/public/schedule/speaker/77317" target="_blank">Adding MQTT to your toolkit</a>, at <a href="https://conferences.oreilly.com/oscon/oscon-or" target="_blank">OSCON</a>, which will be held July 16-19 in Portland, Oregon.</em></p>
