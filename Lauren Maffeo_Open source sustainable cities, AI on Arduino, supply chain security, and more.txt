<p>Open source made it into a lot of news headlines last month. Read on to learn about some of the major advances.</p>
<h2 id="stanford-unveils-open-source-sustainable-cities-software">Stanford unveils open source sustainable cities software</h2>
<p>80% of Americans live in cities, and 70% of the world's population is expected to be urban dwellers by 2050. Thanks to the Stanford National Capital Project, city planners and developers have a new open source tool to help improve urban wellbeing.</p>
<p><a href="https://naturalcapitalproject.stanford.edu/software/urban-invest" target="_blank">Urban InVEST</a> is new software that helps users visualize where they might create areas to absorb carbon emissions and encourage public use, such as marshlands and parks.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Great Content</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=7016000000127cYAAQ">Free online course: RHEL technical overview</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=7016000000127cYAAQ">Learn Advanced Linux Commands</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Download Cheat Sheets</a></li>
<li><a href="https://opensource.com/alternatives?intcmp=7016000000127cYAAQ">Find an Open Source Alternative</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=7016000000127cYAAQ">Read Top Linux Content</a></li>
<li><a href="https://opensource.com/resources?intcmp=7016000000127cYAAQ">Check out open source resources</a></li>
</ul></div>
</div>
</div>
</div>
<p>Such investments will <a href="https://scitechdaily.com/stanfords-new-open-source-software-for-designing-sustainable-cities/" target="_blank">become more crucial</a> as climate change increases and larger swaths of people move to tighter spaces. For example, urban planners could use Urban InVEST to forecast how much money green infrastructure might save cities in the event of a major storm.</p>
<p>The software allows users to upload their own data sets or use open data sets from sources including NASA satellites. Urban InVEST is part of the larger <a href="https://naturalcapitalproject.stanford.edu/software/invest" target="_blank">InVEST suite</a>, a set of software to help experts map and model nature's benefits.</p>
<h2 id="artificial-intelligence-comes-to-the-arduino">Artificial intelligence comes to Arduino</h2>
<p>The <a href="https://www.arduino.cc/" target="_blank">Arduino</a> project and <a href="https://www.ims.fraunhofer.de/en.html" target="_blank">Fraunhofer IMS</a> have teamed up to release <a href="https://www.ims.fraunhofer.de/en/Business-Unit/Industry/Industrial-AI/Artificial-Intelligence-for-Embedded-Systems-AIfES.html" target="_blank">AIfES</a>, a standalone open source artificial intelligence (AI) framework programmed in C.</p>
<p>Users can add AlfES to an Arduino project through Arduino's integrated development environment's library manager. You can also use AlfES to run and train machine learning algorithms on even the smallest microcontrollers (such as the popular 8-bit Arduino Uno).</p>
<p>This allows you to develop edge and Internet of Things (IoT) devices that are cloud-independent and can intelligently process sensors onsite. In addition to its GPLv3 license for open source projects, AlfES provides paid licensing for commercial projects.</p>
<h2 id="new-tool-aims-to-save-open-source-from-supply-chain-attacks">New tool aims to save open source from supply chain attacks</h2>
<p>Cyberattacks are increasing in size and scope, and software supply chain attacks (where hackers slip malicious code into legit software) are an especially big risk. A new Linux Foundation project led by Google, Red Hat, and Purdue University aims to prevent them.</p>
<p><a href="https://sigstore.dev/" target="_blank">Sigstore</a> is a public-good service that offers code signing to open source developers who might lack the time, expertise, and resources to implement it themselves. Developers can give all their cryptography work to Sigstore, which automatically produces an open source log of all activity.</p>
<p>Santiago Torres-Arias, a supply chain researcher at Purdue University affiliated with the project, <a href="https://www.wired.com/story/sigstore-open-source-supply-chain-code-signing/" target="_blank">told WIRED</a> that supply chain code signing won't solve every open source security problem, but it does address low-hanging fruit.</p>
<p>At an inaugural key ceremony in June, Torres-Arias and four others become Sigstore's main key holders. If Sigstore gets enough adoption, they hope to rotate keys to other users, which would make it a neutral open source project.</p>
<h2 id="google-rolls-out-unified-security-vulnerability-schema-for-open-source-software">Google rolls out unified security vulnerability schema for open source software</h2>
<p>As calls to improve open source security mount, Google is making moves to deliver. The search giant <a href="https://docs.google.com/document/d/1sylBGNooKtf220RHQn1I8pZRmqXZQADDQ_TOABrKTpA/edit#heading=h.ss425olznxo" target="_blank">unveiled</a> a vulnerability interchange schema to find security risks across open source solutions.</p>
<p>The Google Open Source Security and Go teams built upon Google's work to produce the <a href="https://opensource.googleblog.com/2021/02/launching-osv-better-vulnerability.html" target="_blank">Open Source Vulnerabilities (OSV) database</a> and the <a href="https://github.com/google/oss-fuzz" target="_blank">OSS-Fuzz</a> data set of security risks. This new schema describes vulnerabilities across any open source ecosystem without requiring ecosystem-dependent logic.</p>
<p>That's crucial because, as Steven J. Vaughan-Nichols <a href="https://www.zdnet.com/article/google-rolls-out-a-unified-security-vulnerability-schema-for-open-source-software/" target="_blank">wrote for ZDNet</a>, the lack of a standard interchange format has been a big barrier to tracking dependencies across vulnerability databases. Instead, Google's new schema shares vulnerability data across several open source projects.</p>
<h2 id="gitlab-spins-out-open-source-data-integration-platform-meltano">GitLab spins out open source data integration platform Meltano</h2>
<p>As of June 30, Meltano is officially a standalone business. The open source extract, transfer, and load (ETL) platform is now independent of GitLab.</p>
<p>Meltano queries databases and software-as-a-service applications, transitions the data into a warehouse or storage system, and restructures it. GitLab debuted Meltano in 2018, and it became open source over the course of several iterations.</p>
<p>Several proprietary ETL tools exist, making Meltano's status as an open source alternative noteworthy. It allows users to host the tool on their devices of choice and access it using their own orchestration tools or Meltano's web-based interface.</p>
<p>"Most solutions right now are pay-to-play, which limits how many companies have access to high-quality tooling," Meltano CEO Douwe Maan <a href="https://venturebeat.com/2021/06/30/gitlab-spins-out-open-source-data-integration-platform-meltano/" target="_blank">told VentureBeat</a>. "Being open source means the long-tail of integrations can be better served by a large community, since vendors typically only support about 150."</p>
<p>In other news:</p>
<ul><li><a href="https://www.forbes.com/sites/forbestechcouncil/2021/06/29/why-companies-should-model-their-culture-after-open-source/?sh=58ed96e61abe" target="_blank">Why companies should model their culture after open source</a></li>
<li><a href="https://www.infoq.com/news/2021/06/google-fhe-transpiler/" target="_blank">Google open sources fully homomorphic encryption transpiler</a></li>
<li><a href="https://cointelegraph.com/news/enterprise-ethereum-matures-looks-to-open-source-community-for-standards" target="_blank">Enterprise Ethereum matures, looks to open source community for standards</a></li>
<li><a href="https://venturebeat.com/2021/06/28/atomicjar-wants-to-bring-open-source-integration-testing-to-the-enterprise/" target="_blank">AtomicJar wants to bring open source integration testing to the enterprise</a></li>
</ul>