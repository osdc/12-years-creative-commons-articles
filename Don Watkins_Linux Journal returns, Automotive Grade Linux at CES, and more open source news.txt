<p>
  In this week's edition of our open source news roundup, we cover the rebirth of Linux Journal, Automotive Grade Linux infotainment systems, and more.
</p>
<h2>Linux Journal returns one month after ceasing publication</h2>
<p>
  In December of 2016, Linux Journal publisher Carlie Fairchild <a href="http://www.linuxjournal.com/content/linux-journal-ceases-publication" target="_blank">announced</a> the online magazine's November edition would be its last. <a href="http://www.linuxjournal.com/content/happy-new-year-linux-journal-alive" target="_blank">That changed</a> earlier this week thanks to an investment from the team behind Private Internet Access VPN.
</p>
<p>
  "They aren't merely rescuing this ship we were ready to scuttle; they're making it seaworthy again and are committed to making it bigger and better than we were ever in a position to think about during our entirely self-funded past," Fairchild writes.
</p>
<h2>Automotive Grade Linux heads to CES</h2>
<p>
  Automotive Grade Linux (AGL), an open source project hosted at The Linux Foundation, plans to <a href="http://markets.businessinsider.com/news/stocks/Automotive-Grade-Linux-Showcases-Open-Infotainment-and-Over-20-Member-Demonstrations-at-CES-2018-1011825794" target="_blank">showcase its vehicle infotainment system</a> at the 2018 Consumer Electronics Show (CES) in Las Vegas, Nevada January 9-12.
</p>
<p>
  "AGL has gained a tremendous amount of traction this year as the leading open source automotive infotainment platform," AGL executive director Dan Cauchy said. "This is clearly reflected by the number of participants that will be demonstrating AGL-based products and services at CES 2018 in the AGL Demo Showcase."
</p>
<h2>Eelo: An open source alternative to Android</h2>
<p>
  Mandrake Linux founder Gael Duval <a href="https://www.techworm.net/2017/12/mandrake-linux-founder-launches-google-free-android-os.html" target="_blank">announced a Google-free version of Android OS</a> called Eelo. Its aim, according to Duval, is to free users from the Google ecosystem.
</p>
<h3>In other news</h3>
<ul><li><a href="https://www.express.co.uk/life-style/science-technology/898593/Kodi-Media-Player-Free-Stream-Movie-Xbox-One-Add-On" target="_blank">Install Kodi on Xbox One</a></li>
<li><a href="https://www.ghacks.net/2017/12/28/autobootdisk-copy-linux-distributions-to-bootable-usb-drives/" target="_blank">AutoBootDisk allows copying any Linux distro to a bootable USB</a></li>
<li><a href="https://chromeunboxed.com/news/fuchsia-pixelbook-install-google-developer" target="_blank">Google is running Fuschia on the Pixelbook</a></li>
<li><a href="http://www.zdnet.com/article/ukfast-plans-to-open-raspberry-pi-cafes-in-manchester-schools/" target="_blank">Raspberry Pi cafes coming to schools in the UK</a></li>
<li><a href="https://patch.com/new-york/nanuet/textbook-alternatives-help-rcc-students-save-money" target="_blank">OER textbook alternatives save students money</a></li>
<li><a href="http://www.fsf.org/news/fsf-adds-pureos-to-list-of-endorsed-gnu-linux-distributions-1" target="_blank">Free Software Foundation adds PureOS</a></li>
</ul>