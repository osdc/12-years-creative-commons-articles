<p>Solar photovoltaic technology, which converts sunlight directly into electricity, has fallen in cost so far that it is now the most inexpensive method of getting electricity, period. If you have any money in the bank and own your home or business, consider investing in solar power for yourself now, as you can do some good for the planet and your wallet simultaneously.</p>
<p>But can you still take advantage of solar if you live in an apartment or aren’t flush with cash?</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Great Content</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=7016000000127cYAAQ">Free online course: RHEL technical overview</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=7016000000127cYAAQ">Learn Advanced Linux Commands</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Download Cheat Sheets</a></li>
<li><a href="https://opensource.com/alternatives?intcmp=7016000000127cYAAQ">Find an Open Source Alternative</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=7016000000127cYAAQ">Read Top Linux Content</a></li>
<li><a href="https://opensource.com/resources?intcmp=7016000000127cYAAQ">Check out open source resources</a></li>
</ul></div>
</div>
</div>
</div>
<p>Yes, thanks to open source tech development, you absolutely can. If you are a little handy, you can cut the costs of solar power even further by building your own systems of any size and budget, and <a href="https://www.appropedia.org/To_Catch_the_Sun" target="_blank">To Catch the Sun</a> guides you on exactly how to do it. To Catch the Sun is a brand new book I co-authored with the legendary open source appropriate technology hacker and <a href="https://www.appropedia.org/Welcome_to_Appropedia" target="_blank">Appropedia</a> founder Lonny Grafman. Built on open source MediaWiki and Semantic MediaWiki, Appropedia is the largest wiki dedicated to developing and sharing collaborative solutions in sustainability, poverty reduction, and international development through the use of sound principles and appropriate technology. Together we are something like the double-O 7 team of the solar world. A bit like James Bond, Lonny Grafman from sunny California, is an adventurer taking his students from Humboldt State University all over the world to build solar photovoltaic systems in the most challenging conditions. I am perhaps more Q-like, developing open source solar photovoltaic technology quietly from my labs deep in the north at Western University in Canada. Together we provide ways to make solar work for you in just about any context.</p>
<p><article class="align-center media media--type-image media--view-mode-full"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/photovolticsystem.png" width="1000" height="534" alt="photovoltic solar system" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>Base small-scale photovoltaic system setup. (CC-BY-SA 4.0)</sup></p>
</div>
      
  </article></p>
<p>Working with electricity is exciting for some of us, but to ensure everyone can come along for the ride, we provide lots of interesting and sometimes funny stories of people from all over the world meeting their energy needs with small solar systems.</p>
<p>Better yet, and in keeping with the spirit of the text, it is a free open access e-book.</p>
<p><article class="align-center media media--type-image media--view-mode-full"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/tocatchthesunebook.png" width="364" height="364" alt="To Catch The Sun ebook" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>To Catch The Sun ebook (CC-BY-SA 4.0)</sup></p>
</div>
      
  </article></p>
<p>While you are reading inspiring stories of communities coming together to harness their own solar energy, you will learn how to design and build a photovoltaic system for:</p>
<ul><li>A small home in a financially rich country</li>
<li>A few homes in financially developing countries</li>
<li>School rooms and community spaces</li>
<li>Zombie-apocalypse equipment</li>
<li>Laptop and cellphone chargers</li>
<li>A tiny home or van life</li>
<li>Glamping and backpacking equipment</li>
<li>Emergency supply, e.g., powering an oxygen machine during a power outage</li>
<li>Isolated loads like electric gates, pumps, greenhouse fans, backup generators, telecommunications equipment, and much more</li>
</ul><p><article class="align-center media media--type-image media--view-mode-full"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/system-in-dominican-republic.png" width="750" height="422" alt="Group installing solar system" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>Community coming together in the Dominican Republic to build an off-grid photovoltaic system (CC-BY-SA 4.0)</sup></p>
</div>
      
  </article></p>
<p>Beginners can walk through the steps to build state-of-the-art solar energy systems to meet their own needs using open source plans and transparent calculations. This book is for anyone looking to develop solar projects and knowledge:</p>
<ul><li>Students and teachers in the science, math, engineering, environmental, social justice, and technical fields, especially at high school and undergraduate university levels</li>
<li>Community organizers and builders looking to build solar projects</li>
<li>Entrepreneurs and inventors looking to build solar products</li>
<li>Families looking to learn together</li>
</ul><p>To get a free digital copy of the book, sign up at <a href="https://tocatchthesun.com/">tocatchthesun.com</a> or order a paperback copy from your favorite bookseller. All proceeds from sales support the Appropedia Foundation to ensure the information is freely available for everyone and up to date.</p>
