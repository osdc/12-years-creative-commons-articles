<p>That developers have strong opinions about the tools they use is no secret, and perhaps some of the strongest opinions come out around integrated development environments.</p>
<p>When we asked our community what their favorite <a href="https://opensource.com/resources/python/ides">Python IDE</a> is, more than 10,000 of you responded. Now, it's time for <a href="https://opensource.com/resources/java">Java</a> developers to get their turn.<br /></p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Programming and development</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ&amp;src=programming_resource_menu4">Red Hat Developers Blog</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Programming cheat sheets</a></li>
<li><a href="https://www.redhat.com/en/services/training/learning-subscription?intcmp=7016000000127cYAAQ">Try for free: Red Hat Learning Subscription</a></li>
<li><a href="https://opensource.com/downloads/bash-programming-guide?intcmp=7016000000127cYAAQ">eBook: An introduction to programming with Bash</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/bash-shell-cheat-sheet?intcmp=7016000000127cYAAQ">Bash Shell Scripting Cheat Sheet</a></li>
<li><a href="https://developers.redhat.com/e-books/modernizing-enterprise-java?intcmp=7016000000127cYAAQ">eBook: Modernizing Enterprise Java</a></li>
</ul></div>
</div>
</div>
</div>

<p>Certainly Eclipse has held a top spot among Java developers for many years, although it's far from the only name in town. Although proprietary IDE options are out there, with so many good open source choices, we don't know why you'd stray the open. And of course, general-purpose development environments, and even text editors are all options on the table.</p>
<p>Maybe your choice even depends on what you're developing! From desktop software to mobile app, embedded applications to the enterprise, Java has a footprint on just about every part of the software world.</p>
<p>So let us know what you prefer, and just as importantly, why you prefer it.</p>
<p>If your favorite open source IDE for Java is missing from our list, let us know in the comments, and if it gains enough support we'll consider adding it to the options in the poll above.</p>
