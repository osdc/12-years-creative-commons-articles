<p>There's  a great new blog post up this week on the Harvard Business  Review blog  site by Bill Taylor, founder of Fast Company magazine and  author of the  book Mavericks at Work, entitled <a href="http://blogs.hbr.org/taylor/2010/09/brand_is_culture_culture_is_br.html" target="_blank">Brand is Culture, Culture is Brand</a>.</p>
<p>As  I read the post, I couldn't help but smile, as the primary point  of  the article is one about which I feel strongly. From the article:</p>
<blockquote><p>The new "power couple" inside the best companies... was an iron-clad partnership between marketing leadership and HR leadership. Your brand is your culture, your culture is your brand.</p>
</blockquote>
<p>Bill   goes on to describe a speech he gave recently, where a bank executive   who was in the audience came up afterward "with a Cheshire cat grin"  and  handed him a business card. Her title? Senior Vice President of  Human  Resources and Marketing. Again from the article:</p>
<blockquote><p>Forget a "partnership" between HR and marketing. At Corner Bank, the two functions report to the same executive. It's a title I'd never seen before, and I asked Jana how her fellow bankers tended to react to it. "They're usually kind of shocked," she admitted, "because at most companies the head of marketing and the head of HR have very different personalities."</p>
</blockquote>
<p>I must admit if I had been at Bill's talk, I would have probably gone up and shown him my business card as well, because my title here at Red Hat is Senior Vice President of People &amp; Brand.</p>
<p>This is no accident. At Red Hat we believe the culture you create internally is a powerful tool for driving brand perception externally.  As a company that grew up quickly, without the benefit of a huge advertising budget to grow our brand, Red Hat found creating a strong, distinctive culture not only an effective way to build brand, but an inexpensive way as well.</p>
<p>I've been in this position spanning HR and brand functions for almost two years, and the connection points between the traditional HR and brand roles have never been more clear for me. By working closely together, HR and brand professionals have a unique opportunity to mold the external perception of the brand by impacting internal practices.</p>
<p>A  few  examples:</p>
<ul><li>Employer value proposition: What is the company "story" you use to attract the best and brightest employees to the company?</li>
<li>Hiring:  How can you change the hiring process so you ensure  you are recruiting  and employing people who are a good fit for the brand  and culture?</li>
<li>Orientation:  How can you ensure employees have a  common vision of what the company  is trying to achieve (and can tell  that story to others)?</li>
<li>Performance  management: How can you set up a system that rewards those  who live  the brand, both internally and in their interactions with  customers and  partners?</li>
<li>Internal communications: How can you  consistently  tell and teach the story of the company through company  meetings,  announcements, and events?</li>
<li>Celebrations of brand and  culture:  How can you reinforce the brand and culture by showcasing it  (we host a  "We are Red Hat week" every year)?</li>
<li>Brand ambassadors:  How can  you identify and support the most enthusiastic proponents of the  brand  and culture within the employee ranks?</li>
</ul><p>Do I think HR and brand need to be combined into one function  in order to work well together? Of course not.</p>
<p>But  my experience running  People &amp; Brand at Red Hat has shown me there  are endless  opportunities to better connect HR and brand efforts  within  organizations. If making an organizational change is out of the   question, I'd definitely recommend getting the HR and brand groups  together to  look for additional opportunities to collaborate.<br /><br />My  belief is that a company living a common brand story internally  is  going to be more more effective at showing a passionate,   differentiated, and consistent face to the outside world. <br /><br />Bill sums it up well at the end of his post:</p>
<blockquote><p>You  can't be special, distinctive, and compelling in the marketplace   unless you create something special, distinctive, and compelling in the   workplace.</p>
</blockquote>
<p>Have you seen the strong connections   between brand and culture that we see here at Red Hat? If so please   share your experiences below.</p>
