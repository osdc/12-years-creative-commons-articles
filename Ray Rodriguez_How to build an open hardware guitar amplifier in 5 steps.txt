<p><a href="http://electrosmash.com/" target="_blank">ElectroSmash</a> just released an open hardware guitar amplifier called the <a href="http://www.electrosmash.com/1wamp" target="_blank">1Wamp</a>. Designed as a small and portable 1 watt amplifier loaded with all the features of big amps, the project was fully developed using only open source tools—like <a href="http://kicad-pcb.org/" target="_blank">KiCAD</a>, a design suite to create schematics and layouts in any platform.</p>
<!--break--><p> </p>
<p></p><center><img alt="1wamp-intro-big.jpg" src="https://opensource.com/sites/default/files/resize/images/life-uploads/1wamp-intro-big-320x217.jpg" style="width: 320px; height: 217px;" width="320" height="217" /></center>
<p class="rtecenter"><sup>Photo by Ray Rodriguez, CC BY-SA 4.0</sup></p>
<p>We <a href="http://www.electrosmash.com/1wamp" target="_blank">published a detailed analysis</a> of each part of the amplifier so that anyone can contribute to improving or modifying it, as well as, sharing their thoughts and work on the <a href="http://www.electrosmash.com/forum/1wamp" target="_blank">1Wamp forum.</a></p>
<p>The amplifier features:</p>
<ul><li>Tone/Volume/Gain controls</li>
<li>Speaker/Cabinet output</li>
<li>Headphones output with attenuator switch</li>
<li>Aux/mp3 input</li>
<li>9V battery clip</li>
<li>9V DC boss style power input jack</li>
</ul><p> </p>
<p></p><center><img alt="1wamp-functions-big.jpg" src="https://opensource.com/sites/default/files/resize/images/life-uploads/1wamp-functions-big-320x182.jpg" style="width: 320px; height: 182px;" width="320" height="182" /></center>
<p class="rtecenter"><sup> Photo by Ray Rodriguez, CC BY-SA 4.0</sup></p>
<h2>How to build 1Wamp in 5 steps</h2>
<p>Here's a summarized version from our <a href="http://www.electrosmash.com/images/tech/1wamp/vault/How-to-Build-1Wamp.pdf">full documentation</a>.</p>
<h3>Step 0</h3>
<p>You will need a soldering iron, lead, and cutting pliers. You can get the PCB from ElectroSmash, or build your own at home using the native files or PCB layout transfers from <a href="http://www.electrosmash.com/forum/1wamp/92-1wamp-schematic-layout-open-hardware-files?lang=en" target="_blank">the forum.</a></p>
<p> </p>
<p></p><center><img alt="1wamp-start-big.jpg" src="https://opensource.com/sites/default/files/resize/images/life-uploads/1wamp-start-big-320x97.jpg" style="width: 320px; height: 97px;" width="320" height="97" /></center>
<p class="rtecenter"><sup> Photo by Ray Rodriguez, CC BY-SA 4.0</sup></p>
<h3>Steps 1, 2, 3, and 4</h3>
<p>Soldering the components to the PCB is easy; just follow the <a href="http://www.electrosmash.com/images/tech/1wamp/vault/1Wamp-bill-of-materials.pdf" target="_blank">Bill of Materials</a> and solder the parts, beginning with the small ones. Then put the larger components in place.                                                       </p>
<p>Some tips:</p>
<ol><li>Pay attention to diodes polarity, there's a line indicating the correct position.</li>
<li>Be careful with the electrolytic caps polarity, the negative lead (the short one) has to be placed in the round hole. The positive hole is always square-shaped and it is marked with a "+" symbol.</li>
<li>Be patient soldering the big components perpendicularly, because they tend to be slightly tilted.</li>
</ol><h3>Step 5</h3>
<p>It's time to check your work. Double-check your PCB with the model, component by component, before powering it up. Use this checklist:</p>
<ul><li>Visual inspection of the PCB bottom, there is no short circuits or long uncut leads.</li>
<li>The polarized components are placed correctly: diodes and electrolytic caps.</li>
</ul><p>The circuit board fits into several enclosures (like the <a href="http://www.electrosmash.com/forum/1wamp/86-1wamp-in-a-1590b-or-1591c-enclosure?lang=en" target="_blank">Hammond 1590B and 1591C</a>), and the plastic cover can also be <a href="http://www.electrosmash.com/forum/1wamp/85-using-the-plexiglas-cover-as-a-lighting-plate?lang=en" target="_blank">used as lighting plate</a>.</p>
<p> </p>
<p></p><center><img alt="1wamp-enclosure.jpg" src="https://opensource.com/sites/default/files/resize/images/life-uploads/1wamp-enclosure-320x199.jpg" style="width: 320px; height: 199px;" width="320" height="199" /></center>
<p class="rtecenter"><sup> Photo by Ray Rodriguez, CC BY-SA 4.0</sup></p>
<p>You'll find all the schematics <a href="http://www.electrosmash.com/images/tech/1wamp/vault/1Wamp-schematic.pdf" target="_blank">here</a>, and the parts list <a href="http://www.electrosmash.com/images/tech/1wamp/vault/1Wamp-bill-of-materials.pdf">here.</a> All the native files are <a href="http://www.electrosmash.com/forum/1wamp/92-1wamp-schematic-layout-open-hardware-files?lang=en" target="_blank">open</a>.</p>
<p><em>Watch the two minute video to <a href="https://www.youtube.com/watch?v=cFPIS3gorQA">see this guitar mini amplifier in action</a>!</em></p>
