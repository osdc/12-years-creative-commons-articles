<p>The clock is ticking on your 13 days to grab the six games in this edition of the <a href="http://www.humblebundle.com/">Humble Indie Bundle</a>, a pay-what-you-will collection of games with the benefits going to the Electronic Frontier Foundation and Child's Play. Your loot this time around includes the DRM-free games and soundtracks for:</p>
<p><!--break--></p>
<ul><li><em><a title="Torchlight" href="http://en.wikipedia.org/wiki/Torchlight">Torchlight</a></em> by <a title="Runic Games" href="http://en.wikipedia.org/wiki/Runic_Games">Runic Games</a> </li>
<li><em><a title="Rochard" href="http://en.wikipedia.org/wiki/Rochard">Rochard</a></em> by Recoil Games</li>
<li><em><a title="Shatter (video game)" href="http://en.wikipedia.org/wiki/Shatter_%28video_game%29">Shatter</a></em> by <a class="mw-redirect" title="Sidhe Interactive" href="http://en.wikipedia.org/wiki/Sidhe_Interactive">Sidhe Interactive</a></li>
<li><em><a class="mw-redirect" title="S.P.A.Z." href="http://en.wikipedia.org/wiki/S.P.A.Z.">S.P.A.Z.</a> </em>(Space Pirates and Zombies) by Minmaz Games</li>
<li><em><a title="Vessel (video game)" href="http://en.wikipedia.org/wiki/Vessel_%28video_game%29">Vessel</a></em> by Strange Loop Games (no soundtrack)</li>
</ul><p>The bonus game for those who pay more than the average is Hitbox Team's <em><a title="Dustforce" href="http://en.wikipedia.org/wiki/Dustforce">Dustforce</a></em>. If you pay more than $1, you also get a Steam key. Total value... drum roll... $86.</p>
<p>On top of everything, this is the debut for all of the Linux versions of the games in the bundle. DRM-free gaming. On Linux. For whatever price you want to pay. To benefit sick children and electronic freedom. Doesn't get much better than that.</p>
<p>For more pop culture references in open source, check out my talk <a title="pop culture references in open source" href="http://www.youtube.com/watch?v=m1rDkolRvwo" target="_blank">here</a>.</p>
