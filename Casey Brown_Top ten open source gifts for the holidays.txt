<p>It's the most wonderful time of the year: time to give open source presents. The opensource.com team gathered ten of our favorite gadgets to help you pick out that perfect present for that special (open source) someone.</p>
<p>Some of these items will be a part of our <a title="2012 open source gift guide giveaway" href="https://opensource.com/life/12/11/2012-open-source-gift-giveaway" target="_blank"><strong>2012 open source gift guide giveaway</strong></a>.</p>
<p>Check them out: </p>
<!--break--><p><br /><br /></p>
<h2>Raspberry Pi</h2>
<p><img src="http://adafruit.com/images/medium/998front_MED.jpg" alt="Image from Adafruit website" title="Image from Adafruit website" style="float: right; margin: 5px;" class="mceItem" height="122" width="159" />The <a title="raspberry pi" href="http://www.raspberrypi.org/">Raspberry Pi</a> is the popular credit-card sized Linux computer that was recently updated to come with 512 MB RAM. Use it as a media center, a tiny game station, or for anything you might want to do with a very small computer running any of several Linux distros. You can buy them from <a title="Buy Raspberry pi" href="http://www.farnell.com">Element14</a> or <a href="https://www.adafruit.com/products/998">Adafruit</a> for $39.95</p>
<p> </p>
<hr style="clear: both;" /><p> </p>
<h2>Arduino</h2>
<p><a title="arduino" href="http://www.arduino.cc/"><img src="http://adafruit.com/images/medium/arduinounor3_MED.jpg" alt="Image from Adafruit website" title="Image from Adafruit website" style="float: left; margin: 5px;" class="mceItem" height="101" width="133" />Arduino</a> is the well-known, open source prototyping board intended for artists, designers, and hobbyists. Use any of a myriad of shields with it to control lights, motors, sensors, and actuators. Arduinos are programmed using the Arduino programming language (based on C++) and can be used for standalone projects or with other devices and software. You can get them and accessories <a title="buy arduino" href="http://arduino.cc/en/Main/Buy">from several suppliers</a> for $29.95.</p>
<p> </p>
<hr style="clear: both;" /><p> </p>
<h2>MaKey MaKey</h2>
<p>MaKey MaKey is an invention kit for makers of all levels. It works with multiple operating systems, and there are open source programs you can run that turn different materials into buttons and keys. Watch this video to see how:</p>
<p><iframe width="560" height="315" src="https://www.youtube.com/embed/rfQqh7iCcOU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe></p>
<p>You can buy them <a title="Buy makey makey" href="http://joylabz.myshopify.com/products/makey-makey-kit">from JoyLabz</a> for $49.95</p>
<hr style="clear: both;" /><p> </p>
<h2>BeagleBone</h2>
<p><a title="BeagleBone" href="http://beagleboard.org/#&amp;slider1=2"><img src="http://beagle.s3.amazonaws.com/graphics/beaglebone/beaglebone-in-hand.JPG" alt="Image from BeagleBone website" title="Image from BeagleBone website" style="float: right; margin: 5px;" class="mceItem" height="94" width="142" />BeagleBone</a> is another low-cost, credit-card-sized development board with a 720 MHz processor and 256 MB of RAM. BeagleBone can be complemented with "capes," stackable plug-in boards that augment BeagleBone's functionality. Currently, BeagleBoard (maker of BeagleBone) is <a title="beaglebone contest" href="http://beagleboard.org/CapeContest">running a contest</a> for the best BeagleBone cape design. The deadline is December 31, so start developing now! You can get one <a title="buy beaglebone" href="http://beagleboard.org/buy">here</a> for $89.</p>
<p> </p>
<hr style="clear: both;" /><p> </p>
<h2>Ice Tube Clock</h2>
<p><img src="https://www.adafruit.com/images/medium/icetubeclock_MED.jpg" alt="Image from Adafruit website" title="Image from Adafruit website" style="float: left; margin: 5px;" class="mceItem" height="92" width="124" />The Adafruit <a title="Ice tube clock" href="http://www.ladyada.net/make/icetube/">Ice Tube Clock</a> is a clock kit housed in a retro Russian display tube. It features a glowing blue tube with eight digits and an alarm. The clock is open source, so you can program the chip/firmware. You can get one <a title="buy ice tube clock" href="http://www.adafruit.com/category/39">from Adafruit</a> for $85.</p>
<p> </p>
<hr style="clear: both;" /><p> </p>
<h2>SparkFun Inventor's Kit for Arduino <img src="https://dlnmh9ip6v2uc.cloudfront.net/images/products/1/1/2/2/7/SIK_Update_no_Overlays.jpg" alt="Image from Sparkfun website" title="Image from Sparkfun website" style="float: right; margin: 5px;" class="mceItem" height="120" width="120" /></h2>
<p>This kit includes an Arduino Uno R3, the new baseplate, and lots of sensors, so it's great for beginners to get started with programmable electronics. You can get it <a title="buy inventor kit" href="https://www.sparkfun.com/products/11227">from SparkFun</a> for $94.95.</p>
<p> </p>
<hr style="clear: both;" /><p> </p>
<h2>i-Racer</h2>
<p><img src="https://dlnmh9ip6v2uc.cloudfront.net/images/products/1/1/1/6/2/11162-01_i_ma.jpg" alt="Image from Sparkfun website" title="Image from Sparkfun website" style="float: left; margin: 5px;" class="mceItem" height="92" width="92" />The i-Racer is a remote-controlled car that's ready to drive right out of the box. The Bluetooth radio allows you to pair it with an Android device as the controller (or you can build your own controller). Get it <a title="buy i-racer" href="https://www.sparkfun.com/products/11162">from SparkFun</a> for $29.95.</p>
<p> </p>
<hr style="clear: both;" /><p> </p>
<h2>NanoNote</h2>
<p><img src="http://upload.wikimedia.org/wikipedia/commons/thumb/c/cc/Ben_on_hand.jpg/220px-Ben_on_hand.jpg" alt="Image from Wikimedia page" title="Image from Wikimedia page" style="float: right; margin: 5px;" class="mceItem" height="154" width="183" />The <a title="nanonote" href="http://sharism.cc/specs/">NanoNote</a> is a small-form-factor computing device. It has a 336 MHz processor, 2GB flash memory, a microSD slot, headphone jack, USB device and battery. According to its website, it's the perfect companion for open content. Their vision for the NanoNote is developers turning the device into an open content device like an ogg-video player or MIT OpenCourseWare gadget. The NanoNote boots Linux out of the box, and it's targeted at developers who love open hardware. Get it <a title="buy nanonote" href="https://sharism.cc/">from Sharism</a> for $149.</p>
<p> </p>
<hr style="clear: both;" /><p> </p>
<h2>Flora</h2>
<p>The <a title="flora" href="http://www.adafruit.com/products/659">Flora</a> was just released by Adafruit  for wearable electronics. Check out this video to see the Flora in action:</p>
<p><iframe width="560" height="315" src="https://www.youtube.com/embed/nf542_z3t1Y" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe></p>
<p>Get it <a title="buy flora" href="http://www.adafruit.com/products/659">from Adafruit</a> for $24.95.</p>
<p> </p>
<hr style="clear: both;" /><p> </p>
<h2>MintyBoost</h2>
<p><img src="http://halley.cc/arduino/adafruit-mintyboost-small.jpg" alt="Image from Adafruit website" title="Image from Adafruit website" style="float: left; margin: 5px;" class="mceItem" height="76" width="98" />The <a title="mintyboost " href="http://www.ladyada.net/make/mintyboost/">MintyBoost</a> is a very small, simple kit by Adafruit to make a small USB charger for your mp3 player, camera, cell phone, or anything that charges over USB. Get one <a title="buy mintyboost" href="http://www.ladyada.net/make/mintyboost/">from Adafruit</a> for $19.50.</p>
<p> </p>
<hr style="clear: both;" /><p><br />This gift list was currated by the moderator team for opensource.com with help and suggestions from coworkers at Red Hat.</p>
<p> </p>
<div id="_mcePaste" class="mcePaste" style="position: absolute; left: -10000px; top: 228px; width: 1px; height: 1px; overflow: hidden;"><a href="http://arduino.cc/en/Main/Buy">http://arduino.cc/en/Main/Buy</a></div>
