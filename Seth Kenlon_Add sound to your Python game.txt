<p>This is part 13 in an ongoing series about creating video games in <a href="https://www.python.org/" target="_blank">Python 3</a> using the <a href="https://www.pygame.org/news" target="_blank">Pygame</a> module. Previous articles are:</p>
<ol><li><a href="https://opensource.com/article/17/10/python-101">Learn how to program in Python by building a simple dice game</a></li>
<li><a href="https://opensource.com/article/17/12/game-framework-python">Build a game framework with Python using the Pygame module</a></li>
<li><a href="https://opensource.com/article/17/12/game-python-add-a-player">How to add a player to your Python game</a></li>
<li><a href="https://opensource.com/article/17/12/game-python-moving-player">Using Pygame to move your game character around</a></li>
<li><a href="https://opensource.com/article/18/5/pygame-enemy">What's a hero without a villain? How to add one to your Python game</a></li>
<li><a href="https://opensource.com/article/18/7/put-platforms-python-game">Add platforms to your game</a></li>
<li><a href="https://opensource.com/article/19/11/simulate-gravity-python">Simulate gravity in your Python game</a></li>
<li><a href="https://opensource.com/article/19/12/jumping-python-platformer-game">Add jumping to your Python platformer game</a></li>
<li><a href="https://opensource.com/article/19/12/python-platformer-game-run">Enable your Python game player to run forward and backward</a></li>
<li><a href="https://opensource.com/article/19/12/loot-python-platformer-game">Using Python to set up loot in Pygame</a></li>
<li><a href="https://opensource.com/article/20/1/add-scorekeeping-your-python-game">Add scorekeeping to your Python game</a></li>
<li><a href="https://opensource.com/article/20/9/add-throwing-python-game">Add throwing mechanics to your Python game</a></li>
</ol><p>Pygame provides an easy way to integrate sounds into your Python video game. Pygame's <a href="https://www.pygame.org/docs/ref/mixer.html" target="_blank">mixer module</a> can play one or more sounds on command, and by mixing those sounds together, you can have, for instance, background music playing at the same time you hear the sounds of your hero collecting loot or jumping over enemies.</p>
<p>It is easy to integrate the mixer module into an existing game, so—rather than giving you code samples showing you exactly where to put them—this article explains the four steps required to get sound in your application.</p>
<h2 id="start-the-mixer">Start the mixer</h2>
<p>First, in your code's setup section, start the mixer process. Your code already starts Pygame and Pygame fonts, so grouping it together with these is a good idea:</p>
<pre><code class="language-python">pygame.init()
pygame.font.init()
pygame.mixer.init() # add this line</code></pre><h2 id="define-the-sounds">Define the sounds</h2>
<p>Next, you must define the sounds you want to use. This requires that you have the sounds on your computer, just as using fonts requires you to have fonts, and using graphics requires you to have graphics.</p>
<p>You also must bundle those sounds with your game so that anyone playing your game has the sound files.</p>
<p>To bundle a sound with your game, first create a new directory in your game folder, right along with the directory you created for your images and fonts. Call it <code>sound</code>:</p>
<pre><code class="language-python">s = 'sound'</code></pre><p>Even though there are plenty of sounds on the internet, it's not necessarily <em>legal</em> to download them and give them away with your game. It seems strange because so many sounds from famous video games are such a part of popular culture, but that's how the law works. If you want to ship a sound with your game, you must find an open source or <a href="https://opensource.com/article/20/1/what-creative-commons">Creative Commons</a> sound that gives you permission to give the sound away with your game.</p>
<p>There are several sites that specialize in free and legal sounds, including:</p>
<ul><li><a href="https://freesound.org" target="_blank">Freesound</a> hosts sound effects of all sorts.</li>
<li><a href="https://incompetech.filmmusic.io" target="_blank">Incompetech</a> hosts background music.</li>
<li><a href="https://opengameart.org" target="_blank">Open Game Art</a> hosts some sound effects and music.</li>
</ul><p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Python Resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/topics/middleware/what-is-ide?intcmp=7016000000127cYAAQ">What is an IDE?</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-python-37-beginners?intcmp=7016000000127cYAAQ">Cheat sheet: Python 3.7 for beginners</a></li>
<li><a href="https://opensource.com/resources/python/gui-frameworks?intcmp=7016000000127cYAAQ">Top Python GUI frameworks</a></li>
<li><a href="https://opensource.com/downloads/7-essential-pypi-libraries?intcmp=7016000000127cYAAQ">Download: 7 essential PyPI libraries</a></li>
<li><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ">Red Hat Developers</a></li>
<li><a href="https://opensource.com/tags/python?intcmp=7016000000127cYAAQ">Latest Python content</a></li>
</ul></div>
</div>
</div>
</div>
<p>Some sound files are free to use only if you give the composer or sound designer credit. Read the conditions of use carefully before bundling any with your game! Musicians and sound designers work just as hard on their sounds as you work on your code, so it's nice to give them credit even when they don't require it.</p>
<p>To give your sound sources credit, list the sounds that you use in a text file called <code>CREDIT</code>, and place the text file in your game folder.</p>
<p>You might also try making your own music. The excellent <a href="https://opensource.com/life/16/2/linux-multimedia-studio">LMMS</a> audio workstation is easy to use and ships with lots of interesting sounds. It's available on all major platforms and exports to <a href="https://en.wikipedia.org/wiki/Vorbis" target="_blank">Ogg Vorbis</a> (OGG) audio format.</p>
<h2 id="add-sound-to-pygame">Add sound to Pygame</h2>
<p>When you find a sound that you like, download it. If it comes in a ZIP or TAR file, extract it and move the sounds into the <code>sound</code> folder in your game directory.</p>
<p>If the sound file has a complicated name with spaces or special characters, rename it. The filename is completely arbitrary, and the simpler it is, the easier it is for you to type into your code.</p>
<p>Most video games use OGG sound files because the format provides high quality in small file sizes. When you download a sound file, it might be an MP3, WAVE, FLAC, or another audio format. To keep your compatibility high and your download size low, convert these to Ogg Vorbis with a tool like <a href="https://www.freac.org/index.php/en/downloads-mainmenu-330" target="_blank">fre:ac</a> or <a href="http://getmiro.com" target="_blank">Miro</a>.</p>
<p>For example, assume you have downloaded a sound file called <code>ouch.ogg</code>.</p>
<p>In your code's setup section, create a variable representing the sound file you want to use:</p>
<pre><code class="language-python">ouch = pygame.mixer.Sound(os.path.join(s, 'ouch.ogg'))</code></pre><h2 id="trigger-sound">Trigger a sound</h2>
<p>To use a sound, all you have to do is call the variable when you want to trigger it. For instance, to trigger the <code>OUCH</code> sound effect when your player hits an enemy:</p>
<pre><code class="language-python">for enemy in enemy_hit_list:
    pygame.mixer.Sound.play(ouch)
    score -= 1</code></pre><p>You can create sounds for all kinds of actions, such as jumping, collecting loot, throwing, colliding, and whatever else you can imagine.</p>
<h2 id="add-background-music">Add background music</h2>
<p>If you have music or atmospheric sound effects you want to play in your game's background, you can use the <code>music</code> function of Pygame's mixer module. In your setup section, load the music file:</p>
<pre><code class="language-python">music = pygame.mixer.music.load(os.path.join(s, 'music.ogg'))</code></pre><p>And start the music:</p>
<pre><code class="language-python">pygame.mixer.music.play(-1)</code></pre><p>The <code>-1</code> value tells Pygame to loop the music file infinitely. You can set it to anything from <code>0</code> and beyond to define how many times the music should loop before stopping.</p>
<h2 id="enjoy-the-soundscapes">Enjoy the soundscapes</h2>
<p>Music and sound can add a lot of flavor to your game. Try adding some to your Pygame project!</p>
