<p>The <a href="https://www.osgeo.org/" target="_blank">Open Geospatial (OSGeo) Foundation</a> recently participated in Google's first <a href="https://developers.google.com/season-of-docs" target="_blank">Season of Docs</a>, in which Google sponsored senior technical writers to contribute to open source projects. OSGeo is an umbrella organization for around 50 geospatial open source projects. I've contributed to a number of these projects over the years and recently co-mentored the two Season of Docs technical writers Google allocated to OSGeo.</p>
<p>We discovered during our involvement that, like many open source projects, we knew little about:</p>
<ul><li>The state of our docs</li>
<li>What we were aiming for</li>
<li>Our priorities</li>
<li>The details of the challenges we faced</li>
<li>How to improve</li>
</ul><p>We discovered:</p>
<ul><li>How hard it is to keep tech docs current</li>
<li>Skillsets from multiple roles are needed to create good docs</li>
<li>Open source's docs and writing processes are immature when compared to software development</li>
</ul><p>It is an exciting problem space with high-value challenges ready to be tackled. It reminds me of the early days of open source, before it became trendy with business.</p>
<h2 id="what-should-tech-writers-work-on">What should tech writers work on?</h2>
<p>Open source communities welcomed the chance to have tech writers improve our docs and expressed a pressing need for it, but found it hard to articulate exactly what needed to be fixed:</p>
<ul><li>People explained that their project docs often hadn't been updated between doc releases.</li>
<li>Some projects had new features that had not been documented.</li>
<li>Other projects had issue lists, collating observed deficiencies, but had no systematic review.</li>
<li>Most observed that docs were created by developers with no formal tech writing training.</li>
<li>Many noted that their English docs were written by non-native English speakers.</li>
</ul><p>But where should we start? We needed to decide on what we wanted and what we should work on first.</p>
<h2 id="defining-good-docs">Defining good docs</h2>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Great Content</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=7016000000127cYAAQ">Free online course: RHEL technical overview</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=7016000000127cYAAQ">Learn Advanced Linux Commands</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Download Cheat Sheets</a></li>
<li><a href="https://opensource.com/alternatives?intcmp=7016000000127cYAAQ">Find an Open Source Alternative</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=7016000000127cYAAQ">Read Top Linux Content</a></li>
<li><a href="https://opensource.com/resources?intcmp=7016000000127cYAAQ">Check out open source resources</a></li>
</ul></div>
</div>
</div>
</div>
<p>We then realized that we didn't have a good definition of "good documentation." For our software projects, we have a comprehensive <a href="https://www.osgeo.org/about/committees/incubation/graduation/" target="_blank">incubation process</a> to assess the maturity of software and the project's community, but we couldn't find a similar set of metrics to define "good documentation." So we started the <a href="https://thegooddocsproject.dev/" target="_blank">Good Docs Project</a> to collate "best-practice templates and writing instructions for documenting open source software." This helped us define what we were aiming for and prioritize what we can achieve with our available resources.</p>
<h2 id="documentation-audit">Documentation audit</h2>
<p>Once we knew what good docs looked like, we could audit the status of a project's docs:</p>
<ul><li>What documentation do we have?</li>
<li>Does it cover all the functionality?</li>
<li>Does it cover end-user needs?</li>
<li>Is the documentation any good?</li>
</ul><p>We discovered that the quality, currency, and completeness of our OSGeo docs were immature when compared to the quality of the software they described.</p>
<h2 id="it-takes-a-village-to-raise-good-docs">It takes a village to raise good docs</h2>
<p>In researching open source projects' documentation needs, it became clear that crafting good docs requires multiple skillsets. Ideally, a doc team has access to:</p>
<ul><li>A developer with a deep understanding of the software being described</li>
<li>A software user who can explain the application within the context of the application's domain</li>
<li>An educator who understands the principles of learning</li>
<li>An information architect who understands how to structure material</li>
<li>A writer who writes clearly and concisely with good grammar</li>
<li>Someone who speaks English as a first language (for English docs)</li>
<li>A translator who is good at translating docs into multiple languages</li>
<li>A DevOps person who can set up doc-build pipelines</li>
<li>A community builder, facilitator, and coordinator who can inspire collective action, capture offers of help, and help all these different personas collaborate</li>
</ul><p>Technical writers usually have a high-level understanding of most of these domains, and their skills are often under-appreciated and under-utilized, especially if they are directed with a vague "just clean up the grammar and stuff."</p>
<p>However, the best docs typically have been influenced by multiple stakeholders. This can be partly achieved by <a href="http://cameronshorter.blogspot.com/2019/02/inspiring-techies-to-become-great.html" target="_blank">using templates to collaborate</a> between domains, timeframes, projects, and organizations.</p>
<h2 id="tools-for-documenting-open-source-projects-are-painful">Tools for documenting open source projects are painful</h2>
<p>We experienced significant pain in trying to convert between software and <a href="https://opensource.com/article/20/3/open-source-writing-tools">writing toolsets</a>. We love the versioning of Git, are frustrated by clunky Markdown interfaces, and want access to editing and review workflows in Word and Google Docs along with grammar and syntax plugin tools such as Grammarly. Translation tools such as Transifex are pretty cool, too.</p>
<p>Could someone please write an application that addresses this use case? Maybe there is an idea in here for a future <a href="https://summerofcode.withgoogle.com/" target="_blank">Google Summer of Code</a>?</p>
<h2 id="achievements-during-osgeo's-season-of-docs">Achievements during OSGeo's Season of Docs</h2>
<p>We're quite proud of our achievements during OSGeo's participation in Google's Season of Docs. Our allocated tech writers amplified the effectiveness of our existing documentation communities, and our documentation communities amplified the effectiveness of these tech writers:</p>
<ul><li><a href="https://flicstar.com/2019/11/27/project-report-for-season-of-docs-2019/" target="_blank">Felicity Brand</a> worked with around 50 of OSGeo's open source projects to update the Quickstarts as part of our <a href="https://live.osgeo.org/en/index.html" target="_blank">OSGeoLive</a> distribution of software.</li>
<li><a href="https://docs.google.com/document/d/1sTGz8aWPTS6moxgrtsBRz19roemJlilcdQk6B-9IZOo" target="_blank">Swapnil Ogale</a> worked directly with <a href="https://geonetwork-opensource.org/" target="_blank">GeoNetwork's</a> documentation team, auditing the breadth and quality of docs, setting up templates for future docs, and updating many of the docs.</li>
</ul><p>Further:</p>
<ul><li>We kicked off the Good Docs Project to establish "best-practice templates and writing instructions for documenting open source software."</li>
<li>In conjunction with the <a href="https://www.ogc.org/" target="_blank">OGC</a> and <a href="https://committee.iso.org/home/tc211" target="_blank">ISO</a> spatial standards communities, we kicked off an <a href="https://wiki.osgeo.org/wiki/Lexicon_Committee" target="_blank">OSGeo Lexicon</a> project to coordinate official definitions for terminology used within the OSGeo context. This will apply best-practice definitions to previously haphazard glossaries.</li>
<li>We did a <a href="http://cameronshorter.blogspot.com/2019/12/why-qgis-docs-team-is-struggling.html" target="_blank">deep-dive analysis</a> of the documentation challenges faced by QGIS, one of OSGeo's most successful projects. Surprisingly, its biggest problem isn't a lack of tech writers or complicated tools (although they are factors). Key problems center around:
<ul><li>Poorly capturing community goodwill and offers of assistance</li>
<li>Lack of direction</li>
<li>Struggling to keep up with a rapidly evolving software baseline</li>
<li>Insufficient writing expertise</li>
<li>A high technical barrier to entry</li>
<li>Documentation and training being generated outside of the core project</li>
<li>Awkward documentation tools and processes</li>
</ul></li>
</ul><h2 id="thanks-google">Thanks, Google</h2>
<p>We are grateful to Google for sponsoring Season of Docs. We've learned plenty from Felicity and Swapnil, the great writers that Google provided to us, and we hope what we have learned will help make future Season of Docs initiatives even better.</p>
<hr /><p><em>This originally appeared on <a href="https://cameronshorter.blogspot.com/2020/03/insights-from-mixing-writers-with-open.html" target="_blank">Cameron Shorter's blog</a> and is republished with permission.</em></p>
