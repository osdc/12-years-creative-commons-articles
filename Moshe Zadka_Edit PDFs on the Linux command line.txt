<p>Many of the documents you receive come in PDF format. Sometimes those PDFs need to be manipulated. For example, pages might need to be removed or added, or you might need to sign or change a specific page.</p>
<p>Whether good or bad, this is the reality we all live in.</p>
<p>There are some fancy graphical user interface tools that let you edit PDFs, but I have always been most comfortable with the command line. Of the many command-line tools for this task, the ones I use when I want to modify a PDF are <code>qpdf</code> and <code>poppler-utils</code>.</p>
<h2>Install</h2>
<p>On Linux, you can install <code>qpdf</code> and <code>poppler-utils</code> using your package manager (such as <code>apt</code> or <code>dnf</code>.) For example, on Fedora:</p>
<pre><code class="language-bash">$ sudo dnf install qpdf poppler-utils</code></pre><p>On macOS, use <a href="https://opensource.com/article/20/11/macports">MacPorts</a> or <a href="https://opensource.com/article/20/6/homebrew-mac">Homebrew</a>. On Windows, use <a href="https://opensource.com/article/20/3/chocolatey">Chocolatey</a>.</p>
<h2 id="qpdf">qpdf</h2>
<p>The <code>qpdf</code> command can do a lot, but I mostly use it for:</p>
<ol><li>Splitting a PDF into separate pages</li>
<li>Concatenating, or combining, PDFs into one file</li>
</ol><p>To split a PDF into separate pages:</p>
<pre><code class="language-bash">qpdf --split-pages original.pdf split.pdf</code></pre><p>This generates files like <code>split-01.pdf</code>, <code>split-02.pdf</code>, and so on. Each file is a single-page PDF file.</p>
<p>Concatenating files is a little subtler:</p>
<pre><code class="language-bash">qpdf --empty concatenated.pdf --pages split-*.pdf --</code></pre><p>This is what <code>qpdf</code> does by default. The <code>--empty</code> option tells qpdf to start with an empty file. The two dashes (<code>--</code>) at the end signals that there are no more files to process. This is a case where the parameters reflect an internal model, rather than what people use it for, but at least it runs and produces valid PDFs!</p>
<h2 id="poppler-utils">poppler-utils</h2>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Related content</p>
</div>
<div class="view-content"></div>
</div>
</div>
<p>This package contains several utilities, but the one I use the most is <a href="https://www.xpdfreader.com/pdftoppm-man.html" target="_blank">pdftoppm</a>, which converts PDF files to portable pixmap (<code>ppm</code>) image files. I usually use it after I split pages with <code>qpdf</code> and need to convert a specific page to an image that I can modify. The <code>ppm</code> format is not well known, but the important thing about it is that most image manipulation methods, including <a href="https://opensource.com/article/17/8/imagemagick">ImageMagick</a>, <a href="https://opensource.com/article/20/8/edit-images-python">Pillow</a>, and many other options, work with it. Most of these tools can save files back to PDF, too.</p>
<h2 id="workflow">Workflow</h2>
<p>My usual workflow is:</p>
<ul><li>Use <code>qpdf</code> to split the PDF into pages.</li>
<li>Use <code>poppler-utils</code> to convert the pages that need to be changed into images.</li>
<li>Modify the images as needed and save them to PDF.</li>
<li>Use <code>qpdf</code> to concatenate the pages back into one PDF.</li>
</ul><h2 id="other-tools">Other tools</h2>
<p>There are many great open source commands to deal with PDFs, whether you're <a href="https://opensource.com/article/20/8/reduce-pdf">shrinking them</a>, <a href="https://opensource.com/article/20/5/pandoc-cheat-sheet">creating them from text files</a>, <a href="https://opensource.com/article/21/3/libreoffice-command-line">converting documents</a>, or trying your best to <a href="https://opensource.com/article/19/3/comic-book-archive-djvu">avoid them altogether</a>. What are your favorite open source PDF utilities? Please share them in the comments.</p>
