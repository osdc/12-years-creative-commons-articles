<p>Interested in keeping track of what's happening in the open source cloud? Opensource.com is your source for news in <a href="https://opensource.com/resources/what-is-openstack" target="_blank" title="What is OpenStack?">OpenStack</a>, the open source cloud infrastructure project.</p>
<p><!--break--></p>
<h3>OpenStack around the web</h3>
<p>There's a lot of interesting stuff being written about OpenStack. Here's a sampling:</p>
<ul><li><a href="http://superuser.openstack.org/articles/why-you-should-take-trystack-for-a-spin-now" target="_blank">Why you should take TryStack for a spin now</a>: The free OpenStack testing sandbox is back, and it's bigger, badder and better than ever.</li>
<li><a href="http://blog.flaper87.org/post/something-about-being-a-ptl/" target="_blank">Let me tell you something about being a PTL</a>: What it's like to run an OpenStack project.</li>
<li><a href="http://blog.nemebean.com/content/quintupleo-now-openstack-virtual-baremetal" target="_blank">QuintupleO is now OpenStack Virtual Baremetal</a>: Practical reasons for renaming a project.</li>
<li><a href="http://redhatstackblog.redhat.com/2015/09/08/managing-openstack-integration-matters/" target="_blank">Managing OpenStack</a>: Why integration matters.</li>
<li><a href="http://my1.fr/blog/liberty-cycle-retrospective-in-puppet-openstack/" target="_blank">Liberty cycle retrospective in Puppet OpenStack</a>: What's new, and what to expect in the next release.</li>
<li><a href="http://redhatstackblog.redhat.com/2015/09/10/big-data-in-the-open-private-cloud/" target="_blank">Big data in the open, private cloud</a>: There's a better way to meet your big data initiatives.</li>
</ul><ul><li><a href="http://superuser.openstack.org/articles/how-to-monitor-openstack-deployments-with-docker-graphite-grafana-collectd-and-chef" target="_blank">How to monitor OpenStack deployments with Docker, Graphite, Grafana, collectd and Chef</a>: A tutorial for getting up and running.</li>
<li><a href="http://superuser.openstack.org/articles/getting-hyper-v-and-openstack-set-up-quickly" target="_blank">Getting Hyper-V and OpenStack set up quickly</a>: Exploring your options for hypervisors.</li>
<li><a href="http://superuser.openstack.org/articles/why-openstack-needs-to-keep-it-simple" target="_blank">Why OpenStack needs to keep it simple</a>: Another look back at this week in OpenStack.</li>
<li><a href="http://www.siliconhillsnews.com/2015/09/10/rackspace-and-intel-open-the-openstack-innovation-center/" target="_blank">Rackspace and Intel open the OpenStack Innovation Center</a>: Creating a new space for OpenStack development.</li>
</ul><h3>OpenStack discussions</h3>
<p>Here's a sample of some of the most active discussions this week on the OpenStack developers' listserv. For a more in-depth look, why not <a href="http://lists.openstack.org/pipermail/openstack-dev/" target="_blank" title="OpenStack listserv">dive in</a> yourself?</p>
<ul><li><a href="http://lists.openstack.org/pipermail/openstack-dev/2015-September/074217.html" target="_blank">Creating new users with invalid mail addresses possible</a>: Should addresses be validated in Keystone?</li>
<li><a href="http://lists.openstack.org/pipermail/openstack-dev/2015-September/074159.html" target="_blank">Let's take care of our integration tests</a>: Keeping Horizon tests operational.</li>
<li><a href="http://lists.openstack.org/pipermail/openstack-dev/2015-September/074250.html" target="_blank">Use only Keystone v3 API in DevStack</a>: Kolla contains hard-coded dependencies to an older version.</li>
<li><a href="http://lists.openstack.org/pipermail/openstack-dev/2015-September/074104.html" target="_blank">Security hardening</a>: Keeping OpenStack-Ansible secure.</li>
<li><a href="http://lists.openstack.org/pipermail/openstack-dev/2015-September/074264.html" target="_blank">'team:danger-not-diverse tag' and my concerns</a>: Improving communications with individual projects.</li>
</ul><h3>Cloud &amp; OpenStack events</h3>
<p>Here's what is happening this week. Something missing? <a href="mailto:osdc-admin@redhat.com">Email</a> us and let us know, or add it to our <a href="https://opensource.com/resources/conferences-and-events-monthly" target="_blank" title="Events Calendar">events calendar</a>.</p>
<ul><li><a href="http://www.meetup.com/GlusterFS-Silicon-Valley/events/224932563/" target="_blank">GlusterFS meetup</a>: Monday, September 14; Menlo Park, CA.</li>
<li><a href="http://www.meetup.com/OpenStackDC/events/223978615/" target="_blank">Extensibility in OpenStack Swift</a>: Monday, September 14; Washington, DC.</li>
<li><a href="http://www.meetup.com/OpenStack-New-York-Meetup/events/224928160/" target="_blank">An evening of OpenStack</a>: Wednesday, September 16; New York, NY.</li>
<li><a href="http://www.meetup.com/OpenStack-Austin/events/224238339/" target="_blank">OpenStack meetup</a>: Wednesday, September 16; Austin, TX.</li>
<li><a href="http://www.meetup.com/Openstack-Amsterdam/events/223038288/" target="_blank">OpenStack Benelux</a>: Thursday, September 17; Amsterdam, the Netherlands.</li>
<li><a href="http://www.meetup.com/openstack-pdx/events/224572482/" target="_blank">OpenStack PDX</a>: Thursday, September 17; Portland, OR.</li>
<li><a href="http://www.meetup.com/openstack/events/223231183/" target="_blank">OpenStack networking for multi-hypervisor data centers</a>: Thursday, September 17; San Francisco, CA.</li>
</ul><p><em>Interested in the OpenStack project development meetings? A complete list of these meetings' regular schedules is available <a href="https://wiki.openstack.org/wiki/Meetings" target="_blank" title="OpenStack Meetings">here</a>.</em></p>
