<p>I've been sewing for six years, and I now make all of my own clothes. Sewing is easy. It really is. What's hard is getting things to fit properly. That's why you use a sewing pattern. It's a blueprint for whatever it is you are making. A good pattern gives you good fit. Most patterns don't.</p>
<p>That's because—much like clothes in the shop—patterns come in sizes. And sizes are a crude way to put people in boxes. They are made for an imaginary average person, rather than for you.</p>
<h2>Your very own pattern draft</h2>
<p>There's another way, and that is to draft a pattern based on your measurements. You measure a bunch of different lengths, widths, and circumferences on a person's body, and use those to draw a pattern that fits that unique person just right. These custom-drafted patterns are vastly superior, but they require a lot of work. It's mostly a bunch of calculations and then connecting the dots.</p>
<p>Given that computers are pretty good at calculating things, I figured I'd tried to automate the process a bit.</p>
<h2>Custom patterns</h2>
<p></p><div class="embedded-callout-text callout-float-right">What I wanted was to pool knowledge and distill it into something that is better than what I could do on my own.</div>
<p>I started <a href="https://makemypattern.com/" target="_blank">Makemypattern.com</a> to generate custom-drafted patterns. Today, about four years after I embarked on the project, it has over 7,500 users for which it churns out a steady supply of patterns.</p>
<p>All patterns are free of charge and licensed under a Creative Commons license.</p>
<p>Get started by choosing from a collection of sewing patterns, entering your measurements, and then picking a bunch of options. Then, out comes your custom pattern.</p>
<p>In the world of home sewing, it tends to require a bit of explaining why one would choose to give away their work for free. Things are different in the open source world where the idea of sharing your work with others for the benefit of all is the very thread from which communities are woven.</p>
<p>The culture of sharing and improving each other's work is what I—arguably somewhat naively—aspired to when I started this project. I may have become a pattern designer in the eyes of some, but that was never my intention. What I wanted was to pool knowledge and distill it into something that is better than what I could do on my own.</p>
<p>Now that that is running smoothly, I think it's time to move the goal post. While I can't magically bring the culture of open source to sewing patterns, I certainly can bring sewing patterns into the open source world.</p>
<h2>Making the code open</h2>
<p>About two months ago, I started rewriting the entire Makemypattern.com codebase. I am using this opportunity to implement some improvements and features that were difficult to implement in the existing backend. But my main reason is that I am going to make all of my code available as open source. And for that, I need to make it easy for others to extend and adapt the code.</p>
<p>With a mission that is not merely about making patterns anymore, I also feel like a name change was needed. So when I finish my rewrite, I will relocate to <a href="https://freesewing.org/" target="_blank">Freesewing.org</a>, which is free as in beer and free as in speech.</p>
<h2>Who's with me?</h2>
<p>Freesewing.org will continue to offer what Makemypattern.com does today: free sewing patterns drafted to your measurements. But additionally, it will be open to your contributions.</p>
<p>Here's hoping that in the Venn diagram of the somewhat geeky and sewing, it's not just me in the middle.</p>
