<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>The Linux Terminal</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/life/17/10/top-terminal-emulators?intcmp=7016000000127cYAAQ">Top 7 terminal emulators for Linux</a></li>
<li><a href="https://opensource.com/article/17/2/command-line-tools-data-analysis-linux?intcmp=7016000000127cYAAQ">10 command-line tools for data analysis in Linux</a></li>
<li><a href="https://opensource.com/downloads/advanced-ssh-cheat-sheet?intcmp=7016000000127cYAAQ">Download Now: SSH cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=7016000000127cYAAQ">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://opensource.com/tags/command-line?intcmp=7016000000127cYAAQ">Linux command line tutorials</a></li>
</ul></div>
</div>
</div>
</div>
<p>Welcome back to another installment in our 24-day-long Linux command-line toys advent calendar. If this is your first visit to the series, you might be asking yourself what a command-line toy even is. We’re figuring that out as we go, but generally, it could be a game, or any simple diversion that helps you have fun at the terminal.</p>
<p>Some of you will have seen various selections from our calendar before, but we hope there’s at least one new thing for everyone.</p>
<p>Every year for Christmas, my mother-in-law gives my wife a Sudoku calendar. It sits on our coffee table for the year to follow. Each day is a separate sheet (except for Saturday and Sunday, that are combined onto one page), with the idea being that you have a new puzzle every day while also having a functioning calendar.</p>
<p>The problem, in practice, is that it's a great pad of puzzles but not a great calendar because it turns out some days are harder than others and we just don't get through them at the necessary rate of one a day. Then, we may have a week's worth that gets batched on a lazy Sunday.</p>
<p>Since I've already given you a <a href="https://opensource.com/article/18/12/linux-toy-cal">calendar</a> as a part of this series, I figure it's only fair to give you a Sudoku puzzle as well, except our command-line versions are decoupled so there's no pressure to complete exactly one a day.</p>
<p>I found <strong>nudoku</strong> in my default repositories on Fedora, so installing it was as simple as:</p>
<pre><code class="language-bash">$ sudo dnf install nudoku</code></pre><p>Once installed, just invoke <strong>nudoku</strong> by name to launch it, and it should be fairly self-explanatory from there. If you've never played Sudoku before, it's fairly simple: You need to make sure that each row, each column, and each of the nine 3x3 squares that make up the large square each have one of every digit, 1-9.</p>
<p>You can find <strong>nudoku</strong>'s c source code <a href="https://github.com/jubalh/nudoku" target="_blank">on GitHub</a> under a GPLv3 license.</p>
<p><article class="media media--type-image media--view-mode-full" title="Linux toy: Nudoku animated"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/linux-toy-nudoku-animated.gif" width="600" height="377" alt="Linux toy: Nudoku animated" title="Linux toy: Nudoku animated" /></div>
      
  </article></p>
<p>Do you have a favorite command-line toy that you we should have included? Our calendar is basically set for the remainder of the series, but we'd still love to feature some cool command-line toys in the new year. Let me know in the comments below, and I'll check it out. And let me know what you thought of today's amusement.</p>
<p>Be sure to check out yesterday's toy, <a href="https://opensource.com/article/18/12/linux-toy-figlet">Use your Linux terminal to celebrate a banner </a><a href="https://opensource.com/article/18/12/linux-toy-figlet">year</a>, and come back tomorrow for another!</p>
