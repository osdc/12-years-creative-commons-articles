<p>Tracee is a project by Aqua Security for tracing processes at runtime. By tracing processes using <a href="https://opensource.com/article/22/8/ebpf-network-observability-cloud" target="_blank">Linux eBPF</a> (Berkeley packet filter) technology, Tracee can correlate collected information and identify malicious behavioral patterns.</p>
<h2>eBPF</h2>
<p>BPF is a system to help in network traffic analysis. The later eBPF system extends classic BPF to improve the programmability of the Linux kernel in different areas, such as network filtering, function hooking, and so on. Thanks to its register-based virtual machine, which is embedded in the kernel, eBPF can execute programs written with a restricted C language without needing to recompile the kernel or load a module. Through eBPF, you can run your program in kernel context and hook various events in the kernel path. To do so, eBPF needs to have deep knowledge about data structures that the kernel is using.</p>
<h2>eBPF CO-RE</h2>
<p>eBPF interfaces with Linux kernel ABI (application binary interface). Access to kernel structures from eBPF VM depends on the specific Linux kernel release.</p>
<p>eBPF CO-RE (compile once, run everywhere) is the ability to write an eBPF program that will successfully compile, pass kernel verification, and work correctly across different kernel releases without the need to recompile it for each particular kernel.</p>
<h3>Ingredients</h3>
<p>CO-RE needs a precise synergism of these components:</p>
<ul><li><strong>BTF (BPF type format) information:</strong> Allows the capture of crucial pieces of information about kernel and BPF program types and code, enabling all the other parts of BPF CO-RE puzzle.<br />
	 </li>
<li><strong>Compiler (Clang):</strong> Records relocation information. For example, if you were going to access the <code>task_struct-&gt;pid</code> field, Clang would record that it was exactly a field named <code>pid</code> of type <code>pid_t</code> residing within a struct <code>task_struct</code>. This system ensures that even if a target kernel has a <code>task_struct</code> layout in which the <code>pid</code> field is moved to a different offset within a <code>task_struct</code> structure, you'll still be able to find it just by its name and type information.<br />
	 </li>
<li><strong>BPF loader (libbpf):</strong> Ties BTFs from kernel and BPF programs together to adjust compiled BPF code to specific kernels on target hosts.</li>
</ul><p>So how do these ingredients mix together for a successful recipe?</p>
<p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">More Linux resources</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux commands cheat sheet">Linux commands cheat sheet</a></div>
              <div class="field__item"><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Advanced Linux commands cheat sheet">Advanced Linux commands cheat sheet</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Free online course: RHEL technical overview">Free online course: RHEL technical overview</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux networking cheat sheet">Linux networking cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="SELinux cheat sheet">SELinux cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux common commands cheat sheet">Linux common commands cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="What are Linux containers?">What are Linux containers?</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Register for your free Red Hat account">Register for your free Red Hat account</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Our latest Linux articles">Our latest Linux articles</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<h3>Development/build</h3>
<p>To make the code portable, the following tricks come into play:</p>
<ul><li>CO-RE helpers/macros</li>
<li>BTF-defined maps</li>
<li><code>#include "vmlinux.h"</code> (the header file containing all the kernel types)</li>
</ul><h3>Run</h3>
<p>The kernel must be built with the <code>CONFIG_DEBUG_INFO_BTF=y</code> option in order to provide the <code>/sys/kernel/btf/vmlinux</code> interface that exposes BTF-formatted kernel types. This allows libbpf to resolve and match all the types and fields and update necessary offsets and other relocatable data to make sure that the eBPF program is working properly for the specific kernel on the target host.</p>
<h2>The problem</h2>
<p>The problem arises when an eBPF program is written to be portable but the target kernel doesn't expose the <code>/sys/kernel/btf/vmlinux</code> interface. For more information, <a href="https://github.com/aquasecurity/btfhub/blob/main/docs/supported-distros.md" target="_blank">refer to this list</a> of distributions that support BTF.</p>
<p>To load an run an eBPF object in different kernels, the libbpf loader uses the BTF information to calculate field offset relocations. Without the BTF interface, the loader doesn't have the necessary information to adjust the previously recorded types that the program tries to access after processing the object for the running kernel.</p>
<p>Is it possible to avoid this problem?</p>
<h3>Use cases</h3>
<p>This article explores <a href="https://github.com/aquasecurity/tracee" target="_blank">Tracee</a>, an Aqua Security open source project, that provides a possible solution.</p>
<p>Tracee provides different running modes to adapt itself to the environment conditions. It supports two eBPF integration modes:</p>
<ul><li><strong>CO-RE:</strong> A <em>portable mode</em>, which seamlessly runs on all supported environments</li>
<li><strong>Non CO-RE:</strong> A <em>kernel-specific mode</em>, requiring the eBPF object to be built for the target host</li>
</ul><p>Both of them are implemented in the eBPF C code (<code>pkg/ebpf/c/tracee.bpf.c</code>), where the pre-processing conditional directive takes place. This allows you to compile CO-RE the eBPF binary, passing the <code>-DCORE</code> argument at build time with Clang (take a look at the <code>bpf-core</code> Make target).</p>
<p>In this article, we're going to cover a case of the portable mode when the eBPF binary is built CO-RE, but the target kernel has not been built with <code>CONFIG_DEBUG_INFO_BTF=y</code> option.</p>
<p>To better understand this scenario, it helps to understand what's possible when the kernel doesn't expose BTF-formatted types on sysfs.</p>
<h3>No BTF support</h3>
<p>If you want to run Tracee on a host without BTF support, there are two options:</p>
<ol><li><a href="https://aquasecurity.github.io/tracee/dev/building/nocore-ebpf/#install-the-non-co-re-ebpf-object" target="_blank">Build and install</a> the eBPF object for your kernel. This depends on Clang and on the availability of a kernel version-specific kernel-headers package.<br />
	 </li>
<li>Download the BTF files from <a href="https://github.com/aquasecurity/btfhub-archive" target="_blank">BTFHUB</a> for your kernel release and provide it to the <code>tracee-ebpf</code>'s loader through the <code>TRACEE_BTF_FILE</code> environment variable.</li>
</ol><p>The first option is not a CO-RE solution. It compiles the eBPF binary, including a long list of kernel headers. That means you need kernel development packages installed on the target system. Also, this solution needs Clang installed on your target machine. The Clang compiler can be resource-heavy, so compiling eBPF code can use a significant amount of resources, potentially affecting a carefully balanced production workload. That said, it's a good practice to avoid the presence of a compiler in your production environment. This could lead to attackers successfully building an exploit and performing a privilege escalation.</p>
<p>The second option is a CO-RE solution. The problem here is that you have to provide the BTF files in your system in order to make Tracee work. The entire archive is nearly 1.3 GB. Of course you can provide just the right BTF file for your kernel release, but that can be difficult when dealing with different kernel releases.</p>
<p>In the end, these possible solutions can also introduce problems, and that's where Tracee works its magic.</p>
<h2>A portable solution</h2>
<p>With a non-trivial building procedure, the Tracee project compiles a binary to be CO-RE even if the target environment doesn't provide BTF information. This is possible with the <code>embed</code> Go package that provides, at runtime, access to files embedded in the program. During the build, the continuous integration (CI) pipeline downloads, extracts, minimizes, and then embeds BTF files along with the eBPF object inside the <code>tracee-ebpf</code> resultant binary.</p>
<p>Tracee can extract the right BTF file and provide it to libbpf, which in turn loads the eBPF program to run across different kernels. But how can Tracee embed all these BTF files downloaded from BTFHub without weighing too much in the end?</p>
<p>It uses a feature recently introduced in bpftool by the Kinvolk team called <a href="https://kinvolk.io/blog/2022/03/btfgen-one-step-closer-to-truly-portable-ebpf-programs/" target="_blank">BTFGen</a>, available using the <code>bpftool gen min_core_btf</code> subcommand. Given an eBPF program, BTFGen generates reduced BTF files, collecting just what the eBPF code needs for its run. This reduction allows Tracee to embed all these files that are now lighter (just a few kilobytes) and support kernels that don't have the <code>/sys/kernel/btf/vmlinux</code> interface exposed.</p>
<h3>Tracee build</h3>
<p>Here's the execution flow of the Tracee build:</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-08/tracee%20build.png" width="675" height="447" alt="Detailed flowchart of tracee build from tracee/3rdparty/btfhub.sh to tracee-ebpf bin compiled" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Alessio Greggi and Massimiliano Giovagnoli, CC BY-SA 4.0)</p>
</div>
      
  </article><p>First, you must build the <code>tracee-ebpf</code> binary, the Go program that loads the eBPF object. The Makefile provides the command <code>make bpf-core</code> to build the <code>tracee.bpf.core.o</code> object with BTF records.</p>
<p>Then <code>STATIC=1 BTFHUB=1 make all</code> builds <code>tracee-ebpf</code>, which has <code>btfhub</code> targeted as a dependency. This last target runs the script <code>3rdparty/btfhub.sh</code>, which is responsible for downloading the BTFHub repositories:</p>
<ul><li><code>btfhub</code></li>
<li><code>btfhub-archive</code></li>
</ul><p>Once downloaded and placed in the <code>3rdparty</code> directory, the procedure executes the downloaded script <code>3rdparty/btfhub/tools/btfgen.sh</code>. This script generates reduced BTF files, tailored for the <code>tracee.bpf.core.o</code> eBPF binary.</p>
<p>The script collects <code>*.tar.xz</code> files from <code>3rdparty/btfhub-archive/</code> to uncompress them and finally process them with bpftool, using the following command:</p>
<pre>
<code class="language-bash">for file in $(find ./archive/${dir} -name *.tar.xz); do
    dir=$(dirname $file)
    base=$(basename $file)
    extracted=$(tar xvfJ $dir/$base)
    bpftool gen min_core_btf ${extracted} dist/btfhub/${extracted} tracee.bpf.core.o
done</code></pre><p>This code has been simplified to make it easier to understand the scenario.</p>
<p>Now, you have all the ingredients available for the recipe:</p>
<ul><li><code>tracee.bpf.core.o</code> eBPF object</li>
<li>BTF reduced files (for all kernel releases)</li>
<li><code>tracee-ebpf</code> Go source code</li>
</ul><p>At this point, <code>go build</code> is invoked to do its job. Inside the <code>embedded-ebpf.go</code> file, you can find the following code:</p>
<pre>
<code class="language-c">//go:embed "dist/tracee.bpf.core.o"
//go:embed "dist/btfhub/*"
</code></pre><p>Here, the Go compiler is instructed to embed the eBPF CO-RE object with all the BTF-reduced files inside itself. Once compiled, these files will be available using the <code>embed.FS</code> file system. To have an idea of the current situation, you can imagine the binary with a file system structured like this:</p>
<pre>
<code class="language-bash">dist
├── btfhub
│   ├── 4.19.0-17-amd64.btf
│   ├── 4.19.0-17-cloud-amd64.btf
│   ├── 4.19.0-17-rt-amd64.btf
│   ├── 4.19.0-18-amd64.btf
│   ├── 4.19.0-18-cloud-amd64.btf
│   ├── 4.19.0-18-rt-amd64.btf
│   ├── 4.19.0-20-amd64.btf
│   ├── 4.19.0-20-cloud-amd64.btf
│   ├── 4.19.0-20-rt-amd64.btf
│   └── ...
└── tracee.bpf.core.o
</code></pre><p>The Go binary is ready. Now to try it out!</p>
<h3>Tracee run</h3>
<p>Here's the execution flow of the Tracee run:</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-08/tracee%20run.png" width="675" height="535" alt="Flow chart of tracee run assuming BTF info is not available in the kernel, which leads to &quot;copy btf kernel related file&quot; and &quot;load btf file using libbpf under the hood&quot;" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Alessio Greggi and Massimiliano Giovagnoli, CC BY-SA 4.0)</p>
</div>
      
  </article><p>As the flow chart illustrates, one of the very first phases of <code>tracee-ebpf</code> execution is to discover the environment where it is running. The first condition is an abstraction of the <code>cmd/tracee-ebpf/initialize/bpfobject.go</code> file, specifically where the <code>BpfObject()</code> function takes place. The program performs some checks to understand the environment and make decisions based on it:</p>
<ol><li>BPF file given <strong>and</strong> BTF (vmlinux or env) exists: always load BPF as CO-RE</li>
<li>BPF file given <strong>but</strong> no BTF exists: it is a non CO-RE BPF</li>
<li>No BPF file given <strong>and</strong> BTF (vmlinux or env) exists: load embedded BPF as CO-RE</li>
<li>No BPF file given <strong>and</strong> no BTF available: check embedded BTF files</li>
<li>No BPF file given <strong>and</strong> no BTF available <strong>and</strong> no embedded BTF: non CO-RE BPF</li>
</ol><p>Here's the code extract:</p>
<pre>
<code class="language-c">func BpfObject(config *tracee.Config, kConfig *helpers.KernelConfig, OSInfo *helpers.OSInfo) error {
	...
	bpfFilePath, err := checkEnvPath("TRACEE_BPF_FILE")
	...
	btfFilePath, err := checkEnvPath("TRACEE_BTF_FILE")
	...
	// Decision ordering:
	// (1) BPF file given &amp; BTF (vmlinux or env) exists: always load BPF as CO-RE
        ...
	// (2) BPF file given &amp; if no BTF exists: it is a non CO-RE BPF
        ...
	// (3) no BPF file given &amp; BTF (vmlinux or env) exists: load embedded BPF as CO-RE
        ...
	// (4) no BPF file given &amp; no BTF available: check embedded BTF files
	unpackBTFFile = filepath.Join(traceeInstallPath, "/tracee.btf")
	err = unpackBTFHub(unpackBTFFile, OSInfo)
	
	if err == nil {
		if debug {
			fmt.Printf("BTF: using BTF file from embedded btfhub: %v\n", unpackBTFFile)
		}
		config.BTFObjPath = unpackBTFFile
		bpfFilePath = "embedded-core"
		bpfBytes, err = unpackCOREBinary()
		if err != nil {
			return fmt.Errorf("could not unpack embedded CO-RE eBPF object: %v", err)
		}
	
		goto out
	}
	// (5) no BPF file given &amp; no BTF available &amp; no embedded BTF: non CO-RE BPF
	...
out:
	config.KernelConfig = kConfig
	config.BPFObjPath = bpfFilePath
	config.BPFObjBytes = bpfBytes
	
	return nil
}</code></pre><p>This analysis focuses on the fourth case, when eBPF program and BTF files are not provided to <code>tracee-ebpf</code>. At that point, <code>tracee-ebpf</code> tries to load the eBPF program extracting all the necessary files from its embed file system. <code>tracee-ebpf</code> is able to provide the files that it needs to run, even in a hostile environment. It is a sort of high-resilience mode used when none of the conditions have been satisfied.</p>
<p>As you see, <code>BpfObject()</code> calls these functions in the fourth case branch:</p>
<ul><li><code>unpackBTFHub()</code></li>
<li><code>unpackCOREBinary()</code></li>
</ul><p>They extract respectively:</p>
<ul><li>The BTF file for the underlying kernel</li>
<li>The BPF CO-RE binary</li>
</ul><h4>Unpack the BTFHub</h4>
<p>Now take a look starting from <code>unpackBTFHub()</code>:</p>
<pre>
<code class="go">func unpackBTFHub(outFilePath string, OSInfo *helpers.OSInfo) error {
	var btfFilePath string

	osId := OSInfo.GetOSReleaseFieldValue(helpers.OS_ID)
	versionId := strings.Replace(OSInfo.GetOSReleaseFieldValue(helpers.OS_VERSION_ID), """, "", -1)
	kernelRelease := OSInfo.GetOSReleaseFieldValue(helpers.OS_KERNEL_RELEASE)
	arch := OSInfo.GetOSReleaseFieldValue(helpers.OS_ARCH)

	if err := os.MkdirAll(filepath.Dir(outFilePath), 0755); err != nil {
		return fmt.Errorf("could not create temp dir: %s", err.Error())
	}

	btfFilePath = fmt.Sprintf("dist/btfhub/%s/%s/%s/%s.btf", osId, versionId, arch, kernelRelease)
	btfFile, err := embed.BPFBundleInjected.Open(btfFilePath)
	if err != nil {
		return fmt.Errorf("error opening embedded btfhub file: %s", err.Error())
	}
	defer btfFile.Close()

	outFile, err := os.Create(outFilePath)
	if err != nil {
		return fmt.Errorf("could not create btf file: %s", err.Error())
	}
	defer outFile.Close()

	if _, err := io.Copy(outFile, btfFile); err != nil {
		return fmt.Errorf("error copying embedded btfhub file: %s", err.Error())

	}

	return nil
}
</code></pre><p>The function has a first phase where it collects information about the running kernel (<code>osId</code>, <code>versionId</code>, <code>kernelRelease</code>, etc). Then, it creates the directory that is going to host the BTF file (<code>/tmp/tracee</code> by default). It retrieves the right BTF file from the <code>embed</code> file system:</p>
<pre>
<code>btfFile, err := embed.BPFBundleInjected.Open(btfFilePath)
</code></pre><p>Finally, it creates and fills the file.</p>
<h4>Unpack the CORE Binary</h4>
<p>The <code>unpackCOREBinary()</code> function does a similar thing:</p>
<pre>
<code class="go">func unpackCOREBinary() ([]byte, error) {
	b, err := embed.BPFBundleInjected.ReadFile("dist/tracee.bpf.core.o")
	if err != nil {
		return nil, err
	}

	if debug.Enabled() {
		fmt.Println("unpacked CO:RE bpf object file into memory")
	}

	return b, nil
}
</code></pre><p>Once the main function <code>BpfObject()</code>returns, <code>tracee-ebpf</code> is ready to load the eBPF binary through <code>libbpfgo</code>. This is done in the <code>initBPF()</code> function, inside <code>pkg/ebpf/tracee.go</code>. Here's the configuration of the program execution:</p>
<pre>
<code class="go">func (t *Tracee) initBPF() error {
        ...
        newModuleArgs := bpf.NewModuleArgs{
		KConfigFilePath: t.config.KernelConfig.GetKernelConfigFilePath(),
		BTFObjPath:      t.config.BTFObjPath,
		BPFObjBuff:      t.config.BPFObjBytes,
		BPFObjName:      t.config.BPFObjPath,
	}

	// Open the eBPF object file (create a new module)

	t.bpfModule, err = bpf.NewModuleFromBufferArgs(newModuleArgs)
	if err != nil {
		return err
	}
        ...
}
</code></pre><p>In this piece of code we are initializing the eBPF args filling the libbfgo structure <code>NewModuleArgs{}</code>. Through its <code>BTFObjPath</code> argument, we are able to instruct libbpf to use the BTF file, previously extracted by the <code>BpfObject()</code> function.</p>
<p>At this point, <code>tracee-ebpf</code> is ready to run properly!</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-08/tracee%20bpf.png" width="675" height="386" alt="Illustration of the kernel" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Alessio Greggi and Massimiliano Giovagnoli, CC BY-SA 4.0)</p>
</div>
      
  </article><h4>eBPF module initialization</h4>
<p>Next, during the execution of the <code>Tracee.Init()</code> function, the configured arguments will be used to open the eBPF object file:</p>
<pre>
<code>Tracee.bpfModule = libbpfgo.NewModuleFromBufferArgs(newModuleArgs)
</code></pre><p>Initialize the probes:</p>
<pre>
<code>t.probes, err = probes.Init(t.bpfModule, netEnabled)
</code></pre><p>Load the eBPF object into kernel:</p>
<pre>
<code>err = t.bpfModule.BPFLoadObject()
</code></pre><p>Populate eBPF maps with initial data:</p>
<pre>
<code>err = t.populateBPFMaps()
</code></pre><p>And finally, attach eBPF programs to selected events' probes:</p>
<pre>
<code>err = t.attachProbes()
</code></pre><h2>Conclusion</h2>
<p>Just as eBPF simplified the way to program the kernel, CO-RE is tackling another barrier. But leveraging such features has some requirements. Fortunately, with Tracee, the Aqua Security team found a way to take advantage of portability in case those requirements can't be satisfied.</p>
<p>At the same time, we're sure that this is only the beginning of a continuously evolving subsystem that will find increasing support over and over, even in different operating systems.</p>
