<p>This article takes a deep dive into how to produce a research paper using (mostly) <a href="https://en.wikipedia.org/wiki/Markdown" target="_blank">Markdown</a> syntax. We'll cover how to create and reference sections, figures (in Markdown and <a href="https://www.latex-project.org/" target="_blank">LaTeX</a>) and bibliographies. We'll also discuss troublesome cases and why writing them in LaTeX is the right approach.</p>
<h2>Research</h2>
<p>Research papers usually contain references to sections, figures, tables, and a bibliography. <a href="https://pandoc.org/" target="_blank">Pandoc</a> by itself cannot easily cross-reference these, but it can leverage the <a href="http://lierdakil.github.io/pandoc-crossref/" target="_blank">pandoc-crossref</a> filter to do the automatic numbering and cross-referencing of sections, figures, and tables.</p>
<p>Let’s start by rewriting <a href="https://dl.acm.org/citation.cfm?id=3270118" target="_blank">an example of an educational research paper</a> originally written in LaTeX and rewrites it in Markdown (and some LaTeX) with Pandoc and pandoc-crossref.</p>
<h3>Adding and referencing sections</h3>
<p>Sections are automatically numbered and must be written using the Markdown heading H1. Subsections are written with subheadings H2-H4 (it is uncommon to need more than that). For example, to write a section titled “Implementation”, write <code># Implementation {#sec:implementation}</code>, and Pandoc produces <code>3. Implementation</code> (or the corresponding numbered section). The title “Implementation” uses heading H1 and declares a label <code>{#sec:implementation}</code> that authors can use to refer to that section. To reference a section, type the <code>@</code> symbol followed by the label of the section and enclose it in square brackets: <code>[@sec:implementation]</code>.</p>
<p><a href="https://dl.acm.org/citation.cfm?id=3270118" target="_blank">In this paper</a>, we find the following example:</p>
<pre><code class="language-text">we lack experience (consistency between TAs, [@sec:implementation]).</code></pre><p>Pandoc produces:</p>
<pre><code class="language-text">we lack experience (consistency between TAs, Section 4).</code></pre><p>Sections are numbered automatically (this is covered in the <code>Makefile</code> at the end of the article). To create unnumbered sections, type the title of the section, followed by <code>{-}</code>. For example, <code>### Designing a game for maintainability {-}</code> creates an unnumbered subsection with the title “Designing a game for maintainability”.</p>
<h3>Adding and referencing figures</h3>
<p>Adding and referencing a figure is similar to referencing a section and adding a Markdown image:</p>
<pre><code class="language-text">![Scatterplot matrix](data/scatterplots/RScatterplotMatrix2.png){#fig:scatter-matrix}</code></pre><p>The line above tells Pandoc that there is a figure with the caption <em>Scatterplot matrix</em> and the path to the image is <code>data/scatterplots/RScatterplotMatrix2.png</code>. <code>{#fig:scatter-matrix}</code> declares the name that should be used to reference the figure.</p>
<p>Here is an example of a figure reference from the example paper:</p>
<pre><code class="language-text">The boxes "Enjoy", "Grade" and "Motivation" ([@fig:scatter-matrix]) ...</code></pre><p>Pandoc produces the following output:</p>
<pre><code class="language-text">The boxes "Enjoy", "Grade" and "Motivation" (Fig. 1) ...</code></pre><h3>Adding and referencing a bibliography</h3>
<p>Most research papers keep references in a BibTeX database file. In this example, this file is named <a href="https://github.com/kikofernandez/pandoc-examples/blob/master/research-paper/biblio.bib" target="_blank">biblio.bib</a> and it contains all the references of the paper. Here is what this file looks like:</p>
<pre><code class="language-text">@inproceedings{wrigstad2017mastery,
    Author =       {Wrigstad, Tobias and Castegren, Elias},
    Booktitle =    {SPLASH-E},
    Title =        {Mastery Learning-Like Teaching with Achievements},
    Year =         2017
}

@inproceedings{review-gamification-framework,
  Author =       {A. Mora and D. Riera and C. Gonzalez and J. Arnedo-Moreno},
  Publisher =    {IEEE},
  Booktitle =    {2015 7th International Conference on Games and Virtual Worlds
                  for Serious Applications (VS-Games)},
  Doi =          {10.1109/VS-GAMES.2015.7295760},
  Keywords =     {formal specification;serious games (computing);design
                  framework;formal design process;game components;game design
                  elements;gamification design frameworks;gamification-based
                  solutions;Bibliographies;Context;Design
                  methodology;Ethics;Games;Proposals},
  Month =        {Sept},
  Pages =        {1-8},
  Title =        {A Literature Review of Gamification Design Frameworks},
  Year =         2015,
  Bdsk-Url-1 =   {http://dx.doi.org/10.1109/VS-GAMES.2015.7295760}
}

...</code></pre><p>The first line, <code>@inproceedings{wrigstad2017mastery,</code>, declares the type of publication (<code>inproceedings</code>) and the label used to refer to that paper (<code>wrigstad2017mastery</code>).</p>
<p>To cite the paper with its title, <em>Mastery Learning-Like Teaching with Achievements</em>, type:</p>
<pre><code class="language-text">the achievement-driven learning methodology [@wrigstad2017mastery]</code></pre><p>Pandoc will output:</p>
<pre><code class="language-text">the achievement- driven learning methodology [30]</code></pre><p>The paper we will produce includes a bibliography section with numbered references like these:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="bibliography-numbered examples.png"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/bibliography-example_0.png" width="1238" height="624" alt="bibliography-numbered examples.png" title="bibliography-numbered examples.png" /></div>
      
  </article></p>
<p>Citing a collection of articles is easy: Simply cite each article, separating the labeled references using a semi-colon: <code>;</code>. If there are two labeled references—i.e., <code>SEABORN201514</code> and <code>gamification-leaderboard-benefits</code>—cite them together, like this:</p>
<pre><code class="language-text">Thus, the most important benefit is its potential to increase students' motivation
and engagement [@SEABORN201514;@gamification-leaderboard-benefits].</code></pre><p>Pandoc will produce:</p>
<pre><code class="language-text">Thus, the most important benefit is its potential to increase students’ motivation
and engagement [26, 28]</code></pre><h2>Problematic cases</h2>
<p>A common problem involves objects that do not fit in the page. They then float to wherever they fit best, even if that position is not where the reader expects to see it. Since papers are easier to read when figures or tables appear close to where they are mentioned, we need to have some control over where these elements are placed. For this reason, I recommend the use of the <code>figure</code> LaTeX environment, which enables users to control the positioning of figures.</p>
<p>Let’s take the figure example shown above:</p>
<pre><code class="language-text">![Scatterplot matrix](data/scatterplots/RScatterplotMatrix2.png){#fig:scatter-matrix}</code></pre><p>And rewrite it in LaTeX:</p>
<pre><code class="language-text">\begin{figure}[t]
\includegraphics{data/scatterplots/RScatterplotMatrix2.png}
\caption{\label{fig:matrix}Scatterplot matrix}
\end{figure}</code></pre><p>In LaTeX, the <code>[t]</code> option in the <code>figure</code> environment declares that the image should be placed at the top of the page. For more options, refer to the Wikibooks article <a href="https://en.wikibooks.org/wiki/LaTeX/Floats,_Figures_and_Captions#Figures" target="_blank">LaTex/Floats, Figures, and Captions</a>.</p>
<h2>Producing the paper</h2>
<p>So far, we've covered how to add and reference (sub-)sections and figures and cite the bibliography—now let's review how to produce the research paper in PDF format. To generate the PDF, we will use Pandoc to generate a LaTeX file that can be compiled to the final PDF. We will also discuss how to generate the research paper in LaTeX using a customized template and a meta-information file, and how to compile the LaTeX document into its final PDF form.</p>
<p>Most conferences provide a <strong>.cls</strong> file or a template that specifies how papers should look; for example, whether they should use a two-column format and other design treatments. In our example, the conference provided a file named <strong>acmart.cls</strong>.</p>
<p>Authors are generally expected to include the institution to which they belong in their papers. However, this option was not included in the default Pandoc’s LaTeX template (note that the Pandoc template can be inspected by typing <code>pandoc -D latex</code>). To include the affiliation, take the default Pandoc’s LaTeX template and add a new field. The Pandoc template was copied into a file named <code>mytemplate.tex</code> as follows:</p>
<pre><code class="language-text">pandoc -D latex &gt; mytemplate.tex</code></pre><p>The default template contains the following code:</p>
<pre><code class="language-text">$if(author)$
\author{$for(author)$$author$$sep$ \and $endfor$}
$endif$
$if(institute)$
\providecommand{\institute}[1]{}
\institute{$for(institute)$$institute$$sep$ \and $endfor$}
$endif$</code></pre><p>Because the template should include the author’s affiliation and email address, among other things, we updated it to include these fields (we made other changes as well but did not include them here due to the file length):</p>
<pre><code class="language-text">latex
$for(author)$
    $if(author.name)$
        \author{$author.name$}
        $if(author.affiliation)$
            \affiliation{\institution{$author.affiliation$}}
        $endif$
        $if(author.email)$
            \email{$author.email$}
        $endif$
    $else$
        $author$
    $endif$
$endfor$</code></pre><p>With these changes in place, we should have the following files:</p>
<ul><li><code>main.md</code> contains the research paper</li>
<li><code>biblio.bib</code> contains the bibliographic database</li>
<li><code>acmart.cls</code> is the class of the document that we should use</li>
<li><code>mytemplate.tex</code> is the template file to use (instead of the default)</li>
</ul><p>Let’s add the meta-information of the paper in a <code>meta.yaml</code>file:</p>
<pre><code class="language-text">---
template: 'mytemplate.tex'
documentclass: acmart
classoption: sigconf
title: The impact of opt-in gamification on `\\`{=latex} students' grades in a software design course
author:
- name: Kiko Fernandez-Reyes
  affiliation: Uppsala University
  email: kiko.fernandez@it.uu.se
- name: Dave Clarke
  affiliation: Uppsala University
  email: dave.clarke@it.uu.se
- name: Janina Hornbach
  affiliation: Uppsala University
  email: janina.hornbach@fek.uu.se
bibliography: biblio.bib
abstract: |
  An achievement-driven methodology strives to give students more control over their learning with enough flexibility to engage them in deeper learning. (more stuff continues)

include-before: |
  ```{=latex}
  \copyrightyear{2018}
  \acmYear{2018}
  \setcopyright{acmlicensed}
  \acmConference[MODELS '18 Companion]{ACM/IEEE 21th International Conference on Model Driven Engineering Languages and Systems}{October 14--19, 2018}{Copenhagen, Denmark}
  \acmBooktitle{ACM/IEEE 21th International Conference on Model Driven Engineering Languages and Systems (MODELS '18 Companion), October 14--19, 2018, Copenhagen, Denmark}
  \acmPrice{XX.XX}
  \acmDOI{10.1145/3270112.3270118}
  \acmISBN{978-1-4503-5965-8/18/10}

  \begin{CCSXML}
  &lt;ccs2012&gt;
  &lt;concept&gt;
  &lt;concept_id&gt;10010405.10010489&lt;/concept_id&gt;
  &lt;concept_desc&gt;Applied computing~Education&lt;/concept_desc&gt;
  &lt;concept_significance&gt;500&lt;/concept_significance&gt;
  &lt;/concept&gt;
  &lt;/ccs2012&gt;
  \end{CCSXML}

  \ccsdesc[500]{Applied computing~Education}

  \keywords{gamification, education, software design, UML}
  ```
figPrefix:
  - "Fig."
  - "Figs."
secPrefix:
  - "Section"
  - "Sections"
...</code></pre><p>This meta-information file sets the following variables in LaTeX:</p>
<ul><li><code>template</code> refers to the template to use (‘mytemplate.tex’)</li>
<li><code>documentclass</code> refers to the LaTeX document class to use (<code>acmart</code>)</li>
<li><code>classoption</code> refers to the options of the class, in this case <code>sigconf</code></li>
<li><code>title</code> specifies the title of the paper</li>
<li><code>author</code> is an object that contains other fields, such as <code>name</code>, <code>affiliation</code>, and <code>email</code>.</li>
<li><code>bibliography </code>refers to the file that contains the bibliography (biblio.bib)</li>
<li><code>abstract</code> contains the abstract of the paper</li>
<li><code>include-before </code>is information that should be included before the actual content of the paper; this is known as the <a href="https://www.sharelatex.com/learn/latex/Creating_a_document_in_LaTeX#The_preamble_of_a_document" target="_blank">preamble</a> in LaTeX. I have included it here to show how to generate a computer science paper, but you may choose to skip it</li>
<li><code>figPrefix</code> specifies how to refer to figures in the document, i.e., what should be displayed when one refers to the figure <code>[@fig:scatter-matrix]</code>. For example, the current <code>figPrefix</code> produces in the example <code>The boxes "Enjoy", "Grade" and "Motivation" ([@fig:scatter-matrix])</code> this output: <code>The boxes "Enjoy", "Grade" and "Motivation" (Fig. 3)</code>. If there are multiple figures, the current setup declares that it should instead display <code>Figs.</code> next to the figure numbers.</li>
<li><code>secPrefix</code> specifies how to refer to sections mentioned elsewhere in the document (similar to figures, described above)</li>
</ul><p>Now that the meta-information is set, let’s create a <code>Makefile</code> that produces the desired output. This <code>Makefile</code> uses Pandoc to produce the LaTeX file, <code>pandoc-crossref</code> to produce the cross-references, <code>pdflatex</code> to compile the LaTeX to PDF, and <code>bibtex </code>to process the references.</p>
<p>The <code>Makefile</code> is shown below:</p>
<pre><code class="language-text">all: paper

paper:
	@pandoc -s -F pandoc-crossref --natbib meta.yaml --template=mytemplate.tex -N \
	 -f markdown -t latex+raw_tex+tex_math_dollars+citations -o main.tex main.md
	@pdflatex main.tex &amp;&gt; /dev/null
	@bibtex main &amp;&gt; /dev/null
	@pdflatex main.tex &amp;&gt; /dev/null
	@pdflatex main.tex &amp;&gt; /dev/null

clean:
	rm main.aux main.tex main.log main.bbl main.blg main.out

.PHONY: all clean paper</code></pre><p>Pandoc uses the following flags:</p>
<ul><li><code>-s</code> to create a standalone LaTeX document</li>
<li><code>-F pandoc-crossref</code> to make use of the filter <code>pandoc-crossref</code></li>
<li><code>--natbib</code> to render the bibliography with <code>natbib</code> (you can also choose <code>--biblatex</code>)</li>
<li><code>--template</code> sets the template file to use</li>
<li><code>-N</code> to number the section headings</li>
<li><code>-f</code> and <code>-t</code> specify the conversion <em>from</em> and <em>to</em> which format. <code>-t</code> usually contains the format and is followed by the Pandoc extensions used. In the example, we declared <code>raw_tex+tex_math_dollars+citations</code> to allow use of <code>raw_tex</code> LaTeX in the middle of the Markdown file. <code>tex_math_dollars</code> enables us to type math formulas as in LaTeX, and <code>citations</code> enables us to use <a href="http://pandoc.org/MANUAL.html#citations" target="_blank">this extension</a>.</li>
</ul><p>To generate a PDF from LaTeX, follow the guidelines <a href="http://www.bibtex.org/Using/" target="_blank">from bibtex</a> to process the bibliography:</p>
<pre><code class="language-text">@pdflatex main.tex &amp;&gt; /dev/null
@bibtex main &amp;&gt; /dev/null
@pdflatex main.tex &amp;&gt; /dev/null
@pdflatex main.tex &amp;&gt; /dev/null</code></pre><p>The script contains <code>@</code> to ignore the output, and we redirect the file handle of the standard output and error to <code>/dev/null</code>so that we don’t see the output generated from the execution of these commands.</p>
<p>The final result is shown below. The repository for the article can be found <a href="https://github.com/kikofernandez/pandoc-examples/tree/master/research-paper" target="_blank">on GitHub</a>:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="abstract-image.png"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/abstract-image.png" width="1408" height="560" alt="abstract-image.png" title="abstract-image.png" /></div>
      
  </article></p>
<h2>Conclusion</h2>
<p>In my opinion, research is all about collaboration, dissemination of ideas, and improving the state of the art in whatever field one happens to be in. Most computer scientists and engineers write papers using the LaTeX document system, which provides excellent support for math. Researchers from the social sciences seem to stick to DOCX documents.</p>
<p>When researchers from different communities write papers together, they should first discuss which format they will use. While DOCX may not be convenient for engineers if there is math involved, LaTeX may be troublesome for researchers who lack a programming background. As this article shows, Markdown is an easy-to-use language that can be used by both engineers and social scientists.</p>
