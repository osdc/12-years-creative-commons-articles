<p>DevOps is just another term for <em>value stream development</em>. What does <em>value stream</em> mean?</p>
<p>Value is what arises during our interactions with customers and stakeholders. Once we get into value stream development, we quickly realize that value is not an entity. Value constantly changes. Value is a process. Value is a flow.</p>
<p>Hence the term <em>stream</em>. Value is only value if it's a stream. And this streaming of value is what we call continuous integration (CI).</p>
<h2 id="how-do-we-generate-value">How do we generate value?</h2>
<p>No matter how carefully we specify value, its expectations tend to change. Therefore, the only realistic way to define and generate value is to solicit feedback.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More DevOps resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/resources/devops?intcmp=7013a00000263HlAAI">What is DevOps?</a></li>
<li><a href="https://opensource.com/downloads/devops-hiring-guide?intcmp=7013a00000263HlAAI">The ultimate DevOps hiring guide</a></li>
<li><a href="https://opensource.com/downloads/devops-monitoring-guide?intcmp=7013a00000263HlAAI">DevOps monitoring tools guide</a></li>
<li><a href="https://opensource.com/downloads/devsecops?intcmp=7013a00000263HlAAI">Getting started with DevSecOps</a></li>
<li><a href="https://enterprisersproject.com/cheat-sheet-devops-glossary?intcmp=7013a00000263HlAAI">Download the DevOps glossary</a></li>
<li><a href="https://www.ansible.com/resources/ebooks/ansible-for-devops?intcmp=7013a00000263HlAAI">eBook: Ansible for DevOps</a></li>
<li><a href="https://opensource.com/tags/devops?src=devops_resource_menu2">Latest DevOps articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>But it's obvious that no one is volunteering feedback. People are busy. We need to solicit feedback from our customers and stakeholders, but somehow, they always have something more pressing to do. Even if we throw a tantrum and insist that they stop what they're doing and give us much-needed feedback, at best we'd get a few lukewarm comments. Very little to go by. People are busy.</p>
<p>We slowly learn that the most efficient and effective way to solicit feedback is to fail. Failure is a sure-fire way to make our customers and stakeholders drop everything, sit up, and pay attention. If we refuse to fail, we continue marching down the development path confidently, only to discover later that we're in the wrong.</p>
<p>Agile DevOps culture is about dropping this arrogant stance and adopting the attitude of humility. We admit that we don't know it all, and we commit to a more humble approach to working on the value stream.</p>
<p>It is of paramount importance to fail as soon as possible. That way, failure is not critical; it is innocuous, easy to overcome, easy to fix. But we need feedback to know how to fix it. The best feedback is reaction to failure.</p>
<p>Let's illustrate this dynamic visually:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Value generation via feedback loop"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/valuestreamfeedbackloop.jpg" width="366" height="150" alt="Value generation via feedback loop" title="Value generation via feedback loop" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>Value generation via a feedback loop from continuous solicitation</sup></p>
</div>
      
  </article></p>
<p>This figure illustrates the dynamics of producing value by soliciting feedback in a continuous, never-ending fashion.</p>
<h2 id="where-does-the-failure-fit">Where does failure fit?</h2>
<p>Where in the above process do we see failure? Time for another diagram:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Failure is central to feedback loop"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/valuestreamfeedbackloopfailure.jpg" width="366" height="150" alt="Failure is central to feedback loop" title="Failure is central to feedback loop" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>Failure is the central driving force enabling the delivery of value stream.</sup></p>
</div>
      
  </article></p>
<p><em>Failure is center stage</em>. Without failure, nothing useful ever gets done. From this, we conclude that failure is our friend.</p>
<h2 id="how-do-we-know-we-failed">How do we know we failed?</h2>
<p>In the <s>good</s> bad old days of waterfall methodology, the prime directive was "Failure is not an option." We worked under the pressure that every step must be a fully qualified success. We were going out of our way to avoid getting any feedback. Feedback was reserved for the momentous Big Bang event; the point when we all got an earful on how much the system we built missed the mark.</p>
<p>That was, in a nutshell, the traditional way of learning that we failed. With the advent of agile and DevOps, we underwent cultural transformation and embraced incremental, iterative development processes. Each iteration starts with a mini failure, fixes it, and keeps going (mini being the keyword here). But how do we know if we failed?</p>
<p>The only way to know for sure is to have a measurable test or goal. The measurable test will let us know if—and how—we failed.</p>
<p>Now that we have set the stage and exposed the fundamentals of the blameless, failure-centric culture, the next article in this series will dive into a more detailed exposition on how to iterate over failed attempts to satisfy measurable tests and goals.</p>
