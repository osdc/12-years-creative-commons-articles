<p>Recently, I stumbled on an old MacBook Pro 13" from late 2011, with a 125GB SSD and 8GB RAM. I've taken this machine on trips around the world, and back in the day, I ran many a session, workshop, or demo to share all the AppDev goodness from JBoss technologies.</p>
<p>After verifying that its battery works, charging it up, and reinstalling a new OS X, it turns out that the Safari browser version is limited to an old security specification, which means it can't connect to a lot of HTTPS sites now. This renders this solution defunct.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Linux resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux commands cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Free online course: RHEL Technical Overview</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux networking cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">SELinux cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux common commands cheat sheet</a></li>
<li><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">What are Linux containers?</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Our latest Linux articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>What to do with this old thing?</p>
<p>It's been a few years since I worked solely on Linux workstations as a developer. I specifically worked on Fedora, so I decided to try to install the latest version on this MacBook Pro.</p>
<p>It took me just over an hour to get <a href="https://getfedora.org/en/" target="_blank">Fedora 33</a> working on this laptop with the steps below.</p>
<h2 id="download-fedora-33-and-create-a-live-usb">Download Fedora 33 and create a live USB</h2>
<p>The first step is to find the right way to install Fedora. This machine has a CD slot, so you could burn an ISO and boot from it, but I chose to go straight to a bootable USB option.</p>
<p>I got on my other MacBook and visited the <a href="https://getfedora.org/en/workstation/download/" target="_blank">Fedora Workstation site</a>, which links to Fedora Media Writer. Click on the icon for your machine type (in my case, the Apple logo), and you get an installation package.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Fedora Media Writer download screen"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/fedoramediawriter.png" width="594" height="263" alt="Fedora Media Writer download screen" title="Fedora Media Writer download screen" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Eric D. Shabell, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>Start installing it to see a graphical user interface (GUI) that guides you through the process. Select the Fedora Workstation 33 option:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Fedora Workstation download in Fedora Media Writer"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/fedoraworkstation33-installation.png" width="795" height="502" alt="Fedora Workstation download in Fedora Media Writer" title="Fedora Workstation download in Fedora Media Writer" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Eric D. Shabell, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>Next, select the Create Live USB option in the top-right corner:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Create Live USB button"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/create-live-usb.png" width="675" height="226" alt="Create Live USB button" title="Create Live USB button" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Eric D. Shabell, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>The image will start to download, and you will see a drop-down menu to select where to install it:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Downloading Fedora Workstation"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/download_fedora-workstation.png" width="675" height="430" alt="Downloading Fedora Workstation" title="Downloading Fedora Workstation" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Eric D. Shabell, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>Plug in a USB stick with enough space available, then after the download finishes, you can select and install the image on it. Once it's finished, close the GUI and remove the USB stick.</p>
<h2 id="install-linux">Install Linux</h2>
<p>Insert the USB stick you created into the port on the left side of your MacBook Pro, and restart it while holding down the <strong>Option</strong> (or <strong>Alt</strong>) key just to the left of the <strong>Cmd</strong> key. This opens a menu of options to start the machine; use the EFI option, as that's the USB image.</p>
<p>The laptop will boot from the USB device, and you can follow the <a href="https://docs.fedoraproject.org/en-US/fedora/f33/install-guide/install/Booting_the_Installation/" target="_blank">normal Fedora installation</a> process. It helps if you can plug the MacBook Pro into a network cable connection, as the Broadcom WiFi device will not work out of the box.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="MacBook Pro"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/macbook.jpeg" width="675" height="575" alt="MacBook Pro" title="MacBook Pro" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Eric D. Shabell, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>You should get the opportunity to install Fedora to your hard drive and put it on your machine permanently.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Installing Fedora on MacBook Pro"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/macbook_install-fedora.jpeg" width="675" height="536" alt="Installing Fedora on MacBook Pro" title="Installing Fedora on MacBook Pro" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Eric D. Shabell, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>Once the installer completes, reboot your machine, and Fedora 33 should now be the option to boot from.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="MacBook Pro booting into Fedora"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/macbook_fedora-boot.jpeg" width="413" height="551" alt="MacBook Pro booting into Fedora" title="MacBook Pro booting into Fedora" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Eric D. Shabell, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>The only thing missing is a WiFi driver, so keep your network cable connected to install the development packages for the kernel you are running and to build the <code>broadcom-wl</code> driver for that kernel.</p>
<p>Verify the card you need for WiFi:</p>
<pre><code class="language-bash">$ lspci -vnn -d 14e4:</code></pre><p>There will be several items in the output, including something like:</p>
<pre><code class="language-text">Network controller [0280]: Broadcom Inc. and subsidiaries....

Subsystem: Apple Inc. AirPort Extreme...</code></pre><p>Install a repository to pull the Broadcom stuff:</p>
<pre><code class="language-bash">$ su -c 'dnf install -y http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm'</code></pre><p>The next part is interesting: As you look at the running kernel, you'll see <code>v5.9.8-200.fc33</code>, but you will use the development kernel packages to build your Broadcom wireless driver. So, you need to install <code>v5.8.15-301.fc33</code> (available at the time of this writing). Check them using <code>uname -r</code>, and list the installed kernel packages using <code>sudo dnf list kernel</code>:</p>
<pre><code class="language-bash">$ sudo dnf list kernel

kernel.x86_64                     5.8.15-301.fc33

kernel.x86_64                     5.9.8-200.fc33</code></pre><p>Install the development packages:</p>
<pre><code class="language-bash">$ sudo dnf install -y akmods kernel-devel-5.8.15-301.fc33</code></pre><p>
<article class="align-center media media--type-image media--view-mode-full" title="Installing development packages"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/install-development-packages.jpeg" width="413" height="530" alt="Installing development packages" title="Installing development packages" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Eric D. Shabell, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>Install the Broadcom wireless package:</p>
<pre><code class="language-bash">$ sudo dnf install -y broadcom-wl</code></pre><p>Build the kernel module:</p>
<pre><code class="language-bash">$ sudo akmods</code></pre><p>
<article class="align-center media media--type-image media--view-mode-full" title="Building the kernel module"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/build-kernel-module.jpeg" width="413" height="550" alt="Building the kernel module" title="Building the kernel module" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Eric D. Shabell, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>Reboot your machine, and you should be able to view the wireless driver (<code>wl</code>) with:</p>
<pre><code class="language-bash">$ lsmod | grep wl</code></pre><p>Set up your wireless connection in Fedora:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Set up wireless connection"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/wireless-setup.jpeg" width="675" height="594" alt="Set up wireless connection" title="Set up wireless connection" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Eric D. Shabell, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>This article is a bit out of the ordinary for me, but I hope it might help someone else enjoy some fun on the weekend with some old hardware!</p>
<blockquote class="twitter-tweet"><p dir="ltr" lang="en" xml:lang="en" xml:lang="en">Going off the beaten path now... installing <a href="https://twitter.com/hashtag/Fedora?src=hash&amp;ref_src=twsrc%5Etfw">#Fedora</a> on a macbook pro from 2011. Wish me luck! <a href="https://t.co/zlsESnq2Px">pic.twitter.com/zlsESnq2Px</a></p>
<p>— Eric D. Schabell (@ericschabell) <a href="https://twitter.com/ericschabell/status/1330434517883121665?ref_src=twsrc%5Etfw">November 22, 2020</a></p></blockquote>
<script async="" src="https://platform.twitter.com/widgets.js" charset="utf-8"></script><hr /><p><em>This originally appeared on <a href="https://www.schabell.org/2020/11/installing-fedora33-on-macbook-pro-13inch-late-2011.html" target="_blank">Schabell.org</a> and is republished with permission.</em></p>
