<p>Recently, my husband was telling me about an upcoming job interview where he would have to run through some basic commands on a computer. He was anxious about the interview, but the best way for him to learn and remember things has always been to equate the thing he doesn't know to something very familiar to him. Because our conversation happened right after I was roaming the grocery store trying to decide what to cook that evening, it inspired me to write about kubectl and Helm commands by equating them to an ordinary trip to the grocer.</p>
<p><a href="https://helm.sh/" target="_blank">Helm</a> is a tool to manage applications within Kubernetes. You can easily deploy charts with your application information, allowing them to be up and preconfigured in minutes within your Kubernetes environment. When you're learning something new, it's always helpful to look at chart examples to see how they are used, so if you have time, take a look at these stable <a href="https://github.com/helm/charts/tree/master/stable" target="_blank">charts</a>.</p>
<p><a href="https://kubernetes.io/docs/reference/kubectl/kubectl/" target="_blank">Kubectl</a> is a command line that interfaces with Kubernetes environments, allowing you to configure and manage your cluster. It does require some configuration to work within environments, so take a look through the <a href="https://kubernetes.io/docs/reference/kubectl/overview/" target="_blank">documentation</a> to see what you need to do.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on Kubernetes</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/topics/containers/what-is-kubernetes?intcmp=7016000000127cYAAQ">What is Kubernetes?</a></li>
<li><a href="https://www.redhat.com/en/engage/kubernetes-containers-storage-s-201911201051?intcmp=7016000000127cYAAQ">eBook: Storage Patterns for Kubernetes</a></li>
<li><a href="https://www.redhat.com/en/engage/openshift-storage-testdrive-20170718?intcmp=7016000000127cYAAQ">Test drive OpenShift hands-on</a></li>
<li><a href="https://opensource.com/kubernetes-dump-truck?intcmp=7016000000127cYAAQ">eBook: Getting started with Kubernetes</a></li>
<li><a href="https://www.redhat.com/en/resources/managing-containers-kubernetes-openshift-technology-detail?intcmp=7016000000127cYAAQ">An introduction to enterprise Kubernetes</a></li>
<li><a href="https://enterprisersproject.com/article/2017/10/how-explain-kubernetes-plain-english?intcmp=7016000000127cYAAQ">How to explain Kubernetes in plain terms</a></li>
<li><a href="https://opensource.com/downloads/kubernetes-raspberry-pi?intcmp=7016000000127cYAAQ">eBook: Running Kubernetes on your Raspberry Pi homelab</a></li>
<li><a href="https://opensource.com/downloads/kubernetes-cheat-sheet?intcmp=7016000000127cYAAQ">Kubernetes cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/kubernetes-sysadmin?intcmp=7016000000127cYAAQ">eBook: A guide to Kubernetes for SREs and sysadmins</a></li>
<li><a href="https://opensource.com/tags/kubernetes?intcmp=7016000000127cYAAQ">Latest Kubernetes articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>I'll use namespaces in the examples, which you can learn about in my article <a href="https://opensource.com/article/19/12/kubernetes-namespaces"><em>Kubernetes namespaces for beginners</em></a>.</p>
<p>Now that we have that settled, let's start shopping for basic kubectl and Helm commands!</p>
<h2 id="helm-list">Helm list</h2>
<p>What is the first thing you do before you go to the store? Well, if you're organized, you make a <strong>list</strong>. LIkewise, this is the first basic Helm command I will explain.</p>
<p>In a Helm-deployed application, <strong>list</strong> provides details about an application's current release. In this example, I have one deployed application—the Jenkins CI/CD application. Running the basic <strong>list</strong> command always brings up the default namespace. Since I don't have anything deployed in the default namespace, nothing shows up:</p>
<pre><code class="language-bash">$ helm list
NAME    NAMESPACE    REVISION    UPDATED    STATUS    CHART    APP VERSION</code></pre><p>However, if I run the command with an extra flag, my application and information appear:</p>
<pre><code class="language-bash">$ helm list --all-namespaces
NAME  	 NAMESPACE  REVISION  UPDATED                   STATUS      CHART           APP  VERSION
jenkins  jenkins 	1         2020-01-18 16:18:07 EST   deployed    jenkins-1.9.4   lts</code></pre><p>Finally, I can direct the <strong>list</strong> command to check only the namespace I want information from:</p>
<pre><code class="language-bash">$ helm list --namespace jenkins
NAME  	 NAMESPACE  REVISION  UPDATED                   STATUS 	  CHART          APP VERSION
jenkins    jenkins 	1  	       2020-01-18 16:18:07 EST  deployed  jenkins-1.9.4  lts 	</code></pre><p>Now that I have a list and know what is on it, I can go and get my items with <strong>get</strong> commands! I'll start with the Kubernetes cluster; what can I get from it?</p>
<h2 id="kubectl-get">Kubectl get</h2>
<p>The <strong>kubectl get</strong> command gives information about many things in Kubernetes, including pods, nodes, and namespaces. Again, without a namespace flag, you'll always land in the default. First, I'll get the namespaces in the cluster to see what's running:</p>
<pre><code class="language-bash">$ kubectl get namespaces
NAME          	 STATUS   AGE
default       	 Active   53m
jenkins       	 Active   44m
kube-node-lease  Active   53m
kube-public   	 Active   53m
kube-system   	 Active   53m</code></pre><p>Now that I have the namespaces running in my environment, I'll get the nodes and see how many are running:</p>
<pre><code class="language-bash">$ kubectl get nodes
NAME   	   STATUS   ROLES	AGE   VERSION
minikube   Ready    master  55m   v1.16.2</code></pre><p>I have one node up and running, mainly because my Minikube is running on one small server. To get the pods running on my one node:</p>
<pre><code class="language-bash">$ kubectl get pods
No resources found in default namespace.</code></pre><p>Oops, it's empty. I'll get what's in my Jenkins namespace with:</p>
<pre><code class="language-bash">$ kubectl get pods --namespace jenkins
NAME                   	  READY  STATUS	  RESTARTS  AGE
jenkins-7fc688c874-mh7gv  1/1 	 Running  0         40m</code></pre><p>Good news! There's one pod, it hasn't restarted, and it has been running for 40 minutes. Well, since I know the pod is up, I want to see what I can get from Helm.</p>
<h2 id="helm-get">Helm get</h2>
<p><strong>Helm get</strong> is a little more complicated because this <strong>get</strong> command requires more than an application name, and you can request multiple things from applications. I'll begin by getting the values used to make the application, and then I'll show a snip of the <strong>get all</strong> action, which provides all the data related to the application.</p>
<pre><code class="language-bash">$ helm get values jenkins -n jenkins
USER-SUPPLIED VALUES:
null</code></pre><p>Since I did a very minimal stable-only install, the configuration didn't change. If I run the <strong>all</strong> command, I get everything out of the chart:</p>
<pre><code class="language-bash">$ helm get all jenkins -n jenkins</code></pre><p>
<article class="align-center media media--type-image media--view-mode-full" title="output from helm get all command"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/helm-get-all.png" width="585" height="729" alt="output from helm get all command" title="output from helm get all command" /></div>
      
  </article></p>
<p>This produces a ton of data, so I always recommend keeping a copy of a Helm chart so you can look over the templates in the chart. I also create my own values to see what I have in place.</p>
<p>Now that I have all my goodies in my shopping cart, I'll check the labels that <strong>describe</strong> what's in them. These examples pertain only to kubectl, and they describe what I've deployed through Helm.</p>
<h2 id="kubectl-describe">Kubectl describe</h2>
<p>As I did with the <strong>get</strong> command, which can describe just about anything in Kubernetes, I'll limit my examples to namespaces, pods, and nodes. Since I know I'm working with one of each, this will be easy.</p>
<pre><code class="language-bash">$ kubectl describe ns jenkins
Name:     	jenkins
Labels:   	&lt;none&gt;
Annotations:  &lt;none&gt;
Status:   	Active
No resource quota.
No resource limits.</code></pre><p>I can see my namespace's name and that it is active and has no resource nor quote limits.</p>
<p>The <strong>describe pods</strong> command produces a large amount of information, so I'll provide a small snip of the output. If you run the command without the pod name, it will return information for all of the pods in the namespace, which can be overwhelming. So, be sure you always include the pod name with this command. For example:</p>
<pre><code class="language-bash">$ kubectl describe pods jenkins-7fc688c874-mh7gv --namespace jenkins</code></pre><p>
<article class="align-center media media--type-image media--view-mode-full" title="output of kubectl-describe-pods"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/kubectl-describe-pods.png" width="600" height="408" alt="output of kubectl-describe-pods" title="output of kubectl-describe-pods" /></div>
      
  </article></p>
<p>This provides (among many other things) the status of the container, how the container is managed, the label, and the image used in the pod. The data not in this abbreviated output includes resource requests and limits along with any conditions, init containers, and storage volume information applied in a Helm values file. This data is useful if your application is crashing due to inadequate resources, a configured init container that runs a prescript for configuration, or generated hidden passwords that shouldn't be in a plain text YAML file.</p>
<p>Finally, I'll use <strong>describe node</strong>, which (of course) describes the node. Since this example has just one, named Minikube, that is what I'll use; if you have multiple nodes in your environment, you must include the node name of interest.</p>
<p>As with pods, the node command produces an abundance of data, so I'll include just a snip of the output.</p>
<pre><code class="language-bash">$ kubectl describe node minikube</code></pre><p>
<article class="align-center media media--type-image media--view-mode-full" title="output of kubectl describe node"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/kubectl-describe-node.png" width="600" height="180" alt="output of kubectl describe node" title="output of kubectl describe node" /></div>
      
  </article></p>
<p>Note that <strong>describe node</strong> is one of the more important basic commands. As this image shows, the command returns statistics that indicate when the node is running out of resources, and this data is excellent for alerting you when you need to scale up (if you do not have autoscaling in your environment). Other things not in this snippet of output include the percentages of requests made for all resources and limits, as well as the age and allocation of resources (e.g., for my application).</p>
<h2 id="checking-out">Checking out</h2>
<p>With these commands, I've finished my shopping and gotten everything I was looking for. Hopefully, these basic commands can help you, too, in your day-to-day with Kubernetes.</p>
<p>I urge you to work with the command line often and learn the shorthand flags available in the Help sections, which you can access by running these commands:</p>
<pre><code class="language-bash">$ helm --help</code></pre><p>and</p>
<pre><code class="language-bash">$ kubectl -h</code></pre><h2 id="peanut-butter-and-jelly">Peanut butter and jelly</h2>
<p>Some things just go together like peanut butter and jelly. Helm and kubectl are a little like that.</p>
<p>I often use these tools in my environment. Because they have many similarities in a ton of places, after using one, I usually need to follow up with the other. For example, I can do a Helm deployment and watch it fail using kubectl. Try them together, and see what they can do for you.</p>
