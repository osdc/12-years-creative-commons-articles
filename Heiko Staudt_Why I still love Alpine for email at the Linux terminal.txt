<p>Maybe you can relate to this story: You try out a program and really love it. Over the years, new programs are developed that can do the same things and more, maybe even better. You try them out, and they are great too—but you keep coming back to the first program.</p>
<p>That is the story of my relationship with <a href="https://en.wikipedia.org/wiki/Alpine_(email_client)" target="_blank">Alpine Mail</a>. So I decided to write a little article praising my de facto favorite mail program.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="alpine_main_menu.png"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/alpine_main_menu.png" width="800" height="600" alt="alpine_main_menu.png" title="alpine_main_menu.png" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>The main menu screen of the Alpine email client</sup></p>
</div>
      
  </article></p>
<p>In the mid-90's, I discovered the <a href="https://www.gnu.org/gnu/linux-and-gnu.en.html" target="_blank">GNU/Linux</a> operating system. Because I had never seen a Unix-like system before, I read a lot of documentation and books and tried a lot of programs to find my way through this fascinating OS.</p>
<p>After a while, <a href="https://en.wikipedia.org/wiki/Pine_(email_client)" target="_blank">Pine</a> became my favorite mail client, followed by its successor, Alpine. I found it intuitive and easy to use—you can always see the possible commands or options at the bottom, so navigation is easy to learn quickly, and Alpine comes with very good help.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Great Content</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=7016000000127cYAAQ">Free online course: RHEL technical overview</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=7016000000127cYAAQ">Learn Advanced Linux Commands</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Download Cheat Sheets</a></li>
<li><a href="https://opensource.com/alternatives?intcmp=7016000000127cYAAQ">Find an Open Source Alternative</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=7016000000127cYAAQ">Read Top Linux Content</a></li>
<li><a href="https://opensource.com/resources?intcmp=7016000000127cYAAQ">Check out open source resources</a></li>
</ul></div>
</div>
</div>
</div>
<p>Getting started is easy.</p>
<p>Most distributions include Alpine. It can be installed via the package manager: Just press <strong>S</strong> (or navigate the bar to the setup line) and you will be directed to the categories you can configure. At the bottom, you can use the shortcut keys for commands you can do right away. For commands that don’t fit in there, press <strong>O</strong> (<code>Other Commands</code>).</p>
<p>Press <strong>C</strong> to enter the configuration dialog. When you scroll down the list, it becomes clear that you can make Alpine behave as you want. If you have only one mail account, simply navigate the bar to the line you want to change, press <strong>C</strong> (<code>Change Value</code>), and type in the values:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="alpine_setup_configuration.png"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/alpine_setup_configuration.png" width="800" height="600" alt="alpine_setup_configuration.png" title="alpine_setup_configuration.png" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>The Alpine setup configuration screen</sup></p>
</div>
      
  </article></p>
<p>Note how the SNMP and IMAP servers are entered, as this is not the same as in mail clients with assistants and pre-filled fields. If you just enter the server/SSL/user like this:</p>
<p><code>imap.myprovider.com:993/ssl/user=max@example.com</code></p>
<p>Alpine will ask you if "Inbox" should be used (yes) and put curly brackets around the server part. When you're done, press <strong>E</strong> (<code>Exit Setup</code>) and commit your changes by pressing <strong>Y</strong> (yes). Back in the main menu, you can then move to the folder list and the Inbox to see if you have mail (you will be prompted for your password). You can now navigate using <strong><code>&gt;</code></strong> and <strong><code>&lt;</code></strong>.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="navigating_the_message_index.png"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/navigating_the_message_index.png" width="800" height="600" alt="navigating_the_message_index.png" title="navigating_the_message_index.png" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>Navigating the message index in Alpine</sup></p>
</div>
      
  </article></p>
<p>To compose an email, simply navigate to the corresponding menu entry and write. Note that the options at the bottom change depending on the line you are on. <strong><code>^T</code></strong> (<strong>Ctrl</strong> + <strong>T</strong>) can stand for <code>To Addressbook</code> or <code>To Files</code>. To attach files, just navigate to <code>Attchmt:</code> and press either <strong>Ctrl</strong> + <strong>T</strong> to go to a file browser, or <strong>Ctrl</strong> + <strong>J</strong> to enter a path.</p>
<p>Send the mail with <code>^X</code>.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="composing_an_email_in_alpine.png"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/composing_an_email_in_alpine.png" width="800" height="600" alt="composing_an_email_in_alpine.png" title="composing_an_email_in_alpine.png" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>Composing an email in Alpine</sup></p>
</div>
      
  </article></p>
<h2>Why Alpine?</h2>
<p>Of course, every user's personal preferences and needs are different. If you need a more "office-like" solution, an app like Evolution or Thunderbird might be a better choice.</p>
<p>But for me, Alpine (and Pine) are dinosaurs in the software world. You can manage your mail in a comfortable way—no more and no less. It is available for many operating systems (even <a href="https://termux.com/" target="_blank">Termux for Android</a>). And because the configuration is stored in a plain text file (<code>.pinerc</code>), you can simply copy it to a device and it works.</p>
