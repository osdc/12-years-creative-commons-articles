<p>
  The number of universities and schools that have opted for open source alternatives of popular properties solutions has significantly increased over the last years. We often hear about adopting <a href="https://www.openoffice.org/download/" target="_blank">OpenOffice</a> or <a href="http://www.libreoffice.org/" target="_blank">LibreOffice</a> as alternatives to Microsoft Office or about replacing Windows with Linux. Nevertheless, the amount of open source software designed specially for teachers still remains limited. Here are some tips on how to make the school life easier with the help of the commonly used open source software.
</p>
<h2>Ekiga</h2>
<p>
  <a href="http://www.ekiga.org/" target="_blank">Ekiga</a> (formerly known as GnomeMeeting) is an open source softphone, video conferencing, and instant messenger application. This is the great way to communicate with a classroom, parents, and other teachers to share news and experiences, practice foreign languages, and take virtual trips all around the world. It supports HD sound quality and video up to DVD size and quality.
</p>
<p>
  Ekiga is distributed under GNU GPL v.2. It works on Windows, Linux, and other Unix-like systems. The source code can be <a href="http://ftp.gnome.org/pub/gnome/sources/ekiga/4.0/" target="_blank">downloaded</a> from the official website.
</p>
<h2>Pidgin</h2>
<p>
  <a href="http://pidgin.im/" target="_blank">Pidgin</a> is a chat application with a file transfer and away messages support. You can communicate privately or create a chat room to discuss common issues or share latest news. The great number of <a href="https://developer.pidgin.im/wiki/ThirdPartyPlugins" target="_blank">available plugins</a> significantly extends the Pidgin functionality, allowing you to, for example, automatically translate incoming and outgoing messages, draw on a common board, include mathematical formulas, take screenshots, etc.—all to make the discussion more interesting and comprehensive.
</p>
<p>
  Pidgin is distributed under GNU GPL v.2. It runs on Windows, Linux, and other Unix-like operating systems. The source code is listed on <a href="http://sourceforge.net/projects/pidgin/files/Pidgin/2.10.11/" target="_blank">SourceForge</a>.
</p>
<h2>ONLYOFFICE</h2>
<p>
  <a href="https://www.onlyoffice.com/" target="_blank">ONLYOFFICE</a> is a multifunctional online office suite with real-time text, spreadsheet, and presentation co-editing. It provides teachers with a projects toolset to plan and monitor in-class and out-of-class activities, store and share lectures, handouts, student work, and contact information, and involve the class with built-in mail, chat, blogs, forums, polls, bookmarks, calendars, and tools for creating a wiki.
</p>
<p>
  ONLYOFFICE is distributed under GNU AGPL v.3. It runs on Windows, Linux, and other Unix-like operating systems. The source code is listed <a href="https://github.com/ONLYOFFICE" target="_bank">on GitHub</a>. The <a href="https://github.com/ONLYOFFICE/Docker-CommunityServer#installing-onlyoffice-community-server-integrated-with-document-and-mail-servers" target="_blank">Docker technology</a> allows to deploy ONLYOFFICE with one command.
</p>
<h2>ownCloud</h2>
<p>
  <a href="https://owncloud.org/" target="_blank">ownCloud</a> is a file sync and share server. It lets you create and store files, folders, photo galleries, and contacts to this service and keep them synchronized amongst your devices. Teachers can keep and share lectures, make notes and even create and collaborate on rich text documents together with students using ownCloud Documents app, store all the necessary contact information including email, phone and address with ownCloud Contacts, create a class schedule and share it with students to access them from any device at any time with ownCloud Calendar.
</p>
<p>
  ownCloud is distributed under GNU AGPL v.3. It runs on Linux-like operating systems. The source code is listed <a href="https://github.com/owncloud/core" target="_blank">on GitHub</a>.
</p>
<h2>TagSpaces</h2>
<p>
  <a href="http://www.tagspaces.org/" target="_blank">TagSpaces</a> is an open source data manager. It runs completely offline and does not require internet connection or online registration. With TagSpaces, teachers can create and organize numerous notes, webpages, and e-books, tag, group, sort and prioritize files to quickly find the necessary one.
</p>
<p>
  TagSpaces is distributed under GNU AGPL v.3. It supports Windows, Linux, OS X, Android, Firefox, and Chrome. The source code is listed <a href="https://github.com/tagspaces/tagspaces" target="_blank">on GitHub</a>.
</p>
<h2>TaskBoard</h2>
<p>
  <a href="http://taskboard.matthewross.me/" target="_blank">TaskBoard</a> is a Kanban-inspired to-do list. It's a simple way not to forget about important deadlines and keep track of things that need to get done during busy school days. Teachers can enter details about their class schedules and assignment deadlines, plan their classroom activities and visually judge what they have done, what they should be doing and what they should do next.
</p>
<p>
  TaskBoard is distributed under MIT license. It works on almost any web host (only needs PHP5 w/SQLite pdo). The source code is listed <a href="https://github.com/kiswa/TaskBoard" target="_blank">on GitHub</a>.
</p>
<div class="series-wide">Back to<br /> School</div>
<p><em>This article is part of the <a href="https://opensource.com/resources/diversity-open-source" target="_blank">Back to School</a> series focused on open source projects and tools for students of all levels.</em></p>
