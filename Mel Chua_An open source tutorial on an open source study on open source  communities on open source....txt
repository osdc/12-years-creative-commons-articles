<p>Academics - students and teachers both - often want to know what open source community participation will "count" for. Course credit? Research and publication? Better tools to increase efficiency? Teaching? Presentation opportunities?</p>
<p>The answer is "yes."</p>
<p>Let me show you something. <a href="http://misterdavis.org/">Don Davis</a> and Iffat Jabeen, graduate students in Educational Technology at <a href="http://www.txstate.edu/">Texas State University</a> have written a tutorial introduction to the <a href="http://en.wikipedia.org/wiki/R_%28programming_language%29">open source statistics language R</a> that includes a study on legitimate peripheral participation in the GNU/Linux community that uses the software and methodology. Basically, the study is the example they use in the tutorial, or they wrote a tutorial on how they did their study, or...</p>
<p>Wait. How many things <em>did</em> they manage to pack into a single project? Let's take a look. They've made...</p>
<ul><li>A Texas LinuxFest presentation - <em>open source - speaking</em></li>
<li>On an academic paper <em>academia - research</em></li>
<li>Using open source software <em>open source - usage</em></li>
<li>For statistical analysis <em>academia - research</em></li>
<li>Of sociological patterns ("legitimate peripheral participation")  <em>academia - research</em></li>
<li>In an open source community (GNU/Linux) <em>open source -  introspection</em></li>
<li>Being used as a tutorial example <em>academia - teaching</em></li>
<li>For an open source programming language (R) <em>academia -  teaching</em></li>
</ul><p>See the <a href="http://findpdf.net/pdf-viewer/Legitimate-Peripheral-Participation-in-the-GNULinux-Community.html">Texas LinuxFest presentation slides</a> on the <a href="http://linuxlearningsurveyresults.pbworks.com/w/page/34317936/FrontPage">R tutorial</a> they wrote using their paper, <a href="http://misterdavis.org/foss_survey/presentation_of_findings/Davis_Jabeen_LPP_Linux_current.pdf">"Legitimate Peripheral Participation in the GNU/Linux Community"</a>, as a case study--whichever way you slice it, it's a win for all the groups involved.</p>
<p>Open source allowed Davis and Jabeen to bridge a stunning number of communities and domains with a single project - a great addition to their portfolios. What other fascinating community-research hybrids have hit your radar? Tell us about them!</p>
