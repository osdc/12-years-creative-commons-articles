<p>Git is designed in part to enable experimentation. Once you know that your work is safely being tracked and safe states exist for you to fall back upon if something goes horribly wrong, you're not afraid to try new ideas. Part of the price of innovation, though, is that you're likely to make a mess along the way. Files get renamed, moved, removed, changed, and cut into pieces. New files are introduced. Temporary files that you don't intend to track take up residence in your working directory.</p>
<p>In short, your workspace becomes a house of cards, balancing precariously between <em>"it's almost working!"</em> and <em>"oh no, what have I done?"</em>. So what happens when you need to get your repository back to a known state for an afternoon so that you can get some <em>real</em> work done? The classic commands git branch and <a href="https://opensource.com/article/21/4/git-stash">git stash</a> come immediately to mind, but neither is designed to deal, one way or another, with untracked files, and changed file paths and other major shifts can make it confusing to just stash your work away for later. The answer is Git worktree.</p>
<h2 id="what-is-a-git-worktree">What is a Git worktree</h2>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on Git</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/resources/what-is-git?intcmp=7013a000002CxqLAAS">What is Git?</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-git?intcmp=7013a000002CxqLAAS">Git cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-markdown?intcmp=7013a000002CxqLAAS">Markdown cheat sheet</a></li>
<li><a href="https://opensource.com/tags/git?intcmp=7013a000002CxqLAAS">New Git articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>A Git worktree is a linked copy of your Git repository, allowing you to have multiple branches checked out at a time. A worktree has a separate path from your main working copy, but it can be in a different state and on a different branch. The advantage of a new worktree in Git is that you can make a change unrelated to your current task, commit the change, and then merge it at a later date, all without disturbing your current work environment.</p>
<p>The canonical example, straight from the <code>git-worktree</code> man page, is that you're working on an exciting new feature for a project when your project manager tells you there's an urgent fix required. The problem is that your working repository (your "worktree") is in disarray because you're developing a major new feature. You don't want to "sneak" the fix into your current sprint, and you don't feel comfortable stashing changes to create a new branch for the fix. Instead, you decide to create a fresh worktree so that you can make the fix there:</p>
<pre><code class="language-bash">$ git branch | tee
* dev
trunk
$ git worktree add -b hotfix ~/code/hotfix trunk
Preparing ../hotfix (identifier hotfix)
HEAD is now at 62a2daf commit</code></pre><p>In your <code>code</code> directory, you now have a new directory called <code>hotfix</code>, which is a Git worktree linked to your main project repository, with its <code>HEAD</code> parked at the branch called <code>trunk</code>. You can now treat this worktree as if it were your main workspace. You can change directory into it, make the urgent fix, commit it, and eventually remove the worktree:</p>
<pre><code class="language-bash">$ cd ~/code/hotfix
$ sed -i 's/teh/the/' hello.txt
$ git commit --all --message 'urgent hot fix'</code></pre><p>Once you've finished your urgent work, you can return to your previous task. You're in control of when your hotfix gets integrated into the main project. For instance, you can push the change directly from its worktree to the project's remote repo:</p>
<pre><code class="language-bash">$ git push origin HEAD
$ cd ~/code/myproject</code></pre><p>Or you can archive the worktree as a TAR or ZIP file:</p>
<pre><code class="language-bash">$ cd ~/code/myproject
$ git archive --format tar --output hotfix.tar master</code></pre><p>Or you can fetch the changes locally from the separate worktree:</p>
<pre><code class="language-bash">$ git worktree list
/home/seth/code/myproject  15fca84 [dev]
/home/seth/code/hotfix     09e585d [master]</code></pre><p>From there, you can merge your changes using whatever strategy works best for you and your team.</p>
<h2 id="listing-active-worktrees">Listing active worktrees</h2>
<p>You can get a list of the worktrees and see what branch each has checked out using the <code>git worktree list</code> command:</p>
<pre><code class="language-bash">$ git worktree list
/home/seth/code/myproject  15fca84 [dev]
/home/seth/code/hotfix     09e585d [master]</code></pre><p>You can use this from within either worktree. Worktrees are always linked (unless you manually move them, breaking Git's ability to locate a worktree, and therefore severing the link).</p>
<h2 id="moving-a-worktree">Moving a worktree</h2>
<p>Git tracks the locations and states of a worktree in your project's <code>.git</code> directory:</p>
<pre><code class="language-bash">$ cat ~/code/myproject/.git/worktrees/hotfix/gitdir 
/home/seth/code/hotfix/.git</code></pre><p>If you need to relocate a worktree, you must do that using <code>git worktree move</code>; otherwise, when Git tries to update the worktree's status, it fails:</p>
<pre><code class="language-bash">$ mkdir ~/Temp
$ git worktree move hotfix ~/Temp
$ git worktree list
/home/seth/code/myproject  15fca84 [dev]
/home/seth/Temp/hotfix     09e585d [master]</code></pre><h2 id="removing-a-worktree">Removing a worktree</h2>
<p>When you're finished with your work, you can remove it with the <code>remove</code> subcommand:</p>
<pre><code class="language-bash">$ git worktree remove hotfix
$ git worktree list
/home/seth/code/myproject  15fca84 [dev]</code></pre><p>To ensure your <code>.git</code> directory is clean, use the <code>prune</code> subcommand after removing a worktree:</p>
<pre><code class="language-bash">$ git worktree prune</code></pre><h2 id="worktree">When to use worktrees</h2>
<p>As with many options, whether it's tabs or bookmarks or automatic backups, it's up to you to keep track of the data you generate, or it could get overwhelming. Don't use worktrees so often that you end up with 20 copies of your repo, each in a slightly different state. I find it best to create a worktree, do the task that requires it, commit the work, and then remove the tree. Keep it simple and focused.</p>
<p>The important thing is that worktrees provide improved flexibility for how you manage a Git repository. Use them when you need them, and never again scramble to preserve your working state just to check something on another branch.</p>
