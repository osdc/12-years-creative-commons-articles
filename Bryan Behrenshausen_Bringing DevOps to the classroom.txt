<p>As coordinator for the Minnesota State Digital Curricula Initiative, <a href="https://www.stthomas.edu/gradsoftware/about/faculty/charles-betz.html">Charles Betz</a> faces a unique challenge: How to teach DevOps (agile, relentless, fast) in an academic context (inflexible, deliberate, slow)?</p>
<p>But Betz has found a way—and it's been successful enough that he's ready to share it. He'll do just that at this year's <a href="http://events.itrevolution.com/us/">DevOps Enterprise Summit</a> during his presentation, "Influencing Higher Education to Create the Future DevOps Workforce."</p>
<p>Betz, an <a href="https://www.stthomas.edu/gradsoftware/about/faculty/charles-betz.html">instructor at the University of St. Thomas</a> spoke with Opensource.com about working with the next generation of student programmers.</p>
<p><img alt="Interview banner Q&amp;amp;A.png" src="https://opensource.com/sites/default/files/images/life/Interview%20banner%20Q%26A.png" style="width:520px;height:88px;" width="520" height="88" /></p>
<h3>DevOps culture and academic culture seem to move at vastly different speeds. Why is that?</h3>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More DevOps resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/resources/devops?intcmp=7013a00000263HlAAI">What is DevOps?</a></li>
<li><a href="https://opensource.com/downloads/devops-hiring-guide?intcmp=7013a00000263HlAAI">The ultimate DevOps hiring guide</a></li>
<li><a href="https://opensource.com/downloads/devops-monitoring-guide?intcmp=7013a00000263HlAAI">DevOps monitoring tools guide</a></li>
<li><a href="https://opensource.com/downloads/devsecops?intcmp=7013a00000263HlAAI">Getting started with DevSecOps</a></li>
<li><a href="https://enterprisersproject.com/cheat-sheet-devops-glossary?intcmp=7013a00000263HlAAI">Download the DevOps glossary</a></li>
<li><a href="https://www.ansible.com/resources/ebooks/ansible-for-devops?intcmp=7013a00000263HlAAI">eBook: Ansible for DevOps</a></li>
<li><a href="https://opensource.com/tags/devops?src=devops_resource_menu2">Latest DevOps articles</a></li>
</ul></div>
</div>
</div>
</div>

<p>There are good and bad reasons for this. Change is expensive, and, speaking as a teaching faculty member, we can't radically refactor our courses every semester and still give good service to our students. So academia will always lag industry. But there is a difference between moving at the same speed, a few miles back, versus moving more slowly and falling further and further behind. The problem today is that the gap is increasing. Academia has not had a complete understanding of the interrelated changes to the digital economy. It's no longer enough to teach a little Agile as part of project management courses. I think we need to look to the master curricula guidance that comes from the ACM, IEEE, and AIS, and start asking those major professional organizations to up their game a bit. It's not like there isn't plenty of evidence coming out of industry lately!</p>
<p>However, I disagree a bit with those who think that "things will always be changing too fast." We are seeing a generational—tectonic, if you will—shift in fundamental principles and I don't think it's always "flavor of the month." For example, I don't see infrastructure as code going away any time soon. I'm quite comfortable baking that into my courses. Nor do I think we're going to go back to the bad old days of waterfall, or stage-gated project management, or disregarding culture. The important changes are here to stay and can be engaged with by academics. Even if we're still teaching on VMs and not Docker yet (which is where I'm currently at).</p>
<h3>Why is DevOps important for students today?</h3>
<p>Well, it's clearly how the highest performing digital shops are running nowadays. Software education stopped at "code complete" for many years, especially when software was being stamped on CDs for delivery. Now it's delivered as an operational service, the customer outcome is experienced that way, so covering the full lifecycle is essential. Of course there are deeper reasons as well, like the need for fast feedback in product management, and the move away from big open-loop batches of work. The biggest issue I hear from hiring managers? Students are still coming out of programs unfamiliar with source control. That's just educational malpractice. We've got not one but two courses now at the University of St. Thomas covering DevOps (one higher level and more contextual, one more detailed).</p>
<h3>You've developed materials for other trainers and instructors, too. What's your goal in developing them?</h3>
<p>I guess I have multiple goals. Certainly, there is an aspect of sharing that reflects I think the better cultural aspects of being an academic. Personally, developing the material requires deep learning and investigation, so I grow in my career. And of course, there is a marketing aspect to it. I'm currently finishing up the first graduate-level survey text on IT management grounded in Lean and Agile, and I hope for some attention to that work.</p>
<h3>What can we expect in your talk about the DevOps Enterprise Summit?</h3>
<p>There are two major parts (and I'm going to have to talk fast!).</p>
<p>First, this will be the first official presentation of <a href="http://dynamicit.education/" target="_blank">a report I've developed</a> with colleagues in the Minnesota State system on next-generation digital curricula. We've had great participation in this report and as far as I know it is the first systemic educational response to Agile and DevOps. By the time I speak, that report will have gone out to over 300 faculty and deans at dozens of universities and colleges across Minnesota. I have to give Nicole Forsgren and Gene Kim a big thank you for agreeing to be on the advisory board for that effort.</p>
<p>Second, I'll touch on some of the pedagogical challenges and opportunities we have at the micro scale in the classroom, with some notes on my IT management class at the University of St. Thomas. One thing most academics don't seem to realize is how powerful a tool virtualization can be in the classroom; I've developed a full continuous delivery pipeline using Vagrant and Virtualbox with Jenking and Artifactory... that will run on a laptop! We sure do live in interesting times.</p>
