<p>In most situations, from getting clothing advice to seeking peer review of the next scientific discovery, we harness the help of people around us in order to discuss and analyze potential next steps. Hardly anyone thinks up a perfect solution right off the bat; it's an iterative process full of trials and errors, adjustments, and new experiments.</p>
<p>And it's a process we can always improve. This chapter offers some advice for doing just that.</p>
<h2>What are feedback loops?</h2>
<p>Feedback loops are supposed to be great and solve all sorts of problems. So what are they, exactly?</p>
<p>Remember when you were a child and you drew your first picture of a cat? You proudly showed it to your parents, and they suggested you put a tail on it. You went back to the drawing board (literally) and added the tail, showed them the result, and then they put the drawing up on the fridge.</p>
<p>That was an early feedback loop for you: A process that fed into itself, like the snake eating its own tail.</p>
<p>You might be familiar with another early feedback loop called the "Deming Cycle":</p>
<blockquote><p><em>Plan – Do – Check – Act</em></p></blockquote>
<p>W. Edwards Deming later updated this to:</p>
<blockquote><p><em>Plan – Do – </em><strong><em>Study</em></strong><em> – Act</em></p></blockquote>
<p>...which I agree is a better description.</p>
<p>Similar cycles or processes are The Shewhart Cycle, Six Sigma (<em>Define – Measure – Analyze – Improve – Control</em>), and The Lean Startup (<em>Build – Measure – Learn</em>).</p>
<p>Common to all these is a scientific approach to working in an iterative mode: try something, learn from it, and adapt your work accordingly when moving forward. In other words:</p>
<blockquote><p>Practice doesn't make perfect. Practice makes permanent. Feedback makes perfect.</p></blockquote>
<h2>Why are feedback loops important?</h2>
<p>Shorter feedback loops (that is, loops that take less time between the moment you try something and the moment you learn about its effects or outcomes) allow you to fix or improve work quickly and derive additional value faster. Performing a small fix, receiving feedback on it, and trying again should not be a tremendous burden—as it would be if you'd been working on something for a long time and find out you'll have start over. In other words, you might be running in the wrong direction, but if you receive feedback early you won't have such a long way to backtrack when starting again.</p>
<p>Various agile software development methods consider short feedback loops important for <em>being</em> agile and producing the <em>right result</em> in the shortest amount of time.</p>
<p>
</p><div class="embedded-callout-menu callout-float-left">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Learn about open organizations</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://theopenorganization.org">Download resources</a></li>
<li><a href="https://theopenorganization.community">Join the community</a></li>
<li><a href="https://opensource.com/open-organization/resources/open-org-definition?src=too_resource_menu3a">What is an open organization?</a></li>
<li><a href="https://opensource.com/open-organization/resources/open-org-maturity-model?src=too_resource_menu4a">How open is your organization?</a></li>
</ul></div>
</div>
</div>
</div>

<p>Teams can arrange and work through these feedback loops via techniques like "pair" or "mob programming," daily standup meetings, and sprints. When working via pair or mob programming, for instance, developers experience feedback loops directly between the people working together on a task. In daily standup meetings, they get feedback from coworkers. After a sprint, they would preferably receive feedback from the customer. In all these cases, the feedback people receive helps them improve their work before beginning an additional step in some process.</p>
<p>This way of working is possible even outside of software development. But here I want to specifically focus on how it can enhance IT organizations. I'll cover a few general terms, which apply to all departments, teams, and manners of work.</p>
<h2>How can we enhance feedback loops in IT organizations?</h2>
<p>In IT organizations, being transparent is a prerequisite to giving and receiving feedback. People can be transparent about both their ongoing and their planned work.</p>
<p>For example, I've had positive experiences using kanban boards with operations teams. When the board is visible to all stakeholders, managers, and other teams, everyone receives feedback on the current status of work items and current priorities. People also have the opportunity to receive spontaneous feedback from someone looking at the board and noticing something that, for instance, another team might also be working on, or is not important anymore, or (even better) something very important that's missing from the board but should definitely be on it.</p>
<p>I recommend searching for feedback as early as possible—right from the start, if you can. I'll often outline an assignment and run that by a manager and key stakeholders. It's the best way to find out if I have understood the task properly and helps me set <em>their</em> expectations for what I'll deliver next.</p>
<p></p><div class="embedded-callout-text callout-float-right">Keep your feedback loops short. You should seek feedback before investing too much time or money into something.</div>
<p>This all sounds simple enough. So why is this often so hard to put into place? What are some of the most common blockers, and what can you do to improve or facilitate better feedback?</p>
<p>For feedback loops to work well, you need to have an open climate, one where people feel safe sharing their thoughts. Incidentally, paying attention to feedback loops can also help you improve the current climate in your organization. That's the beauty of a feedback loop: As it feeds into itself, if the climate and culture around you is not open by default, then you can change this by being more open yourself, offer feedback, make sure to get feedback from others, internalize the feedback, and improve. Little by little, you and the organization around you will improve, too. As Mahatma Gandhi put it:</p>
<blockquote><p>Be the change that you wish to see in the world.</p></blockquote>
<p>Remember: Keep your feedback loops short. You should seek feedback before investing too much time or money into something. Then changing things isn't so difficult, should you need to do it.</p>
<p>You'll also want to make sure you have opportunities to receive valuable feedback from people who usually are not comfortable sharing their thoughts openly. Often, people will solicit feedback at a demo or a meeting—meaning they do it <em>in person</em> and receive it when people <em>speak up</em>. That is perhaps not the best form of communication for everyone, and sticking to that single feedback environment might cause you to miss great insights you'd otherwise want to know about. One way to improve this could be to send out a post-meeting email to everyone involved, reminding them to send their feedback directly to you (preferably within a set time frame). Some people prefer to formulate their ideas in their own time and through a medium that suits them better (rather than speaking up in front of everyone!).</p>
<p>The best kind of feedback you can receive is that which comes directly from an actual user or customer—but what do you do when you're working with infrastructure several layers <em>away</em> from the customers?</p>
<p>In the best of worlds, customer feedback would trickle down to all involved areas and teams, but we all know that is hard and usually doesn't happen in real life. Depending on your products or services, you could arrange workshops together with the customer and include people from all layers of the organization. I have done this with great results. Discussions and ideas that would never have popped up otherwise suddenly appear and action plans get put in place.</p>
<p>If you have regular meetings with your customers, you could invite people from other parts of the organization as guests every once in a while. In my experience, all parties have appreciated this kind of initiative.</p>
<p>In other organizations, one might consider the surrounding internal teams and departments to be customers and facilitate feedback loops with them. They could, in turn, get feedback from <em>their</em> customers. I recommend that you try to set this up in all teams, as you will need good feedback not only from your external customers but also from your partners and peers as well.</p>
<p>Last, but not least: Remember that feedback loops doesn't necessary have to involve <em>human</em> interactions. For instance you should receive valuable feedback from your monitoring systems and incident management tools. Automated feedback can make you aware of slower response times that might indicate an underlying problem with a new release, re-occuring minor incidents could be the result of soon-to-be faulty hardware that would cause a major outage, and statistics showing a growing user base of your services should trigger a plan to scale up the environment in time before it suffers from performance issues.</p>
<h2>Some feedback on feedback</h2>
<p>In the end, a team's ability to feedback loops boils down to two factors: communication and transparency.</p>
<p>When you're open about your progress and willing to accept others' insights, you can more quickly adapt and create the best outcomes. For a long time, working in silos until you're ready to reveal your results has been the norm; however, that's changing as we discover the advantages of involving more people and ideas into the design and execution processes.</p>
<p>The world is changing more rapidly than it ever has, and adapting quickly has never been more crucial. Ignoring the feedback loops occurring all around you could cause your solution or idea to arrive too late—or to chase the wrong problem altogether. Feedback, on the other hand, makes perfect.</p>
<p><em>This article is part of <a href="https://opensource.com/open-organization/resources/culture-change">The Open Organization Guide to IT culture change</a>.</em></p>
