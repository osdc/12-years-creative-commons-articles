<p>Culture is an important part of DevOps, so many organizations have put an enormous amount of effort into trying to figure out what makes a great team. Google formed an entire initiative, called <a href="https://rework.withgoogle.com/print/guides/5721312655835136/" target="_blank">Project Aristotle</a>, to study this question. The answer wasn't free lunches, great benefits, or wonderful salaries. Rather, Google's research determined that culture played a significant role in effective teams, particularly culture around creating physiological safety.</p>
<p>So who is in charge of culture? Who should be shepherding a culture that is conducive of safety, security, and speed?</p>
<p>The knee-jerk reaction is "everyone is in charge of culture." This makes perfect sense. Culture, by definition, involves everyone who is part of it. The person sitting next to you, their manager, and the people occupying corner offices all have a part in defining culture. Your organization lives and dies by the success of its people, so the people should drive its culture. But to say <em>everyone</em> is in charge of something is to say <em>no one</em> is in charge of something, especially when that thing is as fragile as culture.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More DevOps resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/resources/devops?intcmp=7013a00000263HlAAI">What is DevOps?</a></li>
<li><a href="https://opensource.com/downloads/devops-hiring-guide?intcmp=7013a00000263HlAAI">The ultimate DevOps hiring guide</a></li>
<li><a href="https://opensource.com/downloads/devops-monitoring-guide?intcmp=7013a00000263HlAAI">DevOps monitoring tools guide</a></li>
<li><a href="https://opensource.com/downloads/devsecops?intcmp=7013a00000263HlAAI">Getting started with DevSecOps</a></li>
<li><a href="https://enterprisersproject.com/cheat-sheet-devops-glossary?intcmp=7013a00000263HlAAI">Download the DevOps glossary</a></li>
<li><a href="https://www.ansible.com/resources/ebooks/ansible-for-devops?intcmp=7013a00000263HlAAI">eBook: Ansible for DevOps</a></li>
<li><a href="https://opensource.com/tags/devops?src=devops_resource_menu2">Latest DevOps articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>Culture change in DevOps can come from the bottom up or the top down. Let's take a look at both.</p>
<h2>Bottom-up approach</h2>
<p>The bottom-up approach to culture change is more common when the push towards digital transformation begins at the individual contributor level. This bottom-up approach to managing culture is one of the most difficult to adopt, but the shift will be the easiest to put in place, to a point. If individual contributors on a team decide to change the way work occurs, a de facto center of excellence is born.</p>
<p>To be successful, the team members must agree upon their core values and work to bring in as many allies as possible. Once there are enough allies on the team's level, they need to gain more allies above them on the organizational chart. This is where the rubber meets the road. Are the team's core values beneficial to the business? Does the individual contributors' culture fit management's leadership style?</p>
<p>If this is going to be too difficult or take too much time, the individual contributors must bring in more allies to influence others. Something to remember with this approach is: people resist change. The bottom-up approach to drive and maintain culture in an organization is the most arduous, because it can often feel threatening to people.</p>
<h2>Top-down approach</h2>
<p>The top-down approach involves a set of core values that is driven through all facets of the business. Engineers combine into product teams built by human resources that have the company's cultural values woven through them. The top-down approach is more powerful and the easiest to put in place, but it's the hardest to change unless it's adjusted at the top.</p>
<p></p><div class="embedded-callout-text callout-float-left">Writing a few bullet points and printing them on colorful posters is not leadership.</div>
<p>If you analyze culture at Uber under Travis Kalanick, you can see that the company's culture was growth at all costs. This was obvious in the no-holds-barred approaches to fundraising, conflict resolution, and human resources.</p>
<p>If you look at Red Hat, you can see a positive, top-down approach to culture. President and CEO <a href="https://opensource.com/users/jwhitehurst">Jim Whitehurst</a> has fostered a <a href="https://opensource.com/open-organization">culture of openness</a>, where ideas flow from bottom to top and back again. On a recent trip to Red Hat headquarters in Raleigh, I felt this positive, idea-fostering culture just by walking around the offices. It was easy to see the culture in action during employees' everyday business activities.</p>
<p><article class="media media--type-image media--view-mode-full" title="Facebook post"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/chris_short_fb_embed.png" width="520" height="570" alt="Facebook post" title="Facebook post" /></div>
      
  </article></p>
<blockquote class="twitter-tweet" data-lang="en"><p dir="ltr" lang="en" xml:lang="en" xml:lang="en">I think the philosophy of people who value scale above all else is "It's okay to be unstable as a business &amp; miserable as a human now if that means one day I might be really really rich" and I reject this theory wholeheartedly</p>
<p>— Stephanie Hurlburt (@sehurlburt) <a href="https://twitter.com/sehurlburt/status/931956357602799616?ref_src=twsrc%5Etfw">November 18, 2017</a></p></blockquote>
<script async="" src="https://platform.twitter.com/widgets.js" charset="utf-8"></script><h2>Why leadership matters</h2>
<p>The best driver of culture is the CEO of an organization. The head of the organization can set the tone for all activities and spread the culture out to all the people within it. If the leader does not drive the organization's culture in the desired direction, the culture will not be defined. Writing a few bullet points and printing them on colorful posters is not leadership. Having HR draft up some talking points for managers is not driving culture. To define a culture, the organization's ideals, values, and tenets must be instilled and driven by its most senior leader.</p>
