<p>Version control is an inseparable part of software developers' daily lives. It's hard to imagine any team developing software without using a version control tool. It's equally difficult to envision any developer who hasn't worked with (or at least heard of) Git. In the 2018 Stackoverflow Developer Survey, 87.2% of the 74,298 participants <a href="https://insights.stackoverflow.com/survey/2018#work-_-version-control" target="_blank">use Git</a> for version control.</p>
<p>Linus Torvalds created git in 2005 for developing the Linux kernel. This article walks through the <code>git stash</code> command and explores some useful options for stashing changes. It assumes you have basic familiarity with <a href="https://opensource.com/downloads/cheat-sheet-git">Git concepts</a> and a good understanding of the working tree, staging area, and associated commands.</p>
<h2 id="why-is-git-stash-necessary">Why is git stash important?</h2>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on Git</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/resources/what-is-git?intcmp=7013a000002CxqLAAS">What is Git?</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-git?intcmp=7013a000002CxqLAAS">Git cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-markdown?intcmp=7013a000002CxqLAAS">Markdown cheat sheet</a></li>
<li><a href="https://opensource.com/tags/git?intcmp=7013a000002CxqLAAS">New Git articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>The first thing to understand is why stashing changes in Git is important. Assume for a moment that Git doesn't have a command to stash changes. Suppose you are working on a repository with two branches, A and B. The A and B branches have diverged from each other for quite some time and have different heads. While working on some files in branch A, your team asks you to fix a bug in branch B. You quickly save your changes to A and try to check out branch B with <code>git checkout B</code>. Git immediately aborts the operation and throws the error, "Your local changes to the following files would be overwritten by checkout … Please commit your changes or stash them before you switch branches."</p>
<p>There are few ways to enable branch switching in this case:</p>
<ul><li>Create a commit at that point in branch A, commit and push your changes to fix the bug in B, then check out A again and run <code>git reset HEAD^</code> to get your changes back.</li>
<li>Manually keep the changes in files not tracked by Git.</li>
</ul><p>The second method is a bad idea. The first method, although appearing conventional, is less flexible because the unfinished saved changes are treated as a checkpoint rather than a patch that's still a work in progress. This is exactly the kind of scenario git stash is designed for.</p>
<p>Git stash saves the uncommitted changes locally, allowing you to make changes, switch branches, and perform other Git operations. You can then reapply the stashed changes when you need them. A stash is locally scoped and is not pushed to the remote by <code>git push</code>.</p>
<h2 id="how-to-use-git-stash">How to use git stash</h2>
<p>Here's the sequence to follow when using git stash:</p>
<ol><li>Save changes to branch A.</li>
<li>Run <code>git stash</code>.</li>
<li>Check out branch B.</li>
<li>Fix the bug in branch B.</li>
<li>Commit and (optionally) push to remote.</li>
<li>Check out branch A</li>
<li>Run <code>git stash pop</code> to get your stashed changes back.</li>
</ol><p>Git stash stores the changes you made to the working directory locally (inside your project's .git directory; <code>/.git/refs/stash</code>, to be precise) and allows you to retrieve the changes when you need them. It's handy when you need to switch between contexts. It allows you to save changes that you might need at a later stage and is the fastest way to get your working directory clean while keeping changes intact.</p>
<h2 id="how-to-create-a-stash">How to create a stash</h2>
<p>The simplest command to stash your changes is <code>git stash</code>:</p>
<pre><code class="language-bash">$ git stash
Saved working directory and index state WIP on master; d7435644 Feat: configure graphql endpoint</code></pre><p>By default, <code>git stash</code> stores (or "stashes") the uncommitted changes (staged and unstaged files) and overlooks untracked and ignored files. Usually, you don't need to stash untracked and ignored files, but sometimes they might interfere with other things you want to do in your codebase.</p>
<p>You can use additional options to let <code>git stash</code> take care of untracked and ignored files:</p>
<ul><li><code>git stash -u</code> or <code>git stash --include-untracked</code> stash untracked files.</li>
<li><code>git stash -a</code> or <code>git stash --all</code> stash untracked files and ignored files.</li>
</ul><p>To stash specific files, you can use the command <code>git stash -p</code> or <code>git stash –patch</code>:</p>
<pre><code class="language-bash">$ git stash --patch
diff --git a/.gitignore b/.gitignore
index 32174593..8d81be6e 100644
--- a/.gitignore
+++ b/.gitignore
@@ -3,6 +3,7 @@
 # dependencies
 node_modules/
 /.pnp
+f,fmfm
 .pnp.js

 # testing
(1/1) Stash this hunk [y,n,q,a,d,e,?]?</code></pre><h2 id="listing-your-stashes">Listing your stashes</h2>
<p>You can view your stashes with the command <code>git stash list</code>. Stashes are saved in a last-in-first-out (LIFO) approach:</p>
<pre><code class="language-bash">$ git stash list
stash@{0}: WIP on master: d7435644 Feat: configure graphql endpoint</code></pre><p>By default, stashes are marked as WIP on top of the branch and commit that you created the stash from. However, this limited amount of information isn't helpful when you have multiple stashes, as it becomes difficult to remember or individually check their contents. To add a description to the stash, you can use the command <code>git stash save &lt;description&gt;</code>:</p>
<pre><code class="language-bash">$ git stash save "remove semi-colon from schema"
Saved working directory and index state On master: remove semi-colon from schema

$ git stash list
stash@{0}: On master: remove semi-colon from schema
stash@{1}: WIP on master: d7435644 Feat: configure graphql endpoint</code></pre><h2 id="retrieving-stashed-changes">Retrieving stashed changes</h2>
<p>You can reapply stashed changes with the commands <code>git stash apply</code> and <code>git stash pop</code>. Both commands reapply the changes stashed in the latest stash (that is, <code>stash@{0}</code>). A <code>stash</code> reapplies the changes while <code>pop</code> removes the changes from the stash and reapplies them to the working copy. Popping is preferred if you don't need the stashed changes to be reapplied more than once.</p>
<p>You can choose which stash you want to pop or apply by passing the identifier as the last argument:</p>
<pre><code class="language-bash">$ git stash pop stash@{1} </code></pre><p>or</p>
<pre><code class="language-bash">$ git stash apply stash@{1}</code></pre><h2 id="cleaning-up-the-stash">Cleaning up the stash</h2>
<p>It is good practice to remove stashes that are no longer needed. You must do this manually with the following commands:</p>
<ul><li><code>git stash clear</code> empties the stash list by removing all the stashes.</li>
<li><code>git stash drop &lt;stash_id&gt;</code> deletes a particular stash from the stash list.</li>
</ul><h2 id="checking-stash-diffs">Checking stash diffs</h2>
<p>The command <code>git stash show &lt;stash_id&gt;</code> allows you to view the diff of a stash:</p>
<pre><code class="language-bash">$ git stash show stash@{1}
console/console-init/ui/.graphqlrc.yml        |   4 +-
console/console-init/ui/generated-frontend.ts | 742 +++++++++---------
console/console-init/ui/package.json          |   2 +-</code></pre><p>To get a more detailed diff, pass the <code>--patch</code> or <code>-p</code> flag:</p>
<pre><code class="language-bash">$ git stash show stash@{0} --patch
diff --git a/console/console-init/ui/package.json b/console/console-init/ui/package.json
index 755912b97..5b5af1bd6 100644
--- a/console/console-init/ui/package.json
+++ b/console/console-init/ui/package.json
@@ -1,5 +1,5 @@
 {
- "name": "my-usepatternfly",
+ "name": "my-usepatternfly-2",
  "version": "0.1.0",
  "private": true,
  "proxy": "http://localhost:4000"
diff --git a/console/console-init/ui/src/AppNavHeader.tsx b/console/console-init/ui/src/AppNavHeader.tsx
index a4764d2f3..da72b7e2b 100644
--- a/console/console-init/ui/src/AppNavHeader.tsx
+++ b/console/console-init/ui/src/AppNavHeader.tsx
@@ -9,8 +9,8 @@ import { css } from "@patternfly/react-styles";

interface IAppNavHeaderProps extends PageHeaderProps {
- toolbar?: React.ReactNode;
- avatar?: React.ReactNode;
+ toolbar?: React.ReactNode;
+ avatar?: React.ReactNode;
}

export class AppNavHeader extends React.Component&lt;IAppNavHeaderProps&gt;{
  render()</code></pre><h2 id="checking-out-to-a-new-branch">Checking out to a new branch</h2>
<p>You might come across a situation where the changes in a branch and your stash diverge, causing a conflict when you attempt to reapply the stash. A clean fix for this is to use the command <code>git stash branch &lt;new_branch_name stash_id&gt;</code>, which creates a new branch based on the commit the stash was created <em>from</em> and pops the stashed changes to it:</p>
<pre><code class="language-bash">$ git stash branch test_2 stash@{0}
Switched to a new branch 'test_2'
On branch test_2
Changes not staged for commit:
(use "git add &lt;file&gt;..." to update what will be committed)
(use "git restore &lt;file&gt;..." to discard changes in working directory)
modified: .graphqlrc.yml
modified: generated-frontend.ts
modified: package.json
no changes added to commit (use "git add" and/or "git commit -a")
Dropped stash@{0} (fe4bf8f79175b8fbd3df3c4558249834ecb75cd1)</code></pre><h2 id="stashing-without-disturbing-the-stash-reflog">Stashing without disturbing the stash reflog</h2>
<p>In rare cases, you might need to create a stash while keeping the stash reference log (reflog) intact. These cases might arise when you need a script to stash as an implementation detail. This is achieved by the <code>git stash create</code> command; it creates a stash entry and returns its object name without pushing it to the stash reflog:</p>
<pre><code class="language-bash">$ git stash create "sample stash" 
63a711cd3c7f8047662007490723e26ae9d4acf9</code></pre><p>Sometimes, you might decide to push the stash entry created via <code>git stash create</code> to the stash reflog:</p>
<pre><code class="language-bash">$ git stash store -m "sample stash testing.." "63a711cd3c7f8047662007490723e26ae9d4acf9"
$ git stash list
stash @{0}: sample stash testing..</code></pre><h2 id="conclusion">Conclusion</h2>
<p>I hope you found this article useful and learned something new. If I missed any useful options for using stash, please let me know in the comments.</p>
