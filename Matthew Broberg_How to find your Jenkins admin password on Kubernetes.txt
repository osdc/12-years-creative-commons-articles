<p>The tooling to make Kubernetes easier to navigate is so good at times, I get surprised when I can't find a simple way to get an answer. As someone who doesn't use Kubernetes day-to-day, any intermediate level of troubleshooting turns into an afternoon of first, questioning my sanity and second, considering a job as a shepherd or something else that's away from the keyboard.</p>
<p>It began simply. In this case, I was following a tutorial that depended on a Kubernetes cluster, so I used <a href="https://github.com/kubernetes/minikube" target="_blank">Minikube</a>, which is a nice and easy virtual environment that makes running a clustered environment possible on a laptop. That installation went well, so I got started with <a href="https://helm.sh/" target="_blank">Helm</a>, which is a wonderfully straightforward package manager for Kubernetes. I used one of Helm's <a href="https://helm.sh/docs/developing_charts/" target="_blank">Charts</a> to install Jenkins, and I was up and running without a problem. <a href="https://jenkins.io/" target="_blank">Jenkins</a> is the de facto standard for <a href="https://opensource.com/article/19/4/devops-pipeline">DevOps pipelines</a> and to have it running in just a few minutes is pretty great. </p>
<p>I had my environment configured and everything was responding. <strong>Now, what's the <span style="color:null;">admin</span> password to log into Jenkins?</strong></p>
<p>As part of the Helm Chart, Jenkins configures a default username and password for its graphical user interface (GUI). I had the GUI up and running, but I had no idea how to log in.</p>
<p>I had no clue what I missed, so I looked at the GitHub <a href="https://github.com/helm/charts/tree/master/stable/jenkins" target="_blank">repository for the chart</a> and searched as much as I could. I tried googling all the related search patterns I could think of:</p>
<ul><li>How to retrieve a Jenkins admin password on Kubernetes</li>
<li>Kubernetes Jenkins password Helm</li>
<li>Jenkins admin password Helm Chart</li>
<li>Helm Jenkins how-to</li>
</ul><p>While there are a ton of great tutorials out there, there wasn't an answer that met me where I was at the beginner level.</p>
<h2 id="tldr-the-answer">TL;DR the answer</h2>
<p>If you just want to look up your Jenkins password and don't care about why this works, run the following:</p>
<pre><code class="language-bash">printf $(kubectl get secret --namespace jenkins jenkins -o jsonpath="{.data.jenkins-admin-password}" | base64 --decode);echo</code></pre><p>The default user is <strong>admin</strong>. Now that you know both of those details, you can log into the UI. Where is the UI you ask? Run this:</p>
<pre><code class="language-bash"># Be sure to update your jenkins pod name in the following command
$ kubectl port-forward jenkins-7565554b8f-cvhbd 8088:8080</code></pre><p>Then you can navigate to <strong>127.0.0.1:8088</strong> to access the UI.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Jenkins login screen"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/jenkins-login.png" width="600" height="430" alt="Jenkins login screen" title="Jenkins login screen" /></div>
      
  </article></p>
<h2 id="the-scenario">The scenario</h2>
<p>Here's the background on this solution. I have a working local Kubernetes environment, thanks to Minikube. I installed the Kubernetes package manager <a href="https://helm.sh/" target="_blank">Helm</a> via Homebrew (<a href="https://opensource.com/article/19/6/virtual-environments-python-macos">as a MacOS user</a>) and took the next step:</p>
<pre><code class="language-bash">$ helm install --name jenkins stable/jenkins --namespace jenkins
Error: could not find tiller</code></pre><p><em><strong>Just kidding</strong></em>—whenever Helms runs for the first time in a cluster, you have to initialize the Tiller service. The <a href="https://helm.sh/docs/glossary/#tiller" target="_blank">Helm glossary</a> says:</p>
<blockquote><p>Tiller is the in-cluster component of Helm. It interacts directly with the Kubernetes API server to install, upgrade, query, and remove Kubernetes resources. It also stores the objects that represent releases.</p>
</blockquote>
<p>Let's do this right:</p>
<pre><code class="language-bash">$ helm init
Creating /Users/mbbroberg/.helm
Creating /Users/mbbroberg/.helm/repository
Creating /Users/mbbroberg/.helm/repository/cache
Creating /Users/mbbroberg/.helm/repository/local
Creating /Users/mbbroberg/.helm/plugins
Creating /Users/mbbroberg/.helm/starters
Creating /Users/mbbroberg/.helm/cache/archive
Creating /Users/mbbroberg/.helm/repository/repositories.yaml
Adding stable repo with URL: https://kubernetes-charts.storage.googleapis.com
Adding local repo with URL: http://127.0.0.1:8879/charts
$HELM_HOME has been configured at /Users/mbbroberg/.helm.

Tiller (the Helm server-side component) has been installed into your Kubernetes Cluster.

Please note: by default, Tiller is deployed with an insecure 'allow unauthenticated users' policy.
To prevent this, run `helm init` with the --tiller-tls-verify flag.
For more information on securing your installation see: https://docs.helm.sh/using_helm/#securing-your-helm-installation</code></pre><p>Now it's time to install Jenkins and confirm it is online:</p>
<pre><code class="language-bash">helm install --name jenkins stable/jenkins --namespace jenkins
NAME:   jenkins
LAST DEPLOYED: Tue May 28 11:12:39 2019
NAMESPACE: jenkins
STATUS: DEPLOYED

RESOURCES:
==&gt; v1/ConfigMap
NAME           DATA  AGE
jenkins        5     0s
jenkins-tests  1     0s

==&gt; v1/Deployment
NAME     READY  UP-TO-DATE  AVAILABLE  AGE
jenkins  0/1    1           0          0s

==&gt; v1/PersistentVolumeClaim
NAME     STATUS   VOLUME    CAPACITY  ACCESS MODES  STORAGECLASS  AGE
jenkins  Pending  standard  0s

==&gt; v1/Pod(related)
NAME                      READY  STATUS   RESTARTS  AGE
jenkins-7565554b8f-cvhbd  0/1    Pending  0         0s

==&gt; v1/Role
NAME                     AGE
jenkins-schedule-agents  0s

==&gt; v1/RoleBinding
NAME                     AGE
jenkins-schedule-agents  0s

==&gt; v1/Secret
NAME     TYPE    DATA  AGE
jenkins  Opaque  2     0s

==&gt; v1/Service
NAME           TYPE          CLUSTER-IP    EXTERNAL-IP  PORT(S)         AGE
jenkins        LoadBalancer  10.96.90.0    &lt;pending&gt;    8080:32015/TCP  0s
jenkins-agent  ClusterIP     10.103.85.49  &lt;none&gt;       50000/TCP       0s

==&gt; v1/ServiceAccount
NAME     SECRETS  AGE
jenkins  1        0s


NOTES:
1. Get your 'admin' user password by running:
  printf $(kubectl get secret --namespace jenkins jenkins -o jsonpath="{.data.jenkins-admin-password}" | base64 --decode);echo
2. Get the Jenkins URL to visit by running these commands in the same shell:
  NOTE: It may take a few minutes for the LoadBalancer IP to be available.
        You can watch the status of by running 'kubectl get svc --namespace jenkins -w jenkins'
  export SERVICE_IP=$(kubectl get svc --namespace jenkins jenkins --template "{{ range (index .status.loadBalancer.ingress 0) }}{{ . }}{{ end }}")
  echo http://$SERVICE_IP:8080/login

3. Login with the password from step 1 and the username: admin


For more information on running Jenkins on Kubernetes, visit:
https://cloud.google.com/solutions/jenkins-on-container-engine

$ kubectl get pods --namespace jenkins
NAME                       READY     STATUS    RESTARTS   AGE
jenkins-7565554b8f-cvhbd   1/1       Running   0          9m</code></pre><p>You'll notice that the magical command to get the admin password is part of the lengthy output above (in Note 1). This incredibly helpful command:</p>
<ul><li>Executes a <strong>kubectl</strong> command to extract the secret created by Jenkins</li>
<li>Sends that execution, thanks to <strong>printf</strong> and <strong>$()</strong>, as input to the pipe operator</li>
<li>Decodes the password, which is stored in base64 encoding, then outputs it to the screen</li>
</ul><p>I didn't notice that line in the output at first, and I couldn't easily find the answer by searching, so hopefully, it's simpler for everyone now.</p>
<h2 id="one-more-thing...">One more thing…</h2>
<p>There is still a bit of a dead end here. My configuration never assigned a <strong>SERVICE_IP</strong>,<strong> </strong>as explained in the output (Note 2) above. <a href="https://twitter.com/alynderthered1?lang=en" target="_blank">Jessica Repka</a> recommends setting up port forwarding from the pod to the local host so it can be addressed locally. By doing so, I'll be able to access the UI as if it's running locally on my laptop and not within a container within Minikube.</p>
<pre><code class="language-bash"># Be sure to update your jenkins pod name in the following command
$ kubectl port-forward jenkins-7565554b8f-cvhbd 8088:8080</code></pre><p>By leaving that terminal window running, I have a port forwarded to a locally available port–I chose <strong>8088</strong> here–and can get going with Jenkins at <strong><a href="http://127.0.0.1:8088/login">http://127.0.0.1:8088/login</a></strong>.</p>
<p>I hope this how-to will help other people getting started with Jenkins on Kubernetes.</p>
