<p>I have covered multiple chaos engineering tools in this series. The first article in this series explained <a href="https://opensource.com/article/21/5/11-years-kubernetes-and-chaos">what chaos engineering is</a>; the second demonstrated how to get your <a href="https://opensource.com/article/21/5/get-your-steady-state-chaos-grafana-and-prometheus">system's steady state</a> so that you can compare it against a chaos state; the third showed how to <a href="https://opensource.com/article/21/5/total-chaos-litmus">use Litmus to test</a> arbitrary failures and experiments in your Kubernetes cluster; and the fourth article got into <a href="https://opensource.com/article/21/5/get-meshy-chaos-mesh">Chaos Mesh</a>, an open source chaos orchestrator with a web user interface.</p>
<p>In this fifth article, I want to talk about arbitrary pod failure. <a href="https://github.com/asobti/kube-monkey" target="_blank">Kube-monkey</a> offers an easy way to stress-test your systems by scheduling random termination pods in your cluster. This aims to encourage and validate the development of failure-resilient services. As in the previous walkthroughs, I'll use Pop!_OS 20.04, Helm 3, Minikube 1.14.2, and Kubernetes 1.19.</p>
<h2 id="configure-minikube">Configure Minikube</h2>
<p>If you haven't already, <a href="https://minikube.sigs.k8s.io/docs/start/" target="_blank">install Minikube</a> in whatever way makes sense for your environment. If you have enough resources, I recommend giving your virtual machine a bit more than the default memory and CPU power:</p>
<pre><code class="language-bash">$ minikube config set memory 8192
❗  These changes will take effect upon a minikube delete and then a minikube start
$ minikube config set cpus 6
❗  These changes will take effect upon a minikube delete and then a minikube start</code></pre><p>Then start and check the status of your system:</p>
<pre><code class="language-bash">$ minikube start
?  minikube v1.14.2 on Debian bullseye/sid
?  minikube 1.19.0 is available! Download it: https://github.com/kubernetes/minikube/releases/tag/v1.19.0
?  To disable this notice, run: 'minikube config set WantUpdateNotification false'

✨  Using the docker driver based on user configuration
?  Starting control plane node minikube in cluster minikube
?  Creating docker container (CPUs=6, Memory=8192MB) ...
?  Preparing Kubernetes v1.19.0 on Docker 19.03.8 ...
?  Verifying Kubernetes components...
?  Enabled addons: storage-provisioner, default-storageclass
?  Done! kubectl is now configured to use "minikube" by default
$ minikube status
minikube
type: Control Plane
host: Running
kubelet: Running
apiserver: Running
kubeconfig: Configured</code></pre><h2 id="preconfiguring-with-deployments">Preconfiguring with deployments</h2>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on Kubernetes</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/topics/containers/what-is-kubernetes?intcmp=7016000000127cYAAQ">What is Kubernetes?</a></li>
<li><a href="https://www.redhat.com/en/engage/kubernetes-containers-storage-s-201911201051?intcmp=7016000000127cYAAQ">eBook: Storage Patterns for Kubernetes</a></li>
<li><a href="https://www.redhat.com/en/engage/openshift-storage-testdrive-20170718?intcmp=7016000000127cYAAQ">Test drive OpenShift hands-on</a></li>
<li><a href="https://opensource.com/kubernetes-dump-truck?intcmp=7016000000127cYAAQ">eBook: Getting started with Kubernetes</a></li>
<li><a href="https://www.redhat.com/en/resources/managing-containers-kubernetes-openshift-technology-detail?intcmp=7016000000127cYAAQ">An introduction to enterprise Kubernetes</a></li>
<li><a href="https://enterprisersproject.com/article/2017/10/how-explain-kubernetes-plain-english?intcmp=7016000000127cYAAQ">How to explain Kubernetes in plain terms</a></li>
<li><a href="https://opensource.com/downloads/kubernetes-raspberry-pi?intcmp=7016000000127cYAAQ">eBook: Running Kubernetes on your Raspberry Pi homelab</a></li>
<li><a href="https://opensource.com/downloads/kubernetes-cheat-sheet?intcmp=7016000000127cYAAQ">Kubernetes cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/kubernetes-sysadmin?intcmp=7016000000127cYAAQ">eBook: A guide to Kubernetes for SREs and sysadmins</a></li>
<li><a href="https://opensource.com/tags/kubernetes?intcmp=7016000000127cYAAQ">Latest Kubernetes articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>Start by adding some small deployments to run chaos against. These deployments will need some special labels, so you need to create a new Helm chart. The following labels will help kube-monkey determine what to kill if the app is opted-in to doing chaos and understand what details are behind the chaos:</p>
<ul><li><strong>kube-monkey/enabled</strong>: This setting opts you in to starting the chaos.</li>
<li><strong>kube-monkey/mtbf</strong>: This stands for mean time between failure (in days). For example, if it's set to 3, the Kubernetes (K8s) app expects to have a pod killed approximately every third weekday.</li>
<li><strong>kube-monkey/identifier</strong>: This is a unique identifier for the K8s apps; in this example, it will be "nginx."</li>
<li><strong>kube-monkey/kill-mode</strong>: The kube-monkey's default behavior is to kill only one pod in the cluster, but you can change it to add more:
<ul><li><strong>kill-all:</strong> Kill every pod, no matter what is happening with a pod</li>
<li><strong>fixed:</strong> Pick a number of pods you want to kill</li>
<li><strong>fixed-percent:</strong> Kill a fixed percent of pods (e.g., 50%)</li>
</ul></li>
<li><strong>kube-monkey/kill-value</strong>: This is where you can specify a value for kill-mode
<ul><li><strong>fixed:</strong> The number of pods to kill</li>
<li><strong>random-max-percent:</strong> The maximum number from 0–100 that kube-monkey can kill</li>
<li><strong>fixed-percent:</strong> The percentage, from 0–100 percent, of pods to kill</li>
</ul></li>
</ul><p>Now that you have this background info, you can start <a href="https://opensource.com/article/20/5/helm-charts">creating a basic Helm chart</a>.</p>
<p>I named this Helm chart <code>nginx</code>. I'll show only the changes to the Helm chart deployment labels below. You need to change the deployment YAML file, which is <code>nginx/templates</code> in this example:</p>
<pre><code class="language-bash">$ /chaos/kube-monkey/helm/nginx/templates$ ls -la
total 40
drwxr-xr-x 3 jess jess 4096 May 15 14:46 .
drwxr-xr-x 4 jess jess 4096 May 15 14:46 ..
-rw-r--r-- 1 jess jess 1826 May 15 14:46 deployment.yaml
-rw-r--r-- 1 jess jess 1762 May 15 14:46 _helpers.tpl
-rw-r--r-- 1 jess jess  910 May 15 14:46 hpa.yaml
-rw-r--r-- 1 jess jess 1048 May 15 14:46 ingress.yaml
-rw-r--r-- 1 jess jess 1735 May 15 14:46 NOTES.txt
-rw-r--r-- 1 jess jess  316 May 15 14:46 serviceaccount.yaml
-rw-r--r-- 1 jess jess  355 May 15 14:46 service.yaml
drwxr-xr-x 2 jess jess 4096 May 15 14:46 tests</code></pre><p>In your <code>deployment.yaml</code> file, find this section:</p>
<pre><code class="language-yaml">  template:
    metadata:
      {{- with .Values.podAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        {{- include "nginx.selectorLabels" . | nindent 8 }}</code></pre><p>And make these changes:</p>
<pre><code class="language-yaml">  template:
    metadata:
      {{- with .Values.podAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        {{- include "nginx.selectorLabels" . | nindent 8 }}
        kube-monkey/enabled: enabled
        kube-monkey/identifier: monkey-victim
        kube-monkey/mtbf: '2'
        kube-monkey/kill-mode: "fixed"
        kube-monkey/kill-value: '1'</code></pre><p>Move back one directory and find the <code>values</code> file:</p>
<pre><code class="language-bash">$ /chaos/kube-monkey/helm/nginx/templates$ cd ../
$ /chaos/kube-monkey/helm/nginx$ ls
charts  Chart.yaml  templates  values.yaml</code></pre><p>You need to change one line in the values file, from:</p>
<pre><code class="language-yaml">replicaCount: 1</code></pre><p>to:</p>
<pre><code class="language-yaml">replicaCount: 8</code></pre><p>This will give you eight different pods to test chaos against.</p>
<p>Move back one more directory and install the new Helm chart:</p>
<pre><code class="language-bash">$ /chaos/kube-monkey/helm/nginx$ cd ../
$ /chaos/kube-monkey/helm$ helm install nginxtest nginx
NAME: nginxtest
LAST DEPLOYED: Sat May 15 14:53:47 2021
NAMESPACE: default
STATUS: deployed
REVISION: 1
NOTES:
1. Get the application URL by running these commands:
  export POD_NAME=$(kubectl get pods --namespace default -l "app.kubernetes.io/name=nginx,app.kubernetes.io/instance=nginxtest" -o jsonpath="{.items[0].metadata.name}")
  export CONTAINER_PORT=$(kubectl get pod --namespace default $POD_NAME -o jsonpath="{.spec.containers[0].ports[0].containerPort}")
  echo "Visit http://127.0.0.1:8080 to use your application"
  kubectl --namespace default port-forward $POD_NAME 8080:$CONTAINER_PORT</code></pre><p>Then check the labels in your Nginx pods:</p>
<pre><code class="language-bash">$ /chaos/kube-monkey/helm$ kubectl get pods -n default
NAME                                 READY   STATUS    RESTARTS   AGE
nginxtest-8f967857-88zv7             1/1     Running   0          80s
nginxtest-8f967857-8qb95             1/1     Running   0          80s
nginxtest-8f967857-dlng7             1/1     Running   0          80s
nginxtest-8f967857-h7mmc             1/1     Running   0          80s
nginxtest-8f967857-pdzpq             1/1     Running   0          80s
nginxtest-8f967857-rdpnb             1/1     Running   0          80s
nginxtest-8f967857-rqv2w             1/1     Running   0          80s
nginxtest-8f967857-tr2cn             1/1     Running   0          80s</code></pre><p>Chose the first pod to describe and confirm the labels are in place:</p>
<pre><code class="language-bash">$ /chaos/kube-monkey/helm$ kubectl describe pod nginxtest-8f967857-88zv7 -n default
Name:         nginxtest-8f967857-88zv7
Namespace:    default
Priority:     0
Node:         minikube/192.168.49.2
Start Time:   Sat, 15 May 2021 15:11:37 -0400
Labels:       app.kubernetes.io/instance=nginxtest
              app.kubernetes.io/name=nginx
              kube-monkey/enabled=enabled
              kube-monkey/identifier=monkey-victim
              kube-monkey/kill-mode=fixed
              kube-monkey/kill-value=1
              kube-monkey/mtbf=2
              pod-template-hash=8f967857</code></pre><h2 id="configure-and-install-kube-monkey">Configure and install kube-monkey</h2>
<p>To install kube-monkey using Helm, you first need to run <code>git clone on </code>the <a href="https://github.com/asobti/kube-monkey" target="_blank">kube-monkey repository</a>:</p>
<pre><code class="language-bash">$ /chaos$ git clone https://github.com/asobti/kube-monkey
Cloning into 'kube-monkey'...
remote: Enumerating objects: 14641, done.
remote: Counting objects: 100% (47/47), done.
remote: Compressing objects: 100% (36/36), done.
remote: Total 14641 (delta 18), reused 22 (delta 8), pack-reused 14594
Receiving objects: 100% (14641/14641), 30.56 MiB | 39.31 MiB/s, done.
Resolving deltas: 100% (6502/6502), done.</code></pre><p>Change to the <code>kube-monkey/helm</code> directory:</p>
<pre><code class="language-bash">$ /chaos$ cd kube-monkey/helm/
$ /chaos/kube-monkey/helm$</code></pre><p>Then go into the Helm chart and find the <code>values.yaml</code> file:</p>
<pre><code class="language-bash">$ /chaos/kube-monkey/helm$ cd kubemonkey/
$ /chaos/kube-monkey/helm/kubemonkey$ ls
Chart.yaml  README.md  templates  values.yaml</code></pre><p>Below, I will show just the sections of the <code>values.yaml</code> file you need to change. They disable dry-run mode by changing it in the config section to <code>false</code>, then add the default namespace to the whitelist so that it can kill the pods you deployed. You must keep the <code>blacklistedNamespaces</code> value or you will cause severe damage to your system.</p>
<p>Change this:</p>
<pre><code class="language-yaml">config:
  dryRun: true  
  runHour: 8
  startHour: 10
  endHour: 16
  blacklistedNamespaces:
    - kube-system
  whitelistedNamespaces:  []</code></pre><p>To this:</p>
<pre><code class="language-bash">config:
  dryRun: false  
  runHour: 8
  startHour: 10
  endHour: 16
  blacklistedNamespaces:
    - kube-system
  whitelistedNamespaces:  ["default"]</code></pre><p>In the debug section, set <code>enabled</code> and <code>schedule_immediate_kill</code> to <code>true</code>. This will show the pods being killed.</p>
<p>Change this:</p>
<pre><code class="language-yaml">  debug:
   enabled: false 
   schedule_immediate_kill: false</code></pre><p>To this:</p>
<pre><code class="language-yaml">  debug:
   enabled: true 
   schedule_immediate_kill: true</code></pre><p>Run a <code>helm install</code>:</p>
<pre><code class="language-bash">$ /chaos/kube-monkey/helm$ helm install chaos kubemonkey
NAME: chaos
LAST DEPLOYED: Sat May 15 13:51:59 2021
NAMESPACE: default
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
1. Wait until the application is rolled out:
  kubectl -n default rollout status deployment chaos-kube-monkey
2. Check the logs:
  kubectl logs -f deployment.apps/chaos-kube-monkey -n default</code></pre><p>Check the kube-monkey logs and see that the pods are being terminated:</p>
<pre><code class="language-bash"> $ /chaos/kube-monkey/helm$ kubectl logs -f deployment.apps/chaos-kube-monkey -n default

	********** Today's schedule **********
	k8 Api Kind	Kind Name		Termination Time
	-----------	---------		----------------
	v1.Deployment	nginxtest		05/15/2021 15:15:22 -0400 EDT
	********** End of schedule **********
I0515 19:15:22.343202       1 kubemonkey.go:70] Termination successfully executed for v1.Deployment nginxtest
I0515 19:15:22.343216       1 kubemonkey.go:73] Status Update: 0 scheduled terminations left.
I0515 19:15:22.343220       1 kubemonkey.go:76] Status Update: All terminations done.
I0515 19:15:22.343278       1 kubemonkey.go:19] Debug mode detected!
I0515 19:15:22.343283       1 kubemonkey.go:20] Status Update: Generating next schedule in 30 sec</code></pre><p>You can also use <a href="https://opensource.com/article/20/5/kubernetes-administration">K9s</a> and watch the pods die.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Pods dying in K9s"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/podsdying.png" width="675" height="371" alt="Pods dying in K9s" title="Pods dying in K9s" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Jess Cherry, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>Congratulations! You now have a running chaos test with arbitrary failures. Anytime you want, you can change your applications to test at a certain day of the week and time of day.</p>
<h2 id="final-thoughts">Final thoughts</h2>
<p>While kube-monkey is a great chaos engineering tool, it does require heavy configurations. Therefore, it isn't the best starter chaos engineering tool for someone new to Kubernetes. Another drawback is you have to edit your application's Helm chart for chaos testing to run.</p>
<p>This tool would be best positioned in a staging environment to watch how applications respond to arbitrary failure regularly. This gives you a long-term way to keep track of unsteady states using cluster monitoring tools. It also keeps notes that you can use for recovery of your internal applications in production.</p>
