<p><span style="background-color:null;">November's wander-around-the-web has turned up some interesting Ansible stories. It's fascinating to explore Ansible development through data, as Greg Sutcliffe does in his blog linked below. On the YouTubes this month<span style="color:null;">,</span> we found a couple of really interesting talks. Read on to see them for yourself.</span></p>
<p>If you spot an interesting Ansible story on your travels, please send us the link <a href="https://twitter.com/thismarkp" target="_blank">via Mark on Twitter</a>, and the Ansible Community team will curate the best submissions.</p>
<h2 id="articles">Articles</h2>
<p>Data science, orchestration and Ansible configuration. An eclectic bunch huh?</p>
<p><a href="https://twitter.com/gwmngilfen" target="_blank">Greg Sutcliffe</a>, <span style="color:null;">a data scientist</span> on the Ansible Community team, has been taking a look at <a href="https://emeraldreverie.org/2019/11/12/ansible-modules-contribution-structure/" target="_blank">Ansible Modules Contribution Structure</a><span style="color:null;">.</span></p>
<p><span style="color:null;">In </span><a href="https://github.com/thisdougb/AnsibleNginxCertbot" target="_blank">Ansible, Nginx, Certbot, and EasyDNS</a> on DigitalOcean<span style="color:null;">,</span> <a href="https://twitter.com/thisdougb" target="_blank">Doug Bridgens</a> demonstrates Ansible as an orchestrator<span style="color:null;">.</span></p>
<p>Keerthi Chinthaguntla wrote about <a href="https://www.redhat.com/sysadmin/configuring-ansible" target="_blank">Configuring Ansible</a> to keep your installation secure and tidy<span style="color:null;">.</span></p>
<h2 id="videos">Videos</h2>
<p>On <span style="color:null;">the YouTubes </span>this month<span style="color:null;">,</span> we found interesting talks on security and using Ansible to simplify other tools:</p>
<p>How to <a href="https://youtu.be/tK8GZzCiGUg" target="_blank">install Ansible on RHEL8</a></p>
<p><a href="https://youtu.be/gm0H5hOmUys" target="_blank">Automating system compliance</a> using Ansible.</p>
<p>Brian J. Atkisson gave a talk on <a href="https://youtu.be/a4XdH4F4Zic" target="_blank">Pulling the Puppet strings with Ansible</a> at LISA19.</p>
<h2 id="meetups">Meetups</h2>
<p>Atlanta, Bern<span style="color:null;">, </span>Switzerland<span style="color:null;">, and Vienna, Austria,</span> saw like-minded folk gather this past month:</p>
<p><a href="https://www.meetup.com/Ansible-Atlanta/events/264897225/" target="_blank">Atlanta</a> had a post-AnsibleFest panel discussion.</p>
<p><span style="color:null;">Fifty-five </span>people signed up for <a href="https://www.meetup.com/Ansible-Bern/events/264366918/" target="_blank">the second Bern</a> meetup. Talks included IoT, Terraform+Ansible<span style="color:#e74c3c;">,</span> and <a href="https://www.ansible.com/blog/getting-started-with-ansible-collections" target="_blank">Ansible Collections</a>.</p>
<p>At the <a href="https://www.meetup.com/Vienna-Ansible-Meetup/events/265469626/" target="_blank">Vienna meetup</a><span style="color:#e74c3c;">,</span> 31 people signed up to listen to Ansible <span style="color:null;">best pr</span>actices.</p>
<h2 id="happy-person-of-the-month">Happy person of the month</h2>
<blockquote class="twitter-tweet"><p dir="ltr" lang="en" xml:lang="en">Just started using <a href="https://twitter.com/ansible?ref_src=twsrc%5Etfw">@ansible</a>. I can't believe how easy, useful, and powerful it is.</p>
<p>— Albert Cloete (@albert_cloete) <a href="https://twitter.com/albert_cloete/status/1168687330070343681?ref_src=twsrc%5Etfw">September 3, 2019</a></p></blockquote>
<script async="" src="https://platform.twitter.com/widgets.js" charset="utf-8"></script><p>Found something interesting you'd like us to share next month? <a href="https://twitter.com/thismarkp" target="_blank">Drop me a message</a> on Twitter with the link<span style="color:#e74c3c;">,</span> please!</p>
