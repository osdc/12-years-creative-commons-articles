<p>As someone who's been known to string a few words together, I know that a well-crafted outline can be a key part of any writing project. Why? A good outline helps you organize your work. It provides a structure for what you're writing as well as a roadmap from beginning to end.</p>
<p>Outlines aren't just for writing, either. They can be a great tool for organizing just about any kind of project.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Linux resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux commands cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Free online course: RHEL Technical Overview</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux networking cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">SELinux cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux common commands cheat sheet</a></li>
<li><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">What are Linux containers?</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Our latest Linux articles</a></li>
</ul></div>
</div>
</div>
</div>

<p>I've found that the best way to create an outline is with a dedicated piece of software. Most of the outlining software that I've tried on the Linux desktop, though, has been OK. Nothing special, but OK. One outliner I keep coming back to is <a href="http://treeline.bellz.org/" target="_blank">TreeLine</a>.</p>
<h2>A little bit about TreeLine</h2>
<p>According to its developer:</p>
<blockquote><p>Some would call TreeLine an Outliner, others would call it a PIM. Basically, it just stores almost any kind of information. A tree structure makes it easy to keep things organized. And each node in the tree can contain several fields, forming a mini-database.</p>
</blockquote>
<p>You can use TreeLine to (as the tag line for a popular web-based outline tool goes) <em>organize your brain</em>. You can use TreeLine like that if you want, but that's overkill for me.</p>
<h2>Getting and installing TreeLine</h2>
<p>If you're using Linux, you might be able to install TreeLine from your distro's package manager. If not, it's not available from the package manager, or if you're using Windows, you'll need to <a href="http://treeline.bellz.org/download.html" target="_blank">download</a> and <a href="http://treeline.bellz.org/install.html" target="_blank">install</a> TreeLine.</p>
<p>Before you do that, however, check to see if your computer meets all of <a href="http://treeline.bellz.org/require.html" target="_blank">TreeLine's requirements</a>. You might need to install some supporting software and libraries before you do the deed.</p>
<h2>Creating an outline</h2>
<p>When you first start TreeLine, you'll be asked to choose a template for your outline.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Create new file"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/treeline_new_file.png" width="320" height="361" alt="Create new file" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>When creating outlines, I usually choose <strong>Long HTML Text</strong>. That template is simple, is free form, and gives me enough flexibility to change the structure of my outlines if I need to. Click <strong>OK</strong>.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="A blank outline"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/treeline_blank_outline.png" width="700" height="438" alt="A blank outline" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>Notice that pane on the right? It's kind of unsightly and not really useful with an outline. But you don't have to look at it while you're working. Just drag it to the right to hide it.</p>
<p>With each outline or list you create, there are two items (called <em>nodes</em>) named <em>Parent</em> and <em>Child</em>. Those are the default names. Just double click them to change their names to something more useful.</p>
<p>Most outlines have more than two nodes. To add a node, click either the <strong>Insert Sibling After</strong> or <strong>Add Child Node</strong> buttons on the toolbar.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Add node buttons"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/treeline_add_nodes.png" width="67" height="26" alt="Add node buttons" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>A <em>sibling</em> node is one that's on the same level as the node above it. Think of sibling nodes as branches in the tree of your outline. So what are the leaves? Those are the child nodes. When you add a child node, it's indented. If you accidentally add a child node or a sibling node and want to adjust its level in the hierarchy, click the <strong>Indent Node</strong> or <strong>Unindent Node</strong> buttons on the toolbar.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Indent or outdent node buttons"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/treeline_indent-outdent.png" width="55" height="26" alt="Indent or outdent node buttons" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>Add nodes for all the items in your outline. You'll wind up with something like this:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Treeline outline"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/treeline_completed_outline.png" width="700" height="570" alt="Treeline outline" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>Yes, that was the outline for this article. And I stuck to it!</p>
<h2>Exporting your outlines</h2>
<p>You're not always at your computer. Sometimes you'll want to take your outlines with you. But since TreeLine doesn't have a mobile version, you'll have to export your outlines to a more portable format. TreeLine, as you've guessed, lets you do that.</p>
<p>To export an outline, select <strong>File &gt; Export</strong>.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Export menu"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/treeline_export.png" width="517" height="402" alt="Export menu" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>You can export information from TreeLine in a variety of formats. The ones I found most useful, and which retained the integrity of the lists, are <strong>HTML single file output</strong> and <strong>Tabbed title text</strong>. When you click <strong>OK</strong>, you're prompted to give the export file a name and a directory in which to save it. Once the file is exported, you can view it in a web browser:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Exporting to HTML"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/treeline-export-html.png" width="700" height="497" alt="Exporting to HTML" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>Or using a text editor:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Exporting to text"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/treeline_export_text.png" width="700" height="502" alt="Exporting to text" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>The results aren't incredibly pretty, but they give you a good idea of what you're working on.</p>
<h2>Final thought</h2>
<p>I haven't covered all of TreeLine's features in this article, just the ones I regularly use. While I think the interface could do with some sprucing up and some usability tweaks, TreeLine is a very flexible tool for creating outlines on the Linux desktop.</p>
