<p>Like almost everyone on the planet, I have a few social media accounts. I mostly stick to Facebook to stay up to date with friends and family and Twitter to follow a few other people.</p>
<p>Have you ever wanted to make a post that includes italics or some other fancy formatting? You can easily change the text to italics or bold when you're writing an email, but most social media platforms don't provide many formatting options.</p>
<p>And sometimes, I just want to put a little <em>emphasis</em> into what I'm writing. If I've had a really good day and I want to share that with my friends, I might want to put that text in italics. For other posts, I might want to use different formatting that will help my text stand out. Sure, you can use emoji, but sometimes a little text formatting can add that extra pizzazz to your posts.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Programming and development</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ&amp;src=programming_resource_menu4">Red Hat Developers Blog</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Programming cheat sheets</a></li>
<li><a href="https://www.redhat.com/en/services/training/learning-subscription?intcmp=7016000000127cYAAQ">Try for free: Red Hat Learning Subscription</a></li>
<li><a href="https://opensource.com/downloads/bash-programming-guide?intcmp=7016000000127cYAAQ">eBook: An introduction to programming with Bash</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/bash-shell-cheat-sheet?intcmp=7016000000127cYAAQ">Bash Shell Scripting Cheat Sheet</a></li>
<li><a href="https://developers.redhat.com/e-books/modernizing-enterprise-java?intcmp=7016000000127cYAAQ">eBook: Modernizing Enterprise Java</a></li>
</ul></div>
</div>
</div>
</div>
<p>I found a way to do just that. With a short <a href="https://www.gnu.org/software/gawk/" target="_blank">Gawk</a> script I wrote, I can create fancy formatting that I can copy and paste into my social media posts.</p>
<h2 id="special-html">Special HTML</h2>
<p>HTML includes a bunch of special characters for mathematics, symbols, and other languages that most people are not aware of. Within the Mathematical Markup Language (<a href="https://en.wikipedia.org/wiki/MathML" target="_blank">MathML</a>) math character support, HTML includes alternate versions of the alphabet for script, fraktur, and double-strike (shown respectively in this image) characters.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="script, fraktur, and double-strike text"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/hello_world.png" width="490" height="354" alt="script, fraktur, and double-strike text" title="script, fraktur, and double-strike text" /></div>
      
  </article></p>
<p>You can use these alternate versions of the alphabet to create fancy text.</p>
<p>The script alphabet variation is written as the letter followed by <strong>scr</strong>. Characters can be uppercase or lowercase. For example, to print the script letter <strong>a</strong> in an HTML page, type <strong>&amp;ascr;</strong>, and to print the script letter <strong>Z</strong> in HTML, type <strong>&amp;Zscr;</strong>.</p>
<p>The fraktur and double-strike variations are referenced in similar ways. The fraktur mathematical lower-case <strong>a</strong> is <strong>&amp;afr;</strong>, and the capital <strong>Y</strong> is <strong>&amp;Yfr;</strong>. The mathematical double-strike <strong>a</strong> is referenced as <strong>&amp;aopf;</strong>, and the double-strike <strong>X</strong> is <strong>&amp;Xopf;</strong>.</p>
<h2 id="gawk-functions">Gawk functions</h2>
<p>Once you know how to reference the alternate versions of each letter, it's easy to define a few Gawk functions to print those HTML entities. Since these alternate characters exist only for letters and not punctuation or numbers, start with a simple wrapper function to determine if a character is an uppercase or lowercase letter.</p>
<pre><code class="language-text">#!/usr/bin/gawk -f
# Echo the input as different "fonts." Redirect this into an html
# page and copy/paste fancy text into twitter or facebook.

BEGIN { alpha="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"; }

function is_alpha(c) {
  return(index(alpha, c));
}</code></pre><p>The <strong>BEGIN</strong> statement defines an alphabet string called <strong>alpha</strong> that contains all letters a–z and A–Z. The <strong>is_alpha(c)</strong> function uses the built-in <strong>index()</strong> function to return the location of the character <strong>c</strong> in the string <strong>alpha</strong>. If the character <strong>c</strong> is not a letter, <strong>index()</strong> returns zero, which the script uses as a False value.</p>
<p>Because the <strong>is_alpha(c)</strong> function just "wraps" a call to the <strong>index()</strong> function without doing anything else; this is called a <em>wrapper</em> function. Think of it as shorthand that makes the Gawk script easier to read.</p>
<p>With that, it's easy to define a few functions that convert single letters into each of the alternate versions of the alphabet. In general, each function calls <strong>is_alpha(c)</strong> to determine if a character is a letter a–z or A–Z. If it is (i.e., if the returned value is non-zero), then the function prints the HTML entity for that letter as script, fraktur, and double-strike. Otherwise, the function prints the letter.</p>
<pre><code class="language-text">function print_script(c) {
  if ( is_alpha(c) ) { printf("&amp;%cscr;", c); } else { printf("%c", c); }
}
function print_fraktur(c) {
  if ( is_alpha(c) ) { printf("&amp;%cfr;", c); }  else { printf("%c", c); }
}
function print_double(c) {
  if ( is_alpha(c) ) { printf("&amp;%copf;", c); } else { printf("%c", c); }
}</code></pre><p>The <strong>print_script(c)</strong> function prints a single letter in script. Similarly, the <strong>print_fraktur(c)</strong> function prints a letter in fraktur, and the <strong>print_double(c)</strong> function prints a single letter in double-strike.</p>
<p>All that's left is a Gawk loop to convert plain text into each of the alternate alphabet characters. This script loops over each line three times and prints the text in script, fraktur, or double-strike. Each line is wrapped in <strong>&lt;p&gt;</strong> and <strong>&lt;/p&gt;</strong> HTML tags.</p>
<pre><code class="language-text">{ text=$0;
  len=length(text);

  print "&lt;p&gt;";
  for (i=1; i&lt;=len; i++) {
    print_script( substr(text, i, 1) );
  }
  print "&lt;/p&gt;&lt;p&gt;";
  for (i=1; i&lt;=len; i++) {
    print_fraktur( substr(text, i, 1) );
  }
  print "&lt;/p&gt;&lt;p&gt;";
  for (i=1; i&lt;=len; i++) {
    print_double( substr(text, i, 1) );
  }
  print "&lt;/p&gt;";
}</code></pre><h2 id="putting-it-all-together">Putting it all together</h2>
<p>I saved the above lines in a script file called <strong>htmlecho</strong> and put it in my <strong>~/bin</strong> directory.</p>
<pre><code class="language-text">$ htmlecho &gt; /tmp/hello.html
Hello world
^Z</code></pre><p>Whenever I want to add fancy text to my Facebook and Twitter posts, I just run the script and save the output to a temporary HTML page. I open the temporary page in a web browser and copy and paste the fancy text I like best into my social media posts.</p>
