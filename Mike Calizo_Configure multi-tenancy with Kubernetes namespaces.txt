<p>Most enterprises want a multi-tenancy platform to run their cloud-native applications because it helps manage resources, costs, and operational efficiency and control <a href="https://devops.com/the-cloud-is-booming-but-so-is-cloud-waste/" target="_blank">cloud waste</a>.</p>
<p><a href="https://opensource.com/resources/what-is-kubernetes">Kubernetes</a> is the leading open source platform for managing containerized workloads and services. It gained this reputation because of its flexibility in allowing operators and developers to establish automation with declarative configuration. But there is a catch: Because Kubernetes grows rapidly, the old problem of velocity becomes an issue. The bigger your adoption, the more issues and resource waste you discover.</p>
<h2 id="an-example-of-scale">An example of scale</h2>
<p>Imagine your company started small with its Kubernetes adoption by deploying a variety of internal applications. It has multiple project streams running with multiple developers dedicated to each project stream.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on Kubernetes</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/topics/containers/what-is-kubernetes?intcmp=7016000000127cYAAQ">What is Kubernetes?</a></li>
<li><a href="https://www.redhat.com/en/engage/kubernetes-containers-storage-s-201911201051?intcmp=7016000000127cYAAQ">eBook: Storage Patterns for Kubernetes</a></li>
<li><a href="https://www.redhat.com/en/engage/openshift-storage-testdrive-20170718?intcmp=7016000000127cYAAQ">Test drive OpenShift hands-on</a></li>
<li><a href="https://opensource.com/kubernetes-dump-truck?intcmp=7016000000127cYAAQ">eBook: Getting started with Kubernetes</a></li>
<li><a href="https://www.redhat.com/en/resources/managing-containers-kubernetes-openshift-technology-detail?intcmp=7016000000127cYAAQ">An introduction to enterprise Kubernetes</a></li>
<li><a href="https://enterprisersproject.com/article/2017/10/how-explain-kubernetes-plain-english?intcmp=7016000000127cYAAQ">How to explain Kubernetes in plain terms</a></li>
<li><a href="https://opensource.com/downloads/kubernetes-raspberry-pi?intcmp=7016000000127cYAAQ">eBook: Running Kubernetes on your Raspberry Pi homelab</a></li>
<li><a href="https://opensource.com/downloads/kubernetes-cheat-sheet?intcmp=7016000000127cYAAQ">Kubernetes cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/kubernetes-sysadmin?intcmp=7016000000127cYAAQ">eBook: A guide to Kubernetes for SREs and sysadmins</a></li>
<li><a href="https://opensource.com/tags/kubernetes?intcmp=7016000000127cYAAQ">Latest Kubernetes articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>In a scenario like this, you need to make sure your cluster administrator has full control over the cluster to manage its resources and implement cluster policy and security standards. In a way, the admin is herding the cluster's users to use best practices. A namespace is very useful in this instance because it enables different teams to share a single cluster where computing resources are subdivided into multiple teams.</p>
<p>While namespaces are your first step to Kubernetes multi-tenancy, they are not good enough on their own. There are a number of Kubernetes primitives you need to consider so that you can administer your cluster properly and put it into a production-ready implementation.</p>
<p>The Kubernetes primitives for multi-tenancy are:</p>
<ol><li><strong>RBAC:</strong> Role-based access control for Kubernetes</li>
<li><strong>Network policies:</strong> To isolate traffic between namespaces</li>
<li><strong>Resource quotas:</strong> To control fair access to cluster resources</li>
</ol><p>This article explores how to use Kubernetes namespaces and some basic RBAC configurations to partition a single Kubernetes cluster and take advantage of this built-in Kubernetes tooling.</p>
<h2 id="what-is-a-kubernetes-namespace">What is a Kubernetes namespace?</h2>
<p>Before digging into how to use namespaces to prepare your Kubernetes cluster to become multi-tenant-ready, you need to know what namespaces are.</p>
<p>A <a href="https://kubernetes.io/docs/concepts/overview/working-with-objects/namespaces/" target="_blank">namespace</a> is a Kubernetes object that partitions a Kubernetes cluster into multiple virtual clusters. This is done with the aid of <a href="https://kubernetes.io/docs/concepts/overview/working-with-objects/names/" target="_blank">Kubernetes names and IDs</a>. Namespaces use the Kubernetes name object, which means that each object inside a namespace gets a unique name and ID across the cluster to allow virtual partitioning.</p>
<h2 id="how-namespaces-help-in-multi-tenancy">How namespaces help in multi-tenancy</h2>
<p>Namespaces are one of the Kubernetes primitives you can use to partition your cluster into multiple virtual clusters to allow multi-tenancy. Each namespace is isolated from every other user's, team's, or application's namespace. This isolation is essential in multi-tenancy so that updates and changes in applications, users, and teams are contained within the specific namespace. (Note that namespace does not provide network segmentation.)</p>
<p>Before moving ahead, verify the default namespace in a working Kubernetes cluster:</p>
<pre><code class="language-bash">[root@master ~]# kubectl get namespace
NAME              STATUS   AGE
default           Active   3d
kube-node-lease   Active   3d
kube-public       Active   3d
kube-system       Active   3d</code></pre><p>Then create your first namespace, called <strong>test</strong>:</p>
<pre><code class="language-bash">[root@master ~]# kubectl create namespace test
namespace/test created</code></pre><p>Verify the newly created namespace:</p>
<pre><code class="language-bash">[root@master ~]# kubectl get namespace
NAME              STATUS   AGE
default           Active   3d
kube-node-lease   Active   3d
kube-public       Active   3d
kube-system       Active   3d
test              Active   10s
[root@master ~]# </code></pre><p>Describe the newly created namespace:</p>
<pre><code class="language-bash">[root@master ~]# kubectl describe namespace test
Name:         test
Labels:       &lt;none&gt;
Annotations:  &lt;none&gt;
Status:       Active
No resource quota.
No LimitRange resource.</code></pre><p>To delete a namespace:</p>
<pre><code class="language-bash">[root@master ~]# kubectl delete namespace test
namespace "test" deleted</code></pre><p>Your new namespace is active, but it doesn't have any labels, annotations, or quota-limit ranges defined. However, now that you know how to create and describe and delete a namespace, I'll show how you can use a namespace to virtually partition a Kubernetes cluster.</p>
<h2 id="partitioning-clusters-using-namespace-and-rbac">Partitioning clusters using namespace and RBAC</h2>
<p>Deploy the following simple application to learn how to partition a cluster using namespace and isolate an application and its related objects from "other" users.</p>
<p>First, verify the namespace you will use. For simplicity, use the <strong>test</strong> namespace you created above:</p>
<pre><code class="language-bash">[root@master ~]# kubectl get namespaces
NAME              STATUS   AGE
default           Active   3d
kube-node-lease   Active   3d
kube-public       Active   3d
kube-system       Active   3d
test              Active   3h</code></pre><p>Then deploy a simple application called <strong>test-app</strong> inside the test namespace by using the following configuration:</p>
<pre><code class="language-yaml">apiVersion: v1
kind: Pod
metadata:
  name: test-app                 ⇒ name of the application
  namespace: test                ⇒ the namespace where the app runs
  labels:
     app: test-app			⇒ labels for the app
spec:
  containers:
  - name: test-app
    image: nginx:1.14.2         ⇒ the image we used for the app. 
    ports:
    - containerPort: 80</code></pre><p>Deploy it:</p>
<pre><code class="language-bash">$ kubectl create -f test-app.yaml
    pod/test-app created</code></pre><p>Then verify the application pod was created:</p>
<pre><code class="language-bash">$ kubectl get pods -n test
  NAME       READY   STATUS    RESTARTS   AGE
  test-app   1/1     Running   0          18s</code></pre><p>Now that the running application is inside the <strong>test</strong> namespace, test a use case where:</p>
<ul><li><strong>auth-user</strong> can edit and view all the objects inside the test namespace</li>
<li><strong>un-auth-user</strong> can only view the namespace</li>
</ul><p>I pre-created the users for you to test. If you want to know how I created the users inside Kubernetes, view the commands <a href="https://www.adaltas.com/en/2019/08/07/users-rbac-kubernetes/" target="_blank">here</a>.</p>
<pre><code class="language-bash">$ kubectl config view -o jsonpath='{.users[*].name}' 
  auth-user 
  kubernetes-admin
  un-auth-user</code></pre><p>With this set up, create a Kubernetes <a href="https://kubernetes.io/docs/reference/access-authn-authz/rbac/" target="_blank">Role and RoleBindings</a> to isolate the target namespace <strong>test</strong> to allow <strong>auth-user</strong> to view and edit objects inside the namespace and not allow <strong>un-auth-user</strong> to access or view the objects inside the <strong>test</strong> namespace.</p>
<p>Start by creating a ClusterRole and a Role. These objects are a list of verbs (action) permitted on specific resources and namespaces.</p>
<p>Create a ClusterRole:</p>
<pre><code class="language-yaml">$ cat clusterrole.yaml 
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRole
metadata:
  name: list-deployments
  namespace: test
rules:
  - apiGroups: [ apps ]
    resources: [ deployments ]
    verbs: [ get, list ]</code></pre><p>Create a Role:</p>
<pre><code class="language-yaml">$ cat role.yaml 
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: Role
metadata:
  name: list-deployments
  namespace: test
rules:
  - apiGroups: [ apps ]
    resources: [ deployments ]
    verbs: [ get, list ]</code></pre><p>Apply the Role:</p>
<pre><code class="language-bash">$ kubectl create -f role.yaml 
roles.rbac.authorization.k8s.io "list-deployments" created</code></pre><p>Use the same command to create a ClusterRole:</p>
<pre><code class="language-bash">$ kubectl create -f clusterrole.yaml

$ kubectl get role -n test
  NAME               CREATED AT
  list-deployments   2021-01-18T00:54:00Z</code></pre><p>Verify the Roles:</p>
<pre><code class="language-bash">$ kubectl describe roles -n test
  Name:         list-deployments
  Labels:       &lt;none&gt;
  Annotations:  &lt;none&gt;
  PolicyRule:
    Resources         Non-Resource URLs  Resource Names  Verbs
    ---------         -----------------  --------------  -----
    deployments.apps  []                 []              [get list]</code></pre><p>Remember that you must create RoleBindings by namespace, not by user. This means you need to create two role bindings for user <strong>auth-user</strong>.</p>
<p>Here are the sample RoleBinding YAML files to permit <strong>auth-user</strong> to edit and view.</p>
<p><strong>To edit:</strong></p>
<pre><code class="language-yaml">$ cat rolebinding-auth-edit.yaml 
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: auth-user-edit
  namespace: test
subjects:
- kind: User
  name: auth-user
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: ClusterRole
  name: edit
  apiGroup: rbac.authorization.k8s.io</code></pre><p><strong>To view:</strong></p>
<pre><code class="language-yaml">$ cat rolebinding-auth-view.yaml 
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: auth-user-view
  namespace: test
subjects:
- kind: User
  name: auth-user
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: ClusterRole
  name: view
  apiGroup: rbac.authorization.k8s.io</code></pre><p>Create these YAML files:</p>
<pre><code class="language-bash">$ kubectl create rolebinding-auth-view.yaml 
$ kubectl create rolebinding-auth-edit.yaml</code></pre><p>Verify if the RoleBindings were successfully created:</p>
<pre><code class="language-bash">$ kubectl get rolebindings -n test
NAME             ROLE               AGE
auth-user-edit   ClusterRole/edit   48m
auth-user-view   ClusterRole/view   47m</code></pre><p>With the requirements set up, test the cluster partitioning:</p>
<pre><code class="language-bash">[root@master]$ sudo su un-auth-user
[un-auth-user@master ~]$ kubect get pods -n test
[un-auth-user@master ~]$ kubectl get pods -n test
Error from server (Forbidden): pods is forbidden: User "un-auth-user" cannot list resource "pods" in API group "" in the namespace "test"</code></pre><p>Log in as <strong>auth-user</strong>:</p>
<pre><code class="language-bash">[root@master ]# sudo su auth-user
[auth-user@master auth-user]$ kubectl get pods -n test
NAME       READY   STATUS    RESTARTS   AGE
test-app   1/1     Running   0          3h8m
[auth-user@master un-auth-user]$

[auth-user@master auth-user]$ kubectl edit pods/test-app -n test
Edit cancelled, no changes made.</code></pre><p>You can view and edit the objects inside the <strong>test</strong> namespace. How about viewing the cluster nodes?</p>
<pre><code class="language-bash">[auth-user@master auth-user]$ kubectl get nodes 
Error from server (Forbidden): nodes is forbidden: User "auth-user" cannot list resource "nodes" in API group "" at the cluster scope
[auth-user@master auth-user]$ </code></pre><p>You can't because the role bindings for user <strong>auth-user</strong> dictate they have access to view or edit objects only inside the <strong>test</strong> namespace.</p>
<h2 id="conclusion">Enable access control with namespaces</h2>
<p>Namespaces provide basic building blocks of access control using RBAC and isolation for applications, users, or groups of users. But using namespaces alone as your multi-tenancy solution is not enough in an enterprise implementation. It is recommended that you use other Kubernetes multi-tenancy primitives to attain further isolation and implement proper security.</p>
<p>Namespaces can provide some basic isolation in your Kubernetes cluster; therefore, it is important to consider them upfront, especially when planning a multi-tenant cluster. Namespaces also allow you to logically segregate and assign resources to individual users, teams, or applications.</p>
<p>By using namespaces, you can increase resource efficiencies by enabling a single cluster to be used for a diverse set of workloads.</p>
