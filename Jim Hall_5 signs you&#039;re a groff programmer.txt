<p>I first discovered Unix systems in the early 1990s, when I was an undergraduate at university. I liked it so much that I replaced the MS-DOS system on my home computer with the Linux operating system.</p>
<p>One thing that Linux didn't have in the early to mid-1990s was a word processor. A standard office application on other desktop operating systems, a word processor lets you edit text easily. I often used a word processor on DOS to write my papers for class. I wouldn't find a Linux-native word processor until the late 1990s. Until then, word processing was one of the rare reasons I maintained dual-boot on my first computer, so I could occasionally boot back into DOS to write papers.</p>
<p>Then I discovered that Linux provided kind of a word processor. GNU troff, better known as <a href="https://en.wikipedia.org/wiki/Groff_(software)" target="_blank">groff</a>, is a modern implementation of a classic text processing system called troff, short for "typesetter roff," which is an improved version of the nroff system. And nroff was meant to be a new implementation of the original roff (which stood for "run off," as in to "run off" a document).</p>
<p>With text processing, you edit text in a plain text editor, and you add formatting through macros or other processing commands. You then process that text file through a text-processing system such as groff to generate formatted output suitable for a printer. Another well-known text processing system is LaTeX, but groff was simple enough for my needs.</p>
<p>With a little practice, I found I could write my class papers just as easily in groff as I could using a word processor on Linux. While I don't use groff to write documents today, I still remember the macros and commands to generate printed documents with it. And if you're the same and you learned how to write with groff all those years ago, you probably recognize these five signs that you're a groff writer.</p>
<h2 id="you-have-a-favorite-macro-set">1. You have a favorite macro set</h2>
<p>You format a document in groff by writing plain text interspersed with macros. A macro in groff is a short command that starts with a single period at the beginning of a line. For example: if you want to insert a few lines into your output, the <code>.sp 2</code> macro command adds two blank lines. groff supports other basic macros for all kinds of formatting.</p>
<p>To make formatting a document easier for the writer, groff also provides different <em>macro sets</em>, collections of macros that let you format documents your own way. The first macro set I learned was the <code>-me</code> macro set. Really, the macro set is called the <code>e</code> macro set, and you specify the <code>e</code> macro set when you process a file using the <code>-me</code> option.</p>
<p>groff includes other macro sets, too. For example, the <code>-man</code> macro set used to be the standard macro set to format the built-in <em>manual</em> pages on Unix systems, and the <code>-ms</code> macro set is often used to format certain other technical documents. If you learned to write with groff, you probably have a favorite macro set.</p>
<h2 id="you-want-to-focus-on-your-content-not-the-formatting">2. You want to focus on your content, not the formatting</h2>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Great Content</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=7016000000127cYAAQ">Free online course: RHEL technical overview</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=7016000000127cYAAQ">Learn Advanced Linux Commands</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Download Cheat Sheets</a></li>
<li><a href="https://opensource.com/alternatives?intcmp=7016000000127cYAAQ">Find an Open Source Alternative</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=7016000000127cYAAQ">Read Top Linux Content</a></li>
<li><a href="https://opensource.com/resources?intcmp=7016000000127cYAAQ">Check out open source resources</a></li>
</ul></div>
</div>
</div>
</div>
<p>One great feature of writing with groff is that you can focus on your <em>content</em> and not worry too much about what it looks like. That is a handy feature for technical writers. groff is a great "distraction-free" environment for professional writers. At least, as long as you don't mind delivering your output in any of the formats that groff supports with the <code>-T</code> command-line option, including PDF, PostScript, HTML, and plain text. You can't generate a LibreOffice ODT file or Word DOC file directly from groff.</p>
<p>Once you get comfortable writing in groff, the macros start to <em>disappear</em>. The formatting macros become part of the background, and you focus purely on the text in front of you. I've done enough writing in groff that I don't even see the macros anymore. Maybe it's like writing programming code, and your mind just switches gears, so you think like a computer and see the code as a set of instructions. For me, writing in groff is like that; I just see my text, and my mind interprets the macros automatically into formatting.</p>
<h2 id="you-like-the-old-school-feel">3. You like the old-school feel</h2>
<p>Sure, it might be <em>easier</em> to write your documents with a more typical word processor like LibreOffice Writer or even Google Docs or Microsoft Word. And for certain kinds of documents, a desktop word processor is the right fit. But if you want the "old-school" feel, it's hard to beat writing in groff.</p>
<p>I'll admit that I do most of my writing with LibreOffice Writer, which does an outstanding job. But when I get that itch to do it "old-school," I'll open an editor and write my document using groff.</p>
<h2 id="you-like-that-you-can-use-it-anywhere">4. You like that you can use it anywhere</h2>
<p>groff (and its cousins) are a standard package on almost any Unix system. And with groff, the macros don't change. For example, the <code>-me</code> macros should be the same from system to system. So once you've learned to use the macros on one system, you can use them on the next system.</p>
<p>And because groff documents are just plain text, you can use any editor you like to edit your documents for groff. I like to use GNU Emacs to edit my groff documents, but you can use GNOME Gedit, Vim, or your <a href="https://opensource.com/article/21/2/open-source-text-editors">favorite text editor</a>. Most editors include some kind of "mode" that will highlight the groff macros in a different color from the rest of your text to help you spot errors before processing the file.</p>
<h2 id="you-wrote-this-article-in--me">5. You wrote this article in -me</h2>
<p>When I decided to write this article, I thought the best way would be to use groff directly. I wanted to demonstrate how flexible groff was in preparing documents. So even though you're reading this on a website, the article was originally written using groff.</p>
<p>I hope this has interested you in learning how to use groff to write documents. If you'd like to use more advanced functions in the <code>-me</code> macro set, refer to Eric Allman's <em>Writing papers with groff using -me</em>, which you should find on your system as <strong>meintro.me</strong> in groff's documentation. It's a great reference document that explains other ways to format papers using the <code>-me</code> macros.</p>
<p>I've also included a copy of the original draft of my article that uses the <code>-me</code> macros. Save the file to your system as <strong>five-signs-groff.me</strong>, and run it through groff to view it. The <code>-T</code> option sets the output type, such as <code>-Tps</code> to generate PostScript output or <code>-Thtml</code> to create an HTML file. For example:</p>
<p>groff -me -Thtml five-signs-groff.me &gt; five-signs-groff.html</p>
