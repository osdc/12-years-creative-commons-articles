<p>The beauty of building extra-large Linux clusters is it's easy. Hadoop, OpenStack, hypervisor, and high-performance computing (HPC) installers enable you to build on commodity hardware and deal with node failure reasonably simply. Learning and managing Linux administration on a small scale involves basic day-to-day tasks; however, when planning and scaling production to several thousand node clusters, it can take over your life, including your weekends and holidays.</p>
<p>Specific requirements for encrypting people-related data in transit and at rest have been heavily discussed elsewhere, so I won't be covering them here. Rather, we'll focus on preparations to keep an audit off the backs of your Linux admin team. </p>
<h2>1. Fundamentals: Connecting your cluster to the world</h2>
<p>It's tempting to build a cluster on a standalone network with admin access on a second corporate LAN interface. Like Oracle databases in the past, Hadoop and HPC clusters tend to execute all running tasks in a cluster with a single user identification (UID) account (e.g., "hadoop").</p>
<p>Audit needs to prove not only how personal data is stored, but also how data is manipulated, aggregated, or anonymized, and that includes who can create, change, or log in these application-specific accounts. That's you and your admin team in the spotlight.</p>
<h2>2. Don't let software installers create accounts or Linux groups</h2>
<p>Use your favorite configuration manager or identity manager to create needed accounts on each cluster node (or directory) first. If the Hadoop account and group already exist, the cluster software installer will use those instead. There are several reasons we want this behavior, as outlined below in the next three steps.</p>
<h2>3. Maintain UID/GID consistency <em>everywhere</em></h2>
<p>For traceability later, ensure your organization has a consistent UID/group identification (GID) strategy—a way individuals and groups can be identified within the system. For your cluster's software, the unique application UIDs and GIDs need to fit into that matrix across the organization's infrastructure, not just in your cluster. </p>
<h2>4. Sudo, not Sudon't</h2>
<p>If you are manually distributing sudoers files into your cluster or managing a site-specific scripting environment, it will be up to you and your team to prove you know exactly the state the sudoers file on cluster node 47 was in at a moment several weeks in the past. That is a headache we can all do without.</p>
<p>For self-protection, your team needs to have a strategy to make this centrally managed and under version control. This can be achieved by using a tool like Ansible during node OS setup or versioning machine images for auto-deployment.</p>
<h2>5. Attach the cluster to your organization's SIEM</h2>
<p>Clusters can generate a large wave of log files. For example, the Hortonworks distribution of Hadoop generates hundreds or thousands of "su hadoop" messages in a few minutes. Security information and event management (SIEM) platforms (open source or not) are a fantastic way to make sense of correlated events. SIEM systems provide quicker identification, analysis, and recovery of security events. For example:</p>
<ul><li>David logged into the corporate network from home via a VPN using multifactor authentication (MFA)</li>
<li>David SSHed into the production jumpstart server</li>
<li>David SSHed into cluster node 47, then SUed to root</li>
<li>David changed the UID of the Hadoop account from 10011 to 13011</li>
<li>The cluster ran 138 SU jobs as the Hadoop account on node 47 until 18:00</li>
</ul><p>An operating system, application, or cluster manager's log viewers may show you only slices of this picture. Sending everything to a SIEM is safer, more complete, and frankly becomes another team's responsibility to create reports. Auditors actually prefer the hands-off model, where someone separate from your Linux admin team is proving what happened.  </p>
<h2>6. Get the right training and tools</h2>
<p>A big sign that your team is overwhelmed is if a team member is taking more than four days per audit cycle to help auditors. Something is broken and/or not obvious. Your ideal is two days maximum (and one day if possible). Obtaining sufficient training is critical for productivity.</p>
<p>For example, if your cluster processes people data, get some operations-focused training on each compliance regime where your cluster's operation will run. Make sure there is an exam—achieving certification for a specific version of the requirements is not just good for your resume; it makes an audit review of your team a quick checkbox.</p>
<p>Finally, know when to ask your boss to open the wallet for a commercial tool when pure open source won't pass audit. If you process people data, all the tools need maintenance contracts. There are good reasons open source vendors have commercial distributions and why companies pay for maintenance. First, ask your boss if she or he would like to go on vacation around audit time. I think we all know that answer.</p>
<h2>7. Keep the auditors' needs in mind</h2>
<p>Operational time-saving techniques are great for optimizing efficiency, but it's important to maintain records in case of an audit.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on Ansible</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/downloads/ansible-quickstart?intcmp=701f2000000h4RcAAI">A quickstart guide to Ansible</a></li>
<li><a href="https://opensource.com/downloads/ansible-cheat-sheet?intcmp=701f2000000h4RcAAI">Ansible cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/do007-ansible-essentials-simplicity-automation-technical-overview?intcmp=701f2000000h4RcAAI">Free online course: Ansible essentials</a></li>
<li><a href="https://docs.ansible.com/ansible/latest/intro_installation.html?intcmp=701f2000000h4RcAAI">Download and install Ansible</a></li>
<li><a href="https://go.redhat.com/automated-enterprise-ebook-20180920?intcmp=701f2000000h4RcAAI">eBook: The automated enterprise</a></li>
<li><a href="https://www.ansible.com/resources/ebooks/ansible-for-devops?intcmp=701f2000000h4RcAAI">eBook: Ansible for DevOps</a></li>
<li><a href="https://www.ansible.com/resources/ebooks?intcmp=701f2000000h4RcAAI">Free Ansible eBooks</a></li>
<li><a href="https://opensource.com/tags/ansible?intcmp=701f2000000h4RcAAI">Latest Ansible articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>Specifically for audit trails, delegate reporting tools running against your company's SIEM to another team and have them build reports. Most open source and commercial SIEM systems have interactive reporting capabilities, and there are robust third-party reporting tool vendors, often specializing in specific market sectors. Providing meaningful daily/weekly/monthly operational information from the SIEM to your own data and system holders is an excellent side effect. Again, it should not be your admin team doing the grunt work. Also, have your team incorporate configuration management products, such as Puppet or <a href="https://opensource.com/tags/ansible">Ansible</a>. </p>
<p>What tips do you have for avoiding auditors' wrath? Please share your ideas in the comments.</p>
