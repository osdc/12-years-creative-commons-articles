<p>This week we look at TensorFlow and image classification, mining cryptocurrencies with the Raspberry Pi, developing your own game with Python, and more.</p>
<p></p><center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/5iqBKTDWzFw" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen=""></iframe><p></p></center>
<h3>5. <a href="https://opensource.com/article/17/12/how-to-tensorboard">Getting started with a TensorFlow surgery classifier with TensorBoard data viz</a></h3>
<p>In part two of his series, Adam Monsen trains TensorFlow with a new dataset and introduces the TensorBoard suite of data visualization tools to make it easier to understand, debug, and optimize TensorFlow code.</p>
<h3>4. <a href="https://opensource.com/article/17/12/raspberry-pi-cryptocurrency-mining">Mining cryptocurrency with Raspberry Pi and Storj</a></h3>
<p>Are you looking for a way to monetize your Raspberry Pi? This article is for you. David Egts of Red Hat is always looking for ways to map hot technologies to fun, educational classroom use. You will be fascinated with his ideas for mining cryptocurrencies.</p>
<h3>3. <a href="https://opensource.com/article/17/12/game-framework-python">Build a game framework with Python using the Pygame module</a></h3>
<p>Are you interested in game development with Python? The first part of this series explored Python by creating a simple dice game. Now, it's time to make your own game from scratch. Seth Kenlon of Red Hat will have you developing your own game after you read this excellent article. </p>
<h3>2. <a href="https://opensource.com/life/17/12/home-automation-tools">6 open source home automation tools</a></h3>
<p>The Internet of Things isn't just a buzzword, it's a rapidly expanding reality. Editor Jason Baker explores six new open source home automation technologies. Are you thinking about using an open source home automation system? Or perhaps you already have one in place. What advice would you have to a newcomer to home automation, and what system or systems would you recommend?</p>
<h3>1. <a href="https://opensource.com/article/17/12/editing-text-linux-desktop-notepadqq">Getting started with the Notepadqq Linux text editor</a></h3>
<p>Community moderator Scott Nesbitt uses open source software even when using Microsoft Windows. Long an admirer of Notepad++, Scott was looking for its equivalent on Linux. Notepadqq is the application he was hoping for. His excellent review might give you a reason to try Notepadqq yourself.</p>
<h3>Honorable mention: <a href="https://opensource.com/article/17/12/holiday-oreilly-safari-giveaway">Win a one-year subscription to O'Reilly Safari</a></h3>
<p>Enter by this Sunday at 11:59 p.m. ET for a chance to win a year off access to thousands of technology and business eBooks, videos, live online trainings, and real-time support from experts.</p>
