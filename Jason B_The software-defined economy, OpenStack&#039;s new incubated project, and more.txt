<p>Interested in keeping track of what's happening in the open source cloud? Opensource.com is your source for what's happening right now in <a href="https://opensource.com/resources/what-is-openstack" target="_blank" title="What is OpenStack?">OpenStack</a>, the open source cloud infrastructure project.</p>
<p><!--break--></p>
<h3>OpenStack around the web</h3>
<p>There's a lot of interesting stuff being written about OpenStack. Here's a sampling:</p>
<ul><li><a href="http://superuser.openstack.org/awards" target="_blank" title="OpenStack Superuser" style="line-height: 1.538em; background-color: transparent;">Announcing the Superuser Awards</a>: Do you know an OpenStack Superuser? Nominate them today!</li>
<li><a href="http://www.mysqlperformanceblog.com/2014/08/28/openstack-trove-day-2014-recap-mysql-and-dbaas/">OpenStack Trove Day 2014 recap</a>: Database as a Service takes the day in Cambridge, MA.</li>
<li><a href="http://netapp.github.io/openstack/2014/08/26/manila-incubated/" target="_blank" title="OpenStack news">Manila is an OpenStack incubated project</a>: Welcome shared file systems to OpenStack.</li>
<li><a href="http://www.openstack.org/blog/2014/08/openstack-taking-its-place-in-the-software-defined-economy/">OpenStack taking its place in the software-defined economy</a>: Jonathan Bryce reflects on how his summit keynote theme has caught on.</li>
<li><a href="https://swiftstack.com/blog/2014/08/27/ops-meetup-refections/">OpenStack Ops Meetup reflections</a>: What developers learn from meeting with real-world users.</li>
</ul><p>... and just for fun ...</p>
<ul><li><a href="http://openstackreactions.enovance.com/2014/08/setting-a-1-on-your-own-review/" target="_blank" title="OpenStack Reactions">OpenStack reactions</a>: Setting a -1 on your own review.</li>
</ul><h3>OpenStack discussions</h3>
<p>Here's a sample of some of the most active discussions this week on the OpenStack developers' listserv. For a more in-depth look, why not <a href="http://lists.openstack.org/pipermail/openstack-dev/" target="_blank" title="OpenStack listserv">dive in</a> yourself?</p>
<ul><li><a href="http://lists.openstack.org/pipermail/openstack-dev/2014-August/044347.html" target="_blank" title="OpenStack discussion">Design sessions for Neutron LBaaS</a>: What do we want and need?</li>
<li><a href="http://lists.openstack.org/pipermail/openstack-dev/2014-August/044289.html" target="_blank" title="OpenStack discussion">Is the BP approval process broken</a>: How can the blueprint approval process be more effective?</li>
<li><a href="http://lists.openstack.org/pipermail/openstack-dev/2014-August/044301.html" target="_blank" title="OpenStack discussion">Specs for K release</a>: Juno is nearing release; let's start planning for Kilo.</li>
<li><a href="http://lists.openstack.org/pipermail/openstack-dev/2014-August/044169.html" target="_blank" title="OpenStack news">Desigin Summit reloaded</a>: Making changes to increase productivity.</li>
</ul><h3>OpenStack events</h3>
<p>Here's what is happening this week. Something missing? <a href="mailto:osdc-admin@redhat.com">Email</a> us and let us know, or add it to our <a href="https://opensource.com/resources/conferences-and-events-monthly" target="_blank" title="Events Calendar">events calendar</a>.</p>
<ul><li><a href="https://wiki.openstack.org/wiki/Meetings/Community" target="_blank" title="OpenStack Event">Weekly community office hours</a>: September 2, 2014; #openstack-community channel on Freenode</li>
<li><a href="https://etherpad.openstack.org/p/rdo_community_manager_sync" target="_blank" title="OpenStack Event">RDO weekly community meeting</a>: September 2, 2014; #RDO channel on Freenode</li>
<li><a href="http://www.meetup.com/Openstack-Amsterdam/events/202482492/" target="_blank" title="OpenStack events">OpenStack 101</a>: September 3, 2014; Amsterdam</li>
<li><a href="http://www.meetup.com/IGTCloud/events/200115582/" target="_blank" title="OpenStack event">Securing your IaaS infrastructure</a>: September 4, 2014; Tel Aviv </li>
<li><a href="http://www.meetup.com/openstack/events/150932602/" target="_blank" title="OpenStack event">South Bay OpenStack Meetup</a>: September 4, 2014; San Francisco</li>
<li><a href="https://plus.google.com/events/c9u4sjn7ksb8jrmma7vd25aok94" title="OpenStack event">Deploying things with Heat</a>: September 5, 2014: Google+ Hangout</li>
<li><a href="https://wiki.openstack.org/wiki/Meetings/Community" title="OpenStack Event" style="line-height: 1.538em; background-color: transparent;">Weekly community office hours</a>: September 5, 2014; #openstack-community channel on Freenode</li>
<li><a href="http://www.meetup.com/Indian-OpenStack-User-Group/events/198128012/" target="_blank" title="OpenStack India">OpenStack India Meetup</a>: September 6, 2014; Bangalore</li>
</ul><p>Interested in the OpenStack project development meetings? A complete list of these meetings' regular schedules is available <a href="https://wiki.openstack.org/wiki/Meetings" target="_blank" title="OpenStack Meetings">here</a>.</p>
<p><em>A special thanks to Stefano Maffulli for the <a href="http://www.openstack.org/blog/2014/08/openstack-community-weekly-newsletter-aug-22-29/" target="_blank" title="OpenStack Community Weekly Newsletter">OpenStack Community Weekly Newsletter</a>, available under a Creative Commons 3.0 License, for linking us to some of these stories and events.</em></p>
