<p>Why settle for boring numbers and static graphs to describe your open source project when you can dynamically display the movements and activity within your project? <a href="http://gource.io/" target="_blank">Gource</a> is an amazing and flexible tool that can be used to display activity from your repositories using a video visualization that people love. Gource can be redistributed and modified under the terms of the <a href="https://en.wikipedia.org/wiki/GNU_General_Public_License" target="_blank">GNU General Public License</a> (version 3 or later). Here are a few interesting videos of how other people have used Gource, which might help you think about how you might use it:</p>
<h2>Yum Gource visualization</h2>
<p>The <a href="https://www.youtube.com/watch?v=OARZB0jGziQ" target="_blank">Yum Gource Visualization video</a> is a tribute to Seth Vidal, lead developer of the <a href="https://fedoraproject.org/wiki/Yum" target="_blank">Yum project</a> and long-time Fedora contributor, shortly after he was killed by a hit-and-run driver while cycling. Seth's contributions to Yum are in blue, whereas contributions from others are in white.</p>
<p class="rtecenter">
<iframe allowfullscreen="" frameborder="0" height="293" src="https://www.youtube.com/embed/OARZB0jGziQ?rel=0" width="520"></iframe></p>
<h2>Linux kernel development</h2>
<p><a href="https://www.youtube.com/watch?v=5iFnzr73XXk" target="_blank">Linux Kernel Development, 1991-2015</a> shows how Gource can help you visualize the full development history of any open source project (the Linux kernel, in this example).</p>
<p class="rtecenter">
<iframe allowfullscreen="" frameborder="0" height="293" src="https://www.youtube.com/embed/5iFnzr73XXk?rel=0" width="520"></iframe></p>
<h2>Population dynamics</h2>
<p><a href="https://www.youtube.com/watch?v=yh9IW9dXFQw" target="_blank">Population Dynamics 1970-2010</a> offers a visualization of population dynamics.</p>
<p class="rtecenter">
<iframe allowfullscreen="" frameborder="0" height="293" src="https://www.youtube.com/embed/yh9IW9dXFQw?rel=0" width="520"></iframe></p>
<p>With any flexible and customizable tool comes a dizzying array of options and configurations. For example, I used the command below to generate a visualization of the <a href="https://github.com/MetricsGrimoire/MailingListStats" target="_blank">MailingListStats (mlstats) repository</a> from the <a href="http://metricsgrimoire.github.io/" target="_blank">Metrics Grimoire</a> suite of tools:</p>
<p><code>$ gource -f --logo images/bitergia_logo_sm.png --title "MailingListStats AKA mlstats" --key --start-date '2014-01-01' --user-image-dir images -a 1 -s .05 --path ../MailingListStats </code></p>
<p>And here is the <a href="https://youtu.be/KM2YMfOYOPY" target="_blank">video</a> generated using these options:</p>
<p class="rtecenter">
<iframe allowfullscreen="" frameborder="0" height="390" src="https://www.youtube.com/embed/KM2YMfOYOPY?rel=0" width="520"></iframe></p>
<p>Option Details:</p>
<ul><li>--path /path/to/repo (or omit and run Gource from the top level of the repo dir)</li>
<li>-f show full screen</li>
<li>--logo images/bitergia_logo_sm.png</li>
<li>--title "MailingListStats AKA mlstats"</li>
<li>--key (shows color key for file types)</li>
<li>--start-date '2014-05-01'</li>
<li>--user-image-dir images (Directory with .jpg or .png images of users - ‘Full Name.png' for avatars)</li>
<li>-a 1 (auto skip to next entry if nothing happens in x seconds – default 3)</li>
<li>-s .05 (speed in seconds per day – default 10)</li>
</ul><p>Gource can also be used to display non-repository information (bug trackers or mailing lists) using the custom log format.</p>
<p>For example, this is how I did a visualization of a mailing list using Gource's custom log format:</p>
<ul><li>Ran a query on a database created by mlstats with all of the messages from one of the Linux kernel mailing lists.</li>
<li>Gathered data about each mailing list post, including the person who posted the message, and if it was a reply, which user they were replying to (<code>M</code> - modified thread by replying to it). Otherwise, flag it as a new message (<code>A</code> - added new thread)</li>
<li>Stripped the emails down to the username (everything before <code>@example.com</code>) to have a shorter identifier for the users, which looks much better for the visualization.</li>
<li>Formatted the output into a file called gource_output.log, a nice pipe-separated Gource custom log format sorted by time in this format: <code> unixtime|user-email_sender|A|new unixtime|user-email_sender|M|user-in_response_to </code></li>
<li>Ran gource <code>$ gource -i 5 --max-user-speed 100 -a 1 --highlight-users gource_output.log</code></li>
<li>Options used:
<ul><li><code>-i 5</code> Time files remain idle (default 0). This allows people being replied to to disappear after 5 seconds to clean up a bit and make it more readable.</li>
<li><code>--max-user-speed 100</code> Speed users can travel per second (default: 500). I slowed this down to 100 to make it easier to see the users.</li>
<li><code>-a 1</code> Auto skip to next entry if nothing happens for a number of seconds (default: 3) sped this up a bit.</li>
<li><code>--highlight-users</code> keeps the usernames for the people sending emails from disappearing. I would have liked to have the same for filenames, which are the people being replied to, but can't seem to find at option for that.</li>
</ul></li>
</ul><p>To learn more about Gource, check out the project's Github page or see Dawn speak at LinuxCon in Toronto.</p>
<p><i>This article offers examples of what Dawn will include her in LinuxCon talk, <a href="https://lcccna2016.sched.org/event/7JWe" target="_blank">Visualize Your Code Repos and More with Gource</a> on Tuesday, August 23. At LinuxCon, she will dive into more options and creative ways to use Gource beyond the typical source code visualization. Attendees should walk away from her talk with ideas and techniques for how to create awesome videos showing the activity within open source projects and communities.</i></p>
