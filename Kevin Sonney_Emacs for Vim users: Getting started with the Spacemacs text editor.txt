<p>I use <a href="https://www.vim.org/" target="_blank">Vim</a> a lot. I'm a site reliability engineer (SRE), and Vim is the one thing I know I can access on every machine in our fleet. I also like <a href="https://www.gnu.org/software/emacs" target="_blank">Emacs</a>, with its wide variety of useful packages, ease of extending, and its many built-in tools. Because they each have their own set of commands, I have to actively switch codes in my head (usually after typing <strong>:wq</strong> in Emacs or trying to <strong>C+X</strong> in Vim). The <a href="https://www.emacswiki.org/emacs/Evil" target="_blank">Evil</a> package for Emacs helps quite a bit by making Emacs behave more like Vim, but there is some effort required to set it up the first time.</p>
<h2 id="enter-spacemacs">Enter Spacemacs</h2>
<p><article class="align-center media media--type-image media--view-mode-full" title="Spacemacs splash screen"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/spacemacs_spash.png" width="675" height="618" alt="Spacemacs splash screen" title="Spacemacs splash screen" /></div>
      
  </article></p>
<p><a href="https://www.spacemacs.org/" target="_blank">Spacemacs</a> is a set of configurations for Emacs that combines an easy setup, Evil, and a system to manage and set up additional Emacs packages with pre-built configurations to make them easier to use out of the box.</p>
<h2 id="installation-and-setup">Installation and setup</h2>
<p>As I mentioned above, Spacemacs is easy to install. No, really: it takes just one command:</p>
<pre><code class="language-text">git clone https://github.com/syl20bnr/spacemacs ~/.emacs.d</code></pre><p>Then just start Emacs. It will prompt you through the basic configuration options and generate a <strong>.spacemacs</strong> configuration file. The defaults are as safe as can be: Vim keybindings, Spacemacs' recommended packages, and the Helm search engine. When the configuration completes, you will see a help screen with some basic information and commands.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Spacemacs help screen"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/spacemacs_help.png" width="500" height="604" alt="Spacemacs help screen" title="Spacemacs help screen" /></div>
      
  </article></p>
<p>Now Spacemacs is set up and ready to go and will behave like Vim. You can start right away by entering <strong>:e &lt;/path/to/file&gt;</strong> to open and edit a file and using good old <strong>:wq</strong> to save (among other commands). As a bonus, if you are a seasoned Emacs user, many of the commands you are used to are still there.</p>
<h2 id="using-spacemacs">Using Spacemacs</h2>
<p>On the main splash screen, you'll notice a lot of information. There are buttons to update Spacemacs and the packages, access different forms of documentation, and open recently edited files.</p>
<p>Whenever you're not in insert mode, you can press the <strong>Space Bar</strong> to bring up a menu of other available options. The default options include access to the Helm search engine and the basic functions for opening and editing files. As you add packages, they will also show up in the menu. In most special screens (i.e., those that are not a document you are editing), the <strong>q</strong> key will exit the screen.</p>
<h2 id="configuring-spacemacs">Configuring Spacemacs</h2>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on Vim</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/resources/what-vim?intcmp=7013a000002CxqfAAC">What is Vim?</a></li>
<li><a href="https://opensource.com/tags/vim?intcmp=7013a000002CxqfAAC">Latest Vim articles</a></li>
<li><a href="https://opensource.com/article/18/1/top-11-vi-tips-and-tricks?intcmp=7013a000002CxqfAAC">Top 11 vi tips and tricks</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-vim?intcmp=7013a000002CxqfAAC">Vim cheat sheet</a></li>
</ul></div>
</div>
</div>
</div>
<p>Before getting into Spacemacs' configuration, you need to understand <strong>layers</strong>. Layers are self-contained configuration files that load on top of one another. A layer is comprised of the instructions to download and install the package and any dependencies, as well as the basic configuration and key mappings for the package.</p>
<p>Spacemacs has quite a few <a href="https://www.spacemacs.org/layers/LAYERS.html" target="_blank">layers available</a>, and more are being added all the time. You can find the complete list in the <strong>~/.emacs.d/layers</strong> directory tree. They are organized by type; to use one, just add it in the main <strong>.spacemacs</strong> configuration file to the <strong>dotspacemacs-configuration-layers</strong> list.</p>
<p>I generally enable the Git, Version-Control, Markdown, and Colors (theme support) layers. If you are familiar with configuring Emacs, you can also add custom configurations in <a href="https://en.wikipedia.org/wiki/Lisp_(programming_language)" target="_blank">Lisp</a> to the <strong>dotspacemacs/user-config</strong> section.</p>
<p>You can also enable a Nyan Cat progress bar by adding the following line in your layers list:</p>
<pre><code class="language-haskell">(colors :variables colors-enable-nyan-cat-progress-bar t)</code></pre><p> </p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Nyan Cat progress bar in Spacemacs"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/nyan-cat-progress.png" width="675" height="32" alt="Nyan Cat progress bar in Spacemacs" title="Nyan Cat progress bar in Spacemacs" /></div>
      
  </article></p>
<h2 id="using-org-mode-in-spacemacs">Using Org mode in Spacemacs</h2>
<p>One of my other favorite layers is <a href="https://orgmode.org" target="_blank">Org mode</a>, probably one of the most popular notes, to-do, and project management applications in the open source world.</p>
<p>To install Org, just open up the <strong>.spacemacs</strong> file and uncomment the line for <strong>org</strong> under <strong>dotspacemacs-configuration-layers</strong>.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Installing Org mode in Spacemacs"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/spacemacs_org_change.png" width="675" height="309" alt="Installing Org mode in Spacemacs" title="Installing Org mode in Spacemacs" /></div>
      
  </article></p>
<p>Exit and restart Emacs, and it will download the Org packages and set them up. Type <strong>Space Bar+a</strong>, and you see a new menu item for Org with the hotkey <strong>o</strong>, and the common Org functions—agenda, to-do list, etc.—are under that menu. They are  blank until you configure the default Org files. The easiest way to do that is with the built-in Emacs configuration tool, which you can access by typing <strong>Space Bar+?</strong> and searching for <strong>Customize</strong>. When the Customize screen opens, search for <strong>org-agenda-files</strong>. Add a file or two to the list (I used <strong>~/todo.org</strong> and <strong>~/notes.org</strong>), click Apply and Save, then exit Customize.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Emacs Customize tool in Spacemacs"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/emacs_customize.png" width="675" height="199" alt="Emacs Customize tool in Spacemacs" title="Emacs Customize tool in Spacemacs" /></div>
      
  </article></p>
<p>Next, create a file so that Org can read them into the agenda and to-do list. Even if the file is blank, that's OK—it just has to exist. Since I added two files—todo.org and notes.org—to my configuration, I can type <strong>:e todo.org</strong> and <strong>:e notes.org</strong> to open both, and then <strong>:w</strong> to save the blank files.</p>
<p>Next, enter the Org agenda with <strong>Space Bar+a+o+a</strong> or the Org to-do list with <strong>Space Bar+a+o+t</strong>. If you have added actionable items or scheduled events to the notes or to-do files, you will see them now. You can find out more about Org's structure and syntax in <em><a href="https://opensource.com/article/19/1/productivity-tool-org-mode">Get started with Org mode without Emacs</a></em> or on the <a href="https://orgmode.org" target="_blank">Org mode</a> website.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Spacemacs todo.org and the Org todo agenda"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/spacemacs_org.png" width="675" height="613" alt="Spacemacs todo.org and the Org todo agenda" title="Spacemacs todo.org and the Org todo agenda" /></div>
      
  </article></p>
<p>Spacemacs offers all the power of Emacs combined with the keystroke commands and functionality you are used to with Vim. Give it a try, and please let me know what you think in the comments.</p>
