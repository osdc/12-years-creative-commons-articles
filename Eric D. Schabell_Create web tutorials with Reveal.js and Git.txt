<p>Whether you're a learner or a teacher, you probably recognize the value of online workshops set up like slideshows for communicating knowledge. If you've ever stumbled upon one of these well-organized tutorials that are set up page by page, chapter by chapter, you may have wondered how hard it was to create such a website.</p>
<p>Well, I'm here to show you how easy it is to generate this type of workshop using a fully automated process.</p>
<h2 id="introduction">Introduction</h2>
<p>When I started putting my learning content online, it was not a nice, seamless experience. So, I wanted something repeatable and consistent that was also easy to maintain, since my content changes as the technology I teach progresses.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Great Content</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=7016000000127cYAAQ">Free online course: RHEL technical overview</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=7016000000127cYAAQ">Learn Advanced Linux Commands</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Download Cheat Sheets</a></li>
<li><a href="https://opensource.com/alternatives?intcmp=7016000000127cYAAQ">Find an Open Source Alternative</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=7016000000127cYAAQ">Read Top Linux Content</a></li>
<li><a href="https://opensource.com/resources?intcmp=7016000000127cYAAQ">Check out open source resources</a></li>
</ul></div>
</div>
</div>
</div>
<p>I tried many delivery models, from low-level code generators, such as <a href="https://asciidoctor.org/" target="_blank">Asciidoctor</a>, to laying out a workshop in a single PDF file. All failed to satisfy me. When I deliver live, onsite workshops, I like using slideshows, so I wondered if I could do the same thing for my online, self-paced workshop experiences.</p>
<p>After some digging, I built a foundation for creating painless workshop websites. It helped that I was already using a presentation-generation framework that resulted in a website-friendly format (HTML).</p>
<h2 id="setting-it-up">Setting it up</h2>
<p>Here the basic components you need for this project:</p>
<ul><li>Workshop idea (this is your problem, can't help you here)</li>
<li>Reveal.js for the workshop slides</li>
<li>GitLab project repository</li>
<li>Your favorite HTML code editor</li>
<li>Web browser</li>
<li>Git installed on your machine</li>
</ul><p>If this list looks intimidating, there's a quick way to get started that doesn't involve pulling all the pieces together one by one: You can use my template project to give you a kickstart with the slides and project setup.</p>
<p>This article assumes you're familiar with Git and projects hosted on a Git platform like GitLab. If you need a refresher or tutorial, check out our <a href="https://opensource.com/resources/what-is-git">introductory Git series</a>.</p>
<p>Start by cloning the template project to your local machine:</p>
<pre><code class="language-bash">$ git clone https://gitlab.com/eschabell/beginners-guide-automated-workshops.git</code></pre><p>Set up a new GitLab project for this and import the template project as the initial import.</p>
<p>There are a number of important files for the workshop website. In the <strong>root</strong> directory, you'll find a file called <strong>.gitlab-ci.yml</strong>, which is used as a trigger when you commit changes to the master branch (i.e., merge pull requests to <strong>master</strong>). It triggers a copy of the complete contents of the <strong>slides</strong> directory into the GitLab project's <strong>website</strong> folder.</p>
<p>I have this hosted as a project called <strong>beginners-guide-automated-workshops</strong> in my GitLab account. When it deploys, you can view the contents of the <strong>slides</strong> directory in your browser by navigating to:</p>
<pre><code class="language-text">https://eschabell.gitlab.io/beginners-guide-automated-workshops</code></pre><p>For your user account and project, the URL would look like:</p>
<pre><code class="language-text">https://[YOUR_USERNAME].gitlab.io/[YOUR_PROJECT_NAME]</code></pre><p>These are the basic materials you need to start creating your website content. When you push changes, they will automatically generate your updated workshop website. Note that the default template contains several example slides, which will be your first workshop website after you complete the full check-in to your repository.</p>
<p>The workshop template results in a <a href="https://revealjs.com/#/" target="_blank">reveal.js</a> slideshow that can run in any browser, with automatic resizing that allows it to be viewed by almost anyone, anywhere, on any device.</p>
<p>How's that for creating handy and accessible workshops?</p>
<h2 id="how-it-works">How it works</h2>
<p>With this background in place, you're ready to explore the workshop materials and start putting your content together. Everything you need can be found in the project's <strong>slides</strong> directory; this is where all of the magic happens with reveal.js to create the workshop slideshow in a browser.</p>
<p>The files and directories you'll be working with to craft your workshop are:</p>
<ul><li>The <strong>default.css</strong> file</li>
<li>The <strong>images</strong> directory</li>
<li>The <strong>index.html</strong> file</li>
</ul><p>Open each one in your favorite HTML/CSS editor and make the changes described below. It does not matter which editor you use; I prefer <a href="https://www.jetbrains.com/ruby/" target="_blank">RubyMine IDE</a> because it offers a page preview in the local browser. This helps when I'm testing out content before pushing it online to the workshop website.</p>
<h3 id="default.css-file">Default.css file</h3>
<p>The file <strong>css/theme/default.css</strong> is the base file where you will set important global settings for your workshop slides. The two main items of interest are the default font and background image for all slides.</p>
<p>In <strong>default.css</strong>, look at the section labeled <strong>GLOBAL STYLES</strong>. The current default font is listed in the line:</p>
<pre><code class="language-text">font-family: "Red Hat Display", "Overpass", san-serif;</code></pre><p>If you're using a non-standard font type, you must import it (as was done for the Overpass font type) in the line:</p>
<pre><code class="language-bash">@import url('SOME_URL');</code></pre><p>The <strong>background</strong> is the default image for every slide you create. It is stored in the <strong>images</strong> directory (see below) and set in the line below (focus on the image path):</p>
<pre><code class="language-text">background: url("…/…/images/backgrounds/basic.png")</code></pre><p>To set a default background, just point this line to the image you want to use.</p>
<h3 id="images-directory">Images directory</h3>
<p>As its name implies, the <strong>images</strong> directory is used for storing the images you want to use on your workshop slides. For example, I usually put screenshots that demonstrate the progress of the workshop topic on my individual slides.</p>
<p>For now, just know that you need to store the background images in a subdirectory (<strong>backgrounds</strong>) and the images you plan to use in your slides in the <strong>Images</strong> directory.</p>
<h3 id="index.html-file">Index.html file</h3>
<p>Now that you have those two files sorted out, you'll spend the rest of your time creating slides in the HTML files, starting with <strong>index.html</strong>. For your workshop website to start taking shape, pay attention to the following three sections in this file:</p>
<ul><li>The <strong>head</strong> section, where you set the title, author, and description</li>
<li>The <strong>body</strong> section, where you find the individual slides to design</li>
<li>Each <strong>section</strong>, where you define the contents of individual slides</li>
</ul><p>Start with the <strong>head</strong> section, since it's at the top. The template project has three placeholder lines for you to update:</p>
<pre><code class="language-text">&lt;title&gt;INSERT-YOUR-TITLE-HERE&lt;/title&gt;
&lt;meta name="description" content="YOUR DESCIPTION HERE."&gt;
&lt;meta name="author" content="YOUR NAME"&gt;</code></pre><p>The <strong>title</strong> tag contains the text that appears in the browser tab when the file is open. Change it to something relevant to the title of your workshop (or maybe a section of your workshop), but remember to keep it short since tab title space is limited. The <strong>description</strong> meta tag contains a short description of your workshop, and the <strong>author</strong> meta tag is where you should put your name (or the workshop creator's name, if you're doing this for someone else).</p>
<p>Now move on to the <strong>body</strong> section. You'll notice that it's divided into a number of <strong>section</strong> tags. The opening of the <strong>body</strong> contains a comment that explains that you're creating slides for each open and closing tag labeled <strong>section</strong>:</p>
<pre><code class="language-text">&lt;body&gt;
	&lt;div class="reveal"&gt;

	&lt;!-- Any section element inside of this container is displayed as a slide --&gt;
	&lt;div class="slides"&gt;</code></pre><p>Next, create your individual slides, with each slide enclosed in <strong>section</strong> tags. The template includes a few slides to help you get started. For example, here's the first slide:</p>
<pre><code class="language-text">&lt;section&gt;
      &lt;div style="width: 1056px; height: 300px"&gt;
            &lt;h1&gt;Beginners guide&lt;/h1&gt;
            &lt;h2&gt;to automated workshops&lt;/h2&gt;
      &lt;/div&gt;
      &lt;div style="width: 1056px; height: 200px; text-align: left"&gt;
            Brought to you by,&lt;br/&gt;
            YOUR-NAME&lt;br/&gt;
      &lt;/div&gt;
      &lt;aside class="notes"&gt;Here are notes: Welcome to the workshop!&lt;/aside&gt;
&lt;/section&gt;</code></pre><p>This slide has two areas divided with <strong>div</strong> tags. Spacing separates the title and the author.</p>
<p>Assuming you have some knowledge of using HTML, try various things to develop your workshop. It's really handy to use a browser as you go to preview the results. Some IDEs provide local viewing of changes, but you can also open the <strong>index.html</strong> file and view your changes before pushing them to the repository.</p>
<p>Once you're satisfied with your workshop, push your changes, and wait for them to pass through the continuous integration pipeline. They'll be hosted like the template project at <a href="https://eschabell.gitlab.io/beginners-guide-automated-workshops" target="_blank">https://eschabell.gitlab.io/beginners-guide-automated-workshops</a>.</p>
<h2 id="learn-more">Learn more</h2>
<p>To learn more about what you can do with this workflow, check out the following example workshops and sites that host workshop collections. All of these are based on the workflow described in this article.</p>
<p>Workshop examples:</p>
<ul><li><a href="https://gitlab.com/bpmworkshop/rhpam-devops-workshop" target="_blank">Red Hat Process Automation Manage workshop</a></li>
<li><a href="https://gitlab.com/bpmworkshop/presentation-bpmworkshop-travel-agency" target="_blank">JBoss Travel Agency BPM Suite online workshop</a></li>
</ul><p>Workshop collections:</p>
<ul><li><a href="https://bpmworkshop.gitlab.io/" target="_blank">Rule the world: Practical decisions &amp; process automation development workshops</a></li>
<li><a href="https://appdevcloudworkshop.gitlab.io/" target="_blank">Application development in the cloud workshop</a></li>
<li><a href="https://redhatdemocentral.gitlab.io/portfolio-architecture-workshops" target="_blank">Portfolio architecture: Workshops for creating impactful architectural diagrams</a></li>
</ul><p>I hope this beginner's guide and the template workshop project show you easy and painless it can be to develop and maintain workshop websites in a consistent manner. I also hope this workflow gives your workshop audiences full access to your content on almost any device so they can learn from the knowledge you're sharing.</p>
