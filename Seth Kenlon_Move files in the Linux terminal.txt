<p>To move a file on a computer with a graphical interface, you open the folder where the file is currently located, and then open another window to the folder you want to move the file into. Finally, you drag and drop the file from one to the other.</p>
<p><span class="emphasis">To move a file in a terminal, you use the<em> </em></span> <span class="command"><strong>mv </strong>command</span> to move a file from one location to another.</p>
<pre><code class="language-bash">$ mv example.txt ~/Documents

$ ls ~/Documents
example.txt</code></pre><p>In this example, you've moved <strong>example.txt</strong> from its current folder into the <strong>Documents</strong> folder.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>The Linux Terminal</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/life/17/10/top-terminal-emulators?intcmp=7016000000127cYAAQ">Top 7 terminal emulators for Linux</a></li>
<li><a href="https://opensource.com/article/17/2/command-line-tools-data-analysis-linux?intcmp=7016000000127cYAAQ">10 command-line tools for data analysis in Linux</a></li>
<li><a href="https://opensource.com/downloads/advanced-ssh-cheat-sheet?intcmp=7016000000127cYAAQ">Download Now: SSH cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=7016000000127cYAAQ">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://opensource.com/tags/command-line?intcmp=7016000000127cYAAQ">Linux command line tutorials</a></li>
</ul></div>
</div>
</div>
</div>
<p>As long as you know where you want to take a file <em>from</em> and where you want to move it <em>to</em>, you can send files from any location to any location, no matter where you are. This can be a serious time saver compared to navigating through all the folders on your computer in a series of windows just to locate a file, and then opening a new window to where you want that file to go, and then dragging that file.</p>
<p>The <strong>mv</strong> command by default does exactly as it's told: it moves a file from one location to another. Should a file with the same name already exist in the destination location, it gets overwritten. To prevent a file from being overwritten without warning, use the <strong>--interactive </strong>(or <strong>-i </strong>for short) option:</p>
<pre><code class="language-bash">$ mv -i example.txt ~/Documents
mv: overwrite '/home/tux/Documents/example.txt'? 
</code></pre><p> </p>
