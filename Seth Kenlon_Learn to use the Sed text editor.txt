<p>Created for version 7 of AT&amp;T’s original Unix operating system, the <code>sed</code> command has been included with probably every Unix and Linux OS since. The <code>sed</code> application is a <em>stream editor</em>, and unlike a text editor it doesn’t open a visual buffer into which a file’s data is loaded for processing. Instead, it operates on a file, line by line, according to either a command typed into a terminal or a series of commands in a script.</p>
<h2>Installing</h2>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Linux resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux commands cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Free online course: RHEL Technical Overview</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux networking cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">SELinux cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux common commands cheat sheet</a></li>
<li><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">What are Linux containers?</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Our latest Linux articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>If you’re using Linux, BSD, or macOS, then you already have GNU or BSD <code>sed</code> installed. These are two unique reimplementations of the original <code>sed</code> command, and while they’re similar, there can be minor differences. GNU <code>sed</code> is generally regarded to be the most feature-rich <code>sed</code> out there, and it’s widely available on any of these platforms.</p>
<p>If you can’t find GNU <code>sed</code> (often called <code>gsed</code> on non-Linux systems), then you can <a href="http://www.gnu.org/software/sed/" target="_blank">download its source code from the GNU website</a>. The nice thing about having GNU <code>sed</code> installed is that it can be used for its extra functions, but it can also be constrained to conform to just the <a href="https://opensource.com/article/19/7/what-posix-richard-stallman-explains" target="_self">POSIX</a> specifications of <code>sed</code>, should you require portability.</p>
<p>On Windows, you can <a href="https://chocolatey.org/packages/sed" target="_blank">install</a> GNU <code>sed</code> with <a href="https://opensource.com/article/20/3/chocolatey" target="_self">Chocolatey</a>.</p>
<h2>How Sed works</h2>
<p>The <code>sed</code> application works on one line at a time. Because it has no visual display, it creates a pattern space—a space in memory containing the current line from the input stream (with any trailing newline character removed). Once the pattern space is populated, your instructions to <code>sed</code> are executed. Sometimes your commands are conditional, and other times they are absolute, so the results of these commands depend on how you’re using <code>sed</code>.</p>
<p>When the end of commands is reached, <code>sed</code> prints the contents of the pattern space to the output stream. The default output stream is <strong>stdout</strong>, but it can be redirected to a file or even back into the same file using the <code>--in-place=.bak</code> option.</p>
<p>Then the cycle begins again with the next input line.</p>
<p>The syntax for the <code>sed</code> command is:</p>
<pre><code class="language-bash">$ sed --options [optional SCRIPT] [INPUT FILE or STREAM]</code></pre><h3>Finding what you want to edit</h3>
<p>In a visual editor, you usually locate what you want to change in a text file without thinking much about it. Your eye (or screen reader) scans the text, finds the word you want to change or the place you want to insert or remove text, and then you just start typing. There is no interactive mode for <code>sed</code>, though, so you must tell it what conditions must be met for it to run specific commands.</p>
<p>For these examples, assume that a file called <code>example.txt</code> contains this text:</p>
<pre><code class="language-text">hello
world
This is line three.
Here is the final line.</code></pre><h3 id="helloworldthis-is-line-three.here-is-the-final-line.">Line number</h3>
<p>Specifying a line number tells <code>sed</code> to operate only on that specific line in the file.</p>
<p>For instance, this command selects line 1 of a file and prints it. Because <code>sed</code>’s default action after processing is also to print a line to <strong>stdout</strong>, this has the effect of duplicating the first line:</p>
<pre><code class="language-bash">$ sed ‘1p’ example.txt
hello
hello
world
This is line three.
Here is the final line.</code></pre><p>You can specify line numbers in steps, too. For instance, <code>1~2</code> indicates that every 2 lines are selected ("select every second line starting with the first"). The instruction <code>1~3</code> means to select every third line after the first:</p>
<pre><code class="language-bash">$ sed ‘1p’ example.txt
hello
hello
world
This is line three.
Here is the final line.
Here is the final line.</code></pre><h3>Line position</h3>
<p>You can operate only on the last line of a file by using <code>$</code> as a selector:</p>
<pre><code class="language-bash">$ sed ‘$p’ example.txt
hello
world
This is line three.
Here is the final line.
Here is the final line.</code></pre><p>In GNU <code>sed</code>, you can select more than one line (<code>sed '1,$p'</code> prints the first and final line, for example).</p>
<h3>Negation</h3>
<p>Any selection by number or position, you can invert with the exclamation mark (<code>!</code>) character. This selects all lines <em>except</em> the first line:</p>
<pre><code class="language-bash">$ sed ‘1!p’ example.txt
hello
world
world
This is line three.
This is line three.
Here is the final line.
Here is the final line.</code></pre><h3>Pattern matching</h3>
<p>You can think of a pattern match as a <strong>find</strong> operation in a word processor or a browser. You provide a word (a <em>pattern</em>), and the results are selected. The syntax for a pattern match is <code>/pattern/</code>.</p>
<pre><code class="language-bash">$ sed ‘/hello/p’ example.txt
hello
hello
world
This is line three.
Here is the final line.
$ sed ‘/line/p’ example.txt
hello
world
This is line three.
This is line three.
Here is the final line.
Here is the final line.</code></pre><h2 id="sed-hellop-example.txthellohelloworldthis-is-line-three.here-is-the-final-line.-sed-linep-example.txthelloworldthis-is-line-three.this-is-line-three.here-is-the-final-line.here-is-the-final-line.">Editing with Sed</h2>
<p>Once you’ve found what you want to edit, you can perform whatever action you want. You perform edits with <code>sed</code> with commands. Commands in <code>sed</code> are different from the <code>sed</code> command itself. If it helps, think of them as "actions" or "verbs" or "instructions."</p>
<p>Commands in <code>sed</code> are single letters, such as the <code>p</code> for <strong>print</strong> command used in previous examples. They can be difficult to recall at first, but as with everything, you get to know them with practice.</p>
<h3>p for print</h3>
<p>The <code>p</code> instruction prints whatever is currently held in pattern space.</p>
<h3>d for delete</h3>
<p>The <code>d</code> instruction deletes the pattern space.</p>
<pre><code class="language-bash">$ sed ‘$d’ example.txt
hello
world
This is line three.
$ sed ‘1d’ example.txt
world
This is line three.
Here is the final line.</code></pre><h3>s for search and replace</h3>
<p>The <code>s</code> command searches for a pattern and replaces it with something else. This is probably the most popular and casual use for <code>sed</code>, and it’s often the first (and sometimes the only) <code>sed</code> command a user learns. It’s almost certainly the most useful command for text editing.</p>
<pre><code class="language-bash">$ sed ‘s/world/opensource.com/’
hello
opensource.com
This is line three.
Here is the final line.</code></pre><p>There are special functions you can use in your replacement text, too. For instance, <code>\L</code> transforms the replacement text to lowercase and <code>\l</code> does the same for just the next character. There are others, which are listed in the <code>sed</code> documentation (you can view that with the <code>info sed</code> command).</p>
<p>The special character <code>&amp;</code> in the replacement clause refers to the matched pattern:</p>
<pre><code class="language-bash">$ sed ‘s/is/\U&amp;/’ example.txt
hello
world
ThIS is line three.
Here IS the final line.</code></pre><p>You can also pass special flags to affect how <code>s</code> processes what it finds. The <code>g</code> (for <em>global</em>, presumably) flag tells <code>s</code> to apply the replacement to all matches found on the line and not just the first match:</p>
<pre><code class="language-bash">$ sed ‘s/is/\U&amp;/g’ example.txt
hello
world
ThIS IS line three.
Here IS the final line.</code></pre><p>Other important flags include a number to indicate which occurrence of a matched pattern to affect:</p>
<pre><code class="language-bash">$ sed ‘s/is/\U&amp;/2’ example.txt
hello
world
This IS line three.
Here is the final line.</code></pre><p id="sed-sisu2-example.txthelloworldthis-is-line-three.here-is-the-final-line.">The <code>w</code> flag, followed by a filename, writes a matched line to a file <em>only if</em> a change is made:</p>
<pre><code class="language-bash">$ sed ‘s/is/\U&amp;/w sed.log’ example.txt
hello
world
ThIS is line three.
Here IS the final line.
$ cat sed.log
ThIS is line three.
Here IS the final line.</code></pre><p id="sed-sisuw-sed.log-example.txthelloworldthis-is-line-three.here-is-the-final-line.-cat-sed.logthis-is-line-three.here-is-the-final-line.">Flags can be combined:</p>
<pre><code class="language-bash">$ sed ‘s/is/\U&amp;/2w sed.log’ example.txt
hello
world
This IS line three.
Here is the final line.
$ cat sed.log
This IS line three.</code></pre><h2>Scripts</h2>
<p>There are lots of great sites out there with <code>sed</code> "one-liners." They give you task-oriented <code>sed</code> commands to solve common problems. However, learning <code>sed</code> for yourself enables you to write your own one-liners, and those can be tailored to your specific needs.</p>
<p>Scripts for <code>sed </code>can be written as lines in a terminal, or they can be saved to a file and executed with <code>sed</code> itself. I tend to write small scripts all as one command because I find myself rarely re-using <code>sed</code> commands in real life. When I write a <code>sed </code>script, it’s usually very specific to one file. For example, after writing the initial draft of this very article, I used <code>sed</code> to standardize the capitalization of "sed", and that’s a task I’ll probably never have to do again.</p>
<p>You can issue a series of distinct commands to <code>sed</code> separated by a semicolon (<code>;</code>).</p>
<pre><code class="language-bash">$ sed ‘3t ; s/line/\U&amp;/’ example.txt
hello
world
This is LINE three.
This is the final line.</code></pre><h2 id="sed-3t--slineu-example.txthelloworldthis-is-line-three.this-is-the-final-line.">Scope changes with braces</h2>
<p>You can also limit which results are affected with braces (<code>{}</code>). When you enclose <code>sed</code> commands in braces, they apply only to a specific selection. For example, the word "line" appears in two lines of the sample text. You can force <code>sed</code> to affect only the final line by declaring the required match condition (<code>$</code> to indicate the final line) and placing the <code>s</code> command you want to be performed in braces immediately thereafter:</p>
<pre><code class="language-bash">$ sed ‘$ {s/line/\U&amp;/}’ example.txt
hello
world
This is line three.
This is the final LINE.</code></pre><h2 id="sed--slineu-example.txthelloworldthis-is-line-three.this-is-the-final-line.">Learn Sed</h2>
<p>You can do a lot more with <code>sed</code> than what’s explained in this article. I haven’t even gotten to branching (<code>b</code>), tests (<code>t</code>), the <em>hold</em> space (<code>H</code>), and many other features. Like <a href="https://opensource.com/article/20/12/gnu-ed"><code>ed</code></a>, <code>sed </code>probably isn’t the text editor you’re going to use for document creation or even for every scripted task you need doing, but it is a powerful option you have as a POSIX user. Learning how <code>sed</code> commands are structured and how to write short scripts can make for quick changes to massive amounts of text. Read through the <code>info</code> pages of GNU <code>sed</code>, or the man pages of BSD <code>sed</code>, and find out what <code>sed</code> can do for you.</p>
