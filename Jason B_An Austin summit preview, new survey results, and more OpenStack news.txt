<p>Interested in keeping track of what is happening in the open source cloud? Opensource.com is your source for news in <a href="https://opensource.com/resources/what-is-openstack" target="_blank" title="What is OpenStack?">OpenStack</a>, the open source cloud infrastructure project.</p>
<!--break--><h3>OpenStack around the web</h3>
<p>There's a lot of interesting stuff being written about OpenStack. Here's a sampling:</p>
<ul><li><a href="http://www.zdnet.com/article/openstack-the-open-source-cloud-still-gaining-converts-survey-shows/" target="_blank">OpenStack, the open-source cloud, still gaining converts, survey shows</a>: Survey finds IT managers say OpenStack is less costly alternative.</li>
<li><a href="http://superuser.openstack.org/articles/the-openstack-summit-austin-unveils-new-content-events-training" target="_blank">The OpenStack Summit Austin unveils new content, events, training</a>: More tracks and double the workshops await Stackers.</li>
<li><a href="https://thestack.com/cloud/2016/03/07/red-hat-insider-to-make-openstack-cheaper-make-it-easier/" target="_blank">To make OpenStack cheaper, make it easier</a>: Reduce costs by reducing complexity.</li>
<li><a href="http://my1.fr/blog/puppet-openstack-mitaka-success/" target="_blank">Puppet OpenStack success during Mitaka cycle</a>: A glance at what's new.</li>
<li><a href="http://superuser.openstack.org/articles/what-s-new-under-the-openstack-big-tent-dragonflow-and-kuryr" target="_blank">What's new under the OpenStack big tent</a>: Dragonflow and Kuryr.</li>
<li><a href="https://developer.ibm.com/opentech/2016/03/08/day-1-openstack-bug-smash-in-nyc/" target="_blank">OpenStack bug smash in NYC</a>: A recap from one location of a global event.</li>
<li><a href="http://superuser.openstack.org/articles/women-of-openstack-launches-new-mentoring-program" target="_blank">Women of OpenStack launches new mentoring program</a>: The new program aims to provide technical or career guidance.</li>
<li><a href="http://www.infoworld.com/article/3041064/application-development/what-ebay-looks-like-under-the-hood.html" target="_blank">What eBay looks like under the hood</a>: A look at the auction giant's infrastructure, including OpenStack.</li>
<li><a href="http://superuser.openstack.org/articles/openstack-academy-opens-its-doors-at-the-austin-summit" target="_blank">OpenStack Academy opens its doors at the Austin Summit</a>: On-site training and workshops are a big focus of the April summit schedule released earlier this week.</li>
</ul><h3>OpenStack discussions</h3>
<p>Here's a sample of some of the most active discussions this week on the OpenStack developers' listserv. For a more in-depth look, why not <a href="http://lists.openstack.org/pipermail/openstack-dev/" target="_blank" title="OpenStack listserv">dive in</a> yourself?</p>
<ul><li><a href="http://lists.openstack.org/pipermail/openstack-dev/2016-March/089021.html" target="_blank">Question about electorate for project without Gerrit contribution</a>: How do you determine who is active when contributions aren't code?</li>
<li><a href="http://lists.openstack.org/pipermail/openstack-dev/2016-March/088641.html" target="_blank">How do we move forward with xstatic releases</a>: How should Horizon solve a blocking issue?</li>
<li><a href="http://lists.openstack.org/pipermail/openstack-dev/2016-March/088684.html" target="_blank">Using multiple token formats in a one OpenStack cloud</a>: Handling more than one token format in Keystone.</li>
<li><a href="http://lists.openstack.org/pipermail/openstack-dev/2016-March/088853.html" target="_blank">Preparing to create stable/mitaka branches for libraries</a>: The next major release is fast approaching.</li>
<li><a href="http://lists.openstack.org/pipermail/openstack-dev/2016-March/088895.html" target="_blank">Default ports list</a>: How are port collisions avoided between OpenStack projects?</li>
</ul><h3>Cloud &amp; OpenStack events</h3>
<p>Here's what is happening this week. Something missing? <a href="mailto:osdc-admin@redhat.com">Email</a> us and let us know, or add it to our <a href="https://opensource.com/resources/conferences-and-events-monthly" target="_blank" title="Events Calendar">events calendar</a>.</p>
<ul><li><a href="http://www.meetup.com/OpenStack-Seattle/events/228266427/" target="_blank">Learn how to use DBaaS for OpenStack</a>: Tuesday, March 15; Seattle, WA.</li>
<li><a href="http://www.meetup.com/OpenStack-for-Enterprises-NYC/events/228618706/" target="_blank">The future of OpenStack</a>: Wednesday, March 16; New York, NY.</li>
<li><a href="http://www.meetup.com/Openstack-Boston/events/229083602/" target="_blank">Container networking in OpenStack</a>: Wednesday, March 16; Boston, MA.</li>
<li><a href="http://www.meetup.com/openstack-pdx/events/229034475/" target="_blank">OpenStack PDX meetup</a>: Thursday, March 17; Portland, OR.</li>
<li><a href="http://www.meetup.com/Silicon-Valley-Financial-Services-Cloud/events/229351781/" target="_blank">An introduction to OpenStack Trove</a>: Thursday, March 17; Palo Alto, CA.</li>
<li><a href="http://www.meetup.com/OpenStack-Austin/events/228427794/" target="_blank">Trove, database as a service, and networking with containers</a>: Thursday, March 17; Austin, TX.</li>
<li><a href="http://www.meetup.com/openstack/events/224950337/" target="_blank">Orchestrating OpenStack with OneOps</a>: Thursday, March 17; San Francisco, CA.</li>
</ul><p><em>Interested in the OpenStack project development meetings? A complete list of these meetings' regular schedules is available <a href="https://wiki.openstack.org/wiki/Meetings" target="_blank" title="OpenStack Meetings">here</a>.</em></p>
