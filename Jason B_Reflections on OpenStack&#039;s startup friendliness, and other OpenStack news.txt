<p>Interested in keeping track of what's happening in the open source cloud? Opensource.com is your source for news in <a href="https://opensource.com/resources/what-is-openstack" target="_blank" title="What is OpenStack?">OpenStack</a>, the open source cloud infrastructure project.</p>
<p><!--break--></p>
<h3>OpenStack around the web</h3>
<p>There's a lot of interesting stuff being written about OpenStack. Here's a sampling:</p>
<ul><li><a href="http://superuser.openstack.org/articles/containers-and-openstack-here-s-what-you-need-to-know" target="_blank">Containers and OpenStack</a>: Here's what you need to know.</li>
<li><a href="http://www.forbes.com/sites/benkepes/2015/06/05/five-years-on-and-openstack-is-absolutely-a-thing/" target="_blank">Five years on, and OpenStack is absolutely a thing</a>: It's time to put skepticism in the past.</li>
<li><a href="http://www.idevnews.com/stories/6536/OpenStack-Foundation-Looks-To-Promote-Collaboration-and-Access-to-New-Technology" target="_blank">OpenStack Foundation looks to promote collaboration and access to new technology</a>: Leveling out the learning curve.</li>
<li><a href="http://siliconangle.com/blog/2015/06/01/openstack-is-maturing-with-the-community-openstack/" target="_blank">OpenStack is maturing with the community</a>: An interview with Radhesh Balakrishnan and Mike Cohen.</li>
<li><a href="http://ronaldbradford.com/blog/contributing-to-openstack-2015-06-03/" target="_blank">Contributing to OpenStack</a>: Resources for getting started.</li>
<li><a href="http://superuser.openstack.org/articles/what-building-legos-can-teach-you-about-open-source" target="_blank">What building Legos can teach you about open source</a>: OpenStack Upstream Training unites play with open source development.</li>
<li><a href="http://robhirschfeld.com/2015/06/03/openstack-start-up-friendly/" target="_blank">10 ways to make OpenStack more start-up friendly</a>: Suggestions from OpenStack contributor Rob Hirschfeld.</li>
<li><a href="http://maffulli.net/2015/06/04/openstack-is-as-start-up-friendly-as-anything/" target="_blank">OpenStack is as start-up friendly as anything</a>: A rebuttal from OpenStack Community Manager Steffano Maffuli.</li>
</ul><h3>OpenStack discussions</h3>
<p>Here's a sample of some of the most active discussions this week on the OpenStack developers' listserv. For a more in-depth look, why not <a href="http://lists.openstack.org/pipermail/openstack-dev/" target="_blank" title="OpenStack listserv">dive in</a> yourself?</p>
<ul><li><a href="http://lists.openstack.org/pipermail/openstack-dev/2015-June/065283.html" target="_blank">Progressing/tracking work on libvirt / vif drivers</a>: A look at recent updates.</li>
<li><a href="http://lists.openstack.org/pipermail/openstack-dev/2015-June/065298.html" target="_blank">Adding packaging as an OpenStack project</a>: Considering how OpenStack gets distributed.</li>
<li><a href="http://lists.openstack.org/pipermail/openstack-dev/2015-June/065313.html" target="_blank">Updating our concept of resources</a>: How does the Nova scheduler handle them?</li>
<li><a href="http://lists.openstack.org/pipermail/openstack-dev/2015-June/065424.html" target="_blank">Help needed with TOSCA support in Murano</a>: A follow-up from the summit.</li>
<li><a href="http://lists.openstack.org/pipermail/openstack-dev/2015-June/065532.html" target="_blank">Scaling up code review process</a>: Making code review work for big projects.</li>
<li><a href="http://lists.openstack.org/pipermail/openstack-dev/2015-June/065626.html" target="_blank">Big Tent Mode within respective projects</a>: Which projects are implementing this model?</li>
</ul><h3>Cloud &amp; OpenStack events</h3>
<p>Here's what is happening this week. Something missing? <a href="mailto:osdc-admin@redhat.com">Email</a> us and let us know, or add it to our <a href="https://opensource.com/resources/conferences-and-events-monthly" target="_blank" title="Events Calendar">events calendar</a>.</p>
<ul><li><a href="http://www.meetup.com/OpenStack-Hungary-Meetup-Group/events/222834045/" target="_blank">OpenStack CEE Day 2015</a>: June 8, 2015; Budapest, Hungary.</li>
<li><a href="http://www.meetup.com/Openstack-London/events/222985068/" target="_blank">Meet the Foundation with OpenStack London</a>: June 9, 2015; London, UK.</li>
<li><a href="http://www.meetup.com/Boston-Devops/events/222822041/" target="_blank">DevOps alignment in the enterprise</a>: June 10, 2015; Boston, MA.</li>
</ul><p><em style="font-size: 13.0080003738403px; line-height: 1.538em; background-color: transparent;">Interested in the OpenStack project development meetings? A complete list of these meetings' regular schedules is available <a href="https://wiki.openstack.org/wiki/Meetings" target="_blank" title="OpenStack Meetings">here</a>.</em></p>
