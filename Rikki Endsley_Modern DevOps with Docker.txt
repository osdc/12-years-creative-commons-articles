<p class="p1">Is <a href="https://opensource.com/resources/what-docker" target="_blank">Docker</a> production-ready?</p>
<!--break-->
<p class="p1"><img class="media-element file-default" data-file_info="%7B%22fid%22:%22278586%22,%22view_mode%22:%22default%22,%22fields%22:%7B%22format%22:%22default%22,%22field_file_image_alt_text%5Bund%5D%5B0%5D%5Bvalue%5D%22:%22%22,%22field_file_image_title_text%5Bund%5D%5B0%5D%5Bvalue%5D%22:%22%22,%22field_file_image_caption%5Bund%5D%5B0%5D%5Bvalue%5D%22:%22%22,%22field_file_image_caption%5Bund%5D%5B0%5D%5Bformat%5D%22:%22panopoly_wysiwyg_text%22,%22field_folder%5Bund%5D%22:%229387%22%7D,%22type%22:%22media%22%7D" height="150" src="https://opensource.com/sites/default/files/cavale-photo.jpg" style="float: right; margin: 8px;" width="114" />According to <a href="https://twitter.com/avinci" style="font-size: 13.0080003738403px; line-height: 1.538em; background-color: transparent;" target="_blank">Avi Cavale</a>, Docker not only is ready, his company already runs thousands of Docker containers in production per week. Cavale is the co-founder and CEO of <a href="http://www.shippable.com/" style="font-size: 13.0080003738403px; line-height: 1.538em; background-color: transparent;" target="_blank">Shippable</a>, a containerized continuous integration (CI) platform, and he says that Docker provides opportunities to radically accelerate how DevOps is "re-engineering the corporation" for IT.</p>
<p class="p1">In this interview in our <a href="https://opensource.com/resources/speaker-interview-series-apachecon-2015" style="font-size: 13.0080003738403px; line-height: 1.538em; background-color: transparent;" target="_blank">ApacheCon North America series</a>, Cavale looks at how Docker fits into DevOps. He also explains how continuous integration and continuous development are changing the way developers work, and why Docker is a particularly good fit for CI/CD applications.</p>
<p class="p1"><img alt="" class="media-element file-default" data-file_info="%7B%22fid%22:%2215378%22,%22view_mode%22:%22default%22,%22fields%22:%7B%22format%22:%22default%22,%22field_file_image_alt_text%5Bund%5D%5B0%5D%5Bvalue%5D%22:%22%22,%22field_file_image_title_text%5Bund%5D%5B0%5D%5Bvalue%5D%22:%22%22,%22field_file_image_caption%5Bund%5D%5B0%5D%5Bvalue%5D%22:%22%3C/p%3E%3Cp%3Eopensource.com%3C/p%3E%22,%22field_file_image_caption%5Bund%5D%5B0%5D%5Bformat%5D%22:%22panopoly_wysiwyg_text%22,%22field_folder%5Bund%5D%22:%229397%22%7D,%22type%22:%22media%22%7D" height="88" src="https://opensource.com/sites/default/files/images/life/Interview%20banner%20Q%26A.png" title="" width="520" /></p>
<h3 class="p2">For those not familiar with continuous integration/continuous development, how would you describe it? How are these processes changing the way developers are creating and developing software?</h3>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More DevOps resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/resources/devops?intcmp=7013a00000263HlAAI">What is DevOps?</a></li>
<li><a href="https://opensource.com/downloads/devops-hiring-guide?intcmp=7013a00000263HlAAI">The ultimate DevOps hiring guide</a></li>
<li><a href="https://opensource.com/downloads/devops-monitoring-guide?intcmp=7013a00000263HlAAI">DevOps monitoring tools guide</a></li>
<li><a href="https://opensource.com/downloads/devsecops?intcmp=7013a00000263HlAAI">Getting started with DevSecOps</a></li>
<li><a href="https://enterprisersproject.com/cheat-sheet-devops-glossary?intcmp=7013a00000263HlAAI">Download the DevOps glossary</a></li>
<li><a href="https://www.ansible.com/resources/ebooks/ansible-for-devops?intcmp=7013a00000263HlAAI">eBook: Ansible for DevOps</a></li>
<li><a href="https://opensource.com/tags/devops?src=devops_resource_menu2">Latest DevOps articles</a></li>
</ul></div>
</div>
</div>
</div>

<p>Continuous Integration is a development practice that requires developers to integrate code into a shared repository several times a day. Each check-in is then verified by an automated build, allowing teams to detect problems early. Continuous Delivery (CD) is a software engineering approach in which teams keep producing valuable software in short cycles and ensure that the software can be reliably released at any time. It is used in software development to automate and improve the process of software delivery. Automating these processes improves the efficiency of software development and deployment, helping companies to reduce costs and prevent errors.</p>
<p class="p2">The most significant changes we’ve seen in how these automated processes have changed the way developers are creating and developing software are:</p>
<ul><li>Developers develop and release in smaller increments since the overhead of merging code into the master branch is almost eliminated with automated CI/CD triggered on every check-in.</li>
<li>Less time is spent trying to determine whose code "broke the build" as each individual check-in is evaluated immediately. As a result, teams have eliminated the practice of running a single integration build once per week and then performing time-consuming investigations to debug which code introduced errors.</li>
<li>Debugging takes less time as developers receive immediate feedback on code they just worked on, while the details are fresh in their minds. Whenever a delay occurs in discovering bugs, debugging takes longer as important details may have been forgotten and the developer must ramp-up on the code again.</li>
</ul><h3 class="p2">What makes Docker a particularly good fit for CI/CD applications?</h3>
<p class="p2">CI/CD applications are a particularly good fit for Docker for several reasons:</p>
<ul class="ul1"><li class="li2">Docker is great for transient workloads, since you can spin up and shut down a container in seconds—a fraction of the time of provisioning a VM. This makes Docker tailor-made for CI processes such as build/CI validation cycles that initiate each time code is checked in. With Docker, each of these cycles can be initiated on-demand in its own container, executed, and then have the container shut down, freeing the resources. This proves to be a much more efficient model than using VMs for this purpose (Shippable reduced its Dev/Test environment costs by 70 percent when converting from VMs to containers).</li>
<li class="li2">Within a container, all details of an application live together—i.e. the code and the environment in which it will run—making it easy to fully replicate the environment that a developer’s code will run in. This solves the “but, it worked on my machine” problem where code worked on a developer’s local machine, but didn’t work in one of the subsequent environments, i.e. Test or Prod.</li>
<li class="li2">Once an application image is defined, it can easily be stored and reused anywhere that has Docker installed, making it easy to eliminate time-consuming tasks developers often go through to create their environments locally. These images can also be picked up by IT operations for use in deployment activities.</li>
<li class="li2">A good CI/CD system will also enable caching of those images, so spinning up an environment for a developer can happen quickly and painlessly for the developer.</li>
</ul><h3>Do you see Docker replacing traditional virtualization in the CI/CD space, or is it more about adding new capabilities and speeding up the process?</h3>
<p>VMs won’t be eliminated in the short-term, but in the long-term we see container-based virtualization fully replacing VMs.</p>
<h3>What role do Apache projects play at Shippable? And how are you and/or Shippable involved with the Apache Foundation?</h3>
<p class="p2">Shippable has only recently begun getting involved with Apache. Of primary interest for us currently is the Kafka project. We’ve begun evaluating it in an internal POC to see if it is a viable replacement for RabbitMQ, which we run for our messaging service. The driver for this is Kafka’s scalability.</p>
<h3 class="p2">Without giving too much away, what can attendees expect to learn in your ApacheCon talk, "Modern DevOps with Docker in 2015?"</h3>
<ul class="ul1"><li class="li2">Docker is ready for production—Shippable runs more than 25 thousand containers in production per week</li>
<li class="li2">Key elements of the DevOps lifecycle completely transform with the use of containers allowing for significant re-engineering of the lifecycle. DevOps is "re-engineering the corporation" for IT, and Docker provides opportunities to radically accelerate this.</li>
<li class="li2">With modern DevOps, you should be deploying to production multiple times a day. We’ll explain how Shippable deploys 10-40 times per day.</li>
</ul><p class="p2">To learn more about modern devops with Docker, attend <a href="http://sched.co/2P9d" target="_blank">Cavale's talk</a> at ApacheCon in Austin. </p>
<div class="series-wide">ApacheCon 2015<br />
Speaker Interview</div>
<p><em>This article is part of the <a href="https://opensource.com/resources/speaker-interview-series-apachecon-2015" target="_blank">Speaker Interview Series</a> for ApacheCon 2015. <a href="http://events.linuxfoundation.org/events/apachecon-north-america" target="_blank">ApacheCon North America</a> brings together the open source community to learn about the technologies and projects driving the future of open source and more. The conference takes place in Austin, TX from April 13-16, 2015</em>.</p>
