<p>There's a notion in <a href="https://opensource.com/resources/devops">DevOps</a> that our work begins when we understand the strategic business goals that we're trying to meet, then we deliver on them. This is typically a two-step process where one team creates goals, then hands them off to another team to implement them.</p>
<p>What would happen if, instead of a thinking of this as two-step process, we thought of strategy and implementation as a single-flow, continuous learning cycle?</p>
<p>The magic of flawless delivery on a perfect business strategy doesn't stem from setting the strategy at the top, divvying it into pieces, then providing performance bonuses as a reward for the folks who delivered on those pieces (and possibly punishing those who did not). This is a common view; however, if you stop to think about your own projects, you'll see that it's quite misguided.</p>
<p>Putting your systems-thinking, non-fragile hats on, you'll see that companies are actually complex networks where strategy absolutely needs to allow emergent activities. The alternative view is that companies are hierarchies where orders from the top are handed off through the silos, leaving far more points where failure is possible.</p>
<h2>Don't separate development from delivery</h2>
<p>Business strategy and implementation should be an interconnected continuum of feedback loops with the goal of fast learning, rather than separate activities. Reasons why include:</p><div class="embedded-callout-text callout-float-right">Business strategy and implementation should be an interconnected continuum of feedback loops with the goal of fast learning, rather than separate activities. </div>
<ul><li><strong>The market isn't slowing down: </strong>Unless we have constant checks against what competitors are doing and what is delighting our customers, our strategy is doomed—<em>even if we've already delivered on it.</em></li>
<li><strong>The strategic plan itself: </strong>Focusing on delivering to the plan isn't as valuable as learning from the implementation. Instead, focus on learning and iterating on the business goals/strategic plans themselves. Your experience of delivery will improve any business strategy. An imperfect strategy that the organizations can learn from is much better than managing to a set-in-stone strategic plan.</li>
<li><strong>Don't do the Band-Aid thing</strong>: If delivery is not an essential part of developing your business goals, it becomes the bandage at the end, an afterthought, which is then handed to a delivery team to make good on.</li>
</ul><h2>5 ways to start iterating on the business strategy</h2>
<p>Are you ready to begin integrating business strategy development and implementation? Try these tactics:</p>
<ol><li><strong>Encourage interaction</strong> between those who are creating the business strategy and its associated goals, and those who are delivering on said goals. These interactions will cause evolution and improvement of the strategy itself.</li>
<li><strong>Co-create the strategy: </strong>Involving cross-functional teams to co-create and prototype the business strategy and opportunities generates buy-in, guarantees involvement, and allows the rapid feedback cycle to begin. Co-creation sessions are an excellent way to create this environment; see below for more on how to set one up.</li>
<li><strong>Make the feedback loops big and visible: </strong>To create a rapid-response, agile delivery system, you'll need to know how the strategy and implementation are adjusted to exploit the risks and opportunities your experiments uncover. Take over a wall space and do a block-arrow diagram sharing such an experiment. Make note of how long the whole feedback cycle took—the total time for a learning to happen while implementing on a strategic business goal and perhaps tweaking the original strategy—and see if the feedback cycle could be improved.</li>
<li><strong>Reward experiments:</strong> When we're delivering on a business goal, perhaps even implementing on a new business strategy, there will be challenges. Ask your teams what these might be. Discuss them. Invite them to run safe-to-fail experiments. Discuss these challenges openly and support your teams to adjust plans as needed. Hopefully some of these experiments will fail; failures offer huge learnings. Reward these failed experiments as valuable input to a possibly flawed strategic plan.</li>
<li><strong>Look outside your company: </strong>Explore open source communities to see what happens outside your company walls, why/how/what other folks are doing to iterate on their business strategies, fail, and learn fast. Who else might be involved in adjusting strategy and its implementation? Customers? Suppliers? Stakeholders?</li>
</ol><h2>How a co-creation session works</h2>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More DevOps resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/resources/devops?intcmp=7013a00000263HlAAI">What is DevOps?</a></li>
<li><a href="https://opensource.com/downloads/devops-hiring-guide?intcmp=7013a00000263HlAAI">The ultimate DevOps hiring guide</a></li>
<li><a href="https://opensource.com/downloads/devops-monitoring-guide?intcmp=7013a00000263HlAAI">DevOps monitoring tools guide</a></li>
<li><a href="https://opensource.com/downloads/devsecops?intcmp=7013a00000263HlAAI">Getting started with DevSecOps</a></li>
<li><a href="https://enterprisersproject.com/cheat-sheet-devops-glossary?intcmp=7013a00000263HlAAI">Download the DevOps glossary</a></li>
<li><a href="https://www.ansible.com/resources/ebooks/ansible-for-devops?intcmp=7013a00000263HlAAI">eBook: Ansible for DevOps</a></li>
<li><a href="https://opensource.com/tags/devops?src=devops_resource_menu2">Latest DevOps articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>Are you ready to set up a co-creation session to work on your business strategy and implementation plan? First, book a large room with enough round tables and chairs for the people who will participate. Divide up the entire group into cross-functional teams, comprising the most diverse, soup-to-nuts perspectives possible, seated at each table.</p>
<p>Start by asking the marketing and sales people to deliver a 15-minute pitch on the strategic opportunity, including the customer's key pains you need to solve.</p>
<p>Next, give the cross-functional teams 15 minutes to rapid prototyping by answering the questions: "How did you interpret this strategy?" and "What are the opportunities you see?" If you have five table teams, you will get five different interpretations of the business strategy and more than five different opportunities you may not have considered. Do one more round of iterating on these prototypes, telling each team they will present their strategy-plus-opportunity prototype, including a rough cost-benefit estimate, their top key assumptions, and top risks, to the entire group.</p>
<p>After the teams present their final round of prototyping, allow two minutes for structured feedback per table team (I'm a big fan of design thinking's <a href="https://dschool-old.stanford.edu/sandbox/groups/dstudio/wiki/2fced/attachments/1ba97/Feedback-Capture-Grid-Method.pdf?sessionID=d07c198d92501ebb3eee4ff3da193b387130fcbf" target="_blank">four-quadrant feedback grid</a>). Then provide two minutes for questions per table team, and end the entire co-creation session with a 15-minute retrospective on how the co-creation session could be improved next time. The entire session should take less than two hours.</p>
<p><strong>A word of advice:</strong> Giving each table team the space to present their rough view of cost-benefits, key assumptions, and risks is important. A real-life example of why this is so important comes from a co-creation strategy session in which the sales and marketing folks pitched a strategy to be the best healthcare supplier for business X, offering distinct goals to win the business from a competitor and making clear the customer's key pain points. When the table teams began rapid prototyping their interpretation of the strategy along with opportunities, one team focused on using open source programs and procedures as the "backbone" for databasing and analyzing the data produced for a key healthcare project. Another team focused on using a big data supplier's (expensive) version of the same type of solution. The initial implementation went to the big data supplier, however a business decision was made eight months later to switch to the open source option. I'm certain that if they had done a really rough cost-benefit analysis, key assumptions and risks would have been uncovered during the co-creation session and produced different results.</p>
<h2>Example timeline and agenda</h2>
<p>Book two hours as a buffer for your co-creation session, and keep to a strict schedule. Supply the room with low-fidelity prototyping tools: paper, modeling tools, role-play props, etc.</p>
<p></p><div class="embedded-callout-text callout-float-right"> Book two hours as a buffer for your co-creation session, and keep to a strict schedule.</div> Because timeboxing is critical, here's an example schedule you can use as a model:
<ul><li>5 minutes: Explain the logistics and schedule</li>
<li>15 minutes: Sales and marketing pitch on strategy and opportunity</li>
<li>15 minutes: Round 1 rapid prototyping: suggested breakdown of the 15 minutes</li>
<li class="rteindent1">1 minutes: decide what to build</li>
<li class="rteindent1">6 minutes: build</li>
<li class="rteindent1">2 minutes: get feedback</li>
<li class="rteindent1">6 minutes: build</li>
<li>15 minutes: Share prototypes, look for examples (i.e., 3 minutes per table team, assuming 5 teams)</li>
<li>2 minutes: Explain Round 2: Facilitator sets goals for Round 2 by presenting one example of a prototype including a rough cost-benefit estimate, listing key assumptions and risks</li>
<li>15 minutes: Round 2 rapid prototyping: Iterate on the first prototype, adding advantages heard from the previous session, adding rough cost-benefit, assumptions and risk</li>
<li>20 minutes: Structured feedback with Q&amp;A (i.e. 4 minutes per table team, assuming 5 teams)</li>
<li>15 minutes: Retrospective on the session itself</li>
</ul><p>A co-creation session is an excellent way to get feedback and ideas on your business strategy in a quick and productive way. Early in this article, I warned about problems that can come from hand-offs, so after you've been through the co-creation process, think about how your feedback loops could expand to avoid other needless hand-offs.</p>
<p>Have you run a co-creation session to integrate strategy development and implementation? I would love to hear your experience in the comments below!</p>
