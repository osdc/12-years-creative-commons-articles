<p>I took a break in December from the Top 5 to refresh and reflect. It was great, but I'm excited to be back with you. So, in this week's edition, the first of 2017, I bring you the 5 articles from the past three weeks (December 16 - January 6) that have topped the charts with readers.</p>
<p><iframe allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/SrGbek-VeFU" width="560"></iframe></p>
<h2>Top 5 articles of the week</h2>
<p><strong>5. <a href="https://opensource.com/article/16/12/yearbook-top-open-source-creative-tools-2016">Top open source creative tools in 2016</a></strong></p>
<p>Máirín Duffy is a principal interaction designer at Red Hat. She tells us about her favorite open source tools to manipulate images, edit audio, and animate stories.</p>
<p><strong>4. <a href="https://opensource.com/article/16/12/yearbook-top-10-open-source-projects">Top 10 open source projects of 2016</a></strong></p>
<p>In our annual list of the year's top open source projects, we look back at popular projects our writers covered in 2016, plus favorites our Community Moderators picked.</p>
<p><strong>3. <a href="https://opensource.com/article/17/1/yearbook-4-hot-skills-linux-pros-2017">4 hot skills for Linux pros in 2017</a></strong></p>
<p>Shawn Powers of CBT Nuggets tells us that the definition of a Linux expert is constantly changing, so in this article he shares what he thinks are the four vital skills for a Linux pro today.</p>
<p><strong>2. <a href="https://opensource.com/article/16/12/yearbook-top-programming-trends-2016">Hot programming trends in 2016</a></strong></p>
<p>Get an overview of the year's hottest languages for AI projects and containers, new languages, and more programming trends.</p>
<p><strong>1. <a href="https://opensource.com/article/16/12/yearbook-linux-test-driving-distros">How Linux got to be Linux: Test driving 1993-2003 distros</a></strong></p>
<p>Seth Kenlon revisits the early release of 7 Linux distros to get a picture of what progress Linux has made over the years.</p>
<h2>Honorable mention</h2>
<p><strong><a href="https://opensource.com/article/16/12/yearbook-50-ways-avoid-getting-hacked">50 ways to avoid getting hacked in 2017</a></strong></p>
<p>Dan Walsh works on SELinux at Red Hat. In this article, he shares 50 ways sysadmins and laypeople can avoid getting hacked.</p>
