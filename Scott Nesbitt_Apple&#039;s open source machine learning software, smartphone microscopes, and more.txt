<p>
  In this edition of our open source news roundup, we take a look at Apple's open source AI framework, an open source smartphone microscope, and more.
</p>
<p></p><center><strong>Open source news roundup for December 10-23, 2017</strong></center>
<h2>Apple open sources machine learning software</h2>
<p>
  It was a little over a year ago that Apple bought a machine learning startup called Turi. Since then, the Cupertino-based tech giant has been quietly continuing work on Turi's software. Earlier this month, Apple released some of the results of that labor by open sourcing the <a href="https://www.datanami.com/2017/12/11/apple-releases-turi-ml-software-open-source/" target="_blank">Turi Create machine learning software</a>.
</p>
<p>
  Apple states that with Turi Create, mobile app developers can "focus on tasks instead of algorithms" and easily "infuse machine learning into their products with just a few lines of code." That includes adding "recommendations, object detection, image classification, image similarity, or activity classification" to an app.
</p>
<p>
  If you're interested, you can peruse the source code <a href="https://github.com/apple/turicreate" target="_blank">in the Turi Create GitHub repository</a>.
</p>
<h2>Open source attachment turns a smartphone into a microscope</h2>
<p>
  For anyone interested or working in biology (or one of its related fields), a microscope is a key piece of scientific gear. If you wanted to go into the field, you either had to haul samples back to the lab or luug a bulky and fragile microscope around with you. Not any more, thanks to a <a href="https://www.osa-opn.org/home/newsroom/2017/december/an_open-source_smartphone_microscope/" target="_blank">3D printed smartphone microscope attachment</a> developed by a team at the University of Houston.
</p>
<p>
  The attachment is aimed at "researchers and healthcare providers in developing and rural areas, as well as hobbyists and backpackers." It fits over a smartphone camera's lens and has its own light source. Wei-Chuan Shih, who heads the University of Houston team, said it was a simple decision to <a href="https://figshare.com/articles/_/5313643" target="_blank">open source the attachment's design</a>. The attachment will, Shih said, "have more impact if we let people play with it, rather than trying to hold it as a secret. We should make it as easy and accessible as possible for everyone."
</p>
<h2>Bell Canada puts open source network platform into production</h2>
<p>
  Canadian communications giant Bell Canada became the <a href="https://www.sdxcentral.com/articles/news/bell-canada-first-to-deploy-open-source-onap-in-production/2017/12/" target="_blank">first telecom firm</a> to "deploy an open source version of the Open Network Automation Platform (ONAP) in a production environment." It's a significant milestone because it's only been nine months since AT&amp;T and the Linux Foundation <a href="https://www.sdxcentral.com/articles/news/logical-happens-open-o-merges-ecomp/2017/02/" target="_blank">joined forced to form ONAP</a>.
</p>
<p>
  Bell Canada is using ONAP to "automate its data center tenant network provisioning" and to "support deployments, reduce its operational footprint, and enable continuous delivery." According to Arpit Joshipura of the Linux Foundation, Bell Canada's ONAP deployment "sends a clear message to the industry that ONAP is ready and usable."
</p>
<h2>Code for Greenland's public records system to go open source</h2>
<p>
  The government of Greenland announced that it will be <a href="https://joinup.ec.europa.eu/news/shared-ready-reuse" target="_blank">making its new public record system available as open source</a> in early 2018.
</p>
<p>
  The system, called <a href="http://digitalimik.gl/da/Losninger-og-infrastruktur/Grunddata-ny" target="_blank">Grunddata</a>, "shares data about people, companies, properties and addresses between public services, companies and citizens." The software was developed as an alternative to a similar system from Denmark, which Greenland considered adopting. However, delays and rising costs nixed that idea. According to Morten Kjærsgaard, CEO of Magenta (the firm that coded and is implementing the system), the code for Grunddata will be posted to GitHub.
</p>
<h3>In other news</h3>
<ul><li><a href="https://www.theguardian.com/science/political-science/2016/dec/16/could-the-best-way-to-make-money-from-science-be-to-give-it-away-for-free" target="_blank">Could the best way to make money from science be to give it away for free?</a></li>
<li><a href="https://www.businesslive.co.za/bd/companies/telecoms-and-technology/2017-12-12-open-source-software-is-everywhere-and-is-defining-innovation-says-mark-shuttleworth/" target="_blank">Open source is everywhere and defining innovation, says Mark Shuttleworth</a></li>
<li><a href="https://betanews.com/2017/12/13/avast-retdec-open-source-machine-code-decompiler/" target="_blank">Avast open sources RetDec machine code decompiler</a></li>
<li><a href="https://www.linuxfoundation.org/blog/what-open-means-openstack/" target="_blank">What open means to OpenStack</a></li>
<li><a href="https://www.linuxinsider.com/story/85005.html" target="_blank">New open source tool checks for VPN leaks</a></li>
</ul><p>
  <em>Thanks, as always, to Opensource.com staff members and moderators for their help this week. Make sure to check out <a href="https://opensource.com/resources/conferences-and-events-monthly">our event calendar</a>, to see what's happening next week in open source.</em>
</p>
