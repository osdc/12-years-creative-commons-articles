<p>Creative Commons, the U.S. Department of Education, and the Open Society Foundations are offering up to $25,000 in cash prizes to "the best short videos that explain the use and promise of free,   high-quality open educational resources and describe the benefits and   opportunities these materials create for teachers, students and  schools".</p>
<!--break-->
<p>Submissions are due June 5, 2012. For more information you can read the <a title="press release" href="http://creativecommons.org/weblog/entry/31615">press release</a> or visit their <a title="website" href="http://whyopenedmatters.org">website</a>.</p>
<p>Creative friends of open source: get busy!</p>
<p> </p>
<p> </p>
