<p>The Linux desktop is a beautiful thing, but if you're tired of boring wallpaper, then you should try <a href="https://github.com/terroo/wallset">wallset</a>, a command-line utility allowing you to set a video as your wallpaper. Wallset can also help you manage your wallpaper collection so you can conveniently make changes as often as you want.</p>
<h2>Installation</h2>
<p>First, you must have the following software installed on your system:</p>
<ul><li><a href="https://ffmpeg.org/" target="_blank">ffmpeg &gt;=4.2.3</a></li>
<li><a href="https://feh.finalrewind.org/" target="_blank">feh &gt;=3.4.1</a></li>
<li><a href="https://www.imagemagick.org/" target="_blank">imagemagick &gt;=7.0.10.16</a></li>
<li><a href="https://gitlab.freedesktop.org/xorg/app/xrandr" target="_blank">xrandr &gt;=1.5.1</a></li>
<li><a href="https://www.freedesktop.org/wiki/Software/xdg-utils/" target="_blank">xdg-utils &gt;=1.1.3</a></li>
</ul><p>After that, you need <a href="https://git-scm.com/" target="_blank">Git</a> to clone the repository and then install it. To do this, run the commands below:</p>
<pre><code class="language-bash">$ git clone https://github.com/terroo/wallset wallset.clone
$ cd wallset.clone
$ ./install.sh
</code></pre><p>Enter your <code>sudo</code> password to complete the installation.</p>
<h2>Usage</h2>
<p>For quick help with the commands, run the <code>wallset</code> command with the <code>--help</code> parameter, or just <code>-h</code> for short.</p>
<p>The first necessary task is to add images. Without images added to wallset, you won't have anything to choose from when setting your wallpaper. To add images, run this:</p>
<pre><code class="language-bash">$ wallset --add image.jpg
</code></pre><p>You can also add multiple images at once:</p>
<pre><code class="language-bash">$ wallset --add image-1.jpg image-2.jpg image-3.jpg
</code></pre><p>If it is in a directory with several images, you can also add it like this:</p>
<pre><code class="language-bash">$ wallset --add `ls *.jpg *.png`
</code></pre><h3>Setting a wallpaper</h3>
<p>Wallpapers are saved and indexed:</p>
<pre><code class="language-bash">$ wallset --use 001
</code></pre><p>If you want to set your current wallpaper to the image you're adding, use the <code>--set</code> parameter:</p>
<pre><code class="language-bash">$ wallset --set --add imagem.png
</code></pre><p>When you add content to wallpaper, a copy of the file is made for wallset to use, so wallset won't break when you move your copy of an image.</p>
<h3>List wallpapers</h3>
<p>Each time you add an image to wallset, it's indexed with a three-digit number (001, 002, 003, and so on). Obviously, if you have several images, it gets difficult to remember these, so you can see a list of all images you've added with the <code>--count</code> option:</p>
<pre><code class="language-bash">$ wallset --count
</code></pre><p>You can also browse the images in wallset with the <code>--display</code> option:</p>
<pre><code class="language-bash">$ wallset --display
</code></pre><p>You can get the number assigned to your current wallpaper using the <code>--show</code> option:</p>
<pre><code class="language-bash">$ wallset --show
</code></pre><h3>Removing images</h3>
<p>You can remove the last image added:</p>
<pre><code class="language-bash">$ wallset --remove
</code></pre><p>This removes the image from wallset.</p>
<h3>Looping images</h3>
<p>If you love change, you can loop through all images in wallset, so your wallpaper changes at whatever interval you set. To do this, use the <code>--time</code> option, providing some number of seconds as the argument. For example, should you want your wallpaper to change every hour:</p>
<pre><code class="language-bash">$ wallset --quit
$ wallset --time 3600
</code></pre><h3>Adding video as wallpaper</h3>
<p>One of the most interesting features of wallset is that you can add videos as your wallpaper. To do this, run the command:</p>
<pre><code class="language-bash">$ wallset --video /path/to/your-video.mp4
</code></pre><p>To stop a video, the procedure is the same as the image loop: use <code>--quit</code> (or just<code>-q</code> for short).</p>
<p>When you quit, the video image is paused or frozen, and the current frame becomes your current wallpaper. If you want to change that, use the <code>--use [number]</code> parameter to set a new one.</p>
<p>After using a video once, it is automatically added to the wallset video index. To list all videos, use the <code>--list-videos</code> option:</p>
<pre><code class="language-bash">$ wallset --list-videos
</code></pre><p>If you want to use a video in your video directory, use the <code>--set-video</code> parameter, and then enter the number displayed when listing the videos.</p>
<p>For example:</p>
<pre><code class="language-bash">$ wallset --set-video 1
</code></pre><h1 id="comments">Uninstall</h1>
<p>Wallset is a relatively new script and is largely intended for window managers such as <a href="https://github.com/baskerville/bspwm" target="_blank">bspwm</a>, <a href="https://opensource.com/article/19/12/fluxbox-linux-desktop">Fluxbox</a>, <a href="https://opensource.com/article/19/12/openbox-linux-desktop">Openbox</a>, <a href="https://opensource.com/article/19/12/ratpoison-linux-desktop">Ratpoison</a>, and similar. Feel free to report bugs at <a href="https://github.com/terroo/wallset/issues" target="_blank">https://github.com/terroo/wallset/issues</a>.</p>
<p>If you want to uninstall <code>wallset</code>, use the installer script with the <code>uninstall</code> parameter:</p>
<pre><code class="language-bash">$ cd ~/wallset.clone
$ ./install.sh uninstall
</code></pre><p>During the uninstallation, the copies of images and videos added to wallset are also deleted.</p>
<h2 id="demo">Demo</h2>
<p>I've created a <a href="https://youtu.be/Mb0SXMft2sw" target="_blank">video showing the main features</a> of the program and using the <a href="https://github.com/terroo/wallset/tree/master/examples" target="_blank">examples</a> that are part of the <a href="https://github.com/terroo/wallset/tree/master/examples" target="_blank">repository</a> directory itself.</p>
<p>The video is in Brazilian Portuguese, but the commands and visuals are universal.</p>
