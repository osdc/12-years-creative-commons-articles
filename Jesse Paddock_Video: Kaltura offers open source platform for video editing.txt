<p>This past summer at the first annual <a href="http://openvideoalliance.org/open-video-conference/?l=en">Open Video Conference</a>, there was a lot of buzz surrounding <a href="http://www.kaltura.org/">Kaltura</a>, a software that enables people to edit, host and publish their own videos, as well as make use of re-mixable <a href="http://creativecommons.org/">Creative Commons</a> content. We had an opportunity to sit down with Ron Yekutiel, Kaltura's CEO, at the event and discuss why the web needs an open source video platform so badly.</p>
<!--break-->
<p><iframe width="560" height="315" src="https://www.youtube.com/embed/R5OLFSA7ny4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe></p>
<p>(Update: Download an ogg of this video <a href="http://www.redhat.com/v/ogg/kaltura-osdc.ogg">here</a>.)</p>
<p>For the record, it was Ron's idea to wear the red hat in the video.</p>
