<p><i>Automation is a hot topic right now. In my day job as an SRE part of my remit is to automate as many repeating tasks as possible. But how many of us do that in our daily, not-work, lives? This year, I am focused on automating away the toil so that we can focus on the things that are important. </i></p>
<p>Server-side mail rules are one of the most efficient ways to pre-sort and filter mail. Sadly, Gmail, the most popular mail service in the world, doesn't use any of the standard protocols to allow users to manage their rules. Adding, editing, or removing a single rule can be a time-consuming task in the web interface, depending on how many rules the user has in place. The options for editing them "out of band" as provided by the company are limited to an XML export and import.</p>
<p>I have 109 mail filters, so I know what a chore it can be to manage them using the provided methods. At least until I discovered <a href="https://github.com/mbrt/gmailctl">gmailctl</a>, the command-line tool for managing Gmail filters with a (relatively) simple standards-based configuration file.</p>
<pre>
<code class="language-bash">$ gmailctl test
$ gmailctl diff
Filters:
--- Current
+++ TO BE APPLIED
@@ -1 +1,6 @@
+* Criteria:
+ from: @opensource.com 
+ Actions: 
+ mark as important 
+ never mark as spam

$ gmailctl apply
You are going to apply the following changes to your settings:
Filters:
--- Current
+++ TO BE APPLIED
@@ -1 +1,6 @@
+* Criteria: 
+ from: @opensource.com 
+ Actions: 
+ mark as important 
+ never mark as spam
Do you want to apply them? [y/N]: </code></pre><p>To define rules in a flexible manner <code>gmailctl </code>uses the <a href="https://jsonnet.org/">jsonnet</a> templating language. Using <code>gmailctl</code> also allows the user to export the existing rules for modification.</p>
<p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">More Linux resources</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux commands cheat sheet">Linux commands cheat sheet</a></div>
              <div class="field__item"><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Advanced Linux commands cheat sheet">Advanced Linux commands cheat sheet</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Free online course: RHEL technical overview">Free online course: RHEL technical overview</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux networking cheat sheet">Linux networking cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="SELinux cheat sheet">SELinux cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux common commands cheat sheet">Linux common commands cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="What are Linux containers?">What are Linux containers?</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Register for your free Red Hat account">Register for your free Red Hat account</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Our latest Linux articles">Our latest Linux articles</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<p>To get started, install <code>gmailctl</code> via your system's package manager, or install from source with <code>go install github.com/mbrt/gmailctl/cmd/gmailctl@latest</code>. Follow that with <code>gmailctl init</code> which will walk you through the process of setting up your credentials and the correct permissions in Google. If you already have rules in Gmail, I recommend running <code>gmailctl download</code> next, in order to backup the existing rules. These will be saved in the default configuration file <code>~/.gmailctl/config.jsonnet</code>. Copy that file somewhere safe for future reference, or to restore your old rules just in case!</p>
<p>If you wish to start from a clean slate, or you don't have any rules yet, you need to create a new, empty <code>~/.gmailctl/config.jsonnet</code> file. The most basic structure for this file is:</p>
<pre>
<code class="language-bash">local lib = import 'gmailctl.libsonnet';
{
  version: "v1alpha3",
  author: {
    name: "OSDC User",
    email: "your-email@gmail.com"
  },
  rules: [
    {
      filter: {
        or: [
          { from: "@opensource.com" },
        ]
      },
      actions: {
        markRead: false,
        markSpam: false,
        markImportant: true
      },
    },
  ]
}</code></pre><p>As you can see, this file format is similar to, but not as strict as <code>JSON</code>. This file sets up a simple rule to mark any mail from <code>opensource.com</code> as important, leave it unread, and not mark it as spam. It does this by defining the criteria in the <code>filters</code> section, and then the rules to apply in the <code>actions</code> section. Actions include the following boolean commands: <code>markRead</code>, <code>markSpam</code>,<code>markImportant</code>, and <code>archive</code>. You can also use actions to specify a <code>category</code> for the mail, and assign folders, which we will get to later in the article.</p>
<p>Once the file is saved, the configuration file format can be verified with <code>gmailctl test</code>. If everything is good, then you can use <code>gmailctl diff</code> to view what changes are going to be made, and <code>gmailctl apply</code> to upload your new rule to Gmail.</p>
<pre>
<code class="language-bash">$ gmailctl diff
Filters:
---
Current
+++ TO BE APPLIED
@@ -1,6 +1,8 @@
* Criteria:
from: @opensource.com Actions:
+ archive
  mark as important
  never mark as spam 
+ apply label: 1-Projects/2022-OSDC

$ gmailctl apply -y 
You are going to apply the following changes to your settings:
Filters:
--- Current
+++ TO BE APPLIED 
@@ -1,6 +1,8 @@ 
* Criteria:
  from: @opensource.com Actions: 
+ archive
  mark as important 
  never mark as spam
  apply label: 1-Projects/2022-OSDC

Applying the changes...</code></pre><p>As mentioned previously, new mail messages can be auto-filed by setting labels in the configuration. I want to assign all mails from Opensource.com to a folder specifically for them, and remove them from the inbox (or <code>archive</code> in Gmail terms). To do that, I would change the <code>actions</code> section to be:</p>
<pre>
<code class="language-bash">  actions: {
        markRead: false,
        markSpam: false,
        markImportant: true,
        archive: true,
        labels: [
          "1-Projects/2022-OSDC"
        ]
      },</code></pre><p>As you can see in the image above, <code>gmailctl diff</code> now shows only what is going to change. To apply it, I used <code>gmailctl apply -y</code> to skip the confirmation prompt. If the label doesn't exist, then an error is given, since a filter cannot be made for a label that does not already exist.</p>
<p>You can also make more complex rules that target specific conditions or multiple emails. For example, the following rule uses an <code>and</code> condition to look for messages from <code>Cloudflare</code> that are not purchase confirmations.</p>
<pre>
<code class="language-json"> filter: {
        and: [
          { from: "noreply@notify.cloudflare.com" },
          { subject: "[cloudflare]" },
          { query: "-{Purchase Confirmation}" }
        ]
      },</code></pre><p>In the case of a rule that performs the same action on multiple messages, you can use an <code>or</code> structure. I use that to file all emails relating to tabletop games to a single folder.</p>
<pre>
<code class="language-json"> filter: {
        or: [
          { from: "no-reply@obsidianportal.com" },
          { from: "no-reply@roll20.net" },
          { from: "team@arcanegoods.com" },
          { from: "team@dndbeyond.com" },
          { from: "noreply@forge-vtt.com" },
          { from: "@elventower.com" },
          { from: "no-reply@dmsguild.com"},
          { from: "info@goodman-games.com" },
          { from: "contact@mg.ndhobbies.com" },
          { from: "@monkeyblooddesign.co.uk" },
        ]
      },</code></pre><p>
For people with multiple Gmail accounts that need their own sets of rules, you can specify a unique configuration file for them with the <code>--config</code> command line parameter. For example, my work uses Gmail, and I have a whole <em>other</em> set of rules for that. I can create a new <code>gmailctl</code> directory, and use that for the work configuration, like so:</p>
<pre>
<code class="language-bash">$ gmailctl --config ~/.gmailctl-work/ diff</code></pre><p>To make this easier on myself, I have two shell aliases to make it clear which configuration I'm using.</p>
<pre>
<code class="language-json">alias gmailctl-home="gmailctl --config $HOME/.gmailctl"
alias gmailctl-work="gmailctl --config $HOME/.gmailctl-work"</code></pre><p>The one drawback <code>gmailctl</code> has is that it will not apply a new filter to existing messages, so you still have to manually do things for mail received before doing <code>gmailctl apply</code>. I hope they are able to sort that out in the future. Other than that, <code>gmailctl</code> has allowed me to make adding and updating Gmail filters fast and almost completely automatic, and I can use my favorite email client without having to constantly go back to the web UI to change or update a filter.</p>
