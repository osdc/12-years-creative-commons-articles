<p>I've written, used, and seen a lot of loose scripts in my career. They start with someone that needs to semi-automate some task. After a while, they grow. They can change hands many times in their lifetime. I've often wished for a more command-line <em>tool-like</em> feeling in those scripts. But how hard is it really to bump the quality level from a one-off script to a proper tool? It turns out it's not that hard in Python.</p>
<h2>Scaffolding</h2>
<p>In this article, I start with a little Python snippet. I'll drop it into a <code>scaffold</code> module, and extend it with <code>click</code> to accept command-line arguments.</p>
<pre>
<code class="language-python">#!/usr/bin/python

from glob import glob
from os.path import join, basename
from shutil import move
from datetime import datetime
from os import link, unlink

LATEST = 'latest.txt'
ARCHIVE = '/Users/mark/archive'
INCOMING = '/Users/mark/incoming'
TPATTERN = '%Y-%m-%d'

def transmogrify_filename(fname):
    bname = basename(fname)
    ts = datetime.now().strftime(TPATTERN)
    return '-'.join([ts, bname])

def set_current_latest(file):
    latest = join(ARCHIVE, LATEST)
    try:
        unlink(latest)
    except:
        pass
    link(file, latest)

def rotate_file(source):
    target = join(ARCHIVE, transmogrify_filename(source))
    move(source, target)
    set_current_latest(target)

def rotoscope():
    file_no = 0
    folder = join(INCOMING, '*.txt')
    print(f'Looking in {INCOMING}')
    for file in glob(folder):
        rotate_file(file)
        print(f'Rotated: {file}')
        file_no = file_no + 1
    print(f'Total files rotated: {file_no}')

if __name__ == '__main__':
    print('This is rotoscope 0.4.1. Bleep, bloop.')
    rotoscope()</code></pre><p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">More Python resources</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://www.redhat.com/en/topics/middleware/what-is-ide?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="What is an IDE?">What is an IDE?</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-python-37-beginners?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Cheat sheet: Python 3.7 for beginners">Cheat sheet: Python 3.7 for beginners</a></div>
              <div class="field__item"><a href="https://opensource.com/resources/python/gui-frameworks?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Top Python GUI frameworks">Top Python GUI frameworks</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/7-essential-pypi-libraries?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Download: 7 essential PyPI libraries">Download: 7 essential PyPI libraries</a></div>
              <div class="field__item"><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Red Hat Developers">Red Hat Developers</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Register for your free Red Hat account">Register for your free Red Hat account</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/python?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Latest Python articles">Latest Python articles</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<p>All non-inline code samples in this article refer to a specific version of the code you can find at <a href="https://codeberg.org/ofosos/rotoscope">https://codeberg.org/ofosos/rotoscope</a>. Every commit in that repo describes some meaningful step in the course of this how-to article.</p>
<p>This snippet does a few things:</p>
<ul><li>Check whether there are any text files in the path specified in <code>INCOMING</code></li>
<li>If it exists, it creates a new filename with the current timestamp and moves the file to <code>ARCHIVE</code></li>
<li>Delete the current <code>ARCHIVE/latest.txt</code> link and create a new one pointing to the file just added</li>
</ul><p>As an example, this is pretty small, but it gives you an idea of the process.</p>
<h2>Create an application with pyscaffold</h2>
<p>First, you need to install the <code>scaffold</code>, <code>click</code>, and <a href="https://opensource.com/article/19/5/python-tox" target="_blank"><code>tox</code> Python modules</a>.</p>
<pre>
<code class="language-bash">$ python3 -m pip install scaffold click tox</code></pre><p>After installing <code>scaffold</code>, change to the directory where the example <code>rotoscope</code> project resides, and then execute the following command:</p>
<pre>
<code class="language-bash">$ putup rotoscope -p rotoscope \
--force --no-skeleton -n rotoscope \
-d 'Move some files around.' -l GLWT \
-u http://codeberg.org/ofosos/rotoscope \
--save-config --pre-commit --markdown
</code></pre><p>Pyscaffold overwrote my <code>README.md</code>, so restore it from Git:</p>
<pre>
<code class="language-bash">$ git checkout README.md</code></pre><p>Pyscaffold set up a complete sample project in the docs hierarchy, which I won't cover here but feel free to explore it later. Besides that, Pyscaffold can also provide you with continuous integration (CI) templates in your project.</p>
<pa includes:="" project="" pyscaffold=""><ul><li><strong>packaging</strong>: Your project is now PyPi enabled, so you can upload it to a repo and install it from there.</li>
<li><strong>documentation</strong>: Your project now has a complete docs folder hierarchy, based on Sphinx and including a readthedocs.org builder.</li>
<li><strong>testing</strong>: Your project can now be used with the tox test runner, and the tests folder contains all necessary boilerplate to run pytest-based tests.</li>
<li><strong>dependency management</strong>: Both the packaging and test infrastructure need a way to manage dependencies. The <code>setup.cfg</code> file solves this and includes dependencies.</li>
<li><strong>pre-commit hook</strong>: This includes the Python source formatter "black" and the "flake8" Python style checker.</li>
</ul><p>Take a look into the tests folder and run the <code>tox</code> command in the project directory. It immediately outputs an error. The packaging infrastructure cannot find your package.</p>
<p>Now create a Git tag (for instance, <code>v0.2</code>) that the tool recognizes as an installable version. Before committing the changes, take a pass through the auto-generated <code>setup.cfg</code> and edit it to suit your use case. For this example, you might adapt the <code>LICENSE</code> and project descriptions. Add those changes to Git's staging area, I have to commit them with the pre-commit hook disabled. Otherwise, I'd run into an error because flake8, Python style checker, complains about lousy style.</p>
<pre>
<code class="language-bash">$ PRE_COMMIT_ALLOW_NO_CONFIG=1 git commit </code></pre><p>It would also be nice to have an entry point into this script that users can call from the command line. Right now, you can only run it by finding the <code>.py</code> file and executing it manually. Fortunately, Python's packaging infrastructure has a nice "canned" way to make this an easy configuration change. Add the following to the <code>options.entry_points</code> section of your <code>setup.cfg</code>:</p>
<pre>
<code class="language-python">console_scripts =
    roto = rotoscope.rotoscope:rotoscope
</code></pre><p>This change creates a shell command called <code>roto</code>, which you can use to call the rotoscope script. Once you install rotoscope with <code>pip</code>, you can use the <code>roto</code> command.</p>
<p>That's that. You have all the packaging, testing, and documentation setup for free from Pyscaffold. You also got a pre-commit hook to keep you (mostly) honest.</p>
<h2>CLI tooling</h2>
<p>Right now, there are values hardcoded into the script that would be more convenient as command <a href="https://opensource.com/article/21/8/linux-terminal#argument" target="_blank">arguments</a>. The <code>INCOMING</code> constant, for instance, would be better as a command-line parameter.</p>
<p>First, import the <a href="https://click.palletsprojects.com" target="_blank">click</a> library. Annotate the <code>rotoscope()</code> method with the command annotation provided by Click, and add an argument that Click passes to the <code>rotoscope</code> function. Click provides a set of validators, so add a path validator to the argument. Click also conveniently uses the function's here-string as part of the command-line documentation. So you end up with the following method signature:</p>
<pre>
<code class="language-python">@click.command()
@click.argument('incoming', type=click.Path(exists=True))
def rotoscope(incoming):
    """
    Rotoscope 0.4 - Bleep, blooop.
    Simple sample that move files.
    """
</code></pre><p>The main section calls <code>rotoscope()</code>, which is now a Click command. It doesn't need to pass any parameters.</p>
<p>Options can get filled in automatically by <a href="https://opensource.com/article/19/8/what-are-environment-variables" target="_blank">environment variables</a>, too. For instance, change the <code>ARCHIVE</code> constant to an option:</p>
<pre>
<code class="language-python">@click.option('archive', '--archive', default='/Users/mark/archive', envvar='ROTO_ARCHIVE', type=click.Path())
</code></pre><p>The same path validator applies again. This time, let Click fill in the environment variable, defaulting to the old constant's value if nothing's provided by the environment.</p>
<p>Click can do many more things. It has colored console output, prompts, and subcommands that allow you to build complex CLI tools. Browsing through the Click documentation reveals more of its power.</p>
<p>Now add some tests to the mix.</p>
<h2>Testing</h2>
<p>Click has some advice on <a href="https://click.palletsprojects.com/en/8.1.x/testing" target="_blank">running end-to-end tests</a> using the CLI runner. You can use this to implement a complete test (in the <a href="https://codeberg.org/ofosos/rotoscope/commit/dfa60c1bfcb1ac720ad168e5e98f02bac1fde17d" target="_blank">sample project</a>, the tests are in the <code>tests</code> folder.)</p>
<p>The test sits in a method of a testing class. Most of the conventions follow what I'd use in any other Python project very closely, but there are a few specifics because rotoscope uses <code>click</code>. In the <code>test</code> method, I create a <code>CliRunner</code>. The test uses this to run the command in an isolated file system. Then the test creates <code>incoming</code> and <code>archive</code> directories and a dummy <code>incoming/test.txt</code> file within the isolated file system. Then it invokes the CliRunner just like you'd invoke a command-line application. After the run completes, the test examines the isolated filesystem and verifies that <code>incoming</code> is empty, and that <code>archive</code> contains two files (the latest link and the archived file.)</p>
<pre>
<code class="language-python">from os import listdir, mkdir
from click.testing import CliRunner
from rotoscope.rotoscope import rotoscope

class TestRotoscope:
    def test_roto_good(self, tmp_path):
        runner = CliRunner()

        with runner.isolated_filesystem(temp_dir=tmp_path) as td:
            mkdir("incoming")
            mkdir("archive")
            with open("incoming/test.txt", "w") as f:
                f.write("hello")

            result = runner.invoke(rotoscope, ["incoming", "--archive", "archive"])
            assert result.exit_code == 0

            print(td)
            incoming_f = listdir("incoming")
            archive_f = listdir("archive")
            assert len(incoming_f) == 0
            assert len(archive_f) == 2
</code></pre><p>To execute these tests on my console, run <code>tox</code> in the project's root directory.</p>
<p>During implementing the tests, I found a bug in my code. When I did the Click conversion, rotoscope just unlinked the latest file, whether it was present or not. The tests started with a fresh file system (not my home folder) and promptly failed. I can prevent this kind of bug by running in a nicely isolated and automated test environment. That'll avoid a lot of "it works on my machine" problems.</p>
<h2>Scaffolding and modules</h2>
<p>This completes our tour of advanced things you can do with <code>scaffold</code> and <code>click</code>. There are many possibilities to level up a casual Python script, and make even your simple utilities into full-fledged CLI tools.</p>
<p><code><code> </code></code></p></pa>