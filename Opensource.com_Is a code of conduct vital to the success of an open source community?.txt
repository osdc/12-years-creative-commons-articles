<p>Late last month, the Debian project voted to adopt a <a href="https://www.debian.org/vote/2014/vote_002#text" target="_blank">community code of conduct</a>, a set of guidelines for acceptable participation in its official communication channels. Members agreed to abide by the following principles:</p>
<ul><li>Be respectful</li>
<li>Assume good faith</li>
<li>Be collaborative</li>
<li>Try to be concise</li>
<li>Be open</li>
</ul><p>The project also cemented a formal process for altering the code: only a vote from the Debian general assembly can amend the community's code of conduct.</p>
<p>In ratifying this general resolution, Debian joins a host of high-profile open source projects—like <a href="http://fedoraproject.org/code-of-conduct" target="_blank">Fedora</a>, <a href="http://www.ubuntu.com/about/about-ubuntu/conduct" target="_blank">Ubuntu</a>, <a href="https://wiki.gnome.org/action/show/Foundation/CodeOfConduct?action=show&amp;redirect=CodeOfConduct" target="_blank">GNOME</a>, and <a href="https://www.openstack.org/legal/community-code-of-conduct/" target="_blank">OpenStack</a>—that have adopted community codes of conduct.</p>
<p>Open source communities' codes of conduct differ, but all tend to embody a core collection of values: respect, consideration, collaboration, concision, transparency, patience, deference to electoral processes, and the importance of seeking assistance when in need.</p>
<p>Certainly these values express something central to <a href="https://opensource.com/open-source-way" target="_blank">the open source way</a>, but do they go without saying? <strong>Should all open source projects formalize community codes of conduct?</strong></p>
