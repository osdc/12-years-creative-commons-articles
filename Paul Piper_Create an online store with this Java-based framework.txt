<p>So you want to sell products or services online, but either can't find a fitting software or think customization would be too costly? <a href="https://www.scipioerp.com" target="_blank">Scipio ERP</a> may just be what you are looking for.</p>
<p>Scipio ERP is a Java-based open source e-commerce framework that comes with a large range of applications and functionality. The project was forked from <a href="https://ofbiz.apache.org/" target="_blank">Apache OFBiz</a> in 2014 with a clear focus on better customization and a more modern appeal. The e-commerce component is quite extensive and works in a multi-store setup, internationally, and with a wide range of product configurations, and it's also compatible with modern HTML frameworks. The software also provides standard applications for many other business cases, such as accounting, warehouse management, or sales force automation. It's all highly standardized and therefore easy to customize, which is great if you are looking for more than a virtual cart.</p>
<p>The system makes it very easy to keep up with modern web standards, too. All screens are constructed using the system's "<a href="https://www.scipioerp.com/community/developer/freemarker-macros/" target="_blank">templating toolkit</a>," an easy-to-learn macro set that separates HTML from all applications. Because of it, every application is already standardized to the core. Sounds confusing? It really isn't—it all looks a lot like HTML, but you write a lot less of it.</p>
<h2 id="initial-setup">Initial setup</h2>
<p>Before you get started, make sure you have Java 1.8 (or greater) SDK and a Git client installed. Got it? Great! Next, check out the master branch from GitHub:</p>
<pre><code class="language-text">git clone https://github.com/ilscipio/scipio-erp.git
cd scipio-erp
git checkout master</code></pre><p>To set up the system, simply run <strong>./install.sh</strong> and select either option from the command line. Throughout development, it is best to stick to an <strong>installation for development</strong> (Option 1), which will also install a range of demo data. For professional installations, you can modify the initial config data ("seed data") so it will automatically set up the company and catalog data for you. By default, the system will run with an internal database, but it <a href="https://www.scipioerp.com/community/developer/installation-configuration/configuration/#database-configuration" target="_blank">can also be configured</a> with a wide range of relational databases such as PostgreSQL and MariaDB.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Setup wizard"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/setup_step5_sm.jpg" width="600" height="311" alt="Setup wizard" title="Setup wizard" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>Follow the setup wizard to complete your initial configuration,</sup></p>
</div>
      
  </article></p>
<p>Start the system with <strong>./start.sh</strong> and head over to <strong><a href="https://localhost:8443/setup/">https://localhost:8443/setup/</a></strong> to complete the configuration. If you installed with demo data, you can log in with username <strong>admin</strong> and password <strong>scipio</strong>. During the setup wizard, you can set up a company profile, accounting, a warehouse, your product catalog, your online store, and additional user profiles. Keep the website entries on the product store configuration screen for now. The system allows you to run multiple webstores with different underlying code; unless you want to do that, it is easiest to stick to the defaults.</p>
<p>Congratulations, you just installed Scipio ERP! Play around with the screens for a minute or two to get a feel for the functionality.</p>
<h2 id="shortcuts">Shortcuts</h2>
<p>Before you jump into the customization, here are a few handy commands that will help you along the way:</p>
<ul><li>Create a shop-override: <strong>./ant create-component-shop-override</strong></li>
<li>Create a new component: <strong>./ant create-component</strong></li>
<li>Create a new theme component: <strong>./ant create-theme</strong></li>
<li>Create admin user: <strong>./ant create-admin-user-login</strong></li>
<li>Various other utility functions: <strong>./ant -p</strong></li>
<li>Utility to install &amp; update add-ons: <strong>./git-addons help</strong></li>
</ul><p>Also, make a mental note of the following locations:</p>
<ul><li>Scripts to run Scipio as a service: <strong>/tools/scripts/ </strong></li>
<li>Log output directory: <strong>/runtime/logs </strong></li>
<li>Admin application: <strong><a href="https://localhost:8443/admin/">https://localhost:8443/admin/</a></strong></li>
<li>E-commerce application: <strong><a href="https://localhost:8443/shop/">https://localhost:8443/shop/</a></strong></li>
</ul><p>Last, Scipio ERP structures all code in the following five major directories:</p>
<ul><li>Framework: framework-related sources, the application server, generic screens, and configurations</li>
<li>Applications: core applications</li>
<li>Addons: third-party extensions</li>
<li>Themes: modifies the look and feel</li>
<li>Hot-deploy: your own components</li>
</ul><p>Aside from a few configurations, you will be working within the hot-deploy and themes directories.</p>
<h2 id="webstore-customizations">Webstore customizations</h2>
<p>To really make the system your own, start thinking about <a href="https://www.scipioerp.com/community/developer/architecture/components/" target="_blank">components</a>. Components are a modular approach to override, extend, and add to the system. Think of components as self-contained web modules that capture information on databases (<a href="https://www.scipioerp.com/community/developer/entities/" target="_blank">entity</a>), functions (<a href="https://www.scipioerp.com/community/developer/services/" target="_blank">services</a>), screens (<a href="https://www.scipioerp.com/community/developer/views-requests/" target="_blank">views</a>), <a href="https://www.scipioerp.com/community/developer/events-actions/" target="_blank">events and actions</a>, and web applications. Thanks to components, you can add your own code while remaining compatible with the original sources.</p>
<p>Run <strong>./ant create-component-shop-override</strong> and follow the steps to create your webstore component. A new directory will be created inside of the hot-deploy directory, which extends and overrides the original e-commerce application.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="component directory structure"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/component_structure.jpg" width="271" height="521" alt="component directory structure" title="component directory structure" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>A typical component directory structure.</sup></p>
</div>
      
  </article></p>
<p>Your component will have the following directory structure:</p>
<ul><li>config: configurations</li>
<li>data: seed data</li>
<li>entitydef: database table definitions</li>
<li>script: Groovy script location</li>
<li>servicedef: service definitions</li>
<li>src: Java classes</li>
<li>webapp: your web application</li>
<li>widget: screen definitions</li>
</ul><p>Additionally, the <strong>ivy.xml</strong> file allows you to add Maven libraries to the build process and the <strong>ofbiz</strong><strong>-component.xml</strong> file defines the overall component and web application structure. Apart from the obvious, you will also find a <strong>controller.xml</strong> file inside the web apps' <strong>WEB-INF</strong> directory. This allows you to define request entries and connect them to events and screens. For screens alone, you can also use the built-in CMS functionality, but stick to the core mechanics first. Familiarize yourself with <strong>/applications/shop/</strong> before introducing changes.</p>
<h3 id="adding-custom-screens">Adding custom screens</h3>
<p>Remember the <a href="https://www.scipioerp.com/community/developer/freemarker-macros/" target="_blank">templating toolkit</a>? You will find it used on every screen. Think of it as a set of easy-to-learn macros that structure all content. Here's an example:</p>
<pre><code class="language-text">&lt;@section title="Title"&gt;
    &lt;@heading id="slider"&gt;Slider&lt;/@heading&gt;
    &lt;@row&gt;
        &lt;@cell columns=6&gt;
            &lt;@slider id="" class="" controls=true indicator=true&gt;
                &lt;@slide link="#" image="https://placehold.it/800x300"&gt;Just some content…&lt;/@slide&gt;
                &lt;@slide title="This is a title" link="#" image="https://placehold.it/800x300"&gt;&lt;/@slide&gt;
            &lt;/@slider&gt;
        &lt;/@cell&gt;
        &lt;@cell columns=6&gt;Second column&lt;/@cell&gt;
    &lt;/@row&gt;
&lt;/@section&gt;</code></pre><p>Not too difficult, right? Meanwhile, themes contain the HTML definitions and styles. This hands the power over to your front-end developers, who can define the output of each macro and otherwise stick to their own build tools for development.</p>
<p>Let's give it a quick try. First, define a request on your own webstore. You will modify the code for this. A built-in CMS is also available at <strong><a href="https://localhost:8443/cms/">https://localhost:8443/cms/</a></strong>, which allows you to create new templates and screens in a much more efficient way. It is fully compatible with the templating toolkit and comes with example templates that can be adopted to your preferences. But since we are trying to understand the system here, let's go with the more complicated way first.</p>
<p>Open the <strong><a href="https://www.scipioerp.com/community/developer/views-requests/request-controller/" target="_blank">controller.xml</a></strong> file inside of your shop's webapp directory. The controller keeps track of request events and performs actions accordingly. The following will create a new request under <strong>/shop/test</strong>:</p>
<pre><code class="language-text">&lt;!-- Request Mappings --&gt;
&lt;request-map uri="test"&gt;
     &lt;security https="true" auth="false"/&gt;
      &lt;response name="success" type="view" value="test"/&gt;
&lt;/request-map&gt;</code></pre><p>You can define multiple responses and, if you want, you could use an event or a service call inside the request to determine which response you may want to use. I opted for a response of type "view." A view is a rendered response; other types are request-redirects, forwards, and alike. The system comes with various renderers and allows you to determine the output later; to do so, add the following:</p>
<pre><code class="language-text">&lt;!-- View Mappings --&gt;
&lt;view-map name="test" type="screen" page="component://mycomponent/widget/CommonScreens.xml#test"/&gt;</code></pre><p>Replace <strong>my-component</strong> with your own component name. Then you can define your very first screen by adding the following inside the tags within the <strong>widget/CommonScreens.xml</strong> file:</p>
<pre><code class="language-text">&lt;screen name="test"&gt;
        &lt;section&gt;
            &lt;actions&gt;
            &lt;/actions&gt;
            &lt;widgets&gt;
                &lt;decorator-screen name="CommonShopAppDecorator" location="component://shop/widget/CommonScreens.xml"&gt;
                    &lt;decorator-section name="body"&gt;
                        &lt;platform-specific&gt;&lt;html&gt;&lt;html-template location="component://mycomponent/webapp/mycomponent/test/test.ftl"/&gt;&lt;/html&gt;&lt;/platform-specific&gt;
                    &lt;/decorator-section&gt;
                &lt;/decorator-screen&gt;
            &lt;/widgets&gt;
        &lt;/section&gt;
    &lt;/screen&gt;</code></pre><p>Screens are actually quite modular and consist of multiple elements (<a href="https://www.scipioerp.com/community/developer/views-requests/screen-widgets-decorators/" target="_blank">widgets, actions, and decorators</a>). For the sake of simplicity, leave this as it is for now, and complete the new webpage by adding your very first templating toolkit file. For that, create a new <strong>webapp/mycomponent/test/test.ftl</strong> file and add the following:</p>
<pre><code class="language-text">&lt;@alert type="info"&gt;Success!&lt;/@alert&gt;</code></pre><p>
<article class="align-center media media--type-image media--view-mode-full" title="Custom screen"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/success_screen_sm.jpg" width="600" height="250" alt="Custom screen" title="Custom screen" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>A custom screen.</sup></p>
</div>
      
  </article></p>
<p>Open <strong><a href="https://localhost:8443/shop/control/test/">https://localhost:8443/shop/control/test/</a></strong> and marvel at your own accomplishments.</p>
<h3 id="custom-themes">Custom themes</h3>
<p>Modify the look and feel of the shop by creating your very own theme. All themes can be found as components inside of the themes folder. Run <strong> ./ant create-theme</strong> to add your own.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="theme component layout"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/theme_structure.jpg" width="246" height="601" alt="theme component layout" title="theme component layout" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>A typical theme component layout.</sup></p>
</div>
      
  </article></p>
<p>Here's a list of the most important directories and files:</p>
<ul><li>Theme configuration: <strong>data/*ThemeData.xml</strong></li>
<li>Theme-specific wrapping HTML: <strong>includes/*.ftl</strong></li>
<li>Templating Toolkit HTML definition: <strong>includes/themeTemplate.ftl</strong></li>
<li>CSS class definition: <strong>includes/themeStyles.ftl</strong></li>
<li>CSS framework: <strong>webapp/theme-title/*</strong></li>
</ul><p>Take a quick look at the Metro theme in the toolkit; it uses the Foundation CSS framework and makes use of all the things above. Afterwards, set up your own theme inside your newly constructed <strong>webapp/theme-title</strong> directory and start developing. The Foundation-shop theme is a very simple shop-specific theme implementation that you can use as a basis for your own work.</p>
<p>Voila! You have set up your own online store and are ready to customize!</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Finished Scipio ERP shop"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/finished_shop_1_sm.jpg" width="600" height="791" alt="Finished Scipio ERP shop" title="Finished Scipio ERP shop" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>A finished shop based on Scipio ERP.</sup></p>
</div>
      
  </article></p>
<h2 id="what's-next">What's next?</h2>
<p>Scipio ERP is a powerful framework that simplifies the development of complex e-commerce applications. For a more complete understanding, check out the project <a href="https://www.scipioerp.com/community/developer/architecture/components/" target="_blank">documentation</a>, try the <a href="https://www.scipioerp.com/demo/" target="_blank">online demo</a>, or <a href="https://forum.scipioerp.com/" target="_blank">join the community</a>.</p>
