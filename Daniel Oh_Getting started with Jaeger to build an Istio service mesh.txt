<p><a href="https://blog.buoyant.io/2017/04/25/whats-a-service-mesh-and-why-do-i-need-one/" target="_blank">Service mesh</a> provides a dedicated network for service-to-service communication in a transparent way. <a href="https://istio.io/docs/concepts/what-is-istio/" target="_blank">Istio</a> aims to help developers and operators address service mesh features such as dynamic service discovery, mutual transport layer security (TLS), circuit breakers, rate limiting, and tracing. <a href="https://www.jaegertracing.io/docs/1.9/" target="_blank">Jaeger</a> with Istio augments monitoring and tracing of cloud-native apps on a distributed networking system. This article explains how to get started with Jaeger to build an Istio service mesh on the Kubernetes platform.</p>
<h2 id="spinning-up-a-kubernetes-cluster">Spinning up a Kubernetes cluster</h2>
<p><a href="https://opensource.com/article/18/10/getting-started-minikube">Minikube</a> allows you to run a single-node Kubernetes cluster based on a virtual machine such as <a href="https://www.linux-kvm.org/page/Main_Page" target="_blank">KVM</a>, <a href="https://www.virtualbox.org/wiki/Downloads" target="_blank">VirtualBox</a>, or <a href="https://github.com/moby/hyperkit" target="_blank">HyperKit</a> on your local machine. <a href="https://kubernetes.io/docs/tasks/tools/install-minikube/" target="_blank">Install Minikube</a> and use the following shell script to run it:</p>
<pre>
<code class="language-bash">#!/bin/bash

export MINIKUBE_PROFILE_NAME=istio-jaeger
minikube profile $MINIKUBE_PROFILE_NAME
minikube config set cpus 3
minikube config set memory 8192

# You need to replace appropriate VM driver on your local machine
minikube config set vm-driver hyperkit

minikube start</code></pre><p>More on Kubernetes</p>
<div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/topics/containers/what-is-kubernetes?intcmp=7016000000127cYAAQ">What is Kubernetes?</a></li>
<li><a href="https://www.redhat.com/en/engage/kubernetes-containers-storage-s-201911201051?intcmp=7016000000127cYAAQ">eBook: Storage Patterns for Kubernetes</a></li>
<li><a href="https://www.redhat.com/en/engage/openshift-storage-testdrive-20170718?intcmp=7016000000127cYAAQ">Test drive OpenShift hands-on</a></li>
<li><a href="https://opensource.com/kubernetes-dump-truck?intcmp=7016000000127cYAAQ">eBook: Getting started with Kubernetes</a></li>
<li><a href="https://www.redhat.com/en/resources/managing-containers-kubernetes-openshift-technology-detail?intcmp=7016000000127cYAAQ">An introduction to enterprise Kubernetes</a></li>
<li><a href="https://enterprisersproject.com/article/2017/10/how-explain-kubernetes-plain-english?intcmp=7016000000127cYAAQ">How to explain Kubernetes in plain terms</a></li>
<li><a href="https://opensource.com/downloads/kubernetes-raspberry-pi?intcmp=7016000000127cYAAQ">eBook: Running Kubernetes on your Raspberry Pi homelab</a></li>
<li><a href="https://opensource.com/downloads/kubernetes-cheat-sheet?intcmp=7016000000127cYAAQ">Kubernetes cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/kubernetes-sysadmin?intcmp=7016000000127cYAAQ">eBook: A guide to Kubernetes for SREs and sysadmins</a></li>
<li><a href="https://opensource.com/tags/kubernetes?intcmp=7016000000127cYAAQ">Latest Kubernetes articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>In the above script, replace the <strong>--vm-driver=xxx</strong> option with the appropriate virtual machine driver on your operating system (OS).</p>
<h2 id="deploying-istio-service-mesh-with-jaeger">Deploying Istio service mesh with Jaeger</h2>
<p>Download the Istio installation file for your OS from the <a href="https://github.com/istio/istio/releases" target="_blank">Istio release page</a>. In the Istio package directory, you will find the Kubernetes installation YAML files in <strong>install/</strong> and the sample applications in <strong>sample/</strong>. Use the following commands:</p>
<pre>
<code class="language-text">$ curl -L https://git.io/getLatestIstio | sh -
$ cd istio-1.0.5
$ export PATH=$PWD/bin:$PATH</code></pre><p>The easiest way to deploy Istio with Jaeger on your Kubernetes cluster is to use <a href="https://kubernetes.io/docs/concepts/extend-kubernetes/api-extension/custom-resources/#customresourcedefinitions" target="_blank">Custom Resource Definitions</a>. Install Istio with mutual TLS authentication between sidecars with these commands:</p>
<pre>
<code class="language-text">$ kubectl apply -f install/kubernetes/helm/istio/templates/crds.yaml
$ kubectl apply -f install/kubernetes/istio-demo-auth.yaml</code></pre><p>Check if all pods of Istio on your Kubernetes cluster are deployed and running correctly by using the following command and review the output:</p>
<pre>
<code class="language-text">$ kubectl get pods -n istio-system
NAME                                      READY     STATUS      RESTARTS   AGE
grafana-59b8896965-p2vgs                  1/1       Running     0          3h
istio-citadel-856f994c58-tk8kq            1/1       Running     0          3h
istio-cleanup-secrets-mq54t               0/1       Completed   0          3h
istio-egressgateway-5649fcf57-n5ql5       1/1       Running     0          3h
istio-galley-7665f65c9c-wx8k7             1/1       Running     0          3h
istio-grafana-post-install-nh5rw          0/1       Completed   0          3h
istio-ingressgateway-6755b9bbf6-4lf8m     1/1       Running     0          3h
istio-pilot-698959c67b-d2zgm              2/2       Running     0          3h
istio-policy-6fcb6d655f-lfkm5             2/2       Running     0          3h
istio-security-post-install-st5xc         0/1       Completed   0          3h
istio-sidecar-injector-768c79f7bf-9rjgm   1/1       Running     0          3h
istio-telemetry-664d896cf5-wwcfw          2/2       Running     0          3h
istio-tracing-6b994895fd-h6s9h            1/1       Running     0          3h
prometheus-76b7745b64-hzm27               1/1       Running     0          3h
servicegraph-5c4485945b-mk22d             1/1       Running     1          3h</code></pre><h2 id="building-sample-microservice-apps">Building sample microservice apps</h2>
<p>You can use the <a href="https://github.com/istio/istio/tree/master/samples/bookinfo" target="_blank">Bookinfo</a> app to learn about Istio's features. Bookinfo consists of four microservice apps: <em>productpage</em>, <em>details</em>, <em>reviews</em>, and <em>ratings</em> deployed independently on Minikube. Each microservice will be deployed with an Envoy sidecar via Istio by using the following commands:</p>
<pre>
<code class="language-text">// Enable sidecar injection automatically
$ kubectl label namespace default istio-injection=enabled
$ kubectl apply -f samples/bookinfo/platform/kube/bookinfo.yaml

// Export the ingress IP, ports, and gateway URL
$ kubectl apply -f samples/bookinfo/networking/bookinfo-gateway.yaml

$ export INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="http2")].nodePort}')
$ export SECURE_INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="https")].nodePort}')
$ export INGRESS_HOST=$(minikube ip)

$ export GATEWAY_URL=$INGRESS_HOST:$INGRESS_PORT</code></pre><h2 id="accessing-the-jaeger-dashboard">Accessing the Jaeger dashboard</h2>
<p>To view tracing information for each HTTP request, create some traffic by running the following commands at the command line:</p>
<pre>
<code class="language-text">$ while true; do
  curl -s http://${GATEWAY_URL}/productpage &gt; /dev/null
  echo -n .;
  sleep 0.2
done</code></pre><p>You can access the Jaeger dashboard through a web browser with <a href="http://localhost:16686/">http://localhost:16686</a> if you set up port forwarding as follows:</p>
<pre>
<code class="language-text">kubectl port-forward -n istio-system $(kubectl get pod -n istio-system -l app=jaeger -o jsonpath='{.items[0].metadata.name}') 16686:16686 &amp;</code></pre><p>You can explore all traces by clicking "Find Traces" after selecting the <em>productpage</em> service. Your dashboard will look similar to this:</p>
<p> </p>
<article class="align-center media media--type-image media--view-mode-full" title="Find traces in Jaeger"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/traces_productpages.png" width="675" height="339" alt="Find traces in Jaeger" title="Find traces in Jaeger" /></div>
      
  </article><p>You can also view more details about each trace to dig into performance issues or elapsed time by clicking on a certain trace.</p>
<p> </p>
<article class="align-center media media--type-image media--view-mode-full" title="Viewing details about a trace"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/traces_performance.png" width="675" height="226" alt="Viewing details about a trace" title="Viewing details about a trace " /></div>
      
  </article><h2 id="conclusion">Conclusion</h2>
<p>A distributed tracing platform allows you to understand what happened from service to service for individual ingress/egress traffic. Istio sends individual trace information automatically to Jaeger, the distributed tracing platform, even if your modern applications aren't aware of Jaeger at all. In the end, this capability helps developers and operators do troubleshooting easier and quicker at scale.</p>
