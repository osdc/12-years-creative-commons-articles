<p>In prior years, this annual series covered individual apps. This year, we are looking at all-in-one solutions in addition to strategies to help in 2021. Welcome to day 11 of 21 Days of Productivity in 2021.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on productivity</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/downloads/collaboration-tools-ebook">5 open source collaboration tools</a></li>
<li><a href="https://opensource.com/downloads/organization-tools">6 open source tools for staying organized</a></li>
<li><a href="https://opensource.com/downloads/desktop-tools">7 open source desktop tools</a></li>
<li><a href="https://opensource.com/tags/tools">The latest on open source tools</a></li>
</ul></div>
</div>
</div>
</div>
<p>Web-based services allow for access to your data almost anywhere and they support millions of users hourly. For some of us, though, running our own service is preferable to using a big company's service for various reasons. Maybe we work on things that are subject to regulation or have explicit security requirements. Perhaps we have privacy concerns, or just like being able to build, run, and fix things ourselves. Whatever the case may be, <a href="https://nextcloud.com/" target="_blank">Nextcloud</a> can provide most of the services you need, but on your own hardware.</p>
<p><article class="align-center media media--type-image media--view-mode-full"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/day11-image1_0.png" width="2874" height="1462" alt="NextCloud Dashboard displaying service options" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>The NextCloud Dashboard</p>
</div>
      
  </article></p>
<p>Most of the time, when we think of Nextcloud, we think of file sharing and syncing, similar to commercial products like Dropbox, OneDrive, and Google Drive. However, these days, it is a full-on productivity suite, with an email client, calendaring, tasks, and notebooks.</p>
<p>There are several ways to set up and run Nextcloud. You can install it to a bare metal server, run it in Docker containers, or as a virtual machine. There are also hosted services that will run Nextcloud for you, should you choose. Finally, there are apps for every major OS, including mobile apps for access on-the-go.</p>
<p><article class="align-center media media--type-image media--view-mode-wysiwyg" title="Nextcloud virtual machine"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/styles/medium/public/pictures/nextcloud-vm.png?itok=savOCiNX" width="220" height="189" alt="Nextcloud virtual machine" title="Nextcloud virtual machine" class="image-style-medium" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>The Nextcloud VM (Kevin Sonney, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</p>
</div>
      
  </article></p>
<p>By default, Nextcloud comes with file sharing and a few other related apps (or add-ons) installed. You can find the Apps page in the Admin interface, which allows you to install individual add-ons and some pre-defined bundles of related apps. For my install, I chose the <em>Groupware Bundle</em>, which includes mail, calendars, contacts, and Deck. Deck is a lightweight Kanban Board for handling tasks. I installed the <em>Notes</em> and <em>Tasks</em> apps as well.</p>
<p>The Nextcloud Mail app is a very straight-forward IMAP email client. While Nextcloud does not include an IMAP or SMTP server as part of the packages, you can easily add one to the OS or use a remote service. The Calendar application is pretty standard and also allows you to subscribe to remote calendars. One drawback is that remote calendars (say, from a large cloud provider) are read-only, so you can view but not modify them.</p>
<p><article class="align-center media media--type-image media--view-mode-wysiwyg" title="NextCoud App Interface"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/styles/medium/public/pictures/nextcloud-app-interface.png?itok=QqDyvRP2" width="220" height="95" alt="NextCoud App Interface" title="NextCoud App Interface" class="image-style-medium" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>The NextCloud App interface (Kevin Sonney, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</p>
</div>
      
  </article></p>
<p>Notes is a simple text notepad, allowing you to create and update short notes, journals, and related things. Tasks is a to-do list application with support for multiple lists, task priorities, percent complete, and several other standard features that users expect. If you installed Deck, its task cards are listed as well. Each Board will show up as its own list, so you can use Deck or Tasks to track what is complete.</p>
<p>Deck itself is a Kanban Board app, presenting tasks as cards to move through a process. If you like the Kanban flow, it is an excellent application for tracking progress.</p>
<p><article class="align-center media media--type-image media--view-mode-wysiwyg" title="Taking notes in Nextcloud"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/styles/medium/public/day11-image3.png?itok=uCG2S7UP" width="220" height="112" alt="Taking notes" class="image-style-medium" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>The NextCloud App interface</p>
</div>
      
  </article></p>
<p>All the applications in Nextcloud natively support sharing via standard protocols. Unlike some similar solutions, this sharing does not feel like it was added on to check a features box. Sharing is one of the primary reasons Nextcloud exists, and so it is very simple to use. You can also add the ability to share links to social media, via email, and so on. You can use Nextcloud to replace several online services in a single interface, accessible from anywhere, with a collaboration-first mindset.</p>
