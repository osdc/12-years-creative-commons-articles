<p>Habits have an unfair reputation of being bad things that need to be stopped. But consider this: Automation and habits have many things in common. First, they're both repetitive. They're done over and over, the exact same way. More importantly, they reduce costs (both cost per piece and cost of energy used).</p>
<p><a href="https://www.amazon.com/Smart-Thinking-Essential-Problems-Innovate/dp/0399537759" target="blank">Art Markman's book <i>Smart Thinking</i></a> stresses the importance of creating habits to take your mind away from unimportant thoughts and redirect your attention toward more interesting and creative ideas. You can apply this concept to the use of <a href="https://theopenorganization.org/definition/open-organization-definition/" target="blank">open organization principles</a>.</p>
<p>This article is the second part of a two-part series on habits. The first part drew from a presentation I gave a few years ago. In this article, I apply new information from Markman.</p>
<h2>What is smart thinking?</h2>
<p><i>Smart thinking</i> is about the information you have, how you get valuable information, and how you use what you have successfully. It's also the ability to solve new problems using your current knowledge. Smart thinking is a skill you can use, practice, and improve on. Based on that definition, those benefits would be very helpful in promoting open organization principles.</p>
<h2>Forming habits</h2>
<p>Markman says habits are formed, unthinkingly automatic, and rarely reviewed for their importance. A process of strategic thinking is required to do this review. If performed correctly, less effort is needed to change these habits. The human mental system is designed to avoid thinking<b> </b>when possible. It's preferable to just effortlessly act on routine tasks so you can direct your attention to more important issues. When you perform tasks repeatedly, you form habits so your mind doesn't have to think through them step by step.</p>
<p>That's why you can't remember things you did a few seconds ago. Markman gives the example of not remembering whether you locked the door of your apartment or house. Because you did it automatically, if you were thinking of something completely different when you locked the door, you likely don't know whether you locked it.</p>
<p>The goal of smart thinking is to develop as many good habits as possible, so your mind doesn't have to go through the effort of thinking every time you perform them. Developing checklists is a good way to avoid excessive thinking. This can be applied to open organization principles as well. To save on energy and effort, follow a list of items and act on them. This was my goal when I developed the <a href="https://opensource.com/sites/default/files/images/open-org/communication_tech_worksheet.pdf">Communication technology maturity worksheet</a>, which I wrote about in my article "<a href="https://opensource.com/open-organization/20/3/communication-technology-worksheet">How to assess your organization's technical maturity</a>." If there's no routine and the activity is new, you must resort to a more stressful and tiring thought process.</p>
<h2>Importance of environment and repetition</h2>
<p>Markman describes a formula for creating smart habits, considering two key facts:</p>
<ul><li>There is interaction between environment and action.</li>
<li>Only work on repeated actions to create good habits.</li>
</ul><h3>Environment</h3>
<p>Markman defines an environment as both the <i>outside world</i> and one's <i>internal mental world</i> (such as goals, feelings, or perspectives). In the outside world, you can develop seemingly meaningless activities to create triggers and reminders. For example, you assign convenient locations for things, so you don't have to think about where they are. You can create triggers (such as lists or empty packages for resupply reminders) to highlight things you need to do, thereby avoiding a lot of thought.</p>
<p>If you want to read a book but have been putting it off, consider taking it out and putting it in front of your computer or TV. That way, you'll see it, and probably have to move it out of your way, each time you're at your computer or TV, which can be a powerful way to get you reading.</p>
<p>If you learn something in a particular place (with all its surroundings), you will recall that learning more easily if you are in a similar place or the same place. The environment triggers the information to come to the top of your mind. Because new (wanted) habits must be consciously performed, you must add elements in your environment to remind yourself to do them. Also, you must eliminate triggers that remind you of the old (unwanted) habit and replace them with another activity.</p>
<p>For the internal mental environment, consider trying to memorize useful facts that are continually required, such as names, locations, the best road routes, and so on. If I meet someone I want to get to know better, I ask for their name and use it as much as possible, then write it down when I have a chance. Later, I add that name to my personal contact information. By repeatedly seeing the name, I eventually memorize it and use it without thinking.</p>
<h3>Repetition</h3>
<p>All habits are formed through repeated use, and the first time is the hardest. After many repetitions, minimal thought is required, until the habit becomes automatic. Structure your daily life by forming beneficial habits and practicing them. Regular role playing is equally important. By getting those memories top of mind, they become faster to retrieve.</p>
<p>If you can make each habit as<i> </i>distinct<i> </i>as possible, it will be easier to remember and retrieve when needed. You know you've created a good habit when you no longer have to think about it.</p>
<h2><b>When good habits go bad</b></h2>
<p>A habit can go from good to bad with a situational change. Getting rid of a bad habit and creating a productive new one takes a lot of thought. When a habit change is required, regularly getting a good night's sleep is essential. Your brain helps process the new experiences and behaviors and makes the activity more effective in the future.</p>
<p>There are two important characteristics of changing habits:</p>
<ol><li>Your current habit pushes you into action. You must overcome the pressure to act.</li>
<li>When there's pressure to act, replace the habit with a more helpful action. New memories of the desired action must be developed and stored.</li>
</ol><p>You must develop triggers to engage the new behavior. This trigger requires your attention, as you are forcing yourself to stop the strong current habit and develop a new and, initially, weak habit. If you are distracted, the old habit remains strong. Your effort could also be disrupted by stress, fatigue, or the powerful influence of your environment.</p>
<p>Willpower alone cannot change habits, and it's extremely exhausting to try. You might have some short-term successes, but you will wear out eventually. You need environmental support. Cravings happen when you have an active goal that has been blocked. They are your mind's way of saying you must strengthen your desired goal further. You must not only stop the old habit but study what in your environment is activating it.</p>
<p>To apply these concepts to open organization principles, consider how you can create an environment that makes it easy to activate a principle while discouraging the current unproductive behavior. You must create an environment that reduces the chance of the bad habit being recalled and create triggers to activate the principles. That's the goal of the open organization leadership assessments. They become triggers.</p>
<p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">Learn about open organizations</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://theopenorganization.org?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Download resources">Download resources</a></div>
              <div class="field__item"><a href="https://theopenorganization.community?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Join the community">Join the community</a></div>
              <div class="field__item"><a href="https://opensource.com/open-organization/resources/open-org-definition?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="What is an open organization?">What is an open organization?</a></div>
              <div class="field__item"><a href="https://opensource.com/open-organization/resources/open-org-maturity-model?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="How open is your organization?">How open is your organization?</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<h2>The Role of 3</h2>
<p>One of Markman's key concepts is the Role of 3, which guides both effective learning and effective presentation of information in a way that will be remembered. Markman is talking about getting someone else to change his behavior. This reminds me of my sales seminar in which I wanted to improve (change) the sales associates' selling activities. There is a difference between education, which is just providing knowledge, and training. Training is developing a skill one can use and improve on. Therefore, I had to cut three-quarters of the content and repeat it four times in different ways in my seminars, including role playing. I think what Markman is saying is that stressing just three actions is ideal initially. In my sales training case, my greatest success was role playing product presentations. Not only did the sales people learn the features of the product, but they could talk about them confidently to customers. Afterward, that led to other skills that could be developed.</p>
<p>This Role of 3 applies to you too when wanting to act on something. Imagine a critical business meeting with vital information presented. Suppose you were distracted by jokes or anxiety-provoking rumors (about layoffs, demotions, or inadequate performance). The meeting could be a waste of your time unless you manage your attention on specific issues.</p>
<p>Here is what Markman recommends before, during, and after an important event, to learn three key points: </p>
<ol><li><b>Prepare:</b> Consider what you want to get out of the event (be it a meeting, presentation, or one-on-one discussion) and what you want to achieve. Consider at most three things. Information connected to your goal is easier to detect and remember. Your mental preparation directs your mind to the information you seek. Psychologists call this <i>advance organizer</i> activity. Consider what will be discussed and review any available materials beforehand.<br />
	 </li>
<li><b>Pay attention:</b> This is hard work, particularly when there are distractions. Avoid multitasking on unrelated work (including emails and instant messaging). Working memory is an important concept: Your attention impacts what you will remember later.<br />
	 </li>
<li><b>Review:</b> After any critical information-gathering event (a class, meeting, reading a book or article), write down three key points. If you can't write it down, review it in your mind, or record it on your phone or digital recorder. Try to attach what you learned to what you know. Where do they match or conflict?</li>
</ol><p>Suppose you're presenting open organization principles to a group. To make sure they remember those principles and use them, follow these procedures:</p>
<ol><li>Start presentations with an agenda and outline for all to review.</li>
<li>During the presentation, stay focused primarily on your three critical points. Present all principles, but continually review and highlight three of them.</li>
<li>Help people connect the content to what they are doing now and how the principles will improve their activities.</li>
<li>Share additional ways the principles can be applied with example use cases and their benefits.</li>
<li>End all presentations with a three-point summary. An action plan can be helpful as well.</li>
</ol><p>Say you're presenting the five open organization principles of transparency, collaboration, inclusivity, adaptability, and community. First, you present all of them in basic terms, but the Role of 3 tells you the audience will probably only pick up on three of them, so you focus on transparency, collaboration, and inclusivity.</p>
<p>Here's an example outline, following the Role of 3:</p>
<ol><li>Present three key ideas.
<ol><li>Transparency</li>
<li>Collaboration</li>
<li>Inclusivity</li>
</ol></li>
<li>Organize the presentation by describing each key idea in one sentence.
<ol><li>Transparency is getting as much useful information to other people as possible.</li>
<li>Collaboration is discussing issues with as many other people as appropriate and avoiding making final decisions on your own.</li>
<li>Inclusivity is getting as many different kinds of people involved in a project as appropriate.</li>
</ol></li>
<li>Develop key idea relationships.
<ol><li>How is transparency related to what people already know? The more transparent you are, the easier it is to collaborate.</li>
<li>How is collaboration related to what people already know, including transparency? If people are doing well with transparency, mention many times that by collaborating more, their transparency will improve.</li>
<li>How is inclusivity related to what people already know, including transparency and inclusivity? The more inclusive we are, the more diverse the ideas generated, leading to more transparency and collaboration.</li>
</ol></li>
<li>Provide three questions to help people remember and explain the ideas.
<ol><li>When was the last time you paused before deciding something and decided instead to collaborate with all people concerned?</li>
<li>Who do you think would be an easy person to collaborate with?</li>
<li>Each week, how many people do you collaborate with? Should that number be improved upon?</li>
</ol></li>
</ol><h2>Better for everyone</h2>
<p>Now you know what smart thinking is all about. You've learned the Role of 3, and you understand the importance of environment and repetition. These concepts can help you be more productive and happier in whatever you get involved in, including bringing the open organization principles to your own communities.</p>
