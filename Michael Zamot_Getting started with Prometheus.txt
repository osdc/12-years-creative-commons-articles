<p><a href="https://prometheus.io/" target="_blank">Prometheus</a> is an open source monitoring and alerting system that directly scrapes metrics from agents running on the target hosts and stores the collected samples centrally on its server. Metrics can also be pushed using plugins like <strong>collectd_exporter</strong>—although this is not Promethius' default behavior, it may be useful in some environments where hosts are behind a firewall or prohibited from opening ports by security policy.</p>
<p>Prometheus, a project of the <a href="https://www.cncf.io/" target="_blank">Cloud Native Computing Foundation</a>, scales up using a federation model, which enables one Prometheus server to scrape another Prometheus server. This allows creation of a hierarchical topology, where a central system or higher-level Prometheus server can scrape aggregated data already collected from subordinate instances.</p>
<p>Besides the Prometheus server, its most common components are its <a href="https://prometheus.io/docs/alerting/alertmanager/" target="_blank">Alertmanager</a> and its exporters.</p>
<p>Alerting rules can be created within Prometheus and configured to send custom alerts to Alertmanager. Alertmanager then processes and handles these alerts, including sending notifications through different mechanisms like email or third-party services like <a href="https://en.wikipedia.org/wiki/PagerDuty" target="_blank">PagerDuty</a>.</p>
<p>Prometheus' exporters can be libraries, processes, devices, or anything else that exposes the metrics that will be scraped by Prometheus. The metrics are available at the endpoint <strong>/metrics</strong>, which allows Prometheus to scrape them directly without needing an agent. The tutorial in this article uses <strong>node_exporter</strong> to expose the target hosts' hardware and operating system metrics. Exporters' outputs are plaintext and highly readable, which is one of Prometheus' strengths.</p>
<p>In addition, you can configure <a href="https://grafana.com/" target="_blank">Grafana</a> to use Prometheus as a backend to provide data visualization and dashboarding functions.</p>
<h2 id="making-sense-of-prometheus-configuration-file">Making sense of Prometheus' configuration file</h2>
<p>The number of seconds between when <strong>/metrics</strong> is scraped controls the granularity of the time-series database. This is defined in the configuration file as the <strong>scrape_interval</strong> parameter, which by default is set to 60 seconds.</p>
<p>Targets are set for each scrape job in the <strong>scrape_configs</strong> section. Each job has its own name and a set of labels that can help filter, categorize, and make it easier to identify the target. One job can have many targets.</p>
<h2 id="installing-prometheus">Installing Prometheus</h2>
<p>In this tutorial, for simplicity, we will install a Prometheus server and <strong>node_exporter</strong> with docker. Docker should already be installed and configured properly on your system. For a more in-depth, automated method, I recommend Steve Ovens' article <a href="https://opensource.com/article/18/3/how-use-ansible-set-system-monitoring-prometheus">How to use Ansible to set up system monitoring with Prometheus</a>.</p>
<p>Before starting, create the Prometheus configuration file <strong>prometheus.yml</strong> in your work directory as follows:</p>
<pre><code class="language-text">global:
  scrape_interval: 	15s
  evaluation_interval: 15s

scrape_configs:
  - job_name: 'prometheus'

	static_configs:
	- targets: ['localhost:9090']

  - job_name: 'webservers'

	static_configs:
	- targets: ['&lt;node exporter node IP&gt;:9100']</code></pre><p>Start Prometheus with Docker by running the following command:</p>
<pre><code class="language-text">$ sudo docker run -d -p 9090:9090 -v 
/path/to/prometheus.yml:/etc/prometheus/prometheus.yml 
prom/prometheus</code></pre><p>By default, the Prometheus server will use port 9090. If this port is already in use, you can change it by adding the parameter <strong>--web.listen-address="&lt;IP of machine&gt;:&lt;port&gt;"</strong> at the end of the previous command.</p>
<p>In the machine you want to monitor, download and run the <strong>node_exporter</strong> container by using the following command:</p>
<pre><code class="language-text">$ sudo docker run -d -v "/proc:/host/proc" -v "/sys:/host/sys" -v 
"/:/rootfs" --net="host" prom/node-exporter --path.procfs 
/host/proc --path.sysfs /host/sys --collector.filesystem.ignored-
mount-points "^/(sys|proc|dev|host|etc)($|/)"</code></pre><p>For the purposes of this learning exercise, you can install <strong>node_exporter</strong> and Prometheus on the same machine. Please note that it's not wise to run <strong>node_exporter</strong> under Docker in production—this is for testing purposes only.</p>
<p>To verify that <strong>node_exporter</strong> is running, open your browser and navigate to <strong>http://&lt;IP of Node exporter host&gt;:9100/metrics</strong>. All the metrics collected will be displayed; these are the same metrics Prometheus will scrape.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Checking node_exporter"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/check-node_exporter.png" width="675" height="357" alt="Checking node_exporter" title="Checking node_exporter" /></div>
      
  </article></p>
<p>To verify the Prometheus server installation, open your browser and navigate to <a href="http://localhost:9090" target="_blank">http://localhost:9090</a>.</p>
<p>You should see the Prometheus interface. Click on <strong>Status</strong> and then <strong>Targets</strong>. Under State, you should see your machines listed as <strong>UP</strong>.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Target machine status"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/targets-up.png" width="675" height="385" alt="Target machine status" title="Target machine status" /></div>
      
  </article></p>
<h2 id="using-prometheus-queries">Using Prometheus queries</h2>
<p>It's time to get familiar with <a href="https://prometheus.io/docs/prometheus/latest/querying/basics/" target="_blank">PromQL</a>, Prometheus' query syntax, and its graphing web interface. Go to <strong><a href="http://localhost:9090/graph">http://localhost:9090/graph</a></strong> on your Prometheus server. You will see a query editor and two tabs: Graph and Console.</p>
<p>Prometheus stores all data as time series, identifying each one with a metric name. For example, the metric <strong>node_filesystem_avail_bytes</strong> shows the available filesystem space. The metric's name can be used in the expression box to select all of the time series with this name and produce an instant vector. If desired, these time series can be filtered using selectors and labels—a set of key-value pairs—for example:</p>
<pre><code class="language-text">node_filesystem_avail_bytes{fstype="ext4"}</code></pre><p>When filtering, you can match "exactly equal" (<strong>=</strong>), "not equal" (<strong>!=</strong>), "regex-match" (<strong>=~</strong>), and "do not regex-match" (<strong>!~</strong>). The following examples illustrate this:</p>
<p>To filter <strong>node_filesystem_avail_bytes</strong> to show both ext4 and XFS filesystems:</p>
<pre><code class="language-text">node_filesystem_avail_bytes{fstype=~"ext4|xfs"}</code></pre><p>To exclude a match:</p>
<pre><code class="language-text">node_filesystem_avail_bytes{fstype!="xfs"}</code></pre><p>You can also get a range of samples back from the current time by using square brackets. You can use <strong>s</strong> to represent seconds, <strong>m</strong> for minutes, <strong>h</strong> for hours, <strong>d</strong> for days, <strong>w</strong> for weeks, and <strong>y</strong> for years. When using time ranges, the vector returned will be a range vector.</p>
<p>For example, the following command produces the samples from five minutes to the present:</p>
<pre><code class="language-text">node_memory_MemAvailable_bytes[5m]</code></pre><p>Prometheus also includes functions to allow advanced queries, such as this:</p>
<pre><code class="language-text">100 * (1 - avg by(instance)(irate(node_cpu_seconds_total{job='webservers',mode='idle'}[5m])))</code></pre><p>Notice how the labels are used to filter the job and the mode. The metric <strong>node_cpu_seconds_total</strong> returns a counter, and the <strong>irate()</strong> function calculates the per-second rate of change based on the last two data points of the range interval (meaning the range can be smaller than five minutes). To calculate the overall CPU usage, you can use the idle mode of the <strong>node_cpu_seconds_total</strong> metric. The idle percent of a processor is the opposite of a busy processor, so the <strong>irate</strong> value is subtracted from 1. To make it a percentage, multiply it by 100.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="CPU usage graph"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/cpu-usage.png" width="675" height="237" alt="CPU usage graph" title="CPU usage graph" /></div>
      
  </article></p>
<h2 id="learn-more">Learn more</h2>
<p>Prometheus is a powerful, scalable, lightweight, and easy to use and deploy monitoring tool that is indispensable for every system administrator and developer. For these and other reasons, many companies are implementing Prometheus as part of their infrastructure.</p>
<p>To learn more about Prometheus and its functions, I recommend the following resources:</p>
<ul><li>About <a href="https://prometheus.io/docs/prometheus/latest/querying/basics/" target="_blank">PromQL</a></li>
<li>What <a href="https://github.com/prometheus/node_exporter#collectors" target="_blank"><strong>node_exporters</strong> collects</a></li>
<li><a href="https://prometheus.io/docs/prometheus/latest/querying/functions/" target="_blank">Prometheus functions</a></li>
<li><a href="https://opensource.com/article/18/8/open-source-monitoring-tools">4 open source monitoring tools</a></li>
<li><a href="https://opensource.com/article/18/8/now-available-open-source-guide-devops-monitoring-tools">Now available: The open source guide to DevOps monitoring tools</a></li>
</ul>