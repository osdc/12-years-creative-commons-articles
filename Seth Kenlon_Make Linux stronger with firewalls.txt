<p>Everyone's heard of firewalls, even if only as a plot device in a TV cybercrime drama. Many people also know that their computer is (likely) running a firewall, but fewer people understand how to take control of their firewall when necessary.</p>
<p>Firewalls block unwanted network traffic, but different networks have different threat levels. For instance, if you're at home, you probably trust the other computers and devices on your network a lot more than when you're out at the local café using public WiFi. You can hope your computer differentiates between a trusted network and an untrusted one, or you can learn to manage, or at least verify, your security settings yourself.</p>
<h2 id="how-firewalls-work">How firewalls work</h2>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Linux resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux commands cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Free online course: RHEL Technical Overview</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux networking cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">SELinux cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux common commands cheat sheet</a></li>
<li><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">What are Linux containers?</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Our latest Linux articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>Communication between devices on a network happens through gateways called <em>ports</em>. Port, in this context, doesn't mean a physical connection like a USB port or an HDMI port. In network lingo, a port is an entirely virtual concept representing pathways for a specific type of data to either arrive at or depart from a computer. This system could have been called anything, like "connections" or "doorways," but they were named ports at least <a href="https://tools.ietf.org/html/rfc793" target="_blank">as early as 1981</a>, and that's the name in use today. The point is, there's nothing special about any port; they're just a way to designate an address where data transference may happen.</p>
<p>Back in 1972, <a href="https://tools.ietf.org/html/rfc433" target="_blank">a list of port numbers</a> (then called "sockets") was published, and this has since evolved into a set of well-known standard port numbers that help manage specific kinds of traffic. For instance, you access ports 80 and 443 on a daily basis when you visit a website, because most everyone on the internet has agreed, implicitly or explicitly, that data is transferred from web servers over those ports. You can test this theory by opening a web browser and navigating to a website with a nonstandard port appended to the URL. For instance, if you navigate to <strong>example.com:42</strong>, your request is denied because example.com does not serve a website at port 42.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Navigating to a nonstandard port produces an error"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/web-port-nonstandard.png" width="550" height="340" alt="Navigating to a nonstandard port produces an error" title="Navigating to a nonstandard port produces an error" /></div>
      
  </article></p>
<p>If you revisit the same website at port 80, you get a website, as expected. You can specify port 80 with <strong>:80</strong> at the end of the URL, but because port 80 is the standard port for HTTP traffic, your web browser assumes port 80 by default.</p>
<p>When a computer, like a web server, expects traffic at a specific port, it's acceptable (and necessary) to have the port open for traffic. The danger is leaving ports open that you have no reason to expect traffic on, and that's exactly what a firewall is for.</p>
<h2 id="install-firewalld">Install firewalld</h2>
<p>There are many interfaces for firewall configuration. This article covers <a href="https://firewalld.org/" target="_blank"><strong>firewalld</strong></a>, which integrates with Network Manager on the desktop and <strong>firewall-cmd</strong> in the terminal. Many Linux distributions ship with these tools installed. If yours doesn't, you can either take this article as general advice for firewall management and apply it to what you use, or you can install <strong>firewalld</strong>.</p>
<p>On Ubuntu, for instance, you must enable the <strong>universe</strong> repository, deactivate the default <strong>ufw</strong> firewall, and then install <strong>firewalld</strong>:</p>
<pre><code class="language-bash">$ sudo systemctl disable ufw
$ sudo add-apt-repository universe
$ sudo apt install firewalld</code></pre><p>Fedora, CentOS, RHEL, OpenSUSE, and many others include <strong>firewalld</strong> by default.</p>
<p>Regardless of your distribution, for a firewall to be effective, it must be active and set to be loaded at boot. The less you have to think about firewall maintenance, the better.</p>
<pre><code class="language-bash">$ sudo systemctl enable --now firewalld</code></pre><h2 id="choose-your-zone-with-network-manager">Choose your zone with Network Manager</h2>
<p>You probably connect to many different networks every day. You're on one network at work, another at the café, and yet another at home. Your computer can detect which network you use more frequently than others, but it doesn't know which you trust.</p>
<p>A firewall <em>zone</em> contains presets deciding what ports to open and close. Using zones, you can choose a policy that makes the most sense for the network you're currently on.</p>
<p>To see a list of available zones, open the Network Manager Connection Editor, found in your Applications menu, or with the <strong>nm-connection-editor &amp;</strong> command.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Network Manager Connection Editor"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/nm-connection-editor.png" width="550" height="352" alt="Network Manager Connection Editor" title="Network Manager Connection Editor" /></div>
      
  </article></p>
<p>From the list of network connections, double-click on your current network.</p>
<p>In the network configuration window that appears, click the General tab.</p>
<p>In the General panel, click the drop-down menu next to Firewall Zone for a list of all available zones.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Firewall zones"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/nm-zone.png" width="675" height="367" alt="Firewall zones" title="Firewall zones" /></div>
      
  </article></p>
<p>You can get this same list with this terminal command:</p>
<pre><code class="language-bash">$ sudo firewall-cmd --get-zones</code></pre><p>The zone titles indicate what their designers had in mind when creating them, but you can get the specifics of any zone with this terminal command:</p>
<pre><code class="language-bash">$ sudo firewall-cmd --zone work --list-all
work
  target: default
  icmp-block-inversion: no
  interfaces: 
  sources: 
  services: ssh dhcpv6-client
  ports: 
  protocols: 
  [...]</code></pre><p>In this example, the <strong>work</strong> zone is configured to permit SSH and DHCPv6-client incoming traffic but drops any other incoming traffic not explicitly requested by the user. (In other words, the <strong>work</strong> zone doesn't block HTTP response traffic when you visit a website, but it <em>does</em> deny an HTTP request on your port 80.)</p>
<p>View each zone to get familiar with the traffic each one allows. The most common ones are:</p>
<ul><li><strong>Work:</strong> Use this one when on a network you mostly trust. SSH, DHCPv6, and mDNS are permitted, and you can add more as needed. This zone is meant to be a starting point for a custom work environment based on your daily office requirements.</li>
<li><strong>Public:</strong> For networks you do not trust. This zone is the same as the work zone, but presumably, you would not add the same exceptions as your work zone.</li>
<li><strong>Drop:</strong> All incoming connections are dropped with no response given. This is as close to a stealth mode as you can get without shutting off networking entirely because only outgoing network connections are possible (even a casual port scanner could detect your computer from outgoing traffic, though, so don't mistake this zone for a cloaking device). This is arguably the safest zone when on public WiFi, and definitely the best when you have reason to believe a network is hostile.</li>
<li><strong>Block:</strong> All incoming connections are rejected with a message declaring that the requested port is prohibited. Only network connections you initiate are possible. This is a "friendly" version of the drop zone because, even though no port is open for incoming traffic, a port verbosely declines an uninitiated connection.</li>
<li><strong>Home:</strong> Use this when you trust other computers on the network. Only selected incoming connections are accepted, and you can add more as needed.</li>
<li><strong>Internal:</strong> Similar to the work zone, this is intended for internal networks where you mostly trust the other computers. You can open more ports and services as needed but still maintain a different rule set than you have on your work zone.</li>
<li><strong>Trusted:</strong> All network connections are accepted. Good for troubleshooting or on networks you absolutely trust.</li>
</ul><h2 id="assigning-a-zone-to-a-network">Assigning a zone to a network</h2>
<p>You can assign a zone to any network connection you make. Furthermore, you can assign a different zone to each network interface (Ethernet cable, WiFi, and so on) that attaches to each network.</p>
<p>Select the zone you want and click the Save button to commit the change.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Setting a new zone"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/nm-set.png" width="550" height="525" alt="Setting a new zone" title="Setting a new zone" /></div>
      
  </article></p>
<p>The easiest way to get into the habit of assigning a zone to a network interface is to tend to the networks you use most often. Assign the home zone to your home network, the work zone to your work network, and the public network to your favorite library or café network.</p>
<p>Once you have assigned a zone to all your usual networks, make an effort to assign a zone to the next new network you join, whether it's a new café or your mate's home network. Assigning zones is the best way to reinforce your own awareness that networks are not all equal and that you're not any more secure than anybody else just because you run Linux.</p>
<h2 id="default-zone">Default zone</h2>
<p>Rather than prompting you for a zone every time you join a new network, firewalld assigns any unknown network a default zone. Open a terminal and type this command to get your default zone:</p>
<pre><code class="language-bash">$ sudo firewall-cmd --get-default
public</code></pre><p>In this example, the public zone is the default. It's expected that you will keep the public zone highly restrictive, so it's a pretty safe zone to assign unknown networks. However, you can set your own default instead.</p>
<p>For instance, if you're more paranoid than most, or if you know that you frequent networks you have reason to distrust, you can assign a highly restrictive zone as default:</p>
<pre><code class="language-bash">$ sudo firewall-cmd --set-default-zone drop
success
$ sudo firewall-cmd --get-default
drop</code></pre><p>Now any new network you join will be subject to the drop zone rules unless you manually change it to something less restrictive.</p>
<h2 id="customizing-zones-by-opening-ports-and-services">Customizing zones by opening ports and services</h2>
<p>Firewalld's developers don't intend for their zone definitions to satisfy the needs of all the different networks and levels of trust in existence. They're just starting points for you to use and customize.</p>
<p>You don't have to know much about firewalls to be able to open and close ports based on the kinds of network activity you know you generate.</p>
<h3 id="predefined-services">Predefined services</h3>
<p>The simplest way to add permissions to your firewall is to add a predefined service. Strictly speaking, there's no such thing as a "service" as far as your firewall knows, because firewalls understand port numbers and protocol types. However, firewalld provides collections of ports and protocols based on standards and conventions.</p>
<p>For example, if you're a web developer and want to open your computer up on your local network so your colleagues can see the website you're building, you would add the <strong>http</strong> and <strong>https</strong> services. If you're a gamer and you're running the open source <a href="https://www.mumble.com/" target="_blank">murmur</a> voice-chat server for your guild, then you'd add the <strong>murmur</strong> service. There are many other services available, which you can view with this command:</p>
<pre><code class="language-bash">$ sudo firewall-cmd --get-services
amanda-client amanda-k5-client bacula bacula-client \
bgp bitcoin bitcoin-rpc ceph cfengine condor-collector \
ctdb dhcp dhcpv6 dhcpv6-client dns elasticsearch \
freeipa-ldap freeipa-ldaps ftp [...]</code></pre><p>If you see a service you need, add it to your current firewall configuration, for example:</p>
<pre><code class="language-bash">$ sudo firewall-cmd --add-service murmur</code></pre><p>This command opens all the ports and protocols needed for a particular service <em>within your default zone</em>, but only until you reboot your computer or restart your firewall. To make your changes permanent, use the <strong>--permanent</strong> flag:</p>
<pre><code class="language-bash">$ sudo firewall-cmd --add-service murmur --permanent</code></pre><p>You can also issue the command for a zone other than your default:</p>
<pre><code class="language-bash">$ sudo firewall-cmd --add-service murmur --permanent --zone home</code></pre><h3 id="ports">Ports</h3>
<p>Sometimes you want to allow traffic for something that just isn't defined by firewalld's services. Maybe you're setting up a nonstandard port for a common service or you need to open an arbitrary port.</p>
<p>For example, maybe you're running the open source <a href="https://opensource.com/article/18/5/maptool">virtual tabletop</a> software <a href="https://github.com/RPTools" target="_blank">MapTool</a>. Since you're running the MapTool server and there's no industry standard governing which port MapTool runs on, you can decide what port it uses and then "poke a hole" in your firewall to allow traffic on that port.</p>
<p>The process is basically the same as for services:</p>
<pre><code class="language-bash">$ sudo firewall-cmd --add-port 51234/tcp</code></pre><p>This command opens port 51234 to incoming TCP connections <em>in your default zone</em>, but only until you reboot your computer or restart your firewall. To make your changes permanent, use the <strong>--permanent</strong> flag:</p>
<pre><code class="language-bash">$ sudo firewall-cmd --add-port 51234/tcp --permanent</code></pre><p>You can also issue the command for a zone other than your default:</p>
<pre><code class="language-bash">$ sudo firewall-cmd --add-port 51234/tcp --permanent --zone home</code></pre><p>Allowing traffic through your computer is different from letting traffic through your router"s firewall. Your router probably has a different interface for its own embeded firewall (though the principle is the same), which is outside the scope of this article.</p>
<h2 id="removing-ports-and-services">Removing ports and services</h2>
<p>If you decide a service or a port is no longer needed, you can restart your firewall to clear your changes, unless you use the <strong>--permanent</strong> flag.</p>
<p>If you made your changes permanent, use the <strong>--remove-port</strong> or <strong>--remove-service</strong> flag:</p>
<pre><code class="language-bash">$ sudo firewall-cmd --remove-port 51234/tcp --permanent</code></pre><p>You can remove ports and services from a zone other than your default zone by specifying a zone in your command:</p>
<pre><code class="language-bash">$ sudo firewall-cmd --remove-service murmur --permanent --zone home</code></pre><h2 id="custom-zones">Custom zones</h2>
<p>You can use and abuse the default zones provided by firewalld, but you also have the freedom to create your own. For instance, if it makes sense for you to have a zone specific to gaming, then you can create one and switch over to it only while gaming.</p>
<p>To create a new, empty zone, create a new zone called <strong>game</strong> and reload the firewall rules so that your new zone becomes active:</p>
<pre><code class="language-bash">$ sudo firewall-cmd --new-zone game --permanent
success
$ sudo firewall-cmd --reload</code></pre><p>Once it's created and active, you can customize it with all the services and ports you need to have open for game night.</p>
<h2 id="diligence">Diligence</h2>
<p>Start thinking about your firewall strategy today. Start slow, and build up some sane defaults that make sense for you. It may take time before you make it a habit to think about your firewall and understand which network services you use, but with a little exploration, you can strengthen your Linux workstation no matter what your environment.</p>
