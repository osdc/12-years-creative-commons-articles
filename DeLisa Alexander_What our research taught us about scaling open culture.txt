<p>In an open organization, culture is critical. In my previous column, I explained how and why we’re on a journey at Red Hat to scale our open culture into the future. Today, I want to share the findings of the first phase of our "Scaling Our Culture For The Future" project, where we decided to gather and analyze some data, and at the same time, engage every Red Hatter in a company-wide conversation about our culture.</p>
<h2>Culture is everyone's job</h2>
<p>As we look to scale our open culture into the future, we wanted to know how Red Hatters would answer questions like:</p>
<ul><li>What is special about our company?</li>
<li>When you look ahead to the future of our company, what things are important not to lose, as we grow?</li>
<li>On the flip-side, what things do we need to change? What gets in our way?</li>
<li>What things should we start doing, stop doing, and continue doing, as individuals and as a company?</li>
</ul><p>We equipped every Red Hatter with a <a href="https://github.com/red-hat-people-team/scaling-our-culture/blob/master/Scaling%20Our%20Culture-guided-discussion.odp">discussion deck and tips for how to facilitate</a> their own group conversation and asked them to contribute their group’s findings back to the project. At the same time, we started similar discussions on memo-list (our company-wide email list) and on our social intranet platform.</p>
<h2>Making sense of the data</h2>
<p>We fed all this feedback into our project along with data from other sources including our associate survey and our exit interview data. As the project team analyzed the data and organized comments into themes, we began to identify common themes across different countries and departments.</p>
<p>
</p><div class="embedded-callout-menu callout-float-left">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Learn about open organizations</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://theopenorganization.org">Download resources</a></li>
<li><a href="https://theopenorganization.community">Join the community</a></li>
<li><a href="https://opensource.com/open-organization/resources/open-org-definition?src=too_resource_menu3a">What is an open organization?</a></li>
<li><a href="https://opensource.com/open-organization/resources/open-org-maturity-model?src=too_resource_menu4a">How open is your organization?</a></li>
</ul></div>
</div>
</div>
</div>

<p>Some themes—like the need for transparency in decision-making, for strengthening trust and respect across teams, and for <a href="https://opensource.com/open-organization/17/2/assuming-positive-intent">assuming </a><a href="https://opensource.com/open-organization/17/2/what-happens-when-we-just-assume-positive-intent">positive </a><a href="https://opensource.com/open-organization/17/1/force-for-good-community">intent</a>—were familiar ones that had long surfaced in our associate surveys. Other themes were more surprising and reflected the challenges of life in a growing open organization.</p>
<p>One theme we heard frequently was a request for more clarity, commonality, and simplicity in our processes and tools. As one Red Hatter put it, "Maybe we need to get over the idea that we have to do every single thing in a way that’s special."</p>
<p></p><div class="embedded-callout-text callout-float-right">"Maybe we need to get over the idea that we have to do every single thing in a way that’s special."</div>
<p>We <a href="https://github.com/red-hat-people-team/scaling-our-culture/blob/master/Scaling_our_culture_data_summary.pdf">published the findings in a set of summary slides</a> for all Red Hatters to see, along with a good bit of the raw data and a few hypotheses of where we should focus our efforts as a company over the next few years.</p>
<p>For example, we identified two key areas of tension in our culture:</p>
<ol><li>The desire for individual freedom versus the shared cost and complexity of everyone doing things in their own way.</li>
<li>Our strong passion for open source technology and desire to adopt and contribute to it versus the demands of running a business where we sometimes need to use a mature technology that solves a problem quickly (even if it’s proprietary).</li>
</ol><p>We also identified some potentially valuable culture projects, including:</p>
<ul><li>Reinforce and live our core values (freedom, courage, commitment, and accountability—all lived in balance) as well as the <a href="https://github.com/red-hat-people-team/red-hat-multiplier">Red Hat Multiplier</a>.</li>
<li>Have a carefully structured, thoughtful discussion to resolve the endless debate over what technologies we use, with the goal of publishing a position on the internal use of open source and proprietary tools to guide our decisions.</li>
<li>Create clear, mature, scalable processes and procedures to make our jobs easier and make our company successful, using the <a href="https://github.com/red-hat-people-team/open-decision-framework">Open Decision Framework</a> to guide us, and keeping in mind that flexibility and exceptions are important to consider.</li>
</ul><h2>A company-wide conversation</h2>
<p>Sharing our findings with thousands of Red Hatters sparked further conversation and some refining of the overall plan. In the coming months, we’ll be kicking off some of the culture projects mentioned above, as well as a few others that are still under development. I’d like to invite you to come along this journey with us and share your own culture sustainment and transformation stories in the comments. I’ll share what we learn along the way.</p>
