<p>Karen Cranston (<a title="@kcranstn" href="https://twitter.com/kcranstn" target="_blank">@kcranstn</a>) is an evolutionary biologist at the National Evolutionary Synthesis Center (NESCent), a nonprofit science center dedicated to cross-disciplinary research in evolution. NESCent promotes the synthesis of information, concepts, and knowledge to  address significant, emerging, or novel questions in evolutionary  science and its applications. They collect new data under a Creative Commons license (CC0) to free scientific data and make it more widely available.</p>
<!--break-->
<h3>Sticks</h3>
<p>Modern science uses programs like BLAST to release data and this carries with it a public domain license. And scientists get their "scientific cred" by the huge numbers of citations that this program has, despite the fact that it's publicly available.</p>
<p><span>So, NESCent developed Dryad, a data repository where scientists don't have to hire lawyers to help determine and</span><span style="letter-spacing: 0px;"> explain what you can and can't do with the data. </span><span style="letter-spacing: 0px;">There are 31 journals and growing that have signed on to this joint data archiving policy; if a scientist publishes to one of these journals, the data must be made available in Dryad or another repository.</span></p>
<p>The same thing happening with funding agencies, but it's all not enough. <span style="letter-spacing: 0px;">Databases and agreements to share the data exist, but only 35% of studies are actually doing it. </span></p>
<h3>Carrots</h3>
<p>Beyond the obvious gains to society and the world when more scientific data is made available, it also causes there to be an increase in citations made, helping scientists gain tenure. Plus, there is a growing interest in old metrics: who is citing your data and using it? And organizations like PLOS and Figshare use a CC0 license.</p>
<p>CC0 plays an important role in the ecosystem of open science: open access for our publications, open source for our software, and CC0 and open data so that the products of our research are available to everyone.</p>
<p><strong>Note:</strong> Slides can be viewed <a href="http://www.slideshare.net/kcranstn" target="_blank">here</a>.</p>
<!--break-->
<h2>Lightning talk video</h2>
<p><iframe width="560" height="315" src="https://www.youtube.com/embed/hALfiG4ZcfY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe></p>
<ul><li><a href="http://www.youtube.com/watch?v=hALfiG4ZcfY">http://www.youtube.com/watch?v=hALfiG4ZcfY</a></li>
</ul>