<p><a href="https://opensource.com/article/22/5/write-c-appplications-vely-linux" target="_blank">Vely</a> combines high performance and the low footprint of C with the ease of use and improved safety of languages like PHP. It's free and open source software, licensed under GPLv3 and LGPL 3 for libraries, so you can even build commercial software with it.</p>
<h2>Using Vely for SaaS</h2>
<p>You can use Vely to create a multitenant web application that you can run on the Internet as Software-as-a-Service (SaaS). Each user has a completely separate data space from any other.</p>
<p>In this example web application, a user can sign up for a notebook service to create notes and then view and delete them. It demonstrates several technology integrations in just 310 lines of code across seven source files. The technologies include:</p>
<ul><li>MariaDB</li>
<li>Web browser</li>
<li>Apache</li>
<li>Unix sockets</li>
</ul><h3>How it works</h3>
<p>Here's how the application works from a user's perspective. A code walk-through follows the images.</p>
<p>The app allows a user to create a new login by specifying an email address and password. You can style these any way you like, such as with CSS:</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-10/1createuser.png" width="1428" height="194" alt="Create a user account" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Sergio Mijatovic, CC BY-SA 4.0)</p>
</div>
      
  </article><p>Verify the user's email:</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-10/2verifyemail.png" width="780" height="144" alt="Verify the user's email address" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Sergio Mijatovic, CC BY-SA 4.0)</p>
</div>
      
  </article><p>Each user logs in with their unique username and password:</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-10/3login.png" width="1386" height="228" alt="The user logs in" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Sergio Mijatovic, CC BY-SA 4.0)</p>
</div>
      
  </article><p>Once logged in, a user can add a note:</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-10/4addnote.png" width="784" height="476" alt="The user can add a note" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Sergio Mijatovic, CC BY-SA 4.0)</p>
</div>
      
  </article><p>A user can get a list of notes:</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-10/5listnotes.png" width="792" height="550" alt="User lists notes" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Sergio Mijatovic, CC BY-SA 4.0)</p>
</div>
      
  </article><p>The app asks for confirmation before deleting a note:</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-10/6confirmdelete.png" width="992" height="368" alt="The app asks for confirmation before deleting a note" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Sergio Mijatovic, CC BY-SA 4.0)</p>
</div>
      
  </article><p>After the user confirms, the note is deleted:</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-10/7notedeleted.png" width="922" height="358" alt="After confirmation, the note is deleted" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Sergio Mijatovic, CC BY-SA 4.0)</p>
</div>
      
  </article><h3>Setup prerequisites</h3>
<p>Follow the installation instructions on <a href="https://vely.dev/" target="_blank">Vely.dev</a>. It's a quick process that uses standard packaging tools, such as DNF, APT, Pacman, or Zypper.</p>
<p>Because they are part of this example, you must install Apache as a web server and MariaDB as a database.</p>
<p>After installing Vely, turn on syntax highlighting in Vim if you're using it:</p>
<pre>
<code class="language-text">vv -m</code></pre><h3>Get the source code</h3>
<p>The source code for this demonstration SaaS app is part of the Vely installation. It's a good idea to create a separate source code directory for each application (and you can name it whatever you like). In this case, unpacking the source code does that for you:</p>
<pre>
<code class="language-bash">$ tar xvf $(vv -o)/examples/multitenant_SaaS.tar.gz
$ cd multitenant_SaaS</code></pre><p>By default, the application is named <code>multitenant_SaaS</code>, but you can call it anything (if you do that, change it everywhere).</p>
<h2>Set up the application</h2>
<p>The very first step is to create an application. It's simple to do with Vely's <code>vf</code> utility:</p>
<pre>
<code class="language-bash">$ sudo vf -i -u $(whoami) multitenant_SaaS</code></pre><p>This command creates a new application home (<code>/var/lib/vv/multitenant_SaaS</code>) and performs the application setup for you. Mostly, that means creating various subdirectories in the home folder and assigning privileges. In this case, only the current user (the result of <code>whoami</code>) owns the directories, with 0700 privileges, which ensures that no one else has access to the files.</p>
<h2>Set up the database</h2>
<p>Before doing any coding, you need a place to store the information used by the application. First, create a MariaDB database called <code>db_multitenant_SaaS</code>, owned by the user <code>vely</code> with password <code>your_password</code>. You can change any of these values, but remember to change them everywhere during this example.</p>
<p>Logged in as root in the MySQL utility:</p>
<pre>
<code class="language-sql">create database if not exists db_multitenant_SaaS;
create user if not exists vely identified by 'your_password';
grant create,alter,drop,select,insert,delete,update on db_multitenant_SaaS.* to vely;</code></pre><p>Then create database objects (tables and records and so on) in the database:</p>
<pre>
<code class="language-sql">use db_multitenant_SaaS;
source setup.sql;
exit</code></pre><h2>Connect Vely to a database</h2>
<p>To let Vely know where your database is and how to log into it, create a database config file named <code>db_multitenant_SaaS</code>. (This is the name used by the database statements in the source code, so if you change it, make sure you change it everywhere.)</p>
<p>Vely uses native MariaDB database connectivity, so you can specify any options that a given database lets you:</p>
<pre>
<code class="language-bash">$ echo '[client]
user=vely
password=your_password
database=db_multitenant_SaaS
protocol=TCP
host=127.0.0.1
port=3306' &gt; db_multitenant_SaaS</code></pre><h2>Build the application</h2>
<p>Use the <code>vv</code> utility to make the application, using the <code>--db</code> option to specify the MariaDB database and the database config file:</p>
<pre>
<code class="language-bash">$ vv -q --db=mariadb:db_multitenant_SaaS</code></pre><p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">Programming and development</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Red Hat Developers Blog">Red Hat Developers Blog</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Programming cheat sheets">Programming cheat sheets</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/services/training/learning-subscription?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Try for free: Red Hat Learning Subscription">Try for free: Red Hat Learning Subscription</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/bash-programming-guide?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="eBook: An introduction to programming with Bash">eBook: An introduction to programming with Bash</a></div>
              <div class="field__item"><a href="https://developers.redhat.com/cheat-sheets/bash-shell-cheat-sheet?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Bash shell scripting cheat sheet">Bash shell scripting cheat sheet</a></div>
              <div class="field__item"><a href="https://developers.redhat.com/e-books/modernizing-enterprise-java?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="eBook: Modernizing Enterprise Java">eBook: Modernizing Enterprise Java</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/building-applications?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="An open source developer's guide to building applications">An open source developer's guide to building applications</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Register for your free Red Hat account">Register for your free Red Hat account</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<h2>Start the application server</h2>
<p>To start the application server for your web application, use the <code>vf</code> FastCGI process manager. The application server uses a Unix socket to communicate with the web server (creating a reverse proxy):</p>
<pre>
<code class="language-bash">$ vf -w 3 multitenant_SaaS</code></pre><p>This starts three daemon processes to serve the incoming requests. You can also start an adaptive server that increases the number of processes to serve more requests and gradually reduce the number of processes when they're not needed:</p>
<pre>
<code class="language-bash">$ vf multitenant_SaaS</code></pre><p>See <code>vf</code> for more options to help you achieve the best performance.</p>
<p>When you need to stop your application server, use the <code>-m quit</code> option:</p>
<pre>
<code class="language-bash">$ vf -m quit multitenant_SaaS</code></pre><h2>Set up the web server</h2>
<p>This is a web application, so the application needs a web server. This example uses Apache by way of a Unix socket listener.</p>
<h3>1. Set up Apache</h3>
<p>To configure Apache as a reverse proxy and connect your application to it, you need to enable FastCGI proxy support, which generally means using the <code>proxy</code> and <code>proxy_fcgi</code> modules.</p>
<p>For Fedora systems (or others, like Arch) enable the <code>proxy</code> and <code>proxy_fcgi</code> modules by adding (or uncommenting) the appropriate <strong>LoadModule</strong> directives in the <code>/etc/httpd/conf/httpd.conf</code> Apache configuration file.</p>
<p>For Debian, Ubuntu, and similar systems, enable the <code>proxy</code> and <code>proxy_fcgi</code> modules:</p>
<pre>
<code class="language-bash">$ sudo a2enmod proxy
$ sudo a2enmod proxy_fcgi</code></pre><p>For OpenSUSE, add these lines to the end of <code>/etc/apache2/httpd.conf</code>:</p>
<pre>
<code class="language-text">LoadModule proxy_module modules/mod_proxy.so
LoadModule proxy_fcgi_module modules/mod_proxy_fcgi.so</code></pre><h3>2. Configure Apache</h3>
<p>Now you must add the proxy information to the Apache configuration file:</p>
<pre>
<code class="language-text">ProxyPass "/multitenant_SaaS" unix:///var/lib/vv/multitenant_SaaS/sock/sock|fcgi://localhost/multitenant_SaaS</code></pre><p>The location of your configuration may vary, depending on your Linux distribution:</p>
<ul><li>Fedora, CentOS, Mageia, and Arch: <code>/etc/httpd/conf/httpd.conf</code></li>
<li>Debian, Ubuntu, Mint: <code>/etc/apache2/apache2.conf</code></li>
<li>OpenSUSE: <code>/etc/apache2/httpd.conf</code></li>
</ul><h3>3. Restart</h3>
<p>Finally, restart Apache. On Fedora and similar systems, as well as Arch Linux:</p>
<pre>
<code class="language-bash">$ sudo systemctl restart httpd</code></pre><p>On Debian and Debian-based systems, as well as OpenSUSE:</p>
<pre>
<code class="language-bash">$ sudo systemctl restart apache2</code></pre><h2>Set up local mail</h2>
<p>This example uses email as a part of its function. If your server can already send email, you can skip this. Otherwise, you can use local mail (<code>myuser@localhost</code>) just to test it out. To do that, install Sendmail.</p>
<p>On Fedora and similar:</p>
<pre>
<code class="language-bash">$ sudo dnf install sendmail
$ sudo systemctl start sendmail</code></pre><p>On Debian systems (like Ubuntu):</p>
<pre>
<code class="language-bash">$ sudo apt install sendmail
$ sudo systemctl start sendmail</code></pre><p>When the application sends an email to a local user, such as <code>OS_user@localhost</code>, then you can verify that the email was sent by looking at <code>/var/mail/<os_user code=""></os_user></code> (the "mail spool").</p>
<h2>Access the application server from the browser</h2>
<p>Assuming you're running the application locally, use <code>http://127.0.0.1/multitenant_SaaS?req=notes&amp;action=begin</code> to access your application server from your web browser. If you're running this on a live server on the Internet, you may need to adjust your firewall settings to allow HTTP traffic.</p>
<h2>Source code</h2>
<p>This example application contains seven source files. You can review the code yourself (remember, it's just 310 lines across these files), but here's an overview of each one.</p>
<h3>SQL setup (setup.sql)</h3>
<p>The two tables created are:</p>
<ul><li><strong>users</strong>: Information about each user. Each user in the <strong>users</strong> table has its own unique ID (<strong>userId</strong> column) along with other information such as email address and whether it's verified. There's also a hashed password. An actual password is never stored in plain text (or otherwise); a one-way hash is used to check the password.</li>
<li><strong>notes</strong>: Notes entered by the user. The <strong>notes</strong> table contains the notes, each along with <strong>userId</strong> column that states which user owns them. The <strong>userId</strong> column's value matches the namesake column from <strong>users</strong> table. This way, every note clearly belongs to a single user.</li>
</ul><p>The file contents:</p>
<pre>
<code class="language-sql">create table if not exists notes (dateOf datetime, noteId bigint auto_increment primary key, userId bigint, note varchar(1000));
create table if not exists users (userId bigint auto_increment primary key, email varchar(100), hashed_pwd varchar(100), verified smallint, verify_token varchar(30), session varchar(100));
create unique index if not exists users1 on users (email);</code></pre><h3>Run-time data (login.h)</h3>
<p>To properly display the Login, Sign Up, and Logout links, you need some flags that are available anywhere in the application. Also, the application uses cookies to maintain a session, so this needs to be available anywhere, for example, to verify that the session is valid. Every request sent to the application is confirmed that way. Only requests that come with verifiable cookies are permitted.</p>
<p>So to that effect, you have a <strong>global_request_data</strong> type <code>reqdata</code> (request data) and in it there's <code>sess_userId</code> (ID of user) and <code>sess_id</code> (user's current session ID). You also have rather self-explanatory flags that help render pages:</p>
<pre>
<code class="language-text">#ifndef _VV_LOGIN
#define _VV_LOGIN

typedef struct s_reqdata {
    bool displayed_logout; // true if Logout link displayed
    bool is_logged_in; // true if session verified logged-in
    char *sess_userId; // user ID of current session
    char *sess_id; // session ID
} reqdata;

void login_or_signup ();

#endif</code></pre><h3>Session checking and session data (_before.vely)</h3>
<p>Vely has a notion of a <strong>before_request_handler</strong>. The code you write executes before any other code that handles a request. To do this, all you need is to write this code in a file named <code>_before.vely</code>, and the rest is automatically handled.</p>
<p>Anything that a SaaS application does, such as handling requests sent to an application, must be validated for security. This way, the application knows whether the caller has the permissions needed to perform an action.</p>
<p>Checking for permission is done here in a before-request handler. That way, whatever other code you have handling a request, you already have the session information.</p>
<p>To keep session data (like session ID and user ID) available anywhere in your code, you use <strong>global_request_data</strong>. It's just a generic pointer (<strong>void*</strong>) to memory that any code that handles requests can access. This is perfect for handling sessions, as shown below:</p>
<pre>
<code class="language-text">#include "vely.h"
#include "login.h"

// _before() is a before-request-handler. It always executes before
// any other code that handles a request. It's a good place for any
// kind of request-wide setting or data initialization
void _before() {
    // Output HTTP header
    out-header default
    reqdata *rd; // this is global request data, see login.h
    // allocate memory for global request data, will be automatically deallocated
    // at the end of request
    new-mem rd size sizeof(reqdata)
    // initialize flags
    rd-&gt;displayed_logout = false;
    rd-&gt;is_logged_in = false;
    // set the data we created to be global request data, accessible
    // from any code that handles a request
    set-req data rd
    // check if session exists (based on cookies from the client)
    // this executes before any other request-handling code, making it
    // easier to just have session information ready
    _check_session ();
}</code></pre><h3>Checking if the session is valid (_check_session.vely)</h3>
<p>One of the most important tasks in a multitenant SaaS application is to check (as soon as possible) if the session is valid by checking whether a user is logged in. It's done by getting the session ID and user ID cookies from the client (such as a web browser) and checking these against the database where sessions are stored:</p>
<pre>
<code class="language-text">#include "vely.h"
#include "login.h"


// Check if session is valid
void _check_session () {
    // Get global request data
    reqdata *rd;
    get-req data to rd
    // Get cookies from user browser
    get-cookie rd-&gt;sess_userId="sess_userId"
    get-cookie rd-&gt;sess_id="sess_id"
    if (rd-&gt;sess_id[0] != 0) {
        // Check if session ID is correct for given user ID
        char *email;
        run-query @db_multitenant_SaaS = "select email from users where userId='%s' and session='%s'" output email : rd-&gt;sess_userId, rd-&gt;sess_id row-count define rcount
            query-result email to email
        end-query
        if (rcount == 1) {
            // if correct, set logged-in flag
            rd-&gt;is_logged_in = true;
            // if Logout link not display, then display it
            if (rd-&gt;displayed_logout == false) {
                @Hi &lt;&lt;p-out email&gt;&gt;! &lt;a href="https://opensource.com/?req=login&amp;action=logout"&gt;Logout&lt;/a&gt;&lt;br/&gt;
                rd-&gt;displayed_logout = true;
            }
        } else rd-&gt;is_logged_in = false;
    }
}</code></pre><h3>Signing up, Logging in, Logging out (login.vely)</h3>
<p>The basis of any multitenant system is the ability for a user to sign up, log in, and log out. Typically, signing up involves verifying the email address; more often than not, the same email address is used as a username. That's the case here.</p>
<p>There are several subrequests implemented here that are necessary to perform the functionality:</p>
<ul><li>When Signing Up a new user, display the HTML form to collect the information. The URL request signature for this is <code>req=login&amp;action=newuser</code>.</li>
<li>As a response to the Sign Up form, create a new user. The URL request signature is <code>req=login&amp;action=createuser</code>. The <strong>input-param</strong> signal obtains an <strong>email</strong> and <strong>pwd</strong> POST form fields. The password value is a one-way hash, and an email verification token is created as a random five-digit number. These are inserted into the <strong>users</strong> table, creating a new user. A verification email is sent, and the user is prompted to read the email and enter the code.</li>
<li>Verify the email by entering the verification code sent to that email. The URL request signature is <code>req=login&amp;action=verify</code>.</li>
<li>Display a Login form for the user to log in. The URL request signature is <code>req=login</code> (for instance, <code>action</code> is empty.)</li>
<li>Log in by verifying the email address (username) and password. The URL request signature is <code>req=login&amp;action=login</code>.</li>
<li>Logout at the user's request. The URL request signature is <code>req=login&amp;action=logout</code>.</li>
<li>Landing page for the application. The URL request signature is <code>req=login&amp;action=begin</code>.</li>
<li>If the user is currently logged in, go to the application's landing page.</li>
</ul><p>See examples of these below:</p>
<pre>
<code class="language-text">#include "vely.h"
#include "login.h"

// Handle session maintenance, login, logout, session verification
// for any multitenant Cloud application
void login () {
    // Get URL input parameter "action"
    input-param action

    // Get global request data, we record session information in it, so it's handy
    reqdata *rd;
    get-req data to rd

    // If session is already established, the only reason why we won't proceed to
    // application home is if we're logging out
    if (rd-&gt;is_logged_in) {
        if (strcmp(action, "logout")) {
            _show_home();
            exit-request
        }
    }

    // Application screen to get started. Show links to login or signup and show
    // home screen appropriate for this
    if (!strcmp (action, "begin")) {
        _show_home();
        exit-request

    // Start creating new user. Ask for email and password, then proceed to create user
    // when this form is submitted.
    } else if (!strcmp (action, "newuser")) {
        @Create New User&lt;hr/&gt;
        @&lt;form action="https://opensource.com/?req=login" method="POST"&gt;
        @&lt;input name="action" type="hidden" value="createuser"&gt;
        @&lt;input name="email" type="text" value="" size="50" maxlength="50" required autofocus placeholder="Email"&gt;
        @&lt;input name="pwd" type="password" value="" size="50" maxlength="50" required placeholder="Password"&gt;
        @&lt;input type="submit" value="Sign Up"&gt;
        @&lt;/form&gt;

    // Verify code sent to email by user. The code must match, thus verifying email address    
    } else if (!strcmp (action, "verify")) {
        input-param code
        input-param email
        // Get verify token based on email
        run-query @db_multitenant_SaaS = "select verify_token from users where email='%s'" output db_verify : email
            query-result db_verify to define db_verify
            // Compare token recorded in database with what user provided
            if (!strcmp (code, db_verify)) {
                @Your email has been verifed. Please &lt;a href="https://opensource.com/?req=login"&gt;Login&lt;/a&gt;.
                // If matches, update user info to indicate it's verified
                run-query @db_multitenant_SaaS no-loop = "update users set verified=1 where email='%s'" : email
                exit-request
            }
        end-query
        @Could not verify the code. Please try &lt;a href="https://opensource.com/?req=login"&gt;again&lt;/a&gt;.
        exit-request

    // Create user - this runs when user submits form with email and password to create a user     
    } else if (!strcmp (action, "createuser")) {
        input-param email
        input-param pwd
        // create hashed (one-way) password
        hash-string pwd to define hashed_pwd
        // generate random 5 digit string for verify code
        random-string to define verify length 5 number
        // create user: insert email, hashed password, verification token. Current verify status is 0, or not verified
        begin-transaction @db_multitenant_SaaS
        run-query @db_multitenant_SaaS no-loop = "insert into users (email, hashed_pwd, verified, verify_token, session) values ('%s', '%s', '0', '%s', '')" : email, hashed_pwd, verify affected-rows define arows error define err on-error-continue
        if (strcmp (err, "0") || arows != 1) {
            // if cannot add user, it probably doesn't exist. Either way, we can't proceed.
            login_or_signup();
            @User with this email already exists.
            rollback-transaction @db_multitenant_SaaS
        } else {
            // Create email with verification code and email it to user
            write-string define msg
                @From: vely@vely.dev
                @To: &lt;&lt;p-out email&gt;&gt;
                @Subject: verify your account
                @
                @Your verification code is: &lt;&lt;p-out verify&gt;&gt;
            end-write-string
            exec-program "/usr/sbin/sendmail" args "-i", "-t" input msg status define st
            if (st != 0) {
                @Could not send email to &lt;&lt;p-out email&gt;&gt;, code is &lt;&lt;p-out verify&gt;&gt;
                rollback-transaction @db_multitenant_SaaS
                exit-request
            }
            commit-transaction @db_multitenant_SaaS
            // Inform the user to go check email and enter verification code
            @Please check your email and enter verification code here:
            @&lt;form action="https://opensource.com/?req=login" method="POST"&gt;
            @&lt;input name="action" type="hidden" value="verify" size="50" maxlength="50"&gt;
            @&lt;input name="email" type="hidden" value="&lt;&lt;p-out email&gt;&gt;"&gt;
            @&lt;input name="code" type="text" value="" size="50" maxlength="50" required autofocus placeholder="Verification code"&gt;
            @&lt;button type="submit"&gt;Verify&lt;/button&gt;
            @&lt;/form&gt;
        }

    // This runs when logged-in user logs out.    
    } else if (!strcmp (action, "logout")) {
        // Update user table to wipe out session, meaning no such user is logged in
        if (rd-&gt;is_logged_in) {
            run-query @db_multitenant_SaaS = "update users set session='' where userId='%s'" : rd-&gt;sess_userId no-loop affected-rows define arows
            if (arows == 1) {
                rd-&gt;is_logged_in = false; // indicate user not logged in
                @You have been logged out.&lt;hr/&gt;
            }
        }
        _show_home();

    // Login: this runs when user enters user name and password
    } else if (!strcmp (action, "login")) {
        input-param pwd
        input-param email
        // create one-way hash with the intention of comparing with user table - password is NEVER recorded
        hash-string pwd to define hashed_pwd
        // create random 30-long string for session ID
        random-string to rd-&gt;sess_id length 30
        // Check if user name and hashed password match
        run-query @db_multitenant_SaaS = "select userId from users where email='%s' and hashed_pwd='%s'" output sess_userId : email, hashed_pwd
            query-result sess_userId to rd-&gt;sess_userId
            // If match, update user table with session ID
            run-query @db_multitenant_SaaS no-loop = "update users set session='%s' where userId='%s'" : rd-&gt;sess_id, rd-&gt;sess_userId affected-rows define arows
            if (arows != 1) {
                @Could not create a session. Please try again. &lt;&lt;.login_or_signup();&gt;&gt; &lt;hr/&gt;
                exit-request
            }
            // Set user ID and session ID as cookies. User's browser will return those to us with every request
            set-cookie "sess_userId" = rd-&gt;sess_userId
            set-cookie "sess_id" = rd-&gt;sess_id
            // Display home, make sure session is correct first and set flags
            _check_session();
            _show_home();
            exit-request
        end-query
        @Email or password are not correct. &lt;&lt;.login_or_signup();&gt;&gt;&lt;hr/&gt;

    // Login screen, asks user to enter user name and password    
    } else if (!strcmp (action, "")) {
        login_or_signup();
        @Please Login:&lt;hr/&gt;
        @&lt;form action="https://opensource.com/?req=login" method="POST"&gt;
        @&lt;input name="action" type="hidden" value="login" size="50" maxlength="50"&gt;
        @&lt;input name="email" type="text" value="" size="50" maxlength="50" required autofocus placeholder="Email"&gt;
        @&lt;input name="pwd" type="password" value="" size="50" maxlength="50" required placeholder="Password"&gt;
        @&lt;button type="submit"&gt;Go&lt;/button&gt;
        @&lt;/form&gt;
    }
}

// Display Login or Sign Up links
void login_or_signup() {
        @&lt;a href="https://opensource.com/?req=login"&gt;Login&lt;/a&gt; &amp; &amp; &lt;a href="https://opensource.com/?req=login&amp;action=newuser"&gt;Sign Up&lt;/a&gt;&lt;hr/&gt;
}</code></pre><h3>General-purpose application (_show_home.vely)</h3>
<p>With this tutorial, you can create any multitenant SaaS application you want. The multitenant-processing module above (<code>login.vely</code>) calls the <strong>_show_home()</strong> function, which can house any code of yours. This example code shows the Notes application, but it could be anything. The <strong>_show_home()</strong> function calls any code you wish and is a general-purpose multitenant application plug-in:</p>
<pre>
<code class="language-text">#include "vely.h"

void _show_home() {
    notes();
    exit-request
}</code></pre><h3>Notes application (notes.vely)</h3>
<p>The application is able to add, list, and delete any given note:</p>
<pre>
<code class="language-text">#include "vely.h"
#include "login.h"

// Notes application in a multitenant Cloud
void notes () {
    // get global request data
    reqdata *rd;
    get-req data to rd
    // If session invalid, display Login or Signup
    if (!rd-&gt;is_logged_in) {
        login_or_signup();
    }
    // Greet the user
    @&lt;h1&gt;Welcome to Notes!&lt;/h1&gt;&lt;hr/&gt;
    // If not logged in, exit - this ensures security verification of user's identity
    if (!rd-&gt;is_logged_in) {
        exit-request
    }
    // Get URL parameter that tells Notes what to do
    input-param subreq
    // Display actions that Notes can do (add or list notes)
    @&lt;a href="https://opensource.com/?req=notes&amp;subreq=add"&gt;Add Note&lt;/a&gt; &lt;a href="https://opensource.com/?req=notes&amp;subreq=list"&gt;List Notes&lt;/a&gt;&lt;hr/&gt;

    // List all notes for this user
    if (!strcmp (subreq, "list")) {
        // select notes for this user ONLY
        run-query @db_multitenant_SaaS = "select dateOf, note, noteId from notes where userId='%s' order by dateOf desc" : rd-&gt;sess_userId output dateOf, note, noteId
            query-result dateOf to define dateOf
            query-result note to define note
            query-result noteId to define noteId
            // change new lines to &lt;br/&gt; with fast cached Regex
            match-regex "\n" in note replace-with "&lt;br/&gt;\n" result define with_breaks status define st cache
            if (st == 0) with_breaks = note; // nothing was found/replaced, just use original
            // Display a note
            @Date: &lt;&lt;p-out dateOf&gt;&gt; (&lt;a href="https://opensource.com/?req=notes&amp;subreq=delete_note_ask&amp;note_id=%3C%3Cp-out%20noteId%3E%3E"&gt;delete note&lt;/a&gt;)&lt;br/&gt;
            @Note: &lt;&lt;p-out with_breaks&gt;&gt;&lt;br/&gt;
            @&lt;hr/&gt;
        end-query
    }

    // Ask to delete a note
    else if (!strcmp (subreq, "delete_note_ask")) {
        input-param note_id
        @Are you sure you want to delete a note? Use Back button to go back, or &lt;a href="https://opensource.com/?req=notes&amp;subreq=delete_note&amp;note_id=%3C%3Cp-out%20note_id%3E%3E"&gt;delete note now&lt;/a&gt;.
    }

    // Delete a note
    else if (!strcmp (subreq, "delete_note")) {
        input-param note_id
        // Delete note
        run-query @db_multitenant_SaaS = "delete from notes where noteId='%s' and userId='%s'" : note_id, rd-&gt;sess_userId affected-rows define arows no-loop error define errnote
        // Inform user of status
        if (arows == 1) {
            @Note deleted
        } else {
            @Could not delete note (&lt;&lt;p-out errnote&gt;&gt;)
        }
    }

    // Add a note
    else if (!strcmp (subreq, "add_note")) {
        // Get URL POST data from note form
        input-param note
        // Insert note under this user's ID
        run-query @db_multitenant_SaaS = "insert into notes (dateOf, userId, note) values (now(), '%s', '%s')" : rd-&gt;sess_userId, note affected-rows define arows no-loop error define errnote
        // Inform user of status
        if (arows == 1) {
            @Note added
        } else {
            @Could not add note (&lt;&lt;p-out errnote&gt;&gt;)
        }
    }

    // Display an HTML form to collect a note, and send it back here (with subreq="add_note" URL param)
    else if (!strcmp (subreq, "add")) {
        @Add New Note
        @&lt;form action="https://opensource.com/?req=notes" method="POST"&gt;
        @&lt;input name="subreq" type="hidden" value="add_note"&gt;
        @&lt;textarea name="note" rows="5" cols="50" required autofocus placeholder="Enter Note"&gt;&lt;/textarea&gt;
        @&lt;button type="submit"&gt;Create&lt;/button&gt;
        @&lt;/form&gt;
    }
}</code></pre><h2>SaaS with C performance</h2>
<p>Vely makes it possible to leverage the power of C in your web applications. A multitenant SaaS application is a prime example of a use case that benefits from that. Take a look at the code examples, write some code, and give Vely a try.</p>
