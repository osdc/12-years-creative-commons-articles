<p>Ah, June. A month of springing flowers and gardens over here in the United States, and possibly also for you wherever you are. If not, I'll send you some right now.</p>
<p class="rtecenter">
<article class="media media--type-image media--view-mode-full" title="Dandelion bouquet"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/8941161679_04ea8b3e24_z.jpg" width="576" height="640" alt="Dandelion bouquet" title="Dandelion bouquet" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p> </p>
<p><a data-rapid_p="71" data-track="attributionNameClick" href="https://www.flickr.com/photos/commonweeds/" title="Go to Dandelion Vicious's photostream" rel="ugc">Dandelion Vicious</a>, Flickr. CC BY-SA 2.0</p>
</div>
      
  </article></p>
<p>Our editorial outlook seems to have taken on a similar appearance.</p>
<h2>Series</h2>
<p>Popping up this month is a series around "<a href="https://opensource.com/node/29526">open source for entertainment</a>," including projects for home automation and working with movies and videos.</p>
<h2>Anniversaries</h2>
<p>We'll celebrate a couple of important birthdays: the <a href="https://opensource.com/node/15279">GPLv3</a> turns license turns 10 and the 1.0 release of <a href="https://opensource.com/node/25696">GIMP</a> turns 19! </p>
<p>Share your experiences with them with us in the comments or on social media.</p>
<h2>The Open Org</h2>
<p>Tomorrow is a big day for the open organization community at Opensource.com: the launch of the long-awaited <em>Guide to IT Culture Change</em>, the fifth book in the <a href="https://opensource.com/node/28446"><em>Open Organization</em></a> book series.</p>
<p><strong>Be sure to join us on June 2 for the book's official launch to grab your own copy of the free eBook.</strong></p>
<p>Also in June, <a href="https://opensource.com/node/24211">Open Org Ambassador</a> Allison Matlack explains why open leaders are more powerful than they think, DeLisa Alexander continues her series on scaling open cultures, and Justin Holmes explores the power of impact mapping techniques for teams looking to work openly.</p>
