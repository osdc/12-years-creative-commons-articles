<p>It's incredible to think about how much has changed for technology companies in the last decade. Start ups pop up seemingly by the hour. The most financially successful company on earth freely shared an <a href="https://www.apple.com/newsroom/2015/12/03Apple-Releases-Swift-as-Open-Source/" target="_blank">entire programming language</a>. And the open source community is thriving in ways we didn't think possible.</p>
<p>In short, transparency seems to be spreading in many different forms.</p>
<p>It used to be the case that some facet of leadership didn't want information freely and transparently shared across an organization. But it's different now. The prospect of sharing your business' most sacred commodities or secrets is no longer off limits. The way we think about transparency in the workplace is changing, and we all seem to be keenly aware of it.</p>
<p>It's exciting, but what does transparency really <em>do</em> for a company? How does it help, and what problems does it solve? How can it go from being a great one liner on your company's "About" page and grow into a tool to leverage across your teams?</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Learn about open organizations</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://theopenorganization.org">Download resources</a></li>
<li><a href="https://theopenorganization.community">Join the community</a></li>
<li><a href="https://opensource.com/open-organization/resources/open-org-definition?src=too_resource_menu3a">What is an open organization?</a></li>
<li><a href="https://opensource.com/open-organization/resources/open-org-maturity-model?src=too_resource_menu4a">How open is your organization?</a></li>
</ul></div>
</div>
</div>
</div>

<p>I work as an iOS developer at Buffer, one of <a href="https://www.inc.com/jeff-haden/inside-buffer-company-complete-transparency.html" target="_blank">the most open tech companies around</a>. I've seen transparency in action for several years now and firmly believe it can streamline solutions to many different problems.</p>
<p>I'll walk you through a recent challenge we faced internally at Buffer and show how transparency helped play a pivotal role in fixing it.</p>
<h2>You're transparent—now what?</h2>
<p>At Buffer, we've instilled transparency as a core value impacting the way we do things across the company. Our revenues, profits, and stock information is open for anyone to see. Our employee salaries are public domain. Even <a href="https://buffer.com/transparency" target="_blank">our product road map is accessible</a> to anyone that has access to the internet.</p>
<p></p><div class="embedded-callout-text callout-float-right">Even though we were a transparent company, we needed to learn to leverage that freedom of choice, knowledge, and autonomy in an deliberate, actionable way.</div>
<p>You'd think that as a transparent tech company, we'd be heavily involved with the open source community too. It should be inherent to our culture. And it was, but really only in the sense that we <em>used</em> plenty of open source code. We just weren't sharing much of our own.</p>
<p>Why?</p>
<p>I spent time really grappling with that question. How come we weren't sharing more code to the open source community?</p>
<p>What I discovered is that even though we were a transparent company, we needed to learn to <em>leverage</em> that freedom of choice, knowledge, and autonomy in an deliberate, actionable way.</p>
<p>In this case, transparency was both part of our problem and nearly the entire solution.</p>
<h2>Diagnosing issues via transparent communication</h2>
<p>We only recently discovered this open source contribution gap at Buffer. It's something that most of us just assumed was happening due to our transparent nature. Sure, there were a few projects we had open to the public, but there were certainly things we could've released and made the community more aware of.</p>
<p>It was something our CEO, Joel Gascoigne, noticed, too. In our internal tool for communication, Discourse, Joel raised the question more or less asking "Why do we not open source more of our code?"</p>
<p>It was a fair question to ask, and one that would've just began and ended as a casual talking point in a meeting—if it hadn't been posed to the entire company.</p>
<p>This is the first point at which transparency worked to solve a problem we had. It made us realize which people were in a position to take on this challenge—and how they were spread far and wide across the company. It also initiated a dialog between all of us in a natural way, to get the conversation started. It's a stark contrast to the closed doors, "higher ups" meetings that seemed to be standard at other companies I've worked for.</p>
<p>From talking around the company, it was clear that we were in a unique position to share our code with the world. Our CEO approved of doing it, our direct management thought it was a great practice, and our engineers were eager and willing to contribute.</p>
<p>For us at Buffer, the issue was never "we shouldn't do this" or "we don't have the time." In fact, it was quite the opposite. By chatting with different engineering teams, I soon learned that the problem was simply that we didn't know how.</p>
<h2>Finding the answer</h2>
<p>Arriving at that conclusion took some digging. I started taking notes about the barriers engineers claimed they encountered when attempting to work with open source code, and I posted those to a public document for others to collaborate on. After three or four informal chats, all of the engineering teams had created a rough outline of any roadblocks that existed and the various forms of stop energy they were hitting.</p>
<p>The above paragraph will either seem very practical and uninteresting to you—or radical and different depending on the type of company you work for.</p>
<p>I've been in both corners of the room: the one where the managers take action without much input from the team and works alone to produce change, and the one where everyone is called upon to be part of the solution and identify the real, common problems.</p>
<p>When you practice the latter more than the former, you start to cultivate a sense of empathy for your team—as well as a stronger degree of trust. You don't have all the answers; no single teammate likely does. But if you form them all together, a clearer sense of direction results.</p>
<p>At Buffer, I ended up creating nothing more than a public FAQ that helped engineers open source their code. It had the answers to the questions we were asking <em>ourselves</em> but not <em>each other</em>, and it outlined logistical steps to making code public (along with few tips on how to share it).</p>
<p>That's exactly all we needed: fewer assumptions on the state of open source and a quick write-up explaining how to share our code. Since the creation of that simple FAQ guide, 80% of our open source projects have been released.</p>
<p>The best part? We've since <a href="https://open.buffer.com/guide-open-source/" target="_blank">shared that FAQ with the world</a> too, in the hope that it could help solve the same problems at other tech companies.</p>
<h2>Leveraging transparency</h2>
<p>That small example of transparency in action might seem to have a bit of an anticlimactic ending—until you consider the way it helped us get where it did.</p>
<p>First, our CEO initiated a dialogue across the board about the state of open source. Then, engineering teams started to realize that not many of us were actually open sourcing code. At this point, the floor was open to let anyone who felt they could help solve the problem to freely jump in.</p>
<p></p><div class="embedded-callout-text callout-float-right">I made sure anyone who had ideas on how to get open source rolling would have their ideas heard, and went to all of the teams to figure out ways to do it.</div>
<p>In this case, that was me. I made sure anyone who had ideas on how to get open source rolling would have their ideas heard, and went to all of the teams to figure out ways to do it. This was quick to do, because at this point we were all aware of the problem I was trying to help solve. The proper steps forward came into focus directly from this clear, encouraging, and egoless communication.</p>
<p>Now, consider this: What if Buffer did <em>not</em> have a culture of transparency in this particular case? What might've happened, and why did transparency help?</p>
<p>Traditionally, managers might've huddled together and thought of ways to encourage their team. A brainstorm meeting likely would have occurred, and people might have tossed ideas back and forth. Then they may have agreed on a course of action put it into place.</p>
<p>Business has been done this way for a long time, and there is certainly nothing right or wrong about those methods. But we're not asking if it's right or wrong; we're pondering how transparency helps in the workplace.</p>
<p>In that case, then, we can anticipate problems arising from the fact that (in this scenario, anyway) the people working toward solving the "end goal"—in this case, open sourcing code—were not the ones involved in making the decision on how to do it. The <em>real</em> issues that may have existed or the barriers in place might not have ever been known to the managers trying to solve the problem.</p>
<p>Further, managers are just that: managers. They are skilled, empathetic team leaders who help ensure their units succeed and have what they need to do so. But they aren't typically on the front lines, like the employees they manage. When they work without the context of their front-line teammates, they often have difficulty getting the amount of information they need to get at the heart of an issue. At worst, their work becomes a series of wild guesses.</p>
<p>This works both ways, too, as managers have valuable insight that those who are on the front lines often don't. Considering this, it stands to reason that the best way to make work more efficient is to ensure that both sides have an open line of communication they can use to bounce ideas back and forth. If that line only travels one way or the other, then communication becomes less collaborative, informative, and helpful—and more of a firm decision that seemingly came out of nowhere.</p>
<p>Consider that last point for a moment. In this case, not only did the solution to open source more code come from those without all the context required, but it could also can come across as an unexpected failure to the team. Thoughts like "Where we not doing a great job here?" or "I didn't even know that was a priority" could be common.</p>
<p>Thankfully, everyone can effectively side-step scenarios like that with open, honest, and collaborative communication. Transparency is not a silver bullet for your entire business—but it certainly can help when it comes to fostering empathy, knowledge, and trust among your team.</p>
<h2>Considering transparency</h2>
<p>We've seen that transparency in the workplace can be a very powerful tool. If used properly, it can ensure that the right people have access to the right things. It can enable outside perspectives and additional context that might've been lost without it. If you choose to make some parts of your organization (or all of it!) embrace transparency, then be willing to tackle problems that your team encounters and be passionate about solving them.</p>
<p>But your actions have to meet your ambition. Simply <em>wanting to be</em> transparent isn't enough.</p>
<p></p><div class="embedded-callout-text callout-float-right">You wouldn't buy a house without first asking your partner or family what would work best for them, what price is most affordable, or where you should live.</div>
<p>At Buffer, we wanted to open source more code, we had the green light to do it, we had plenty of helpful code to share—but <em>wanting</em> to share it was usually where the story ended. We had to bring all of these ideas together into one, cohesive, and comprehensible flow—a flow formed by taking input from the whole team, leaders, and the community to create something truly useful.</p>
<p>The magic, then, comes about when you encourage your team to be transparent while matching all the ideas that sprout up into decisions taking place - either now or later. The magic, then, arrives as you encourage your team to be transparent and commit to hearing everyone's input. You'll likely find yourself with more ideas than you'd have gotten if you'd taken a different approach. Take your time, weigh the options, and follow through. Then you'll be in a great place to take some action. Organizations have never lacked decision makers. What they <em>have</em> historically lacked is important context.</p>
<p>You wouldn't buy a house without first asking your partner or family what would work best for them, what price is most affordable, or where you should live. You open a dialog, allow others to be a part of it, and <em>then</em> get things done.</p>
<p>The same should be true in any organization today, and transparency can help foster it.</p>
<p><em>This article is part of the <a href="https://opensource.com/open-organization/17/8/workbook-project-announcement">Open Organization Workbook project</a>.</em></p>
