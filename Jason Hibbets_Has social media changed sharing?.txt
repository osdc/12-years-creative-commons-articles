<p>We like to highlight the values of the open source way like collaboration, meritocracy, transparency, and sharing here on opensource.com. We share ideas, best practices, content, images, opinions, and much more. But has "sharing" changed with the increased use of social media?</p>
<!--break-->
<p>In a chapter titled <em>Personal, Communal, Public, Civic</em> in Clay Shirly's <a title="Cognitive Surplus" href="http://en.wikipedia.org/wiki/Cognitive_Surplus:_Creativity_and_Generosity_in_a_Connected_Age" target="_blank">Cognitive Surplus</a>, readers are asked to consider this impact.</p>
<blockquote><p>The spread of social media that allows for public address has led to a subtle change in the word <em>share</em>. Sharing has typically required a high degree of connection between donor and recipient, so the idea of sharing a photograph implied that you knew the sharees. Such sharing tended to be a reciprocal and coordinated action amoung people who new one another. But now that social media has dramatically lengthened the radius and half-life of sharing, the organization of sharing has many forms.</p>
</blockquote>
<h3>
<hr />What do you think?</h3>
<p>Has social media made this subtle change and created different levels of sharing ranging from personal to communal and public to civic? Or is social media just another Internet tool where sharing is a side effect?</p>
