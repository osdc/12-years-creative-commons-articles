<p>I’m willing to bet that you do some (probably more than some) reading in a web browser. That reading might include everything from blog posts, articles, and essays to long forum posts and more.</p>
<p>The problem with reading in a browser window is that it's full of distractions. Elements like navigation tools, headers and footers, ads, link boxes, and more can slow your reading and cause frustration.</p>
<p>Let’s take a look at four open source tools that can get rid of those distractions.</p>
<h2>wallabag</h2>
<p><a href="https://wallabag.org/en" target="_blank">wallabag</a>, which I <a href="https://opensource.com/life/14/4/open-source-read-it-later-app-wallabag" target="_blank">looked at in 2014</a>, is an open source alternative to apps like <a href="https://www.instapaper.com/" target="_blank">Instapaper</a> and <a href="https://getpocket.com/" target="_blank">Pocket</a>. It’s a web application that removes the cruft from web pages, leaving you with clear text and the occasional image.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="wallabag screenshot"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/wallabag.png" width="1294" height="890" alt="wallabag screenshot" title="wallabag screenshot" /></div>
      
  </article></p>
<p>wallabag makes for a very clean reading experience. You can also add annotations to an article; share it via email, Twitter, or a public link; and export it in several formats including EPUB, plain text, and PDF. You can also tag articles to organize them and use themes to change the look and feel of the application.</p>
<p>wallabag is primarily a self-hosted app, but don’t worry if you don’t have a server on which to run it. If you’re willing part with €9 (around $11 USD at the time I wrote this article) a year, you can get a subscription to the <a href="https://www.wallabag.it/en" target="_blank">hosted version of wallabag</a>.</p>
<h2>Firefox Reader View</h2>
<p>Do you use Firefox? If so, you can take advantage of a built-in tool that eliminates distractions from whatever you’re reading online.</p>
<p>That tool is called Firefox Reader View. To use it, simply head over to a web page and click the <strong>Toggle reader view</strong> icon in the address bar, or press CTRL+ALT+R. Firefox transforms a cluttered web page into a single column of easy-to-read, text-only goodness.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="firefox screenshot"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/firefox_1.png" width="1418" height="577" alt="firefox screenshot" title="firefox screenshot" /></div>
      
  </article></p>
<p>You can click the <strong>Type Controls</strong> icon on the sidebar to change the font (between serif and sans-serif), the size of that font, and the width of the column in which your text appears. You can also modify the space between lines and change the background color from black on white to white on black or a sepia tone.</p>
<h2>McReadability</h2>
<p>Although it’s no longer supported (and the developer <a href="https://github.com/anoved/mcreadability/issues/5" target="_blank">suggests</a> that you use Firefox Reader View), you might want to give <a href="http://anoved.github.io/mcreadability/" target="_blank">McReadability</a> a try.</p>
<p>It’s a <a href="https://en.wikipedia.org/wiki/Bookmarklet" target="_blank">bookmarklet</a> based on the late, lamented <a href="https://en.wikipedia.org/wiki/Readability_(service)" target="_blank">Readability service</a>. Click the bookmarklet, and it reformats the page as a series of short columns. You read down and across rather than down and up. (Yeah, it takes some getting used to.)</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="mcreadability screenshot"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/mcreadability.png" width="1562" height="945" alt="mcreadability screenshot" title="mcreadability screenshot" /></div>
      
  </article></p>
<p>You can customize the bookmarklet to change the column style, the width of columns, and the size of the font. On the downside, since it’s no longer actively supported, not all web pages play nicely with McReadability.</p>
<h2>Text-mode browsers</h2>
<p>The most effective way to get rid of distractions on a web page is to go with plain text. And the most effective way to do that is to go old-school and use a text-mode web browser. I’m talking about browsers like the venerable <a href="https://en.wikipedia.org/wiki/Lynx_(web_browser)" target="_blank">Lynx</a>.</p>
<p>The advantages of using a text mode are obvious. The browser uses minimal system resources, pages load faster, and anything that is not text on a web page goes bye-bye. No need for web apps, browser plugins, or anything like that.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="browser screenshot"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/browser.png" width="649" height="328" alt="browser screenshot" title="browser screenshot" /></div>
      
  </article></p>
<p>Need a few suggestions for a text-mode browser? Check out <a href="https://opensource.com/article/16/12/web-browsers-linux-command-line" target="_blank">my article</a> on web browsers for the Linux command line.</p>
<p>Do you have a favorite open source tool for eliminating distractions from your online reading? Share it by leaving a comment.</p>
