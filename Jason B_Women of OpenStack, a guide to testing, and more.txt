<p>Interested in keeping track of what's happening in the open source cloud? Opensource.com is your source for what's happening right now in <a href="https://opensource.com/resources/what-is-openstack" target="_blank" title="What is OpenStack?">OpenStack</a>, the open source cloud infrastructure project.</p>
<p><!--break--></p>
<h3>OpenStack around the web</h3>
<p>There's a lot of interesting stuff being written about OpenStack. Here's a sampling:</p>
<ul><li><a href="http://www.openstack.org/blog/2015/01/women-of-openstack-working-session-update/" target="_blank" title="OpenStack news">Women of OpenStack working session update</a>: What came out of the Paris meetup?</li>
<li><a href="http://superuser.openstack.org/articles/a-guide-to-testing-in-openstack" target="_blank" title="OpenStack news">A guide to testing in OpenStack</a>: Get the basics on how to start running tests in OpenStack.</li>
<li><a href="http://sarob.com/2015/01/openstack-evolution/" target="_blank" title="OpenStack news">OpenStack evolution</a>: Changing the OpenStack project structure.</li>
<li><a href="http://www.zdnet.com/article/the-future-is-in-hybrid-clouds/" target="_blank" title="OpenStack news">The future is in hybrid clouds</a>: How enterprises are preparing for what's coming next.</li>
</ul><p>And just for fun...</p>
<ul><li><a href="http://openstackreactions.enovance.com/2015/01/endmeeting/" target="_blank" title="OpenStack reaction">OpenStack reactions</a>: #endmeeting</li>
</ul><h3>OpenStack discussions</h3>
<p>Here's a sample of some of the most active discussions this week on the OpenStack developers' listserv. For a more in-depth look, why not <a href="http://lists.openstack.org/pipermail/openstack-dev/" target="_blank" title="OpenStack listserv">dive in</a> yourself?</p>
<ul><li><a href="http://lists.openstack.org/pipermail/openstack-dev/2015-January/054122.html" target="_blank" title="OpenStack discussion">Vancouver Design Summit format changes</a>: How can these developer events be more productive?</li>
<li><a href="http://lists.openstack.org/pipermail/openstack-dev/2015-January/054107.html" target="_blank" title="OpenStack discussion">Python 2.6 for clients</a>: Maintaining compatibility for all client tools.</li>
<li><a href="http://lists.openstack.org/pipermail/openstack-dev/2015-January/054083.html" target="_blank" title="OpenStack discussion">Question about scheduling two instances to same baremetal node</a>: Steps to resolving a bug.</li>
</ul><h3>OpenStack events</h3>
<p>Here's what is happening this week. Something missing? <a href="mailto:osdc-admin@redhat.com">Email</a> us and let us know, or add it to our <a href="https://opensource.com/resources/conferences-and-events-monthly" target="_blank" title="Events Calendar">events calendar</a>. </p>
<ul><li><a href="http://www.meetup.com/OpenStackTO/events/219195501/" target="_blank" title="OpenStack news" style="line-height: 1.538em; background-color: transparent;">Storage and Network with CloudFounders and Midokura</a>: January 12, 2015; Toronto, ON</li>
<li><a href="http://www.meetup.com/Cloud-Online-Meetup/events/219714843/" target="_blank" title="OpenStack event">Build a rock-solid foundation under your OpenStack cloud</a>: January 13, 2015; Online meetup</li>
<li><a href="http://www.meetup.com/Indian-OpenStack-User-Group/events/219637729/" target="_blank" title="OpenStack event">OpenStack India meetup</a>: January 13, 2015; New Delhi, India</li>
<li><a href="http://www.meetup.com/Calgary-OpenStack-Meetup/events/219503704/" target="_blank" title="OpenStack event">First Calgary OpenStack meetup</a>: January 13, 2015; Calgary, AB</li>
<li><a href="http://www.meetup.com/Openstack-Boston/events/218862617/" target="_blank" title="OpenStack event">OpenStack Neutron networking and more</a>: January 14, 2015; Boston, MA</li>
<li><a href="http://www.meetup.com/chicagoacm/events/219410203/" target="_blank" title="OpenStack event">At the crossroads of HPC and cloud computing with OpenStack</a>: January 14, 2015; Chicago, IL</li>
<li><a href="http://www.meetup.com/OpenStack-Israel/events/219611133/" target="_blank" title="OpenStack event">DevOps for OpenStack—migrating a major bank to OpenStack</a>: January 14, 2015; Tel Aviv, Israel</li>
<li><a href="http://www.meetup.com/OpenStack-Austin/events/218860202/" target="_blank" title="OpenStack event">Deep dive into Trove and preview OpenStack Kilo</a>: January 15, 2015; Austin, TX</li>
<li><a href="http://www.meetup.com/London-SDN-ODLUG/events/218972643/" target="_blank" title="OpenStack event">OpenDaylight and SDN meetup</a>: January 15, 2015; London, UK</li>
<li><a href="http://www.meetup.com/OpenStack-for-Enterprises-NYC/events/219241367/" target="_blank" title="OpenStack event">Mix-and-match­ your cloud</a>: January 15, 2015; New York, NY</li>
<li><a href="http://www.meetup.com/Lahore-OpenStack-Meetup/events/219586680/" target="_blank" title="OpenStack event">OpenStack introductory meetup</a>: January 16, 2015; Lahore, Pakistan</li>
</ul><p>Interested in the OpenStack project development meetings? A complete list of these meetings' regular schedules is available <a href="https://wiki.openstack.org/wiki/Meetings" target="_blank" title="OpenStack Meetings">here</a>.</p>
