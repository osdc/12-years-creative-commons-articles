<p>The topic of DevOps vs. Agile is almost like debating iPhone vs. Android—everyone has an opinion, and emotions can become heated, especially if people disagree.</p>
<p>After writing <em><a href="https://opensource.com/article/20/2/devops-vs-agile">DevOps v. Agile: What's the difference?</a></em> and reading the comments on the article, I wanted to add some more thoughts—including how some of my thinking has changed on the topic.</p>
<p>My perspective comes from where I am now but also where I have been. I used to be a systems administrator and infrastructure engineer, and now I am a senior scrum master with a major utility company in Missouri. (I actually was a scrum master before I was an admin or engineer, but I digress.)</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More DevOps resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/resources/devops?intcmp=7013a00000263HlAAI">What is DevOps?</a></li>
<li><a href="https://opensource.com/downloads/devops-hiring-guide?intcmp=7013a00000263HlAAI">The ultimate DevOps hiring guide</a></li>
<li><a href="https://opensource.com/downloads/devops-monitoring-guide?intcmp=7013a00000263HlAAI">DevOps monitoring tools guide</a></li>
<li><a href="https://opensource.com/downloads/devsecops?intcmp=7013a00000263HlAAI">Getting started with DevSecOps</a></li>
<li><a href="https://enterprisersproject.com/cheat-sheet-devops-glossary?intcmp=7013a00000263HlAAI">Download the DevOps glossary</a></li>
<li><a href="https://www.ansible.com/resources/ebooks/ansible-for-devops?intcmp=7013a00000263HlAAI">eBook: Ansible for DevOps</a></li>
<li><a href="https://opensource.com/tags/devops?src=devops_resource_menu2">Latest DevOps articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>My team consists of six frontend software engineers and IT programmer analysts, a business analyst, two product owners, and me. Recently, we learned that management wants our team to become a <a href="https://opensource.com/article/19/1/what-devsecops">DevSecOps</a> team, so our core scrum team is working with a DevSecOps team that is helping us make the transition. No one is naive to the fact that this will not be easy, but the DevSecOps team's experience gives us confidence that we can succeed.</p>
<p>Our team's manager recently hired a senior software engineer who will drive the DevSecOps goal. As a scrum master, I will continue to focus on continuous improvement. The team is fairly young, so they don't have expansive work experience, but they are smart and driven, and there is much room for greatness. In addition, our entire organization is going through an Agile transformation, so most people are new to all things Agile, including the <a href="https://agilemanifesto.org/" target="_blank">Agile Manifesto</a> and the <a href="https://www.scrumalliance.org/about-scrum/values" target="_blank">Five Scrum Values</a>.</p>
<p><article class="media media--type-image media--view-mode-full" title="Spray paint on a container"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/roomtogrow_kyleglenn.jpg" width="2500" height="1667" alt="Spray paint on a container" title="Spray paint on a container" /></div>
      
  </article></p>
<h2 id="agile-scrum-devops-and-more">Agile, Scrum, DevOps, and more</h2>
<p>There is a clear relationship between DevOps and Agile. Agile is the methodology, Scrum is the framework, and DevOps falls under the agile <a href="https://opensource.com/article/20/4/kanban-devops">umbrella</a> along with kanban, lean, large-scale Scrum, Extreme Programming, Crystal, and more. For example, our Scrum team is an Agile team that will operate as a DevSecOps team.</p>
<p>Neither DevOps nor Agile is about the tools. Rather, both are about the <a href="https://opensource.com/article/19/5/values-devops-mindset">mindset and culture</a>. When it is done right, teams think and act differently and achieve greater results, including faster software delivery, continuous integration (CI), continuous delivery (CD), continuous improvement, working software, faster solutions, more collaboration, and fewer silos. Additional results are seen in quality testing, better automation, and improved systems, processes, and practices.</p>
<h3 id="common-concepts">Common concepts</h3>
<p>Some of the Agile concepts they have in common are associated with the Agile Manifesto; the most familiar of the 12 principles are the first four:</p>
<ul><li>Individual and interactions over processes and tools</li>
<li>Working software over comprehensive documentation</li>
<li>Customer collaboration over contract negotiations</li>
<li>Responding to change over following a plan</li>
</ul><p>Some of the DevOps concepts they have in common are the CI/CD pipeline, optimizing software delivery and quality, a culture of innovation, service-level objectives and indicators (SLOs and SLIs), collaboration across teams, and automation.</p>
<h3 id="devops-and-agile-benefits">DevOps and Agile benefits</h3>
<p>DevOps speeds up things between developers and operations. Furthermore, even though DevOps isn't about the tools, the fact that the dev and ops teams use the same tech stack creates a shared language and empathy between the two. Our Scrum team uses Jira to track all bugs, enhancements, and team performance. Common DevOps tools are Jenkins, AWS, SonarQube, GitHub, Splunk, and Ansible. While the tools differ from team to team, the mindset and culture should be common across all.</p>
<p>DevOps also creates less division between dev and ops and a sense of understanding of what it's like to walk in each other's shoes because now they function as one.</p>
<p>Agile teams continuously deliver often and fast, adapting incrementally along the way. Working in two-week sprints appears to be the sweet spot for most software- or product-delivery teams. Agile teams may utilize DevOps principles in their work (e.g., implementing a CI/CD pipeline), and dev teams that work with ops are likely to work in the same two-week increments.</p>
<p>DevOps traditionally leads to continuous deployment, delivery, and integration. Teamwork is integrated; problems and failures are jointly owned by development, operations, and other entities, such as quality assurance (QA), testing, automation, etc.</p>
<h2 id="summing-up">Summing up</h2>
<p>I believe that Agile and DevOps breathe the same air, with many concepts and theories crossing between them.</p>
<p>While I have no doubts that there will be counter opinions and even some sharply worded disagreements with my opinions, I think we would all agree that Agile and DevOps seek to address complexity, improve quality, and innovate around software design.</p>
