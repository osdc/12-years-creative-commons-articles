<p>Starting today, shops and newsagents are stocking a computer magazine called <a href="https://www.raspberrypi.org/magpi/" target="_blank">The MagPi</a>, and as a world's first, this magazine comes with a free computer—literally stuck to the front cover. It's the newest Raspberry Pi release, called Pi Zero. This computer also goes on sale around the world for just $5.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on Raspberry Pi</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/resources/what-raspberry-pi?src=raspberry_pi_resource_menu1&amp;intcmp=701f2000000h4RcAAI">What is Raspberry Pi?</a></li>
<li><a href="https://opensource.com/downloads/raspberry-pi-guide?src=raspberry_pi_resource_menu2&amp;intcmp=701f2000000h4RcAAI">eBook: Guide to Raspberry Pi</a></li>
<li><a href="https://opensource.com/downloads/getting-started-raspberry-pi-cheat-sheet?src=raspberry_pi_resource_menu3&amp;intcmp=701f2000000h4RcAAI">Getting started with Raspberry Pi cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/kubernetes-raspberry-pi?src=raspberry_pi_resource_menu6&amp;intcmp=701f2000000h4RcAAI">eBook: Running Kubernetes on your Raspberry Pi</a></li>
<li><a href="https://www.redhat.com/en/resources/data-intensive-applications-hybrid-cloud-blueprint-detail?src=raspberry_pi_resource_menu7&amp;intcmp=701f2000000h4RcAAI">Whitepaper: Data-intensive intelligent applications in a hybrid cloud blueprint</a></li>
<li><a href="https://www.redhat.com/en/topics/edge-computing?src=raspberry_pi_resource_menu5&amp;intcmp=701f2000000h4RcAAI">Understanding edge computing</a></li>
<li><a href="https://opensource.com/tags/raspberry-pi?src=raspberry_pi_resource_menu4&amp;intcmp=701f2000000h4RcAAI">Our latest on Raspberry Pi </a></li>
</ul></div>
</div>
</div>
</div>

<p>Pi Zero is a tiny device and contains the first generation Raspberry Pi's BCM2835 chip, safely overclocked to 1GHz. Pi Zero packs the same great GPU as the regular Raspberry Pi, and comes with 512MB RAM. It runs Linux, and runs all the programs and applications any other Pi will—including Python, Sonic Pi, Java, a web browser, and much more. You can run a media center, teach programming with it, learn to make music, or embed it in a project—and it fits on your keyring!</p>
<p><article class="media media--type-image media--view-mode-full" title="Pi Zero"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/business-uploads/pi-zero_f1.png" width="520" height="293" alt="Pi Zero" title="Pi Zero" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p><a href="https://www.raspberrypi.org/" target="_blank" rel="ugc">Raspberry Pi Foundation</a>. <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a></p>
</div>
      
  </article></p>
<p style="text-align: center;"><em><sup><a href="https://www.raspberrypi.org/" target="_blank">Raspberry Pi Foundation</a>. <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank">CC BY-SA 4.0</a></sup></em></p>
<h2>How to get hold of one</h2>
<p><b>In the UK:</b> The MagPi can be found in <a href="http://www.whsmith.co.uk/" target="_blank">WHSmith</a>, <a href="http://www.sainsburys.co.uk/" target="_blank">Sainsbury's</a>, and <a href="http://www.tesco.com/" target="_blank">Tesco</a>—it costs £5.99 and comes with the Pi Zero for free.</p>
<p><b>In the US:</b> The December issue of The MagPi will be out soon in <a href="http://www.barnesandnoble.com/" target="_blank">Barnes &amp; Noble</a>.</p>
<p><b>Everyone:</b> You can buy the magazine online from the Raspberry Pi <a href="http://swag.raspberrypi.org/collections/magpi" target="_blank">swag store</a> or <a href="https://www.raspberrypi.org/magpi/subscribe/" target="_blank">order a subscription</a>. Alternatively, the Pi Zero itself is available to buy from the usual distributors—<a href="http://www.element14.com/community/welcome" target="_blank">element14</a> and <a href="http://uk.rs-online.com/web/" target="_blank">RS Components</a>—as well as the swag store and others.</p>
<h2>Getting started</h2>
<p>First look at Pi Zero:</p>
<p><article class="media media--type-image media--view-mode-full" title="Pi Zero"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/business-uploads/pi-zero_f2.png" width="520" height="246" alt="Pi Zero" title="Pi Zero" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p><a href="https://www.raspberrypi.org/" target="_blank" rel="ugc">Raspberry Pi Foundation</a>. <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a></p>
</div>
      
  </article></p>
<p style="text-align: center;"><em><sup><a href="https://www.raspberrypi.org/" target="_blank">Raspberry Pi Foundation</a>. <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank">CC BY-SA 4.0</a></sup></em></p>
<p>What will you need? If you want to use it like a full computer, you'll need a Micro SD card and some cables:</p>
<ul><li>Micro SD card loaded with NOOBS or Raspbian</li>
<li>Mini HDMI to HDMI adapter or cable</li>
<li>Micro USB adapter or hub</li>
<li>Micro USB power cable</li>
<li>USB mouse &amp; keyboard</li>
<li>HDMI monitor</li>
</ul><h2>Alternative use</h2>
<p>Alternatively, you can prepare your Micro SD card using a regular Raspberry Pi, set your code to run on boot, stick your card into the Zero, and as soon as it has power, your program will run. A great way to deploy code to an embedded project.</p>
<p>You can also connect to the Zero via SSH or VNC over wired or wireless connection and control it from another computer.</p>
<h2>GPIO?</h2>
<p>One of the most interesting things about Raspberry Pi is the GPIO pins that allow you to connect up real world components and program them for your hobby project or IoT smart home. The Zero doesn't have GPIO pins, as such, but it does expose pin holes—the same 40 pins you'll find on a Raspberry Pi 2. You can solder a 40-pin header to the Zero and use the pins the usual way, including individual pin access with jumper wires, or even using add-on boards and HATs (like the Sense HAT).</p>
<p>Alternatively, you can use the pin holes as they are—by using conductive thread, conductive paint, soldering directly to the pins, making connections with paper clips, and whatever else you can make work.</p>
<h2>Pi Zero projects</h2>
<p>Of course, most projects that have been done with a Raspberry Pi can be done with a Zero (some requiring the GPIO pin header), but what other projects can be done with the Zero that make use of its compact and discreet form factor?</p>
<h3>Wearables: Zero tech glove</h3>
<p>Sew a Pi Zero into a glove (preferably a glove with mitten flaps you can use to cover the Pi), then with conductive thread, sew some wearable LEDs into the back of the palm, and sew in a tilt switch. Be sure to wire up your LEDs and switch carefully and correctly. Write some code (try <a href="https://www.raspberrypi.org/blog/gpio-zero-a-friendly-python-api-for-physical-computing/" target="_blank">GPIO Zero</a>) to make the LEDs flash when the tilt switch is activated, then add a battery pack and prepare to shine!</p>
<p><article class="media media--type-image media--view-mode-full" title="Pi Zero"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/business-uploads/pi-zero_f3.png" width="520" height="425" alt="Pi Zero" title="Pi Zero" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p><a href="https://www.raspberrypi.org/" target="_blank" rel="ugc">Raspberry Pi Foundation</a>. <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a></p>
</div>
      
  </article></p>
<p style="text-align: center;"><em><sup><a href="https://www.raspberrypi.org/" target="_blank">Raspberry Pi Foundation</a>. <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank">CC BY-SA 4.0</a></sup></em></p>
<h3>Paintables: Zero conductive paint circuits</h3>
<p>Get some conductive paint, and draw a circuit on paper connecting the GPIO pin holes to components, such as LEDs made for wearable projects, and write code to have them flash. Not as messy as it sounds—and easy to try out different ideas on different bits of paper. Try writing out words or patterns or make your own circuit puzzles for people to play.</p>
<p><article class="media media--type-image media--view-mode-full" title="Pi Zero"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/business-uploads/pi-zero_f4.png" width="520" height="322" alt="Pi Zero" title="Pi Zero" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p><a href="https://www.raspberrypi.org/" target="_blank" rel="ugc">Raspberry Pi Foundation</a>. <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a></p>
</div>
      
  </article></p>
<p style="text-align: center;"><em><sup><a href="https://www.raspberrypi.org/" target="_blank">Raspberry Pi Foundation</a>. <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank">CC BY-SA 4.0</a></sup></em></p>
<h3>Drive-ables: Zero robot</h3>
<p>Get hold of a standard motor controller board, such as the <a href="https://ryanteck.uk/add-ons/6-ryanteck-rpi-motor-controller-board-0635648607160.html" target="_blank">RTK MCB</a>, and motors and wheels, or use the new <a href="http://camjam.me/?page_id=1035" target="_blank">CamJam EduKit 3</a>, then solder a 40-pin GPIO header to the Pi Zero and attach your board and a battery pack. Write some code to control the motors, build a chassis, maybe add some sensors, and you have yourself a very cheap but equally nifty robot.</p>
<p><article class="media media--type-image media--view-mode-full" title="Pi Zero"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/business-uploads/pi-zero_f5.png" width="520" height="315" alt="Pi Zero" title="Pi Zero" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p><a href="https://www.raspberrypi.org/" target="_blank" rel="ugc">Raspberry Pi Foundation</a>. <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a></p>
</div>
      
  </article></p>
<p style="text-align: center;"><em><sup><a href="https://www.raspberrypi.org/" target="_blank">Raspberry Pi Foundation</a>. <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank">CC BY-SA 4.0</a></sup></em></p>
<h2>Weightlessness</h2>
<p>Ok, so the Pi Zero is not weightless, but 9g is one heck of a light computer. Dave Akerman, who makes a habit of sending the smallest and lightest of Raspberry Pis up into the sky attached to helium balloons with a camera module taking pictures or video the whole journey, is sure to want to make use of the Zero in his next high-altitude balloon flight. Although the Zero doesn't have a CSI camera port, you can always use a USB webcam instead of the official camera module.</p>
<p><article class="media media--type-image media--view-mode-full" title="Pi Zero"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/business-uploads/pi-zero_f6.png" width="520" height="461" alt="Pi Zero" title="Pi Zero" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p><a href="https://www.raspberrypi.org/" target="_blank" rel="ugc">Raspberry Pi Foundation</a>. <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a></p>
</div>
      
  </article></p>
<p style="text-align: center;"><em><sup><a href="https://www.raspberrypi.org/" target="_blank">Raspberry Pi Foundation</a>. <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank">CC BY-SA 4.0</a></sup></em></p>
<h2>Zero add-on boards?</h2>
<p>Add-on boards for the Pi Zero? It's too soon to say, as this is a brand new development, but the Raspberry Pi community and ecosystem is a rapid and agile one. Although existing add-on boards and HATs will work with the Zero (with a pin header soldered on), we're sure to see some expansion (or reduction, if you like) in the range of accessories available—perhaps a Zero-sized HAT-like standard will arise and we'll see a host of new and exciting miniature add-on boards, such as motor controllers and LED boards, perfectly sitting aboard the Zero with the mounting holes in the corners. Let's wait and see what happens!</p>
<p><article class="media media--type-image media--view-mode-full" title="Pi Zero"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/business-uploads/pi-zero_f7.png" width="520" height="346" alt="Pi Zero" title="Pi Zero" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p><a href="https://www.raspberrypi.org/" target="_blank" rel="ugc">Raspberry Pi Foundation</a>. <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a></p>
</div>
      
  </article></p>
<p style="text-align: center;"><em><sup><a href="https://www.raspberrypi.org/" target="_blank">Raspberry Pi Foundation</a>. <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank">CC BY-SA 4.0</a></sup></em></p>
