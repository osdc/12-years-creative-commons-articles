<p>I see this question popping up quite often in different conversations. Recently, we had a good discussion about it within my team. The main question was about how to communicate openly with the community, as well as have the space to build a team and work as a team. This can be challenging; for example, when a company or a sponsor pays a part of the contributors to work full time on a project.</p>
<p>In this article, I will explain why agile works with the open source development model.</p>
<h2>Building agility with transparency</h2>
<p>My idea of agile is that it is a mindset rather than a set of processes and tools. As the quote says, "Don't do agile, be agile." So what are the core values of this mindset?</p>
<p>An answer to that question is <a href="https://heartofagile.com/" target="_blank">The Heart of Agile</a>. This initiative aims to make agile simpler and more human. It emphasizes the four following values:</p>
<ul><li>Collaborate</li>
<li>Deliver</li>
<li>Reflect</li>
<li>Improve</li>
</ul><p><article class="align-center media media--type-image media--view-mode-full" title="Heart of agile"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/hero_graphic2.png" width="523" height="547" alt="Heart of agile" title="Heart of agile" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter">Image credit: <a href="https://heartofagile.com/" target="_blank" rel="ugc">heartofagile.com</a></p>
</div>
      
  </article></p>
<p>These are compatible with open source values—open source is all about collaboration and delivering the result of that. Reflection and improvement help us adapt to changes and provide the opportunity for an honest look in the mirror.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Great Content</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=7016000000127cYAAQ">Free online course: RHEL technical overview</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=7016000000127cYAAQ">Learn Advanced Linux Commands</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Download Cheat Sheets</a></li>
<li><a href="https://opensource.com/alternatives?intcmp=7016000000127cYAAQ">Find an Open Source Alternative</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=7016000000127cYAAQ">Read Top Linux Content</a></li>
<li><a href="https://opensource.com/resources?intcmp=7016000000127cYAAQ">Check out open source resources</a></li>
</ul></div>
</div>
</div>
</div>
<p>Now, what about transparency? Scrum is often presented with <a href="https://opensource.com/article/19/10/ways-developers-what-agile" target="_self">three pillars</a>—transparency, inspection, and adaptation, though these are not limited to Scrum and can be applied to agile as a whole.</p>
<p>Can we apply these pillars to open source as well? Absolutely, and I would even go so far as to say that it is one of the best examples of those values in action.</p>
<p>Open source development provides a direct link between the developers and the users of a software. Discussions happen in the open, and everyone is welcome to give their opinions. This results in the users feeling valued, which leads them to be more involved in the project.</p>
<p>The core values of both agile and open source overlap, so why do we still think that agile does not work for open source?</p>
<h2>Building the community is creating value</h2>
<p>In agile, we talk a lot about creating value, the main idea being to simplify processes and focus on delivery. A possible downside of this is that it can lead teams to focus only on delivering new features<strong> </strong>and not invest enough time to pay back technical debt or build on automation to facilitate the future growth of the project. A way to avoid such a situation is to involve the team in defining what creating value actually means for the project.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Many hands in a circle"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/perry-grone-lblgffladry-unsplash.jpg" width="675" height="450" alt="Many hands in a circle" title="Many hands in a circle" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>Image credit: Perry Grones via <a href="https://unsplash.com/photos/lbLgFFlADrY" target="_blank" rel="ugc">Unsplash</a></p>
</div>
      
  </article></p>
<p>How does that relate to open source? For any team working in an open source community, it is crucial to understand that building and caring for that community creates value. In open source, the community is the heartbeat of a project—it provides crucial feedback, support, and documentation and contributes in many other ways.</p>
<p>So, whatever process is used by a team, it is important to build in time and space for the community. Invite people to review your user stories; after all, you do have users in your community. Dedicate time to reviewing contributions, answering questions, providing support, etc. All these should be part of the team's activities and be seen as valuable.</p>
<h2>"Default to open" does not mean 100% open</h2>
<p>Building a team also requires building trust between its members. To allow this trust to grow, the team needs a safe space to have conversations. In most cases, for this space to be safe, it cannot be open to the wider community. Such a space allows the team to grow, discuss possible improvement, and enforce good internal practices. It can also be used to provide constructive feedback if needed.</p>
<p>An important warning, however—be careful not to overuse that safe space. If all conversations default to that safe space, then the team loses transparency. The team should remind itself to default to open communication channels, and only use the safe space when needed. Embracing the "default to open" motto lets the team challenge itself when to use, or not use, that safe space.</p>
<p>As such, when it is appropriate and consented to by the team, you should consider sharing the output of these conversations with the wider community. That will uphold the practice of transparency while ensuring ongoing trust between the team and the community.</p>
<p>What do you think about agile and open source working together? Share your experiences and thoughts on the subject in the comments!</p>
