<p>I am a Fedora Linux user who runs <code>yum upgrade</code> daily. While this habit enables me to run all the latest software (one of <a href="https://docs.fedoraproject.org/en-US/project/#_what_is_fedora_all_about" target="_blank">Fedora's four foundations</a> is "first," and it lives up to that), it also highlights any incompatibilities between the <a href="https://zfsonlinux.org/" target="_blank">ZFS</a> storage platform and a new kernel.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Linux resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux commands cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Free online course: RHEL Technical Overview</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux networking cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">SELinux cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux common commands cheat sheet</a></li>
<li><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">What are Linux containers?</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Our latest Linux articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>As a developer, sometimes I need new features from the latest ZFS branch. For example, ZFS 2.0.0 contains an exciting new feature greatly <a href="https://www.phoronix.com/scan.php?page=news_item&amp;px=OpenZFS-3x-Boost-Sync-ZVOL" target="_blank">improving ZVOL sync performance</a>, which is critical to me as a KVM user. But this means that if I want to use the 2.0.0 branch, I have to build ZFS myself.</p>
<p>At first, I just compiled ZFS manually from its Git repo after every kernel update. If I forgot, ZFS would fail to be recognized on the next boot. Luckily, I quickly learned how to set up dynamic kernel module support (<a href="https://www.linuxjournal.com/article/6896" target="_blank">DKMS</a>) for ZFS. However, this solution isn't perfect. For one thing, it doesn't utilize the powerful <a href="https://en.wikipedia.org/wiki/Yum_%28software%29" target="_blank">yum</a> system, which can help with resolving dependencies and upgrading. In addition, switching between your own package and an upstream package is pretty easy with yum.</p>
<p>In this article, I will demonstrate how to set up a yum repo for packaging ZFS. The solution has two steps:</p>
<ol><li>Create RPM packages from the ZFS Git repository</li>
<li>Set up a yum repo to host the packages</li>
</ol><h2 id="create-rpm-packages">Create RPM packages</h2>
<p>To create RPM packages, you need to install the RPM toolchain. Yum provides groups to bundle installing the tools:</p>
<pre><code class="language-bash">sudo dnf group install 'C Development Tools and Libraries' 'RPM Development Tools'</code></pre><p>After these have been installed, you must install all the packages necessary to build ZFS from the ZFS Git repo. The packages belong to three groups:</p>
<ol><li><a href="https://opensource.com/article/19/7/introduction-gnu-autotools">Autotools</a> to generate build files from platform configurations</li>
<li>Libraries for building ZFS kernel and userland tools</li>
<li>Libraries for building RPM packages</li>
</ol><pre><code class="language-bash">sudo dnf install libtool autoconf automake gettext createrepo \
    libuuid-devel libblkid-devel openssl-devel libtirpc-devel \
    lz4-devel libzstd-devel zlib-devel \
    kernel-devel elfutils-libelf-devel \
    libaio-devel libattr-devel libudev-devel \
    python3-devel libffi-devel </code></pre><p>Now you are ready to create your own packages.</p>
<h2 id="build-openzfs">Build OpenZFS</h2>
<p><a href="https://openzfs.org/wiki/Main_Page" target="_blank">OpenZFS</a> provides excellent infrastructure. To build it:</p>
<ol><li>Clone the repository with <code>git</code> and switch to the branch/tag that you hope to use.</li>
<li>Run Autotools to generate a makefile.</li>
<li>Run <code>make rpm</code> and, if everything works, RPM files will be placed in the build folder.</li>
</ol><pre><code class="language-bash">$ git clone --branch=zfs-2.0.0-rc3 https://github.com/openzfs/zfs.git zfs 
$ cd zfs 
$ ./autogen.sh
$ ./configure
$ make rpm </code></pre><h2 id="set-up-a-yum-repo">Set up a yum repo</h2>
<p>In yum, a repo is a server or local path that includes metadata and RPM files. A consumer sets up an INI configuration file, and the <code>yum</code> command automatically resolves the metadata and downloads the corresponding packages.</p>
<p>Fedora provides the <code>createrepo</code> tool to set up a yum repo. First, create the repo and copy all RPM files from the ZFS folder to the repo. Then run <code>createrepo --update</code> to include all packages in the metadata:</p>
<pre><code class="language-bash">$ sudo mkdir -p /var/lib/zfs.repo 
$ sudo createrepo /var/lib/zfs.repo 
$ sudo cp *.rpm /var/lib/zfs.repo/ 
$ sudo createrepo --update /var/lib/zfs.repo </code></pre><p>Create a new configuration file in <code>/etc/yum.repos.d</code> to include the repo path:</p>
<pre><code class="language-bash">$ echo \
"[zfs-local]\\nname=ZFS Local\\nbaseurl=file:///var/lib/zfs.repo\\nenabled=1\\ngpgcheck=0" |\
sudo tee /etc/yum.repos.d/zfs-local.repo 

$ sudo dnf --repo=zfs-local list available --refresh </code></pre><p>Finally, you have reached the end of the journey! You have a working yum repo and ZFS packages. Now you just need to install them:</p>
<pre><code class="language-bash">$ sudo dnf install zfs 
$ sudo /sbin/modprobe zfs</code></pre><p>Run <code>sudo zfs version</code> to see the version of your userland and kernel tools. Congratulations! You have <a href="https://openzfs.github.io/openzfs-docs/Getting%20Started/Fedora.html" target="_blank">ZFS for Fedora</a>.</p>
