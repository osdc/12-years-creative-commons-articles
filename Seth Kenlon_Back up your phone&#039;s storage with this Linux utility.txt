<p>One of the great failings of mobile devices is how difficult it can be to transfer data from your device to your computer. Mobile devices have a long history of this. Early mobiles, like Pilot and Handspring PDA devices, required special synchronization software (which you had to do religiously for fear of your device running out of batteries and losing all of your data forever). Old iPods required a platform-specific interface. Modern mobile devices default to sending your data to an online account so you can download it again on your computer.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Linux resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux commands cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Free online course: RHEL Technical Overview</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux networking cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">SELinux cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux common commands cheat sheet</a></li>
<li><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">What are Linux containers?</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Our latest Linux articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>Good news—if you're running Linux, you can probably interface with your mobile device using the <code>gphoto2</code> command. Originally developed as a way to communicate with digital cameras back when a digital camera was just a camera, <code>gphoto2</code> can talk to many different kinds of mobile devices now. Don't let the name fool you, either. It can handle all types of files, not just photos. Better yet, it's scriptable, flexible, and a lot more powerful than most GUI interfaces.</p>
<p>If you've ever struggled with finding a comfortable way to sync your data between your computer and mobile, take a look at <code>gphoto2</code>.</p>
<h2>Install gPhoto2</h2>
<p>Chances are your Linux system already has libgphoto2 installed, because it's a key library for interfacing with mobile devices. You may have to install the command <code>gphoto2</code>, however, which is probably available from your repository.</p>
<p>On Fedora or RHEL:</p>
<pre><code class="language-bash">$ sudo dnf install gphoto2
</code></pre><p>On Debian or Ubuntu:</p>
<pre><code class="language-bash">$ sudo apt install gphoto2
</code></pre><h2>Verify compatibility</h2>
<p>To verify that your mobile device is supported, use the <code>--list-cameras</code> piped through <code>less</code>:</p>
<pre><code class="language-bash">$ gPhoto2 --list-cameras | less </code></pre><p>Or you can pipe it through <code>grep</code> to search for a term. For example, if you have a Samsung Galaxy, then use <code>grep</code> with case sensitivity turned off with the <code>-i</code> switch:</p>
<pre><code class="language-bash">$ gphoto2 --list-cameras | grep -i galaxy
  "Samsung Galaxy models (MTP)"
  "Samsung Galaxy models (MTP+ADB)"
  "Samsung Galaxy models Kies mode"
</code></pre><p>This confirms that Samsung Galaxy devices are supported through MTP and MTP with ADB.</p>
<p>If you can't find your device listed, you can still try using <code>gphoto2</code> on the off chance that your device is actually something on the list masquerading as a different brand.</p>
<h2>Find your mobile device</h2>
<p>To use gPhoto2, you first have to have a mobile device plugged into your computer, set to MTP mode, and you probably need to give your computer permission to interact with it. This usually requires physical interaction with your device, specifically pressing a button in the UI to permit its filesystem to be accessed by the computer it's just been attached to.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Screenshot of allow access message"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/gphoto2-mtp-allow.jpg" width="675" height="1387" alt="Screenshot of allow access message" title="Screenshot of allow access message" /></div>
      
  </article></p>
<p>If you don't give your computer access to your mobile, then gPhoto2 detects your device, but it isn't unable to interact with it.</p>
<p>To ensure your computer detects the device you've attached, use the <code>--auto-detect</code> option:</p>
<pre><code class="language-bash">$ gphoto2 --auto-detect
Model                       Port
---------------------------------------
Samsung Galaxy models (MTP) usb:002,010
</code></pre><p>If your device isn't detected, check your cables first, and then check that your device is configured to interface over MTP or ADB, or whatever protocol gPhoto2 supports for your device, as shown in the output of <code>--list-cameras</code>.</p>
<h2>Query your device for features</h2>
<p>With modern devices, there's usually a plethora of potential features, but not all features are supported. You can find out for sure with the <code>--abilities</code> option, which I find rather intuitive.</p>
<pre><code class="language-bash">$ gphoto2 --abilities
Abilities for camera            : Samsung Galaxy models (MTP)
Serial port support             : no
USB support                     : yes
Capture choices                 : Capture not supported by driver
Configuration support           : no
Delete selected files on camera : yes
Delete all files on camera      : no
File preview (thumbnail) support: no
File upload support             : yes
</code></pre><p>There's no need to specify what device you're querying as long as you only have one device attached. If you have attached more than one device that gPhoto2 can interact with, though, you can specify the device by port, camera model, or usbid.</p>
<h2>Interacting with your device</h2>
<p>If your device supports capture, then you can grab media through your camera from your computer. For instance, to capture an image:</p>
<pre><code class="language-bash">$ gphoto2 --capture-image
</code></pre><p>To capture an image and immediately transfer it to the computer you're on:</p>
<pre><code class="language-bash">$ gphoto2 --capture-image-and-download
</code></pre><p>You can also capture video and sound. If you have more than one camera attached, you can specify which device you want to use by port, camera model, or usbid:</p>
<pre><code class="language-bash">$ gphoto2 --camera "Samsung Galaxy models (MTP)" \
--capture-image-and-download
</code></pre><h2>Files and folders</h2>
<p>To interact with files on your device intelligently, you need to understand the structure of the filesystem being exposed to gPhoto2.</p>
<p>You can view available folders with the <code>--get-folders</code> option:</p>
<pre><code class="language-bash">$ gphoto2 --list-folders
There are 2 folders in folder '/'.                                             
 - store_00010001
 - store_00020002
There are 0 folders in folder '/store_00010001'.
There are 0 folders in folder '/store_00020002'.
</code></pre><p>Each of these folders represents a storage destination on the device. In this example, <code>store_00010001</code> is the internal storage and <code>store_00020002</code> is an SD card. Your device may be structured differently.</p>
<h2>Getting files</h2>
<p>Now that you know the folder layout of your device, you can ingest photos from your device. There are many different options you can use, depending on what you want to take from the device.</p>
<p>You can get a specific file, providing you know the full path:</p>
<pre><code class="language-bash">$ gphoto2 --get-file IMG_0001.jpg --folder /store_00010001/myphotos</code></pre><p>You can get all files at once:</p>
<pre><code class="language-bash">$ gphoto2 --get-all-files --folder /store_00010001/myfiles</code></pre><p>You can get just audio files:</p>
<pre><code class="language-bash">gphoto2 --get-all-audio-data --folder /store_00010001/mysounds</code></pre><p>There are other options, too, and most of them depend on what your device, and the protocol you're using, support.</p>
<h2>Uploading files</h2>
<p>Now that you know your potential target folders, you can upload files from your computer to your device. For example, assuming there's a file called <code>example.epub</code> in your current directory, you can send the file to your device with the <code>--upload-file</code> option combined with the <code>--folder</code> option to specify which storage location you want to upload to:</p>
<pre><code class="language-bash">$ gphoto2 --upload file example.epub \
--folder store_00010001
</code></pre><p>You can make a directory on your device, should you prefer to upload several files to a consolidated location:</p>
<pre><code class="language-bash">$ gphoto2 --mkdir books \
--folder store_00010001
$ gphoto2 --upload-file *.epub \
--folder store_00010001/books
</code></pre><h2>Listing files</h2>
<p>To see files uploaded to your device, use the <code>--list-files</code> option:</p>
<pre><code class="language-bash">$ gphoto2 --list-files --folder /store_00010001
There is 1 file in folder '/store_00010001'
#1     example.epub 17713 KB application/x-unknown
$ gphoto2 --list-files --folder /store_00010001/books
There is 1 file in folder '/store_00010001'
#1    example0.epub 17713 KB application/x-unknown
#2    example1.epub 12264 KB application/x-unknown
[...]
</code></pre><h2>Exploring your options</h2>
<p>Much of gPhoto2's power depends on your device, so your experience will be different than anyone else's. There are many operations listed in <code>gphoto2 --help</code> for you to explore. Use gPhoto2 and never struggle with transferring files from your device to your computer ever again!</p>
