<p>In my <a href="https://opensource.com/article/18/12/podman-and-user-namespaces">previous article</a> on user namespace and <a href="https://podman.io/" target="_blank">Podman</a>, I discussed how you can use Podman commands to launch different containers with different user namespaces giving you better separation between containers. Podman also takes advantage of user namespaces to be able to run in rootless mode. Basically, when a non-privileged user runs Podman, the tool sets up and joins a user namespace. After Podman becomes root inside of the user namespace, Podman is allowed to mount certain filesystems and set up the container. Note there is no privilege escalation here other then additional UIDs available to the user, explained below.</p>
<h2 id="how-does-podman-create-the-user-namespace">How does Podman create the user namespace?</h2>
<h3 id="shadow-utils">shadow-utils</h3>
<p>Most current Linux distributions include a version of shadow-utils that uses the <strong>/etc/subuid</strong> and <strong>/etc/subgid</strong> files to determine what UIDs and GIDs are available for a user in a user namespace.</p>
<pre><code class="language-text">$ cat /etc/subuid
dwalsh:100000:65536
test:165536:65536
$ cat /etc/subgid
dwalsh:100000:65536
test:165536:65536</code></pre><p>The useradd program automatically allocates 65536 UIDs for each user added to the system. If you have existing users on a system, you would need to allocate the UIDs yourself. The format of these files is <strong>username:</strong><strong>STARTUID</strong><strong>:TOTALUIDS</strong>. Meaning in my case, dwalsh is allocated UIDs 100000 through 165535 along with my default UID, which happens to be 3265 defined in /etc/passwd. You need to be careful when allocating these UID ranges that they don't overlap with any <strong>real</strong> UID on the system. If you had a user listed as UID 100001, now I (dwalsh) would be able to become this UID and potentially read/write/execute files owned by the UID.</p>
<p>Shadow-utils also adds two setuid programs (or setfilecap). On Fedora I have:</p>
<pre><code class="language-text">$ getcap /usr/bin/newuidmap
/usr/bin/newuidmap = cap_setuid+ep
$ getcap /usr/bin/newgidmap
/usr/bin/newgidmap = cap_setgid+ep</code></pre><p>Podman executes these files to set up the user namespace. You can see the mappings by examining /proc/self/uid_map and /proc/self/gid_map from inside of the rootless container.</p>
<pre><code class="language-text">$ podman run alpine cat /proc/self/uid_map /proc/self/gid_map
     	0   	3267      	1
     	1 	100000  	65536
     	0   	3267      	1
     	1 	100000  	65536</code></pre><p>As seen above, Podman defaults to mapping root in the container to your current UID (3267) and then maps ranges of allocated UIDs/GIDs in /etc/subuid and /etc/subgid starting at 1. Meaning in my example, UID=1 in the container is UID 100000, UID=2 is UID 100001, all the way up to 65536, which is 165535.</p>
<p>Any item from outside of the user namespace that is owned by a UID or GID that is not mapped into the user namespace appears to belong to the user configured in the <strong>kernel.overflowuid</strong> sysctl, which by default is 35534, which my /etc/passwd file says has the name <strong>nobody</strong>. Since your process can't run as an ID that isn't mapped, the owner and group permissions don't apply, so you can only access these files based on their "other" permissions. This includes all files owned by <strong>real</strong> root on the system running the container, since root is not mapped into the user namespace.</p>
<p>The <a href="https://buildah.io/" target="_blank">Buildah</a> command has a cool feature, <a href="https://github.com/containers/buildah/blob/master/docs/buildah-unshare.md" target="_blank"><strong>buildah unshare</strong></a>. This puts you in the same user namespace that Podman runs in, but without entering the container's filesystem, so you can list the contents of your home directory.</p>
<pre><code class="language-text">$ ls -ild /home/dwalsh
8193 drwx--x--x. 290 dwalsh dwalsh 20480 Jan 29 07:58 /home/dwalsh
$ buildah unshare ls -ld /home/dwalsh
drwx--x--x. 290 root root 20480 Jan 29 07:58 /home/dwalsh</code></pre><p>Notice that when listing the home dir attributes outside the user namespace, the kernel reports the ownership as dwalsh, while inside the user namespace it reports the directory as owned by root. This is because the home directory is owned by 3267, and inside the user namespace we are treating that UID as root.</p>
<h2 id="what-happens-next-in-podman-after-the-user-namespace-is-setup">What happens next in Podman after the user namespace is set up?</h2>
<p>Podman uses <a href="https://github.com/containers/storage" target="_blank">containers/storage</a> to pull the container image, and containers/storage is smart enough to map all files owned by root in the image to the root of the user namespace, and any other files owned by different UIDs to their user namespace UIDs. By default, this content gets written to ~/.local/share/containers/storage. Container storage works in rootless mode with either the vfs mode or with Overlay. Note: Overlay is supported only if the <a href="https://github.com/containers/fuse-overlayfs" target="_blank">fuse-overlayfs</a> executable is installed.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Linux Containers</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/topics/containers/whats-a-linux-container?utm_campaign=containers&amp;intcmp=70160000000h1s6AAA">What are Linux containers?</a></li>
<li><a href="https://developers.redhat.com/blog/2016/01/13/a-practical-introduction-to-docker-container-terminology/?utm_campaign=containers&amp;intcmp=70160000000h1s6AAA">An introduction to container terminology</a></li>
<li><a href="https://opensource.com/downloads/containers-primer?utm_campaign=containers&amp;intcmp=70160000000h1s6AAA">Download: Containers Primer</a></li>
<li><a href="https://www.redhat.com/en/resources/oreilly-kubernetes-operators-automation-ebook?utm_campaign=containers&amp;intcmp=70160000000h1s6AAA">Kubernetes Operators: Automating the container orchestration platform</a></li>
<li><a href="https://www.redhat.com/en/engage/kubernetes-containers-architecture-s-201910240918?utm_campaign=containers&amp;intcmp=70160000000h1s6AAA">eBook: Kubernetes patterns for designing cloud-native apps</a></li>
<li><a href="https://www.redhat.com/en/topics/containers/what-is-kubernetes?utm_campaign=containers&amp;intcmp=70160000000h1s6AAA">What is Kubernetes?</a></li>
</ul></div>
</div>
</div>
</div>
<p>The kernel only allows user namespace root to mount certain types of filesystems; at this time it allows mounting of procfs, sysfs, tmpfs, fusefs, and bind mounts (as long as the source and destination are owned by the user running Podman. OverlayFS is not supported yet, although the kernel teams are working on allowing it).</p>
<p>Podman then mounts the container's storage if it is using fuse-overlayfs; if the storage driver is using vfs, then no mounting is required. Podman on vfs requires a lot of space though, since each container copies the entire underlying filesystem.</p>
<p>Podman then mounts /proc and /sys along with a few tmpfs and creates the devices in the container.</p>
<p>In order to use networking other than the host networking, Podman uses the <a href="https://github.com/rootless-containers/slirp4netns" target="_blank">slirp4netns</a> program to set up <strong>User mode networking for unprivileged network namespace</strong>. Slirp4netns allows Podman to expose ports within the container to the host. Note that the kernel still will not allow a non-privileged process to bind to ports less than 1024. Podman-1.1 or later is required for binding to ports.</p>
<p>Rootless Podman can use user namespace for container separation, but you only have access to the UIDs defined in the /etc/subuid file.</p>
<h2 id="conclusion">Conclusion</h2>
<p>The Podman tool is enabling people to build and use containers without sacrificing the security of the system; you can give your developers the access they need without giving them root.</p>
<p>And when you put your containers into production, you can take advantage of the extra security provided by the user namespace to keep the workloads isolated from each other.</p>
