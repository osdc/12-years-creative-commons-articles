<p>Friends –</p>
<p>Vivek Kundra, the Federal CIO, is leaving government for academia, and today a <a href="http://www.nytimes.com/2011/08/04/technology/white-house-picks-new-information-chief.html?_r=2">new Federal CIO was named</a>. Below, you’ll find a letter from <a href="http://opensourceforamerica.org">Open Source for  America</a> wishing Mr. Kundra well.</p>
<!--break-->
<p>In about a week, we’ll have the opportunity to present him this  letter in person. We’d love to have the OSFA membership and supporters  co-sign the letter as a way of thanking Mr. Kundra for all he’s done for  open source in the Federal government.</p>
<p>So if you want to write a quick note of thanks, <a href="http://opensourceforamerica.org/2011/08/farewell-vivek/">visit our website</a> and throw it in the comments, and we’ll make sure he gets it!</p>
<p>Thanks,</p>
<p>Open Source for America Steering Committee</p>
<p> </p>
<blockquote><p>Mr. Kundra,</p>
<p><br /> For more than two years, Open Source for America’s membership has  witnessed a tremendous change in the way the government thinks about  information technology, and how it manages information technology  projects. For the first time, a Federal CIO was able to articulate the  way forward for much-needed reform, and we were extraordinarily  fortunate that you were able to assume that role, setting an  extraordinary example for all Federal CIOs to come.</p>
<p>Open source has witnessed an unprecedented popularity under your  leadership. The need for more agility has led, for example, the  State Department to examine the use of open source as a tool for  facilitating international collaboration. The need to transition from  capital-intensive IT to operations-focused IT has driven cloud adoption,  much of which is based on open source.<br /> Most exciting for Open Source for America, though, are the projects that  improve public-private collaboration. By releasing taxpayer-funded  software to the public, we allow citizens themselves to take a role in  improving their government. The Veterans Administration, for example,  now provides an open source EHR system through their VistA project.</p>
<p>You have been instrumental in leading this change, and for that you  have our heartfelt gratitude. You have left a permanent mark on the  Federal government’s IT operations, particularly through the 25-point  plan for Federal IT Reform, and are very much looking forward to a  continuing collaboration on that ambitious agenda.</p>
<p>Most sincerely,</p>
<p>Open Source for America</p>
</blockquote>
