<p>Google recently released <a href="http://google-opensource.blogspot.com/2015/02/introducing-grpc-new-open-source-http2.html">gRPC as open source</a>, an efficient framework for remote procedure calls (RPC) that uses a variety of programming languages.</p>
<p><a href="https://en.wikipedia.org/wiki/Remote_procedure_call">RPCs</a>, which are a common method of communication between servers, are not new. They date back to the 1980s, and because of their server-side nature, they are usually not exposed to most computer users, and not even to most software developers. Given that the RPCs role is to support communications between computer servers, they are mostly visible to system administrators and <a href="https://opensource.com/tags/devops">DevOps</a>. That said, their importance is fundamental, given that they are the standard mechanism to connect the many distributed systems that carry a very large fraction of Internet traffic.</p>
<p><img src="https://opensource.com/sites/default/files/resize/640px-Wikimedia_Foundation_Servers-8055_23_0-520x347.jpg" alt="Image of Wikipedia servers" title="Image of Wikipedia servers" style="display: block; margin-left: auto; margin-right: auto;" class="media-element file-default" data-file_info="%7B%22fid%22:%22250967%22,%22view_mode%22:%22default%22,%22fields%22:%7B%22format%22:%22default%22,%22field_file_image_alt_text%5Bund%5D%5B0%5D%5Bvalue%5D%22:%22Image%20of%20Wikipedia%20servers%22,%22field_file_image_title_text%5Bund%5D%5B0%5D%5Bvalue%5D%22:%22Image%20of%20Wikipedia%20servers%22,%22field_file_image_caption%5Bund%5D%5B0%5D%5Bvalue%5D%22:%22%3C/p%3E%3Cp%3Eopensource.com%3C/p%3E%22,%22field_file_image_caption%5Bund%5D%5B0%5D%5Bformat%5D%22:%22panopoly_wysiwyg_text%22,%22field_folder%5Bund%5D%22:%229400%22%7D,%22type%22:%22media%22%7D" height="347" width="520" /></p>
<p style="text-align: center;"><em>Image of Wikipedia servers</em></p>
<p>gRPC is based on the recently finalized <a href="https://http2.github.io/">HTTP/2</a> standard that enables <a href="http://www.grpc.io/faq/">new capabilities</a> such as:</p>
<ul><li>Bidirectional streaming</li>
<li>Flow control</li>
<li>Header compression</li>
<li>Multiplexing requests over a single TCP connection</li>
</ul><p>gRPC provides libraries for multiple languages, including:</p>
<ul><li>C</li>
<li>C++</li>
<li>Java</li>
<li>Go</li>
<li>Node.js</li>
<li>Python</li>
<li>Ruby</li>
</ul><p>The source code of gRPC can be found <a href="https://github.com/grpc">on GitHub</a>, which includes <a href="https://github.com/grpc/grpc-common">extensive documentation</a> and a <a href="http://www.grpc.io/">main webpage</a>. The release of gRPC is synchronized with a new release of <a href="https://github.com/google/protobuf/releases">Protocol Buffers: proto3</a>. Protocol buffers are used to:</p>
<ul><li>Specify the content and structure of messages exchanged between servers</li>
<li>Serialize and deserialize the messages</li>
<li>Generate source code for objects in multiple languages that read and write messages</li>
<li>Provide a mechanism for versioning the message definitions, so that clients and servers with different versions can still interoperate</li>
</ul><h2>Show me gRPC</h2>
<p>The <a href="https://github.com/grpc/grpc-common">Quick Start guide</a> has a summary of <a href="https://github.com/grpc/grpc-common/tree/master/python/helloworld">Hello World in Python</a>. It defines a service to which you (the client) send your name, i.e. “John Doe”, and the service replies with the composed string: "Hello, John Doe”.</p>
<p>Here are the messages that will go through the wire:</p>
<blockquote><p><code> // The request message containing the user's name. </code></p>
<p><code>message HelloRequest { optional string name = 1; } </code></p>

<p><code></code> <code> // The response message containing the greetings. </code></p>
<p><code>message HelloReply { optional string message = 1; } </code></p>
</blockquote>
<p>And here is the service definition:</p>
<blockquote><p><code> syntax = "proto2"; </code></p>
<p><code>// The greeting service definition. </code></p>
<p><code>service Greeter {</code></p>
<p><code>  // Sends a greeting </code></p>
<p><code>  rpc SayHello (HelloRequest) returns (HelloReply) {}</code></p>
<p><code> } </code></p>
</blockquote>
<p>If we put this service and messages definition in a file <a href="https://github.com/grpc/grpc-common/blob/master/python/helloworld/helloworld.proto">helloworld.proto</a> and process it with the Proto3 compiler, it will generate Python code for both the client side and the server side.</p>
<p>The <a href="https://github.com/grpc/grpc-common/blob/master/python/helloworld/greeter_client.py">client side</a> will look like:</p>
<blockquote><p><code> import helloworld_pb2</code></p>
<p><code>_TIMEOUT_SECONDS = 10</code></p>
<p><code>def run():</code></p>
<p><code>  with helloworld_pb2.early_adopter_create_Greeter_stub('localhost', 50051) as stub:</code></p>
<p><code>    response = stub.SayHello(helloworld_pb2.HelloRequest(name='you'), _TIMEOUT_SECONDS)</code></p>
<p><code>    print "Greeter client received: " + response.message</code></p>

<p><code>if __name__ == '__main__':</code></p>
<p><code>  run() </code></p>
</blockquote>
<p>And the <a href="https://github.com/grpc/grpc-common/blob/master/python/helloworld/greeter_server.py">server side</a> will look like:</p>
<blockquote><p><code> import time</code></p>
<p><code>import helloworld_pb2</code></p>
<p><code>_ONE_DAY_IN_SECONDS = 60 * 60 * 24</code></p>

<p><code>class Greeter(helloworld_pb2.EarlyAdopterGreeterServicer):</code></p>

<p><code>  def SayHello(self, request, context):</code></p>
<p><code>    return helloworld_pb2.HelloReply(message='Hello, %s!' % request.name)</code></p>

<p><code>  def serve():</code></p>
<p><code>    server = helloworld_pb2.early_adopter_create_Greeter_server(Greeter(), 50051, None, None)</code></p>
<p><code>    server.start()</code></p>
<p><code>    try: </code></p>
<p><code>      while True:</code></p>
<p><code>        time.sleep(_ONE_DAY_IN_SECONDS)</code></p>
<p><code>      except KeyboardInterrupt:</code></p>
<p><code>        server.stop()</code></p>

<p><code>if __name__ == '__main__':</code></p>
<p><code>  serve() </code></p>
</blockquote>
<p><strong>More details are available in the <a href="https://github.com/grpc/grpc-common">starting guide</a>.</strong></p>
<p><em>Disclaimer: The indentation in the Python code above are not exact. For proper and correct Python syntax, please follow the links provided.</em></p>
