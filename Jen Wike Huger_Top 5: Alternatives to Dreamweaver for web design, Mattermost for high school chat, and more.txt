<p>In this week's Top 5, we highlight alternatives to Dreamweaver for web design, Mattermost for high school chat, ageism in tech, OSCON's 18th year, and the director of Google.org on unconscious bias in the workplace.</p>
<p><iframe allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/uugpj9WzWpA" width="520"></iframe></p>
<h2>Top 5 articles of the week</h2>
<p><strong>5. <a href="https://opensource.com/life/16/3/sxsw-diversity-google-org" target="_blank">Director of Google.org on challenges of unconscious bias</a></strong></p>
<p>Nicole Engard traveled to SxSW Interactive this year and reported on the events she attended. First up in her series is a conversation between the director of Google.org, <a href="https://plus.google.com/+JacquellineFuller/about" target="_blank">Jacquelline Fuller</a>, and Hugh Forrest about diversity in tech, including what Google is doing about it.</p>
<p><strong>4. <a href="https://opensource.com/life/16/3/interview-oscon-chairs" target="_blank">OSCON moves to Austin: Will the 18th OSCON be the best one yet?</a></strong></p>
<p>Nicole Engard is back with an interview with the OSCON chairs where they discuss the conference's move to Austin, Texas from Portland, Oregan during this, their 18th year.</p>
<p><strong>3. <a href="https://opensource.com/life/16/3/can-we-talk-about-ageism" target="_blank">Can we talk about ageism?</a></strong></p>
<p>Deb Nicholson explores what negative bias against age, for the young and the old, is like in tech.</p>
<p><strong>2. <a href="https://opensource.com/education/16/3/mattermost-open-source-chat" target="_blank">How our high school replaced IRC with Mattermost</a></strong></p>
<p>Charlie Reisinger and his high school students bid farewell to IRC as they replace it with Mattermost, a new open source chat service.</p>
<p><strong>1. <a href="https://opensource.com/business/16/3/open-source-alternatives-dreamweaver" target="_blank">4 open source alternatives to Dreamweaver</a></strong></p>
<p>If you're looking for an open source alternative to Dreamweaver or other proprietary HTML/CSS editor, Jason Baker's got you covered with this round up.</p>
<h2>Honorable mention</h2>
<p><a href="https://opensource.com/community/16/3/2016-raspberry-pi-3-ultimate-starter-kit-giveaway" target="_blank"><strong>We're giving away a Raspberry Pi 3 Ultimate Starter Kit</strong></a></p>
<p>We announced our Raspberry Pi 3 Sweepstakes this week, where we'll be giving away a <a href="http://www.canakit.com/raspberry-pi-3-ultimate-kit.html" target="_blank">Raspberry Pi 3 Ultimate Starter Kit</a>. Enter to win by March 31 at 11:59 p.m. EDT. All you need to do is complete the official entry form in the article. Good luck!</p>
