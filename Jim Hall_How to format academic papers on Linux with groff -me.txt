<p>I was an undergraduate student when I discovered Linux in 1993. I was so excited to have the power of a Unix system right in my dorm room, but despite its many capabilities, Linux lacked applications. Word processors like LibreOffice and OpenOffice were years away. If you wanted to use a word processor, you likely booted your system into MS-DOS and used WordPerfect, the shareware GalaxyWrite, or a similar program.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Linux resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux commands cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Free online course: RHEL Technical Overview</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux networking cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">SELinux cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux common commands cheat sheet</a></li>
<li><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">What are Linux containers?</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Our latest Linux articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>That was my method, since I needed to write papers for my classes, but I preferred staying in Linux. I knew from our "big Unix" campus computer lab that Unix systems provided a set of text-formatting programs called <code>nroff</code> and <code>troff</code>. They are different interfaces to the same system: <code>nroff</code> generates plaintext output, suitable for screens or line printers, and <code>troff</code> generates very pretty output, usually for printing on a laser printer.</p>
<p>On Linux, <code>nroff</code> and <code>troff</code> are combined as GNU troff, more commonly known as <a href="https://www.gnu.org/software/groff/" target="_blank">groff</a>. I was happy to see a version of groff included in my early Linux distribution, so I set out to learn how to use it to write class papers. The first macro set I learned was the <code>-me</code> macro package, a straightforward, easy to learn macro set.</p>
<p>The first thing to know about <code>groff</code> is that it processes and formats text according to a set of macros. A macro is usually a two-character command, set on a line by itself, with a leading dot. A macro might carry one or more options. When <code>groff</code> encounters one of these macros while processing a document, it will automatically format the text appropriately.</p>
<p>Below, I'll share the basics of using <code>groff -me</code> to write simple documents like class papers. I won't go deep into the details, like how to create nested lists, keeps and displays, tables, and figures.</p>
<h2>Paragraphs</h2>
<p>Let's start with an easy example you see in almost every type of document: paragraphs. Paragraphs can be formatted with the first line either indented or not (i.e., flush against the left margin). Many printed documents, including academic papers, magazines, journals, and books, use a combination of the two types, with the first (leading) paragraph in a document or chapter flush left and all other (regular) paragraphs indented. In <code>groff -me</code>, you can use both paragraph types: leading paragraphs (<code>.lp</code>) and regular paragraphs (<code>.pp</code>).</p>
<pre>
<code class="language-text">.lp
This is the first paragraph.
.pp
This is a standard paragraph.</code></pre><h2>Text formatting</h2>
<p>The macro to format text in bold is <code>.b</code> and to format in italics is <code>.i</code>. If you put <code>.b</code> or <code>.i</code> on a line by itself, then all text that comes after it will be in bold or italics. But it's more likely you just want to put one or a few words in bold or italics. To make one word bold or italics, put that word on the same line as <code>.b</code> or <code>.i</code>, as an option. To format multiple words in <strong>bold</strong> or <em>italics</em>, enclose your text in quotes.</p>
<pre>
<code class="language-text">.pp
You can do basic formatting such as
.i italics
or
.b "bold text."</code></pre><p>In the above example, the period at the end of <strong>bold text</strong> will also be in bold type. In most cases, that's not what you want. It's more correct to only have the words <strong>bold text</strong> in bold, but not the trailing period. To get the effect you want, you can add a second argument to <code>.b</code> or <code>.i</code> to indicate any text that should trail the bolded or italicized text, but in normal type. For example, you might do this to ensure that the trailing period doesn't show up in bold type.</p>
<pre>
<code class="language-text">.pp
You can do basic formatting such as
.i italics
or
.b "bold text" .</code></pre><h2>Lists</h2>
<p>With <code>groff -me</code>, you can create two types of lists: bullet lists (<code>.bu</code>) and numbered lists (<code>.np</code>).</p>
<pre>
<code class="language-text">.pp
Bullet lists are easy to make:
.bu
Apple
.bu
Banana
.bu
Pineapple
.pp
Numbered lists are as easy as:
.np
One
.np
Two
.np
Three
.pp
Note that numbered lists will reset at the next pp or lp.</code></pre><h2>Subheads</h2>
<p>If you're writing a long paper, you might want to divide your content into sections. With <code>groff -me</code>, you can create numbered headings (<code>.sh</code>) and unnumbered headings (<code>.uh</code>). In either, enclose the section title in quotes as an argument. For numbered headings, you also need to provide the heading level: <code>1</code> will give a first-level heading (e.g., 1.). Similarly, <code>2</code> and <code>3</code> will give second and third level headings, such as 2.1 or 3.1.1.</p>
<pre>
<code class="language-text">.uh Introduction
.pp
Provide one or two paragraphs to describe the work
and why it is important.
.sh 1 "Method and Tools"
.pp
Provide a few paragraphs to describe how you
did the research, including what equipment you used</code></pre><h2>Smart quotes and block quotes</h2>
<p>It's standard in any academic paper to cite other people's work as evidence. If you're citing a brief quote to highlight a key message, you can just type quotes around your text. But groff won't automatically convert your quotes into the "smart" or "curly" quotes used by modern word processing systems. To create them in <code>groff -me</code>, insert an inline macro to create the left quote (<code>\*(lq</code>) and right quote mark (<code>\*(rq</code>).</p>
<pre>
<code class="language-text">.pp
Christine Peterson coined the phrase \*(lqopen source.\*(rq</code></pre><p>There's also a shortcut in <code>groff -me</code> to create these quotes (<code>.q</code>) that I find easier to use.</p>
<pre>
<code class="language-text">.pp
Christine Peterson coined the phrase
.q "open source."</code></pre><p>If you're citing a longer quote that spans several lines, you'll want to use a block quote. To do this, insert the blockquote macro (<code>.(q</code>) at the beginning and end of the quote.</p>
<pre>
<code class="language-text">.pp
Christine Peterson recently wrote about open source:
.(q
On April 7, 1998, Tim O'Reilly held a meeting of key
leaders in the field. Announced in advance as the first
.q "Freeware Summit,"
by April 14 it was referred to as the first
.q "Open Source Summit."
.)q</code></pre><h2>Footnotes</h2>
<p>To insert a footnote, include the footnote macro (<code>.(f</code>) before and after the footnote text, and use an inline macro (<code>\**</code>) to add the footnote mark. The footnote mark should appear both in the text and in the footnote itself.</p>
<pre>
<code class="language-text">.pp
Christine Peterson recently wrote about open source:\**
.(f
\**Christine Peterson.
.q "How I coined the term open source."
.i "OpenSource.com."
1 Feb 2018.
.)f
.(q
On April 7, 1998, Tim O'Reilly held a meeting of key
leaders in the field. Announced in advance as the first
.q "Freeware Summit,"
by April 14 it was referred to as the first
.q "Open Source Summit."
.)q</code></pre><h2>Cover page</h2>
<p>Most class papers require a cover page containing the paper's title, your name, and the date. Creating a cover page in <code>groff -me</code> requires some assembly. I find the easiest way is to use centered blocks of text and add extra lines between the title, name, and date. (I prefer to use two blank lines between each.) At the top of your paper, start with the title page (<code>.tp</code>) macro, insert five blank lines (<code>.sp 5</code> ), then add the centered text (<code>.(c</code>), and extra blank lines (<code>.sp 2</code>).</p>
<pre>
<code class="language-text">.tp
.sp 5
.(c
.b "Writing Class Papers with groff -me"
.)c
.sp 2
.(c
Jim Hall
.)c
.sp 2
.(c
February XX, 2018
.)c
.bp</code></pre><p>The last macro (<code>.bp</code>) tells groff to add a page break after the title page.</p>
<h2>Learning more</h2>
<p>Those are the essentials of writing professional-looking a paper in <code>groff -me</code> with leading and indented paragraphs, bold and italics text, bullet and numbered lists, numbered and unnumbered section headings, block quotes, and footnotes.</p>
<p>I've included a sample groff file to demonstrate all of this formatting. Save the <code>lorem-ipsum.me</code> file to your system and run it through groff. The <code>-Tps</code> option sets the output type to PostScript so you can send the document to a printer or convert it to a PDF file using the <code>ps2pdf</code> program.</p>
<pre>
<code class="language-text">groff -Tps -me lorem-ipsum.me &gt; lorem-ipsum.me.ps
ps2pdf lorem-ipsum.me.ps lorem-ipsum.me.pdf</code></pre><p>If you'd like to use more advanced functions in <code>groff -me</code>, refer to Eric Allman's "Writing Papers with Groff using <code>−me</code>," which you should find on your system as <code>meintro.me</code> in groff's <code>doc</code> directory. It's a great reference document that explains other ways to format papers using the <code>groff -me</code> macros.</p>
