<p><a href="https://opensource.com/article/19/8/corteza-open-source-alternative-salesforce">Corteza</a> is open source software often used as an alternative to Salesforce. In addition to its customer relationship management (CRM) application, one of its most popular and empowering features is its low-code development environment, which helps users create custom Corteza apps that give them exactly what they need.</p>
<h2 id="what-is-low-code">What is low code?</h2>
<p>A low-code development platform provides an environment where you can create your own applications with mostly graphical interfaces instead of code. Its main benefits are that it reduces development time and you don't need advanced programming knowledge to use it.</p>
<p>The <a href="https://cortezaproject.org/technology/core/corteza-low-code/" target="_blank">Corteza Low Code development platform</a> is web-based and specifically built for records-based business applications. In fact, <a href="https://cortezaproject.org/technology/core/corteza-crm/" target="_blank">Corteza CRM</a> is built with it. Corteza is built with Go in the backend and Vue.js in the frontend, making custom applications fast and scalable.</p>
<h2 id="why-open-source">Why open source?</h2>
<p>One common criticism of low-code development platforms, in general, is that the people who build these applications don't have the knowledge to solve issues with the program, such as: "Why is my application taking a long time to show a list of records?"</p>
<p>If you use a proprietary low-code platform, you are stuck to the vendor, and this can become more expensive over time. However, with an open source low-code platform (like Corteza, which is available under the <a href="https://github.com/cortezaproject/corteza-docs/blob/master/LICENSE" target="_blank">Apache 2.0</a> license), there is no vendor lock-in. All code, documentation, and backend development of the platform are open, helping you get to the bottom of more complex issues faster.</p>
<h2 id="what-can-you-build">What can you build?</h2>
<p>You can build any records-based application with Corteza Low Code, such as:</p>
<ul><li>Customer relationship management</li>
<li>Enterprise resource planning</li>
<li>Case management</li>
<li>Expense management</li>
<li>Event management</li>
<li>Support desk</li>
<li>Knowledge base</li>
<li>Logistics management</li>
<li>Employee onboarding</li>
<li>Leave tracker</li>
<li>Volunteer portal</li>
<li>Donor management</li>
<li>Franchise management</li>
<li>Hotel management</li>
<li>Construction management</li>
<li>Restaurant management</li>
<li>Travel management</li>
<li>Fleet hub</li>
<li>Inventory management</li>
<li>And a lot more</li>
</ul><p><article class="align-center media media--type-image media--view-mode-full" title="Corteza Low-Code application"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/corteza_calendar.png" width="675" height="379" alt="Corteza Low-Code application" title="Corteza Low-Code application" /></div>
      
  </article></p>
<h2 id="create-an-application-in-corteza-low-code">Create an application in Corteza Low Code</h2>
<p>Creating an application with Corteza Low Code takes only a handful of steps. First, you create modules with fields, followed by pages to visualize the modules. This creates the basic data model of your application, after which you can add charts, automation and workflow rules, and role-based permissions.</p>
<p>The first step is ensuring that the Low Code application is available to you in Corteza. Read <a href="https://opensource.com/article/19/8/corteza-open-source-alternative-salesforce"><em>Intro to Corteza, an open source alternative to Salesforce</em></a> for more information on setting up and configuring Corteza.</p>
<p>To demonstrate how to use Corteza Low Code, this three-article series will explain, step by step, how to build a sample application to manage donations.</p>
<h3 id="open-corteza-low-code">1. Open Corteza Low Code</h3>
<p>To enter Corteza Low Code, first, you need to enter Corteza. Once you're inside Corteza, you have several options for opening Corteza Low Code:</p>
<ul><li>Open it in a new tab by clicking on the <strong>+</strong> button close to the top of the page, which opens the application menu;</li>
<li>Open it in a new panel by selecting the grid icon on the top right (the icon with four squares); or</li>
</ul><p><article class="align-center media media--type-image media--view-mode-full" title="Options for opening Corteza Low Code"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/corteza_openlowcodetab.png" width="675" height="401" alt="Options for opening Corteza Low Code" title="Open Corteza in a new tab by clicking the +" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>The <strong>+</strong> button opens Corteza in a new tab.</sup></p>
</div>
      
  </article></p>
<ul><li>Open it in a new window by selecting <strong>open in a new window</strong> when hovering over the Low Code icon in the application menu.</li>
</ul><p><article class="align-center media media--type-image media--view-mode-full" title="Other options for opening Corteza Low Code"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/corteza_openlowcodeapp.png" width="675" height="401" alt="Other options for opening Corteza Low Code" title="Other options for opening Corteza Low Code" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>The grid icon and Low-Code icon that open Corteza Low Code in a new panel and a new window, respectively.</sup></p>
</div>
      
  </article></p>
<p>The first thing you see is the Corteza Low Code Namespaces menu. This shows all the applications you have installed (if any) and an option to create new applications.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Corteza Low-Code Namespaces menu"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/corteza_namespaces.png" width="675" height="401" alt="Corteza Low-Code Namespaces menu" title="Corteza Low-Code Namespaces menu" /></div>
      
  </article></p>
<p>If you cannot see Corteza Low Code, your existing applications, or the <strong>Create a new namespace</strong> button, you might not have the permissions you need. In this case, contact the administrator of your Corteza platform.</p>
<h3 id="create-an-application">2. Create an application</h3>
<p>Each Low Code application lives in its own unique "namespace." This feature allows you to have an unlimited number of Corteza Low Code applications.</p>
<p>To create a new application, you need to fill out the following fields:</p>
<ul><li><strong>Full namespace name</strong> (mandatory): The name of your application, which will be shown in the Low Code Namespaces menu. This name does not have to be unique, but it's best practice to give each application a unique and descriptive name. For example: <em>Donations.</em></li>
<li><strong>Subtitle</strong> (optional): The subtitle is shown below the name of the application in the Low Code Namespaces menu. For example: <em>Donor Management for the Development Department</em>.</li>
<li><strong>Namespace description</strong> (optional): A longer description that appears when hovering over the namespace in the Low Code Namespaces menu. For example: <em>In this Donations application, our organization tracks contributors and contributions</em>.</li>
<li><strong>Slug</strong> (mandatory): A short name for the namespace that is used in the URL. It's best to only use letters, numbers, and characters like the dash or underscore. For example: <em>donation-management</em>.</li>
<li><strong>Enabled</strong> (mandatory in order to enter the application): By checking the <strong>Enabled</strong> checkbox, the application will be available in the Low Code Namespaces menu.</li>
<li><strong>Set namespace permissions</strong> (optional): By clicking on <strong>Set namespace permissions</strong>, you can define the access permissions to the namespace for each role. Other access permissions for your application will be created later because they depend on the type of application you build.</li>
</ul><p>Try it out by following these steps to create an example app to manage donations.</p>
<p>First, click on the <strong>Create namespace</strong> button. It's the last block on the Corteza Low Code Namespaces menu.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Create namespace button"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/corteza_createnamespacebutton.png" width="300" height="181" alt="Create namespace button" title="Create namespace button" /></div>
      
  </article></p>
<p>This opens the <strong>Create namespace</strong> form. For the donation application, fill in the following data:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Form for creating a donations application"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/corteza_createdonationnamespace.png" width="675" height="337" alt="Form for creating a donations application" title="Form for creating a donations application" /></div>
      
  </article></p>
<p>Hit <strong>Save and close</strong>. You've created an application in Corteza!</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Donations application button"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/corteza_donationsappbutton.png" width="300" height="181" alt="Donations application button" title="Donations application button" /></div>
      
  </article></p>
<p>Of course, it doesn't do anything yet because it has no logic, no modules, and no rules, but you've completed the first step in creating a Corteza Low Code application. The second article in this series will create the modules and fields to serve as the data structure for your application. Fortunately, it is easy to do whether or not you have database experience, so stay tuned!</p>
<p>The Donations application is <a href="https://latest.cortezaproject.org/compose/ns/donations/" target="_blank">available on the Corteza community server</a>. You need to be logged in or create a free Corteza community server account to check it out.</p>
