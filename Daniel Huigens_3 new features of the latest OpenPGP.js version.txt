<p><meta charset="utf-8" /></p>
<p><a href="https://github.com/openpgpjs/openpgpjs" target="_blank">OpenPGP.js</a> is a cryptography library that implements the <a href="https://tools.ietf.org/html/rfc4880" target="_blank">OpenPGP standard</a>, most commonly used for email encryption. ProtonMail, Mailvelope, and FlowCrypt all use OpenPGP.js, to name a few. That means the OpenPGP.js library encrypts millions of users' messages.</p>
<p>The OpenPGP standard, first published in the 1990s, like almost anything, requires maintenance and updating for both security and usability. A "crypto refresh" of the standard <a href="https://datatracker.ietf.org/doc/charter-ietf-openpgp/" target="_blank">is in the works</a>, which adds modern encryption algorithms and deprecates outdated ones. To improve usability, various email applications now allow users to seamlessly encrypt their communication—without managing their keys or those of their contacts.</p>
<p>First released in 2014, OpenPGP.js began based on an early prototype called GPG4Browsers, which is based on several scripts by Herbert Hanewinkel (among other contributors). The second version of OpenPGP.js, released in 2016, was completely reworked to use Uint8Arrays instead of strings (which significantly increased its performance) and modern ES6 modules rather than CommonJS modules internally. Versions 3 and 4, both released in 2018, added support for Elliptic-curve cryptography (ECC) and streaming, respectively.</p>
<p>My team and I continue working on OpenPGP.js to ensure its evolution as an easy-to-use library for strong encryption.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on security</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/articles/defensive-coding-guide/?intcmp=70160000000h1s6AAA">The defensive coding guide</a></li>
<li><a href="https://www.redhat.com/en/events/webinar/automating-system-security-and-compliance-standard-operating-system?intcmp=70160000000h1s6AAA">Webinar: Automating system security and compliance with a standard operating system</a></li>
<li><a href="https://www.redhat.com/en/resources/container-security-openshift-cloud-devops-whitepaper?intcmp=70160000000h1s6AAA">10 layers of Linux container security </a></li>
<li><a href="https://developers.redhat.com/books/selinux-coloring-book?intcmp=70160000000h1s6AAA">SELinux coloring book</a></li>
<li><a href="https://opensource.com/tags/security?intcmp=70160000000h1s6AAA">More security articles</a></li>
</ul></div>
</div>
</div>
</div>

<h2>1. Elliptic-curve cryptography by default</h2>
<p>In OpenPGP.js version 4, RSA was used when generating new keys by default. Although ECC is faster and more secure, Curve25519 had not been standardized in the OpenPGP specification yet. The crypto refresh draft does include Curve25519, and it is expected to be included "as is" in the next version of the OpenPGP specification, so OpenPGP.js version 5 now generates keys using ECC by default.</p>
<h2>2. Import only the modules you need</h2>
<p>Similarly, while OpenPGP.js used ES6 modules internally for years, version 4 still didn't publish a proper ES6 module. Instead, it published only a Univeral Module Definition (UMD) module that could run both in the browser and on Node.js. In version 5, this changes by publishing separate modules for the browser and Node.js (both ES6 and non-ES6), making it easier for library users to import OpenPGP.js on all platforms and (when using the ES6 module) only import the parts they need. This is enabled in large part by switching the build system to <a href="https://rollupjs.org/" target="_blank">rollup</a>.</p>
<h2>3. Reject weak cryptography</h2>
<p>There are also many other security improvements. For example, 1024-bit RSA keys, ElGamal, and DSA keys are considered insecure and are rejected by default. Additionally, where version 4 already defaulted to AES-encryption, version 5 now refuses to encrypt using weaker algorithms entirely by default, even if the public key claims to only support a weaker algorithm. It instead assumes that all OpenPGP implementations support AES (which has been the case for a very long time).</p>
<h2>What's next for OpenPGP.js</h2>
<p>Looking ahead, there are some security improvements to make. Key fingerprints, used to identify public keys, still use SHA-1, though a fix for this is planned in the crypto refresh. In the meantime, it is recommended to use different means to ascertain the authenticity of any public key used for encryption, such as by fetching the entire key directly from the recipient's domain using the proposed <a href="https://datatracker.ietf.org/doc/html/draft-koch-openpgp-webkey-service">Web Key Directory (WKD)</a> standard—already implemented by various <a href="https://wiki.gnupg.org/WKD#Mail_Service_Providers_offering_WKD">email providers</a>. WKD support was built into OpenPGP.js version 4 but is a separate module in version 5 to keep the main library lean.</p>
<p>Similarly, when encrypting messages or files with a password rather than a public key (uncommon when using OpenPGP for email encryption, but more so when used for encrypted backups, for example), the password is converted to a symmetric key using a relatively weak key derivation function (KDF). It is thus advisable for applications to pass the user's password through a strong KDF, such as <a href="https://en.wikipedia.org/wiki/Argon2" target="_blank">Argon2</a> or <a href="https://en.wikipedia.org/wiki/Scrypt" target="_blank">scrypt</a>, before passing it to OpenPGP.js. Hopefully, the crypto refresh will include one of these algorithms to implement in a future version of OpenPGP.js.</p>
<h2>How to use OpenPGP.js version 5</h2>
<p>For now, though, OpenPGP.js version 5 has been <a data-saferedirecturl="https://www.google.com/url?q=https://www.npmjs.com/package/openpgp&amp;source=gmail&amp;ust=1633605647470000&amp;usg=AFQjCNG5t8yG7X_BARp9XDjQU5p4Iv-JuA" href="https://www.npmjs.com/package/openpgp" rel="noopener noreferrer" target="_blank">published</a> to the npm package registry. If you like, feel free to try it out! Feedback is welcome in the <a href="https://github.com/openpgpjs/openpgpjs/discussions" target="_blank">discussions tab</a> on GitHub. Please note, however, that while OpenPGP.js is a general-purpose encryption library, its primary use case is in situations where compatibility with the OpenPGP specification is required (for example, when sending or receiving PGP-encrypted email). For other use cases, a different library may be a more appropriate or performant choice. In general, of course, be careful when rolling any crypto.</p>
<p>Thanks for reading, and here's to securing the future of email!</p>
