<p>I've been a fan of the Squeezebox ever since I acquired Logitech's now-obsolete <a href="https://en.wikipedia.org/wiki/Squeezebox_(network_music_player)#Squeezebox_Touch_.28April_2010.29" target="_blank">Squeezebox Touch</a>, which my family is still using.</p>
<p>The Squeezebox Touch provided a family-friendly interface to access our music library, either directly on the device or via a range of mobile applications. Logitech discontinued its development in 2012, but I was happy as they open sourced the Squeezebox's server software as <a href="http://wiki.slimdevices.com/index.php/Logitech_Media_Server" target="_blank">Logitech Media Server</a> and supplied the open source code used on the physical Squeezebox devices.</p>
<p class="rtecenter">
<article class="media media--type-image media--view-mode-full" title="SqueezeBox Touch"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/squeezebox_touch_2.jpg" width="700" height="506" alt="SqueezeBox Touch" title="SqueezeBox Touch" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>SqueezeBox Touch, Steve Ellis, CC BY-SA 4.0</sup></p>
</div>
      
  </article></p>
<p>About a year ago I decided I wanted some decent sound for my home office and wanted to reuse an old Raspberry Pi 1 Model B as the source. Ideally, I wanted something compatible with our existing Squeezebox ecosystem and looked for options. The Logitech Media Server quickly became my choice. The Logitech Media Server software used to manage the audio library is updated by the community with a number of enhancements and big fixes (info on recent builds is available via the <a href="http://wiki.slimdevices.com/index.php/Logitech_Media_Server" target="_blank">Slimdevices forum</a> and its <a href="http://wiki.slimdevices.com/index.php/Logitech_Media_Server" target="_blank">Wiki</a>).</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Explore open hardware </p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/resources/what-open-hardware?src=open_hardware_resources_menu1">What is open hardware?</a></li>
<li><a href="https://opensource.com/resources/what-raspberry-pi?src=open_hardware_resources_menu2">What is Raspberry Pi?</a></li>
<li><a href="https://opensource.com/resources/what-arduino?src=open_hardware_resources_menu3">What is an Arduino?</a></li>
<li><a href="https://opensource.com/tags/hardware?src=open_hardware_resources_menu4">Our latest open hardware articles</a></li>
</ul></div>
</div>
</div>
</div>

<p>Here are my three steps to creating a near-audiophile experience that you can use with almost any iteration of the Raspberry Pi.</p>
<h2>Phase 1: Proof of concept</h2>
<p>While the Raspberry Pi Model B comes with a standard 3.5mm stereo audio jack, it has a limited frequency response and is subject to a lot of interference. Later models haven't really improved this, but it's definitely sufficient to test the Pi as a SqueezeBox client.</p>
<p class="rtecenter">
<article class="media media--type-image media--view-mode-full" title="Base Raspberry Pi Hardware"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/base_raspberry_pi_hardware_0.jpg" width="700" height="538" alt="Base Raspberry Pi Hardware" title="Base Raspberry Pi Hardware" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>Steve Ellis under CC BY-SA 4.0</p>
</div>
      
  </article></p>
<h3>Baseline hardware and software</h3>
<ul><li>Raspberry Pi 1 Model B with a case</li>
<li>128MB SD card</li>
<li>USB charger capable of 1A output</li>
<li>3.5mm stereo audio cable</li>
<li>Network cable</li>
<li>Old pair of laptop speakers</li>
<li>piCorePlayer software</li>
</ul><p class="rtecenter">
<article class="media media--type-image media--view-mode-full" title="Initial PoC hardware, including old PC speakers"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/initial_poc_hardware_including_old_pc_speakers.jpg" width="700" height="621" alt="Initial PoC hardware, including old PC speakers" title="Initial PoC hardware, including old PC speakers" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>Steve Ellis under CC BY-SA 4.0</p>
</div>
      
  </article></p>
<p>A key aim in the PoC stage was to reuse old, spare equipment—I'd guess that the Altec Lansing desktop speakers I used are about 20 years old, and the 128MB SD card was the smallest spare card I could find. (I later switched to a 1GB SD card, due to reliability issues with the 128MB card.) I also went for a physical network cable, as the device will be operating headless (without a screen), and DHCP over a physical Ethernet cable is simple to implement and test. Another common issue is a poor or unreliable USB power source, hence the 1 amp unit.</p>
<p>I'm a big fan of taking an appliance-centric approach where possible, and <a href="https://sites.google.com/site/picoreplayer/home" target="_blank">piCorePlayer</a> provides a tiny build of Linux with the <a href="https://en.wikipedia.org/wiki/Squeezelite" target="_blank">Squeezelite</a> software client for Logitech Media Server integrated in an easy-to-install form. Under the hood, it's TinyCore-based Linux running in RAM and only mounts the storage when backing up configuration changes. This makes it very resilient to power outages. For audio output, it supports the 3.5mm stereo jack, HDMI audio, and a range of USB digital-to-analog converters (DACs). In addition, there are a wide range of I<sup>2</sup>C DACs, which plug directly onto the Raspberry Pi, if you want a more audiophile experience.</p>
<p>I downloaded the <a href="https://sites.google.com/site/picoreplayer/home/download" target="_blank">latest build of piCorePlayer</a> and installed it onto the SD card; note that with a more modern Pi, you'll need to use a microSD card. In addition, Raspberry Pi's don't support booting from USB devices. I used <b>dd</b> to image the SD card via the card slot on my Linux laptop.</p>
<pre>
<code class="language-bash">sudo dd if=piCorePlayer3.20.img of=/dev/mmcblk0 bs=16M

</code></pre><p>Optionally, some distros now ship with image-burning tools like <a href="https://developers.redhat.com/blog/2016/04/26/fedora-media-writer-the-fastest-way-to-create-live-usb-boot-media/" target="_blank">Fedora Media Writer</a>.</p>
<p class="rtecenter">
<article class="media media--type-image media--view-mode-full" title="Fedora Media Writer"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/fedora_media_writer.png" width="640" height="352" alt="Fedora Media Writer" title="Fedora Media Writer" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>Steve Ellis under CC BY-SA 4.0</p>
</div>
      
  </article></p>
<p>After booting, the Pi found an IP address via DHCP, and I could connect via a browser to complete the configuration.</p>
<p>Under Squeezelite's settings, I set audio output to analog audio and clicked save. Then I specified a name for my player, which was detected by the SqueezeCenter server software, and again clicked save.</p>
<p class="rtecenter">
<article class="media media--type-image media--view-mode-full" title="Initial PiCorePlayer configuration"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/initial_picoreplayer_configuration_0.png" width="686" height="350" alt="Initial PiCorePlayer configuration" title="Initial PiCorePlayer configuration" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p><sup>Initial PiCorePlayer configuration, Steve Ellis, CC BY-SA 4.0</sup></p>
</div>
      
  </article></p>
<p>Using my <a href="https://play.google.com/store/apps/details?id=uk.org.ngo.squeezer&amp;hl=en" target="_blank">Squeezer</a> Android mobile client, I could now see two Squeezebox clients on my network, the old Squeezebox Touch and my home office piCorePlayer, and I could select and play audio from my library. This was a quick and easy test, but I did have some audio issues. Occasionally I'd get a hum through the speakers if I knocked or moved the Pi, and the audio quality was less than impressive.</p>
<p class="rtecenter">
<article class="media media--type-image media--view-mode-full" title="Browser-based audio playback"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/browser_based_playback_of_audio.png" width="700" height="395" alt="Browser-based audio playback" title="Browser-based audio playback" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>Steve Ellis under CC BY-SA 4.0</p>
</div>
      
  </article></p>
<h2>Phase 2: USB audio adapter</h2>
<p>Because I was still trying to reuse existing hardware in Phase 2, I grabbed an old USB audio adapter that came with a Logitech headset, hoping this would provide a more reliable audio experience than the onboard 3.5mm jack.</p>
<p class="rtecenter">
<article class="media media--type-image media--view-mode-full" title="Logitech USB audio adapter"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/logitech_usb_audio_adapter.jpg" width="700" height="329" alt="Logitech USB audio adapter" title="Logitech USB audio adapter" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>Steve Ellis under CC BY-SA 4.0</p>
</div>
      
  </article></p>
<p>The USB adapter was automatically detected by the Pi. Via a browser, I just needed to change the audio output to USB and the output setting to <b>front:CARD=Headset,DEV=0</b>. After clicking save, I got more reliable audio through my 20-year-old speakers via the USB adapter.</p>
<p>There is a <a href="https://sites.google.com/site/picoreplayer/home/List-of-USB-DACs" target="_blank">great guide</a> for various USB devices on the piCorePlayer site; I've updated it to included the USB IDs for the Logitech adapter.</p>
<h2>Phase 3: Audiophile quality?</h2>
<p class="rtecenter">
<article class="media media--type-image media--view-mode-full" title="Audiophile-ready SqueezePi"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/audiophile_ready_squeezepi.png" width="700" height="689" alt="Audiophile-ready SqueezePi" title="Audiophile-ready SqueezePi" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>Steve Ellis under CC BY-SA 4.0</p>
</div>
      
  </article></p>
<p>I had some spare bookshelf speakers that are considerably better than the laptop speakers, but no power source to drive them. At this stage, I could have used an HDMI-ready small amplifier and avoided the USB and 3.5mm outputs, but I'd heard of small amplifiers with built-in USB DACs and decided to explore that route.</p>
<p>I picked up a <a href="http://www.tpdz.net/en/products/vx1/" target="_blank">Topping VX1</a> amplifier from a local supplier after reading a large number of positive reviews. This is a gorgeous device with enough power for a small home-office environment. The USB DAC is automatically detected by modern Linux kernels and works well with piCorePlayer.</p>
<p class="rtecenter">
<article class="media media--type-image media--view-mode-full" title="Topping VX1 front"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/topping_vx1_front.jpg" width="700" height="363" alt="Topping VX1 front" title="Topping VX1 front" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>Steve Ellis under CC BY-SA 4.0</p>
</div>
      
  </article></p>
<p class="rtecenter">
<article class="media media--type-image media--view-mode-full" title="Connecting the Topping VX1 with a USB cable"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/connecting_the_topping_vx1_with_a_usb_cable.jpg" width="700" height="755" alt="Connecting the Topping VX1 with a USB cable" title="Connecting the Topping VX1 with a USB cable" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>Steve Ellis under CC BY-SA 4.0</p>
</div>
      
  </article></p>
<h2>Next steps: Is there a phase 4?</h2>
<p>You may be wondering if I would I do it the same way again if I were starting from scratch.</p>
<p>Mostly. I'd consider using a device like a <a href="https://opensource.com/node/24776">Pi Zero</a> if I were purchasing hardware. There are some great audiophile add-ons for this device, and it has an incredibly small footprint.</p>
<p>For the amplifier DAC, I'd hoped to use something along the lines of the device being developed by my old friend Bdale Garbee. <a href="https://lwn.net/Articles/674191/" target="_blank">These small devices</a> combine a PCM2705C USB DAC with a TPA3118D2 Class-D audio amp and could provide a simple, low-cost, and considerably more open solution as an alternative to the Topping VX1 amplifier. For more about Bdale's solution, see <a href="https://www.youtube.com/watch?v=gMTKyW6MXME" target="_blank">his presentation</a> from <a data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://linux.conf.au&amp;source=gmail&amp;ust=1495289022030000&amp;usg=AFQjCNF-wS4uERpCH6KbIzxWa9RjAkTUZg" href="http://linux.conf.au/" target="_blank">linux.conf.au</a> 2016 or visit his <a href="http://gag.com/electronics/audio/wholehouse/" target="_blank">project website</a>.</p>
<p>If I hadn't already been a Squeezebox user, I might have considered a music player daemon (MPD)-based solution, perhaps using something like <a href="https://www.mopidy.com/" target="_blank">Mopidity</a>.</p>
<p>I'm considering getting a touch screen to provide an even more complete Squeezebox Touch-like experience. There is a fully open source implementation of the Squeezebox UI that is compatible with touch screens for the Pi. In addition, my daughter will soon outgrow her current stereo, which might provide an opportunity to play with a Pi Zero to build another Squeezebox-compatible frontend.</p>
<p>But right now, I'm too busy enjoying the excellent audio quality in my home office while writing this article for Opensource.com to think about what's next.</p>
