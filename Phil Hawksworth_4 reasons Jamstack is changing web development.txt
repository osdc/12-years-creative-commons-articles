<p>The way we use and the way we build the web have evolved dramatically since its inception. Developers have seen the rise and fall of many architectural and development paradigms intended to satisfy more complex user experiences, support evolving device capabilities, and enable more effective development workflows.</p>
<p>In 2015, <a href="https://www.netlify.com/jamstack?utm_source=opensource.com&amp;utm_medium=jamstack-benefits-pnh&amp;utm_campaign=devex" target="_blank">Netlify</a> founders Matt Biilmann and Chris Bach coined the term "<a href="https://jamstack.org/?utm_source=opensource.com&amp;utm_medium=jamstack-benefits-pnh&amp;utm_campaign=devex" target="_blank">Jamstack</a>" to describe the architectural model they were championing and that was gaining popularity. In reality, the foundations of this model have existed from the beginning of the web. But multiple factors led them to coin this new term to encapsulate the approach and to give developers and technical architects a better means to discuss it.</p>
<p>In this article, I'll look at those factors, Jamstack's attributes, why the term came into existence, and how it is changing how we approach web development.</p>
<h2 id="what-is-jamstack">What is Jamstack?</h2>
<p>All Jamstack sites share a core principle: They are a collection of static assets generated during a build or compilation process so that they can be served from a simplified web server or directly from a content delivery network (CDN).</p>
<p>Before the term "Jamstack" emerged, many people described these sites as "static." This describes how the first sites on the web were created (although CDNs would come later). But the term "static sites" does a poor job of capturing what is possible with the Jamstack due to the way tools, services, and browsers have evolved.</p>
<p>The simplest Jamstack site is a single HTML file served as a static file. For a long time, open source web servers efficiently hosted static assets this way. This has become a commodity, with companies including Amazon, Microsoft, and Google offering hosting services based on file serving rather than spending compute cycles generating a response for each request on-demand.</p>
<p>But that's just a static site? Right?</p>
<p>Well, yes. But it's the thin end of the wedge. Jamstack builds upon this to deliver sites that confound the term "static" as a useful descriptor.</p>
<p>If you take things a stage further and introduce JavaScript into the equation, you can begin enhancing the user's experience. Modern browsers have increasingly capable JavaScript engines (like the open source <a href="https://v8.dev/" target="_blank">V8</a> from Google) and powerful browser <a href="https://www.redhat.com/en/topics/api/what-are-application-programming-interfaces">APIs</a> to enable services such as local-caching, location services, identity services, media access, and much more.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Great Content</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=7016000000127cYAAQ">Free online course: RHEL technical overview</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=7016000000127cYAAQ">Learn Advanced Linux Commands</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Download Cheat Sheets</a></li>
<li><a href="https://opensource.com/alternatives?intcmp=7016000000127cYAAQ">Find an Open Source Alternative</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=7016000000127cYAAQ">Read Top Linux Content</a></li>
<li><a href="https://opensource.com/resources?intcmp=7016000000127cYAAQ">Check out open source resources</a></li>
</ul></div>
</div>
</div>
</div>
<p>In many ways, the browser's JavaScript engine has replaced the runtime environment needed to perform dynamic operations in web experiences. Whereas a traditional technology stack such as <a href="https://en.wikipedia.org/wiki/LAMP_(software_bundle)" target="_blank">LAMP</a> requires configuration and maintenance of an operating system (Linux) and an active web server (Apache), these are not considerations with Jamstack. Files are served statically to clients (browsers), and if any computation is required, it can happen there rather than on the hosting infrastructure.</p>
<p>As Matt Biilmann describes it, "the runtime has moved up a level, to the browser."</p>
<p>Web experiences don't always require content to be dynamic or personalized, but they often do. Jamstack sites can provide this, thanks to the efficient use of JavaScript as well as a booming API economy. Many companies now provide content, tools, and services via APIs. These APIs enable even small project teams to inject previously unattainable, prohibitively expensive, and complex abilities into their Jamstack projects. For example, rather than needing to build identity, payment, and fulfillment services or to host, maintain, secure, and scale database capabilities, teams can source these functionalities from experts in those areas through APIs.</p>
<p>Businesses have emerged to provide these and many other services with all of the economies of scale and domain specializations needed to make them robust, efficient, and sustainable. The ability to consume these services via APIs decouples them from web applications' code, which is a very desirable thing.</p>
<p>Because these services took us beyond the old concept of static sites, a more descriptive term was needed.</p>
<h2 id="what's-in-a-name">What's in a name?</h2>
<p>The <em>JavaScript</em> available in modern browsers, calling on <em>APIs</em>, and enriching the underlying site content delivered with <em>markup</em> (HTML) are the J, A, and M in the Jamstack name. They identify properties that allow sites to move far beyond static sites without the need for complex and expensive active-hosting infrastructure.</p>
<p>But whether you decide to use all or just some of these attributes, the Jamstack's key principle is that the assets are created in advance to vastly improve hosting and development workflows. It's a shift from the higher-risk, just-in-time request-servicing method to a simplified, more predictable, prepared-in-advance approach.</p>
<p>As <a href="http://www.aaronsw.com/weblog/000404" target="_blank">Aaron Swartz</a> succinctly put it way back in 2002, "Bake, don't fry" your pages.</p>
<h2 id="jamstack-benefits">4 benefits of using Jamstack</h2>
<p>Embracing this model of decoupling the generation (or rendering) of assets from the work of serving assets creates significant opportunities.</p>
<h3 id="lowering-the-cost-of-scaling">Lowering the cost of scaling</h3>
<p>In a traditional stack, where views are generated for each incoming request, there is a correlation between the volume of traffic and the computation work done on the servers. This might reach all levels of the hosting stack, from load balancers and web servers to application servers and database servers. When these additional layers of infrastructure are provisioned to help manage the load, it adds cost and complexity to the environment and the work of operating the infrastructure and the site itself.</p>
<p>Jamstack sites flatten that stack. The work of generating the views happens only when the code or content changes, rather than for every request. Therefore, the site can be prepared in advance and hosted directly from a CDN without needing to perform expensive computations on the fly. Large pieces of infrastructure (and the work associated with them) disappear.</p>
<p>In short: Jamstack sites are optimized for scale by default.</p>
<h3 id="improving-speed">Improving speed</h3>
<p>Traditionally, to improve the hosting infrastructure's response time, those with the budget and the skills would add a CDN. Identifying assets that might be considered "static" and offloading serving those resources to a CDN could reduce the load on the web-serving infrastructure. Therefore, some requests could be served more rapidly from a CDN that is optimized for that task.</p>
<p>With a Jamstack site, the site is served entirely from the CDN. This avoids the complex logic of determining what must be served dynamically and what can be served from a CDN.</p>
<p>Every deployment becomes an operation to update the entire site on the CDN rather than across many pieces of infrastructure. This allows you to automate the process, which can increase your confidence in and decrease the cost and friction of deploying site updates.</p>
<h3 id="reducing-security-risks">Reducing security risks</h3>
<p>Removing hosting infrastructure, especially servers that do computation based on the requests they receive, has a profound impact on a site's security profile. A Jamstack site has far fewer attack vectors than a traditional site since many servers are no longer needed. There is no server more secure than the one that does not exist.</p>
<p>The CDN infrastructure remains but is optimized to serve pre-generated assets. Because these are read-only operations, they have fewer opportunities for attack.</p>
<h3 id="supercharging-the-workflow">Supercharging the workflow</h3>
<p>By removing so many moving parts from site hosting, you can vastly improve the workflows involved in developing and deploying them.</p>
<p>Jamstack sites support <a href="https://www.netlify.com/products/build/?utm_source=opensource.com&amp;utm_medium=jamstack-benefits-pnh&amp;utm_campaign=devex" target="_blank">version control from end to end</a> and commonly use Git and Git conventions to do everything from defining and provisioning new environments to executing a deployment. Deployments no longer need to change the state, resources, or configuration of multiple pieces of hosting infrastructure. And they can be tested locally, on staging environments, and in production environments with ease.</p>
<p>The approach also allows more comprehensive project encapsulation. A site's code repository can include everything needed to bootstrap a project, including defining the dependencies and operations involved in building the site. This simplifies onboarding developers and walking the path to production. (Here's <a href="https://github.com/philhawksworth/hello-trello" target="_blank">an example</a>.)</p>
<h2>Jamstack for all</h2>
<p>Web developers familiar with a wide variety of tools and frameworks are embracing the Jamstack. They are achieving new levels of productivity, ignited by the recognition that they can use many tools and languages that put more power and ability in their hands.</p>
<p>Open source libraries and frameworks with high levels of adoption and love from the development community are being used in combination and with third- and first-party APIs to produce incredibly capable solutions. And the low barriers of entry mean they are easier to explore, leading to higher levels of developer empowerment, effectiveness, and enthusiasm.</p>
