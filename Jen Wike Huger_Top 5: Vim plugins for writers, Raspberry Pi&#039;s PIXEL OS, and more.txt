<p>In this week's Top 5, I highlight favorite vim plugins, Getting started with PIXEL OS, OpenToonz for Linux users, GNOME 3 extensions, and careers in open source.</p>
<p><iframe allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/JGUvfVivVq8" width="560"></iframe></p>
<h2>Top 5 articles of the week</h2>
<p><strong>5. <a href="https://opensource.com/article/17/1/grad-school-open-source-academic-lab" target="_blank">A graduate degree could springboard you into an open source job</a></strong></p>
<p>A professor of Materials Science &amp; Engineering and Electrical &amp; Computer Engineering at Michigan Tech makes the case for a graduate degree that could lead to a career in open source science.</p>
<p><strong>4. <a href="https://opensource.com/article/17/2/top-gnome-shell-extensions" target="_blank">Top 9 GNOME shell extensions to customize your desktop Linux experience</a></strong></p>
<p>If you're a GNOME 3 user, learn how to optimize your Linux desktop with these must-have shell extensions.</p>
<p><strong>3. <a href="https://opensource.com/article/17/2/opentoonz-2d-animation-software">OpenToonz: Making high-end animation software accessible</a></strong></p>
<p>In this interview, one of the lead developers of Synfig tells us what it was like to port OpenToonz to Linux and why it's an important step for animation.</p>
<p><strong>2. ​<a href="https://opensource.com/article/17/1/try-raspberry-pis-pixel-os-your-pc" target="_blank">Try Raspberry Pi's PIXEL OS on your PC</a></strong></p>
<p>In September, the Raspberry Pi Foundation released an update that introduced PIXEL, the Pi's new desktop environment. From community manager Ben Nuttall, learn how to install it on your PC, Mac, or laptop.</p>
<p><strong>1. <a href="https://opensource.com/article/17/2/vim-plugins-writers">Awesome vim plugins for writers</a></strong></p>
<p>A Vim user shares his favorite plugins that benefits writers, whether they're technically minded or not.</p>
