<p>Screen readers are a specialized area of assistive technology software that reads and then speaks the content of a computer screen. People with no sight at all are just a fraction of those with visual impairments, and screen reader software can help all groups. Screen readers are mostly specific to an operating system, and they're used by people with visual impairments and accessibility trainers, as well as developers and accessibility consultants wanting to test how accessible a website or application is.</p>
<p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">Our favorite resources about open source</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-git?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Git cheat sheet">Git cheat sheet</a></div>
              <div class="field__item"><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Advanced Linux commands cheat sheet">Advanced Linux commands cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/alternatives?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Open source alternatives">Open source alternatives</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Free online course: RHEL technical overview">Free online course: RHEL technical overview</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Register for your free Red Hat account">Register for your free Red Hat account</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Check out more cheat sheets">Check out more cheat sheets</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<h2 id="nvda">How to use the NVDA screen reader</h2>
<p>The <a href="https://webaim.org/projects" target="_blank">WebAIM screen reader user surveys</a> began in 2009 and ran to 2021. In the first survey, the most common screen reader used was JAWS at 74%. It is a commercial product for Microsoft Windows, and the long-time market leader. NVDA, then a relatively new open source screen reader for Windows came in at just 8%. Fast forward to 2021 and JAWS comes in with 53.7% with NVDA at 30.7%.</p>
<p>You can download the latest release of NVDA from the <a href="https://www.nvaccess.org" target="_blank">NVAccess website</a>. Why do I use NVDA and recommend it to my MS Windows using clients? Well, it is open source, fast, powerful, easy to install, supports a wide variety of languages, can be run as a portable application, has a large user base, and there is a regular release cycle for new versions.</p>
<p>NVDA has been translated into fifty-five languages and is used in one-hundred and seventy-five different countries. There is also an active developer community with their own <a href="https://addons.nvda-project.org/index.en.html" target="_blank">Community Add-ons website</a>. Any add-ons you choose to install will depend on your needs and there are a lot to choose from, including extensions for common video conferencing platforms.</p>
<p>Like all screen readers, there are a lot of key combinations to learn with NVDA. Using any screen reader proficiently takes training and practice.</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-05/nvda1.png" width="747" height="421" alt="Image of NVDA welcome screen" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Peter Cheer, CC BY-SA 4.0)</p>
</div>
      
  </article><p>Teaching NVDA to people familiar with computers and who have keyboard skills is not too difficult. Teaching basic computer skills (without the mouse, touch pad, and keyboard skills) and working with NVDA to a complete beginner is far more of a challenge. Individual learning styles and preferences differ. In addition, people may not need to learn how to do everything if all that they want to do is browse the web and use email. A good source of links to NVDA tutorials and resources is <a href="http://www.accessibilitycentral.net/" target="_blank">Accessibility Central</a>.</p>
<p>It becomes easier once you have mastered operating NVDA with keyboard commands, but there is also a menu-driven system for many configuration tasks.</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-05/nvda2.png" width="728" height="324" alt="Image of NVDA menu" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Peter Cheer, CC BY-SA 4.0)</p>
</div>
      
  </article><h2 id="test-for-accessibility">Test for accessibility</h2>
<p>The inaccessibility of some websites to screen reader users has been a problem for many years, and still is despite disability equality legislation like the Americans with Disabilities Act (ADA). An excellent use for NVDA in the sighted community is for website accessibility testing. NVDA is free to download, and by running a portable version, website developers don't even need to install it. Run NVDA, turn off your monitor or close your eyes, and see how well you can navigate a website or application.</p>
<p>NVDA can also be used for testing when working through the (often ignored) task of properly <a href="https://www.youtube.com/watch?v=rRzWRk6cXIE" target="_blank">tagging a PDF document for accessibility</a>.</p>
<p>There are several guides that concentrate on using NVDA for accessibility testing. I can recommend <a href="https://www.unimelb.edu.au/accessibility/tools/testing-web-pages-with-nvda" target="_blank">Testing Web Pages with NVDA</a> and Using <a href="https://webaim.org/articles/nvda" target="_blank">NVDA to Evaluate Web Accessibility</a>.</p>
