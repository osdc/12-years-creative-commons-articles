<p>Spreadsheets can be very useful—and not just for <a href="https://opensource.com/article/17/8/budget-libreoffice-calc">managing your finances</a>. That said, desktop spreadsheets have their limitations. The biggest is that you need to be at your computer to use one. On top of that, collaborating on a spreadsheet can quickly become a messy affair.</p>
<p>Enter <a href="https://ethercalc.net/" target="_blank">EtherCalc</a>, an open source, web-based spreadsheet. While not as fully featured as a desktop spreadsheet, EtherCalc packs enough features for most people.</p>
<p>Let's take a look at how to get started using it.</p>
<h2>Getting EtherCalc</h2>
<p>If you're self-hosting, you can <a href="https://github.com/audreyt/ethercalc" target="_blank">download the code</a>, get it through <a href="https://sandstorm.io" target="_blank">Sandstorm.io</a>, or use npm (the Node.js package manager) to install it on a server.</p>
<p>But what if you don't have a server? You can use one of the many hosted instances of EtherCalc—for example, at <a href="https://ethercalc.org" target="_blank">EtherCalc.org</a>, the <a href="https://accueil.framacalc.org/en/" target="_blank">instance hosted</a> by the folks at <a href="https://opensource.com/article/18/8/framasoft">Framasoft</a>, or use it through <a href="https://oasis.sandstorm.io/" target="_blank">Sandstorm Oasis</a>.</p>
<h2>What can you use EtherCalc for?</h2>
<p>Just about everything you'd use a desktop spreadsheet for. That could be to balance your budget, track your savings, record your income, schedule meetings, or take an inventory of your possessions.</p>
<p>I've used EtherCalc to track time on freelance projects, to create invoices for those projects, and even to share article ideas with my fellow <a href="https://opensource.com/community-moderator-program">Opensource.com community moderators</a>. How you use EtherCalc is up to your needs and your imagination.</p>
<h2>Working with EtherCalc</h2>
<p>The first step is to create a spreadsheet.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Empty EtherCalc spreadsheet"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/ethercalc-empty-spreadsheet.png" width="650" height="345" alt="Empty EtherCalc spreadsheet" title="Empty EtherCalc spreadsheet" /></div>
      
  </article></p>
<p>If you've used a desktop or web-based spreadsheet before, EtherCalc will look somewhat familiar. As with any spreadsheet, you type what you need to type in the cells on the sheet. The includes column headings, labels, and functions (more on those in a moment).</p>
<p>Before you do anything else, bookmark the URL to your spreadsheet. EtherCalc uses randomly generated URLs—for example, <a href="https://ethercalc.org/9krfqj2en6cke" target="_blank">https://ethercalc.org/9krfqj2en6cke</a>—which aren't easy to remember.</p>
<h2>Formatting your spreadsheet</h2>
<p>To add formatting to your spreadsheet, highlight the cell or cells that you want to format and click the <strong>Format</strong> menu.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="EtherCalc's Format menu"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/ethercalc-formatting.png" width="650" height="348" alt="EtherCalc's Format menu" title="EtherCalc's format menu" /></div>
      
  </article></p>
<p>You can add borders and padding, change fonts and their attributes, align text, and change the format of numbers, for example to dates or currency formats. When you're done, click the <strong>Save to:</strong> button to apply the formatting.</p>
<h2>Adding functions</h2>
<p><em>Functions</em> enable you to add data, manipulate data, and make calculations in a spreadsheet. They can do a lot more, too.</p>
<p>To add a function to your spreadsheet, click a cell. Then, click the <strong>Function</strong> button on the toolbar.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="EtherCalc Function button"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/ethercalc-function.png" width="636" height="128" alt="EtherCalc Function button" title="EtherCalc Functions" /></div>
      
  </article></p>
<p>That opens a list all of the functions EtherCalc supports, along with a short description of what each function does.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="EtherCalc Functions list"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/ethercalc-function-list.png" width="411" height="339" alt="EtherCalc Functions list" title="EtherCalc Functions list" /></div>
      
  </article></p>
<p>Select the function you want to use, then click <strong>Paste</strong>. EtherCalc adds the function, along with an opening parenthesis, to the cell. Type what you need to after the parenthesis, then type a closing parenthesis. For example, if you want to total up all the numbers in column B in the spreadsheet using the <em>=SUM()</em> function, type <em>B1:B21</em> and close the parenthesis.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Entering a function in EtherCalc "><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/ethercalc-function-example.png" width="351" height="151" alt="Entering a function in EtherCalc " title="Entering a function in EtherCalc " /></div>
      
  </article></p>
<p>You can also add functions by double-clicking in a cell and typing them. There's no reference in the documentation for EtherCalc's functions. However, it does support <a href="https://en.wikipedia.org/wiki/OpenFormula" target="_blank">OpenFormula</a> (a standard for math formulas that spreadsheets support). If you're not familiar with spreadsheet functions, you can look up what you need in the <a href="https://docs.oasis-open.org/office/v1.2/os/OpenDocument-v1.2-os-part2.html" target="_blank">OpenFormula specification</a> or this handy dandy <a href="https://help.libreoffice.org/Calc/Functions_by_Category" target="_blank">reference to LibreOffice Calc's functions</a>.</p>
<h2>Collaborating with others</h2>
<p>Earlier this year, I worked with two friends on a content strategy project. I'm in New Zealand, one friend is in British Columbia, and the other is in Toronto. Since we were working across time zones, each of us needed access to the spreadsheet we were using to track and coordinate our work. Emailing a LibreOffice Calc file wasn't an option. Instead, we turned to EtherCalc, and it worked very well.</p>
<p>Collaborating with EtherCalc starts with sharing your spreadsheet's URL with your collaborators. You can tell when someone else is working on the spreadsheet by the blue border that appears around one or more cells.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Collaborating in EtherCalc"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/ethercalc-collaborators.png" width="650" height="158" alt="Collaborating in EtherCalc" title="Collaborating in EtherCalc" /></div>
      
  </article></p>
<p>You and your collaborators can enter information into the spreadsheet simultaneously. All you need to remember is to respect the sanctity of those blue borders.</p>
<p>The <strong>Comment</strong> tab comes in handy when you need to ask a question, include additional information, or make a note to follow up on something. To add a comment, click the tab, and type what you need to type. When you're finished, click <strong>Save</strong>.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Adding a comment in EtherCalc"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/ethercalc-add-comment.png" width="650" height="130" alt="Adding a comment in EtherCalc" title="Adding a comment in EtherCalc" /></div>
      
  </article></p>
<p>You can tell when a cell has a comment by the small red triangle in the top-right corner of the cell. Hold your mouse pointer over it to view the comment.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Viewing a comment in EtherCalc"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/ethercalc-view-comment.png" width="442" height="111" alt="Viewing a comment in EtherCalc" title="Viewing a comment in EtherCalc" /></div>
      
  </article></p>
<h2>Final thoughts</h2>
<p>EtherCalc doesn't do everything that, say, <a href="https://www.libreoffice.org/discover/calc/" target="_blank">LibreOffice Calc</a> or <a href="http://www.gnumeric.org/" target="_blank">Gnumeric</a> can do. And there's nothing wrong with that. In this case, the <a href="https://en.wikipedia.org/wiki/Pareto_principle" target="_blank">80/20 rule</a> applies.</p>
<p>If you need a simple spreadsheet and one that you can work on with others, EtherCalc is a great choice. It takes a bit of getting used to, but once you do, you'll have no problems using EtherCalc.</p>
