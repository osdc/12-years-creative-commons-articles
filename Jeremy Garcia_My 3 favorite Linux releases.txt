<p>For the 25th anniversary of the Linux kernel, I gave a <a href="https://opensource.com/article/16/11/25-years-linux-5-minutes" target="blank">25 years of Linux in 5 minutes</a> lightning talk at <a href="https://allthingsopen.org/" target="_blank">All Things Open</a> in Raleigh. As we approach the kernel's 27th anniversary, I'd like to take a stroll down memory lane and look back at the three releases that have been most significant to me.</p>
<h2>Linux 2.4.x</h2>
<p>Initially released in January of 2001, the <a href="https://mirrors.edge.kernel.org/pub/linux/kernel/v2.4/" target="_blank">2.4.x series</a> was significant in the kernel's history and contained many major milestones. USB and Bluetooth support were added, ext3 was released, LVM was introduced, ISA Plug-and-Play support was included, and much, much more.</p>
<p>Although 2.4.x was a successful and long-lived series released during a critical time for Linux adoption, it's significant to me for an entirely different reason. Starting with 2.4.19, I <a href="https://www.linuxquestions.org/questions/lq-suggestions-and-feedback-7/first-linuxquestions-org-kernel-patch-is-released-29017/" target="_blank">maintained an -lq patchset</a> that became surprisingly popular. In fact, despite my assurances that it would probably result in your machine starting on fire, I found out a couple large companies were using it in production. The patchset began with my desire to have rmap and the O(1) scheduler in the same kernel. It grew based on feature requests from there. I ended up winding down the project after those two features were rolled into mainline, but it was a great learning experience and a rewarding project that I look back on fondly.</p>
<h2>Linux 0.99.5</h2>
<p>This was the Linux kernel version installed with the first version of Linux that I managed to use on a daily basis. The distribution was <a href="https://en.wikipedia.org/wiki/Yggdrasil_Linux/GNU/X" target="_blank">Yggdrasil</a>, included with <i><a href="https://www.wiley.com/en-us/Linux+Bible%2C+9th+Edition-p-9781118999882" target="_blank">Linux Bible</a></i>. Linux was a bit more raw back then, and it took me a few tries to convert my main machine. For those who haven't had to hand craft a XF86Config or manually "swapon", these were much different times.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Linux resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux commands cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Free online course: RHEL Technical Overview</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux networking cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">SELinux cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux common commands cheat sheet</a></li>
<li><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">What are Linux containers?</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Our latest Linux articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>Since that install, I haven't looked back; I've used Linux on my desktop ever since. I've installed many distributions since Yggdrasil, which have included many kernels more recent than 0.99.5, but as they say, you always remember your firsts.</p>
<h2>Linux 2.2.x</h2>
<p>The <a href="https://www.tldp.org/LDP/lame/LAME/linux-admin-made-easy/linux-2.2.x.html" target="_blank">2.2.x</a> release removed the global spinlock, but it was also the current version when I launched <a href="https://www.linuxquestions.org/" target="_blank">LinuxQuestions.org</a> and when I got my first full-time Linux job. These were two significant personal milestones, which make this series memorable for me. This release also stands out to me as it's the first mainline version to be passed to a maintainer, which was <a href="https://en.wikipedia.org/wiki/Alan_Cox" target="_blank">Alan Cox</a>.</p>
<h2>What's your favorite Linux kernel release?</h2>
<p>Those are the three Linux kernel versions that are the most memorable for me, but I'd like to know which version(s) are important to you, and why. Let me know in the comments.</p>
<p>We're also running a <a href="https://www.linuxquestions.org/questions/linux-general-1/what-was-your-first-linux-kernel-version-4175635820/" target="_blank">"What was your first Linux kernel version?" poll</a> on LinuxQuestions.org.</p>
