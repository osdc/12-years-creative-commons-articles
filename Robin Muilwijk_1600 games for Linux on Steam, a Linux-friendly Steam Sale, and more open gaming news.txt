<p>Hello, open gaming fans! In this week's edition, we take a look at 1600 games for Linux on Steam, a Linux-friendly Steam Sale, and an Arduino project for high scores, and more.</p>
<h2 style="text-align: left;">Open gaming roundup for November 7 — 13, 2015</h2>
<p><!--break--></p>
<h3>Steam for Linux passes 1600 games</h3>
<p>Another milestone this week for Linux gaming. The Steam Store is now offering just over <a href="http://linuxgamenews.com/post/132825175690/steam-for-linux-games-now-top-over-1600#.VkUHc3arSCp" target="_blank">1600 Linux native titles</a>, and we can expect a lot more to come. Plus, the Vulkan graphics API begins to clear the path for game engines such as Unity to gain traction.</p>
<h3>Linux-friendly Steam sale</h3>
<p>Valve is celebrating the launch of its Steam Machines this week with a sale that is full of Linux-compatible SteamOS games. Discounts go as far as 80% off—get the full list <a href="http://store.steampowered.com/sale/playwsteammachines/" target="_blank">here</a>. This sale ends on Monday, November 16, 2015.</p>
<p>A few titles on sale are: <em>Alien: Isolation</em>, <em>Borderlands 2</em>, <em>Middle Earth: Shadow of Mordor</em>, <em>Victor Vran ARPG</em>, and more.</p>
<h3>Pick of the week: Arduino playing Timberman</h3>
<p>If you are looking to beat some high scores, you might want to use an Arduino. That's what Valentin Heun did. He wrote a simple program for the Arduino to play <a href="https://www.google.nl/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;source=web&amp;cd=3&amp;cad=rja&amp;uact=8&amp;ved=0CC4QFjACahUKEwig_Nrs34vJAhVBKA8KHQo4D5k&amp;url=https%3A%2F%2Fplay.google.com%2Fstore%2Fapps%2Fdetails%3Fid%3Dcom.dm.timber%26hl%3Den&amp;usg=AFQjCNEDLLnjQyL0dy1hjkEwkn1SnKOpPw&amp;sig2=pocyeuE0h27O31leE2NtWQ" target="_blank">Timberman</a> and shared his code under the MIT license <a href="https://github.com/vheun/ArduinoPlaysTimberman" target="_blank">on GitHub</a>.</p>
<p><iframe src="https://player.vimeo.com/video/101663279" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" width="520" frameborder="0" height="292"></iframe></p>
<h3>Gaming events</h3>
<p>There are two upcoming events in January 2016 that are worth mentioning.</p>
<p>The <a href="http://globalgamejam.org/" target="_blank">Global Game Jam</a> (GGJ) is the world's largest game jam (creation) event on January 29 - 31, 2016. It takes place around the world at physical locations; it's like a hackathon focused on game development.</p>
<p>Another interesting event is <a href="https://fosdem.org/2016/" target="_blank">FOSDEM's</a> Open Game Development Room on February 1, 2016. "This devroom will be all about open source in games, whether it's open games, open tools, open assets, open hardware, or open data." More information is available <a href="http://gamelier.org/open-game-development-room-at-fosdem-call-for-participation/" target="_blank">here</a>.</p>
<h2>New games out for Linux</h2>
<h3>Krosmaster Arena</h3>
<p>Ankama announced the launch of <em>Krosmaster Arena</em> for Linux <a href="http://store.steampowered.com/app/329790/?snr=1_5_1100__1100" target="_blank">on Steam</a>. As a 3D tactical board game, Krosmaster Arena "fuses explosive animated battling with collectible 3D heroes for a captivating dose of tactical action either solo or against the world." Read the full launch story <a href="http://linuxgamenews.com/post/132958311400/krosmaster-arena-free-to-play-on-steam#.VkTzlHarSCp" target="_blank">on Linux Game News</a>.</p>
<blockquote><p>Challenge players over the world in a tactical board game of strategy and epic battles! Discover more than a hundred fighters and face down your enemies in ruthless battles.</p>
</blockquote>
<p><iframe src="https://www.youtube.com/embed/Y37eLqI-lHA" allowfullscreen="" width="520" frameborder="0" height="293"></iframe></p>
<h3>Magicka 2</h3>
<p>Paradox Interactive has released <a href="http://store.steampowered.com/app/238370/?snr=1_5_1100__1100" target="_blank"><em>Magicka 2</em></a> for Linux and the new Steam Machines, including full Steam controller support.</p>
<blockquote><p>The world’s most irreverent co-op action adventure returns! In the next chapter of Magicka, players ascend from the ruins of Aldrheim to experience a Midgård almost wiped free of Wizards after the Wizard Wars, with the few that do remain having either gone mad or extremely hostile toward all others.</p>
</blockquote>
<p><iframe src="https://www.youtube.com/embed/fWVaIE3HUXY" allowfullscreen="" width="520" frameborder="0" height="293"></iframe></p>
