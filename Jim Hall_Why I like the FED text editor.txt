<p>When I’m not <a href="https://opensource.com/article/19/9/business-creators-open-source-tools" target="_self">at work on my Linux desktop</a>, you can usually find me writing code for a legacy 16-bit system. <a href="https://opensource.com/article/19/6/freedos-anniversary" target="_self">FreeDOS</a> is an open source DOS-compatible operating system that you can use to play classic DOS games, run legacy business software, or develop embedded systems. Any program that works on MS-DOS should also run on FreeDOS.</p>
<p>I grew up with DOS. My family’s first personal computer was an Apple II clone, but we eventually upgraded to an IBM PC running DOS. I was a DOS user for over ten years, from the early 1980s until 1993, when I <a href="https://opensource.com/article/17/5/how-i-got-started-linux-jim-hall-freedos" target="_self">discovered Linux</a>.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Linux resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux commands cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Free online course: RHEL Technical Overview</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux networking cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">SELinux cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux common commands cheat sheet</a></li>
<li><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">What are Linux containers?</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Our latest Linux articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>I was impressed by the freedom afforded by Linux and open source software. So when Microsoft announced the end of DOS in 1994, with the forthcoming Windows 95, I decided to write my own open source DOS. That’s <a href="https://opensource.com/article/17/5/how-i-got-started-linux-jim-hall-freedos" target="_self">how FreeDOS started</a>.</p>
<p>All these years later, and I continue working on FreeDOS. It is an excellent hobby system, where I can run my favorite DOS applications and games. And yes, I still write code for FreeDOS.</p>
<p>My favorite editor for DOS programming is the FED editor. FED is a minimal text editor without a lot of visual flair. This minimal approach helps me make the most of the standard 80x25 screen in DOS. When editing a file, FED displays a single status line at the bottom of the screen, leaving you the remaining 24 lines to write your code. FED also supports color syntax highlighting to display different parts of your code in various colors, making it easier to spot typos before they become bugs.</p>
<p><article class="align-center media media--type-image media--view-mode-full"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/1-fed.png" width="720" height="400" alt="Writing a Solitaire game with FED" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>Writing a Solitaire game with FED - opensource.com</p>
</div>
      
  </article></p>
<p>When you need to do something in the menus, press the <strong>Alt</strong> key on the keyboard, and FED displays a menu on the top line. FED supports keyboard shortcuts too, but be careful about the defaults. For example, <strong>Ctrl-C</strong> will close a file, and <strong>Ctrl-V</strong> will change the view. If you don’t like these default keys, you can change the key mapping in the <strong>Config</strong> menu.</p>
<p><article class="align-center media media--type-image media--view-mode-full"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2-fed.png" width="720" height="400" alt="Tap the Alt key to bring up the menu" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>Tap the Alt key to bring up the menu - opensource.com</p>
</div>
      
  </article></p>
<p>If you don’t like the default black-on-white text display, you can change the colors under the <strong>Config</strong> menu. I prefer white-on-blue for my main text, with keywords in bright white, comments in bright blue, special characters in cyan, and numbers in green. FED makes it easy to set the colors you want.</p>
<p><article class="align-center media media--type-image media--view-mode-full"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/3-fed.png" width="720" height="400" alt="My preferred colors when programming on DOS" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>My preferred colors when programming on DOS - opensource.com</p>
</div>
      
  </article></p>
<p>FED is also a folding text editor, which means that it can collapse or expand portions of my code so that I can see more of my file. Tap <strong>Ctrl-F</strong> on a function name and FED will collapse the entire function. Folding works on other code, as well. I also use folding to hide <strong>for</strong> and <strong>while</strong> loops or other flow controls like <strong>if</strong> and <strong>switch</strong> blocks.</p>
<p><article class="align-center media media--type-image media--view-mode-full"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/4-fed.png" width="720" height="400" alt="Folding a function lets you see more of the file" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>Folding a function lets you see more of the file - opensource.com</p>
</div>
      
  </article></p>
<p>Shawn Hargreaves wrote and maintained FED from 1994 to 2004. Robert Riebisch has maintained FED since then. FED is distributed under the GNU GPL and supports DOS, Linux, and Windows.</p>
<p>You can download FED at <a href="https://www.bttr-software.de/products/fed/" target="_blank">https://www.bttr-software.de/products/fed/</a></p>
