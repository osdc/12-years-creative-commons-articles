<p>There seems to be a mad rush at the beginning of every year to find ways to be more productive. New Year's resolutions, the itch to start the year off right, and of course, an "out with the old, in with the new" attitude all contribute to this. And the usual round of recommendations is heavily biased towards closed source and proprietary software. It doesn't have to be that way.</p>
<p>Here's the sixth of my picks for 19 new (or new-to-you) open source tools to help you be more productive in 2019.</p>
<h2 id="wtf">WTF</h2>
<div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>The Linux Terminal</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/life/17/10/top-terminal-emulators?intcmp=7016000000127cYAAQ">Top 7 terminal emulators for Linux</a></li>
<li><a href="https://opensource.com/article/17/2/command-line-tools-data-analysis-linux?intcmp=7016000000127cYAAQ">10 command-line tools for data analysis in Linux</a></li>
<li><a href="https://opensource.com/downloads/advanced-ssh-cheat-sheet?intcmp=7016000000127cYAAQ">Download Now: SSH cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=7016000000127cYAAQ">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://opensource.com/tags/command-line?intcmp=7016000000127cYAAQ">Linux command line tutorials</a></li>
</ul></div>
</div>
</div>
</div>
<p>Once upon a time, I was doing some consulting at a firm that used <a href="https://en.wikipedia.org/wiki/Bloomberg_Terminal" target="_blank">Bloomberg Terminals</a>. My reaction was, "Wow, that's WAY too much information on one screen." These days, however, it seems like I can't get enough information on a screen when I'm working and have multiple web pages, dashboards, and console apps open to try to keep track of things.</p>
<p>While <a href="https://github.com/tmux/tmux" target="_blank">tmux</a> and <a href="https://www.gnu.org/software/screen/" target="_blank">Screen</a> can do split screens and multiple windows, they are a pain to set up, and the keybindings can take a while to learn (and often conflict with other applications).</p>
<p><a href="https://wtfutil.com/" target="_blank">WTF</a> is a simple, easily configured information dashboard for the terminal. It is written in <a href="https://golang.org/" target="_blank">Go</a>, uses a YAML configuration file, and can pull data from several different sources. All the data sources are contained in <a href="https://wtfutil.com/posts/modules/" target="_blank">modules</a> and include things like weather, issue trackers, date and time, Google Sheets, and a whole lot more. Some panes are interactive, and some just update with the most recent information available.</p>
<p>Setup is as easy as downloading the latest release for your operating system and running the command. Since it is written in Go, it is very portable and should run anywhere you can compile it (although the developer only builds for Linux and MacOS at this time).</p>
<p> </p>
<article class="align-center media media--type-image media--view-mode-full" title="WTF default screen"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/wtf-1.png" width="650" height="355" alt="WTF default screen" title="WTF default screen" /></div>
      
  </article><p>When you run WTF for the first time, you'll get the default screen, identical to the image above.</p>
<p> </p>
<article class="align-center media media--type-image media--view-mode-full" title="WTF's default config.yml"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/wtf-2.png" width="650" height="263" alt="WTF's default config.yml" title="WTF's default config.yml" /></div>
      
  </article><p>You also get the default configuration file in <strong>~/.wtf/config.yml</strong>, and you can edit the file to suit your needs. The grid layout is configured in the top part of the file.</p>
<pre>
<code class="language-text">grid:
  columns: [45, 45]
  rows: [7, 7, 7, 4]</code></pre><p>The numbers in the grid settings represent the character dimensions of each block. The default configuration is two columns of 40 characters, two rows 13 characters tall, and one row 4 characters tall. In the code above, I made the columns wider (45, 45), the rows smaller, and added a fourth row so I can have more widgets.</p>
<p> </p>
<article class="align-center media media--type-image media--view-mode-full" title="prettyweather on WTF"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/wtf-3.png" width="650" height="408" alt="prettyweather on WTF" title="prettyweather on WTF" /></div>
      
  </article><p>I like to see the day's weather on my dashboard. There are two weather modules to chose from: <a href="https://wtfutil.com/posts/modules/weather/" target="_blank">Weather</a>, which shows just the text information, and <a href="https://wtfutil.com/posts/modules/prettyweather/" target="_blank">Pretty Weather</a>, which is colorful and uses text-based graphics in the display.</p>
<pre>
<code class="language-text">prettyweather:
  enabled: true
  position:
    top: 0
    left: 1
    height: 2
    width: 1</code></pre><p>This code creates a pane two blocks tall (height: 2) and one block wide (height: 1), positioned on the second column (left: 1) on the top row (top: 0) containing the Pretty Weather module.</p>
<p>Some modules, like Jira, GitHub, and Todo, are interactive, and you can scroll, update, and save information in them. You can move between the interactive panes using the Tab key. The \ key brings up a help screen for the active pane so you can see what you can do and how. The Todo module lets you add, edit, and delete to-do items, as well as check them off as you complete them.</p>
<p> </p>
<article class="align-center media media--type-image media--view-mode-full" title="WTF dashboard with GitHub, Todos, Power, and the weather"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/wtf-4.png" width="650" height="368" alt="WTF dashboard with GitHub, Todos, Power, and the weather" title="WTF dashboard with GitHub, Todos, Power, and the weather" /></div>
      
  </article><p>There are also modules to execute commands and present the output, watch a text file, and monitor build and integration server output. All the documentation is very well done.</p>
<p>WTF is a valuable tool for anyone who needs to see a lot of data on one screen from different sources.</p>
