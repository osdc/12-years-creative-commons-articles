<p>If code is poetry, then this Acquia Certified <a href="http://training.acquia.com/registry/grand-masters" target="_blank">Grand Master</a> churns out several chapbooks a day from his home town of Bangalore, India.</p>
<!--break-->
<p>For going on two years, <a href="http://twitter.com/hussainweb/" target="_blank">Hussain Abbas</a> has been consistently achieving at <a href="https://axelerant.com/" target="_blank">Axelerant</a>—an India-based, open source incubator—where he holds the title of technical architect. His experience runs the gamut from x86 assembly and C#, to modern PHP-based platforms, to mainly Drupal these days. Hussain happened to be in the middle of a community summit at <a href="https://events.drupal.org/losangeles2015" target="_blank">DrupalCon Los Angeles</a> this year when we began talking about his dedication to the project he contributes to nonstop.</p>
<p><img src="https://opensource.com/sites/default/files/images/life/Interview%20banner%20Q%26A.png" width="100%" height="88" /></p>
<h3>What is your elevator pitch to sell Drupal to newcomers?</h3>
<p>I don't have just one! I have multiple depending on who's asking. My favorite is that Drupal is a secure and reliable application framework that can be used to build simple websites quickly and complex web applications easily.</p>
<h3>How did DrupalCon LA differ from your typical Drupal events?</h3>
<p>Mainly in two ways: scale and people. My typical Drupal events are relatively small with over 90% newcomers. They're a great place to learn Drupal, but you don't have a lot of contributors (or even participants). DrupalCons have a good sized crowd across the spectrum of Drupal usage.</p>
<h3>Tell me about Birds of a Feather gatherings (BoFs) and your opinion of them versus sessions.</h3>
<p>At a Con, I would be more likely to attend a BoF than a session. The most basic reason is that BoFs are not usually recorded, and even if they were, it is usually a discussion rather than a presentation.</p>
<h3>What is it like being a top core contributor for Drupal?</h3>
<p>I don't feel different, but I am overwhelmed when I am received with great gusto whenever I am in Drupal crowds. I appreciate the feeling very much and am grateful to everyone who has helped me reach where I am.</p>
<h3>Do you have any advice for developers on becoming core contributors?</h3>
<p>Don't wait to start—just jump in. Most, if not all, people are incredibly supportive, helpful, and nice to contributors new and old. Listen to the "Let It Go" variant from DrupalCon LA prenote. It is exactly like that.</p>
<h3>Would you care to wager on a Drupal 8 release date, and when you will start using it?</h3>
<p>My feeling is that we could see a final release in early 2016, although I hope sooner. I have already started using it for a prototype web application built in a hackathon at Axelerant last July.</p>
<hr /><p><em style="font-size: 13.0080003738403px; line-height: 1.538em; background-color: transparent;">Read more interviews and articles from <a href="https://opensource.com/tags/drupalcon-los-angeles-2015" target="_blank">DrupalCon Los Angeles 2015</a>.</em></p>
