<p>It's that time of year again! Our amazing community members shared their favorite open-source-related products and gifts with us, and we've pulled together some of the best for our annual holiday gift guide.</p>
<p>Kick off the holiday shopping season by checking out these 10 great gifts for open source enthusiasts. While you're at it, don't forget to enter our <a href="https://opensource.com/life/16/11/2016-holiday-gift-guide-sweepstakes">Holiday Gift Guide Giveaway</a> for a chance to win your very own LulzBot Mini 3D printer.</p>
<h2>LulzBot Mini 3D printer</h2>
<p><img align="right" src="https://opensource.com/sites/default/files/resize/images/life-uploads/lulzbot_mini-250x405.png" width="250" height="405" /></p>
<p>The <a href="https://www.lulzbot.com/store/printers/lulzbot-mini" target="_blank">LulzBot Mini</a> is the ultimate gift for the maker and DIY enthusiast in your life. This auto-bed-leveling, auto-nozzle-cleaning printer can produce designs up to 6"x6"x6.2" and heat filament to a max temperature of 300° Celsius (572° Fahrenheit). Best of all, open source and open hardware are <a href="https://opensource.com/business/15/11/open-ethos-powers-lulzbots-success">at the core</a> of the LulzBot line—LulzBot is the first and only 3D printer brand to have a product meet the <a href="http://www.oshwa.org/definition/" target="_blank">Open Source Hardware Association definition</a> and earn the Free Software Foundation's <a href="http://www.fsf.org/resources/hw/endorsement/respects-your-freedom" target="_blank">Respects Your Freedom</a> certification.</p>
<p><strong>$1,250 USD, <a href="https://www.amazon.com/gp/product/B00S54E1AI/ref=as_li_tl?ie=UTF8&amp;camp=1789&amp;creative=9325&amp;creativeASIN=B00S54E1AI&amp;linkCode=as2&amp;tag=alephobjectsi-20&amp;linkId=2957678a638094295e88ba63d0bf04b4" target="_blank">Amazon</a>.</strong></p>
<h2>Raspberry Pi starter kit</h2>
<p><img align="right" src="https://opensource.com/sites/default/files/resize/raspberrypistarterkit-250x728.jpg" width="250" height="728" /></p>
<p>Jump start your Raspberry Pi experience with this kit from Adafruit. In addition to the Raspberry Pi 3 Model B, the kit ships with LEDs, a breadboard, jumper wires, an 8GB microSD card, and more. Though the <a href="https://opensource.com/resources/what-raspberry-pi" target="_blank">Raspberry Pi</a> itself isn't open hardware, the 8GB microSD card comes preloaded with <a href="https://www.raspbian.org/" target="_blank">Raspbian</a>, the Raspberry Pi's free and open source default operating system.</p>
<p>Looking for a more affordable stocking stuffer? Consider the $5 USD <a href="https://www.adafruit.com/products/2885" target="_blank">Raspberry Pi Zero</a>.</p>
<p><strong>$89.95 USD, <a href="https://www.adafruit.com/products/3058" target="_blank">Adafruit</a>.</strong></p>
<h2>Open source swag</h2>
<p><img align="right" src="https://opensource.com/sites/default/files/resize/tshirt-200x359.png" width="200" height="359" /></p>
<p>Bring some visibility to free and open source projects with branded gear. From <a href="https://www.libretees.com/products/libreoffice-free-as-a-bird-t-shirt" target="_blank">LibreOffice T-shirts</a> to <a href="https://shop.fsf.org/gear/gnu-emacs-reference-mug" target="_blank">Emacs cheat sheet mugs</a>, there's bound to be a unique piece of swag that's right for every open source enthusiast on your list.</p>
<p><strong>Prices vary, <a href="https://www.libretees.com/" target="_blank">LibreTees</a>, <a href="https://shop.fsf.org/" target="_blank">FSF shop</a>, <a href="https://fsfe.org/order/" target="_blank">FSFE shop</a>, <a href="https://www.freewear.org/" target="_blank">FreeWear</a>, and more.</strong></p>
<h2>Vim keyboard stickers</h2>
<p><img align="right" src="https://opensource.com/sites/default/files/resize/vimkeys-250x416.jpg" width="250" height="416" /></p>
<p>Celebrate <a href="https://opensource.com/life/16/11/happy-birthday-vim-25" target="_blank">25 years</a> of the popular open source <a href="https://opensource.com/life/16/7/tips-getting-started-vim">Vim text editor</a> with these helpful keyboard shortcut stickers. The stickers are compatible with all default shortcuts in all versions of Vi and Vim. Amazon vendor 4Keyboard estimates this low-cost sticker set can increase productivity by anywhere from 15 to 40 percent for even the most experienced Vim users.</p>
<p><strong>$7.00 USD, <a href="https://www.amazon.com/NEW-VIM-EDITOR-KEYBOARD-STICKER/dp/B00359YK5E/ref=sr_1_1?s=office-products&amp;ie=UTF8&amp;qid=1479691654&amp;sr=1-1&amp;keywords=vi+sticker" target="_blank">Amazon</a>.</strong></p>
<h2>Donations and memberships</h2>
<p>If there's a minimalist on your list, consider the gesture of making a donation in their name or buying them a membership to one of the many great organizations fighting for open source software and user freedom. Every bit helps! As an added bonus, some organizations provide gifts like T-shirts and membership cards for certain donation levels.</p>
<p><strong>Name your price.</strong></p>
<h2><em>Secret Coders</em> graphic novels</h2>
<p><img align="right" src="https://opensource.com/sites/default/files/resize/secretcoders-150x1969.jpg" width="150" height="1969" /></p>
<p>Give the gift of computer science education with this graphic novel series from Gene Luen Yang and Mike Holmes. Each book in the New York Times bestselling <em>Secret Coders</em> series introduces young readers to a new computer science concept, with the subject matter getting more advanced as the series progresses. Two books are out now, with another coming soon.</p>
<p><strong>$8.99 USD (Paperback), <a href="https://www.amazon.com/Secret-Coders-Gene-Luen-Yang/dp/1626720754/ref=sr_1_1?ie=UTF8&amp;qid=1479695123&amp;sr=8-1&amp;keywords=secret+coders" target="_blank">Amazon</a>.</strong></p>
<h2>Cubetto programming toy</h2>
<p><img align="right" src="https://opensource.com/sites/default/files/resize/cubetto-250x575.jpg" width="250" height="575" /></p>
<p>The Cubetto wooden robot helps teach young children the basics of computer programming without the added complexity of screens, keyboards, and other complex hardware. A simple wooden control board and set of blocks lets kids "program" the Arduino-powered robot's movement and actions. The set includes 16 blocks and an interface board, robot, world map, and story book.</p>
<p><strong>$225.00 USD, <a href="https://www.primotoys.com/shop/" target="_blank">Primo Toys</a>.</strong></p>
<h2>System76 Lemur laptop</h2>
<p><img align="right" src="https://opensource.com/sites/default/files/resize/system76_lemur-250x772.jpg" width="250" height="772" /></p>
<p>Hardware maker System76 has a longstanding reputation for building high-quality machines that ship with Linux pre-installed, and its 14" Lemur laptop is no exception. The lightweight machine boasts a crisp 1080p display and low base weight of just 3.6 lbs. As with all System76 devices, it runs Linux with <a href="https://www.wired.com/2016/02/review-system76-lemur/" target="_blank">far fewer driver issues</a> than its mainstream counterparts.</p>
<p><strong>Starts at $699.00 USD, <a href="https://system76.com/laptops/lemur" target="_blank">System76</a>.</strong></p>
<h2>Mooltipass password keeper</h2>
<p><img align="right" src="https://opensource.com/sites/default/files/resize/mooltipass-250x570.jpg" width="250" height="570" /></p>
<p>The Mooltipass is an open source, encrypted offline password keeper that works with Windows, Linux, and OS X. Simply plug the Mooltipass into your device, insert the smartcard that comes with it, enter your pin, and you're logged in and ready to go.</p>
<p><strong>$170.00 USD, <a href="https://www.themooltipass.com/" target="_blank">Mooltipass</a>.</strong></p>
<h2>ColorHug2 display colorimeter</h2>
<p><img align="right" src="https://opensource.com/sites/default/files/resize/colorhug-250x653.jpg" width="250" height="653" /></p>
<p>The ColorHug is an open source display colorimeter that helps calibrate your screens and monitors for a more accurate display. The tiny device is perfect for photo and video junkies and supports Linux operating systems.</p>
<p><strong>£85.00 (about $105.00 USD), <a href="http://www.hughski.com" target="_blank">Hugski</a>.</strong></p>
