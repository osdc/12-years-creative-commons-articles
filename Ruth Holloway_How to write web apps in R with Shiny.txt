<p>Happy new year! I'm in the thick of working on a couple of larger articles this month, so look for those in the coming weeks. For this month's Nooks and Crannies, I want to point out briefly a great R library I've been playing with for my own self-education.</p>
<p>A close friend of mine has been hacking things in R lately. I've been intrigued, and I've been trying to squeeze in a little time to at least learn more about R and the kinds of things you can do with it. Figuring out the number-crunching capabilities is an ongoing struggle for me, as I'm not nearly the brilliant math-oriented mind my friend is. It's kind of a slow go for me, but I've been trying to relate it to my experience in other areas, and I started thinking about even very simple web applications.</p>
<p><a href="http://shiny.rstudio.com/" target="_blank">Shiny</a> is a toolkit from RStudio that makes creating web applications much easier. Installation is easy from an R console, just one line, and the latest stable version will be loaded up for you to use. There's a great <a href="http://shiny.rstudio.com/tutorial" target="_blank">tutorial</a> that walks you through the concepts of setting up application, building one skill on top of prior lessons. Shiny is licensed GPLv3, and the source is available on <a href="https://github.com/studio/shiny" target="_blank">GitHub</a>.</p>
<p>Here's a simple little web application written with Shiny:</p>
<pre>
<small>
<code class="language-cpp">library(shiny)

server &lt;- function(input, output, session) {
    observe({
	    myText &lt;- paste("Value above is: ", input$textIn)
		updateTextInput(session, "textOut", value=myText)
    })
}

ui &lt;- basicPage(
    h3("My very own sample application!"),
	textInput("textIn", "Input goes here, please."),
	textInput("textOut", "Results will be printed in this box")
)

shinyApp(ui = ui, server = server)</code></small></pre><p>When you type in the input box, the text is copied after the prefix in the output box. This is nothing fancy, but it shows you the fundamental structure of a Shiny application. The "server" section lets you handle all your back-end work, such as calculations, database retrieval, or whatever else the app needs to happen. The "UI" section defines the interface, which can be as simple or as complicated as needed.</p>
<p>Included in Shiny are extensive capabilities for styling and display themes using <a href="http://getbootstrap.com/" target="_blank">Bootstrap</a>, so you can, after learning a bit, create extensive, feature-rich applications for the web in R. Add-on packages can extend the capabilities to even more advanced JavaScript applications, templating, and more.</p>
<p>You can handle the back-end work of Shiny in several ways. If you're just running your application locally, having the library loaded will do the trick. For applications you want to serve out to the web, you can share them on <a href="http://shinyapps.io/" target="_blank">RStudio's Shiny website</a>, run an open source version of the Shiny server, or buy Shiny Server Pro from RStudio via a yearly subscription service.</p>
<p>Experienced R gurus may already know all about Shiny; it's been around a couple of years now. For people like me who come from a completely different sort of programming and want to learn a bit about R, I've found it quite helpful.</p>
