<p>On the docker-dev list someone asked about Fedora documentation that described how you add a user to the <code>docker</code> group. The user wanted to allow his users to do a <code>docker search</code> to try to find images that they could use.</p>
<!--break-->
<p>From the <a href="http://docs.docker.com/installation/fedora/">Docker installation documentation regarding Fedora</a>:</p>
<blockquote><p>Granting rights to users to use Docker</p>
<p>Fedora 19 and 20 shipped with Docker 0.11. The package has already been updated to 1.0 in Fedora 20. If you are still using the 0.11 version, you will need to grant rights to users of Docker.</p>
<p>The <code>docker</code> command line tool contacts the <code>docker</code> daemon process via a socket file <code>/var/run/docker.sock</code> owned by group <code>docker</code>. One must be a member of that group in order to contact the <code>docker -d</code> process.</p>
</blockquote>
<p>Luckily this documentation is somewhat wrong, and you still need to add users to the <code>docker</code> group in order for them to use <code>docker</code> from a non-root account. I would hope that all distributions have this policy.</p>
<p>On Fedora and RHEL we have the following permissions on the <code>docker.sock</code>:</p>
<p><code># ls -l /run/docker.sock<br />
srw-rw----. 1 root docker 0 Sep 19 12:54<br />
/run/docker.sock<br /></code>
</p><p>This means that only the root user or the users in the <code>docker</code> group can talk to this socket. Also since <code>docker</code> runs as<code>docker_t</code>, SELinux prevents all confined domains from connecting to this <code>docker.sock</code>.</p>
<h3>No Authorization controls from docker</h3>
<p>Docker currently does not have any authorization controls. If you can talk to the <code>docker</code> socket or if <code>docker</code> is listening on a network port and you can talk to it, you are allowed to execute all <code>docker</code> commands.</p>
<p>For example, if I add "dwalsh" to the <code>docker</code> group on my machine, I can execute.</p>
<p><code>&gt; docker run -ti --rm --privileged --net=host -v /:/host fedora /bin/sh<br />
# chroot /host<br /></code>
</p><p>At which point you, or any user that has these permissions, has total control on your system.</p>
<p>Adding a user to the docker group should be considered the same as adding:</p>
<p><code>USERNAME    ALL=(ALL)   NOPASSWD: ALL<br /></code>
</p><p>to the /etc/sudoers file. Any application the user runs on his machine can become root, even without him knowing. I believe a better more secure solution would be to write scripts to allow the user the access you want to allow.</p>
<p><code>cat /usr/bin/dockersearch<br />
#!/bin/sh<br />
docker search $@</code></p>
<p>Then set up sudo with:</p>
<p><code>USERNAME    ALL=(ALL)   NOPASSWD: /usr/bin/dockersearch</code></p>
<p>I hope to eventually add some kind of authorization database to <code>docker</code> to allow admins to configure which commands you would allow a user to execute, and which containers you might allow them to start/stop.</p>
<p>First eliminating the ability to execute <code>docker run --privileged</code> or <code>docker run --cap-remove</code> would be a step in the right direction. But, if you have read my other posts, you know that a lot more needs to be done to make <a href="http://www.projectatomic.io/blog/2014/09/keeping-up-with-docker-security/">containers contain</a>.</p>
<p><em>Originally posted on <a href="http://www.projectatomic.io" target="_blank">www.projectatomic.io</a> as "<a href="http://www.projectatomic.io/blog/2014/09/granting-rights-to-users-to-use-docker-in-fedora/" target="_blank">Granting Rights to Users to Use Docker in Fedora</a>." Reposted under Creative Commons.</em></p>
