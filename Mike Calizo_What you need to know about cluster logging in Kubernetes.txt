<p><meta charset="utf-8" /></p>
<p>Server and application logging is an important facility for developers, operators, and security teams to understand an application's state running in their production environment.</p>
<p>Logging allows operators to determine if the applications and the required components are running smoothly and detect if something unusual is happening so they can react to the situation.</p>
<p>For developers, logging gives visibility to troubleshoot the code during and after development. In a production setting, the developer usually relies on a logging facility without debugging tools. Coupled with logging from the systems, developers can work hand in hand with operators to effectively troubleshoot issues.</p>
<p>The most important beneficiary of logging facilities is the security team, especially in a cloud-native environment. Having the ability to collect information from applications and system logs enables the security team to analyze the data from authentication, application access to malware activities where they can respond to them if needed.</p>
<p>Kubernetes is the leading container platform where more and more applications get deployed in production. I believe that understanding the logging architecture of Kubernetes is a very important endeavor that every Dev, Ops, and Security team needs to take seriously.</p>
<p>In this article, I discuss how different container logging patterns in Kubernetes work.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on Kubernetes</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/topics/containers/what-is-kubernetes?intcmp=7016000000127cYAAQ">What is Kubernetes?</a></li>
<li><a href="https://www.redhat.com/en/engage/kubernetes-containers-storage-s-201911201051?intcmp=7016000000127cYAAQ">eBook: Storage Patterns for Kubernetes</a></li>
<li><a href="https://www.redhat.com/en/engage/openshift-storage-testdrive-20170718?intcmp=7016000000127cYAAQ">Test drive OpenShift hands-on</a></li>
<li><a href="https://opensource.com/kubernetes-dump-truck?intcmp=7016000000127cYAAQ">eBook: Getting started with Kubernetes</a></li>
<li><a href="https://www.redhat.com/en/resources/managing-containers-kubernetes-openshift-technology-detail?intcmp=7016000000127cYAAQ">An introduction to enterprise Kubernetes</a></li>
<li><a href="https://enterprisersproject.com/article/2017/10/how-explain-kubernetes-plain-english?intcmp=7016000000127cYAAQ">How to explain Kubernetes in plain terms</a></li>
<li><a href="https://opensource.com/downloads/kubernetes-raspberry-pi?intcmp=7016000000127cYAAQ">eBook: Running Kubernetes on your Raspberry Pi homelab</a></li>
<li><a href="https://opensource.com/downloads/kubernetes-cheat-sheet?intcmp=7016000000127cYAAQ">Kubernetes cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/kubernetes-sysadmin?intcmp=7016000000127cYAAQ">eBook: A guide to Kubernetes for SREs and sysadmins</a></li>
<li><a href="https://opensource.com/tags/kubernetes?intcmp=7016000000127cYAAQ">Latest Kubernetes articles</a></li>
</ul></div>
</div>
</div>
</div>

<h2>System logging and application logging</h2>
<p>Before I dig deeper into the Kubernetes logging architecture, I'd like to explore the different logging approaches and how both functionalities are critical features of Kubernetes logging.</p>
<p>There are two types of system components: Those that run in a container and those that do not. For example:</p>
<ul><li>The Kubernetes scheduler and <code>kube-proxy</code> run in a container.</li>
<li>The <code>kubelet</code> and container runtime do not run in containers.</li>
</ul><p>Similar to container logs, system container logs get stored in the <code>/var/log</code> directory, and you should rotate them regularly.</p>
<p>Here I consider container logging. First, I look at cluster-level logging and why it is important for cluster operators. Cluster logs provide information about how the cluster performs. Information like why pods got evicted or the node dies. Cluster logging can also capture information like cluster and application access and how the application utilizes compute resources. Overall, a cluster logging facility provides the cluster operators information that is useful for cluster operation and security.</p>
<p>The other way to capture container logs is through the application's native logging facility. Modern application design most likely has a logging mechanism that helps developers troubleshoot application performance issues through standard out (<code>stdout</code>) and error streams (<code>stderr</code>).</p>
<p>To have an effective logging facility, Kubernetes implementation requires both app and system logging components.</p>
<h2>3 types of Kubernetes container logging</h2>
<p>There are three prominent methods of cluster-level logging that you see in most of the Kubernetes implementations these days.</p>
<ol><li>Node-level logging agent</li>
<li>Sidecar container application for logging</li>
<li>Exposing application logs directly to logging backend</li>
</ol><h3>Node level logging agent</h3>
<p>I'd like to consider the node-level logging agent. You usually implement these using a DaemonSet as a deployment strategy to deploy a pod (which acts as a logging agent) in all the Kubernetes nodes. This logging agent then gets configured to read the logs from all Kubernetes nodes. You usually configure the agent to read the nodes <code>/var/logs</code> directory capturing <code>stdout</code>/<code>stderr</code> streams and send it to the logging backend storage.</p>
<p>The figure below shows node-level logging running as an agent in all the nodes.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Node-level logging agent"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/node-level-logging-agent.png" width="882" height="384" alt="Node-level logging agent" title="Node-level logging agent" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Mike Calizo, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>To set up node-level logging using the <code>fluentd</code> approach as an example, you need to do the following:</p>
<ol><li>First, you need to create a ServiceAccount called <code>fluentdd</code>. This service account gets used by the Fluentd Pods to access the Kubernetes API, and you need to create them in the logging Namespace with the label <code>app: fluentd</code>.<br /><pre><code class="language-yaml">#fluentd-SA.yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: fluentd
  namespace: logging
  labels:
    app: fluentd</code></pre><p>	You can view the complete example in this <a href="https://github.com/mikecali/kubernetes-logging-example-article" target="_blank">repo</a>.</p></li>
<li>You then need to create a ConfigMap <code>fluentd-configmap</code>. This provides a config file to the <code>fluentd daemonset</code> with all the required properties.<br /><pre><code class="language-yaml">#fluentd-daemonset.yaml
apiVersion: extensions/v1beta1
kind: DaemonSet
metadata:
  name: fluentd
  namespace: logging
  labels:
    app: fluentd
    kubernetes.io/cluster-service: "true"
spec:
  selector:
    matchLabels:
      app: fluentd
      kubernetes.io/cluster-service: "true"
  template:
    metadata:
      labels:
        app: fluentd
        kubernetes.io/cluster-service: "true"
    spec:
      serviceAccount: fluentd
      containers:
      - name: fluentd
        image: fluent/fluentd-kubernetes-daemonset:v1.7.3-debian-elasticsearch7-1.0
        env:
          - name:  FLUENT_ELASTICSEARCH_HOST
            value: "elasticsearch.logging.svc.cluster.local"
          - name:  FLUENT_ELASTICSEARCH_PORT
            value: "9200"
          - name: FLUENT_ELASTICSEARCH_SCHEME
            value: "http"
          - name: FLUENT_ELASTICSEARCH_USER
            value: "elastic"
          - name: FLUENT_ELASTICSEARCH_PASSWORD
            valueFrom:
              secretKeyRef:
                name: efk-pw-elastic
                key: password
          - name: FLUENT_ELASTICSEARCH_SED_DISABLE
            value: "true"
        resources:
          limits:
            memory: 512Mi
          requests:
            cpu: 100m
            memory: 200Mi
        volumeMounts:
        - name: varlog
          mountPath: /var/log
        - name: varlibdockercontainers
          mountPath: /var/lib/docker/containers
          readOnly: true
        - name: fluentconfig
          mountPath: /fluentd/etc/fluent.conf
          subPath: fluent.conf
      terminationGracePeriodSeconds: 30
      volumes:
      - name: varlog
        hostPath:
          path: /var/log
      - name: varlibdockercontainers
        hostPath:
          path: /var/lib/docker/containers
      - name: fluentconfig
        configMap:
          name: fluentdconf</code></pre><p>	You can view the complete example in this <a href="https://github.com/mikecali/kubernetes-logging-example-article" target="_blank">repo</a>.</p></li>
</ol><p>Now, I look at the code on how to deploy a <code>fluentd daemonset</code> as the log agent.</p>
<pre><code class="language-yaml">#fluentd-daemonset.yaml
apiVersion: extensions/v1beta1
kind: DaemonSet
metadata:
  name: fluentd
  namespace: logging
  labels:
    app: fluentd
    kubernetes.io/cluster-service: "true"
spec:
  selector:
    matchLabels:
      app: fluentd
      kubernetes.io/cluster-service: "true"
  template:
    metadata:
      labels:
        app: fluentd
        kubernetes.io/cluster-service: "true"
    spec:
      serviceAccount: fluentd
      containers:
      - name: fluentd
        image: fluent/fluentd-kubernetes-daemonset:v1.7.3-debian-elasticsearch7-1.0
        env:
          - name:  FLUENT_ELASTICSEARCH_HOST
            value: "elasticsearch.logging.svc.cluster.local"
          - name:  FLUENT_ELASTICSEARCH_PORT
            value: "9200"
          - name: FLUENT_ELASTICSEARCH_SCHEME
            value: "http"
          - name: FLUENT_ELASTICSEARCH_USER
            value: "elastic"
          - name: FLUENT_ELASTICSEARCH_PASSWORD
            valueFrom:
              secretKeyRef:
                name: efk-pw-elastic
                key: password
          - name: FLUENT_ELASTICSEARCH_SED_DISABLE
            value: "true"
        resources:
          limits:
            memory: 512Mi
          requests:
            cpu: 100m
            memory: 200Mi
        volumeMounts:
        - name: varlog
          mountPath: /var/log
        - name: varlibdockercontainers
          mountPath: /var/lib/docker/containers
          readOnly: true
        - name: fluentconfig
          mountPath: /fluentd/etc/fluent.conf
          subPath: fluent.conf
      terminationGracePeriodSeconds: 30
      volumes:
      - name: varlog
        hostPath:
          path: /var/log
      - name: varlibdockercontainers
        hostPath:
          path: /var/lib/docker/containers
      - name: fluentconfig
        configMap:
          name: fluentdconf</code></pre><p>To put this together: </p>
<pre><code class="language-bash">kubectl apply -f fluentd-SA.yaml \
              -f fluentd-configmap.yaml \
              -f fluentd-daemonset.yaml</code></pre><h3>Sidecar container application for logging</h3>
<p>The other approach is by using a dedicated sidecar container with a logging agent. The most common implementation of the sidecar container is by using <a href="https://www.fluentd.org/" target="_blank">Fluentd</a> as a log collector. In the enterprise deployment (where you won't worry about a little compute resource overhead), a sidecar container using <code>fluentd</code> (or <a href="https://www.g2.com/products/fluentd/competitors/alternatives" target="_blank">similar</a>) implementation offers flexibility over cluster-level logging. This is because you can tune and configure the collector agent based on the type of logs, frequency, and other possible tunings you need to capture.</p>
<p>The figure below shows a sidecar container as a logging agent.</p>
<article class="align-center media media--type-image media--view-mode-full" title="Sidecar container as logging agent"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/sidecar-container-as-logging-agent.png" width="1021" height="548" alt="Sidecar container as logging agent" title="Sidecar container as logging agent" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Mike Calizo, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article><p>For example, a pod runs a single container, and the container writes to two different log files using two different formats. Here's a configuration file for the pod:</p>
<pre><code class="language-yaml">#log-sidecar.yaml
apiVersion: v1
kind: Pod
metadata:
  name: counter
spec:
  containers:
  - name: count
    image: busybox
    args:
    - /bin/sh
    - -c
    - &gt;
      i=0;
      while true;
      do
        echo "$i: $(date)" &gt;&gt; /var/log/1.log;
        echo "$(date) INFO $i" &gt;&gt; /var/log/2.log;
        i=$((i+1));
        sleep 1;
      done
    volumeMounts:
    - name: varlog
      mountPath: /var/log
  - name: count-log
    image: busybox
    args: [/bin/sh, -c, 'tail -n+1 -f /var/log/1.log']
    volumeMounts:
    - name: varlog
      mountPath: /var/log
  volumes:
  - name: varlog
    emptyDir: {}</code></pre><p>To put this together, you can run this pod:</p>
<pre><code class="language-bash">$ kubectl apply -f log-sidecar.yaml</code></pre><p>To verify if the sidecar container works as a logging agent, you can do:</p>
<pre><code class="language-bash">$ kubectl logs counter count-log</code></pre><p>The expected output should look like this:</p>
<pre><code class="language-bash">$ kubectl logs counter count-log-1

Thu 04 Nov 2021 09:23:21 NZDT
Thu 04 Nov 2021 09:23:22 NZDT
Thu 04 Nov 2021 09:23:23 NZDT
Thu 04 Nov 2021 09:23:24 NZDT</code></pre><h3>Exposing application logs directly to logging backend</h3>
<p>The third approach, which (in my opinion) is the most flexible logging solution for Kubernetes container and application logs, is by pushing the logs directly to the logging backend solution. Although this pattern does not rely on the native Kubernetes capability, it offers flexibility that most enterprises need like:</p>
<ol><li>Extend a wider variety of support for network protocols and output formats.</li>
<li>Allows load balancing capability and enhances performance.</li>
<li>Configurable to accept complex logging requirements through upstream aggregation</li>
</ol><p>Because this third approach relies on a non-Kubernetes feature by pushing logs directly from every application, it is outside the Kubernetes scope.</p>
<h2>Conclusion</h2>
<p>The Kubernetes logging facility is a very important component for an enterprise deployment of a Kubernetes cluster. I discussed three possible patterns that are available for use. You need to find a suitable pattern for your needs.</p>
<p>As shown, the node-level logging using <code>daemonset</code> is the easiest deployment pattern to use, but it also has some limitations that might not fit your organization's needs. On the other hand, the sidecar pattern offers flexibility and customization that allows you to customize what type of logs to capture, giving you compute resource overhead. Finally, exposing application logs directly to the backend log facility is another enticing approach that allows further customization.</p>
<p>The choice is yours. You just need to find the approach that fits your organization's requirements.</p>
