<p style="text-align: center;">Hello, open gaming fans! In this week's edition, we take a look at the Raspberry Pi 2, some thoughts on Alibaba's recent investment into the Ouya, a way to test your Git knowledge, and more!</p>
<p style="text-align: center;">And, welcome to <a href="https://twitter.com/ajman1101" target="_blank">Andrew Mandula</a>! A new writer for our weekly open source and Linux games roundup.</p>
<h2 style="text-align: center;">Open source and Linux games roundup</h2>
<h3 style="text-align: center;">Week of January 31 - February 7, 2015
<!--break--></h3>
<h3>Emulation on the Raspberry Pi 2</h3>
<p>This week the team at Raspberry Pi unvieled the <a href="http://www.raspberrypi.org/raspberry-pi-2-on-sale/" target="_blank">Raspberry Pi 2</a>. Its increased horsepower means that emulation of the Nintendo 64 and Playstation 1 are possible, as the Raspberry Pi team shows us with some <a href="http://www.raspberrypi.org/emulation-on-raspberry-pi-2/" target="_blank">gameplay footage of Mario Kart 64 and Spyro the Dragon</a>.</p>
<h3>Alibaba secures a storefront by investing in OUYA</h3>
<p>Last week Alibaba, a large Chinese e-commerce company, invested $10M into the OUYA. This week, Brandon Sheffield, senior editor at Gamasutra, <a href="http://www.gamasutra.com/view/news/235565/Opinion_Why_Alibabas_investment_in_Ouya_makes_a_lot_of_sense.php?utm_source=feedburner&amp;utm_medium=feed&amp;utm_campaign=Feed%3A+GamasutraNews+%28Gamasutra+News%29" target="_blank">discusses the potential reasons Alibaba</a> had when making the investment.</p>
<h3>Git Game of skills</h3>
<p>One of the ways we find news for the open gaming roundup is by looking for interesting projects on GitHub. This week during the search, <a href="http://github.com/hgarc014/git-game" target="_blank">Git Game</a> stumbled across our radar; it's a game to test your git abilities. This is a great way to keep your git skills sharp or to test a coworker who says they are the ultimate Git Guru!</p>
<h3>Grow this list!</h3>
<p>I have recently started a site that lists games, tools, engines and other bits of software that are using OSI approved licenses. The list has just started, so help grow the list by <a href="http://github.com/ajman1101/FOSSawesome-Games" target="_blank">contributing to the JSON file</a>.</p>
<h3>New games out for Linux</h3>
<p><a href="http://store.steampowered.com/app/347480" target="_blank">Pixalo</a> is a retro platfromer that has players jump around pop culture references from the 60's and beyond.</p>
<p><iframe src="https://www.youtube.com/embed/qcDMCvEg7qo" allowfullscreen="" frameborder="0" height="293" width="520"></iframe></p>
<p><a href="http://store.steampowered.com/app/342980" target="_blank">Destination Sol</a> is a free-to-play game with RPG elements for ship upgrades and arcade action for its ship combat.</p>
<p><iframe src="https://www.youtube.com/embed/lnGvyRHqf8w" allowfullscreen="" frameborder="0" height="293" width="520"></iframe></p>
<p><a href="http://store.steampowered.com/app/324710" target="_blank">To Be or Not To Be</a> has players guiding Hamlet, Ophelia, and all your favorite characters from Shakespeare's Hamlet to their death (or not!). This one is some heavy reading though, so non-English beware.</p>
