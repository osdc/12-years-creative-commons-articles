<p>In a <a href="http://www.nytimes.com/2011/05/24/opinion/24hajdu.html?_r=1">NY Times op-ed</a>, David Hajdu posits that the spate of notable musicians all of the same age (turning 70 this year) is attributable to their turning 14 in the mid-1950s when rock 'n roll was just getting its start. "Fourteen is a formative age," his theory goes. What if that's not just for musicians? What about technology? And what does it mean for today's 14-year-olds?</p>
<p>"Pubertal growth hormones make everything we’re experiencing, including music, seem very important. We’re just reaching a point in our cognitive development when we’re developing our own tastes," said McGill University psychology professor Daniel J. Levitin in the piece.</p>
<p>Let's look at the late 1960s. It was the era of the Vietnam War and the civil rights movement. Yale began admitting women for the first time. Or in <a href="http://en.wikipedia.org/wiki/We_Didn%27t_Start_the_Fire#1965">musical terms</a>, "Birth control, Ho Chi Minh, Richard Nixon back again / Moonshot, Woodstock..."</p>
<p>Moonshot--these were also big years for technology. The moon landing in 1969 of course. Half a billion people watched it on TV in a year when the world population was only 3.6 billion. A year before that, the first manned Apollo mission included the first TV broadcast from orbit.</p>
<p>Back here on Earth, the first hypertext link system, NLS was born. A need for standards and a future in data led President Johnson to mandate all federal computers support ASCII encoding. In 1969, the first message was sent over the Advanced Research Projects Agency Network (ARPANET). Two years later, the first email was sent. UNIX was developed. These were hot years for both software and the things it could do.</p>
<p>So who was turning fourteen?</p>
<ul><li> <a href="http://en.wikipedia.org/wiki/Paul_Allen">Paul Allen</a>: January 21, 1967 </li>
<li><a href="http://en.wikipedia.org/wiki/Richard_Stallman">Richard Stallman</a>: March 16, 1967 </li>
<li><a href="http://en.wikipedia.org/wiki/Andy_Hertzfeld">Andy Hertzfeld</a>: April 6, 1967 </li>
<li><a href="http://en.wikipedia.org/wiki/Bill_Joy">Bill Joy</a>: November 8, 1968 </li>
<li><a href="http://en.wikipedia.org/wiki/Toru_Iwatani">Tōru Iwatani</a>: January 25, 1969 </li>
<li><a href="http://en.wikipedia.org/wiki/Steve_Jobs">Steve Jobs</a>: February 24, 1969 </li>
<li><a href="http://en.wikipedia.org/wiki/Tim_Berners-Lee">Tim Berners-Lee</a>: June 8, 1969 </li>
<li><a href="http://en.wikipedia.org/wiki/Bill_Gates">Bill Gates</a>: October 28, 1969 </li>
</ul><p>Was the technology burst that happened when they were all turning fourteen responsible for the things they chose to do? (Or was it a geek rebellion against the jocks? The Super Bowl began in 1967.)</p>
<p>Arguably, I could probably produce a similar list for any age group, but that's a pretty impressive set of resumes for the births of 1953-55. It's also worth noting that as I searched the birthdays of notable technology names, those not fitting these three years seemed clustered in other sets--it would be interesting to compare the current events of their fourteenth birthdays as well.</p>
<p>In his op-ed, Hadju concludes by asking what the 140-character and mashup culture of today will produce for those turning 14 in 2011. Consider:</p>
<ul><li>The culture emerging now is increasingly one of wider openness and transparency. </li>
<li>They're learning at an early age what a reputation economy is--what someone says about you (or what you say about yourself) can go much further and last much longer than a note passed in class. </li>
<li>Concepts like "open government" and "open data" pre-date their ability to vote. </li>
<li><a href="http://www.pcworld.com/businesscenter/article/228136/open_source_software_is_now_a_norm_in_businesses.html">Open source software has become mainstream</a>, years before any of them has had a job. </li>
</ul><p>If 14 is the formative year, and openness is the message of the day, what can we expect from the babies of 1997? Check back here around 2030, and we'll review.</p>
