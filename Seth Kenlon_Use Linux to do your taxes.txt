<p>In 2021, there are more reasons why people love Linux than ever before. In this series, I'll share 21 different reasons to use Linux. Here's how to use Linux to do your taxes.</p>
<p>Unless you want to be arrested, you generally have to pay taxes. If you live outside your country of birth, you probably even have to pay taxes twice or thrice. However unpleasant, taxes are a common thread among all computer users, and so it stands to reason that a computer ought to be able to help you pay your taxes. One of the complexities of this, though, is that each country is different in its requirements. For instance, in New Zealand, my income taxes are calculated automatically by my employer and the IRD. I don't have to account for tax rates, much less <em>file taxes</em> the way US citizens do.</p>
<p>This system drastically improves my available accounting software choices because none of my potential options need to have a contract with a tax agency to work correctly. If you're in the US, though, your tax application choices fully integrated with the IRS are limited, and none of them are open source.</p>
<h2>Tax applications</h2>
<p>In the US, you have three options. You can:</p>
<ul><li>Use <a href="https://www.irs.gov/filing/free-file-do-your-federal-taxes-for-free" target="_blank">Free File online</a> with the IRS directly</li>
<li>Try an IRS-approved application in WINE (a compatibility layer allowing Windows applications to run on Linux)</li>
<li>Use the excellent <a href="https://opensource.com/article/20/2/open-source-taxes" target="_blank">OpenTaxSolver</a> bundle</li>
</ul><h2>WINE</h2>
<p>WINE is not an emulator but a compatibility layer you can enable on Linux. Many Windows applications can launch and run as if they were native Linux applications. It's a technology that's been used for gaming for years, and it's most recently gotten a boost from gaming giant Valve Software, which uses the project in conjunction with their Steam client. You can install WINE from the software center of your Linux distribution or a terminal.</p>
<p>On Fedora or RHEL:</p>
<pre><code class="language-bash">$ sudo dnf install wine</code></pre><p>On Debian, Elementary, Pop, and similar:</p>
<pre><code class="language-bash">$ sudo apt install wine</code></pre><p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Linux resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux commands cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Free online course: RHEL Technical Overview</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux networking cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">SELinux cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux common commands cheat sheet</a></li>
<li><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">What are Linux containers?</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Our latest Linux articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>WINE isn't an application itself, so you don't use it directly. When you launch the install wizard for your tax application, it opens because WINE is installed on your system, and you proceed through the installer as usual.</p>
<p>A word of warning, though: tax applications by necessity release new versions every year, so keeping up with all the components required for system compatibility is difficult. WINE can't guarantee that an application will function as expected, and the time to find that out isn't when you're rushing to get your taxes done by the filing deadline. If you want to use WINE for a tax application you rely upon, investigate compatibility first.</p>
<h2>Free File</h2>
<p>Lately, the IRS has made an online tax filing system available to almost anyone running a browser. It's an easy way to ensure compliance with federal taxes and a quick way to submit. All you need is a web browser, and <a href="https://opensource.com/article/19/7/open-source-browsers" target="_blank">Linux has plenty of browsers you can choose from</a>.</p>
<h2>OpenTaxSolver</h2>
<p>Functionally, applications that get the IRS's approval are really only interactive compendiums of tax rules. You enter the data, you select what you believe to be accurate for your situation, and the application verifies your choices with the rules it has access to. And that's exactly what OpenTaxSolver does, but without the pretense of being an application that files your taxes for you. With OpenTaxSolver, you <a href="http://opentaxsolver.sourceforge.net/download.html" target="_blank">download the latest version of the software</a>, launch it, and fill out each question as prompted.</p>
<p><article class="align-center media media--type-image media--view-mode-full"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/ots.jpg" width="449" height="477" alt="OpenTaxSolver user interface" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>OpenTaxSolver interface</p>
</div>
      
  </article></p>
<p>In the end, you have a PDF that you print, sign, and mail. Depending on your state, OpenTaxSolver may be able to help you with your local taxes, too. Luckily, every state I've lived in has had very simple tax forms, so OpenTaxSolver's help is less vital for those.</p>
<p>OpenTaxSolver is simple enough not to get in your way but helpful enough to help remove the dread of filling out tax paperwork seemingly written to come across as a not very enjoyable riddle.</p>
<h2>The Linux advantage</h2>
<p>Before switching to Linux, I resented that filing taxes forced me to either not use my computer (a device specifically designed to <em>compute</em> numerical data) or to pay for a specific software that usually advertised itself as a clever way to get out of paying as much tax as I would likely calculate on my own. There never seemed to be a great answer to making taxes painless while avoiding mistakes, and without spending extra money. With Linux, I can file my US taxes with the help of OpenTaxSolver, for no extra cost, and on the platform of my choice. Try filing on Linux this year, and see if you can learn to love taxes.</p>
