<p><em>Editor's note: This article is part of a series on working with mental health conditions. It details the author's personal experiences and is not meant to convey professional medical advice or guidance.</em></p>
<p>A few months ago, I was chatting with one of our VPs about my new role and some of the work I was hoping to do with my team. I'd decided that one of my first actions in the new position would be to interview members of the senior leadership team to get their input on strategy. I'd be leading an entirely new function for my department, and I felt it would be good to get input from a wide array of stakeholders before jumping to action. This is part of my standard practice working in an open organization.</p>
<p>I had several goals for these one-on-one conversations. I wanted to be transparent in my work. I wanted to validate some of my hypotheses. I wanted to confirm that what I wanted to do would be valuable to other leaders. And I wanted to get some assurance that I was on the right track.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Learn about open organizations</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://theopenorganization.org">Download resources</a></li>
<li><a href="https://theopenorganization.community">Join the community</a></li>
<li><a href="https://opensource.com/open-organization/resources/open-org-definition?src=too_resource_menu3a">What is an open organization?</a></li>
<li><a href="https://opensource.com/open-organization/resources/open-org-maturity-model?src=too_resource_menu4a">How open is your organization?</a></li>
</ul></div>
</div>
</div>
</div>

<p>Or so I thought.</p>
<p>"Hmmm," said the VP after I had shared my initial ideas. He hesitated. "It's very broad." More hesitation. I'm not sure what I had expected him to say, but this was definitely not the kind of assurance I was hoping for. "You need to be careful about tilting at windmills." I didn't know what "<a href="https://en.wikipedia.org/wiki/Don_Quixote#Tilting_at_windmills">tilting at windmills</a>" meant, but it sounded like a good thing to avoid doing.</p>
<p>After having several more of these conversations over the course of a few weeks—many of them lively and fruitful—I came to one clear conclusion: Although I was getting lots of great input, I wasn't going to find any kind of consensus about priorities among the leadership team.</p>
<p>So why was I asking?</p>
<p>Eventually I realized what was <em>really</em> underlying my desire to seek input: not just a desire to learn from the people I was interviewing, but also a nagging question in my gut. "Am I doing the right thing?"</p>
<p>One manifestation of anxiety is a worry that we're doing something wrong, which is also related to <a href="https://en.m.wikipedia.org/wiki/Impostor_syndrome">imposter syndrome</a> (worry that we're going to be "found out" as unqualified for or incapable of the work or the role we've been given).</p>
<p>I've <a href="https://opensource.com/open-organization/20/1/leading-openly-anxiety">previously described</a> a positive "anxiety performance loop" that can drive high performance. I can occasionally fall into another kind of anxiety loop, an "inaction loop," which can <em>lower</em> performance. Figure 1 (below) illustrates it.</p>
<p style="text-align:center"><img alt="" height="443" src="https://opensource.com/sites/default/files/images/open-org/loop_2.png" width="488" /></p>
<p class="rtecenter"><em><sup>Figure 1: The "anxiety inaction loop"</sup></em></p>
<p>One challenge of this manifestation of anxiety is that it creeps up on me; I don't consciously realize that I'm stuck in it until something happens that makes it apparent.</p>
<p>In this case, that "something" was my coach.</p>
<p></p><div class="embedded-callout-text callout-float-right">My desire to get input from a large variety of stakeholders was resulting in so much input that it was preventing me from moving forward.</div>
<p>During a session when my coach was asking me questions about my work, I came to the realization that I was overly worried about whether I was on the right track. My desire to get input from a large variety of stakeholders (a legitimate thing to do) was resulting in so much input that it was preventing me from moving forward.</p>
<p>If I hadn't been fortunate enough to be working with a coach, I may never have had that realization—or I may have had it through a much harder experience. At some point, anxiety about whether you are doing the right thing could lead to failure, not because you did the wrong thing but because you didn't do anything at all.</p>
<p>I've found a few signs to help me realize if I'm in an anxiety inaction loop. I may be in one of these loops if:</p>
<ul><li>I talk about what I'm <em>planning</em> to do rather than what I <em>am</em> doing</li>
<li>I feel that I need just <em>one more person's</em> opinion, or I just need to check in with my boss <em>one more time</em>, before moving ahead</li>
<li>I am revising the same presentation repeatedly but never actually giving the presentation to anyone</li>
<li>I am avoiding or delaying something (such as giving a talk, or making a decision)</li>
</ul><p>Having tools for self-reflection is critical. The reality is that most of the time I'm not working with a coach, and I need to be able to recognize these symptoms on my own. That will only happen if I set aside time to reflect on how things are going and to ask myself hard questions about whether I am stuck in any of these stalling patterns. I've started to build this time into my calendar, usually on Friday afternoons, or early in the morning before my meetings start.</p>
<p></p><div class="embedded-callout-text callout-float-right">The fact that my anxiety can manifest both as dual worries—that I am not doing the right thing and that I am not doing enough—can be paradoxical.</div>
<p>Recognizing the anxiety loop is the first step. To get out of it, I've developed a few techniques like:</p>
<ul><li>Setting achievement milestones in 90 day increments, reviewing them on a weekly basis, and using this as an opportunity to reflect on progress.</li>
<li>Reminding myself that (in fact) I might <em>not</em> be doing the right thing. If that's the case, I'll get feedback, correct, and keep going (it won't be the end of the world).</li>
<li>Reminding myself that I am in this job for a reason; people want me to do the job. They don't want me to ask them what to do or wait for them to tell me what to do.</li>
<li>When seeking input from others, saying "This is what I am planning on doing" rather than "What do you think of this?" then either hearing objections if they arise or moving ahead if not.</li>
</ul><p>The fact that my anxiety can manifest both as dual worries—that I am not doing the right thing <em>and</em> that I am not doing enough—can be paradoxical. Over-correcting to get out of an anxiety inaction loop could put me right into <a href="https://opensource.com/open-organization/20/1/leading-openly-anxiety">an anxiety performance loop</a>. Neither situation feels like a healthy one.</p>
<p>As with most things, the answer is balance and moderation. Finding that balance is precisely the challenge anxiety creates. In some cases I may be worried I'm not doing enough; in others I may be worried that what I'm doing isn't right, which leads me to slow down. The best approach I have found so far is awareness—taking the time to reflect and trying to correct if I'm going too far in either direction.</p>
<h2 class="rtecenter">Read the first part</h2>
