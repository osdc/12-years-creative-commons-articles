<p>Creative Commons</p>
<ul><li>helps you share your knowledge and creativity with the world</li>
<li>develops, supports and stewards legal and technical infrastructure</li>
<li>maximizes digital creativity, sharing, and innovation</li>
</ul><p><!--break--></p><p>There is no registration to use the Creative Commons licenses. Licensing a work is as simple as selecting which of the Creative Commons licenses best meets your goals, and then marking your work in some way so that others know that you have chosen to release the work under the terms of that license.</p>
<p>Tell us your Creative Commons license story. Why do you like it? Or why don't you use this license? Have you thought about it, but have reservations and unanswered questions?</p>
