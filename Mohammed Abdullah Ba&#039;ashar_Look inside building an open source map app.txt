<p>Imagine yourself walking down the middle of a crowded street in a complex city like Cairo. Suddenly a protest builds ahead. A mass of people, cutting off the road. You try to evade, but then violence breaks out in mere seconds. You need help. Someone else, a car to get you out. A phone call might suffice, but wouldn't it be easier to notify all your friends that this place is dangerous and that you need their assistance? This is where a map-based social network could come into play.</p>
<p><!--break--></p>
<p>There are other, less extreme uses for an app like <a href="http://birdeye.yagasoft.com/" target="_blank" title="BirdI">Bird-I</a>, to manage your location and your friends on a Google Map. Our app, that is still in development, hopes to make it possible to locate and navigate to one another as well as create SOS alarms in emergency situations. Birdl is designed with the help of the open source community and free technologies, and it is open for download and further editing under the <a href="http://creativecommons.org/licenses/by-sa/4.0/" target="_blank" title="Creative Commons — Attribution-ShareAlike 4.0 International — CC BY-SA 4.0">CC BY-SA license</a>.</p>
<p>Development of the application has gone through many levels of trial and error. Ultimately, all our trials were extremely valuable for the team. Our development team went through the following phases over a three month period:</p>
<ol><li>Android-based interface and webview to support Google Maps extension</li>
<li>Systems database that would store:
<ul><li>User login info</li>
<li>User personal info (optional)</li>
<li>User history (optional)</li>
<li>User location schedule</li>
</ul></li>
<li>Login, registration, user settings</li>
<li>Proximity notifications when a user is close to friends</li>
<li>Event handling and navigation</li>
<li>Handling traffic and danger alarms</li>
<li>System validation and testing, extra features, iOS development</li>
</ol><p>Lessons learned? They can be summarized as:</p>
<ol><li><b>Never lose your originality.</b> Try your best to architect the software with a unique structure. Essentially, print your own thoughts into code. This includes the graphics used, the music, and everything else in your program.</li>
<li><b>Simplicity gives birth to complexity.</b> Any project, no matter how big it is, is composed from small concepts linked together. Master the small concepts to reach the bigger ones.</li>
<li><b>Ask for help.</b> Take the advice of any programmer you know. Don't give up when you can't translate your thoughts into code. Somebody might inspire you. This being said, don't paste others' code; use their advice as an inspiration for your own thoughts.</li>
<li><b>Get your fuel.</b> Without the proper goal, your project will probably fail. The fuel for our project was simply the aim of helping people, either by giving the source code as an educational model, or as an app for reaching friends easily in times of need.</li>
<li><b>Don't be afraid to share your secrets.</b> Inspire others with your work from time to time by sharing it with the world.</li>
</ol><p>Each project has a starting point. Without the proper material, getting stuck in an infinite loop of confusion is likely. Our team found salvation in the form of open source material and help from programmers all over the world who were ready to give advice and share their experience. We want to pass it on, so here are the three main tips we learned for working in the open source community:</p>
<ol><li>Know what you're asking for. Don't waste the time of others with questions that have already been answered.</li>
<li>Don't seek information from the community before making sure that you read the documentation.</li>
<li>Don't waste the time of any single group or person. Try your best to ask around from different people so you don't overload others.</li>
</ol><p>The conflicts a society faces are dynamic, so we hope an open source app like BirdI could be a good way to overcome them. And because the features can be freely extended, the ability of the app to solve conflicts can be even greater.</p>
