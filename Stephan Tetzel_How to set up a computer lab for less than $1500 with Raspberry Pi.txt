<p><em>Come back for more <a href="https://opensource.com/node/32941">Raspberry Pi articles</a> next week as we celebrate 3.14 on March 14, 2018.</em></p>
<p>Setting up a typical school computer lab with computers, monitors, keyboards, cables, switches, and more can quickly become very expensive. In developing and less prosperous countries (which comprise the largest part of the world), it's common for several hundred students to share a single computer. Even in the developed world, small schools and clubs with small budgets often are poorly equipped with technology.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Tanzanian students at Mekomariro Secondary School"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/tanzanian_students_at_mekomariro_secondary_school.jpg" width="615" height="480" alt="Tanzanian students at Mekomariro Secondary School" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>The <a href="https://www.raspberrypi.org/" target="_blank">Raspberry Pi</a> offers a low-cost alternative to traditional, expensive school computer labs. In fact, with Raspberry Pi, it's possible to set up a complete, reliable, low-maintenance computer lab for under US$ 1,500.</p>
<h2>Computer lab requirements</h2>
<p>If you would like others to use the computer lab, the system must have few technical hurdles and be <a href="https://openschoolsolutions.org/5-great-open-source-apps-we-use-in-our-school/" target="_blank">easy to manage</a> (e.g., management shouldn't take several hours per week).</p>
<p>In my opinion, these are the minimum requirements for a school computer lab:</p>
<ul><li>Is easy to use</li>
<li>Quickly establishes a target state</li>
<li>Offers short start times</li>
<li>Has reliable infrastructure</li>
<li>Is continuously virus- and malware-free</li>
<li>Is low-maintenance</li>
</ul><h2>Ingredients</h2>
<h3>Hardware</h3>
<p>If you have a US$ 1,500 budget to set up a computer lab, it's crucial to save money on hardware. Most of the things students do in a computer room today require only a browser and possibly an office suite. They don't need the latest, most powerful hardware. A set of Raspberry Pis or used computers is quite sufficient.</p>
<p>You can set up a lab with 12 Raspberry Pi workstations within this budget with the following:</p>
<ul><li>Server: It can be an older (3–7 years old) computer with a larger hard drive (15–20GB per computer) that costs between US$ 100 and US$ 150</li>
<li>Router (if not already available): US$ 20–50</li>
<li>Gigabit switch (used): US$ 50</li>
<li>Raspberry Pi 3 (starter kit) or used computer: US$ 50 per workstation</li>
<li>Monitors (if not already available): US$ 30–50 per workstation</li>
<li>Socket strips and network cables: US$ 100</li>
<li>Keyboard and mouse: US$ 15–20 per workstation</li>
</ul><p>If monitors or other hardware are donated or already available, you'll have enough money to set up more workstations.</p>
<h3>Software</h3>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on Raspberry Pi</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/resources/what-raspberry-pi?src=raspberry_pi_resource_menu1&amp;intcmp=701f2000000h4RcAAI">What is Raspberry Pi?</a></li>
<li><a href="https://opensource.com/downloads/raspberry-pi-guide?src=raspberry_pi_resource_menu2&amp;intcmp=701f2000000h4RcAAI">eBook: Guide to Raspberry Pi</a></li>
<li><a href="https://opensource.com/downloads/getting-started-raspberry-pi-cheat-sheet?src=raspberry_pi_resource_menu3&amp;intcmp=701f2000000h4RcAAI">Getting started with Raspberry Pi cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/kubernetes-raspberry-pi?src=raspberry_pi_resource_menu6&amp;intcmp=701f2000000h4RcAAI">eBook: Running Kubernetes on your Raspberry Pi</a></li>
<li><a href="https://www.redhat.com/en/resources/data-intensive-applications-hybrid-cloud-blueprint-detail?src=raspberry_pi_resource_menu7&amp;intcmp=701f2000000h4RcAAI">Whitepaper: Data-intensive intelligent applications in a hybrid cloud blueprint</a></li>
<li><a href="https://www.redhat.com/en/topics/edge-computing?src=raspberry_pi_resource_menu5&amp;intcmp=701f2000000h4RcAAI">Understanding edge computing</a></li>
<li><a href="https://opensource.com/tags/raspberry-pi?src=raspberry_pi_resource_menu4&amp;intcmp=701f2000000h4RcAAI">Our latest on Raspberry Pi </a></li>
</ul></div>
</div>
</div>
</div>
<p>There are many projects for the Raspberry Pi that make administration and maintenance of the computers very easy. For example, the <a href="http://pinet.org.uk/" target="_blank">PiNet project</a>, developed for schools several years ago, offers central user and storage management so students can log into any computer and access their data.</p>
<p>Recently, the Raspberry Pi Foundation released a similar project, <a href="https://www.raspberrypi.org/blog/piserver/" target="_blank">PiServer</a>, which doesn't limit its focus to schools. It also <span style="background-color: rgb(245, 246, 245);">centrally </span>manages users and storage. The Raspberry Pis start over the network and do not require an SD card.</p>
<p>Or you can set up a Raspberry Pi the way you want, then distribute this image to the other computers. A central Samba server is used to store the work results.</p>
<h2>Pros and cons</h2>
<p>Like with every project, there are advantages and disadvantages to building a computer network with Raspberry Pis.</p>
<p>Advantages are:</p>
<ul><li>Very inexpensive</li>
<li>Energy savings</li>
<li>Space savings</li>
<li>Quiet</li>
<li>Low maintenance</li>
</ul><p>Potential disadvantages include:</p>
<ul><li>Learning software (which usually requires Windows) may not run because PiNet or PiServer are running on Linux.</li>
<li>It's unfamiliar to most colleagues. Man is a creature of habit, and many people reject what they don't know. Offering an introduction, a tutorial course, or further training can decrease the fear and make people more comfortable with a new system.</li>
<li>The Raspberry Pi may not be powerful enough for compute-intensive applications that you want students to use.</li>
<li>The Raspberry Pi's network interface is not very fast (hopefully this will change in a future version).</li>
</ul><p>Another solution for schools with a bit more money is to use <a href="https://openschoolsolutions.org/4-linux-school-server-comparison/" target="_blank">linuxmuster.net</a> with more powerful, used computers. It provides more opportunities, including the ability to manage many computers (Ubuntu/Windows) with relatively little effort.</p>
<p>If you use a Raspberry Pi-based computer lab at your school or club, please share your experiences in the comments.</p>
<p><em>This article was first published on <a data-href="https://openschoolsolutions.org/possible-set-computer-lab-less-1500-yes/" href="https://openschoolsolutions.org/possible-set-computer-lab-less-1500-yes/" rel="noopener nofollow" target="_blank">openschoolsolutions.org</a> and is republished with permission.</em></p>
