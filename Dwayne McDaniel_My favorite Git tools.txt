<p>As with any other technology or skill, just <a href="https://opensource.com/article/22/11/git-beginners-guide" target="_blank">reading about Git</a> cannot make you proficient at it or make you an "advanced" user. Now it's time to dig into some of the tools in Git that I've found useful, and hopefully, that will help you use Git.</p>
<h2 id="reflog">Git reflog</h2>
<p>In my previous article, I wrote about <a href="https://opensource.com/article/22/11/git-beyond-just-committing-part-0" target="_blank">Git history</a> as a chain of commits, and that's a very good model for most purposes. However, Git actually remembers <em>everything</em> you do with Git, not just commits. You can see your entire recent history with <a href="https://git-scm.com/docs/git-reflog">git reflog</a>.</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-11/advancedgit.reflog.png" width="2048" height="1197" alt="Image of a Git reflog." /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Dwayne McDaniel, CC BY-SA 4.0)</p>
</div>
      
  </article><p>The log that <code>reflog</code> refers to is found in <code>.git/logs</code>, and it's called HEAD. Opening this file, you can quickly see all the actions taken recently. Inside <code>.git/logs/HEAD</code>, you see rows corresponding to the output of the <code>reflog</code> command.</p>
<p>You can checkout any of the states in a reflog. No matter what you do, Git gives you a way to easily get your files back to a previous state!</p>
<p>To checkout a previous state, use the command:</p>
<pre>
<code>
git checkout HEAD@{&lt;#&gt;}

</code></pre><p>Replace &lt;#&gt; with the number of steps behind HEAD you want to reference. For instance, if I wanted to check out the state right before I did the last <code>git pull</code> from my example, I would use the command <code>git checkout HEAD@{5}</code> Doing this puts Git into a detached head state, so I would need to make a new branch from there if I wanted to preserve any changes I wanted to make.</p>
<p>By default, your reflog sticks around for at least 30 days before Git cleans up its history. But it does not throw this info away; it packs it into a more compressed form. You don't need to wait for Git to tidy up. You can do it any time with the garbage.</p>
<p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">More on Git</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://opensource.com/resources/what-is-git?intcmp=7013a000002CxqLAAS" data-analytics-category="resource list" data-analytics-text="What is Git?">What is Git?</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-git?intcmp=7013a000002CxqLAAS" data-analytics-category="resource list" data-analytics-text="Git cheat sheet">Git cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-markdown?intcmp=7013a000002CxqLAAS" data-analytics-category="resource list" data-analytics-text="Markdown cheat sheet">Markdown cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/git-tricks-tips?intcmp=7013a000002CxqLAAS" data-analytics-category="resource list" data-analytics-text="eBook: Git tips and tricks">eBook: Git tips and tricks</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Register for your free Red Hat account">Register for your free Red Hat account</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/git?intcmp=7013a000002CxqLAAS" data-analytics-category="resource list" data-analytics-text="New Git articles">New Git articles</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<h2 id="gc-garbage-collection">Git gc (garbage collection)</h2>
<p>Even though the size of objects and files in your <code>.git</code> folders are tiny and highly compressed, when there are a lot of items present, then Git can start to slow down. After all, looking up entries from a list of 1,000 refs is more time-consuming than a list of only a handful of entries. From time to time, Git performs an internal garbage collection step, packing up all the objects and files not actively in use. It then stuffs them into a highly compressed pack file.</p>
<p>But you don't need to wait for Git to decide to clean up the unused objects. You can trigger this any time you want with the <code>git </code><code>gc</code> command.</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-11/advancedgit.gcbeforeandafter.png" width="1794" height="972" alt="Image of Git garbage collection before and after." /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Dwayne McDaniel, CC BY-SA 4.0)</p>
</div>
      
  </article><p>Next time you've made hundreds of commits locally, or you just notice that Git commits are taking a little longer than usual, try running <code>git gc</code>. It might speed things up.</p>
<h2 id="bisect">Git bisect</h2>
<p>The <code>git bisect</code> command is a powerful tool that quickly checks out a commit halfway between a known good state and a known bad state and then asks you to identify the commit as either good or bad. Then it repeats until you find the exact commit where the code in question was first introduced.</p>
<h2 id="worktree">Git worktree</h2>
<p>Imagine a scenario where you are working in a branch, very deep into adding new dependencies, and are not in any way ready to make a commit. Suddenly, your pager goes off. There's a fire is happening in production, and you need to drop everything, switch to a hotfix branch, and get that patch built quickly.</p>
<p>It's decision time. Do you could cross your fingers, make a commit, and hope you remember where you left off? Do you <code>git stash</code>, which might cause dependency issues and also mean you have to remember what exactly you were doing when you stashed? Or do you just checkout the other branch with a different folder and work as you usually would?</p>
<p>That last option might sound too good to be true, but that is precisely what <a href="https://opensource.com/article/21/4/git-worktree">Git worktree</a> allows you to do.</p>
<p>Normally with Git, you can only have one branch checked out at a time. This makes sense, now that you know that Git tracks the active branch with HEAD, which can only reference one ref at a time.</p>
<p>Git worktree sidesteps this limitation by making copies of branches outside the repository folder. Git knows that this other folder exists and that any commits made there need to be accounted for in the original repo folder. But the copy of the branch also has its own HEAD file keeping track of where Git is pointing in that other location!</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-11/git2git.tree_.copyingbranches.png" width="1550" height="798" alt="Image of git tree copying branches." /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Dwayne McDaniel, CC BY-SA 4.0)</p>
</div>
      
  </article><p>Git always has at least one worktree open, which you can see by running the command:</p>
<pre>
<code class="language-bash">git worktree list</code></pre><p>This command shows you the current folder where <code>.git</code> is located, the most recent commit ID, and the name of the currently checked out branch at the end of the line.</p>
<p>You can add more entries to the worktree list with the command:</p>
<pre>
<code class="language-bash">git worktree add /path-to-new-folder/&lt;branch-name&gt;</code></pre><p>The <code>add</code> directive creates a new folder at the specified path, named the same as the target branch. Git also sends a linked copy of the repo to that folder, with that branch already checked out. To work in that branch, all you need to do is change the directory then proceed to work as usual.</p>
<p>When you are ready to go back to the original work you were focused on before being interrupted, just change directly back to the original folder. Your work is in the exact same state you left it earlier.</p>
<p>When you are done and want to clean up after yourself, remove any worktree items you want with the command <code>git worktree remove /path-to-new-folder/&lt;branch-name&gt;</code></p>
<p>A few words of warning for using Git worktree:</p>
<ul><li>
<p>If a branch is assigned to a worktree, you can not check it out as you normally would. Attempting to checkout a branch that is already checked out throws an error.</p>
</li>
<li>
<p>It's a good idea to remove any unneeded worktree entries as soon as you're finished with them. Errors might occur when running other Git operations while multiple branches are checked out.</p>
</li>
<li>
<p>When working in a code editor like <a href="https://opensource.com/article/20/6/open-source-alternatives-vs-code" target="_blank">VS Code</a>, changing directory in the terminal doesn't automatically change the open folder in your editor. Remember to open the desired folder through the file menu to ensure you are modifying the correct version of the project files.</p>
</li>
</ul><h2 id="so-much-more">So much more to Git</h2>
<p>While it might feel like I've covered a lot here, I've actually only scratched the surface of what's possible with Git. It's possible to build entire applications that complement Git, and extending that even further is possible. Fortunately, there are also a lot of resources you can turn to when learning Git.</p>
<p>The one book I would recommend everyone read is absolutely free and you can download it right now. <a href="https://git-scm.com/book/en/v2">The Pro Git Book</a> covers how Git works in great detail and gives a lot of excellent examples. The one caveat about the book, though, is that it is a little out of date. The free version linked from the git-scm website is from 2014. Still, this book gives you the best foundational knowledge of how Git works and helps make any other Git-related topics more accessible.</p>
<p>There are also <a href="https://opensource.com/downloads/cheat-sheet-git">cheat sheets</a> and <a href="https://opensource.com/tags/git">articles</a> out there to help you become a Git expert in no time. But as I said earlier in this article, the only way to learn Git, or any other skill, is to practice, practice, practice.</p>
