<p>The <a href="https://docs.gtk.org/gobject/concepts.html">GLib Object System (GObject)</a> is a library providing a flexible and extensible object-oriented framework for C. In this article, I demonstrate using the 2.4 version of the library.</p>
<p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">Programming and development</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Red Hat Developers Blog">Red Hat Developers Blog</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Programming cheat sheets">Programming cheat sheets</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/services/training/learning-subscription?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Try for free: Red Hat Learning Subscription">Try for free: Red Hat Learning Subscription</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/bash-programming-guide?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="eBook: An introduction to programming with Bash">eBook: An introduction to programming with Bash</a></div>
              <div class="field__item"><a href="https://developers.redhat.com/cheat-sheets/bash-shell-cheat-sheet?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Bash shell scripting cheat sheet">Bash shell scripting cheat sheet</a></div>
              <div class="field__item"><a href="https://developers.redhat.com/e-books/modernizing-enterprise-java?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="eBook: Modernizing Enterprise Java">eBook: Modernizing Enterprise Java</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/building-applications?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="An open source developer's guide to building applications">An open source developer's guide to building applications</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Register for your free Red Hat account">Register for your free Red Hat account</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<p>The GObject libraries extend the ANSI C standard, with typedefs for common types such as:</p>
<ul><li><strong>gchar</strong>: a character type</li>
<li><strong>guchar</strong>: an unsigned character type</li>
<li><strong>gunichar</strong>: a fixed 32 bit width unichar type</li>
<li><strong>gboolean</strong>: a boolean type</li>
<li><strong>gint8</strong>, <strong>gint16</strong>, <strong>gint32</strong>, <strong>gint64</strong>: 8, 16, 32, and 64 bit integers</li>
<li><strong>guint8</strong>, <strong>guint16</strong>, <strong>guint32</strong>, <strong>guint64</strong>: unsigned 8, 16, 32, and 64 bit integers</li>
<li><strong>gfloat</strong>: an IEEE Standard 754 single precision floating point number</li>
<li><strong>gdouble</strong>: an IEEE Standard 754 double precision floating point number</li>
<li><strong>gpointer</strong>: a generic pointer type</li>
</ul><h2 id="function-pointers">Function pointers</h2>
<p>GObject also introduces a type and object system with classes and interfaces. This is possible because the ANSI C language understands function pointers.</p>
<p>To declare a function pointer, you can do this:</p>
<pre>
<code class="language-c">void (*my_callback)(gpointer data);
</code></pre><p>But first, you need to assign the <code>my_callback</code> variable:</p>
<pre>
<code class="language-c">void my_callback_func(gpointer data)
{
  //do something
}

my_callback = my_callback_func;
</code></pre><p>The function pointer <code>my_callback</code> can be invoked like this:</p>
<pre>
<code class="language-c">gpointer data;
data = g_malloc(512 * sizeof(gint16));
my_callback(data);</code></pre><h2 id="object-classes">Object classes</h2>
<p>The GObject base class consists of 2 structs (<code>GObject</code> and <code>GObjectClass</code>) which you inherit to implement your very own objects.</p>
<p>You embed GObject and GObjectClass as the first struct field:</p>
<pre>
<code class="language-c">struct _MyObject
{
  GObject gobject;
  //your fields
};

struct _MyObjectClass
{
  GObjectClass gobject;
  //your class methods
};

GType my_object_get_type(void);
</code></pre><p>The object’s implementation contains fields, which might be exposed as properties. GObject provides a solution to private fields, too. This is actually a struct in the C source file, instead of the header file. The class usually contains function pointers only.</p>
<p>An interface can’t be derived from another interface and is implemented as following:</p>
<pre>
<code class="language-c">struct _MyInterface
{
  GInterface ginterface;
  //your interface methods
};
</code></pre><p>Properties are accessed by <code>g_object_get()</code> and <code>g_object_set()</code> function calls. To get a property, you must provide the return location of the specific type. It’s recommended that you initialize the return location first:</p>
<pre>
<code class="language-c">gchar *str

str = NULL;

g_object_get(gobject,
  "my-name", &amp;str,
  NULL);</code></pre><p>Or you might want to set the property:</p>
<pre>
<code class="language-c">g_object_set(gobject,
  "my-name", "Anderson",
  NULL);</code></pre><h2 id="the-libsoup-http-library">The libsoup HTTP library</h2>
<p>The <code>libsoup</code> project provides an HTTP client and server library for GNOME. It uses GObjects and the glib main loop to integrate with GNOME applications, and also has a synchronous API for use in command-line tools. First, create a <code>libsoup</code> session with an authentication callback specified. You can also make use of cookies.</p>
<pre>
<code class="language-c">SoupSession *soup_session;
SoupCookieJar *jar;

soup_session = soup_session_new_with_options(SOUP_SESSION_ADD_FEATURE_BY_TYPE, SOUP_TYPE_AUTH_BASIC,
  SOUP_SESSION_ADD_FEATURE_BY_TYPE, SOUP_TYPE_AUTH_DIGEST,
  NULL);

jar = soup_cookie_jar_text_new("cookies.txt",
  FALSE);     

soup_session_add_feature(soup_session, jar);
g_signal_connect(soup_session, "authenticate",
  G_CALLBACK(my_authenticate_callback), NULL);</code></pre><p>Then you can create a HTTP GET request like the following:</p>
<pre>
<code class="language-c">SoupMessage *msg;
SoupMessageHeaders *response_headers;
SoupMessageBody *response_body;
guint status;
GError *error;

msg = soup_form_request_new("GET",
  "http://127.0.0.1:8080/my-xmlrpc",
  NULL);

status = soup_session_send_message(soup_session,
  msg);

response_headers = NULL;
response_body = NULL;

g_object_get(msg,
  "response-headers", &amp;response_headers,
  "response-body", &amp;response_body,
  NULL);

g_message("status %d", status);
cookie = NULL;
soup_message_headers_iter_init(&amp;iter,
response_headers);

while(soup_message_headers_iter_next(&amp;iter, &amp;name, &amp;value)){    
  g_message("%s: %s", name, value);
}

g_message("%s", response_body-&gt;data);
if(status == 200){
  cookie = soup_cookies_from_response(msg);
  while(cookie != NULL){
    char *cookie_name;
    cookie_name = soup_cookie_get_name(cookie-&gt;data);
    //parse cookies
    cookie = cookie-&gt;next;
  }
}</code></pre><p>The authentication callback is called as the web server asks for authentication.</p>
<p>Here’s a function signature:</p>
<pre>
<code class="language-c">#define MY_AUTHENTICATE_LOGIN "my-username"
#define MY_AUTHENTICATE_PASSWORD "my-password"

void my_authenticate_callback(SoupSession *session,
  SoupMessage *msg,
  SoupAuth *auth,
  gboolean retrying,
  gpointer user_data)
{
  g_message("authenticate: ****");
  soup_auth_authenticate(auth,
			 MY_AUTHENTICATE_LOGIN,
			 MY_AUTHENTICATE_PASSWORD);
}</code></pre><h2 id="a-libsoup-server">A libsoup server</h2>
<p>For basic HTTP authentication to work, you must specify a callback and server context path. Then you add a handler with another callback.</p>
<p>This example listens to any IPv4 address on localhost port 8080:</p>
<pre>
<code class="language-c">SoupServer *soup_server;
SoupAuthDomain *auth_domain;
GSocket *ip4_socket;
GSocketAddress *ip4_address;
MyObject *my_object;
GError *error;

soup_server = soup_server_new(NULL);
auth_domain = soup_auth_domain_basic_new(SOUP_AUTH_DOMAIN_REALM, "my-realm",
  SOUP_AUTH_DOMAIN_BASIC_AUTH_CALLBACK, my_xmlrpc_server_auth_callback,
  SOUP_AUTH_DOMAIN_BASIC_AUTH_DATA, my_object,
  SOUP_AUTH_DOMAIN_ADD_PATH, "my-xmlrpc",
  NULL);

soup_server_add_auth_domain(soup_server, auth_domain);
soup_server_add_handler(soup_server,
  "my-xmlrpc",
  my_xmlrpc_server_callback,
  my_object,
  NULL);

ip4_socket = g_socket_new(G_SOCKET_FAMILY_IPV4,
  G_SOCKET_TYPE_STREAM,
  G_SOCKET_PROTOCOL_TCP,
  &amp;error);

ip4_address = g_inet_socket_address_new(g_inet_address_new_any(G_SOCKET_FAMILY_IPV4),
  8080);
error = NULL;
g_socket_bind(ip4_socket,
  ip4_address,
  TRUE,
  &amp;error);
error = NULL;
g_socket_listen(ip4_socket, &amp;error);

error = NULL;
soup_server_listen_socket(soup_server,
  ip4_socket, 0, &amp;error);</code></pre><p>In this example code, there are two callbacks. One handles authentication, and the other handles the request itself.</p>
<p>Suppose you want a web server to allow a login with the credentials username <strong>my-username</strong> and the password <strong>my-password</strong>, and to set a session cookie with a random unique user ID (UUID) string.</p>
<pre>
<code class="language-c">gboolean my_xmlrpc_server_auth_callback(SoupAuthDomain *domain,
  SoupMessage *msg,
  const char *username,
  const char *password,
  MyObject *my_object)
{
  if(username == NULL || password == NULL){
    return(FALSE);
  }

  if(!strcmp(username, "my-username") &amp;&amp;
     !strcmp(password, "my-password")){
    SoupCookie *session_cookie;
    GSList *cookie;
    gchar *security_token;
    cookie = NULL;

    security_token = g_uuid_string_random();
    session_cookie = soup_cookie_new("my-srv-security-token",
      security_token,
      "localhost",
      "my-xmlrpc",
      -1);

     cookie = g_slist_prepend(cookie,
       session_cookie);  
     soup_cookies_to_request(cookie,
       msg);
    return(TRUE);
  }
  return(FALSE);
}</code></pre><p>A handler for the context path <strong>my-xmlrpc</strong>:</p>
<pre>
<code class="language-c">void my_xmlrpc_server_callback(SoupServer *soup_server,
  SoupMessage *msg,
  const char *path,
  GHashTable *query,
  SoupClientContext *client,
  MyObject *my_object)
{
  GSList *cookie;
  cookie = soup_cookies_from_request(msg);
  //check cookies
}</code></pre><h2 id="a-more-powerful-c">A more powerful C</h2>
<p>I hope my examples show how the GObject and libsoup projects give C a very real boost. Libraries like these extend C in a literal sense, and by doing so they make C more approachable. They do a lot of work for you, so you can turn your attention to inventing amazing applications in the simple, direct, and timeless C language.</p>
