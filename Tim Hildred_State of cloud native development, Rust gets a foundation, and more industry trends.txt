<p>As part of my role as a principal communication strategist at an enterprise software company with an open source development model, I publish a regular update about open source community, market, and industry trends. Here are some of my and their favorite articles from that update.</p>
<h2><a href="https://mbird.biz/writing/do-i-need-kubernetes.html" target="_blank">Do I Need Kubernetes?</a></h2>
<blockquote><p>The most successful Kubernetes implementations I’ve seen are those where infrastructure specialist(s) work with developers to make sure workloads are well-configured, standardised, protected from one another and have defined patterns of communication. They’ll handle the initial setup of an application on an infrastructure, hopefully wired up to a build/release system so dev teams can ship new versions of the code without help. In a lot of ways this mirrors the wider culture of your organisation - if engineering is a chaotic free-for-all of bad communication and ill-defined responsibilities your hosting environment will mirror it. At best this leads to unreliability; at worst an unreliable, unmaintainable, expensive mess.</p>
</blockquote>
<p><strong>The impact</strong>: Getting the most out of Kubernetes is going to require teamwork between experts in different domains.</p>
<h2><a href="https://www.cncf.io/blog/2020/08/14/state-of-cloud-native-development/" target="_blank"> State of Cloud Native Development</a></h2>
<blockquote><p>While containers are popular among backend developers, not all have heard of or use Kubernetes as a tool to manage them. Whereas 59% of backend developers have used containers in the last 12 months, only 27% of developers have used Kubernetes to manage them. We believe this is caused by<strong> developers not realizing that many of the most popular orchestration engines use Kubernetes under the hood. </strong></p>
</blockquote>
<p><strong>The impact</strong>: This is a classic marketing catch 22: build a differentiated brand or include an existing brand in your own to take advantage of name recognition.</p>
<h2 class="post-full-title"><a href="https://www.freecodecamp.org/news/the-kubernetes-handbook/" target="_blank">The Kubernetes Handbook</a></h2>
<blockquote><p>Apart from being very powerful, Kubernetes is known as quite hard to get started with. I won't say it's easy, but if you are equipped with the prerequisites and go through this guide attentively and with patience, you should be able to:</p>
<ul><li>Get a solid understanding of the fundamentals.</li>
<li>Create and manage Kubernetes clusters.</li>
<li>Deploy (almost) any application to a Kubernetes cluster.</li>
</ul></blockquote>
<p><strong>The impact</strong>: This is an amazing resource for someone trying to get their heads around Kuberenetes.</p>
<h2 class="mvp-post-title left entry-title" itemprop="headline"><a href="https://www.financederivative.com/why-banks-need-to-embrace-open-source-communities/" target="_blank">Why banks need to embrace open source communities</a></h2>
<blockquote><p>A company that contributes its developers’ time and resources to an open source community gets rewarded with the output of hundreds of developers working on the same code. This leads to a magnification effect, by virtue of the fact you’re expanding your team many times over while also benefiting from a much more diverse pool of talent. The result is that organisations can be captains of the product development process and work together with the community to design features and functionalities that meet their needs and keep up with customer demands.</p>
</blockquote>
<p><strong>The impact</strong>: The more the merrier! The flipside for everyone who doesn't work at a bank is that their project gets closer to "good enough for a bank" and the technical requirements that entail.</p>
<header><h2><a href="https://blog.rust-lang.org/2020/08/18/laying-the-foundation-for-rusts-future.html" target="_blank">Laying the foundation for Rust's future</a></h2>
<blockquote><p>As the project has grown in size, adoption, and maturity, we’ve begun to feel the pains of our success. We’ve developed legal and financial needs that our current organization lacks the capacity to fulfill. While we were able to be successful with Mozilla’s assistance for quite a while, we’ve reached a point where it’s difficult to operate without a legal name, address, and bank account. “How does the Rust project sign a contract?” has become a question we can no longer put off.</p>
</blockquote>
<p><strong>The impact</strong>: This seems like some good problems to have for the Rust community; maturity level "needs an independent foundation" activated!</p>
<p><em>I hope you enjoyed this list and come back next week for more open source community, market, and industry trends.</em></p>
</header>