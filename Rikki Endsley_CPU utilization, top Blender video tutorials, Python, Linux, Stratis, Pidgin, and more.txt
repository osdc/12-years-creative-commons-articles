<p>Opensource.com contributor <a href="https://opensource.com/users/brendang" target="_blank">Brendan Gregg</a> gave an UPSCALE talk at SCaLE 16x in March and we posted the video, <a href="https://opensource.com/article/18/4/cpu-utilization-wrong" target="_blank">CPU utilization is wrong</a>, which was a big hit with readers last week. Read on for more of our most popular posts from the week of April 23-29:</p>
<ol><li><a href="https://opensource.com/article/18/4/cpu-utilization-wrong" target="_blank">CPU utilization is wrong</a>, by Opensource.com &amp; Brendan Gregg</li>
<li><a href="https://opensource.com/article/18/4/introduction-python-bytecode" target="_blank">An introduction to Python bytecode</a>, by James Bennett</li>
<li><a href="https://opensource.com/article/18/4/easy-2d-game-creation-python-and-arcade" target="_blank">How to create a 2D game with Python and the Arcade library</a>, by Paul Vincent Craven</li>
<li><a href="https://opensource.com/article/18/4/stratis-lessons-learned" target="_blank">What Stratis learned from ZFS, Btrfs, and Linux Volume Manager</a>, by Andy Grover</li>
<li><a href="https://opensource.com/article/18/4/5-best-blender-video-tutorials-beginners" target="_blank">5 top Blender video tutorials for beginners</a>, by Jason van Gumster</li>
<li><a href="https://opensource.com/resources/python/template-libraries" target="_blank">3 Python template libraries compared</a>, by Jason Baker</li>
<li><a href="https://opensource.com/article/18/4/gnu-core-utilities" target="_blank">An introduction to the GNU Core Utilities</a>, by David Both</li>
<li><a href="https://opensource.com/article/18/4/linux-filesystem-forensics" target="_blank">Breach detection with Linux filesystem forensics</a>, by Gary Smith</li>
<li><a href="https://opensource.com/article/18/4/pidgin-open-source-replacement-skype-business" target="_blank">Get started with Pidgin: An open source replacement for Skype</a>, by Ray Shimko</li>
<li><a href="https://opensource.com/article/18/4/stratis-easy-use-local-storage-management-linux" target="_blank">Configuring local storage in Linux with Stratis</a>, by Andy Grover</li>
</ol><h2>LISA18 CFP now open</h2>
<p>The <a href="https://www.usenix.org/conference/lisa18/call-for-participation" target="_blank&quot;">CFP for LISA18</a> is open, and <a href="https://twitter.com/brendangregg" target="_blank">Brendan Gregg</a> and I will co-chair this year's event, which will be held Oct 29-31 in downtown Nashville. See <a href="http://www.brendangregg.com/blog/2018-04-30/usenix-lisa-2018-cfp.html" target="_blank">Brendan's latest blog post</a> for more details.</p>
<p>Send us your talk and tutorial proposals by May 24th. Follow <a href="https://twitter.com/LISAConference" target="_blank">LISA on Twitter</a> to stay updated on deadlines and announcements. If you have questions or feedback, contact us at <a href="mailto:lisa18chairs@usenix.org">lisa18chairs@usenix.org</a>.</p>
<h2>Available now: DevOps hiring guide</h2>
<p>Hiring for DevOps talent presents its own challenges. Our new <a href="https://opensource.com/article/18/4/ultimate-devops-hiring-guide" target="_blank">DevOps hiring guide</a> will help you navigate the unique dynamics when building your DevOps team.</p>
<h2>Free 2017 Open Source Yearbook download</h2>
<p>Our third annual <a href="https://opensource.com/yearbook/2017" target="_blank">open source community yearbook</a> rounds up the top projects, technologies, and stories from 2017.</p>
<h2>Call for articles</h2>
<p>We want to see your Linux command-line and open source programming-related story ideas. Send article proposals, along with brief outlines, to <a href="mailto:rikki@opensource.com">rikki@opensource.com</a>.</p>
<p>Stay up on what's going on with Opensource.com by subscribing to our <a href="https://opensource.com/email-newsletter" target="_blank">highlights newsletter</a>.</p>
<p>Check out the <a href="https://opensource.com/resources/editorial-calendar">editorial calendar</a> for a preview of what's ahead. Got a story idea? <a href="https://opensource.com/how-submit-article">Send us a proposal</a>!</p>
