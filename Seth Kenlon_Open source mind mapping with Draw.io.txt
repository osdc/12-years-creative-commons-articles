<p>There's something special about maps. I remember opening the front book cover of JRR Tolkien's <em>The Hobbit</em> when I was younger, staring at the hand-drawn map of Middle Earth, and feeling the wealth of possibility contained in the simple drawing. Aside from their obvious purpose of actually describing where things are in relation to other things, I think maps do a great job of expressing potential. You could step outside and take the road this way or that way, and if you do, just think of all the new and exciting things you'll be able to see.</p>
<p>Maps don't have to be literal to be valuable and full of possibility, though. Some maps describe a thought process, plan, algorithm, or even random ideas desperately trying to fit together in a potential work of art. Call them "mind maps" or "flowcharts" or "idea boards." They're easy to make with the open source <a href="http://draw.io" target="_blank">Draw.io</a> application.</p>
<h2>Install Draw.io</h2>
<p>Draw.io is designed as an open source online app, so you can either use it as an online application, download the <a href="https://github.com/jgraph/drawio-desktop" target="_blank">desktop version</a>, or <a href="https://github.com/jgraph/drawio" target="_blank">clone the Git repository</a> and host it on your own server.</p>
<h2>Mapping with Draw.io</h2>
<p>When you first start Draw.io, you need to choose where you want to save your data. If you're hosting Draw.io yourself, your choices depend on which API keys you have access to. You can choose from several online storage services for the online public instance, depending on what you've got an account for. If you don't want to store your data on somebody else's server, you can also choose to save your work on a local drive. If you're unsure yet, you can click <strong>Decide later</strong> to continue into the app without choosing anything.</p>
<p>The Draw.io interface has a big workspace in the center, the main toolbar on the left, a toolbar along the top, and a properties panel on the right.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Draw.io interface"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/draw-io-ui.png" width="1440" height="791" alt="Draw.io interface" title="Draw.io interface" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Seth Kenlon, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>The workflow is simple: </p>
<ol><li>Select a shape from the left toolbar.</li>
<li>Edit the shape in the workspace.</li>
<li>Add another shape, and connect them.</li>
</ol><p>Repeat that process, and you've got a map.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Draw.io example"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/draw-io-example.jpg" width="955" height="594" alt="Draw.io example" title="Draw.io example" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Seth Kenlon, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on productivity</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/downloads/collaboration-tools-ebook">5 open source collaboration tools</a></li>
<li><a href="https://opensource.com/downloads/organization-tools">6 open source tools for staying organized</a></li>
<li><a href="https://opensource.com/downloads/desktop-tools">7 open source desktop tools</a></li>
<li><a href="https://opensource.com/tags/tools">The latest on open source tools</a></li>
</ul></div>
</div>
</div>
</div>

<h2>Project planning</h2>
<p>When you first take on a big task, you often have a pretty clear idea of your desired outcome. Say you want to start a community project to paint a mural. Your desired outcome is a mural. It's pretty easy to define, and you can more or less picture the results in your mind's eye.</p>
<p>Once you start moving toward your goal, however, you have to start figuring out the details. Where should the mural be painted? How do you get permission to paint on a public wall, anyway? What about paint? Is there a special kind of paint you ought to use? Will you apply the paint with brushes or airbrushes? What kind of specialized equipment do you need for painting? How many people and hours will it take to paint a mural? What about support services for the painters while they work? And what's the painting going to be about, anyway? A single idea can very quickly become overwhelming the closer you get to making it happen.</p>
<p>This doesn't just apply to painting murals, producing a play, or making a movie. It applies to nearly any non-trivial endeavor. And it's exactly what an application like Draw.io can help map out.</p>
<p>Here's how you can use Draw.io to create a project flowchart:</p>
<ol><li>Start with a brainstorming session. No thoughts are too trivial or big. Make a box for every idea, and double-click the box in the Draw.io workspace to enter text.</li>
<li>Once you have all the ideas you can possibly think of on your workspace, drag and drop them into related groups. The goal is to create little clouds, or clusters, of tasks that more or less go together because they're part of the same process.</li>
<li>Once you've identified the clusters of related tasks, give those tasks a name. For instance, if you're painting a mural, then the tasks might be <em>approval</em>, <em>design</em>, <em>purchase</em>, <em>paint</em>, reflecting that you need to get permission from your local government first, then design the mural, then purchase the supplies, and finally paint the mural. Each task has component parts, but broadly you've now determined the workflow for your project.</li>
<li>Connect the major tasks with arrows. Not all processes are entirely linear. For instance, after you've obtained permission from your city council, you might have to come back to them for final approval once you've designed what you intend to paint. That's normal. It's a loop, there's some back and forth, but eventually, you break out of the loop to proceed to the next stage.</li>
<li>Armed with your flowchart, work through each task until you've reached your ultimate goal.</li>
</ol><h2>Mind mapping </h2>
<p>Mind maps tend to be less about progress and more about maintaining a certain state or putting many ideas into perspective. For instance, suppose you've decided to reduce the amount of waste in your life. You have some ideas about what you can do, but you want to organize and preserve your ideas, so you don't forget them.</p>
<p>Here's how you can use Draw.io to create a mind map:</p>
<ol><li>Start with a brainstorming session. No thoughts are too trivial or big. Make a box for every idea, and double-click the box in the Draw.io workspace to enter text. You can also give boxes a background color by selecting the box and clicking a color swatch in the right properties panel.</li>
<li>Arrange your ideas into groups or categories.</li>
<li>Optionally, connect ideas with arrows that relate directly to one another.</li>
</ol><p><article class="align-center media media--type-image media--view-mode-full" title="Draw.io waste reduction example"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/draw-io-export.jpg" width="711" height="491" alt="Draw.io waste reduction example" title="Draw.io waste reduction example" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Seth Kenlon, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<h2>Saving your diagram</h2>
<p>You can save your diagram as a PNG, JPG image, Draw.io XML, or plain XML file. If you save it as XML, you can open it again in Draw.io for further editing. An exported image is great for sharing with others.</p>
<h2>Use Draw.io</h2>
<p>There are many great diagramming applications, but I don't make diagrams often, so having Draw.io available is convenient at a moment's notice. Its interface is simple and easy to use, and the results are clean and professional. Next time you need to brainstorm, organize ideas, or plan a project, start with Draw.io.</p>
