<p>Like everyone else, those of us who use Linux sometimes need to purchase a new computer. Although the days of poor hardware compatibility are long gone, it is still possible to run into issues. Just going to a local store, like Intrex here in Raleigh, or any of the big box stores, and watching the display models run Windows demo programs does not help my confidence levels about Linux support for all aspects of their hardware.</p>
<p>So it would be really nice to have a way to test a computer I might consider purchasing before I actually take it to the checkout counter. I thought about this problem for a while and then read an interesting email posted to the <a href="http://www.trilug.org/">TriLUG</a> (Triangle Linux User Group) right here in Raleigh. A gentleman named Alan Porter wrote that he had taken a live USB stick to a couple stores and rebooted the computers into a live version of Linux. He found a nice Acer laptop that worked perfectly with Linux that way.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Linux resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux commands cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Free online course: RHEL Technical Overview</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux networking cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">SELinux cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux common commands cheat sheet</a></li>
<li><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">What are Linux containers?</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Our latest Linux articles</a></li>
</ul></div>
</div>
</div>
</div>

<p>So why didn't I think of that? Well, I guess we can't all have the great ideas like Alan.</p>
<p>But when I stopped to think about it, that is exactly what I do when I plug a Fedora 25 Live USB stick into any computer I already own to do an installation. If the Fedora Live USB works well enough to be able to even show the live desktop with its "Install to hard drive" option, most of the hardware should already be compatible, such as the motherboard, USB, graphics adapter, keyboard, hard drive, and mouse or touchpad. Only a few things would remain to be tested, perhaps wired and wireless networking, the sound system, and an integrated webcam.</p>
<h2>Buying laptops</h2>
<p>I talked to Alan to get more information on his experiences testing computers in retail stores using a live Linux USB drive. He has been using Linux for about twenty years so has had a good bit of experience dealing with compatibility issues. Alan has two daughters in school, and they desperately needed upgraded hardware. He wanted to buy two identical laptops but did not want to support/maintain Windows and he did not want to buy two expensive MacBooks. Both girls are familiar enough with Ubuntu that they would be OK with that. Most of their schoolwork is web-based and operating system-agnostic. Alan also imagined that they would be doing a few audio/video projects which might require some open source tools.</p>
<p>In the event that they absolutely must run Windows, he knew that his daughters could run a 90-day copy of Windows on a downloadable virtual machine from Microsoft's <a href="https://developer.microsoft.com/en-us/microsoft-edge/tools/vms/">modern.ie</a> site in VirtualBox.</p>
<p>Alan said that they looked briefly online at the major big box stores but he was pressed for time with Christmas rapidly approaching. He knew that Costco would have a narrowed-down choice of general-purpose laptops. He looked at one other big box store. They had a wider variety, but the models he and his daughters had also seen at Costco were a little more expensive there.</p>
<p>He did not ask permission to perform his testing, but one tech-savvy store employee did wander by. Alan told the staffer what he was doing and what his results were. Alan made sure to tell them that he was not altering anything on their computer, but that he wanted to see if it would run the software he had before they bought anything.</p>
<p>I asked Alan about other ways he has used live Linux USB sticks and he said, "I almost always have a live USB stick on me, or at least in my backpack. Most of the times I've used it, I have had a specific mission, such as copying the contents of a hard disk while it is not mounted or in use. Once or twice, I have used a live USB stick when a system was not bootable. In those cases, I would boot from the USB, mount the filesystem, and run a tool like fsck on the filesystem."</p>
<p>He has also used a live USB stick when resizing or encrypting hard disk partitions. He says, "In these cases, you can't have the filesystem mounted when you work on them."</p>
<p>He went on to say, "Way back in 2005, rather than bring my big heavy laptop on vacation, I brought a live CD for use in the hotels and public internet spots. We were booking hotels and transportation along the way, and I really didn't like the idea of using public (virus-infected) PCs at libraries, hotels, or shops. That worked surprisingly well, though these days, I have a much smaller and lighter laptop that would be easier to carry. And of course, phones are much more capable now than they were in 2005."</p>
<h2>Creating the live USB</h2>
<p>Creating a live USB drive is very easy these days. The Fedora Project website has an article on <a href="https://fedoraproject.org/wiki/How_to_create_and_use_Live_USB">how to create and use Live USB</a> page that contains instructions for doing just that on Linux and Windows. The Fedora Project page describes several methods for creating the live USB device, but my favorite is quite trivial and can be done from the command line with minimal work.</p>
<p>Select a USB memory stick. It should be large enough to contain the ISO image. Almost any USB memory stick will work, even a used one, so long as it is VFAT and bootable. I overwrite Live USB sticks all the time using this procedure and they work fine. If necessary you can use Linux <strong>fdisk</strong> to partition it and format it as VFAT and then make it bootable.</p>
<p>Download the desired ISO image from the <a href="https://getfedora.org/">Get Fedora</a> website. I usually use either the "Workstation" version or the KDE spin. I plug in the USB stick and determine the device ID that has been assigned to it using the <strong>dmesg</strong> command. The results will look something like the following sample and will be at the very end of the output. The numbers in the square braces on the left of the output are time stamps representing the time since the system was booted.</p>
<pre>
<small><code class="language-text">[194543.805330] usb 3-4.1.3: new high-speed USB device number 14 using xhci_hcd
[194543.893708] usb 3-4.1.3: New USB device found, idVendor=abcd, idProduct=1234
[194543.893712] usb 3-4.1.3: New USB device strings: Mfr=1, Product=2, SerialNumber=3
[194543.893714] usb 3-4.1.3: Product: UDisk           
[194543.893716] usb 3-4.1.3: Manufacturer: General 
[194543.893717] usb 3-4.1.3: SerialNumber: 1404161029354477560901
[194543.893980] usb-storage 3-4.1.3:1.0: USB Mass Storage device detected
[194543.894076] scsi host14: usb-storage 3-4.1.3:1.0
[194544.949733] scsi 14:0:0:0: Direct-Access     General  UDisk            5.00 PQ: 0 ANSI: 2
[194544.950065] sd 14:0:0:0: Attached scsi generic sg10 type 0
[194544.950181] sd 14:0:0:0: [sdh] 15761408 512-byte logical blocks: (8.07 GB/7.52 GiB)
[194544.950296] sd 14:0:0:0: [sdh] Write Protect is off
[194544.950299] sd 14:0:0:0: [sdh] Mode Sense: 0b 00 00 08
[194544.951111] sd 14:0:0:0: [sdh] No Caching mode page found
[194544.951114] sd 14:0:0:0: [sdh] Assuming drive cache: write through
[194544.954241] sdh: sdh1 sdh2 sdh3
[194544.955162] sd 14:0:0:0: [sdh] Attached SCSI removable disk</code></small></pre><p>In this case, the device has been assigned as <strong>sdh</strong>, so we address it using the device file, <strong>/dev/</strong><strong>sdh</strong>. Then I use the following command to install the image onto the device, where <strong>if</strong> is the input file and <strong>of</strong> is the output file. Remember that in Linux, <a href="https://opensource.com/life/15/9/everything-is-a-file">everything is a file</a>.</p>
<pre>
<small><code class="language-text">dd if=Fedora-Workstation-Live-x86_64-25-1.3.iso of=/dev/sdh</code></small></pre><p>This command may take several minutes to complete depending on the speed of your computer. When completed you can use <strong>fdisk</strong> to list the partitions on the live USB device. My procedure results in three partitions.</p>
<p>Finally, you can test the live USB device you have just created by using it to boot from. You may have to set BIOS to boot from removable USB devices and set it to boot USB before the regular hard drives.</p>
<h2>Testing an old netbook</h2>
<p>After talking with Alan, I decided that I needed to get a little experience with actual testing using a live Linux environment before I headed out to some retail stores.</p>
<p>I used the Fedora 24 and 25 32-bit USB sticks created using the above procedure to test my old—very old as it was one of the very first netbooks—ASUS EeePC. I had to press Esc during BIOS to get to boot menu then select the USB device. Fedora 25 Live recognized all of the internal devices including the graphics adapter, the wired network card, and the wireless adapter. I did have to turn on the wireless in BIOS because I had turned it off many years ago to save power. At that time wireless was just not that ubiquitous.</p>
<p>I also took a little bit of time to test the wireless network, the sound, and the graphics using a couple videos from YouTube over a wireless connection. The sound worked just fine and the video was perfect with no skips or dropouts.</p>
<p>So I installed Fedora 25 on my EeePC and it works just fine. It just sits there and runs Fedora day after day.</p>
<h2>On the road</h2>
<p>After that test, it was time to move on to the real world. So I took my live Fedora USB sticks on the road to two different stores here in the Raleigh, NC, area. I started with Intrex, a local computer store that has knowledgeable staff, a good selection of laptops, desktops, and parts. I build all of my computers with parts I purchase from Intrex. </p>
<p>On March 13, 2017, I went to Intrex with my trusty live USB drives. I tried the live USB drive on three laptops and one ASUS desktop. One Acer laptop booted to the live USB but spewed lots of errors. One Acer worked just fine, and on one ASUS laptop, I was unable to get to interrupt into BIOS so I could change the boot sequence. The Acer desktops worked just fine and it was easy to alter the boot sequence.</p>
<p>Jonathan Farmer, one of the staff at Intrex, knows me so I am pretty certain that is why he left me to my task after he asked me if I needed any assistance. I later told him what I was doing and got his permission to use both his name and that of the store.</p>
<p>Next, I went to Best Buy. I tried several laptops before someone on staff asked if I needed help. I told him no, that I was just testing some of the computers, and he looked at me a bit strangely but walked away without further comment. Some laptops here failed to boot, others booted and worked ok with the few tests I tried. No one at Best Buy paid much attention to what I was doing and I suspect that no one really cared. It was almost too easy and I could have been installing any kind of malware on the demo systems.</p>
<h2>Conclusions</h2>
<p>This test was not really about the performance or compatibility of the computers. It was all about the ability to boot a live USB on demo computers in a retail store and gauge the reaction of the staff. At Intrex, Jonathan walked by a couple times before asking if I needed help, but basically left me alone because he knew and understood what I was doing. None of the staff at Best Buy paid me much attention.</p>
<p>Other stores might possibly have reacted differently, but based on Alan's experience on top of mine, I don't think that they would have.</p>
<p>So the bottom line is that neither Alan nor I had any trouble walking into retail stores and using a live USB drive to test computers for compatibility with Fedora. And the testing showed that, at the very least, some of the computers that I tested would need more time to figure out how to get booted into BIOS or how to modify BIOS to boot from the live USB drive. I think I would have been able to do that, given more time than I was willing to spend for this test.</p>
<p>I did spend about 20 minutes trying to coax one laptop to boot to the USB drive but was never able to do so. In any real situation where I was looking to purchase a product, I would rule out any device that I could not get to boot to the live USB drive in that time—probably even less time than that.</p>
<p>Testing demo hardware in a retail store is a great way to help narrow down the choices. Just getting the live USB drive booted to the Fedora desktop is a big step in the right direction. And the tools available in the live environment allow testing of the rest of the system's peripherals. Even though I did not try to connect to the wireless access points I could see using the desktop Networks icon, just seeing the list indicated that wireless was working.</p>
<p>Now if I could just figure out a way to use this testing method when I purchase the individual components to build my own computers.</p>
