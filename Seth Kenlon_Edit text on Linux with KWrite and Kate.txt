<p>A text editor is often a good example application to demonstrate what a programming framework is capable of producing. I myself have written at least three example text editors in articles about <a href="https://opensource.com/article/17/4/pyqt-versus-wxpython">wxPython and PyQt</a>, and <a href="https://opensource.com/article/20/12/write-your-own-text-editor">Java</a>. The reason they're seen as easy apps to create is because the frameworks provide so much of the code that's hardest to write. I think that's also the reason that most operating systems provide a simple desktop text editor. They're useful to the user and easy for the developer.</p>
<p>On the KDE Plasma Desktop, there are two text editors to choose from: the humble KWrite and the powerful Kate. They share between them a library called KTextEditor from the KDE Framework, which provides robust text editing options, so no matter which one you choose you have more features than you're probably used to from a "basic" text editor that just happens to be included with your desktop. Using the same component for text editing across text editors means that once you grow accustomed to one text editing interface in KDE, you're essentially familiar with them all: KWrite, Kate, KDevelop, and more.</p>
<h2 id="install-kwrite-or-kate">Install KWrite or Kate</h2>
<p>KWrite and Kate are maintained in the same <a href="https://invent.kde.org/utilities/kate">development repository</a>.</p>
<p>However, they’re distributed as separate applications and have different use cases.</p>
<p>If you have KDE Plasma Desktop installed, you probably already have KWrite installed, but you may need to install Kate separately.</p>
<pre><code class="language-bash">$ sudo dnf install kwrite kate</code></pre><p>KWrite is available from <a href="http://apps.kde.org/kwrite">apps.kde.org/kwrite</a>, and Kate from <a href="https://apps.kde.org/kate">apps.kde.org/kate/</a>.</p>
<p>Both can be installed through KDE Discover, and KWrite can be <a href="https://opensource.com/article/21/11/install-flatpak-linux">installed as a flatpak</a>.</p>
<h2>KWrite, the not-so-basic editor</h2>
<p>Getting started with KWrite is easy. You launch it from your applications menu and you start typing. If you have no expectations that it's anything more than the most basic of text editors, then you can treat it as a simple digital notepad.</p>
<p><article class="media media--type-image media--view-mode-full" title="The KWrite text editor"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/kwrite-ui.jpg" width="733" height="531" alt="The KWrite text editor" title="The KWrite text editor" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter">(Seth Kenlon, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>, Text courtesy <a href="https://www.gutenberg.org/cache/epub/41445/pg41445.txt" target="_blank" rel="ugc">Project Gutenberg</a>)</p>
</div>
      
  </article></p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Linux resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux commands cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Free online course: RHEL Technical Overview</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux networking cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">SELinux cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux common commands cheat sheet</a></li>
<li><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">What are Linux containers?</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Our latest Linux articles</a></li>
</ul></div>
</div>
</div>
</div>

<p>All the usual conventions apply. Type text into the big text field, click the Save button when you're done.</p>
<p>However, what sets KWrite apart from a standard desktop editor is that it uses KTextEditor.</p>
<h2>Bookmarks</h2>
<p>While you're working in KWrite or Kate, you can create temporary bookmarks to help you find important places in your document. To create a bookmark, press <strong>Ctrl+B</strong>. You can move to a bookmark by selecting it in the <strong>Bookmark</strong> menu.</p>
<p>Bookmarks aren't permanent metadata, and they don't get stored as part of your document, but they're useful devices when you're working and need to move back and forth between sections. In other text editors, I used to just type some random word, like <em>foobar, </em>and then perform a <strong>Find</strong> on that string to get back to that location. Bookmarks are a more elegant solution for the problem, and they don't risk littering your document with placeholders that you could forget to delete.</p>
<h2>Highlighting</h2>
<p>In both KWrite and Kate, you can activate syntax highlighting so you can gain insight about the text you're working on. You might not consciously use highlighting in other word processors, but you've seen a form of highlighting if you've ever used an editor with automated spelling and grammar checking. The red warning line that a misspelling gets marked with in most modern word processors is a form of syntax highlighting. KWrite and Kate can notify you of both errors and successes in your writing.</p>
<p>To see spelling errors, go to the <strong>Tools</strong> menu and select <strong>Spelling</strong>. From the <strong>Spelling </strong>submenu, activate <strong>Automatic Spell Checking</strong>.</p>
<p>To get visual feedback about what you're writing in a specific format, such as <a href="https://opensource.com/article/19/9/introduction-markdown">Markdown</a>, HTML, or a programming language like <a href="https://opensource.com/article/17/10/python-101">Python</a>, go to the <strong>Tools</strong> menu and select <strong>Mode</strong>. There are lots of modes, divided between several categories. Find the format you're writing in and select it. A mode loads in a highlighting schema. You can override a mode's highlighting scheme by choosing <strong>Highlighting</strong> instead of <strong>Mode</strong>.</p>
<p><article class="media media--type-image media--view-mode-full" title="Text highlighting"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/kwrite-ui-mode.jpg" width="678" height="564" alt="Text highlighting" title="Text highlighting" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter">(Seth Kenlon, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</p>
</div>
      
  </article></p>
<p>One of my favorite features is the document overview on the right side of the window. It's essentially a very very tall thumbnail of the whole document, so you can scroll to specific regions with just one click. It might look like it's too small to be useful, but it's easier than one might think to pinpoint a section heading or an approximate area within a document and get pretty close to it with a click.</p>
<h2>What sets Kate apart</h2>
<p>With KWrite and Kate using the same underlying component, you might wonder why you'd ever need to graduate on from KWrite at all. If you do decide to try out Kate, you won't do it for the text editing. All the features that affect how you enter and interact with your text are the same between the two applications. However, Kate adds lots of features for coders.</p>
<p><article class="media media--type-image media--view-mode-full" title="Coding in Kate"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/kate-ui.jpg" width="958" height="669" alt="Coding in Kate" title="Coding in Kate" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter">(Seth Kenlon, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</p>
</div>
      
  </article></p>
<p>Kate features a side panel where you can view your filesystem or just a project directory. Notably, Kate has the concept of projects, so it can correlate one file of code to, for instance, a header file in the same directory. It also has a pop-up Terminal (just press <strong>F4</strong>) and the ability to pipe text in your document out to the terminal session.</p>
<p>There's also a session manager so you can have a unique Kate configuration for different activities.</p>
<h2>Choose your Linux text editor</h2>
<p>It's easy to overlook KWrite and Kate. They suffer, in a way, from the <em>default syndrome. </em>Because one or both of them comes along with the desktop, it's easy to think of them as the simple example text editors that developers are obligated to include. That's far from accurate, though. KWrite and Kate are paragons among K-apps. They exemplify what the KDE Framework provides, and they set the stage for an expectation of powerful, meaningful, and useful KDE applications.</p>
<p>Take a look at KWrite and Kate, and see which one is right for you.</p>
