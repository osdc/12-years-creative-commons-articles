<p>Before you start reaching for those implements of mayhem, Emacs and Vim fans, understand that this article isn't about putting the boot to your favorite editor. I'm a professed Emacs guy, but one who also likes Vim. A lot.</p>
<p>That said, I realize that Emacs and Vim aren't for everyone. It might be that the silliness of the so-called <a href="https://en.wikipedia.org/wiki/Editor_war" target="_blank">Editor war</a> has turned some people off. Or maybe they just want an editor that is less demanding and has a more modern sheen.</p>
<p>If you're looking for an alternative to Emacs or Vim, keep reading. Here are three that might interest you.</p>
<h2>Geany</h2>
<p><article class="align-center media media--type-image media--view-mode-full" title="Editing a LaTeX document with Geany"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/geany.png" width="800" height="435" alt="Editing a LaTeX document with Geany" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p><a href="https://www.geany.org/" target="_blank">Geany</a> is an old favorite from the days when I computed on older hardware running lightweight Linux distributions. Geany started out as my <a href="https://opensource.com/article/17/6/introduction-latex">LaTeX</a> editor, but quickly became the app in which I did all of my text editing.</p>
<p>Although Geany is billed as a <em>small and fast <a href="https://en.wikipedia.org/wiki/Integrated_development_environment" target="_blank">IDE</a></em> (integrated development environment), it's definitely not just a techie's tool. Geany is small and it is fast, even on older hardware or a <a href="https://opensource.com/article/17/4/linux-chromebook-gallium-os">Chromebook running Linux</a>. You can use Geany for everything from editing configuration files to maintaining a task list or journal, from writing an article or a book to doing some coding and scripting.</p>
<p><a href="http://plugins.geany.org/" target="_blank">Plugins</a> give Geany a bit of extra oomph. Those plugins expand the editor's capabilities, letting you code or work with markup languages more effectively, manipulate text, and even check your spelling.</p>
<h2>Atom</h2>
<p><article class="align-center media media--type-image media--view-mode-full" title="Editing a webpage with Atom"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/atom.png" width="800" height="569" alt="Editing a webpage with Atom" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p><a href="https://atom.io" target="_blank">Atom</a> is a new-ish kid in the text editing neighborhood. In the short time it's been on the scene, though, Atom has gained a dedicated following.</p>
<p>What makes Atom attractive is that you can customize it. If you're of a more technical bent, you can fiddle with the editor's configuration. If you aren't all that technical, Atom has <a href="https://atom.io/themes" target="_blank">a number of themes</a> you can use to change how the editor looks.</p>
<p>And don't discount Atom's thousands of <a href="https://atom.io/packages" target="_blank">packages</a>. They extend the editor in many different ways, enabling you to turn it into the text editing or development environment that's right for you. Atom isn't just for coders. It's a very good <a href="https://opensource.com/article/17/5/atom-text-editor-packages-writers">text editor for writers</a>, too.</p>
<h2>Xed</h2>
<p><article class="align-center media media--type-image media--view-mode-full" title="Writing this article in Xed"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/xed.png" width="767" height="600" alt="Writing this article in Xed" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>Maybe Atom and Geany are a bit heavy for your tastes. Maybe you want a lighter editor, something that's not bare bones but also doesn't have features you'll rarely (if ever) use. In that case, <a href="https://github.com/linuxmint/xed" target="_blank">Xed</a> might be what you're looking for.</p>
<p>If Xed looks familiar, it's a fork of the Pluma text editor for the MATE desktop environment. I've found that Xed is a bit faster and a bit more responsive than Pluma—your mileage may vary, though.</p>
<p>Although Xed isn't as rich in features as other editors, it doesn't do too badly. It has solid syntax highlighting, a better-than-average search and replace function, a spelling checker, and a tabbed interface for editing multiple files in a single window.</p>
<h2>Other editors worth exploring</h2>
<p>I'm not a KDE guy, but when I worked in that environment, <a href="https://www.kdevelop.org/" target="_blank">KDevelop</a> was my go-to editor for heavy-duty work. It's a lot like Geany in that KDevelop is powerful and flexible without a lot of bulk.</p>
<p>Although I've never really felt the love, more than a couple of people I know swear by <a href="http://brackets.io/" target="_blank">Brackets</a>. It is powerful, and I have to admit its <a href="https://registry.brackets.io/" target="_blank">extensions</a> look useful.</p>
<p>Billed as a "text editor for developers," <a href="http://notepadqq.altervista.org/s/" target="_blank">Notepadqq</a> is an editor that's reminiscent of <a href="https://opensource.com/article/16/12/notepad-text-editor" target="_blank">Notepad++</a>. It's in the early stages of development, but Notepadqq does look promising.</p>
<p><a href="https://wiki.gnome.org/Apps/Gedit" target="_blank">Gedit</a> and <a href="https://kate-editor.org/" target="_blank">Kate</a> are excellent for anyone whose text editing needs are simple. They're definitely not bare bones—they pack enough features to do heavy text editing. Both Gedit and Kate balance that by being speedy and easy to use.</p>
<p>Do you have another favorite text editor that's <em>not</em> Emacs or Vim? Feel free to share by leaving a comment.</p>
