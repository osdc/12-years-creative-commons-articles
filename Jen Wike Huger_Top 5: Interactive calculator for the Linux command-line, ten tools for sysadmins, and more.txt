<p>In this week's Top 5, we highlight why the operating system matters, getting started with Raspberry Pi, seven projects you might have missed in 2016, ten tools for sysadmins, and an interactive calculator for the Linux command-line.</p>
<p><iframe allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/bsI16PRF3kw" width="560"></iframe></p>
<h2>Top 5 articles of the week</h2>
<p>5. <a href="https://opensource.com/16/12/yearbook-why-operating-system-matters">﻿Why the operating system matters even more in 2017</a></p>
<p>Operating systems have been around for a long time, and Gordon Haff tells us why we can expect them to be around a lot longer.</p>
<p>4. <a href="https://opensource.com/article/16/12/getting-started-raspberry-pi">Getting started with Raspberry Pi</a></p>
<p>Learn how to get started with a Raspberry Pi from the Raspberry Foundation's Community Manager, Ben Nuttall.</p>
<p>3. <a href="https://opensource.com/16/12/yearbook-7-cool-little-projects">7 cool little open source projects that stood out in 2016</a></p>
<p>2016 has been a big year for open source software, so Ruth Bavousett tells us about the ones we may have missed.</p>
<p>2. <a href="https://opensource.com/16/12/yearbook-10-open-source-sysadmin-tools">10 open source tools for your sysadmin toolbox</a></p>
<p>Sysadmins don't lack for options when it comes to great open source software tools. In this article, Ben Cotton, takes a look at a few of his favorites.</p>
<p>1. <a href="https://opensource.com/article/16/12/calcshell-interactive-linux-command-line-calculator">An interactive calculator for the Linux command-line</a></p>
<p>Dave Taylor is one of the authors of the recently updated book <em><a href="http://intuitive.com/wicked/" target="_blank">Wicked Cool Shell Scripts</a></em>, and in this article he shares a Bash script for a simple, user-friendly command-line calculator.</p>
