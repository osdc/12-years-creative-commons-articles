<p>In August we published 101 articles, including 33 by our <a href="https://opensource.com/opensourcecom-team" target="_blank">community moderators</a> and 22 from new authors.</p>
<p>We ran 8 articles in our <a href="https://opensource.com/resources/diversity-open-source" target="_blank">Diversity in open source</a> series, 10 <a href="https://opensource.com/resources/linuxcon-2015" target="_blank">LinuxCon Seattle</a> interviews and reports, and 8 <a href="https://opensource.com/resources/texas-linuxfest-2015" target="_blank">Texas Linux Fest</a> interviews and reports. We also announced the <a href="https://opensource.com/open-organization/15/8/announcing-open-organization-ambassador-program" target="_blank">Open Organization Ambassadors Program</a> and the <a href="https://opensource.com/open-organization/15/8/open-org-book-club" target="_blank">Open Organization book club</a>.</p>
<p>Find out what's coming up in our <a href="https://opensource.com/life/15/9/opensourcecom-sept-preview" target="_blank">Opensource.com September preview</a>.</p>
<!--break-->
<p><img src="https://opensource.com/sites/default/files/images/life/osdc-lead-pick-6.png" alt="Top 6" title="Top 6" height="292" width="520" /></p>
<h2>August highlights</h2>
<h3>Editor's Pick 6</h3>
<p>Here are a few articles you might have missed in August:</p>
<ol><li><a href="https://opensource.com/life/15/8/unc-scientists-open-source-way-forward" target="_blank">For UNC scientists, open source is the way forward</a> by Bryan Behrenshausen</li>
<li><a href="https://opensource.com/life/15/8/raspberry-jams-bring-pi-enthusiasts-together" target="_blank">Raspberry Jams bring Pi enthusiasts together</a> by Ben Nuttall</li>
<li><a href="https://opensource.com/life/15/8/interview-marina-zhurakhinskaya-outreachy" target="_blank">Outreachy and the road toward diversity</a> by Jen Wike Huger</li>
<li><a href="https://opensource.com/life/15/8/virtual-reality-the-next-open-web-frontier" target="_blank">Virtual reality is the next open web frontier </a> by Benjamin Kerensa</li>
<li><a href="https://opensource.com/life/15/8/interview-alicia-gibb-oshwa" target="_blank">Founder of Open Source Hardware Association shares her story</a> by Don Watkins</li>
<li><a href="https://opensource.com/business/15/8/docs-or-it-didnt-happen" target="_blank">Docs or it didn't happen</a> by Mikey Ariel</li>
</ol><p><img src="https://opensource.com/sites/default/files/lead-images/osdc-lead-top-10.png" alt="top 10" title="top 10" height="292" width="520" /></p>
<h2>Top 10 articles published in August</h2>
<ol><li><a href="https://opensource.com/life/15/8/beautiful-super-thin-laptop-makes-fedora-shine" target="_blank">A beautiful, super-thin laptop that makes Fedora shine</a> by Anderson Silva</li>
<li><a href="https://opensource.com/life/15/8/patricia-torvalds-interview" target="_blank">Torvalds 2.0: Patricia Torvalds on computing, college, feminism, and increasing diversity in tech </a> by Rikki Endsley</li>
<li><a href="https://opensource.com/business/15/8/5-open-source-alternatives-trello" target="_blank">5 open source alternatives to Trello </a> by Jason Baker</li>
<li><a href="https://opensource.com/life/15/8/libreoffice-community-achievements" target="_blank">LibreOffice community achievements </a> by Jono Bacon</li>
<li><a href="https://opensource.com/life/15/8/interview-robert-booth-saltstack" target="_blank">Now your Raspberry Pi can water your lawn</a> by Ben Cotton</li>
<li><a href="https://opensource.com/life/15/8/happy-24th-birthday-linux-kernel" target="_blank">Happy 24th birthday, Linux kernel</a> by Jason Baker</li>
<li><a href="https://opensource.com/life/15/8/top-4-open-source-command-line-email-clients" target="_blank">Top 4 open source command-line email clients by Jason Baker</a> by Jason Baker</li>
<li><a href="https://opensource.com/life/15/8/compiling-kernel-in-love-linux" target="_blank">I fell in love with Linux while compiling a kernel</a> by Priyanka Nag</li>
<li><a href="https://opensource.com/business/15/4/eight-linux-file-managers" target="_blank">8 Linux file managers to try </a> by David Both</li>
<li><a href="https://opensource.com/government/15/8/india-adopts-open-source-policy" target="_blank">India adopts a comprehensive open source policy</a> by Mark Bohannon</li>
</ol><h2>Send us your story ideas</h2>
<p><a href="https://opensource.com/how-submit-article" target="blank">Send us your story ideas</a>, and see our <a href="https://opensource.com/resources/editorial-calendar" target="blank">2015 editorial calendar and columns</a> for more writing opportunities. We've rounded up <a href="https://opensource.com/life/15/7/7-big-reasons-contribute-opensourcecom" target="_blank">7 big reasons to contribute to Opensource.com</a>. Got questions? Email us at <a href="mailto:open@opensource.com" target="_blank">open@opensource.com</a>.</p>
<p>Are you organizing a 2016 open source conference or event? Be sure to add it to our <a href="https://opensource.com/resources/conferences-and-events-monthly" target="_blank">community calendar</a>.</p>
