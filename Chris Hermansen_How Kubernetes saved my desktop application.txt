<p>Recently, fellow Opensource.com scribe James Farrell wrote a wonderful article entitled <em><a href="https://opensource.com/article/19/9/ansible-documentation-kids-laptops" target="_self">How Ansible brought peace to my home</a></em>. In addition to the great article, I really liked the title, one of those unexpected phrases that I’m sure brought a smile to many faces.</p>
<p>I recently had a weird but positive experience of my own that begs a similar sort of unexpected label. I’ve been grappling with a difficult problem that arose when upgrading some server and networking infrastructure that broke a Java application I’ve been supporting since the early 2000s. Strangely enough, I found the solution in what appears to be a very informative and excellent article on Kubernetes, of all things.</p>
<p>Without further ado, here is my problem:</p>
<p><article class="media media--type-image media--view-mode-full"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/java_art_2019-03_1.png" width="556" height="205" /></div>
      
  </article></p>
<p>I’m guessing that most readers will look at that message and think things like, "I hope there’s more info in the log file," or "I’m really glad I’ve never received a message like that."</p>
<p>Unfortunately, there isn’t a lot of info in the log file, just the same message, in fact. In an effort to debug this, I did three things:</p>
<ol><li>
<p>I searched online for the message. Interestingly, or perhaps ominously, there were only 200 or so hits on this string, <a href="https://dzone.com/articles/troubleshooting-javaxnetsslsslhandshakeexception-r" target="_blank">one of which suggested</a> <a href="https://dzone.com/articles/troubleshooting-javaxnetsslsslhandshakeexception-r">turn</a><a href="https://dzone.com/articles/troubleshooting-javaxnetsslsslhandshakeexception-r">ing</a> <a href="https://dzone.com/articles/troubleshooting-javaxnetsslsslhandshakeexception-r">on more debugging output</a>, which involved adding the setting</p>
<pre><strong>-Djavax.net.debug=ssl:handshake:verbose</strong></pre><p>	to the <strong>java</strong> command running the application.</p></li>
<li>
<p>I tried that suggestion, which resulted in a lot of output (good), most of which only vaguely made sense to me as I’m no kind of expert in the underlying bits of stuff like SSL. But one thing I did notice is that there was no information regarding a response from the server in the midst of all of that output;</p>
</li>
<li>
<p>So I searched some more.</p>
</li>
</ol><p>Another interesting part of this problem is that the code ran fine when executed by the Java command bundled in the OpenJDK, but failed with this error when using a customized runtime <a href="https://opensource.com/article/19/4/java-se-11-removing-jnlp" target="_self">created from the same OpenJDK in this way</a>. So the relatively modest number of apparently similar problems turned up from search #1 above were actually not all that relevant since they all seemed to be dealing mostly with bad SSL certificates on the server in conjunction with the PostgreSQL JDBC’s ability to check the server’s credentials.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on Kubernetes</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/topics/containers/what-is-kubernetes?intcmp=7016000000127cYAAQ">What is Kubernetes?</a></li>
<li><a href="https://www.redhat.com/en/engage/kubernetes-containers-storage-s-201911201051?intcmp=7016000000127cYAAQ">eBook: Storage Patterns for Kubernetes</a></li>
<li><a href="https://www.redhat.com/en/engage/openshift-storage-testdrive-20170718?intcmp=7016000000127cYAAQ">Test drive OpenShift hands-on</a></li>
<li><a href="https://opensource.com/kubernetes-dump-truck?intcmp=7016000000127cYAAQ">eBook: Getting started with Kubernetes</a></li>
<li><a href="https://www.redhat.com/en/resources/managing-containers-kubernetes-openshift-technology-detail?intcmp=7016000000127cYAAQ">An introduction to enterprise Kubernetes</a></li>
<li><a href="https://enterprisersproject.com/article/2017/10/how-explain-kubernetes-plain-english?intcmp=7016000000127cYAAQ">How to explain Kubernetes in plain terms</a></li>
<li><a href="https://opensource.com/downloads/kubernetes-raspberry-pi?intcmp=7016000000127cYAAQ">eBook: Running Kubernetes on your Raspberry Pi homelab</a></li>
<li><a href="https://opensource.com/downloads/kubernetes-cheat-sheet?intcmp=7016000000127cYAAQ">Kubernetes cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/kubernetes-sysadmin?intcmp=7016000000127cYAAQ">eBook: A guide to Kubernetes for SREs and sysadmins</a></li>
<li><a href="https://opensource.com/tags/kubernetes?intcmp=7016000000127cYAAQ">Latest Kubernetes articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>I should also mention that it took me quite some time to realize that the problem was introduced by using the custom Java runtime, as I managed to check many other possibilities along the way (and indeed, I did fix a few minor bugs while I was at it). My efforts included things like getting the latest OpenJDK, checking and re-checking all the URLs in case one had a typo, and so forth.</p>
<p>As often happens, after putting the problem aside for a few hours, an idea occurred to me—perhaps I was missing some module in the customized Java runtime. While I didn’t receive any errors directly suggesting that problem, the observable fact that the standard OpenJDK environment worked while the custom one failed seemed to hint at that possibility. I took a quick look in the <strong>jmods/ </strong>folder in the OpenJDK installation, but there are some 70 modules there and nothing jumped out at me.</p>
<p>But again, what seemed odd was, with debugging turned on (see #1 above), there was no indication of what the server would accept, just what the client mostly couldn’t offer, many lines like this:</p>
<pre><code class="language-java">Ignoring unavailable cipher suite: TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA</code></pre><p>So I was at least thinking by this time that maybe what was missing was the module that offered those kinds of cipher suites. So I started searching with strings like "jdbc crypto," and in the midst of that, the most unlikely article showed up: <a href="https://juan-medina.com/2020/01/22/optimizing-k8s-sv-02/" target="_blank">Optimizing Kubernetes Services—Part 2: Spring Web</a>, written by <a href="https://juan-medina.com/" target="_blank">Juan Medina</a>. Midway down the article, I spotted the following:</p>
<p><article class="align-center media media--type-image media--view-mode-full"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/java_art_2019-03_2.png" width="675" height="557" /></div>
      
  </article></p>
<p>Huh! Imagine that, his script is creating a custom Java runtime, just like mine. But he says he needs to add in manually the module <strong>jdk.crypto.ec</strong> in order to connect via SSL to his PostgreSQL environment. Sure sounded familiar.</p>
<p>In fact, this was the solution to my problem as well; the missing module was <strong>jdk.crypto.ec</strong>, and I was able to add it into my build as follows:</p>
<pre><code class="language-java">DEPS=$(JAVA_HOME)/bin/jdeps --print-module-deps $(TEST_HOME)/MyApp.jar \ $(TEST_HOME)/lib/*.jar,jdk.crypto.ec;
$(JAVA_HOME)/bin/jlink --module-path $(W64_JAVA_HOME)/jmods
–no-header-files --no-man-pages --compress=2 –strip-debug
–add-modules $$DEPS --output $(TEST_HOME)/java-runtime</code></pre><p>(I’m cross-building a Windows Java runtime here; <a href="https://opensource.com/article/19/4/java-se-11-removing-jnlp" target="_self">see my earlier article on this topic</a> for more info).</p>
<h2>In conclusion</h2>
<p>This was a big bullet to dodge for someone like me who doesn’t know much about crypto. And once again, open source, and more than that, the willingness to share that goes with open source, is huge. And wow, using Kubernetes to fix a Java desktop application, that’s pretty huge too! Thanks again to Juan Medina!</p>
