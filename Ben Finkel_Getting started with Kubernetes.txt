<p>One of today's most promising emerging technologies is paring containers with cluster management software such as <a href="https://docs.docker.com/engine/swarm/" target="_blank">Docker Swarm</a>, <a href="http://mesos.apache.org/" target="_blank">Apache Mesos</a>, and the popular <a href="https://kubernetes.io/" target="_blank">Kubernetes</a>. Kubernetes allows you to create a portable and scalable application deployment that can be scheduled, managed, and maintained easily. As an open source project, Kubernetes is continually being updated and improved, and it leads the way among container cluster management software.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Linux Containers</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/topics/containers/whats-a-linux-container?utm_campaign=containers&amp;intcmp=70160000000h1s6AAA">What are Linux containers?</a></li>
<li><a href="https://developers.redhat.com/blog/2016/01/13/a-practical-introduction-to-docker-container-terminology/?utm_campaign=containers&amp;intcmp=70160000000h1s6AAA">An introduction to container terminology</a></li>
<li><a href="https://opensource.com/downloads/containers-primer?utm_campaign=containers&amp;intcmp=70160000000h1s6AAA">Download: Containers Primer</a></li>
<li><a href="https://www.redhat.com/en/resources/oreilly-kubernetes-operators-automation-ebook?utm_campaign=containers&amp;intcmp=70160000000h1s6AAA">Kubernetes Operators: Automating the container orchestration platform</a></li>
<li><a href="https://www.redhat.com/en/engage/kubernetes-containers-architecture-s-201910240918?utm_campaign=containers&amp;intcmp=70160000000h1s6AAA">eBook: Kubernetes patterns for designing cloud-native apps</a></li>
<li><a href="https://www.redhat.com/en/topics/containers/what-is-kubernetes?utm_campaign=containers&amp;intcmp=70160000000h1s6AAA">What is Kubernetes?</a></li>
</ul></div>
</div>
</div>
</div>
<p>Kubernetes uses various architectural components to describe the deployments it manages.</p>
<ul><li><a href="https://kubernetes.io/docs/concepts/workloads/pods/pod/" target="_blank">Pods</a> are a group of one or more containers that share network and storage. The containers in a pod are considered "tightly coupled," and they are managed and deployed as a single unit. If an application were deployed in a more traditional model, the contents of the pod would always be deployed together on the same machine.</li>
</ul><ul><li><a href="https://kubernetes.io/docs/concepts/architecture/nodes/" target="_blank">Nodes</a> represents a worker machine in a Kubernetes cluster. The worker machine can be either physical or (more likely) virtual. A node contains all the required services to host a pod.</li>
</ul><ul><li>A cluster always requires a <a href="https://kubernetes.io/docs/concepts/overview/components/" target="_blank">master</a> node, where the controlling services (known as the master components) are installed. These services can be distributed on a single machine or across multiple machines for redundancy. They control communications, workload, and scheduling.</li>
</ul><ul><li><a href="https://kubernetes.io/docs/concepts/workloads/controllers/deployment/" target="_blank">Deployments</a> are a way to declaratively set a state for your pods or ReplicaSets (groups of pods to be deployed together). Deployments use a "desired state" format to describe how the deployment should look, and Kubernetes handles the actual deployment tasks. Deployments can be updated, rolled back, scaled, and paused at will.</li>
</ul><p>The following tutorial will explain the basics of creating a cluster, deploying an app, and creating a proxy, then send you on your way to learning even more about Kubernetes.</p>
<h2>Create a cluster</h2>
<p>Begin by using the Kubernetes-provided <a href="https://kubernetes.io/docs/tutorials/kubernetes-basics/" target="_blank">tutorial</a> to create a cluster and deploy an app. This cluster will consist of a master and one or more nodes. In the first scenario, you'll create a cluster using a utility called "Minkube," which creates and runs a cluster on a local machine. Minikube is great for testing and development. You will also use the <b>kubectl</b> command, which is installed as part of the Kubernetes API.</p>
<p>In the interactive terminal, start the Minikube software with the command:</p>
<pre><code class="language-bash">minikube start</code></pre><p>View the cluster information with the command:</p>
<pre><code class="language-bash">kubectl cluster-info</code></pre><p>List the available nodes with the command:</p>
<pre><code class="language-bash">kubectl get nodes</code></pre><p>
<article class="align-center media media--type-image media--view-mode-full" title="Output of kubectl get nodes"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/kubernetes_1_kubectl_get_nodes.png" width="800" height="429" alt="Output of kubectl get nodes" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>The screenshot above shows the output from these commands. Note the only available node is host01, which is operating as the master (as seen in the cluster-info output).</p>
<h2>Deploy an app</h2>
<p>In the <a href="https://kubernetes.io/docs/tutorials/kubernetes-basics/deploy-interactive/" target="_blank">next step</a> in the interactive tutorial, you'll deploy a containerized application to your cluster with a deployment configuration. This describes how to create instances of your app, and the master will schedule those instances onto nodes in the cluster.</p>
<p>In the interactive terminal, create a new deployment with the <b>kubectl run</b> command:</p>
<pre><code class="language-bash">kubectl run kubernetes-bootcamp \
--image=docker.io/jocatalin/kubernetes-bootcamp:v1 --port=8080</code></pre><p>This creates a new deployment with the name <b>kubernetes-bootcamp</b> from a public repository at docker.io and overrides the default port to 8080.</p>
<p>View the deployment with the command:</p>
<pre><code class="language-bash">kubectl get deployments</code></pre><p>
<article class="align-center media media--type-image media--view-mode-full" title="Output of kubectl get deployments"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/kubernetes_2_kubectl_get_deployments.png" width="800" height="208" alt="Output of kubectl get deployments" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>The deployment is currently on a single node (host01), because only that node is available.</p>
<h2>Create a proxy</h2>
<p>In the <a href="https://kubernetes.io/docs/tutorials/kubernetes-basics/deploy-interactive/" target="_blank">third part</a> of the tutorial, you will create a proxy into your deployed app. A pod runs on an isolated private network that cannot be accessed from outside. The <b>kubectl</b> command uses an API to communicate with the application, and a proxy is needed to expose the application for use by other services.</p>
<p>Open a new terminal window and start the proxy server with the command:</p>
<pre><code class="language-bash">kubectl proxy</code></pre><p>This creates a connection between your cluster and the virtual terminal window. Notice it's running on port 8001 on the local host.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Output of curl command"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/kubernetes_3_kubectl_proxy.png" width="800" height="68" alt="Output of curl command" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>Return to the first terminal window and run a <b>curl</b> command to see this in action:</p>
<pre><code class="language-bash">curl http://localhost:8001/version</code></pre><p>
<article class="align-center media media--type-image media--view-mode-full" title="Output of curl command"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/kubernetes_4_curl.png" width="800" height="276" alt="Output of curl command" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>The JSON output, shown in the screenshot above, displays the version information from the cluster itself.</p>
<p>Follow the online tutorial to find the internal name of the deployed pod and then query that pod directly. You can also get a detailed output of your pod by using the command:</p>
<pre><code class="language-bash">kubectl describe pods</code></pre><p>This output includes very important information, like the pod name, local IP address, state, and restart count.</p>
<h2>Moving forward</h2>
<p>Kubernetes is a full-fledged deployment, scheduling, and scaling manager and is capable of deciding all of the myriad details of how to deploy an app on your cluster. The few commands explored here are just the beginning of interacting with and understanding a Kubernetes deployment. The crucial takeaway is how fast and easy it is to do and how few details you need to provide to use Kubernetes.</p>
<p>Follow the online <a href="https://kubernetes.io/docs/tutorials/kubernetes-basics/explore-intro/" target="_blank">interactive tutorial</a> to learn more about how Kubernetes works and all that you can do with it.</p>
<ul></ul>