<p>The KDE project doesn't just provide a famous desktop, it generates a lot of software, from tools for <a href="https://opensource.com/article/21/12/kdenlive-linux-creative-app" target="_blank">video editng</a>, <a href="https://opensource.com/life/16/5/how-use-digikam-photo-management">photography</a>, and <a href="https://opensource.com/article/21/12/krita-digital-paint">illustration</a>, to graphing calculators, email, and office work. Those are all productivity tools, but KDE is also good for relaxing. It has games and media players, and the music player I use is known simply as <a href="https://juk.kde.org/" target="_blank">Juk</a>.</p>
<h2>Install Juk on Linux</h2>
<p>As long as your Linux distribution offers KDE packages, you can install Juk using your package manager.</p>
<p>On Fedora, Mageia, and similar:</p>
<pre><code class="language-bash">$ sudo dnf install juk</code></pre><p>On Elementary, Linux Mint, and other Debian-based distributions:</p>
<pre><code class="language-bash">$ sudo apt install juk</code></pre><h2>Using Juk</h2>
<p>When you first launch Juk, you're asked what directory or directories you want to use as the source of your music collection. Select the folders you want Juk to look in for music, and optionally exclude any subdirectories you want Juk to ignore.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Adding music to Juk"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/juk-import.jpg" width="455" height="355" alt="Adding music to Juk" title="Adding music to Juk" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter">(Seth Kenlon, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</p>
</div>
      
  </article></p>
<p>Depending on how much music you have, it may take a few moments for Juk to parse the filesystem and metadata. Once it has, the usual Juk window appears with all of your music in a central collection called <strong>Collection List</strong>.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Juk music player"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/juk-player.jpg" width="774" height="508" alt="Juk music player" title="Juk music player" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter">(Seth Kenlon, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</p>
</div>
      
  </article></p>
<p>In the top search bar, you can search for any string in any field. Double-click on the song to begin playback.</p>
<p>Juk defaults to continuous play, meaning that after it's finished playing your selection, it proceeds to the next song in the Collection List, even if you've filtered your list down to just one song. This keeps the music going no matter what, like a true Jukebox.</p>
<p>Personally, I prefer to listen to complete albums, with just enough of a break in between to meander into the kitchen to top off my coffee, so I use playlists.</p>
<h2>Playlists in Juk</h2>
<p>A playlist in Juk can contain anything in your Collection List. Juk plays through the playlist once, and then stops.</p>
<p>To create a playlist, right-click in the left playlist column and select <strong>New</strong> from the contextual menu that appears. You can create an empty playlist or a playlist from a specific folder on your hard drive.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Creating a new playlist"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/juk-playlist-new.jpg" width="528" height="401" alt="Creating a new playlist" title="Creating a new playlist" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter">(Seth Kenlon, CC BY-SA 4.0)</p>
</div>
      
  </article></p>
<h2>Automatic playlists</h2>
<p>You can also create a special playlist bound to a specific search, which is useful for artists whose music you frequently purchase and add to your collection. With a search playlist, their new music gets added to their playlist without your manual intervention.</p>
<h2>Tagging</h2>
<p>Let's face it, music players are pretty easy to come by, and they mostly all do the same thing. Juk, however, adds an important feature. It gives you the ability to modify metadata tags.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Metadata editor"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/juk-tagger.png" width="585" height="311" alt="Metadata editor" title="Metadata editor" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter">(Seth Kenlon, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</p>
</div>
      
  </article></p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Linux resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux commands cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Free online course: RHEL Technical Overview</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux networking cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">SELinux cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux common commands cheat sheet</a></li>
<li><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">What are Linux containers?</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Our latest Linux articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>A lot of music is tagged inconsistently, incompletely, or not at all, so it's a luxury to be able to correct them as you discover them when they come up in the play queue.</p>
<p>There's also a tag guesser feature, which tries to parse filenames and tag songs accordingly.</p>
<h2>The obvious choice</h2>
<p>Juk is the easy and obvious choice when you're looking for a music player on KDE. There are, of course, lots of other very good choices, as Opensource.com's resident audiophile <a href="https://opensource.com/users/clhermansen">Chris Hermansen</a> details in his <a href="https://opensource.com/article/17/1/open-source-music-players">3 open source music players</a> article, but sometimes you just want to use whatever comes easiest to you. Juk is both intuitive to use and excellent at what it does. How lucky it is that it's the default player on the KDE Plasma Desktop.</p>
