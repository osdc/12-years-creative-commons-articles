<p>The visual components on opensource.com are an important element to the look and feel of our content. The images help set the tone for the site. The imagery embodies several qualities such as motivational, editorial, authoritative (but not authoritarian), human, and optimism.</p>
<p>Without our imagery, the content on the site would be plain and unsightly. We'd like to highlight some of the images from 2010 and give you a chance to pick your favorite.</p>
<!--break--><p><br /><br />Learn <a title="how you can contribute an image" href="https://opensource.com/how-submit-image" target="_blank">how you can contribute an image</a> to the community and see the best of the <a title="Open*Business images on Flickr" href="http://www.flickr.com/photos/opensourceway/sets/72157625520195861/" target="_blank">Open*Business images on Flickr</a>.</p>
<h2>Peloton (bicycles)</h2>
<p><img src="https://opensource.com/sites/default/files/lead-images/BUSINESS_peloton.png" alt="Pelonton (bicycles)" title="Pelonton (bicycles)" class="mceItem" width="520" height="292" /></p>
<p> </p>
<h2>Management innovation (blue circles)</h2>
<p><img src="https://opensource.com/sites/default/files/lead-images/BUSINESS_networks.png" alt="Management innovation (blue circles)" title="Management innovation (blue circles)" class="mceItem" width="520" height="292" /></p>
<h2>Social media overhyped (silly bandz)</h2>
<p><img src="https://opensource.com/sites/default/files/lead-images/BIZ_SillyBandz.png" alt="Social media overhyped (silly bandz)" title="Social media overhyped (silly bandz)" class="mceItem" width="520" height="292" /></p>
<h2>Crowdsourcing (blue/orange)</h2>
<p><img src="https://opensource.com/sites/default/files/lead-images/BUSINESS_crowdvsopen.png" alt="Crowdsourcing (blue/orange)" title="Crowdsourcing (blue/orange)" class="mceItem" width="520" height="292" /></p>
<h2>Open for business (open sign)</h2>
<p><img src="https://opensource.com/sites/default/files/lead-images/BUSINESS_openseries.png" alt="Open for business (open sign)" title="Open for business (open sign)" class="mceItem" width="520" height="292" /></p>
