<p>When operating on the Linux command line, it is easy to get disoriented, which can have disastrous consequences. I once issued a remove command before realizing that I'd moved the boot directory of my computer. I learned to use the <code>pwd</code> command to know exactly which part of the file system I was in (and these days, there are command projects, like <a href="https://www.redhat.com/sysadmin/recover-file-deletion-linux" target="_blank"><code>trashy</code> and <code>trash-cli</code></a>, that serve as intermediates when removing files).</p>
<p>When I was new to Linux, I had a cheat sheet that hung over my desk to help me remember those commands as I managed my Linux servers. It was called the <em>101 commands for Linux</em> cheat sheet. As I became more familiar with these commands, I became more proficient with server administration.</p>
<p>Here are 12 Linux commands I find most useful.</p>
<h2>1. Print working directory (pwd)</h2>
<p>The <code>pwd</code> command prints your working directory. In other words, it outputs the path of the directory you are currently working in. There are two options: <code>--logical</code> to display your location with any symlinks and <code>--physical</code> to display your location after resolving any symlinks.</p>
<h2>2. Make directory (mkdir)</h2>
<p>Making directories is easy with the <code>mkdir</code> command. The following command creates a directory called <code>example</code> unless <code>example</code> already exists:</p>
<pre>
<code class="language-bash">$ mkdir example</code></pre><p>You can make directories within directories:</p>
<pre>
<code class="language-bash">$ mkdir -p example/one/two</code></pre><p>If directories <code>example</code> and <code>one</code> already exist, only directory <code>two</code> is created. If none of them exist, then three nested directories are created.</p>
<h2>3. List (ls)</h2>
<p>Coming from MS-DOS, I was used to listing files with the <code>dir</code> command. I don't recall working on Linux at the time, although today, <code>dir</code> is in the GNU Core Utilities package. Most people use the <code>ls</code> command to display the files, along with all their properties, are in a directory. The <code>ls</code> command has many options, including <code>-l</code> to view a long listing of files, displaying the file owner and permissions.</p>
<h2>4. Change directory (cd)</h2>
<p>It is often necessary to change directories. That's the <code>cd</code> command's function. For instance, this example takes you from your home directory into the <code>Documents</code> directory:</p>
<pre>
<code class="language-bash">$ cd Documents</code></pre><p>You can quickly change to your home directory with <code>cd ~</code> or just <code>cd</code> on most systems. You can use <code>cd ..</code> to move up a level.</p>
<p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">More Linux resources</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux commands cheat sheet">Linux commands cheat sheet</a></div>
              <div class="field__item"><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Advanced Linux commands cheat sheet">Advanced Linux commands cheat sheet</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Free online course: RHEL technical overview">Free online course: RHEL technical overview</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux networking cheat sheet">Linux networking cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="SELinux cheat sheet">SELinux cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux common commands cheat sheet">Linux common commands cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="What are Linux containers?">What are Linux containers?</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Register for your free Red Hat account">Register for your free Red Hat account</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Our latest Linux articles">Our latest Linux articles</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<h2>5. Remove a file (rm)</h2>
<p>Removing files is inherently dangerous. Traditionally, the Linux terminal has no Trash or Bin like the desktop does, so many terminal users have the bad habit of permanently removing data they believe they no longer need. There's no "un-remove" command, though, so this habit can be problematic should you accidentally delete a directory containing important data.</p>
<p>A Linux system provides <code>rm</code> and <code>shred</code> for data removal. To delete file <code>example.txt</code>, type the following:</p>
<pre>
<code class="language-bash">$ rm example.txt</code></pre><p>However, it's much safer to install a trash command, such as <a href="https://gitlab.com/trashy/trashy" target="_blank"><code>trashy</code></a> or <a href="https://github.com/andreafrancia/trash-cli" target="_blank"><code>trash-cli</code></a>. Then you can send files to a staging area before deleting them forever:</p>
<pre>
<code class="language-bash">$ trash example.txt</code></pre><h2>6. Copy a file (cp)</h2>
<p>Copy files with the <code>cp</code> command. The syntax is copy <em>from-here</em> <em>to-there</em>. Here's an example:</p>
<pre>
<code class="language-bash">$ cp file1.txt newfile1.txt</code></pre><p>You can copy entire directories, too:</p>
<pre>
<code class="language-bash">$ cp -r dir1 newdirectory</code></pre><h2>7. Move and rename a file (mv)</h2>
<p>Renaming and moving a file is functionally the same process. When you move a file, you take a file from one directory and put it into a new one. When renaming a file, you take a file from one directory and put it back into the same directory or a different directory, but with a new name. Either way, you use the <code>mv</code> command:</p>
<pre>
<code class="language-bash">$ mv file1.txt file_001.txt</code></pre><h2>8. Create an empty file (touch)</h2>
<p>Easily create an empty file with the <code>touch</code> command:</p>
<pre>
<code class="language-bash">$ touch one.txt

$ touch two.txt

$ touch three.md</code></pre><h2>9. Change permissions (chmod)</h2>
<p>Change the permissions of a file with the <code>chmod</code> command. One of the most common uses of <code>chmod</code> is making a file executable:</p>
<pre>
<code class="language-bash">$ chmod +x myfile</code></pre><p>This example is how you give a file permission to be executed as a command. This is particularly handy for scripts. Try this simple exercise:</p>
<pre>
<code class="language-bash">$ echo 'echo Hello $USER' &gt; hello.sh

$ chmod +x hello.sh

$ ./hello.sh
Hello, Don</code></pre><h2>10. Escalate privileges (sudo)</h2>
<p>While administering your system, it may be necessary to act as the super user (also called root). This is where the <code>sudo</code> (or <em>super user do</em>) command comes in. Assuming you're trying to do something that your computer alerts you that only an administrator (or root) user can do, just preface it with the command <code>sudo</code>:</p>
<pre>
<code class="language-bash">$ touch /etc/os-release &amp;&amp; echo "Success"
touch: cannot touch '/etc/os-release': Permission denied

$ sudo touch /etc/os-release &amp;&amp; echo "Success"
Success</code></pre><h2>11. Shut down (poweroff)</h2>
<p>The <code>poweroff</code> command does exactly what it sounds like: it powers your computer down. It requires <code>sudo</code> to succeed.</p>
<p>There are actually many ways to shut down your computer and some variations on the process. For instance, the <code>shutdown</code> command allows you to power down your computer after an arbitrary amount of time, such as 60 seconds:</p>
<pre>
<code class="language-bash">$ sudo shutdown -h 60</code></pre><p>Or immediately:</p>
<pre>
<code class="language-bash">$ sudo shutdown -h now</code></pre><p>You can also restart your computer with <code>sudo shutdown -r now</code> or just <code>reboot</code>.</p>
<h2>12. Read the manual (man)</h2>
<p>The <code>man</code> command could be the most important command of all. It gets you to the documentation for each of the commands on your Linux system. For instance, to read more about <code>mkdir</code>:</p>
<pre>
<code class="language-bash">$ man mkdir</code></pre><p>A related command is <code>info</code>, which provides a different set of manuals (as long as they're available) usually written more verbosely than the often terse man pages.</p>
<h2>What's your favorite Linux command?</h2>
<p>There are many more commands on a Linux system—hundreds! What's your favorite command, the one you find yourself using time and time again?</p>
