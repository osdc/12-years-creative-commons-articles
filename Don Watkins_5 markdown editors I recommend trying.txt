<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Programming and development</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ&amp;src=programming_resource_menu4">Red Hat Developers Blog</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Programming cheat sheets</a></li>
<li><a href="https://www.redhat.com/en/services/training/learning-subscription?intcmp=7016000000127cYAAQ">Try for free: Red Hat Learning Subscription</a></li>
<li><a href="https://opensource.com/downloads/bash-programming-guide?intcmp=7016000000127cYAAQ">eBook: An introduction to programming with Bash</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/bash-shell-cheat-sheet?intcmp=7016000000127cYAAQ">Bash Shell Scripting Cheat Sheet</a></li>
<li><a href="https://developers.redhat.com/e-books/modernizing-enterprise-java?intcmp=7016000000127cYAAQ">eBook: Modernizing Enterprise Java</a></li>
</ul></div>
</div>
</div>
</div>
<p>You can use markdown for anything—formatting websites, authoring books, and writing technical documentation are just some of its uses. I love how easy it is to create rich documents. Everyone has their favorite markdown editor. I have used several on my markdown journey. Here are five markdown editors I have considered. </p>
<p><meta charset="utf-8" /></p>
<ol><li><a href="https://abricotine.brrd.fr/" target="_blank">Abricotine</a> is an open source editor released under the <a href="https://github.com/brrd/abricotine/blob/develop/LICENSE" target="_blank">GPL v.3.0</a>. You can enter formatting by hand or use the menu to insert <a href="https://guides.github.com/features/mastering-markdown/" target="_blank">Github flavored Markdown</a>. Abricotine allows you to preview text elements like headers, images, math, embedded videos, and to-do lists as you type. The editor is limited to exporting documents as HTML. You can use Abricotine on Linux, macOS, and Windows.<br /><br /><article class="align-center media media--type-image media--view-mode-full" title="Abricontine"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/1_abricontine.png" width="848" height="673" alt="Abricontine" title="Abricontine" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Seth Kenlon, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></li>
<li><a href="https://marktext.app/" target="_blank">MarkText</a> is a simple markdown editor. It has many features, and it does a good job of handling documents formatted in markdown. MarkText also supports Github flavored Markdown, which allows you to add tables and blocks of code with syntax highlighting. It supports real-time preview and has a simple interface. MarkText is licensed under <a href="https://github.com/marktext/marktext/blob/develop/LICENSE" target="_blank">MIT</a>. It supports output in HTML and PDF. MarkText is available on Linux, macOS, and Windows.<br /><br /><article class="align-center media media--type-image media--view-mode-full" title="MarkText"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/2_marktext.png" width="835" height="501" alt="MarkText" title="MarkText" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Seth Kenlon, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></li>
<li><a href="https://wereturtle.github.io/ghostwriter/" target="_blank">Ghostwriter</a> Is a markdown editor for Linux and Windows. According to users of its website: "Enjoy a distraction-free writing experience, including a full-screen mode and a clean interface. With markdown, you can write now and format later." It has built-in light and dark themes that come by default, or you can write your own. You can preview documents as live HTML that you can copy and paste directly into a blog or export into another format. Ghostwriter is released under the <a href="https://github.com/wereturtle/ghostwriter/blob/master/COPYING" target="_blank">GPL v.3.0</a>.<br /><br /><article class="align-center media media--type-image media--view-mode-full" title="Ghostwriter"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/3_ghostwriter.png" width="1375" height="837" alt="Ghostwriter" title="Ghostwriter" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Seth Kenlon, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></li>
<li><a href="https://atom.io/" target="_blank">Atom</a> is billed as a hackable text editor for the twenty-first century. It can function as a markdown editor too. It runs on Linux, Windows, and macOS and is released with an <a href="https://github.com/atom/atom/blob/master/LICENSE.md" target="_blank">MIT</a> license. It supports Github flavored Markdown, and <strong>Ctrl</strong>+<strong>Shift</strong>+<strong>M</strong> opens a preview panel so you can easily see the HTML preview. You can get started easily by creating a file and saving it with the <code>.md</code> file extension. This tells Atom that it is a markdown file. Atom automatically applies the right packages and syntax highlighting.<br /><br /><article class="align-center media media--type-image media--view-mode-full" title="Atom"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/4_atom.png" width="1013" height="685" alt="Atom" title="Atom" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Seth Kenlon, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></li>
<li><a href="https://vscodium.com/" target="_blank">VSCodium</a> is the free open source code release of Microsoft's VSCode editor without the telemetry built into the stock Microsoft product. It is released with an <a href="https://github.com/VSCodium/vscodium/blob/master/LICENSE" target="_blank">MIT</a> license and provides all the functionality of VSCode without the proprietary features. In addition to its other features, VSCodium can function as a markdown editor. Create a new file, click <strong>Select a Language</strong>, choose <em>Markdown</em> and begin writing your code. Easily preview the text by pressing <strong>Ctrl</strong>-<strong>Shift</strong>+<strong>V</strong> and then toggle back to the editor. You can also easily extend the markdown editor by adding an extension. This is my favorite is <a href="https://github.com/zaaack/vscode-markdown-editor" target="_blank">markdown editor</a> and it has an <a href="https://github.com/zaaack/vscode-markdown-editor/blob/master/LICENSE" target="_blank">MIT</a> license.<br /><br /><article class="align-center media media--type-image media--view-mode-full" title="VSCodium"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/5_vscodium.png" width="875" height="617" alt="VSCodium" title="VSCodium" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Seth Kenlon, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></li>
</ol><p>What's your favorite markdown editor? Let us know in the comments.</p>
