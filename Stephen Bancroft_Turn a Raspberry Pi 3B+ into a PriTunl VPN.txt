<p><a href="https://pritunl.com/" target="_blank">PriTunl</a> is a fantastic VPN terminator solution that's perfect for small businesses and individuals who want a quick and simple way to access their network privately. It's open source, and the basic free version is more than enough to get you started and cover most simple use cases. There is also a paid enterprise version with advanced features like Active Directory integration.</p>
<h2 id="special-considerations-with-pritunl-on-raspberry-pi-3b">Special considerations on Raspberry Pi 3B+</h2>
<p>PriTunl is generally simple to install, but this project—turning a Raspberry Pi 3B+ into a PriTunl VPN appliance—adds some complexity. For one thing, PriTunl is supplied only as AMD64 and i386 binaries, but the 3B+ uses ARM architecture. This means you must compile your own binaries from source. That's nothing to be afraid of; it can be as simple as copying and pasting a few commands and watching the terminal for a short while.</p>
<p>Another problem: PriTunl seems to require 64-bit architecture. I found this out when I got errors when I tried to compile PriTunl on my Raspberry Pi's 32-bit operating system. Fortunately, Ubuntu's beta version of 18.04 for ARM64 boots on the Raspberry Pi 3B+.</p>
<p>Also, the Raspberry Pi 3B+ uses a different bootloader from other Raspberry Pi models. This required a complicated set of steps to install and update the necessary files to get a Raspberry Pi 3B+ to boot.</p>
<h2 id="installing-pritunl">Installing PriTunl</h2>
<p>You can overcome these problems by installing a 64-bit operating system on the Raspberry Pi 3B+ before installing PriTunl. I'll assume you have basic knowledge of how to get around the Linux command line and a Raspberry Pi.</p>
<p>Start by opening a terminal and downloading the Ubuntu 18.04 ARM64 beta release by entering:</p>
<pre><code class="language-bash">$ wget http://cdimage.ubuntu.com/releases/18.04/beta/ubuntu-18.04-beta-preinstalled-server-arm64+raspi3.img.xz</code></pre><p>Unpack the download:</p>
<pre><code class="language-bash">$ xz -d ubuntu-18.04-beta-preinstalled-server-arm64+raspi3.xz</code></pre><p>Insert the SD card you'll use with your Raspberry Pi into your desktop or laptop computer. Your computer will assign the SD card a drive letter—something like <strong>/dev/sda</strong> or <strong>/dev/sdb</strong>. Enter the <strong>dmesg</strong> command and examine the last lines of the output to find out the card's drive assignment.</p>
<p><strong><em>Be VERY CAREFUL with the next step! I can't stress that enough; if you get the drive assignment wrong, you could destroy your system.</em></strong></p>
<p>Write the image to your SD card with the following command, changing <strong>&lt;DRIVE&gt;</strong> to your SD card's drive assignment (obtained in the previous step):</p>
<pre><code class="language-bash">$ dd if=ubuntu-18.04-beta-preinstalled-server-arm64+raspi3.img of=&lt;DRIVE&gt; bs=8M</code></pre><p>After it finishes, insert the SD card into your Pi and power it up. Make sure the Pi is connected to your network, then log in with username/password combination ubuntu/ubuntu.</p>
<p>Enter the following commands on your Pi to install a few things to prepare to compile PriTunl:</p>
<pre><code class="language-bash">$ sudo apt-get -y install build-essential git bzr python python-dev python-pip net-tools openvpn bridge-utils psmisc golang-go libffi-dev mongodb</code></pre><p>There are a few changes from the standard PriTunl source <a href="https://github.com/pritunl/pritunl" target="_blank">installation instructions on GitHub</a>. Make sure you are logged into your Pi and <strong>sudo</strong> to root:</p>
<pre><code class="language-bash">$ sudo su -</code></pre><p>This should leave you in root's home directory. To install PriTunl version 1.29.1914.98, enter (per GitHub):</p>
<pre><code class="language-bash">export VERSION=1.29.1914.98
tee -a ~/.bashrc &lt;&lt; EOF
export GOPATH=\$HOME/go
export PATH=/usr/local/go/bin:\$PATH
EOF
source ~/.bashrc
mkdir pritunl &amp;&amp; cd pritunl
go get -u github.com/pritunl/pritunl-dns
go get -u github.com/pritunl/pritunl-web
sudo ln -s ~/go/bin/pritunl-dns /usr/bin/pritunl-dns
sudo ln -s ~/go/bin/pritunl-web /usr/bin/pritunl-web
wget https://github.com/pritunl/pritunl/archive/$VERSION.tar.gz
tar -xf $VERSION.tar.gz
cd pritunl-$VERSION
python2 setup.py build
pip install -r requirements.txt
python2 setup.py install --prefix=/usr/local</code></pre><p>Now the MongoDB and PriTunl systemd units should be ready to start up. Assuming you're still logged in as root, enter:</p>
<pre><code class="language-bash">systemctl daemon-reload
systemctl start mongodb pritunl
systemctl enable mongodb pritunl</code></pre><p>That's it! You're ready to hit PriTunl's browser user interface and configure it by following PriTunl's <a href="https://docs.pritunl.com/docs/configuration-5" target="_blank">installation and configuration instructions</a> on its website.</p>
