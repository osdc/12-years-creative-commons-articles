<p>My first session at OSCON this year was hosted by Jono Bacon on community management.</p>
<!--break-->
<p>We've seen a remarkable growth in community all over the world—people are getting together to make things, do things, hack, etc. This simple idea of people getting together to make communities makes Jono Bacon excited (me too). He hosted a half-day workshop at OSCON about community management, where he shared with us his packaged thoughts on building strong communities.</p>
<p>The day was segmented as follows:</p>
<ul><li><strong>We need a vision.</strong> This is the "fluffy" part.</li>
<li><strong>We need requirements.</strong> Communities are chaotic, and that makes them fun, but we do need to have requirements.</li>
<li><strong>We need to make a plan.</strong> There are many communities that have naturally sprung up (e.g. The Ice Bucket Challenge), but the very best communities have a plan behind them.</li>
<li><strong>We need an infrastructure.</strong></li>
<li><strong>We need to figure out how to get people involved</strong></li>
<li>Once we have people join, <strong>we need to measure the value of the community</strong> (especially if you're at a company). The key is refinement. We will screw some stuff up, and this is a good thing. Failure is an opportunity to be better.</li>
</ul><p>If we want to build strong communities, Bacon says, we have to start with a mission. We have to have a point and a focus. In order to assess the type of mission we want, we have to look at the world we're in. First off we're in the post-Snowden land of privacy, the land of 3D printing, the maker revolution, and a world where everyone is getting connected to the Internet. If building a community within or for your business seems like a marketing ploy, it will fail.</p>
<h2>Strategy (vision + mission + plans)</h2>
<p><strong>Vision:</strong> What are we going out there to do? This is the elevator pitch that will get people excited.</p>
<p>Jono breaks communities in to two types: read and write. Read communities are those that are user groups—people who need a place to talk and share. Write communities want to get together to change things. Open source projects are write communities and the focus of today.</p>
<p>The first thing we need to accept is that people are irrational. We need to use a bit of social engineering or behavioral economics to manage our communities.</p>
<p>Jono brought up the SCARF model, the core foundation for creating a successful community:</p>
<ul><li><strong>Status:</strong> Clarity in relative importance.</li>
<li><strong>Certainty:</strong> Creating a sense of security and predicability.</li>
<li><strong>Autonomy:</strong> Building choice into your environment. Even if those choices all lead to the same results, letting people pick is the key.</li>
<li><strong>Relatedness:</strong> Defining clear social groupings and systems. Build strong teams and help them work together.</li>
<li><strong>Fairness:</strong> Reducing unfair opportunity and rewards.</li>
</ul><p>Every community is different, but every community that is great is great because of great leadership. Some of the most impactful leaders though can be at the bottom of the food chain.</p>
<p>What is great leadership? It's broken in to two areas:</p>
<ul><li>Helping people to succeed in their goals.</li>
<li>Helping people to be the best that they can be.</li>
</ul><p>The goal with strategy is that we want to build predictable yet surprising results. Instead of trying to convince people who are skeptical, go out there and do it and surprise them. You also have to be honest—you cannot promise success when starting a new community—some things are going to work and some are not.</p>
<p>There are three steps to starting your community within a company or as an extension of your company:</p>
<h3>Observe</h3>
<ul><li><strong>Look at your environment.</strong></li>
<li><strong>Define requirements.</strong></li>
<li><strong>Define expectations.</strong></li>
<li><strong>Identify key players.</strong> This is really important. You need to find the people you want to influence and that you want to influence you.</li>
<li><strong>Assess risks and threats to you and others.</strong> When you join a company there are going to be people who are gunning for you, and those people will bemoan the work that you're doing. Others will actively try to derail your work, and both of these are the people you want to make friends with.</li>
<li><strong>Explore short and long-term changes.</strong> See how quickly people are joining and leaving a company.</li>
</ul><h3>Strategize</h3>
<ul><li><strong>Create a mission statement</strong> This isn't something you create once and never look at again—it's something people should think about every single day. It should answer the question, "Why are we doing this?"</li>
<li><strong>Create a set of values</strong> From the mission statement, you can pull out a set of values.</li>
<li><strong>Create a longer term roadmap.</strong> "In two years, we want to be here."</li>
<li><strong>Create a staff engagement plan.</strong> If you work for a company, how are you going to get out there an engage with people?</li>
<li><strong>Create a community engagement plan</strong> Find a way to make visiting the community a habit.</li>
<li><strong>Create a budget.</strong> Pick a budget and don't spend all of it.</li>
</ul><h3>Build</h3>
<ul><li>A strategic plan (for the execs)</li>
<li>An elevator pitch (for the staff)</li>
<li>An execution plan (for you)</li>
<li>Relationships (for the teams)</li>
</ul><p>In the end you have four core documents you end up with: mission statement, elevator pitch, strategic plan, and implementation plan. Through all of this you want to communicate your strategy, keep people included, and make them feel like they're part of the process.</p>
<h2>Planning</h2>
<p>Collaborative planning is really, really hard! We want to build a culture in which people can plan together, but not everyone in your community should play a role in how you plan. These people might be loud, but lack the skills to assist in planning. You need to find the best people to contribute to the plan because they have earned it.</p>
<p>There are two types of people in open source communities: hackers and maintainers. Hackers want to create things, and maintainers want to build stable software, fix bugs, and do QA.</p>
<p>For the hackers, you want to build a culture of chaos so people join in easily. This is like an on-ramp to the project. You also need project plans in place for the maintainers.</p>
<h3>Five areas to consider when planning:</h3>
<ul><li>Practical</li>
<li>Authentic</li>
<li>Influenceable</li>
<li>Visible</li>
<li>Opinionated (It's okay to say no to people)</li>
</ul><p><strong>Objective Key Results (OKR)</strong> is a process used at Google. The first step is to plan your next three month period—create some measurable objectives (no more than five). Next, define key results—set these to be deliberately ambitious (on the edge of impossible), but measurable outcomes (no more than three for each objective). Next, document the previous two steps and share them with everyone (when you share ambitious goals with the public, you don't want to look like an idiot by not achieving them). Provide updates regularly and stress that these are ambitious goals that you might not meet. We shouldn't just seek to have great results, but regularly exercising and stretching ourselves to make ourselves better.</p>
<p>After the three month period, grade yourself from 1 to 10. 1 being you didn't do a thing, and 10 being you finished everything. You should be getting about a 6 or 7. If you're getting a 10, then you're not stretching yourself enough. Finally, you want to revise and improve your goals for the next period. Because you're assessing yourself, you get to improve yourself—it's not designed to be a tool for your boss to grade you.</p>
<p>The next thing we need to do is connect to the hearts and minds of people. A plan that doesn't have people onboard is just words. We want people to really excited about the work we do. Building communities is the way we make the world a better place.</p>
<h2>Infrastructure</h2>
<p>To build a community is a collaborative effort.</p>
<p>New people will join your community and won't know what it is or how they can contribute. They want to see that this is a community that is eager to include them—this is the marketing part of things. Next they're on the on-ramp into your community. To get people on the on-ramp, you want to make it clear that people are critical to what you're doing and that you want them participate.</p>
<p>The next step is to get those community members to develop skills. This is more than providing tools to help people learn, but including instructions on how to participate. People don't want to read reams of information. We live in the time of Twitter and Facebook, so we need to provide efficient instructions—quick bullet points. Once our new members have learned how to contribute, you want them to do something. To help with this, create a list of bite-sized bugs—easy-to-fix bugs new members are encouraged to fix. Once they contribute, be sure to provide feedback—people want to feel validated.</p>
<p>For your open source project you're going to see a basic facilities:</p>
<ul><li>Website</li>
<li>Communication channels</li>
<li>Collaborative editing/knowledge base (wiki)</li>
<li>Code hosting</li>
<li>Issue tracking</li>
<li>News delivery (blog)</li>
<li>Social media</li>
</ul><p>Jono shared some of his recommendations for these different types of tools in the <a href="https://opensource.com/life/15/7/10-community-tools" target="_blank">latest edition</a> of his Six Degrees column.</p>
<h2>Growth</h2>
<p>Growth is about engagement. We want people to stick around. Jono's goal is 66 days, the amount of time he says it takes to develop a habit. So, we want to encourage conversation, creation, communications, and conduct to get our communities to grow in a healthy way.</p>
<h2>Measuring impact</h2>
<p>"If you're not measuring it, it didn't happen"</p>
<p>Aggregate measurements tell a fuller story than KPIs (a single number to tell how well something is working). KPI is something like there are a 1,000 people on the forum, but an aggregate measurement is something like levels where at level 1 you have to spend X amount of time on the site, participate in X topics, etc.. So then when you say you have 500 level 1 members on your site, you know what that means.</p>
<p>What you're looking for are the stories, the patterns, and the trends. If you want to identify a great community member, look a the whole of their contribution—not just how much code they contribute, but how they participate in discussions as well. Come up with a scale for your community.</p>
<p>Quality is way more important than quantity. Having lots of data is not more important than providing quality data. The data is there to show outcomes and outcomes are about patterns and trends not numbers. You want to illustrate the practical ways that you have succeeded in your community.</p>
<p>Our measurements might show that we failed—and that's okay. You need to fail and learn from it and improve upon things. Don't let the fear of failure stop you from measuring the impact of your community. Seeing "failure" in your data lets you realign your plans and community to figure out how to succeed at your goals.</p>
<div class="series-wide">OSCON<br /> Series</div>
<p><em>This article is part of the <a href="https://opensource.com/resources/oscon-2015-speaker-interview-collection" target="_blank">OSCON Series</a> for OSCON 2015. <a href="http://www.oscon.com/open-source-2015" target="_blank">OSCON</a> is everything open source—the full stack, with all of the languages, tools, frameworks, and best practices that you use in your work every day. OSCON 2015 will be held July 20-24 in Portland, Oregon.</em>.</p>
