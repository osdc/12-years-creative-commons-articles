<p>Early in my Linux journey, I came to appreciate the numerous command-line utilities of the operating system and the way they streamlined regular tasks. For example, backing up applications on my Windows server frequently required expensive add-on software packages. By contrast, the <code>tar</code> command makes backing up Linux relatively easy, and it's powerful and reliable too.</p>
<p>When backing up our school district email system, however, I faced a different challenge. Backups couldn't occur during the workday or early evening because people were using the system. The backup had to occur after midnight, and it needed to be reliable. I was used to Windows Task Manager, but what was I going to use on Linux? That's when I learned about cron.</p>
<p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">More Linux resources</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux commands cheat sheet">Linux commands cheat sheet</a></div>
              <div class="field__item"><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Advanced Linux commands cheat sheet">Advanced Linux commands cheat sheet</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Free online course: RHEL technical overview">Free online course: RHEL technical overview</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux networking cheat sheet">Linux networking cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="SELinux cheat sheet">SELinux cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux common commands cheat sheet">Linux common commands cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="What are Linux containers?">What are Linux containers?</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Register for your free Red Hat account">Register for your free Red Hat account</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Our latest Linux articles">Our latest Linux articles</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<h2>Scheduling tasks on Linux with cron</h2>
<p>Cron is a daemon used to execute scheduled commands automatically. Learning how to use cron required some reading and experimenting, but soon I was using cron to shut down our email server, back up the data in a compressed tar file, then restart the email service at 3AM.</p>
<p>The commands for a cron job are stored in the crontab file on a Linux system, which is usually found in /etc/crontab. Display the contents of your crontab file with <code>$ crontab -l</code>.</p>
<p>Edit the crontab file with <code>$ crontab -e</code>.</p>
<p>Some systems default to the <a href="https://opensource.com/article/20/12/vi-text-editor">Vi editor</a> for cron editing. You can override this setting using <a href="https://opensource.com/article/19/8/what-are-environment-variables">environment variables</a>:</p>
<pre>
<code class="language-bash">$ EDITOR=nano crontab -e</code></pre><p>This allows you to use the <a href="https://opensource.com/article/20/12/gnu-nano">nano editor</a> to edit your personal crontab (if you don't have one yet, one is created automatically for you).</p>
<p>All crontab commands have parameters denoted by an asterisk until you insert an integer value. The first represents minutes, then hours, day of the month, month of the year, and finally, day of the week.</p>
<p>Comments are preceded by a hash. Cron ignores comments, so they're a great way to leave yourself notes about what a command does and why it's important.</p>
<h2>A sample cron job</h2>
<p>Suppose you want to scan your home directory for viruses and malware with <a href="https://opensource.com/business/15/9/keeping-your-linux-system-safe">clamscan</a> every week on Monday at 10AM. You also want to back up your home directory every week on Tuesday at 9AM. Using cron and crontab files ensures that your system maintenance occurs every week whether you remember to run those utilities or not.</p>
<p>Edit your crontab file to include the following, using your own username instead of "don" (my user name):</p>
<pre>
<code class="language-bash"># Scan my home directory for viruses
0 10 * * 1 clamscan -ir /home/don
# Backup my home directory
0 9 * * 2 tar -zcf /var/backups/home.tgz /home/don</code></pre><p>If you're using the nano editor, save your work with <b>Ctrl+O</b> to write the file out and <b>Ctrl+X</b> to exit the editor. After editing the file, use <code>crontab -l</code> to list the contents of your cron file to ensure that it has been properly saved.</p>
<p>You can create crontab jobs for any job required on your system. This takes full advantage of the cron daemon.</p>
<h2>Scheduling from the Linux command line</h2>
<p>It's no secret that the hardest part of cron is coming up with the right values for those leading asterisks. There are websites, like <a href="https://crontab.guru/" target="blank">crontab.guru</a>, that dynamically translate cron time into human-readable translations, and Opensource.com has a <a href="https://opensource.com/downloads/linux-cron-cheat-sheet">cron cheat sheet</a> you can download to help you keep it straight.</p>
<p>Additionally, most modern cron systems feature shortcuts to common values, including:</p>
<ul><li><code>@hourly </code>: Run once an hour (0 * * * *)</li>
<li><code>@daily</code> : Run once a day (0 0 * * *)</li>
<li><code>@weekly</code> : Run once a week (0 0 * * 0)</li>
<li><code>@monthly</code> : Run once a month (0 0 1 * *)</li>
<li><code>@reboot</code> : Run once after reboot</li>
</ul><p>There are also alternatives to cron, including <a href="https://opensource.com/article/21/2/linux-automation">anacron</a> for jobs you want to run regularly but not according to a specific schedule, and the <code>at</code> <a href="https://opensource.com/article/21/8/linux-at-command">command</a> for one-off jobs.</p>
<p>Cron is a useful task scheduling system, and it's as easy to use as editing text. Give it a try!</p>
