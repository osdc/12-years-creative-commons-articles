<p>At All Things Open 2016, Joe Brockmeier answers the question: How can companies can work effectively with open source communities?</p>
<p>In his talk, Joe reminded us of the #1 open source myth: Open source is comprised of mostly volunteers. The truth is, these days, pretty much any major open source project has people who are paid to work on it. There are always people who do it because they love it, but these days most of us are paid (and still love it). Over the years we have learned that if you want patches in a timely manner, you need people who are paid to do it.</p>
<p>Even though some of us are paid to contribute, we have to remember that the community is key. You want to focus on the building blocks for your community before you get too deep in the code. "We'll do it right later" doesn't always work out well. If you don't make governance decisions now, things can fall apart, or if you make the wrong decision, it could turn off potential community members going forward.</p>
<h3>Sharing</h3>
<p>Always remember that core community principles of transparency and openness are necessary. There will always be things you can't share for legal reasons, but everything that can be shared legally should be shared. Decisions should be made openly and transparently. </p>
<p>If you're participating in but not leading a project, send pull requests, don't just fix things in your company's version of the software. Have contribution policies for your employees, but let your employees participate. </p>
<h3>Mentoring</h3>
<p>Provide your community with mentors. Mentors are key to showing people the ropes and helping new community members succeed. Nobody in a community is irreplaceable. You don't want to build your community solely on the presence of one person.</p>
<h3>Contributing</h3>
<p>If you're one of those who is paid to contribute, remember to leave your hat at the door. Generally speaking when participating in an open source project, even if it's predominately managed by your company, don't use your company as the reason for your decisions. You should be thinking of the health of your project not your corporate sponsorship. If you work to the interest of the community over your employer this will make you a trusted member of the community. For example, don't block features in the community because it hurts your company. What's best for the community is what's best for the product.</p>
<p>Remember that all contributors matter. It's a bummer when a community only acknowledges coding contributions. You need UX, design, documentation, marketing, and more to have a successful community. Make sure everyone is treated equally, and most importantly don't treat users as an afterthought. If you don't care about the users, then you won't build much momentum with your project.</p>
<h3>Governing </h3>
<p>Define rules for how to handle legal issues, community issues, releases, and more.</p>
<ul><li>One size can't fit all</li>
<li>Have clear guidelines</li>
<li>Have Contributor License Agreements (CLA) that avoid terms that make one entity "more equal than others" </li>
<li>State how a user becomes a contributor</li>
<li>State how a contributor becomes a core contributor or earns rights</li>
<li>Follow the golden rule: Think of what behavior you'd want to see in a community and be that </li>
<li>Emphasize diversity and inclusivity in both skills and people</li>
</ul><p>When working on governance, everything should be on the mailing list or in some sort of archived permanent record. Work on a limit for feedback. Let the community have 72 business hours to give feedback and if no one replies, then you have a "lazy consensus." Legal issues will always be the exception here, but you want to limit over-talking about issues.</p>
<p>Make decisions stick and remember that your job title doesn't matter in the voting process. New people will wander in and question everything and people will claim they forget, but don't try to relitigate things that have been decided. To prevent these questions or requests to change decisions, you want to document, document, document, (software, processes, decisions, etc.), make sure there is a record of everything.</p>
<h3>Infrastructure</h3>
<p>To facilitate communication and open collaboration you want to make sure you have the right infrastructure in place. This includes, but is not limited to:</p>
<ul><li>mailing lists and forums</li>
<li>a bug tracker</li>
<li>documentation and wiki (although, don't use wiki for official documentation, though; it isn't user-friendly)</li>
<li>code repositories</li>
<li>a trello or kanban board of some sort</li>
<li>translation tools</li>
<li>continuous integration and testing infrastructure (tools to automatically test when you commit)</li>
<li>IRC or chat (remember that this is good only if there are enough folks to be there to answer questions; if users go in and don't get an answer, then it's harmful instead of helpful)</li>
</ul><h3>Citizenship</h3>
<p>Speak up! Tell people about your use of open source.</p>
<p>Community is a process—you're never done with community. Successful communities will change and evolve. Single company projects are not as good as diverse projects. If you're not growing you're dying. And finally, always remember the Golden Rule!</p>
