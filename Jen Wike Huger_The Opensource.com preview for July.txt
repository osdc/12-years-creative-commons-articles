<p><strong>The Opensource.com preview</strong> brings you highlights from last month, editorial announcements for coming months, and other tidbits.</p>
<p>In June, we attended and shared articles about topics discussed at <a href="https://opensource.com/resources/southeast-linuxfest-2016">Southeast Linuxfest</a>, <a href="https://opensource.com/tags/open-source-bridge">Open Source Bridge</a>, and Red Hat Summit (including <a href="https://opensource.com/life/16/6/academic-women-open-source-award-interview">my interview</a> with the academic Women in Open Source Award winner Preeti Murthy). We also published a series on <a href="https://opensource.com/tags/entertainment" target="_blank">Open Source in Entertainment</a> that discusses the ways open source communities are having fun, plus some entertaining open source tools.</p>
<h2>July things to do</h2>
<p>Next month, you can:</p>
<ul><li>Send us a sys admin story or your favorite tools for our <b>Sys Admins in Open Source </b>series. Drafts due by July 15.</li>
<li><b>Take our annual reader survey</b> and enter to win an Opensource.com backpack! The survey goes live on July 11.</li>
</ul><p>Check out the Opensource.com <a href="https://opensource.com/resources/editorial-calendar" target="blank">editorial calendar and columns</a> for additional writing opportunities. Not sure about writing? Well, we've rounded up <a href="https://opensource.com/life/15/7/7-big-reasons-contribute-opensourcecom" target="_blank">7 big reasons to contribute to Opensource.com</a>.</p>
<p>If you're organizing a 2016 open source conference or event, be sure to add it to our <a href="https://opensource.com/resources/conferences-and-events-monthly" target="_blank">community calendar</a>. </p>
<p>Got questions? Email us at <a href="mailto:open@opensource.com" target="_blank">open@opensource.com</a> and find us on <a href="https://opensource.com/should-be/10/1/opensourcecom-freenode-irc" target="_blank">Freenode IRC at #opensource.com</a>.</p>
<h2>The Open Org</h2>
<p><strong>Coming soon in print.</strong><em> <a href="https://opensource.com/open-organization/resources/field-guide">The Open Organization Field Guide: Practical Tips for Igniting Passion and Performance</a></em>, a community-produced handbook to the <em>The Open Organization</em>. Licensed for sharing and modification under a Creative Commons license, the book collects cutting-edge ideas about the ways open source values are changing the ways we work, manage, and lead. Contributing authors: Brian Fielkow, Jackie Yeaney, Phil Branon, Jason Hibbets, Pete Savage, Merry Beekman, Brook Manville, Bryan Behrenshausen, Margaret Dawson, Rikki Endsley, Jen Wike Huger, Laura Hilliger, Sam Knuth, Jason Baker, Jim Whitehurst, and Don Watkins.</p>
<p><strong>Special guests tweet in July.</strong> On Thursday, July 21 The Open Org will hold their monthly Twitter chat, this month focusing on "The Open Organization and Non-Profits." Special guests include:</p>
<ul><li>Matt Thompson (<a href="https://twitter.com/OpenMatt" target="_blank">@openmatt</a>)</li>
<li>Karen Sandler (<a href="https://twitter.com/o0karen0o" target="_blank">@o0karen0o</a>)</li>
<li>Danese Cooper (<a href="https://twitter.com/divadanese" target="_blank">@divadanese</a>)</li>
<li>E.G. Nadhan (<a href="https://twitter.com/NadhanEG" target="_blank">@NadhanEG</a>)</li>
<li>Leslie Hawthorn (<a href="https://twitter.com/lhawthorn" target="_blank">@lhawthorn</a>)</li>
<li>David Egts (<a href="https://twitter.com/davidegts" target="_blank">@davidegts</a>)</li>
</ul><p>See <a href="https://opensource.com/open-organization/resources/twitter-chats">August and September's Twitter chats</a>, too.</p>
