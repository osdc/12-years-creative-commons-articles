<p>For decades now, open source software (and lately, hardware!) has been transforming the ways that we live and work. It is, in the opinion of <a href="https://openaps.org/" target="_blank">OpenAPS</a> project founder <a href="https://diyps.org/about/dana-lewis/" target="_blank">Dana Lewis</a>, high time that we start using open source to innovate to save lives and solve health problems.</p>
<p>In her Day 2 OSCON keynote, Dana set out reminding us that Type I diabetes, suffered by millions of people around the world, is incredibly complicated to manage; there are many things that can change your insulin levels from moment to moment, and managing that with monitors and pump devices is difficult, requiring measurement and adjustment many times a day. But this has one core weakness: it's a loop that can become "open" too easily. The patient must sleep at some point, and therefore cannot do the monitoring and calculations necessary to adjust dosages. Devices used for monitoring and dosing are often not interoperable.</p>
<p>The solution is an artificial pancreas, which acts to close that loop by monitoring and adjusting insulin levels, just as the normal pancreas does. But those devices are not yet available on the commercial market, and won't be for some years yet.</p>
<p>Enter the <a href="https://openaps.org/" target="_blank">OpenAPS</a> project (Open Artificial Pancreas System); find it here on <a href="https://github.com/openaps" target="_blank">GitHub</a>. "OpenAPS is a simplified Artificial Pancreas System (APS) designed to automatically adjust an insulin pump's basal insulin delivery to keep blood glucose (BG) in a safe range overnight and between meals."</p>
<p>The goals of the project are to deploy a growing number of DIY artificial pancreases by focusing on these features:</p>
<ul><li>Safely using currently-available and deployed devices</li>
<li>Limiting dosing ability in both hardware and software</li>
<li>Using the same dosage calculations that a person would</li>
<li>Responding, or not, to unexpected data</li>
<li>Tolerating communication failures</li>
<li>Falling back safely to standard device operations</li>
</ul><p>The system is designed to "fail low," rather than the dangerous possibility of overdosing the user. The reference design is written in plain English, so that potential users do not need to be developers to deploy the solution. They need a small amount of DIY skills, the ability to load software to a Raspberry Pi or other tiny computer, and a thorough knowledge of diabetes' impact in their own lives.</p>
<p>The components needed are:</p>
<ol><li>A continuous gluclose monitor</li>
<li>A Rapsberry Pi or other tiny computer</li>
<li>Batteries for all of the devices</li>
<li>CareLink USB Wireless Upload Device</li>
<li>An insulin pump</li>
</ol><p>One concern is how this is regulated. Right now, the open source QA process is robust, and users are monitoring their own system. Additionally, by law, "off-label" usage of medical devices is unregulated. "You can do this!," Dana says. Results so far have been encouraging: 59 users have implemented the system, for both adults and children, including a father who deployed OpenAPS for his one-year-old daughter.</p>
<p>Dana closed with this: "What else is possible if you use your open source knowledge and skills to partner with someone in your life? With open source, there are endless opportunities to improve the lives of those we love."</p>
