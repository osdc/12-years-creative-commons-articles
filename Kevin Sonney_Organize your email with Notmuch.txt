<p>Last year, I brought you 19 days of new (to you) productivity tools for 2019. This year, I'm taking a different approach: building an environment that will allow you to be more productive in the new year, using tools you may or may not already be using.</p>
<h2 id="index-your-email-with-notmuch">Index your email with Notmuch</h2>
<p>Yesterday, I talked about how I use OfflineIMAP to <a href="https://opensource.com/article/20/1/sync-email-offlineimap">sync my mail</a> to my local machine. Today, I'll talk about how I preprocess all that mail before I read it.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Notmuch"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/productivity_4-1.png" width="675" height="118" alt="Notmuch" title="Notmuch" /></div>
      
  </article></p>
<p><a href="https://en.wikipedia.org/wiki/Maildir" target="_blank">Maildir</a> is probably one of the most useful mail storage formats out there. And there are a LOT of tools to help with managing your mail. The one I keep coming back to is a little program called <a href="https://notmuchmail.org/" target="_blank">Notmuch</a> that indexes, tags, and searches mail messages. And there are several programs that work with Notmuch to make it even easier to handle a large amount of mail.</p>
<p>Most Linux distributions include Notmuch, and you can also get it for MacOS. Windows users can access it through Windows Subsystem for Linux (<a href="https://docs.microsoft.com/en-us/windows/wsl/install-win10" target="_blank">WSL</a>), but it may require some additional tweaks.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Notmuch's first run"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/productivity_4-2.png" width="675" height="609" alt="Notmuch's first run" title="Notmuch's first run" /></div>
      
  </article></p>
<p>On Notmuch's very first run, it will ask you some questions and create a <strong>.notmuch-config</strong> file in your home directory. Next, index and tag all your mail by running <strong>notmuch new</strong>. You can verify it with <strong>notmuch search tag:new</strong>; this will find all messages with the "new" tag. That's probably a lot of mail since Notmuch uses the "new" tag to indicate messages that are new to it, so you'll want to clean that up.</p>
<p>Run <strong>notmuch search tag:unread</strong> to find any unread messages; that should result in quite a lot less mail. To remove the "new" tag from messages you've already seen, run <strong>notmuch tag -new not tag:unread</strong>, which will search for all messages without the "unread" tag and remove the "new" tag from them. Now when you run <strong>notmuch search tag:new</strong>, it should show only the unread mail messages.</p>
<p>Tagging messages in bulk is probably more useful, though, since manually updating tags at every run can be really tedious. The <strong>--batch</strong> command-line option tells Notmuch to read multiple lines of commands and execute them. There is also the <strong>--input=filename</strong> option, which reads commands from a file and applies them. I have a file called <strong>tagmail.notmuch</strong> that I use to add tags to mail that is "new"; it looks something like this:</p>
<pre><code class="language-bash"># Manage sent, spam, and trash folders
-unread -new folder:Trash
-unread -new folder:Spam
-unread -new folder:Sent

# Note mail sent specifically to me (excluding bug mail)
+to-me to:kevin at sonney.com and tag:new and not tag:to-me

# And note all mail sent from me
+sent from:kevin at sonney.com and tag:new and not tag:sent

# Remove the new tag from messages
-new tag:new</code></pre><p>I can then run <strong>notmuch tag --input=tagmail.notmuch</strong> to bulk-process my mail messages after running <strong>notmuch new</strong>, and then I can search on those tags as well.</p>
<p>Notmuch also supports running pre- and post-new hooks. These scripts, stored in <strong>Maildir/.notmuch/hooks</strong>, define actions to run before (pre-new) and after (post-new) to index new mail with <strong>notmuch new</strong>. In yesterday's article, I talked about using <a href="http://www.offlineimap.org/" target="_blank">OfflineIMAP</a> to sync mail from my IMAP server. It's very easy to run it from the "pre-new" hook:</p>
<pre><code class="language-bash">#!/bin/bash
# Remove the new tag from messages that are still tagged as new
notmuch tag -new tag:new

# Sync mail messages
offlineimap -a LocalSync -u quiet</code></pre><p>You can also use the Python application <a href="https://afew.readthedocs.io/en/latest/index.html" target="_blank">afew</a>, which interfaces with the Notmuch database, to tag things like <em>Mailing List</em> and <em>Spam</em> for you. You can run afew from the post-new hook in a similar way:</p>
<pre><code class="language-bash">#!/bin/bash
# tag with my custom tags
notmuch tag --input=~/tagmail.notmuch

# Run afew to tag new mail
afew -t -n</code></pre><p>I recommend that when using afew to tag messages, you do NOT use the <strong>[ListMailsFilter]</strong> since some mail handlers add obscure or downright junk List-ID headers to mail messages (I'm looking at you, Google).</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="alot email client"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/productivity_4-3.png" width="675" height="340" alt="alot email client" title="alot email client" /></div>
      
  </article></p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on productivity</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/downloads/collaboration-tools-ebook">5 open source collaboration tools</a></li>
<li><a href="https://opensource.com/downloads/organization-tools">6 open source tools for staying organized</a></li>
<li><a href="https://opensource.com/downloads/desktop-tools">7 open source desktop tools</a></li>
<li><a href="https://opensource.com/tags/tools">The latest on open source tools</a></li>
</ul></div>
</div>
</div>
</div>
<p>At this point, any mail reader that supports Notmuch or Maildir can work with my email. I'll sometimes use <a href="https://github.com/pazz/alot" target="_blank">alot</a>, a Notmuch-specific client, to read mail at the console, but it's not as fancy as some other mail readers.</p>
<p>In the coming days, I'll show you some other mail clients that will likely integrate with tools you already use. In the meantime, check out some of the other tools that work with Maildir mailboxes—you might find a hidden gem I've not tried yet.</p>
