<p>Join us today for an #OpenOrgChat about Open Organizations on Main Street. Our community will gather on Twitter using the #OpenOrgChat hashtag at 2 p.m. Eastern (14:00 ET/19:00 UTC).</p>
<p>We'll send the questions from @OpenOrgBook, watch for your answers, and enjoy the conversation. We hope you can join us.</p>
<!--break--><p>Follow <a href="https://twitter.com/openorgbook" target="_blank">OpenOrgBook</a> and the chat's <a href="https://www.hashtracking.com/streams/openorgbook/openorgchat" target="_blank">live stream</a> for updates!</p>
<p>This week's special guests:</p>
<ul><li>Thomas Cameron (<a href="https://twitter.com/thomasdcameron" target="_blank">@thomasdcameron</a>)</li>
<li>Rebecca Fernandez (<a href="https://twitter.com/ruhbehka" target="_blank">@ruhbehka</a>)</li>
<li>Charlie Reisinger (<a href="https://twitter.com/charlie3" target="_blank">@charlie3</a>)</li>
<li>Jackie Yeaney (<a href="https://twitter.com/jackieyeaney" target="_blank">@jackieyeaney</a>)</li>
</ul><h3>Some questions we'll explore</h3>
<ul><li>What unique challenges do non-tech orgs face when becoming more open?</li>
<li>What do tech-oriented orgs tend to take for granted that non-tech firms can't?</li>
<li>What's the first step a leader can take to foster a culture of sharing in a traditionally closed org?</li>
<li>What kind of transparency is possible in orgs with strong mandates to protect personal information, like hospitals?</li>
<li>How can you practice inclusive decision-making when access to/familiarity with digital tools is limited?</li>
<li>How can we translate "with more eyes, all bugs are shallow" for non-tech orgs?</li>
<li>What types of mainstream organizations could benefit from adopting open org principles?</li>
</ul><h3>#OpenOrgChat Twitter chat</h3>
<p><a class="twitter-timeline" data-widget-id="636969512437444608" href="https://twitter.com/search?q=openorgchat">Tweets about openorgchat</a> </p>
<script>
<!--//--><![CDATA[// ><!--
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
//--><!]]>
</script>