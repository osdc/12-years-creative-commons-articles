<p>In the <a href="https://opensource.com/article/18/1/step-step-guide-git">first article in this series</a> on getting started with Git, we created a simple Git repo and added a file to it by connecting it with our computer. In this article, we will learn a handful of other things about Git, namely how to clone (download), modify, add, and delete files in a Git repo.</p>
<h2>Let's make some clones</h2>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Programming and development</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ&amp;src=programming_resource_menu4">Red Hat Developers Blog</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Programming cheat sheets</a></li>
<li><a href="https://www.redhat.com/en/services/training/learning-subscription?intcmp=7016000000127cYAAQ">Try for free: Red Hat Learning Subscription</a></li>
<li><a href="https://opensource.com/downloads/bash-programming-guide?intcmp=7016000000127cYAAQ">eBook: An introduction to programming with Bash</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/bash-shell-cheat-sheet?intcmp=7016000000127cYAAQ">Bash Shell Scripting Cheat Sheet</a></li>
<li><a href="https://developers.redhat.com/e-books/modernizing-enterprise-java?intcmp=7016000000127cYAAQ">eBook: Modernizing Enterprise Java</a></li>
</ul></div>
</div>
</div>
</div>
<p>Say you already have a Git repo on GitHub and you want to get your files from it—maybe you lost the local copy on your computer or you're working on a different computer and want access to the files in your repository. What should you do? Download your files from GitHub? Exactly! We call this "cloning" in Git terminology. (You could also download the repo as a ZIP file, but we'll explore the clone method in this article.)</p>
<p>Let's clone the repo, called <em>Demo</em>, we created in the last article. (If you have not yet created a <em>Demo</em> repo, jump back to that article and do those steps before you proceed here.) To clone your file, just open your browser and navigate to <code>https://github.com/&lt;your_username&gt;/Demo</code> (where <code>&lt;your_username&gt;</code> is the name of your own repo. For example, my repo is <code>https://github.com/kedark3/Demo</code>). Once you navigate to that URL, click the "Clone or download" button, and your browser should look something like this:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Clone with HTTPS screen"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/git_guide11.png" width="650" height="440" alt="Clone with HTTPS screen" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>As you can see above, the "Clone with HTTPS" option is open. Copy your repo's URL from that dropdown box (<code>https://github.com/&lt;your_username&gt;/Demo.git</code>). Open the terminal and type the following command to clone your GitHub repo to your computer:</p>
<pre>
<code class="language-text">git clone https://github.com/&lt;your_username&gt;/Demo.git</code></pre><p>Then, to see the list of files in the <code>Demo</code> directory, enter the command:</p>
<pre>
<code class="language-text">ls Demo/</code></pre><p>Your terminal should look like this:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Terminal"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/git_guide12.png" width="650" height="378" alt="Terminal" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<h2>Modify files</h2>
<p>Now that we have cloned the repo, let's modify the files and update them on GitHub. To begin, enter the commands below, one by one, to change the directory to <code>Demo/</code>, check the contents of <code>README.md</code>, echo new (additional) content to <code>README.md</code>, and check the status with <code>git status</code>:</p>
<pre>
<code class="language-text">cd Demo/
ls
cat README.md
echo "Added another line to REAMD.md" &gt;&gt; README.md
cat README.md
git status</code></pre><p>This is how it will look in the terminal if you run these commands one by one:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Terminal after running git status"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/git_guide12.5.png" width="650" height="356" alt="Terminal after running git status" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>Let's look at the output of <code>git status</code> and walk through what it means. Don't worry about the part that says:</p>
<pre>
<code class="language-text">On branch master
Your branch is up-to-date with 'origin/master'.".</code></pre><p>because we haven't learned it yet. The next line says: <code>Changes not staged for commit</code>; this is telling you that the files listed below it aren't marked ready ("staged") to be committed. If you run <code>git add</code>, Git takes those files and marks them as <code>Ready for commit</code>; in other (Git) words, <code>Changes staged for commit</code>. Before we do that, let's check what we are adding to Git with the <code>git diff</code> command, then run <code>git add</code>.</p>
<p>Here is your terminal output:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Terminal output of git diff and git add"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/git_guide13.png" width="650" height="383" alt="Terminal output of git diff and git add" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>Let's break this down:</p>
<ul><li><code>diff --git a/README.md b/README.md</code> is what Git is comparing (i.e., <code>README.md</code> in this example).</li>
<li><code>--- a/README.md</code> would show anything removed from the file.</li>
<li><code>+++ b/README.md</code> would show anything added to your file.</li>
<li>Anything added to the file is printed in green text with a + at the beginning of the line.</li>
<li>If we had removed anything, it would be printed in red text with a - sign at the beginning.</li>
<li>Git status now says <code>Changes to be committed:</code> and lists the filename (i.e., <code>README.md</code>) and what happened to that file (i.e., it has been <code>modified</code> and is ready to be committed).</li>
</ul><p>Tip: If you have already run <code>git add</code>, and now you want to see what's different, the usual <code>git diff</code> won't yield anything because you already added the file. Instead, you must use <code>git diff --cached</code>. It will show you the difference between the current version and previous version of files that Git was told to add. Your terminal output would look like this:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Checking differences on an added file"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/git_guide14.png" width="650" height="368" alt="Checking differences on an added file" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<h2>Upload a file to your repo</h2>
<p>We have modified the <code>README.md</code> file with some new content and it's time to upload it to GitHub.</p>
<p>Let's commit the changes and push those to GitHub. Run:</p>
<pre>
<code class="language-text">git commit -m "Updated Readme file"</code></pre><p>This tells Git that you are "committing" to changes that you have "added" to it. You may recall from the first part of this series that it's important to add a message to explain what you did in your commit so you know its purpose when you look back at your Git log later. (We will look more at this topic in the next article.) <code>Updated Readme file</code> is the message for this commit—if you don't think this is the most logical way to explain what you did, feel free to write your commit message differently.</p>
<p>Run <code>git push -u origin master</code>. This will prompt you for your username and password, then upload the file to your GitHub repo. Refresh your GitHub page, and you should see the changes you just made to <code>README.md</code>.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="GitHub after changes made to README.md"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/git_guide15.png" width="650" height="406" alt="GitHub after changes made to README.md" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>The bottom-right corner of the terminal shows that I committed the changes, checked the Git status, and pushed the changes to GitHub. Git status says:</p>
<pre>
<code class="language-text">Your branch is ahead of 'origin/master' by 1 commit
  (use "git push" to publish your local commits)</code></pre><p>The first line indicates there is one commit in the local repo but not present in origin/master (i.e., on GitHub). The next line directs us to push those changes to origin/master, and that is what we did. (To refresh your memory on what "origin" means in this case, refer to the first article in this series. I will explain what "master" means in the next article, when we discuss branching.)</p>
<h2>Add a new file to Git</h2>
<p>Now that we have modified a file and updated it on GitHub, let's create a new file, add it to Git, and upload it to GitHub. Run:</p>
<pre>
<code class="language-text">echo "This is a new file" &gt;&gt; file.txt</code></pre><p>This will create a new file named <code>file.txt</code>.</p>
<p>If you <code>cat</code> it out:</p>
<pre>
<code class="language-text">cat file.txt</code></pre><p>You should see the contents of the file. Now run:</p>
<pre>
<code class="language-text">git status</code></pre><p>Git reports that you have an untracked file (named <code>file.txt</code>) in your repository. This is Git's way of telling you that there is a new file in the repo directory on your computer that you haven't told Git about, and Git is not tracking that file for any changes you make.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Terminal reporting untracked file"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/git_guide16.png" width="650" height="243" alt="Terminal reporting untracked file" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>We need to tell Git to track this file so we can commit it and upload it to our repo. Here's the command to do that:</p>
<pre>
<code class="language-text">git add file.txt
git status</code></pre><p>Your terminal output is:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Terminal after telling Git to track a file"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/git_guide17.png" width="614" height="201" alt="Terminal after telling Git to track a file" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>Git status is telling you there are changes to <code>file.txt</code> to be committed, and that it is a <code>new file</code> to Git, which it was not aware of before this. Now that we have added <code>file.txt</code> to Git, we can commit the changes and push it to origin/master.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Terminal after adding a file to Git"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/git_guide18.png" width="650" height="276" alt="Terminal after adding a file to Git" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>Git has now uploaded this new file to GitHub; if you refresh your GitHub page, you should see the new file, <code>file.txt</code>, in your Git repo on GitHub.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="GitHub after adding a file to Git"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/git_guide19.png" width="650" height="387" alt="GitHub after adding a file to Git" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>With these steps, you can create as many files as you like, add them to Git, and commit and push them up to GitHub.</p>
<h2>Delete a file from Git</h2>
<p>What if we discovered we made an error and need to delete <code>file.txt</code> from our repo. One way is to remove the file from our local copy of the repo with this command:</p>
<pre>
<code class="language-text">rm file.txt</code></pre><p>If you do <code>git status</code> now, Git says there is a file that is <code>not staged for commit</code> and it has been <code>deleted</code> from the local copy of the repo. If we now run:</p>
<pre>
<code class="language-text">git add file.txt
git status</code></pre><p>I know we are deleting the file, but we still run <code>git add</code><i> </i>because we need to tell Git about the <b>change</b> we are making. <code>git add</code><i> </i>can be used when we are adding a new file to Git, modifying contents of an existing file and adding it to Git, or deleting a file from a Git repo. Effectively, <code>git add</code> takes all the changes into account and stages those changes for commit. If in doubt, carefully look at output of each command in the terminal screenshot below.</p>
<p>Git will tell us the deleted file is staged for commit. As soon as you commit this change and push it to GitHub, the file will be removed from the repo on GitHub as well. Do this by running:</p>
<pre>
<code class="language-text">git commit -m "Delete file.txt"
git push -u origin master</code></pre><p>Now your terminal looks like this:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Terminal after deleting a file"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/git_guide20.png" width="575" height="600" alt="Terminal after deleting a file" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>And your GitHub looks like this:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="GitHub after deleting a file"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/git_guide21.png" width="650" height="432" alt="GitHub after deleting a file" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>Now you know how to clone, add, modify, and delete Git files from your repo. The next article in this series will examine Git branching.</p>
