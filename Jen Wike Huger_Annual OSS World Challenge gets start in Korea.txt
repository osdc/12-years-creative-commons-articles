<p>In 2007, the Korean government first held the <a title="OSS World Challenge" href="http://www.ossaward.org/" target="_blank">OSS World Challenge</a> in an effort to promote open source software and bring awareness to developers within the country. </p>
<p><span style="letter-spacing: 0px;">Today, the challenge is open to entries from all over the world: </span><span style="letter-spacing: 0px;">OSS projects that were developed within the last year</span><span style="letter-spacing: 0px;"> are eligible.</span></p>
<p><span style="letter-spacing: 0px;"></span></p>
<!--break--><p><span style="letter-spacing: 0px;">Prizes include:</span></p>
<ul><li><span style="letter-spacing: 0px;">Grand Prize: $8,000 USD</span></li>
<li><span style="letter-spacing: 0px;">Gold Prize $5,000 USD</span></li>
<li><span style="letter-spacing: 0px;">Silver Prize $3,000 USD</span></li>
<li><span style="letter-spacing: 0px;">Additional prizes from Korean enterprises (see below)</span></li>
</ul><p><em>Read more in this interview with <a title="Sohee Kang profile on OSDC" href="https://opensource.com/users/sohee4ever" target="_blank">Sohee Kang</a>.</em></p>
<p><em><img src="https://opensource.com/sites/default/files/images/life/Interview%20banner%20Q%26A.png" alt="Interview banner" title="Interview banner" width="520" height="88" /><br /></em></p>
<h3><span style="letter-spacing: 0px; font-family: Helvetica, Arial, sans-serif; font-size: 16px; line-height: 1.3em;">Why is the Ministry of Science, ICT and Future Planning of Korea invested in open source software?</span></h3>
<p><span style="letter-spacing: 0px;"> </span><span style="font-family: Arial, sans-serif; letter-spacing: 0px;">Korea was once a leader in the IT field. When it began to lag behind other countries, especially as open source software grew, the government realized the gravity of the situation and came up with solutions to cultivate more competent open source software developers.</span></p>
<p>The OSS World Challenge was a way to encourage more participation in the field.</p>
<h3>What other countries, organizations, and champions of OSS are involved in the competition?</h3>
<p>The OSS World Challenge was limited to Korean entries until 2008. The committee decided to expand the competition to the global level to encourage Koreans to study advanced technologies and to participate in mutual exchanges from all over the world, capitalizing on the international nature of open source software.</p>
<p>Currently, the committee is made up of all Koreans. However, the judges are from the USA, Singapore, Japan, and China. Many professors from leading universities have been encouraging their students to participate in this competition.</p>
<h3>How are entries judged?</h3>
<p>See the guidelines <a title="OSS Awards judging criteria" href="http://www.ossaward.org/eng/sub11.php" target="_blank">here</a>. <span style="letter-spacing: 0px;">Criteria include:</span></p>
<ul><li><strong>originality</strong></li>
<li><strong>completeness</strong></li>
<li><span style="letter-spacing: 0px;"><strong>appropriateness of coding</strong>: consistent, logical style; detailed annotations; high code legibility</span></li>
<li><span style="letter-spacing: 0px;"><strong>feasibility of an application</strong>: accurate and detailed project plan; descriptive and detailed including analysis of demand, functionality, interface standard</span></li>
<li><span style="letter-spacing: 0px;"><strong>independence</strong>: not much use of existing open source software, mostly original</span></li>
<li><span style="letter-spacing: 0px;"><strong>use of the open source software platform</strong>: positive use of other open source software</span></li>
<li><span style="letter-spacing: 0px;"><strong>viral nature</strong>: high possibility of further development</span></li>
<li><span style="letter-spacing: 0px;"><strong>test method</strong>: great design of test method and automatic test tool, testable for most functions</span></li>
</ul><h3>Who awards the prizes?</h3>
<p>The Minister of the Ministry of Science, ICT and Future Planning of Korea will award the Grand Prize of $8,000 USD.</p>
<p>The President of the National IT Industry Promotion Agency will award the Gold Prize of $5,000 USD.</p>
<p>The president of the Korea Open Source Software Association wil award the Silver Prize of $3,000 USD.</p>
<p><span style="letter-spacing: 0px;">Additional prizes from a few Korean enterprises include:</span></p>
<ul><li><span style="letter-spacing: 0px;">SK Planet: Mobile application or service based on 'Planet X' which is the open API platform of SK Planet ($6,400 USD)</span></li>
<li><span style="letter-spacing: 0px;">uEngine Solutions: SaaS application based on Open PaaS ($3,600 USD)</span></li>
<li><span style="letter-spacing: 0px;">Software in Life: Open source software based on the designated laaS platform ($3,600 USD)</span></li>
</ul><h3>What projects won last year?</h3>
<p>We have had the winners from various countries including Singapore, Sweden, Malaysia, England, Germany, China and Japan since 2009.</p>
<p>The 2012 Grand Prize winner was "OMPL" from Rice University with <em>The Open Motion Planning Library</em>; team members include: Mark Moll, Ryan Luna, Ioan Sucan, Matt Maly, Lydia Kavraki, and Sachin Chitta.</p>
<p>The 2012 Gold Prize winner was STAN from Columbia University with <em>STAN, a C++ library for probability and sampling</em>; team members include: Daniel Lee, Bob Carpenter, Andrew Gelman, Matthew Hoffman, Jiqiang Guo, Wei Wang, Ben Goodrich, and Marcus Brubaker.</p>
<p>The 2012 Silver Prize winner was "DDT" from the Swiss Federal Institute of Technology Lausanne with <em>DDT - Device Driver Tester</em>; team members include: Vitaly Chipounov, Volodymyr Kuznetsov, and George Candea.</p>
