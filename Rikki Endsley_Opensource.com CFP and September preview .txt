<p>We're looking for open source-angled articles for a few upcoming themes:</p>
<h2>Machine learning and AI</h2>
<p>In September, we'll be taking a deep dive into the world of deep learning, machine learning, and <a href="https://www.redhat.com/en/open-source-stories/road-to-ai" target="_blank">artificial intelligence</a>. We want to hear more about how you're using <a href="https://opensource.com/article/17/2/machine-learning-projects-tensorflow-raspberry-pi" target="_blank">TensorFlow</a>, DSSTNE, Apache MXNet, and other open source projects and tools. <i>Proposals due by September 9. Drafts due by September 18.</i></p>
<p><iframe allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/_sNNSEP-P7A" width="560"></iframe></p>
<h2>Big data and high-performance computing</h2>
<p>In October, we're thinking big—as in big data, high-performance computing, and supercomputing. We want to help share your stories about big data projects using open source technologies, or research you're working on with help from Linux supercomputers. <i>Proposals due by September 20. Drafts due by October 2.</i></p>
<p class="rtecenter"><strong>Send story proposals (along with brief outlines) to us at <a href="mailto:open@opensource.com">open@opensource.com</a>.</strong></p>
<h2>2017 Open Source Yearbook</h2>
<p>We're also working on the <em>2017 Open Source Yearbook</em>. If you have an article idea, send it our way. See <a href="https://opensource.com/yearbook/2016" target="_blank">past</a> <a href="https://opensource.com/yearbook/2015" target="_blank">yearbooks</a> for examples of the kinds of articles we're looking for, then send us your ideas. <i>Proposals due by October 2. Drafts due by October 16.</i></p>
<h2>Fall conference series</h2>
<p>We've been publishing articles from speakers who will be presenting at a variety of conferences. We already have articles from speakers you can meet at the <a href="https://opensource.com/tags/open-source-summit" target="_blank">Open Source Summit (North America)</a> and <a href="https://opensource.com/article/17/8/d3-angular" target="_blank">Connect.Tech</a>. Articles are still rolling in, so check back to hear from speakers you can meet at <a href="https://thestrangeloop.com/schedule.html" target="_blank">Strange Loop</a>, <a href="" target="_blank">SeaGL</a>, <a href="" target="_blank">All Things Open</a>, and other upcoming events.</p>
<p>Will you be speaking at a conference this fall? Send us an article idea based on or inspired by your talk.</p>
<h2>The Open Org</h2>
<p>The open organization community has a fascinating September lined up for us. For starters, they're capping off two series from Open Organization Ambassadors: <a href="https://opensource.com/users/ron-mcfarland" target="_blank">Ron McFarland</a> will continue his writing on open collaboration with external partners, and <a href="https://opensource.com/users/jenkelchner" target="_blank">Jen Kelchner</a> plans to offer her checklist for making digital transformation conversations easier. On top of that, ambassador Allison Matlack will explain the role that open managers play in employees' professional development. And new writer Ingrid Towey offers her take on the value of scaling organizational culture through an onboarding "buddy system." On top of all that, I also hear we can expect a surprise announcement from the community.</p>
<h2>Where to find us</h2>
<ul><li><a href="https://opensource.com/email-newsletter">Subscribe</a> to our weekly newsletter.</li>
<li>Join our <a href="https://opensource.com/should-be/10/6/community-mailing-list-created" target="_blank">community mailing list</a>.</li>
<li><a href="https://opensource.com/how-submit-article">Submit</a> a story idea.</li>
<li>Chat with us on Freenode IRC (#opensource.com).</li>
<li><a href="mailto:open@opensource.com">Email us</a>.</li>
</ul>