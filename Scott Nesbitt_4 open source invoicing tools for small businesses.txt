<p>No matter what your reasons for starting a small business, the key to keeping that business going is getting paid. Getting paid usually means sending a client an invoice.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Great Content</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=7016000000127cYAAQ">Free online course: RHEL technical overview</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=7016000000127cYAAQ">Learn Advanced Linux Commands</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Download Cheat Sheets</a></li>
<li><a href="https://opensource.com/alternatives?intcmp=7016000000127cYAAQ">Find an Open Source Alternative</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=7016000000127cYAAQ">Read Top Linux Content</a></li>
<li><a href="https://opensource.com/resources?intcmp=7016000000127cYAAQ">Check out open source resources</a></li>
</ul></div>
</div>
</div>
</div>
<p>It's easy enough to whip up an invoice using LibreOffice Writer or LibreOffice Calc, but sometimes you need a bit more. A more professional look. A way of keeping track of your invoices. Reminders about when to follow up on the invoices you've sent.</p>
<p>There's a wide range of commercial and closed-source invoicing tools out there. But the offerings on the open source side of the fence are just as good, and maybe even more flexible, than their closed source counterparts.</p>
<p>Let's take a look at four web-based open source invoicing tools that are great choices for freelancers and small businesses on a tight budget. I reviewed two of them in 2014, in an <a href="https://opensource.com/business/14/9/4-open-source-invoice-tools">earlier version</a> of this article. These four picks are easy to use and you can use them on just about any device.</p>
<h2>Invoice Ninja</h2>
<p>I've never been a fan of the term <em>ninja</em>. Despite that, I like <a href="https://www.invoiceninja.org/" target="_blank">Invoice Ninja</a>. A lot. It melds a simple interface with a set of features that let you create, manage, and send invoices to clients and customers.</p>
<p>You can easily configure multiple clients, track payments and outstanding invoices, generate quotes, and email invoices. What sets Invoice Ninja apart from its competitors is its <a href="https://www.invoiceninja.com/integrations/" target="_blank">integration with</a> over 40 online popular payment gateways, including PayPal, Stripe, WePay, and Apple Pay.</p>
<p><a href="https://github.com/invoiceninja/invoiceninja" target="_blank">Download</a> a version that you can install on your own server or get an account with the <a href="https://www.invoiceninja.com/invoicing-pricing-plans/" target="_blank">hosted version</a> of Invoice Ninja. There's a free version and a paid tier that will set you back US$ 8 a month.</p>
<h2>InvoicePlane</h2>
<p>Once upon a time, there was a nifty open source invoicing tool called FusionInvoice. One day, its creators took the latest version of the code proprietary. That didn't end happily, as FusionInvoice's doors were shut for good in 2018. But that wasn't the end of the application. An old version of the code stayed open source and morphed into <a href="https://invoiceplane.com/" target="_blank">InvoicePlane</a>, which packs all of FusionInvoice's goodness.</p>
<p>Creating an invoice takes just a couple of clicks. You can make them as minimal or detailed as you need. When you're ready, you can email your invoices or output them as PDFs. You can also create recurring invoices for clients or customers you regularly bill.</p>
<p>InvoicePlane does more than generate and track invoices. You can also create quotes for jobs or goods, track products you sell, view and enter payments, and run reports on your invoices.</p>
<p><a href="https://wiki.invoiceplane.com/en/1.5/getting-started/installation" target="_blank">Grab the code</a> and install it on your web server. Or, if you're not quite ready to do that, <a href="https://demo.invoiceplane.com/" target="_blank">take the demo</a> for a spin.</p>
<h2>OpenSourceBilling</h2>
<p>Described by its developer as "beautifully simple billing software," <a href="http://www.opensourcebilling.org/" target="_blank">OpenSourceBilling</a> lives up to the description. It has one of the cleanest interfaces I've seen, which makes configuring and using the tool a breeze.</p>
<p>OpenSourceBilling stands out because of its dashboard, which tracks your current and past invoices, as well as any outstanding amounts. Your information is broken up into graphs and tables, which makes it easy to follow.</p>
<p>You do much of the configuration on the invoice itself. You can add items, tax rates, clients, and even payment terms with a click and a few keystrokes. OpenSourceBilling saves that information across all of your invoices, both new and old.</p>
<p>As with some of the other tools we've looked at, OpenSourceBilling has a <a href="http://demo.opensourcebilling.org/" target="_blank">demo</a> you can try.</p>
<h2>BambooInvoice</h2>
<p>When I was a full-time freelance writer and consultant, I used <a href="https://www.bambooinvoice.net/" target="_blank">BambooInvoice</a> to bill my clients. When its original developer stopped working on the software, I was a bit disappointed. But BambooInvoice is back, and it's as good as ever.</p>
<p>What attracted me to BambooInvoice is its simplicity. It does one thing and does it well. You can create and edit invoices, and BambooInvoice keeps track of them by client and by the invoice numbers you assign to them. It also lets you know which invoices are open or overdue. You can email the invoices from within the application or generate PDFs. You can also run reports to keep tabs on your income.</p>
<p>To <a href="https://sourceforge.net/projects/bambooinvoice/" target="_blank">install</a> and use BambooInvoice, you'll need a web server running PHP 5 or newer as well as a MySQL database. Chances are you already have access to one, so you're good to go. </p>
<hr /><p>Do you have a favorite open source invoicing tool? Feel free to share it by leaving a comment.</p>
