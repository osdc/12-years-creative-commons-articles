<p>A unique trait of open source is that it's never truly EOL (End of Life). The disc images mostly remain online, and their licenses don't expire, so going back and installing an old version of Linux in a virtual machine and getting a precise picture of what progress Linux has made over the years is relatively simple.</p>
<p>We begin our journey with Slackware 1.01, posted to the <b>comp.os.linux.announce</b> newsgroup well over 20 years ago.</p>
<h2>Slackware 1.01 (1993)</h2>
<p><img alt="slackware 1.0 screenshot" class="media-image attr__typeof__foaf:Image img__fid__333766 img__view_mode__default attr__format__default attr__field_file_image_alt_text[und][0][value]__slackware 1.0 screenshot attr__field_file_image_title_text[und][0][value]__slackware attr__field_file_image_caption[und][0][value]__ attr__field_file_image_caption[und][0][format]__panopoly_wysiwyg_text attr__field_folder[und]__9402" height="404" src="https://opensource.com/sites/default/files/slack1.png" title="slackware 1.0 screenshot" typeof="foaf:Image" width="720" /></p>
<p><sup>Slackware 1.01</sup></p>
<p>The best part about trying Slackware 1.01 is that there's a pre-made image in Qemu's <a href="http://www.qemu-advent-calendar.org/2014" target="_blank">2014 series</a> of free images, so you don't have to perform the install manually (don't get used to this luxury).</p>
<pre>
<code class="language-bash">
 $ qemu-kvm -m 16M -drive if=ide,format=qcow2,file=slackware.qcow2 \
 -netdev user,id=slirp -device ne2k_isa,netdev=slirp \
 -serial stdio -redir tcp:22122::22</code></pre><p>Many things in 1993's version of Linux works just as you'd expect. All the basic commands, such as <code>ls</code> and <code>cd</code> work, all the basic tools (<code>gawk</code>, <code>cut</code>, <code>diff</code>, <code>perl</code>, and of course <a href="http://www.slackware.com/~volkerdi/" target="_blank">Volkerding</a>'s favorite <code>elvis</code>) are present and accounted for, but some of the little things surprised me. <code>BASH</code> courteously asks for confirmation when you try to tab-complete hundreds of files, and tools to inspect compressed files (such as <code>zless</code> and <code>zmore</code> and <code>zcat</code>) already existed. In more ways than I'd expected, the system feels surprisingly modern.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Linux resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux commands cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Free online course: RHEL Technical Overview</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux networking cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">SELinux cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux common commands cheat sheet</a></li>
<li><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">What are Linux containers?</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Our latest Linux articles</a></li>
</ul></div>
</div>
</div>
</div>

<p>What's missing is any notion of package management. All installs and uninstalls are entirely manual, with no tracking.</p>
<p>Over all, Slackware 1.01 feels a lot like a fairly modern UNIX—or more appropriately, it feels like modern UNIX might feel to a Linux user. Most everything is familiar, but there are differences here and there. Not nearly as much a difference as you might expect from an operating system released in 1993!</p>
<h2>Debian 0.91 (1994)</h2>
<p>To try Debian 0.91, I used the floppy disk images available on the <a href="https://ibiblio.org/pub/historic-linux/distributions/debian-0.91/debian-0.91/dist" target="_blank">Ibiblio digital archive</a>, originally posted in 1994. The commands to boot:</p>
<pre>
<code class="language-bash">
 $ gunzip bootdsk.gz basedsk1.gz basedsk2.gz
 $ qemu-system-i386 -M pc -m 64 -boot order=ac,menu=on \
   -drive file=bootdisk,if=floppy,format=raw \
   -drive file=debian.raw,if=ide,format=raw \
   -device ne2k_isa,netdev=slirp \
   -serial msmouse -vga std \
   -redir tcp:22122::22 \
   -netdev user,id=slirp</code></pre><p>The bootdisk for Debian 0.91 boots to a simple shell, with clear instructions on the steps you're meant to take next.</p>
<p>The install process is surprisingly smooth. It works off of a menu system with seven steps—from partitioning a hard drive and writing the ext2 filesystem to it, all the way through to copying the <code>basedsk</code> images. This provided a minimal Debian install with many of the familiar conventions any modern Linux user would expect from their OS.</p>
<p>Debian is now famous for its package management system, but there are mere hints of that in this early release. The <code>dpkg</code> command exists, but it's an interactive menu-based system—a sort of clunky <code>aptitude</code>, with several layers of menu selections and, unsurprisingly, a fraction of available packages.</p>
<p>Even so, you can sense the convenience factor in the design concept. You download three floppy images and end up with a bootable system, and then use a simple text menu to install more goodies. I sincerely see why Debian made a splash.</p>
<h2>Jurix/S.u.S.E. (1996)</h2>
<p><img alt="Jurix install screen" class="media-image attr__typeof__foaf:Image img__fid__333771 img__view_mode__default attr__format__default attr__field_file_image_alt_text[und][0][value]__Jurix install screen attr__field_file_image_title_text[und][0][value]__Jurix attr__field_file_image_caption[und][0][value]__ attr__field_file_image_caption[und][0][format]__panopoly_wysiwyg_text attr__field_folder[und]__9402" height="396" src="https://opensource.com/sites/default/files/jurix_install.png" title="Jurix install screen" typeof="foaf:Image" width="716" /></p>
<p><sup>Jurix installation</sup></p>
<p>A pre-cursor to SUSE, Jurix shipped with binary <code>.tgz</code> packages organized into directories resembling the structure of Slackware's install packages. The installer itself is also similar to Slackware's installer.</p>
<pre>
<code class="language-bash">
 $ qemu-system-i386 -M pc -m 1024 \
   -boot order=ac,menu=on \
   -drive \
    file=jurix/install,if=floppy,format=raw \
   -drive file=jurix.img,if=ide \
   -drive file=pkg.raw,if=ide,format=raw \
   -device ne2k_isa,netdev=slirp \
   -serial msmouse -vga std \
   -redir tcp:22122::22 \
   -netdev user,id=slirp</code></pre><p>Because I wasn't specifically looking for the earliest instance, Jurix was the first Linux distribution I found that really "felt" like it intended the user to use a GUI environment. <a href="http://www.xfree86.org/" target="_blank">XFree86</a> is installed by default, so if you didn't intend to use it, you had to opt out.</p>
<p>An example <code>/usr/lib/X11/XF86Config</code> (this later became <code>Xorg.conf</code>) file was provided, and that got me 90% of the way to a GUI, but fine-tuning <code>vsync</code>, <code>hsync</code>, and <code>ramdac</code> colormap overrides took me an entire weekend until I finally gave up.</p>
<p>Installing new packages on Jurix was simple; find a <code>.tgz</code> on your sources drive, and run a routine <code>tar</code> command: <code> $ su -c 'tar xzvf foo.tgz -C /' </code> The package gets unzipped and unarchived to the root partition, and ready to use. I did this with several packages I hadn't installed to begin with, and found it easy, fast, and reliable.</p>
<h2>SUSE 5.1 (1998)</h2>
<p><img alt="suse install" class="media-image attr__typeof__foaf:Image img__fid__333776 img__view_mode__default attr__format__default attr__field_file_image_alt_text[und][0][value]__suse install attr__field_file_image_title_text[und][0][value]__suse attr__field_file_image_caption[und][0][value]__ attr__field_file_image_caption[und][0][format]__panopoly_wysiwyg_text attr__field_folder[und]__9402" height="600" src="https://opensource.com/sites/default/files/suse5fvwm.png" title="suse install" typeof="foaf:Image" width="801" /></p>
<p><sup>FVWM running on SuSE 5.1</sup></p>
<p>I installed SUSE 5.1 from a InfoMagic CD-ROM purchased from a software store in Maryland in 1998.</p>
<pre>
<code class="language-bash">
 $ qemu-system-i386 -M pc-0.10 -m 64 \
   -boot order=ad,menu=on \
   -drive file=floppy.raw,if=floppy,format=raw \
   -cdrom /dev/sr0 \
   -drive file=suse5.raw,if=ide,format=raw \
   -vga cirrus -serial msmouse</code></pre><p>The install process was convoluted compared to those that came before. YaST volleyed configuration files and settings between a floppy disk and the boot CD-ROM, requiring several reboots and a few restarts as I tried to understand the sequence expected from me. Once I'd failed the process twice, I got used to the way YaST worked, and the third time was smooth and very much a hint at the Linux user experience to come in later years.</p>
<p>A GUI environment was my main goal for SUSE 5.1. The configuration process was familiar, with a few nice graphical tools (including a good <code>XF86Setup</code> frontend) to help test and debug mouse and monitor problems. It took less than an hour to get a GUI up and running, and most of the delay was caused by my own research on what resolutions and color depths Qemu's virtualized video card could handle.</p>
<p>Included desktops were <code>fvwm</code>, <code>fvwm2</code>, and <code>ctwm</code>. I used <code>fvwm</code>, and it worked as expected. I even discovered <code>tkDesk</code>, a dock and file manager combo pack that is surprisingly similar to Ubuntu's <code>Unity</code> launcher bar.</p>
<p>The experience was, over all, very pleasant, and in terms of getting a successful desktop up and running, SUSE 5.1 was a rousing success.</p>
<h2>Red Hat 6.0 (1999)</h2>
<p><img alt="Red Hat 1999" class="media-image attr__typeof__foaf:Image img__fid__333786 img__view_mode__default attr__format__default attr__field_file_image_alt_text[und][0][value]__Red Hat 1999 attr__field_file_image_title_text[und][0][value]__Red attr__field_file_image_caption[und][0][value]__ attr__field_file_image_caption[und][0][format]__panopoly_wysiwyg_text attr__field_folder[und]__9402" height="599" src="https://opensource.com/sites/default/files/redhat6_gimp_0.png" title="Red Hat 1999" typeof="foaf:Image" width="795" /></p>
<p><sup>Red Hat 6 running GIMP 1.x</sup></p>
<p>The next install disc I happened to have lying around was Red Hat 6.0. That's not RHEL 6.0—just Red Hat 6.0. This was a desktop distribution sold in stores, before RHEL or Fedora existed. The disc I used was purchased in June 1999.</p>
<pre>
<code class="language-bash">
 $ qemu-system-i386 -M pc-0.10 -m 512 \
   -boot order=ad,menu=on \
   -drive file=redhat6.raw,if=ide,format=raw \
   -serial msmouse -netdev user,id=slirp \
   -vga cirrus -cdrom /dev/sr0</code></pre><p>The installation was fully guided and remarkably fast. You never have to leave the safety of the install process, whether choosing what packages to install (grouped together in <b>Workstation</b>, <b>Server</b>, and <b>Custom</b> groups), partitioning a drive, or kicking off the install.</p>
<p>Red Hat 6 included an <code>xf86config</code> application to step you through X configuration, although it strangely allowed some mouse emulation options that X later claimed were invalid. It beat editing the Xf86Config file, but getting X correct was still clearly not a simple task.</p>
<p>The desktop bundled with Red Hat 6 was, as it still is, GNOME, but the window manager was an early <a href="http://enlightenment.org" target="_blank">Enlightenment</a>, which also provided the main sound daemon. <code>Xdm</code> and <code>gdm</code> were both provided as login managers so that normal users could log in without having the permission to start or kill X itself, which is particularly important on multi-user systems.</p>
<p>Certain staple applications are missing; <code>gedit</code> didn't exist yet, there's no grand unified office application, and there was no package manager to speak of. <code>GnoRPM</code>, a GUI interface for RPM installation, review, and removal, was the closest to a <code>yum</code> or <code>PackageKit</code> experience it had, and <code>gnotepad+</code> is the GUI text editor (Emacs notwithstanding, obviously).</p>
<p>Over all, though, the desktop is intuitive. Unlike later implementations of GNOME, this early version featured a panel at the bottom of the screen, with an application menu and launcher icons and virtual desktop control in a central location. I can't imagine a user of another operating system at the time finding this environment foreign.</p>
<p>Red Hat 6 was a strong entry for Linux, which was obviously moving seriously toward being a proper desktop OS.</p>
<h2>Mandrake 8.0 (2001)</h2>
<p><img alt="Mandrake 8.0 installer" class="media-image attr__typeof__foaf:Image img__fid__333806 img__view_mode__default attr__format__default attr__field_file_image_alt_text[und][0][value]__Mandrake 8.0 installer attr__field_file_image_title_text[und][0][value]__Mandrake attr__field_file_image_caption[und][0][value]__ attr__field_file_image_caption[und][0][format]__panopoly_wysiwyg_text attr__field_folder[und]__9402" height="598" src="https://opensource.com/sites/default/files/mandrake8.png" title="Mandrake 8.0 installer" typeof="foaf:Image" width="799" /></p>
<p><sup>Mandrake: A turning point in Linux</sup></p>
<p>Mandrake 8.0 was released in 2001, so it would have been compared to, for instance, Apple OS 9.2 and Windows ME.</p>
<p>I fell back on fairly old emulated tech to be safe.</p>
<pre>
<code class="language-bash">
 $ qemu-system-i386 \
   -M pc-0.10 -m 2048 \
   -boot order=ad,menu=on \
   -drive file=mandrake8.qcow2 \
   -usb -net nic,model=rtl8139 \
   -netdev user,id=slirp \
   -vga cirrus \
   -cdrom mandrake-8.0-i386.iso</code></pre><p>I'd thought the Red Hat installation process had been nice, but Mandrake's was amazing. It was friendly, it gave the user a chance to test configurations before continuing, it was easy and fast, and it worked almost like magic. I didn't even have to import my <code>XF86Config</code> file, because Mandrake's installer got it right.</p>
<p><img alt="Mandrake install" class="media-image attr__typeof__foaf:Image img__fid__333846 img__view_mode__default attr__format__default attr__field_file_image_alt_text[und][0][value]__Mandrake install attr__field_file_image_title_text[und][0][value]__Mandrake attr__field_file_image_caption[und][0][value]__ attr__field_file_image_caption[und][0][format]__panopoly_wysiwyg_text attr__field_folder[und]__9402" height="477" src="https://opensource.com/sites/default/files/mandrake8install_new.png" title="Mandrake install" typeof="foaf:Image" width="644" /></p>
<p><sup>Mandrake 8.0 installer</sup></p>
<p>Using the Mandrake desktop is a lot like using any given desktop of the time, actually. I was a little surprised at how similar the experience was. I feel certain that if I'd somehow stumbled into Mandrake Linux at this time, it actually wouldn't have been beyond my ability, even as a young and not very technical user. The interfaces are intuitive, the documentation helpful, and the package management quite natural, for a time when it still wasn't yet the mental default for people to just go to a website and download an installer for whatever software they wanted.</p>
<h2>Fedora 1 (2003)</h2>
<p><img alt="Fedora Core install" class="media-image attr__typeof__foaf:Image img__fid__333796 img__view_mode__default attr__format__default attr__field_file_image_alt_text[und][0][value]__Fedora Core install attr__field_file_image_title_text[und][0][value]__Fedora attr__field_file_image_caption[und][0][value]__ attr__field_file_image_caption[und][0][format]__panopoly_wysiwyg_text attr__field_folder[und]__9402" height="768" src="https://opensource.com/sites/default/files/fedora1.png" title="Fedora Core install" typeof="foaf:Image" width="1025" /></p>
<p><sup>Blue Fedora, Red Hat </sup></p>
<p>In 2003, the new Fedora Core distribution was released. Fedora Core was based on Red Hat, and was meant to carry on the banner of desktop Linux once Red Hat Enterprise Linux (RHEL) became the flagship product of the company.</p>
<p>Nothing particularly special is required to boot the old Fedora Core 1 disc:</p>
<pre>
<code class="language-bash">
 $ qemu-system-i386 -M pc \
   -m 2048 -boot order=ac,menu=on \
   -drive file=fedora1.qcow2 -usb \
   -net nic,model='rtl8139' -netdev user \
   -vga cirrus -cdrom fedora-1-i386-cd1.iso</code></pre><p>Installing Fedora Core is simple and familiar; it uses the same installer as Fedora and Red Hat for the next 9 years. It's a graphical interface that's easy to use and easy to understand.</p>
<p><img alt="Fedora Anaconda" class="media-image attr__typeof__foaf:Image img__fid__333801 img__view_mode__default attr__format__default attr__field_file_image_alt_text[und][0][value]__Fedora Anaconda attr__field_file_image_title_text[und][0][value]__Fedora attr__field_file_image_caption[und][0][value]__ attr__field_file_image_caption[und][0][format]__panopoly_wysiwyg_text attr__field_folder[und]__9402" height="598" src="https://opensource.com/sites/default/files/fedora1anaconda.png" title="Fedora Anaconda" typeof="foaf:Image" width="802" /><sup>Anaconda GUI</sup></p>
<p>The Fedora Core experience is largely indistinguishable from Red Hat 6 or 7. The GNOME desktop is polished, there are all the signature configuration helper applications, and the presentation is clean and professional.</p>
<p>A <i>Start Here</i> icon on the desktop guides the user toward three locations: an <i>Applications</i> folder, the <i>Preferences</i> panel, and <i>System Settings</i>. A red hat icon marks the applications menu, and the lower GNOME panel holds all the latest Linux application launchers, including the OpenOffice office suite and the Mozilla browser.</p>
<h2>The future</h2>
<p>By the early 2000s, it's clear that Linux has well and truly hit its stride. The desktop is more polished than ever, the applications available want for nothing, the installation is easier and more efficient than other operating systems. In fact, from the early 2000s onward, the relationship between the user and the system is firmly established and remains basically unchanged even today. There are some changes, and of course several updates and improvements and a staggering amount of innovation.</p>
<p>Project names come and go:</p>
<ul><li>Mandrake became Mandriva and then <a href="http://mageia.org" target="_blank">Mageia</a>;</li>
<li>Fedora Core became just <a href="http://fedoraproject.org" target="_blank">Fedora</a>;</li>
<li><a href="http://ubuntu.com" target="_blank">Ubuntu</a> popped up from <a href="http://debian.org" target="_blank">Debian</a> and helped make "Linux" a household term;</li>
<li>Valve has made <a href="http://store.steampowered.com/steamos" target="_blank">SteamOS</a> the official basis for its gaming platform; and</li>
<li><a href="http://slackware.com" target="_blank">Slackware</a> quietly continues to this day.</li>
</ul><p>Whether you're new to Linux, or whether you're such an old hand that most of these screenshots have been more biographical than historical, it's good to be able to look back at how one of the largest open source projects in the world has developed. More importantly, it's exciting to think of where Linux is headed and how we can all be a part of that, starting now, and for years to come.</p>
