<p>Automation is the great IT and DevOps ideal, but in my experience, anything that's not immediately convenient may as well not exist at all. There have been many times when I've come up with a pretty good solution for some task, and I'll even script it, but I stop short of making it literally automated because the infrastructure for easy automation doesn't exist on the machine I'm working on.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on Ansible</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/downloads/ansible-quickstart?intcmp=701f2000000h4RcAAI">A quickstart guide to Ansible</a></li>
<li><a href="https://opensource.com/downloads/ansible-cheat-sheet?intcmp=701f2000000h4RcAAI">Ansible cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/do007-ansible-essentials-simplicity-automation-technical-overview?intcmp=701f2000000h4RcAAI">Free online course: Ansible essentials</a></li>
<li><a href="https://docs.ansible.com/ansible/latest/intro_installation.html?intcmp=701f2000000h4RcAAI">Download and install Ansible</a></li>
<li><a href="https://go.redhat.com/automated-enterprise-ebook-20180920?intcmp=701f2000000h4RcAAI">eBook: The automated enterprise</a></li>
<li><a href="https://www.ansible.com/resources/ebooks/ansible-for-devops?intcmp=701f2000000h4RcAAI">eBook: Ansible for DevOps</a></li>
<li><a href="https://www.ansible.com/resources/ebooks?intcmp=701f2000000h4RcAAI">Free Ansible eBooks</a></li>
<li><a href="https://opensource.com/tags/ansible?intcmp=701f2000000h4RcAAI">Latest Ansible articles</a></li>
</ul></div>
</div>
</div>
</div>

<p>My favorite easy automation tool used to be the cron system—old, reliable, user-facing, and (aside from a scheduling syntax I can never commit to memory) simple. However, the problem with cron is that it assumes a computer is on 24 hours a day, every day. After missing one too many scheduled backups, I discovered <a href="https://opensource.com/article/21/2/linux-automation">anacron</a>, the cron system based on timestamps rather than scheduled times. If your computer is off when a job would typically have run, anacron ensures that it's run when the computer is back on. Creating a job is as easy as dropping a shell script into one of three directories: <code>cron.daily</code>, <code>cron.weekly</code>, or <code>cron.monthly</code> (you can define more if you want). With anacron, I find myself dropping scripts and Ansible playbooks into place for all manner of trivial tasks, including pop-up reminders of upcoming due dates or events.</p>
<p>It's a simple and obvious solution to a modern problem, but it does me no good if anacron isn't installed on the computer.</p>
<h2>Software setup with Ansible</h2>
<p>Any time I set up a new computer, whether it's a laptop, workstation, or server, I install anacron. That's easy, but an anacron install only provides the anacron command. It doesn't set up the anacron user environment. So I created an Ansible playbook to set up what the user needs to use anacron and install the anacron command.</p>
<p>First, the standard Ansible boilerplate:</p>
<pre><code class="language-yaml">---
- hosts: localhost
  tasks:</code></pre><h2>Creating directories with Ansible</h2>
<p>Next, I create the directory tree I use for anacron. You can think of this as a sort of transparent crontab.</p>
<pre><code class="language-yaml">    - name: create directory tree
      ansible.builtin.file:
        path: "{{ item }}"
        state: directory
      with_items:
        - '~/.local/etc/cron.daily'
        - '~/.local/etc/cron.weekly'
        - '~/.local/etc/cron.monthly'
        - '~/.var/spool/anacron'</code></pre><p>The syntax of this might seem a little strange, but it's actually a loop. The <code>with_items:</code> directive defines four directories to create, and Ansible iterates over the <code>ansible.builtin.file:</code> directive once for each directory (the directory name populates the <code>{{ item }}</code> variable). As with everything in Ansible, there's no error or conflict if the directory already exists.</p>
<h2>Copying files with Ansible</h2>
<p>The <code>ansible.builtin.copy</code> module copies files from one location to another. For this to work, I needed to create a file called <code>anacrontab</code>. It's not an Ansible playbook, so I keep it in my <code>~/Ansible/data</code> directory, where I keep support files for my playbooks.</p>
<pre><code class="language-yaml">    - name: copy anacrontab into place
      ansible.builtin.copy:
        src: ~/Ansible/data/anacrontab
        dest: ~/.local/etc/anacrontab
        mode: '0755'</code></pre><p>My <code>anacrontab</code> file is simple and mimics the one some distributions install by default into <code>/etc/anacron</code>:</p>
<pre><code class="language-bash">SHELL=/bin/sh
PATH=/sbin:/bin:/usr/sbin:/usr/bin
1  0  cron.day    run-parts $HOME/.local/etc/cron.daily/
7  0  cron.wek    run-parts $HOME/.local/etc/cron.weekly/
30 0  cron.mon    run-parts $HOME/.local/etc/cron.monthly/</code></pre><h2>Running anacron on login</h2>
<p>Most Linux distributions configure anacron to read jobs from <code>/etc/anacron</code>. I mostly use anacron as a regular user, so I launch anacron from my login <code>~/.profile</code>. I don't want to have to remember to configure that myself, so I have Ansible do it. I use the <code>ansible.builtin.lineinfile</code> module, which creates <code>~/.profile</code> if it doesn't already exist and inserts the anacron launch line.</p>
<pre><code class="language-yaml">    - name: add local anacrontab to .profile
      ansible.builtin.lineinfile:
        path: ~/.profile
        regexp: '^/usr/sbin/anacron'
        line: '/usr/sbin/anacron -t ~/.local/etc/anacrontab'
        create: true</code></pre><h2>Installing anacron with Ansible</h2>
<p>For most of my systems, the <code>dnf</code> module would work for package installation, but my workstation runs Slackware (which uses <code>slackpkg</code>), and sometimes a different Linux distro makes its way into my collection. The <code>ansible.builtin.package</code> module provides a generic interface to package installation, so I use it for this playbook. Luckily, I haven't come across a repo that names <code>anacron</code> anything but <code>anacron</code>, so for now, I don't have to account for potential differences in package names.</p>
<p>This is actually a separate play because package installation requires privilege escalation, provided by the <code>becomes: true</code> directive.</p>
<pre><code class="language-yaml">- hosts: localhost
  become: true
  tasks:
    - name: install anacron
      ansible.builtin.package:
        name: anacron
        state: present</code></pre><h2>Using anacron and Ansible for easy automation</h2>
<p>To install anacron with Ansible, I run the playbook:</p>
<pre><code class="language-bash">$ ansible-playbook ~/Ansible/setup-anacron.yaml</code></pre><p>From then on, I can write shell scripts to perform some trivial but repetitive task and copy it into <code>~/.local/etc/cron.daily</code> to have it automatically run once a day (or thereabouts). I also write Ansible playbooks for tasks such as <a href="https://opensource.com/article/21/9/keep-folders-tidy-ansible">cleaning out my downloads folder</a>. I place my playbooks in <code>~/Ansible</code>, which is where I keep my Ansible plays, and then create a shell script in <code>~/.local/etc/cron.daily</code> to execute the play. It's easy, painless, and quickly becomes second nature.</p>
