<p>Working with remote repositories gets confusing when the names of the remote repositories in your local Git repo are inconsistent.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on Git</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/resources/what-is-git?intcmp=7013a000002CxqLAAS">What is Git?</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-git?intcmp=7013a000002CxqLAAS">Git cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-markdown?intcmp=7013a000002CxqLAAS">Markdown cheat sheet</a></li>
<li><a href="https://opensource.com/tags/git?intcmp=7013a000002CxqLAAS">New Git articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>One approach to solving this issue is to standardize the use and meaning of two words: <code>origin</code>, referring to your personal <code>example.com/&lt;USER&gt;/*</code> repos, and <code>upstream</code>, referring to the <code>example.com</code> repo from which you forked the <code>origin</code> repo. In other words, <code>upstream</code> refers to the upstream repo where work is publicly submitted, while <code>origin</code> refers to your local fork of the upstream repo from which you generate pull requests (PRs), for example.</p>
<p>Using the <a href="https://github.com/distributed-system-analysis/pbench" target="_blank">pbench</a> repo as an example, here is a step-by-step approach to set up a new local clone with <code>origin</code> and <code>upstream</code> defined consistently.</p>
<ol><li>
<p>On most Git hosting services, you must fork a project when you want to work on it. When you run your own Git server, that's not necessary, but for a codebase that's open to the public, it's an easy way to transfer diffs among contributors.</p>
<p>Create a fork of a Git repository. For this example, assume your fork is located at <code>example.com/&lt;USER&gt;/pbench</code>.</p>
</li>
<li>
<p>Next, you must obtain a Uniform Resource Identifier (<a href="https://en.wikipedia.org/wiki/Uniform_Resource_Identifier" target="_blank">URI</a>) for cloning over SSH. On most Git hosting services, such as GitLab or GitHub, it's in a button or panel labeled <strong>Clone</strong> or <strong>Clone over SSH</strong>. Copy the clone URI to your clipboard.</p>
</li>
<li>
<p>On your development system, clone the repo using the text you copied:</p>
<pre><code class="language-bash">$ git clone git@example.com:&lt;USER&gt;/pbench.git</code></pre><p>This clones the Git repository with the default name <code>origin</code> for your forked copy of the pbench repo.</p>
</li>
<li>
<p>Change directory to the repo you just cloned:</p>
<pre><code class="language-bash">$ cd ~/pbench</code></pre></li>
<li>
<p>Next, obtain the SSH URI of the source repo (the one you originally forked). This is probably done the same way as above: Find the <strong>Clone</strong> button or panel and copy the clone address. In software development, this is typically referred to as "upstream" because (in theory) this is where most commits happen, and you intend to let those commits flow downstream into your copy of the repository.</p>
</li>
<li>
<p>Add the URI to your local copy of the repository. Yes, there will be <em>two different</em> remotes assigned to your local copy of the repository:</p>
<pre><code class="language-bash">$ git remote add upstream git@example.com:bigproject/pbench.git</code></pre></li>
<li>
<p>You now have two named remote repos: <code>origin</code> and <code>upstream</code>. You can see your remote repos with the remote subcommand:</p>
<pre><code class="language-bash">$ git remote -v</code></pre><p>Right now, your local <code>master</code> branch is tracking the <code>origin</code> master, which is not necessarily what you want. You probably want to track the <code>upstream</code> version of this branch because upstream is where most development takes place. The idea is that you are adding your changes on top of whatever you get from upstream.</p>
</li>
<li>
<p>Change your local master branch to track <code>upstream/master</code>:</p>
<pre><code class="language-bash">$ git fetch upstream
$ git branch --set-upstream-to=upstream/master master</code></pre><p>You can do this for any branch you want, not just <code>master</code>. For instance, some projects use a <code>dev</code> branch for all unstable changes, reserving <code>master</code> for code approved for release.</p>
</li>
<li>
<p>Once you've set your tracking branches, be sure to <code>rebase</code> your master to bring it up to date to any new changes made to the upstream repo:</p>
<pre><code class="language-bash">$ git remote update
$ git checkout master
$ git rebase</code></pre></li>
</ol><p>This is a great way to keep your Git repositories synchronized between forks. If you want to automate this, read Seth Kenlon's article on <a href="https://opensource.com/article/19/11/how-host-github-gitlab-ansible">cohosting Git repositories with Ansible</a> to learn how.</p>
