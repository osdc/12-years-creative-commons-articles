<p>Case studies about open source project participants and users are a great way to showcase your project and how it works in the real world.</p>
<p>Such studies will highlight interesting features of your software, demonstrate different (and potentially unique) ways your project is in use, and foster positive communication among members of your community.</p>
<!--break--><p>Case studies are also about transparency: while talking to the end user of your software, you can also learn about things that are not necessarily running smoothly in your project. And although no one loves to hear about the things that are going wrong, such feedback can also be invaluable to you and your team.</p>
<section class="post-content entry-content"><p>In this article, I'll explain how to create an open source software project case study.</p>
<h2 id="general-guidelines">Three basic steps</h2>
<p>Putting together a case study is not terribly difficult, and should (depending on your writing style) only take about two hours of time total, including the actual interview time. The three basic steps are:</p>
<ul><li>Finding a case study subject</li>
<li>Interviewing the case study subject</li>
<li>Writing the case study</li>
</ul><h3 id="finding-a-case-study-subject">Finding a case study subject</h3>
<p>Often, locating a potential subject for a case study is the hardest part of any case study project. This is often due to discovering potential subjects and then (if you find someone) getting them to agree to participate.</p>
<p>Ways to locate subjects include:</p>
<ul><li>
<p><strong>Monitor your channels for particularly active end-users. </strong>Typically, if someone is spending a lot of time on your mailing list, IRC channel, or forums seeking answers to their questions, then it is likely they have a large stake in ensuring your project works in their environment. Many times, that means a production-level deployment, which is what you are seeking.</p>
</li>
<li>
<p><strong>Monitor your channels for new end-users. </strong>If you see anyone new in your channels, check their domain name and use that to learn more about where they might be deploying your project.</p>
</li>
<li>
<p><strong>Introduce yourself. </strong>Reach out to these users. Find out why they are using your software, see if they need specific assistance, and (if they do) try to coordinate their getting in touch with the right person on your team to help them. Along the way, you can learn more about the type of deployment they have (personal or business).</p>
</li>
<li>
<p><strong>Invite them to participate. </strong>You should have a standard invitation email/message to use for case study participation. The invitation should include:</p>
</li>
<ul><li><span style="font-size: 13.0080003738403px; line-height: 1.538em; background-color: transparent;">A clear introduction to you and your project if you have had no prior contact</span></li>
<li><span style="font-size: 13.0080003738403px; line-height: 1.538em; background-color: transparent;">A straightforward request to interview them for the case study</span></li>
<li><span style="font-size: 13.0080003738403px; line-height: 1.538em; background-color: transparent;">An outline of what you will need (length of interview, method of communication, time frame of case study)</span></li>
<li><span style="font-size: 13.0080003738403px; line-height: 1.538em; background-color: transparent;">Details of where the case study will be published</span></li>
<li><span style="font-size: 13.0080003738403px; line-height: 1.538em; background-color: transparent;">A statement that says they will have final review approval of the case study before it is published</span></li>
</ul></ul><p>This last point is important. When a journalist writes an article about a person or company, they rarely–if ever–grant final review access to the article. This keeps the article from becoming little more than a positive public relations piece instead of a balanced piece of work. For a case study, however, this kind of review is not a problem. Indeed, it can help assuage any nervousness the case study subject might have about discussing too much about their organization's infrastructure.</p>
<h3 id="conducting-the-interview">Conducting the interview</h3>
<p>Once the invitation is accepted, be sure to follow up with the interview subject and work with them to arrange an interview time that is convenient to their schedule. They are doing you a great service by agreeing to participate, and they should be treated with courtesy.</p>
<p>When you schedule the interview, 30 minutes should be fine. Try, if you can, to schedule 30 minutes, but have space on the back end in case it goes long.</p>
<p>Video chats are best, if it can be arranged. (Skype, Google Hangouts, BlueJeans are all recommended.) Phone calls also work well, though are more inclined for interruptions in the conversation.</p>
<p>If you are a great note taker, then do that. But for most people, have a recording made of the conversation. Most smart phones have recording apps. You're not looking for high fidelity, just something to go back and review quotes and important points in the conversation.</p>
<p>There is no set formula for interview questions, as any conversation can take a sharp detour into really interesting threads that you can pursue. But to keep things on track, generally try to get these topics covered for your case study:</p>
<ul><li>History of the organization</li>
<li>Events leading up to the introduction of the project</li>
<li>Implementation/deployment of the project</li>
<li>Value of the project (SEO, perceived value)</li>
<li>Technical details (hardware used, number of users. etc.)</li>
<li>Specific use cases (if applicable)</li>
<li>Relations with the community</li>
</ul><p>In all of these questions, try to get a balanced view of the topics. Find out if there were any problems along the way. Let your case study subject air any grievances, if any. You may not necessarily include them in the case study, but you can at least take them back to the upstream community as something to improve.</p>
<p>One thing to avoid, unless the subject absolutely insists on this, is sending them a set of questions and having them write out the answers. Very occasionally some people will request this method of interview, but typically that's a lot of work to put on a person and invariably they will take a very long time to reply with their answers.</p>
<h3 id="writing-the-next-great-case-study">Writing the next great case study</h3>
<p>Pulling together your interview materials into a usable case study is at once pretty straightforward and the most daunting. You have all of the facts and information, but how to write the case study well?</p>
<p>Everyone's got their own style of writing, so this is a very subjective task. One recommendation that might help is try to remember a point in the interview where you said to yourself "<em>that's </em>really interesting." It could be something obscure, but as long as it personally resonates with you then it becomes much easier to convey that personal connection to readers. Build on that point. Lead with this in your case study and make it the hook that attracts people to start reading the article.</p>
<p>As you write the study, always keep in mind that you are telling a story. Yes, it is rife with technical details and IT jargon, but that does not mean it has to be dry. Be conversational, and hit all the points you want to make.</p>
<p>One word of caution: when you do let the case study subject review the draft of the study, be sure not to let them add blatantly marketing-oriented phrases like, "the number one maker of widgets" after their company name. Even innocuous phrases like "an industry leader" are self-serving and can skew the effect of the entire study. If you are patient and explain that the study alone is worth some positive press on its own, most case study subjects will be understanding.</p>
<h2 id="post-case-study-tasks">Post-case study tasks</h2>
<p>Once the case study is complete, reviewed, and posted on the appropriate platform, be sure to share it with your community directly, as well as through any social media channels your and your community might have. Of course, now is not the time to rest too long on your success. Given the length of time it can take to locate case studies, you may need to start looking for another case study candidate soon.</p>
<p>The work will be worth it. Case studies are a great way to highlight the success of your software project and share some attention with community members. It's a win for everyone, with only a small investment of time.</p>
</section><footer class="post-meta"></footer><hr /><p><em>This article originally appeared on <a href="http://community.redhat.com/">community.redhat.com</a>. Follow the community on Twitter at <a href="https://twitter.com/redhatopen">@redhatopen</a>, and find us on <a href="https://www.facebook.com/redhatopen">Facebook</a> and <a href="https://plus.google.com/u/0/b/113258037797946990391/113258037797946990391/posts">Google+</a>.</em></p>
