<p>Blockchains are big news at the moment. There are conferences, startups, exhibitions, open source projects (in fact, pretty much all of the blockchain stuff going on out there is open source—look at Ethereum, Zcash, and Bitcoin as examples); all we need now are hipster-run blockchain-themed cafés.<a href="#1"><sup>1</sup></a> If you're looking for an initial overview, you could do worse than the <a href="https://en.wikipedia.org/wiki/Blockchain" target="_blank">Wikipedia entry</a>—but that's not the aim of this post.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on security</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/articles/defensive-coding-guide/?intcmp=70160000000h1s6AAA">The defensive coding guide</a></li>
<li><a href="https://www.redhat.com/en/events/webinar/automating-system-security-and-compliance-standard-operating-system?intcmp=70160000000h1s6AAA">Webinar: Automating system security and compliance with a standard operating system</a></li>
<li><a href="https://www.redhat.com/en/resources/container-security-openshift-cloud-devops-whitepaper?intcmp=70160000000h1s6AAA">10 layers of Linux container security </a></li>
<li><a href="https://developers.redhat.com/books/selinux-coloring-book?intcmp=70160000000h1s6AAA">SELinux coloring book</a></li>
<li><a href="https://opensource.com/tags/security?intcmp=70160000000h1s6AAA">More security articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>Before we go much further, one useful thing to know about many blockchain projects is that they aren't. Blockchains, that is. They are, more accurately, distributed ledgers.<a href="#4"><sup>4</sup></a> For now, however, let's roll in blockchain and distributed ledger technologies and assume we're talking about the same thing: it'll make it easier for now, and in most cases, the difference is immaterial for our discussion.</p>
<p>I'm not planning to go into the basics here, but we should briefly talk about the main link with crypto and blockchains, and that's the blocks themselves. To build a block—a set of transactions to put into a blockchain—and then link it into the blockchain, cryptographic hashes are used. This is the most obvious relationship that the various blockchains have with cryptography.</p>
<p>There's another, equally important one, however, which is about identity.<a href="#5"><sup>5</sup></a> Now, for many blockchain-based crypto-currencies, a major part of the point of using them at all is that identity isn't, at one level, important. There are many actors in a cryptocurrency who may be passing each other vanishingly small or eye-wateringly big amounts of money, and they don't need to know each other's identity in order to make transactions.</p>
<p>To be clearer, the <em>uniqueness</em> of each actor absolutely <em>is</em> important—I want to be sure that I'm sending money to the entity who has just rendered me a service—but being able to tie that unique identity to a particular person IRL<a href="#6"><sup>6</sup></a> is <em>not</em> required. To use the technical term, such a system is pseudonymous. Now, if pseudonymity is a key part of the system, then protecting that property is likely to be important to its users. Cryptocurrencies do this with various degrees of success. The lesson here is that you should do some serious reading and research if you're planning to use a cryptocurrency and this property matters to you.</p>
<p>On the other hand, there are many blockchain/distributed ledger technologies where pseudonymity is not a required property and may actually be unwanted. These are the types of systems in which I am most generally interested from a professional point of view.</p>
<p>In particular, I'm interested in permissioned blockchains. Permissionless (or non-permissioned) blockchains are those where you don't need permission from anyone in order to participate. You can see why pseudonymity and permissionless blockchains can fit well today: most (all?) cryptocurrencies are permissionless. Permissioned blockchains are a different kettle of fish, however, and they're the ones many businesses are looking at now. In these cases, you know the people or entities who are going to be participating—or, if you don't know now, you'll want to check on them and their identity before they join your blockchain (or distributed ledger). And here's why blockchains are interesting in business.<a href="#7"><sup>7</sup></a> It's not just that identity is interesting, although it is, because how you marry a particular entity to an identity and make sure that this binding is not spoofable over the lifetime of the system is <em>difficult, difficult, lemon difficult</em><a href="#8"><sup>8</sup></a>—but there's more to it than that.</p>
<p>What's really interesting is that, if you're thinking about moving to a permissioned blockchain or distributed ledger with permissioned actors, then you're going to have to spend some time thinking about <a href="https://aliceevebob.wordpress.com/2017/05/09/what-is-trust-with-apologies-to-pontius-pilate/" target="_blank">trust</a>. You're unlikely to be using a proof-of-work system for making blocks—there's little point in a permissioned system—so who decides what comprises a "valid" block that the rest of the system should agree on? Well, you can rotate around some (or all) of the entities, or you can have a random choice, or you can elect a small number of über-trusted entities. Combinations of these schemes may also work.</p>
<p>If these entities all exist within one trust domain, which you control, then fine, but what if they're distributors, or customers, or partners, or other banks, or manufacturers, or semi-autonomous drones, or vehicles in a commercial fleet? You really need to ensure that the trust relationships that you're encoding into your implementation/deployment truly reflect the legal and IRL trust relationships that you have with the entities that are being represented in your system.</p>
<p>And the problem is that, once you've deployed that system, it's likely to be very difficult to backtrack, adjust, or reset the trust relationships that you've designed. And if you don't think about the questions I noted above, about long-term bindings of identity, you're going to be in for some serious problems when, for instance:</p>
<ul><li>an entity is spoofed</li>
<li>an entity goes bankrupt</li>
<li>an entity is acquired by another entity (buyouts, acquisitions, mergers, etc.)</li>
<li>an entity moves into a different jurisdiction</li>
<li>a legislation or regulation changes.</li>
</ul><p>These are all issues that are well catered for within existing legal frameworks (with the possible exception of the first), but that are more difficult to manage within the sorts of systems we are generally concerned with in this article.</p>
<p>Please don't confuse the issues noted above with the questions around how to map legal agreements to the so-called "smart contracts" in blockchain/distributed ledger systems. That's another thorny (and, to be honest, not unconnected) issue, but this one goes right to the heart of what a system <em>is</em>, and it's the reason that people need to think very hard about what they're really trying to achieve when they adopt the latest buzzword technology. Yet again, we need to understand how systems and the business work together and be honest about the fit.</p>
<div align="center">
<hr align="center" noshade="noshade" size="1" width="100%" /></div>
<p><a id="1" name="1">1</a> If you come across one of these, please let me know. Put a picture in a comment or something.<a href="#2"><sup>2</sup></a></p>
<p><a id="2" name="2">2</a> Even better—start one yourself. Make sure I get an invitation to the opening.<a href="#3"><sup>3</sup></a></p>
<p><a id="3" name="3">3</a> And free everything.</p>
<p><a id="4" name="4">4</a> There have been online spats about this. I'm not joining in.</p>
<p><a id="5" name="5">5</a> There are others, but I'll save those for another day.</p>
<p><a id="6" name="6">6</a> IRL = "in real life." I'm so old-skool.</p>
<p><a id="7" name="7">7</a> For me. If you've got this far into the article, I'm hoping there's an even chance that the same will go for you, too.</p>
<p><a id="8" name="8">8</a> I'll leave this as an exercise for the reader. Watch it, though, and the <a href="https://en.wikipedia.org/wiki/In_the_Loop" target="_blank">TV series</a> on which it's based. Unless you don't like swearing, in which case <em>don't</em> watch either.</p>
<p><em>This article originally appeared on <a href="https://aliceevebob.com/2017/06/13/is-blockchain-a-security-topic/" target="_blank">Alice, Eve, and Bob—a security blog</a> and is republished with permission.</em></p>
