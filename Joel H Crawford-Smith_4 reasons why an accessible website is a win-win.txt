<p>Why do some people choose to make a website accessible? Some people are do-gooders who, like the World Wide Web Consortium (<a href="https://www.w3.org/standards/webdesign/accessibility" target="_blank">W3C</a>), believe that "the web is fundamentally designed to work for all people, whatever their hardware, software, language, culture, location, or physical or mental ability." And, some people do it because they are compelled by law, based on Section 508 of the Americans with Disabilities act. Most federal and state institutions require that websites are accessible to <a href="https://opensource.com/business/16/6/conference-accessibility-tips">people with a variety of disabilities</a>. Though they may want to do good, their main motivation is to avoid costly legal problems.</p>
<p>No matter what your motivation is, everyone benefits from creating accessible websites. Four unexpected benefits of creating accessible websites include:</p>
<ol><li>Improved search engine optimization (SEO)</li>
<li>Better user experience for all visitors and all devices</li>
<li>Avoiding costly and embarrassing lawsuits</li>
<li>Increasing the audience for your website by making it more inclusive</li>
</ol><h2>SEO</h2>
<p><a href="https://www.w3.org/TR/UNDERSTANDING-WCAG20/navigation-mechanisms-refs.html" target="_blank">The W3C Link Purpose</a> document describes how to properly label URLs: "The purpose of each link can be determined from the link text alone or from the link text together with its programmatically determined link context..." In short, links like this are not accessible:</p>
<ul><li>"More"</li>
<li>"Read more"</li>
<li>"Learn more"</li>
<li>"Click here"</li>
</ul><p>Just like you, people who use screen readers browse a page by looking at the headlines and links. As they tab through the site it reads those links to them. If the computer reads "More" the user asks, "More what?" There is no context to the link's purpose. Suggestions for better links:</p>
<ul><li>"More news"</li>
<li>"Read full article"</li>
<li>"Learn more about open source"</li>
<li>"Click here to read more about Linux containers"</li>
</ul><p>What does this have to do with SEO? Keywords in links are more valuable than plain text. For example, with "Learn More About <strong>Linux containers</strong>" you just added two keywords to your content. Good job! Bad link text is a missed SEO opportunity.</p>
<h2>User experience</h2>
<p>Chances are you have experienced a disability today. We experience temporary artificial disabilities on a daily basis, and the environments we are in change our abilities.</p>
<p>The devices that you browse the web on also have a variety of abilities for disabilities.</p>
<table cellpadding="7" cellspacing="0" target="_blank" width="590"><colgroup><col width="182" /><col width="183" /><col width="182" /></colgroup><tbody><tr target="_blank" valign="top"><td style="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in" target="_blank" width="182">
<p><b>Disability</b></p>
</td>
<td style="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in" target="_blank" width="183">
<p><b>Simulation</b></p>
</td>
<td style="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in" target="_blank" width="182">
<p style="text-indent: 0.5in" target="_blank"><b>Solution</b></p>
</td>
</tr><tr target="_blank" valign="top"><td style="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in" target="_blank" width="182">
<p>Impaired vision</p>
</td>
<td style="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in" target="_blank" width="183">
<p>Use a phone in bright light</p>
</td>
<td style="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in" target="_blank" width="182">
<p>Use higher contrast ratios on text</p>
</td>
</tr><tr target="_blank" valign="top"><td style="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in" target="_blank" width="182">
<p>Tremors / Arthritis</p>
</td>
<td style="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in" target="_blank" width="183">
<p>Use a phone while walking</p>
</td>
<td style="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in" target="_blank" width="182">
<p>Make touch targets like buttons larger</p>
</td>
</tr><tr target="_blank" valign="top"><td style="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in" target="_blank" width="182">
<p>Hearing loss</p>
</td>
<td style="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in" target="_blank" width="183">
<p>Use a phone in a noisy rooms</p>
</td>
<td style="border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in" target="_blank" width="182">
<p>Put captions on videos</p>
</td>
</tr></tbody></table><p>On a day-to-day basis, even hour-by-hour, a person's abilities and disabilities change on a spectrum. Some people are always blind or have limited sight. Perhaps you need glasses. Perhaps you have 20-20 vision. We all live on this spectrum.</p>
<p class="rtecenter"><img alt="Graphic demonstrating that all people live on a continuum of abilities and disabilities" class="media-image attr__typeof__foaf:Image img__fid__326466 img__view_mode__default attr__format__default attr__field_file_image_alt_text[und][0][value]__Graphic demonstrating that all people live on a continuum of abilities and disabilities attr__field_file_image_title_text[und][0][value]__Graphic attr__field_file_image_caption[und][0][value]__&amp;lt;p&amp;gt;Image by Joel Crawford-Smith, CC BY-SA 4.0&amp;lt;/p&amp;gt; attr__field_file_image_caption[und][0][format]__panopoly_wysiwyg_text attr__field_folder[und]__9402" src="https://opensource.com/sites/default/files/disability-spectrum.jpg" style="width: 520px; height: 347px;" title="Graphic demonstrating that all people live on a continuum of abilities and disabilities" typeof="foaf:Image" width="520" height="347" /></p>
<h2>Legal</h2>
<p>Harvard and MIT learned about the legal ramifications <a href="http://www.3playmedia.com/2016/02/23/judge-denies-motion-to-dismiss-web-accessibility-lawsuit-against-harvard-mit/" target="_blank">the hard way</a> when they were sued by the National Association for the Deaf (NAD).</p>
<blockquote><p>The fear is that as we move into the digital age, we're faced with a two-edged sword because in some cases, of course, technology can make the world more accessible to individuals with sensory impairments or disabilities. But in some cases, it can prove to raise new barriers and re-segregate our society. And that's why this issue is being so heavily litigated by individuals who fear that they're going to end up on the wrong side of the digital divide.</p>
</blockquote>
<p>Being on the wrong side of this kind of litigation is far more expensive then dealing with it proactively, and this is not desirable publicity.</p>
<h2>Audience</h2>
<p>People prefer websites they can use. Half of people over the age of 40 start to experience presbyopia, which is normal age-related loss of the near-focusing ability that leads to the need for reading glasses or bifocals. Over 48 million Americans are deaf or hard of hearing. Over 7 million Americans are blind or have low vision. By making a website accessible it increases your audience to include those people.</p>
<h2>How to get started</h2>
<p>All the resources you need are open, and you can start improving today.</p>
<p>Start by testing your website for accessibility using the <a href="http://wave.webaim.org/" target="_blank">WebAIM Web Accessibility Evaluation Tool</a>. It will point out problems as well as explain the success criteria to make your site pass. There is a free online version as well as a free Chrome extention.</p>
<p>Next, read these resources:</p>
<ul><li><a href="http://webaim.org/intro/" target="_blank">Introduction to Web Accessibility</a></li>
<li><a href="http://webaim.org/resources/designers/" target="_blank">Web Accessibility for Designers</a></li>
<li><a href="https://www.w3.org/WAI/gettingstarted/Overview.html" target="_blank">Getting Started with Web Accessibility from the W3C</a></li>
</ul><p>I hope you will choose to keep accessibility in mind on your current and future projects.</p>
