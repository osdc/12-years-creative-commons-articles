<p>In <a href="https://opensource.com/article/20/11/javascript-popular"><em>4 reasons why JavaScript is so popular</em></a>, I touched on a few advanced JavaScript concepts. In this article, I will dive into one of them: closures.</p>
<p>According to <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Closures" target="_blank">Mozilla Developer Network</a> (MDN), "A closure is the combination of a function bundled together (enclosed) with references to its surrounding state (the lexical environment)." Simplified, this means that a function inside another function can access the variables from the outer (parent) function.</p>
<p>To better understand closures, take a look at scopes and their execution context.</p>
<p>Here is a simple code snippet:</p>
<pre><code class="language-javascript">var hello = "Hello";

function sayHelloWorld() {
var world = "World";
	function wish() {
		var year = "2021";
		console.log(hello + " " + world + " "+ year);
}
wish();
}
sayHelloWorld();</code></pre><p>Here's the execution context for this code: </p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Execution context for JS code"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/execution-context.png" width="664" height="556" alt="Execution context for JS code" title="Execution context for JS code" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Nimisha Mukherjee, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>Closures are created every time a function is created (at function-creation time). Every closure has three scopes:</p>
<ul><li>Local scope (own scope)</li>
<li>Outer functions scope</li>
<li>Global scope</li>
</ul><p>I'll modify the above code slightly to demonstrate closure:</p>
<pre><code class="language-javascript">var hello = "Hello";

var sayHelloWorld = function() {
var world = "World";
	function wish() {
		var year = "2021";
		console.log(hello + " " + world + " "+ year);
}
return wish;
}
var callFunc = sayHelloWorld();
callFunc();</code></pre><p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Programming and development</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ&amp;src=programming_resource_menu4">Red Hat Developers Blog</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Programming cheat sheets</a></li>
<li><a href="https://www.redhat.com/en/services/training/learning-subscription?intcmp=7016000000127cYAAQ">Try for free: Red Hat Learning Subscription</a></li>
<li><a href="https://opensource.com/downloads/bash-programming-guide?intcmp=7016000000127cYAAQ">eBook: An introduction to programming with Bash</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/bash-shell-cheat-sheet?intcmp=7016000000127cYAAQ">Bash Shell Scripting Cheat Sheet</a></li>
<li><a href="https://developers.redhat.com/e-books/modernizing-enterprise-java?intcmp=7016000000127cYAAQ">eBook: Modernizing Enterprise Java</a></li>
</ul></div>
</div>
</div>
</div>
<p>The inner function <code>wish()</code> is returned from the outer function before it's executed. This happens because functions in JavaScript form <strong>closures</strong>.</p>
<ul><li><code>callFunc</code> holds a reference to the function <code>wish</code> when <code>sayHelloWorld</code> runs</li>
<li><code>wish</code> maintains a reference to its surrounding (lexical) environment where the variable <code>world</code> exists.</li>
</ul><h2 id="private-variables-and-methods">Private variables and methods</h2>
<p>Natively, JavaScript does not support the creation of private variables and methods. A common and practical use of closure is to emulate private variables and methods and allow data privacy. Methods defined within the closure scope are privileged.</p>
<p>This code snippet captures how closures are commonly written and used in JavaScript:</p>
<pre><code class="language-javascript">var resourceRecord = function(myName, myAddress) {
 var resourceName = myName;
 var resourceAddress = myAddress;
 var accessRight = "HR";
 return {
   changeName: function(updateName, privilege) {
     //only HR can change the name
     if(privilege === accessRight ) {
       resourceName = updateName;
       return true;
     } else {
       return false;
     }
   },   
   changeAddress: function(newAddress) {
     //any associate can change the address
     resourceAddress = newAddress;          
   },   
   showResourceDetail: function() {
     console.log ("Name:" + resourceName + " ; Address:" + resourceAddress);
   }
 }
}
//Create first record
var resourceRecord1 = resourceRecord("Perry","Office");
//Create second record
var resourceRecord2 = resourceRecord("Emma","Office");
//Change the address on the first record
resourceRecord1.changeAddress("Home");
resourceRecord1.changeName("Perry Berry", "Associate"); //Output is false as only an HR can change the name
resourceRecord2.changeName("Emma Freeman", "HR"); //Output is true as HR changes the name
resourceRecord1.showResourceDetail(); //Output - Name:Perry ; Address:Home
resourceRecord2.showResourceDetail(); //Output - Name:Emma Freeman ; Address:Office</code></pre><p>The resource records (<code>resourceRecord1</code> and <code>resourceRecord2</code>) are independent of one another. Each closure references a different version of the <code>resourceName</code> and <code>resourceAddress</code> variable through its own closure. You can also apply specific rules to how private variables need to be handled—I added a check on who can modify <code>resourceName</code>.</p>
<h2 id="use-closures">Use closures</h2>
<p>Understanding closure is important, as it enables deeper knowledge of how variables and functions relate to one another and how JavaScript code works and executes.</p>
