<p>Small Scale Scrum principles address the preferred approach towards communication, the processes introduced to ensure the highest quality of delivery, and the benefits behind implementing Small Scale Scrum for the business.</p>
<p><a href="https://github.com/agagancarczyk/small-scale-scrum/blob/master/Agnieszka-Gancarczyk-20060828-Final-Dissertation.pdf" target="_blank">Small Scale Scrum principles</a> are non-negotiable.</p>
<p class="rtecenter"><strong><a href="https://opensource.com/downloads/small-scale-scrum" target="_blank">[Download the Introduction to Small Scale Scrum guide]</a></strong></p>
<p> </p>
<article class="align-center media media--type-image media--view-mode-full" title="small-scale-scrum-principles.png"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/small-scale-scrum-principles.png" width="570" height="658" alt="small-scale-scrum-principles.png" title="small-scale-scrum-principles.png" /></div>
      
  </article><p><strong>Value-based communication</strong> includes inner (within the development team) and outer (with the customer) communication. It is focused on delivering a value. Understanding the solution, its purpose, and its desired functionality depends upon effective communication. Initiating and maintaining communication between parties requires openness and dedication to look for the best solution to a given functionality request or fix. It may also lead to further discussions around progress made in software delivered, which can, in turn, uncover omissions in the requirements.</p>
<div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More DevOps resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/resources/devops?intcmp=7013a00000263HlAAI">What is DevOps?</a></li>
<li><a href="https://opensource.com/downloads/devops-hiring-guide?intcmp=7013a00000263HlAAI">The ultimate DevOps hiring guide</a></li>
<li><a href="https://opensource.com/downloads/devops-monitoring-guide?intcmp=7013a00000263HlAAI">DevOps monitoring tools guide</a></li>
<li><a href="https://opensource.com/downloads/devsecops?intcmp=7013a00000263HlAAI">Getting started with DevSecOps</a></li>
<li><a href="https://enterprisersproject.com/cheat-sheet-devops-glossary?intcmp=7013a00000263HlAAI">Download the DevOps glossary</a></li>
<li><a href="https://www.ansible.com/resources/ebooks/ansible-for-devops?intcmp=7013a00000263HlAAI">eBook: Ansible for DevOps</a></li>
<li><a href="https://opensource.com/tags/devops?src=devops_resource_menu2">Latest DevOps articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>With finite time, the focus should be put on valuable and urgent communication as opposed to communication deviating from importance and usefulness. This aligns with the <a href="https://www.eisenhower.me/eisenhower-matrix/" target="_blank">Eisenhower Matrix</a> of making urgent vs. important decisions to help prioritize communication flows.</p>
<p><strong>Quality-first development</strong> focuses on taking a quality approach to software development during each sprint. This means ensuring that features are delivered according to acceptance criteria, the solution is bug-free (or at least free from any evident bugs), any inconsistencies in the software are removed, the solution is tested, and any edge-case bugs and omitted features are reported, logged, and considered by the customer.</p>
<p><strong>Delivery ownership</strong> is about the development team taking initiative in driving software delivery on a sprint-to-sprint basis. Enabling the team to consult the customer directly positively impacts the team’s overall performance and the customer’s satisfaction. Elimination of any micromanagement-related barriers is critical to allow the team to take ownership in delivering software solutions.</p>
<p><strong>Iterative signoff</strong> focuses on reducing technical debt and identifying gaps in requirements through an iterative signoff approach. As the solution evolves and matures, business requirements change. With iterative development, functionality delivered within a sprint should be signed off upon the sprint’s completion. Similarly, requirements for the upcoming sprint should be signed off on before it begins.</p>
<p><i>A related version of this article was originally published on <a href="https://medium.com/@agagancarczyk/small-scale-scrum-principles-fdf8567eb424" target="_blank">Medium</a> and is republished with permission.</i></p>
<p class="rtecenter"><a href="https://opensource.com/downloads/small-scale-scrum" style="display: block; width: 520px; background: #69BE28; padding: 10px; text-align: center; border-radius: 2px; color: white; font-weight: bold; text-decoration: none; text-transform: uppercase;" target="_blank">Download the Introduction to Small Scale Scrum guide</a></p>
