<p>If you've ever tried to collaborate with other authors and editors and the many other people who work to make a book successful, you know it's not easy. Even if your experience stops at trying to incorporate three comments with changes tracked in word processing software, you get the idea. Last week at the O'Reilly Tools of Change conference, a new platform called Booktype was announced. It was created to help you collaborate on editing content and getting it ready for publishing.</p>
<!--break-->
<p><iframe width="560" height="315" src="https://www.youtube.com/embed/1mLCDKmmEyg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe></p>
<p>This <a href="https://github.com/sourcefabric/Booktype/">open source platform</a> has quite a few appealing features for authors:</p>
<ul><li>Books formatted for print, Amazon, iBooks, and most ereaders (formats include pdf, epub, mobi, odt, html)</li>
<li>A web interface that does the table of contents, chapters, page numbers, and formatting for you and allows collaboration</li>
<li>When your'e finished, near-instant publishing to Lulu.com, Amazon, or iBooks</li>
<li>Attribution and license tracking; multiple licenses can be used in one book</li>
<li>Version control </li>
<li>Translation and localization framework</li>
</ul><p>It's based on the FLOSS Manuals platform, which means there are already thousands of contributors in multiple languages either familiar with or already using Booktype.</p>
<ul><li><a href="http://www.sourcefabric.org/en/booktype/">Learn more about Booktype</a></li>
<li><a href="http://booktype-demo.sourcefabric.org/">Try out the demo</a></li>
</ul><p> </p>
