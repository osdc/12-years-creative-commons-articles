<p>Opensource.com community manager Jason Hibbets interviewed Angie Byron, a Drupal core co-maintainer, about the latest version of this open source content management system.</p>
<p><em>What can we expect from Drupal 8?</em></p>
<!--break--><p></p>
<p>Angie also shares tips on community management, gives<span style="letter-spacing: 0px;"> practical advice on getting more minorities into open source, and how parents can get their kids involved in technology.</span></p>
<p>Read a more in-depth interview with Angie Byron in:</p>
<p class="title"><a title="interview by Jason Hibbets" href="https://opensource.com/life/13/10/interview-angie-byron-drupal" target="_blank">Top 5 things Angie Byron loves about Drupal</a></p>
<p><em>Interview was held during the 2013 <a title="complete list of interviews at ATO 2013" href="https://opensource.com/resources/ato2013" target="_blank">All Things Open conference</a> in Raleigh, NC.</em></p>
<p><iframe src="//www.youtube.com/embed/wQsPhGqqkbE" width="520" height="292" frameborder="0"></iframe></p>
