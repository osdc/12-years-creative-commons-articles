<p>In a recent article about how to fail gracefully and why it's necessary to do so, Jen Wike Huger, an editor and a friend of mine on the <a href="https://opensource.com/" target="_blank">Opensource.com</a> staff, wrote <em><a href="https://opensource.com/article/18/10/living-command-line" target="_blank">Living on the command line: Why mistakes are a good thing</a></em>. In addition to discussing the fact that mistakes are inevitable, Wike Huger also discussed responses to those mistakes and the use of postmortems as a means of blamelessly moving forward. She lists five steps for making these meetings successful.</p>
<p>But how do you conduct a postmortem when you are the entire team? This can be a challenge, but I find that my yoga classes have given me a solution. The following excerpt from my book, <em><a href="http://www.both.org/?page_id=903" target="_blank">The Linux Philosophy for SysAdmins</a></em>, describes my process:</p>
<hr /><p>As a student of yoga, the first thing I do when starting my (almost) daily practice, whether in my own little yoga room or in a class, is to find my center. This is time to just be, and to use my mind to explore the physical aspects of my being while opening up to the experience of just existing.</p>
<p>Having done it myself, I suggest that this is an excellent method for exploring our thinking and reasoning as sysadmins. That is not to say I use this technique to solve problems, but rather to explore my own methods for problem-solving.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Great Content</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=7016000000127cYAAQ">Free online course: RHEL technical overview</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=7016000000127cYAAQ">Learn Advanced Linux Commands</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Download Cheat Sheets</a></li>
<li><a href="https://opensource.com/alternatives?intcmp=7016000000127cYAAQ">Find an Open Source Alternative</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=7016000000127cYAAQ">Read Top Linux Content</a></li>
<li><a href="https://opensource.com/resources?intcmp=7016000000127cYAAQ">Check out open source resources</a></li>
</ul></div>
</div>
</div>
</div>
<p>Many times, after solving a problem, particularly a new or especially difficult one, I spend some time just thinking about the problem. I start with the symptoms, my thinking process, and where those symptoms led me. I take time to consider what finally led me to the solution, things I might have done better, and what new things I might want to learn.</p>
<p>It gives me an opportunity to, as an individual, perform what we used to call a “lessons learned” meeting at one of my former places of employment. That was an opportunity to look at what we as a team did right and what we could have done better. The best and yet hardest part now is that I don’t have others to help me understand what I could have done better. That makes it all the more important for me to do this as much as possible.</p>
<p>It is not necessary to practice yoga in order to do this. Just set aside some time, find an empty space where you won’t be interrupted, close your eyes, and contemplate. Breathe, relax, and calm your mind before trying to review the incident. Start from the beginning and think your way through the incident. Review the complete sequence of events and the steps you used to ultimately find the solution. The things you need to know and learn will make themselves clear to you. I find that this form of self-evaluation can be quite powerful.</p>
<p>I also like to take a moment before I start to work on a new problem to center myself. This opens my mind to the possibilities. First, there are the possibilities that represent the likely causes of the problem. Then there are the possibilities that represent the methods and tools I have to locate the causes of the problem. Finally, there are the possibilities that represent the ways that there are to fix the problem.</p>
<hr /><p>You may find other methods for performing this essential self-evaluation. I know some people who write up a report about the incident. I sometimes do this too, to focus my own thoughts and to provide the customer with a report that can explain what happened, especially when things were not going well. With something like this, self-awareness, truth, and openness to your own errors and mistakes are important—especially because there is no one else to point these out.</p>
<p>Self-examination is not easy, but my yoga experience has enabled me to do it with much less angst and a more truthful approach.</p>
