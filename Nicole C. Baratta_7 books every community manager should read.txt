<p>For my very first OSCON session this year, I got to attend the <a href="https://opensource.com/business/15/7/building-better-open-source-communities" target="_blank">Community Management Workshop</a> hosted by Jono Bacon. As an avid reader, the best part of this talk was the list of reading materials Jono provided. If you're interested in managing a great community and/or being a great leader, these materials are worth a look.</p>
<!--break-->
<ol><li><em><a href="http://www.amazon.com/Abundance-Future-Better-Than-Think-ebook/dp/B005FLOGMM/" target="_blank">Abundance: The Future Is Better Than You Think</a></em> by Peter H. Diamandis and Steven Kotler</li>
<li><em><a href="http://www.amazon.com/Predictably-Irrational-Revised-Expanded-Decisions/dp/0061353248" target="_blank">Predictably Irrational: The Hidden Forces That Shape Our Decisions</a></em> by Dan Ariely</li>
<li><em><a href="http://www.your-brain-at-work.com/files/NLJ_SCARFUS.pdf" target="_blank">SCARF: a brain-based model for collaborating with and influencing others</a></em> by David Rock</li>
<li><em><a href="http://www.amazon.com/Habits-Highly-Effective-People-Powerful/dp/1451639619/" target="_blank">The 7 Habits of Highly Effective People: Powerful Lessons in Personal Change</a></em> by Stephen R. Covey</li>
<li><em><a href="http://www.amazon.com/Making-Things-Happen-Mastering-Management/dp/0596517718" target="_blank">Making Things Happen: Mastering Project Management</a></em> by Scott Berkun</li>
<li><em><a href="http://www.amazon.com/Starfish-Spider-Unstoppable-Leaderless-Organizations/dp/1591841836" target="_blank">The Starfish and the Spider: The Unstoppable Power of Leaderless Organizations</a></em> by Ori Brafman and Rod A. Beckstrom</li>
<li><em><a href="http://www.artofcommunityonline.org/" target="_blank">Art of Community</a></em> by Jono Bacon</li>
</ol><p>Do you have a book or resource you'd add to the list?</p>
<p>Have you read any of these and want to share your thoughts? Tell us more in the comments.</p>
<div class="series-wide">OSCON<br /> Series</div>
<p><em>This article is part of the <a href="https://opensource.com/resources/oscon-2015-speaker-interview-collection" target="_blank">OSCON Series</a> for OSCON 2015. <a href="http://www.oscon.com/open-source-2015" target="_blank">OSCON</a> is everything open source—the full stack, with all of the languages, tools, frameworks, and best practices that you use in your work every day. OSCON 2015 will be held July 20-24 in Portland, Oregon.</em>.</p>
