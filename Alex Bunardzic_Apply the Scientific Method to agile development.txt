<blockquote><p>"The only way it’s all going to go according to plan is if you don’t learn anything." —Kent Beck</p>
</blockquote>
<p>Experimentation is the foundation of the scientific method, which is a systematic means of exploring the world around us. But experimentation is not only reserved for the field of scientific research. It has a central place in the world of business too.</p>
<p>Most of us are by now familiar with the business methodology called <a href="https://en.wikipedia.org/wiki/Minimum_viable_product#:~:text=A%20minimum%20viable%20product%20%28MVP,feedback%20for%20future%20product%20development.&amp;text=The%20concept%20can%20be%20used,developments%20of%20an%20existing%20product." target="_blank">Minimum Viable Product (MVP)</a>. This Minimum Viable Product is basically just an experiment. By building and launching MVPs, business operations are engaging in a systematic means of exploring the markets.</p>
<p>If we look at market leaders today, we learn that they’re not doing projects anymore; the only thing they’re doing is experiments. Customer discovery and lean strategies are only used to test assumptions about the markets. Such an approach is equivalent to Test-Driven Development (TDD), which is the process we are intimately familiar with. In TDD, we write the hypothesis (the test) first. We then use that test to guide our implementation. Ultimately, product or service development is no different than TDD—we first write a hypothesis, then that hypothesis guides our implementation, which serves as measurable validation of the hypothesis.</p>
<h2>Information discovery</h2>
<p>Back in the pre-agile days, requirements gathering was an important activity that used to always kick off the project. A bunch of subject matter experts (SMEs) would get assigned to the project and be tasked with gathering the requirements. After a prolonged period of upfront information discovery, the gathered requirements got reviewed and, if agreed upon, signed off and frozen. No more changes allowed!</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Programming and development</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ&amp;src=programming_resource_menu4">Red Hat Developers Blog</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Programming cheat sheets</a></li>
<li><a href="https://www.redhat.com/en/services/training/learning-subscription?intcmp=7016000000127cYAAQ">Try for free: Red Hat Learning Subscription</a></li>
<li><a href="https://opensource.com/downloads/bash-programming-guide?intcmp=7016000000127cYAAQ">eBook: An introduction to programming with Bash</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/bash-shell-cheat-sheet?intcmp=7016000000127cYAAQ">Bash Shell Scripting Cheat Sheet</a></li>
<li><a href="https://developers.redhat.com/e-books/modernizing-enterprise-java?intcmp=7016000000127cYAAQ">eBook: Modernizing Enterprise Java</a></li>
</ul></div>
</div>
</div>
</div>
<p>Back then, it seemed a perfectly reasonable thing to do. The fly in the ointment always showed up once the build phase commenced. Sooner or later, as the project progresses, new information comes to light. Suddenly, what we initially viewed as incontrovertible truth gets challenged by the newly acquired information and evidence.</p>
<p>But the clincher was in the gated phases. Remember, once requirements get signed off, they get frozen—no more changes, no scope creep allowed—which means newly obtained market insights get willfully ignored.</p>
<p>Well, that’s kind of foolish neglect. The newly emerging evidence could be of critical importance to the health of the business operation. Can we afford to ignore it? Absolutely not! We have no choice but to embrace the change.</p>
<p>After a number of prominent fiascos in the industry, many software development projects switched to the agile approach. With agile, information discovery is partial. With agile, we never claim that we have gathered the requirements and are now ready to implement them. We discover information and implement it on an ongoing basis. We do it in tiny steps, keeping our efforts interruptible and steerable at all times.</p>
<h2>How to leverage the scientific method</h2>
<p>The scientific method is empirical and consists of the following steps:</p>
<p>Step 1: Make and record careful observations.</p>
<p>Step 2: Perform orientation with regard to observed evidence.</p>
<p>Step 3: Formulate a hypothesis, including measurable indicators for hypothesis evaluation.</p>
<p>Step 4: Design an experiment that will enable testing of the hypothesis.</p>
<p>Step 5: Conduct the experiment (i.e., release the partial implementation).</p>
<p>Step 6: Collect the telemetry that results from running the experiment.</p>
<p>Step 7: Evaluate the results of the experiment.</p>
<p>Step 8: Accept or reject the hypothesis.</p>
<p>Step 9: Return to Step 1.</p>
<h2>How to formulate a hypothesis</h2>
<p>When switching from project to experiments, the traditional user story framework (As a__I want to__so that__) has proven insufficient. The traditional user story format does not expose the signals needed in order to evaluate the outcomes. Instead, the old school user story format is focused on outputs.</p>
<p>The problem with doing an experiment without first formulating a hypothesis is that there is a danger of introducing a bias when interpreting the results of an experiment. Defining the measurable signals that will enable us to corroborate our hypothesis must be done before we conduct the experiment. That way, we can remain completely impartial when interpreting the results of the experiment. We cannot be swayed by wishful thinking.</p>
<p>The best way to proceed with formulating a hypothesis is to use the following format:</p>
<p>We believe [this capability] will result in [this outcome]. We will have the confidence to proceed when [we see a measurable signal].</p>
<h2>Working software is not a measure of progress</h2>
<p>Output-based metrics and concepts (definition of "done," acceptance criteria, burndown charts, and velocity) are good for detecting working software but fall miserably when it comes to detecting if working software adds value.</p>
<p>"Done" only matters if it adds value. Working software that doesn’t add value cannot be declared "done."</p>
<h2>The forgotten column</h2>
<p>Technology-centric projects break activities down into four columns:</p>
<ol><li>Backlog of ideas</li>
<li>Analysis</li>
<li>In progress</li>
<li>Shipped</li>
</ol><p>The above structure is based on the strong belief that all software that works is valuable. That focus must now shift toward continuously delivering real value, something that serves customers. Agilists value outcomes (value to the customers) over features.</p>
<p>The new breakdown for hypothesis-driven development looks something like this:</p>
<table border="1" cellpadding="1" cellspacing="1" style="width:850px;"><thead><tr><th scope="col">
<p>Ideas Backlog</p>
</th>
<th scope="col">
<p>Analysis</p>
</th>
<th scope="col">
<p>In Progress</p>
</th>
<th scope="col">
<p>Shipped</p>
</th>
<th scope="col">
<p>Achieved Desired Outcome</p>
</th>
</tr></thead><tbody><tr><td valign="top">
<p class="rtecenter">Hypothesis 11</p>
<p class="rtecenter">Hypothesis 12</p>
<p class="rtecenter">Hypothesis 13</p>
<p class="rtecenter">Hypothesis 14</p>
<p class="rtecenter">Hypothesis 15</p>
<p class="rtecenter">Hypothesis 16</p>
<p class="rtecenter">Hypothesis 17</p>
<p class="rtecenter">Hypothesis 18</p>
<p class="rtecenter">Hypothesis 19</p>
</td>
<td valign="top">
<p class="rtecenter">Hypothesis 20</p>
<p class="rtecenter">Hypothesis 21</p>
</td>
<td valign="top">
<p class="rtecenter">Hypothesis 26</p>
</td>
<td valign="top">
<p class="rtecenter">Hypothesis 2</p>
<p class="rtecenter">Hypothesis 5</p>
<p class="rtecenter">Hypothesis 9</p>
<p class="rtecenter">Hypothesis 10</p>
</td>
<td valign="top">
<p class="rtecenter">Hypothesis 1</p>
<p class="rtecenter">Hypothesis 5</p>
</td>
</tr></tbody></table><p>All eyes must remain on the Achieved Desired Outcome column.</p>
