<p>When I started learning R, I also needed to learn how to collect Twitter data and map it for research purposes. Despite the wealth of information on the internet about this topic, I found it difficult to understand what was involved in collecting and mapping Twitter data. Not only was I was a novice to R, but I was also unfamiliar with the technical terms in the various tutorials. Despite these barriers, I was successful! In this tutorial, I will break down how to collect Twitter data and display it on a map in a way that even novice coders can understand.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Programming and development</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ&amp;src=programming_resource_menu4">Red Hat Developers Blog</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Programming cheat sheets</a></li>
<li><a href="https://www.redhat.com/en/services/training/learning-subscription?intcmp=7016000000127cYAAQ">Try for free: Red Hat Learning Subscription</a></li>
<li><a href="https://opensource.com/downloads/bash-programming-guide?intcmp=7016000000127cYAAQ">eBook: An introduction to programming with Bash</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/bash-shell-cheat-sheet?intcmp=7016000000127cYAAQ">Bash Shell Scripting Cheat Sheet</a></li>
<li><a href="https://developers.redhat.com/e-books/modernizing-enterprise-java?intcmp=7016000000127cYAAQ">eBook: Modernizing Enterprise Java</a></li>
</ul></div>
</div>
</div>
</div>

<h2>Create the app</h2>
<p>If you don't have a Twitter account, the first thing you need to do is to <a href="https://twitter.com/signup" target="_blank">create one</a>. After that, go to <a href="https://apps.twitter.com/" target="_blank">apps.twitter.com</a> to create an app that allows you to collect Twitter data. Don't worry, creating the app is extremely easy. The app you create will connect to the Twitter application program interface (API). Think of an API as an electronic personal assistant of sorts. You will be using the API to ask another program to do something for you. In this case, you will be connecting to the Twitter API and asking it to collect data. Just make sure you don't ask too much, because there is a <a href="https://dev.twitter.com/rest/public/rate-limiting" target="_blank">limit</a> on how many times you can request Twitter data.</p>
<p>There are two APIs that you can use to collect tweets. If you want to do a one-time collection of tweets, then you'll use the <b>REST API</b>. If you want to do a continuous collection of tweets for a specific time period, you'll use the <b>streaming API</b>. In this tutorial, I'll focus on using the REST API.</p>
<p>After you create your app, go to the <b>Keys and Access Tokens</b> tab. You will need the Consumer Key (API key), Consumer Secret (API secret), Access Token, and Access Token Secret to access your app in R.</p>
<h2>Collect the Twitter data</h2>
<p>The next step is to open R and get ready to write code. For beginners, I recommend using <a href="https://www.rstudio.com/" target="_blank">RStudio</a>, the integrated development environment (IDE) for R. I find using RStudio helpful when I am troubleshooting or testing code. R has a package to access the REST API called <strong><a href="https://cran.r-project.org/web/packages/twitteR/twitteR.pdf" target="_blank">twitteR</a></strong>.</p>
<p>Open RStudio and create a new RScript. Once you have done this, you will need to install and load the <b>twitteR</b> package:</p>
<pre>
install.packages("twitteR") 
#installs TwitteR
library (twitteR) 
#loads TwitteR
</pre><p>Once you've installed and loaded the <b>twitteR</b> package, you will need to enter the your app's API information from the section above:</p>
<pre>
api_key &lt;- "" 
 #in the quotes, put your API key 
api_secret &lt;- "" 
 #in the quotes, put your API secret token 
token &lt;- "" 
 #in the quotes, put your token
token_secret &lt;- "" 
 #in the quotes, put your token secret
</pre><p>Next, connect to Twitter to access the API:</p>
<pre>
setup_twitter_oauth(api_key, api_secret, token, token_secret)</pre><p>Let's try doing a Twitter search about community gardens and farmers markets:</p>
<pre>
tweets &lt;- searchTwitter("community garden OR #communitygarden OR farmers market OR #farmersmarket", n = 200, lang = "en")</pre><p>This code simply says to search for the first 200 tweets <b>(n = 200)</b> in English <b>(lang = "en")</b>, which contain the terms <b>community garden</b> or <b>farmers market</b> or any hashtag mentioning these terms.</p>
<p>After you have done your Twitter search, save your results in a data frame:</p>
<pre>
tweets.df &lt;-twListToDF(tweets)</pre><p>To create a map with your tweets, you will need to export what you collected into a <b>.csv</b> file:</p>
<pre>
write.csv(tweets.df, "C:\Users\YourName\Documents\ApptoMap\tweets.csv") 
 #an example of a file extension of the folder in which you want to save the .csv file.</pre><p>Make sure you save your <b>R</b> code before running it and moving on to the next step.</p>
<h2>Create the map</h2>
<p>Now that you have data, you can display it in a map. For this tutorial, we will make a basic app using the R package <strong><a href="https://rstudio.github.io/leaflet" target="_blank">Leaflet</a></strong>, a popular JavaScript library for making interactive maps. Leaflet uses the <a href="https://github.com/smbache/magrittr" target="_blank"><b>magrittr</b></a> pipe operator (<b>%&gt;%</b>), which makes it easier to write code because the syntax is more natural. It might seem strange at first, but it does cut down on the amount of work you have to do when writing code.</p>
<p>For the sake of clarity, open a new R script in RStudio and install these packages:</p>
<pre>
install.packages("leaflet")
install.packages("maps") 
library(leaflet)
library(maps)
</pre><p>Now you need a way for Leaflet to access your data:</p>
<pre>
read.csv("C:\Users\YourName\Documents\ApptoMap\tweets.csv", stringsAsFactors = FALSE)</pre><p><strong>stringAsFactors = FALSE</strong> means to keep the information as it is and not convert it into factors. (For information about factors, read the article <a href="http://simplystatistics.org/2015/07/24/stringsasfactors-an-unauthorized-biography/" target="_blank">"stringsAsFactors: An unauthorized biography"</a>, by Roger Peng.)</p>
<p>It's time to make your Leaflet map. You are going to use the <b>OpenStreetMap</b> base map for your map:</p>
<pre>
m &lt;- leaflet(mymap) %&gt;% addTiles()</pre><p>Let's add circles to the base map. For <b>lng</b> and <b>lat</b>, enter the name of the columns that contain the latitude and longitude of your tweets followed by <b>~</b>. The <b>~longitude</b> and <b>~latitude</b> refer to the name of the columns in your <b>.csv</b> file:</p>
<pre>
m %&gt;% addCircles(lng = ~longitude, lat = ~latitude, popup = mymap$type, weight = 8, radius = 40, color = "#fb3004", stroke = TRUE, fillOpacity = 0.8)</pre><p>Run your code. A web browser should pop up and display your map. Here is a map of the tweets that I collected in the previous section:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Map of tweets by location"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/leafletmap.jpg" width="650" height="343" alt="Map of tweets by location" title="Map of tweets by location" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>Map of tweets by location, Leaflet and OpenStreetMap, <a href="https://creativecommons.org/licenses/by-sa/2.0/" target="_blank" rel="ugc">CC-BY-SA</a></sup></p>
</div>
      
  </article></p>
<p><add here="" leafletmap.jpg=""><span style="display: none;"> </span><span style="display: none;"> </span></add></p>
<p>Although you might be surprised with the small number of tweets on the map, typically only 1% of tweets are geocoded. I collected a total of 366 tweets, but only 10 (around 3% of total tweets) were geocoded. If you are having trouble getting geocoded tweets, change your search terms to see if you get a better result.</p>
<h2>Wrapping up</h2>
<p>For beginners, putting all the pieces together to create a Leaflet map from Twitter data can be overwhelming. This tutorial is based on my experiences doing this task, and I hope it makes the learning process easier for you.</p>
<p><i>Dorris Scott will present this topic in a workshop, <a href="https://werise.tech/sessions/2017/4/16/from-app-to-map-collecting-and-mapping-social-media-data-using-r?rq=social%20mapping" target="_blank">From App to Map: Collecting and Mapping Social Media Data using R</a>, at the <a href="https://werise.tech/" target="_blank">We Rise</a> Women in Tech Conference (<a href="https://twitter.com/search?q=%23WeRiseTech&amp;src=typd" target="_blank">#WeRiseTech</a>) June 23-24 in Atlanta.</i></p>
<p></p>
