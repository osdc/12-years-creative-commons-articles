<p>In the world of version control, Git has clearly claimed the mantle of the preferred version control tool of most developers. In a recent <a href="https://stackoverflow.com/insights/survey/2017#work-version-control">developer survey</a> on Stack Overflow, Git was the preferred version control of 69.2% of participants, over seven times as many votes as the next runner up, Subversion.</p>
<p>And today, we celebrate a dozen years passing since the initial release of Git on April 7, 2005. Created by Linus Torvalds to manage the expansive source code of the Linux kernel, Git now manages the source code of countless open source projects you know and love. We've rounded up a collection of articles from Opensource.com community moderator Seth Kenlon highlighting the many great uses of Git, and how you can use it to version nearly everything in your day-to-day workflow.</p>
<ul><li>
<p><a href="https://opensource.com/resources/what-is-git">What is Git?</a><br />
	See how Git can help you keep your files organized as they change over time.</p>
</li>
</ul><ul><li>
<p><a href="https://opensource.com/life/16/7/stumbling-git">Getting started with Git</a><br />
	Learn the basics of working with Git from the command line in this easy-to-follow introduction.</p>
</li>
</ul><ul><li>
<p><a href="https://opensource.com/life/16/7/creating-your-first-git-repository">Creating your first Git repository</a><br />
	Create your own Git repository, add files, and make commits.</p>
</li>
</ul><ul><li>
<p><a href="https://opensource.com/life/16/7/how-restore-older-file-versions-git">How to restore older file versions in Git</a><br />
	Find out where you are in the history of your project, restore older file versions, and make branches.</p>
</li>
</ul><ul><li>
<p><a href="https://opensource.com/life/16/8/graphical-tools-git">3 graphical tools for Git</a><br />
	Explore convenient add-ons to help you integrate Git comfortably into your everyday workflow.</p>
</li>
</ul><ul><li>
<p><a href="https://opensource.com/life/16/8/how-construct-your-own-git-server-part-6">How to build your own Git server</a><br />
	Build a Git server, and write custom Git hooks to trigger specific actions on certain events.</p>
</li>
</ul><ul><li>
<p><a href="https://opensource.com/life/16/8/how-manage-binary-blobs-git-part-7">How to manage binary blobs with Git</a><br />
	Explore extensions that allow for easy handling of big files.</p>
</li>
</ul>