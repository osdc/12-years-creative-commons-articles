<p>Chromebooks have been a game-changer for PreK-12 school systems, enabling them to purchase low-cost laptop computers for students, teachers, and administrators to use. While <a href="https://en.wikipedia.org/wiki/Chromebook" target="_blank">Chromebooks</a> have always been powered by a Linux-based operating system (Chrome OS), until recently, there was no way to run most Linux apps on one. But that changed when Google released <a href="https://chromium.googlesource.com/chromiumos/docs/+/master/containers_and_vms.md" target="_blank">Crostini</a>, a virtual machine that allows Chromebooks to run Linux (Beta).</p>
<p>Most Chromebooks released after 2019 and some earlier models can run Crostini and Linux (Beta). Check this <a href="https://www.chromium.org/chromium-os/chrome-os-systems-supporting-linux" target="_blank">list of supported devices</a> to see if your Chromebook is on it. Fortunately, my Acer Chromebook 15 with 2GB RAM and an Intel Celeron processor is supported.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Acer Chromebook specs"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/chromebook-specs.png" width="675" height="422" alt="Acer Chromebook specs" title="Acer Chromebook specs" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Don Watkins, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Linux resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux commands cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Free online course: RHEL Technical Overview</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux networking cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">SELinux cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux common commands cheat sheet</a></li>
<li><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">What are Linux containers?</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Our latest Linux articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>I recommend using a Chromebook with 4GB RAM and more disk space if you plan to install a lot of Linux applications.</p>
<h2 id="set-up-linux-beta">Set up Linux (Beta)</h2>
<p>After you sign into your Chromebook, "mouse over" to the lower-right corner of the screen where the clock is displayed, and left-click there. A panel will open with options at the top (from left to right) to sign out, shut down, lock, and open Settings. Choose the <strong>Settings</strong> icon.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Chromebook Settings button"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/chromebook-settings.png" width="414" height="527" alt="Chromebook Settings button" title="Chromebook Settings button" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Don Watkins, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>Look on the left side of the <strong>Settings</strong> panel, and you will see <strong>Linux (Beta)</strong> listed.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Chromebook Settings"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/chromebook-linux-beta.png" width="675" height="380" alt="Chromebook Settings" title="Chromebook Settings" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Don Watkins, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>Click on <strong>Linux (Beta),</strong> and the main panel will change to present an option to launch it. Click the <strong>Turn on</strong> button.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Turn on Linux (Beta)"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/chromebook-launch-linux-beta.png" width="675" height="220" alt="Turn on Linux (Beta)" title="Turn on Linux (Beta)" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Don Watkins, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>It will start the process of setting up a Linux environment on your Chromebook.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Setting up Linux (Beta)"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/chromebook-setup-linux-beta_0.png" width="675" height="496" alt="Setting up Linux (Beta)" title="Setting up Linux (Beta)" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Don Watkins, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>Next, you will be prompted to enter a <strong>Username</strong> and the size you want your Linux installation to be.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Setting up Linux (Beta)"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/chromebook-linux-beta-username.png" width="675" height="447" alt="Setting up Linux (Beta)" title="Setting up Linux (Beta)" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Don Watkins, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>It takes a few minutes to install Linux on your Chromebook.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Installing Linux (Beta)"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/chromebook-linux-beta-install.png" width="514" height="342" alt="Installing Linux (Beta)" title="Installing Linux (Beta)" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Don Watkins, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>After the installation completes, you can use Linux on your Chromebook. The menu bar on the bottom of your Chromebook's display has a shortcut to a <strong>terminal</strong>, a text-based interface you can use to interact with Linux.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Linux (Beta) terminal"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/chromebook-linux-terminal.png" width="488" height="277" alt="Linux (Beta) terminal" title="Linux (Beta) terminal" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Don Watkins, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>You can use <a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet">common Linux commands</a> like <code>ls</code>, <code>lscpu</code>, and <code>top</code> to see more of your environment. You can install applications using <code>sudo apt install</code>.</p>
<h2 id="install-your-first-linux-application">Install your first Linux application</h2>
<p>Being able to install and run free and open source software on a Chromebook can be a real winner for financially constrained school districts.</p>
<p>The first application I recommend installing is the <a href="https://codewith.mu" target="_blank">Mu editor</a> for Python. Install it by entering the following into your terminal:</p>
<pre><code class="language-bash">$ sudo apt install mu-editor</code></pre><p>It takes a bit over five minutes to install, but in the end, you'll have access to a really good Python editor for students and anyone else who wants to learn Python.</p>
<p>I've had great success using <a href="https://opensource.com/article/20/9/teaching-python-mu">Mu and Python as a learning tool</a>. For example, I have taught my students to write code for Python's turtle module and execute it to create graphics. I was disappointed that I wasn't able to use Mu with a <a href="https://opensource.com/article/19/8/getting-started-bbc-microbit">BBC:Microbit</a> open hardware board. Even though the Microbit connects to USB and there is USB support in the Chromebook's Linux virtual environment, I couldn't make it work.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Mu editor"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/chromebook-mu.png" width="675" height="353" alt="Mu editor" title="Mu editor" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Don Watkins, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>Once you've installed an application, it will show up in a special <strong>Linux Apps</strong> menu, which you can see on the lower-right of this screenshot.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Linux Apps menu"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/chromebook-linux-apps.png" width="675" height="379" alt="Linux Apps menu" title="Linux Apps menu" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Don Watkins, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<h2 id="install-other-applications">Install other applications</h2>
<p>You can install more than just a programming language and code editor. In fact, you can install most of your favorite open source applications.</p>
<p>For example, you can install the LibreOffice suite with:</p>
<pre><code class="language-bash">$ sudo apt install libreoffice</code></pre><p>The open source audio software <a href="https://www.audacityteam.org/" target="_blank">Audacity</a> is one of my other favorite classroom applications. My Chromebook's microphone works with Audacity, making it easy to create podcasts or edit free sounds from <a href="https://commons.wikimedia.org/wiki/Commons:Free_media_resources/Sound" target="_blank">Wikimedia Commons</a>. It's easy to install Audacity on a Chromebook—with the Crostini virtual environment running, open the terminal and enter:</p>
<pre><code class="language-bash">$ sudo apt install audacity</code></pre><p>Then launch Audacity from the command line or look for it in the <strong>Linux Apps</strong> section of the Chromebook menu.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Audacity"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/chromebook-audacity.png" width="675" height="371" alt="Audacity" title="Audacity" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Don Watkins, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>I also easily installed <a href="https://sourceforge.net/projects/tuxmath/" target="_blank">TuxMath</a> and <a href="https://sourceforge.net/projects/tuxtype/" target="_blank">TuxType</a>, a couple of great applications for education. I was even able to install and run the image editor <a href="https://www.gimp.org/" target="_blank">GIMP</a>. All the Linux applications come from Debian Linux repositories.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="GIMP"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/chromebook-gimp.png" width="675" height="397" alt="GIMP" title="GIMP" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Don Watkins, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<h2 id="share-files">Share files</h2>
<p>There is a utility within Linux (Beta) to back up and restore your files. You can also share files between your Linux (Beta) virtual machine and your Chromebook by opening the <strong>Files</strong> app on the Chromebook and right-clicking on the folder you want to share. You can choose to share all of your Chromebook files or create a special folder for shared files. While you are in the Linux virtual machine, you can access this folder by browsing to <code>/mnt/chromeos</code>.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Sharing files"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/chromebook-linux-share-files.png" width="675" height="380" alt="Sharing files" title="Sharing files" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Don Watkins, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<h2 id="learn-more">Learn more</h2>
<p>The <a href="https://support.google.com/chromebook/answer/9145439?p=chromebook_linuxapps&amp;b=banon-signed-mpkeys&amp;visit_id=637346541887671598-1548999339&amp;rd=1" target="_blank">documentation</a> for Linux (Beta) is very complete, so read it thoroughly to learn more about its capabilities. Some key takeaways from the documentation are:</p>
<ul><li>Cameras are not yet supported.</li>
<li>Android devices are supported over USB.</li>
<li>Hardware acceleration is not yet supported.</li>
<li>You can access the microphone.</li>
</ul><p>Are you using Linux applications on your Chromebook? Tell us all about it in the comments!</p>
