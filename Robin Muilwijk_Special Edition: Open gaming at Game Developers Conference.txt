<p>Hello, open gaming fans! This week brings you a special edition as we look at some of the highlights from the <a href="http://www.gdconf.com/" target="_blank">Game Developers Conference</a> (GDC) that took place a little over a week ago.</p>
<h2 style="text-align: left;">Open gaming roundup: March 7 - 14, 2015</h2>
<p><!--break--></p>
<h3>Shadow Of Mordor wins Game Of The Year at GDC</h3>
<p><iframe width="520" height="293" src="https://www.youtube.com/embed/9YDJN7MkZMk" frameborder="0" allowfullscreen=""></iframe></p>
<p><em>Middle-earth: Shadow of Mordor</em> has won <em>Game of the Year</em> for the Game Developers Choice Awards. The game is a role-playin, action adventure from Monolith Productions inspired by J.R.R. Tolkien's <em>The Hobbit</em> and <em>The Lord of the Rings</em>.</p>
<p>Thanks to Feral Interactive, it will arrive on Linux this spring. David Stephen, Managing Director of <a href="https://www.feralinteractive.com/en/" target="_blank">Feral Interactive</a> said:</p>
<blockquote><p>Middle-earth: Shadow of Mordor's immense scale, critical acclaim and well-deserved popularity with players make its release on Linux a major event. It is a monumental game, and we are thrilled to be bringing it to Linux gamers.</p>
</blockquote>
<h3>SteamVR dethrones Oculus Rift</h3>
<p>Valve took its new SteamVR to the Game Developers Conference. Hayden Dingman, reporter at PCWorld, got to demo it, and according to him, it <a href="http://www.pcworld.com/article/2893314/oculus-dethroned-valves-steamvr-is-the-new-virtual-reality-frontrunner.html" target="_blank">dethroned Oculus</a>! Notable here is that it's controllers are actually visible. Dingman says, "this is how VR should control." Also announced was Owlchemy Labs' participation in the SteamVR project with the game <em><a href="http://jobsimulatorgame.com/" target="_blank">Job Simulator</a></em>.</p>
<h3 itemprop="headline">Valve launches Steam Machine hardware store</h3>
<p>The wait is over! Valve has opened up a hardware store for <a href="http://store.steampowered.com/sale/steam_machines" target="_blank">Steam Machines</a>, the Steam <a href="http://store.steampowered.com/app/353370" target="_blank">Controller</a>, and <a href="http://store.steampowered.com/app/353380" target="_blank">Steam Link</a>.</p>
<p>Steam Machines will be available in November 2015; at the low end, the Alienware Steam Machine will retail for $479.99 USD; at the high end, the Falcon Northwest Tiki Steam Machine and the the Origin Omega Steam Machine will retail for $5,000 USD.</p>
<p>The Steam Controller comes with dual trackpads, dual-stage triggers, and ergonomic controls. It will also be available in November 2015, and it will retail for $49.99 USD.</p>
<p><article class="media media--type-image media--view-mode-full" title="Steam Controller"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/life-uploads/steam-controller.jpg" width="520" height="292" alt="Steam Controller" title="Steam Controller" /></div>
      
  </article></p>
<p>Steam Link "brings your existing collection of PC games with you to the living room. Simply connect Steam Link to your TV and home network and access your full library of games and the complete Steam catalog."</p>
<h3>AAA titles coming to SteamOS</h3>
<p>Along with all the hardware announcements at GDC, many AAA game titles were confirmed to come to Linux/SteamOS in this year. Here's the list:</p>
<ul><li>Shadow of Mordor</li>
<li>Batman Arkham Knight</li>
<li>Rome Total War 2</li>
<li>Total War: Attila</li>
<li>Evolve</li>
<li>Witcher 3</li>
<li>Payday 2</li>
<li>Saints Row IV</li>
<li>GRID Autosport</li>
<li>Companies of Heroes 2</li>
</ul><p><article class="media media--type-image media--view-mode-full" title="Batman Arkham Knight"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/images/life-uploads/batman-arkham-knight-1425123258-1024x576.jpg" width="520" height="292" alt="Batman Arkham Knight" title="Batman Arkham Knight" /></div>
      
  </article></p>
<h3>Valve demos new Vulkan API</h3>
<p>Valvue and The Khronos Group demoed the new <a href="https://www.khronos.org/vulkan/" target="_blank">Vulkan API</a>, which is supposed to replace the current OpenGL generation. It's said to do wonders in the <a href="http://www.polygon.com/2015/3/3/8145273/valve-source-2-announcement-free-developers" target="_blank">Source engine</a> from Valve, but this means developers will have to provide support for Vulkan in their engines.</p>
<blockquote><p>Vulkan is the new generation, open standard API for high-efficiency access to graphics and compute on modern GPUs. This ground-up design, previously referred to as the Next Generation OpenGL Initiative, provides applications direct control over GPU acceleration for maximized performance and predictability.</p>
</blockquote>
<p><em>This wraps up our coverage of some of the highlights of the Game Developers Conference in this special edition. If you happened to have been there, feel free to share you experience in the comments.</em></p>
