<p>In 2016, I launched Open Innovation Labs, a place where people seeking to leverage the principles of openness can work with a seasoned team to build innovative software that solves their most pressing business problems. It has been an exciting and daunting undertaking. Today, Open Innovation Labs imparts knowledge and best practices that emerge from the world's most successful open source projects, and we provide a residency-style experience that immerses teams in those practices.</p>
<p>We generally partner with companies looking to do two things: Either they want to move quickly and be disruptive, or they see disruption as an existential threat and seek to adapt their behaviors to facilitate a more rapid pace of change.</p>
<p>Our own team began as one of the former, but we suddenly found ourselves one of the latter. The lessons we learned as we made that transition—lessons about organizational culture and the power of openness to shape it—truly have made us better coaches today.</p>
<p>Here's what happened.</p>
<h2>An identity crisis</h2>
<p>To launch Labs, I built a small, cross-functional team that immediately set to work doing the very things we encourage lab residents to do. First, we created hypothetical scenarios to achieve our team's objective—which, in this case, was to accelerate our customers' work in a residency-style engagement and build enthusiasm for building applications the open source way. Then, we applied emerging technologies to build next-generation software that would help us explore those scenarios. We worked relentlessly to create a clever system for accelerating customers' efforts and delivering real value.</p>
<p>
</p><div class="embedded-callout-menu callout-float-left">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Learn about open organizations</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://theopenorganization.org">Download resources</a></li>
<li><a href="https://theopenorganization.community">Join the community</a></li>
<li><a href="https://opensource.com/open-organization/resources/open-org-definition?src=too_resource_menu3a">What is an open organization?</a></li>
<li><a href="https://opensource.com/open-organization/resources/open-org-maturity-model?src=too_resource_menu4a">How open is your organization?</a></li>
</ul></div>
</div>
</div>
</div>

<p>We called that system "push-button infrastructure," or "PBI." PBI allows us to spin up a customer-ready Labs environment, built for speed and experimentation, in less than an hour. We took it to market as soon as we could (even before it was fully functional), and the reaction actually took us by surprise. "I want that!" was the most common response we received from customers and internal teams. We were onto something. The excitement was palpable.</p>
<p>About nine months into the endeavor, one of my technical staff pointed out that another open source engineering team in the community had made a major breakthrough with their software—something that allowed them to create a system that could eventually replace much of what we'd built. What's more, this team was easily ten times larger than ours, with a huge ecosystem of partners to boot.</p>
<p></p><div class="embedded-callout-text callout-float-right">Our little gem was already in danger of being disrupted by a more powerful force.</div>
<p>I reached out to one of the project leaders, another open source advocate. He candidly told me that our group was the inspiration for much of their efforts.</p>
<p>While I was flattered, my stomach also sank: Our little gem was already in danger of being disrupted by a more powerful force.</p>
<p>The team took stock of the situation. We faced an identity crisis. If our core product became obsolete, then would we continue to exist? In that case, what was our core value proposition? What would we look like?</p>
<h2>The primacy of principles</h2>
<p>We convened face-to-face to sketch our core value proposition in a group exercise called "mind mapping." It produced two strong revelations.</p>
<p>The first was about PBI. As exciting as the technology was, it didn't actually define much of what made us special. In fact, it was barely on the mind map. If it came from another group, or was abandoned entirely, it still wouldn't stop us from achieving our mission. This was a big weight off of our collective shoulders.</p>
<p>The second realization was about something more abstract: The team's most-valued asset was its shared belief system. We all named characteristics like "collaboration," "authenticity," "transparency," "accountability," "open decision making," "meritocracy," "adaptation," "experimentation," and "a focus on impacting people" as the things that made our team truly unique and capable of delivering value to our customers.</p>
<p>These cultural principles came from our experience working in open source communities. We listed principles like:</p>
<ul><li>Shared problems are solved faster</li>
<li>Transparency forces authenticity and honesty</li>
<li>Participative communities are more open to change</li>
<li>Open standards provide business agility</li>
</ul><p>Our core mission, we decided, was to share with our customers and partners these same principles over the course of our engagements with them—to help them leverage lessons from the open source world to build, better, more adaptive solutions to problems. This insight allowed us to pivot productively; we realized we should focus <em>more</em> on imparting new ways of working together to catalyze organizational (and eventually cultural) change, and <em>less</em> on any particular technical solution or offering.</p>
<p>Shortly after that meeting, we made the bold choice to utilize the great work coming from the external community we'd viewed as an existential threat only days before. We adapted our software to take better advantage of that offering. Doing this meant halting development on some of what we initially considered core material in order to ride a bigger, better wave.</p>
<p></p><div class="embedded-callout-text callout-float-right">Our core mission, we decided, was to share with our customers and partners these same principles over the course of our engagements with them.</div>
<p>I still keep the result of that mind map on my desktop. It reminds me that our shared beliefs endure far beyond any particular experiment or project. It centers our team in a way that allows us to transcend the individual, and become part of a bigger purpose, satisfying a fundamental human desire that lays in each of us.</p>
<p>As you read the first section of <a href="https://opensource.com/open-organization/resources/culture-change">this guide</a>, I hope you too will discover the benefits of building an organizational culture on open principles—one that transcends any individual effort and creates an enduring, shared purpose capable of inspiring teams long after we're all gone.</p>
<p><em>This article is part of <a href="https://opensource.com/open-organization/resources/culture-change">The Open Organization Guide to IT culture change</a>.</em></p>
