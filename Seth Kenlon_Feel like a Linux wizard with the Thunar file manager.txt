<p>Computers are fancy filing cabinets, full of virtual folders and files waiting to be referenced, cross-referenced, edited, updated, saved, copied, moved, renamed, and organized. In this article, I'll take a look at a file manager for your Linux system.</p>
<p>The <a href="https://opensource.com/article/19/12/xfce-linux-desktop" target="_blank">XFCE desktop</a> is well-known and respected for its careful and methodical development cycle. It's actively maintained and has continued to evolve over the years, its focus tends to be on delivering a consistent experience with just the right amount of new features. It's a pleasure to use XFCE on everything from the old computer you've repurposed as a home server to the new PC you've just built and are too greedy to waste GPU cycles on fancy desktop effects. XFCE is a budget desktop that doesn't make you skimp on experience, and its file manager exemplifies that.</p>
<h2 id="_thunar">Linux Thunar file manager</h2>
<p>The Thunar file manager is a lightweight file manager based on the GTK toolkit. If you're running GNOME already, you have most of what you need to run Thunar already installed. If you're not running GNOME, you probably still have most of what you need to run Thunar, because much of what Thunar uses is part of a typical Linux install.</p>
<p>The visual design of Thunar is, like much of XFCE, "normal." By that, I mean it's exactly what you'd expect from a file manager: the side pane on the left contains common places you use often, and the pane on the right lists the files and folders on your system.</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-09/thunarfilemanager.png" width="636" height="480" alt="Image of the Thunar file manager." /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Seth Kenlon, CC BY-SA 4.0)</p>
</div>
      
  </article><h2>Side pane</h2>
<p>The panel on the left contains the default folders of a conventional POSIX system: Desktop, Documents, Downloads, Music, Pictures, Videos, and so on. It also has a slot for removable devices, such as USB drives and SD cards. In addition, it has a place for Network locations, in the event that you interact with a remote computer or a file share.</p>
<p>Most options in Thunar are provided through its main menu, and that includes the option to change the side pane to a tree view. This view displays common places in an expandable list so you can collapse an entry you're not using at the moment.</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-09/thunar-side-panel.png" width="174" height="420" alt="Image of a side panel in Thunar." /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Seth Kenlon, CC BY-SA 4.0)</p>
</div>
      
  </article><h2>Menu</h2>
<p>Much of Thunar's power is accessible through its menu bar. Sure, there's a right-click contextual menu, but you will find the most powerful functions in the main menu. From the menu, you can perform quick but meaningful actions.</p>
<ul><li>
<p><strong>Invert selection</strong>: Select one or more items, then go to the <strong>Edit</strong> menu and choose <strong>Invert selection</strong>. Everything you had selected is now unselected, and everything else in the directory is selected.</p>
</li>
<li>
<p><strong>Select by pattern</strong>: Select files based on some combination of characters appearing in their names. For instance, type in <code><strong>jpg</strong></code><strong> </strong>to select all files<strong> </strong>ending<strong> </strong>in<strong> <code>*jpg</code>, </strong>or<strong> <code>*nix</code></strong> to select all files containing the contiguous letters <code>nix</code>.</p>
</li>
<li>
<p><strong>Show hidden files</strong>: Sometimes you need to see the files you normally want out of sight.</p>
</li>
<li>
<p><strong>Open terminal here</strong>: Open a terminal window with its working directory set to your location.</p>
</li>
<li>
<p><strong>Make link</strong>: Make a symlink (sometimes called an "alias" or "shortcut") of the selected file or folder.</p>
</li>
</ul><p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">More Linux resources</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux commands cheat sheet">Linux commands cheat sheet</a></div>
              <div class="field__item"><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Advanced Linux commands cheat sheet">Advanced Linux commands cheat sheet</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Free online course: RHEL technical overview">Free online course: RHEL technical overview</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux networking cheat sheet">Linux networking cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="SELinux cheat sheet">SELinux cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux common commands cheat sheet">Linux common commands cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="What are Linux containers?">What are Linux containers?</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Register for your free Red Hat account">Register for your free Red Hat account</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Our latest Linux articles">Our latest Linux articles</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<h2 id="_fast">Fast</h2>
<p>The beautiful thing about Thunar, aside from its straight-forward simplicity, is how it defaults to immediately performing the action you've requested. There are file managers out there (including my personal favorite) that, at least without advanced knowledge of special shortcuts, interrupt a process in the interest of verification or clarification. Thunar only pauses when it absolutely needs feedback from you.</p>
<p>A great example is the <strong>Make link</strong> function in the <strong>Edit</strong> menu. In some file managers, when you create a symlink you're asked first whether or not you want to make a symlink, and then maybe what you'd like to name the symlink, and maybe even where you want the symlink to be created. All of that's really important information and results in a targeted and precise action. However, sometimes you don't need precision. Sometimes you just want speed, which is why Thunar just <em>makes the symlink</em>. No questions asked. When you tell it to create a symlink, it creates a symlink, with a name prefixed with "link to", in your current directory. If that's not what you wanted the symlink to be named, then you can rename it. If that's not where you want the symlink to exist, then you can relocate it. Your upfront investment is minimal, and you get results fast.</p>
<h2 id="_bulk_rename">Bulk rename</h2>
<p>Possibly Thunar's greatest contribution to the desktop is its bulk renaming function. How many times have you come home from vacation with a hundred photos called some variation of <code>IMG_2022-01-04_10-55-12.JPG</code>, which you add to a thousand other photos with similarly meaningless names? There are lots of <a href="https://opensource.com/life/16/5/how-use-digikam-photo-management">photo managers</a> out there that can help you <a href="https://opensource.com/life/16/4/how-use-darktable-digital-darkroom">organize, browse, and tag</a> those photos, but there's nothing like a descriptive name to start out with. With Thunar, you can select hundreds of files, right-click, and select <strong>Rename</strong> for a bulk renaming interface.</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-09/thunar-bulk-rename.png" width="584" height="522" alt="Image of Thunar's bulk rename window." /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Seth Kenlon, CC BY-SA 4.0)</p>
</div>
      
  </article><p>Imagine opening your <code>~/Pictures</code> folder and seeing hundreds of photos with meaningful names! You probably don't even dare, but with Thunar it's actually possible!</p>
<h2 id="_runic_magic">Runic magic</h2>
<p>It's unclear whether Thunar is the product of mortal programmers or the magical denizens of Asgard. There's a very high probability of the former, but there's also the likelihood that you'll feel like the latter when you use it. It's a humble and simple file manager with a powerful feature set. It may even be a worthy replacement for your existing GNOME file manager, or at the very least worthy of being included in your dock. Install Thunar and wield its power.</p>
