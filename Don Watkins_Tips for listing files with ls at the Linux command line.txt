<p>One of the first commands I learned in Linux was <code>ls</code>. Knowing what’s in a directory where a file on your system resides is important. Being able to see and modify not just some but <em>all</em> of the files is also important.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>The Linux Terminal</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/life/17/10/top-terminal-emulators?intcmp=7016000000127cYAAQ">Top 7 terminal emulators for Linux</a></li>
<li><a href="https://opensource.com/article/17/2/command-line-tools-data-analysis-linux?intcmp=7016000000127cYAAQ">10 command-line tools for data analysis in Linux</a></li>
<li><a href="https://opensource.com/downloads/advanced-ssh-cheat-sheet?intcmp=7016000000127cYAAQ">Download Now: SSH cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=7016000000127cYAAQ">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://opensource.com/tags/command-line?intcmp=7016000000127cYAAQ">Linux command line tutorials</a></li>
</ul></div>
</div>
</div>
</div>
<p>My first LInux cheat sheet was the <a href="http://hackerspace.cs.rutgers.edu/library/General/One_Page_Linux_Manual.pdf" target="_blank">One Page Linux Manual</a>, which was released in1999 and became my go-to reference. I taped it over my desk and referred to it often as I began to explore Linux. Listing files with <code>ls -l</code> is introduced on the first page, at the bottom of the first column.</p>
<p>Later, I would learn other iterations of this most basic command. Through the <code>ls</code> command, I began to learn about the complexity of the Linux file permissions and what was mine and what required root or sudo permission to change. I became very comfortable on the command line over time, and while I still use <code>ls -l</code> to find files in the directory, I frequently use <code>ls -al</code> so I can see hidden files that might need to be changed, like configuration files.</p>
<p>According to an article by Eric Fischer about the <code>ls</code> command in the <a href="http://www.tldp.org/LDP/LG/issue48/fischer.html" target="_blank">Linux Documentation Project</a>, the command's roots go back to the <code>listf</code> command on MIT’s Compatible Time Sharing System in 1961. When CTSS was replaced by <a href="https://en.wikipedia.org/wiki/Multics" target="_blank">Multics</a>, the command became <code>list</code>, with switches like <code>list -all</code>. According to <a href="https://en.wikipedia.org/wiki/Ls" target="_blank">Wikipedia</a>, <code>ls</code> appeared in the original version of AT&amp;T Unix. The <code>ls</code> command we use today on Linux systems comes from the <a href="http://www.gnu.org/s/coreutils/" target="_blank">GNU Core Utilities</a>.</p>
<p>Most of the time, I use only a couple of iterations of the command. Looking inside a directory with <code>ls</code> or <code>ls -al</code> is how I generally use the command, but there are many other options that you should be familiar with.</p>
<p><code>$ ls -l</code> provides a simple list of the directory:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="linux_ls_directory list.png"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/linux_ls_1_0.png" width="517" height="264" alt="linux_ls_directory list.png" title="linux_ls_directory list.png" /></div>
      
  </article></p>
<p>Using the man pages of my Fedora 28 system, I find that there are many other options to <code>ls</code>, all of which provide interesting and useful information about the Linux file system. By entering <code>man ls</code> at the command prompt, we can begin to explore some of the other options:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="linux_ls_man command.png"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/linux_ls_2_0.png" width="725" height="389" alt="linux_ls_man command.png" title="linux_ls_man command.png" /></div>
      
  </article></p>
<p>To sort the directory by file sizes, use <code>ls -lS</code>:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="linux_ls_sort by file size.png"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/linux_ls_3_0.png" width="507" height="258" alt="linux_ls_sort by file size.png" title="linux_ls_sort by file size.png" /></div>
      
  </article></p>
<p>To list the contents in reverse order, use <code>ls -lr</code>:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="linux_ls_list in reverse order.png"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/linux_ls_4.png" width="500" height="259" alt="linux_ls_list in reverse order.png" title="linux_ls_list in reverse order.png" /></div>
      
  </article></p>
<p>To list contents by columns, use <code>ls -c</code>:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="linux_ls_list contents by column.png"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/linux_ls_5.png" width="609" height="60" alt="linux_ls_list contents by column.png" title="linux_ls_list contents by column.png" /></div>
      
  </article></p>
<p><code>ls -al</code> provides a list of all the files in the same directory:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="linux_ls_list all files in directory.png"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/linux_ls_6.png" width="551" height="243" alt="linux_ls_list all files in directory.png" title="linux_ls_list all files in directory.png" /></div>
      
  </article></p>
<p>Here are some additional options that I find useful and interesting:</p>
<ul><li>List only the .txt files in the directory: <code>ls *.txt</code></li>
<li>List by file size: <code>ls -s</code></li>
<li>Sort by time and date: <code>ls -d</code></li>
<li>Sort by extension: <code>ls -X</code></li>
<li>Sort by file size: <code>ls -S</code></li>
<li>Long format with file size: <code>ls -ls</code></li>
<li>List only the .txt files in a directory: <code>ls *.txt</code></li>
</ul><p>To generate a directory list in the specified format and send it to a file for later viewing, enter <code>ls -al &gt; mydirectorylist</code>. Finally, one of the more exotic commands I found is <code>ls -R</code>, which provides a recursive list of all the directories on your computer and their contents.</p>
<p>For a complete list of the all the iterations of the <code>ls</code> command, refer to the <a href="https://www.gnu.org/software/coreutils/manual/html_node/ls-invocation.html#ls-invocation" target="_blank">GNU Core Utilities</a>.</p>
