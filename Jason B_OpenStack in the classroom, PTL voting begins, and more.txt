<p>Interested in keeping track of what's happening in the open source cloud? Opensource.com is your source for news in <a href="https://opensource.com/resources/what-is-openstack" target="_blank" title="What is OpenStack?">OpenStack</a>, the open source cloud infrastructure project.</p>
<p><!--break--></p>
<h3>OpenStack around the web</h3>
<p>There's a lot of interesting stuff being written about OpenStack. Here's a sampling:</p>
<ul><li><a href="http://blog.russellbryant.net/2015/04/08/ovn-and-openstack-integration-development-update/" target="_blank">OVN and OpenStack integration development update</a>: How OpenStack and Open vSwitch are converging.</li>
<li><a href="http://superuser.openstack.org/articles/why-openstack-will-change-the-way-we-live-work-and-play" target="_blank">Why OpenStack will change the way we live, work and play</a>: An interview with Cisco's Niki Acosta.</li>
<li><a href="http://superuser.openstack.org/articles/why-adopting-openstack-for-dev-test-matters" target="_blank">Why adopting OpenStack for dev/test matters</a>: Development and testing are real work.</li>
<li><a href="http://www.tesora.com/openstack-in-the-classroom/" target="_blank">OpenStack in the classroom</a>: Teaching students to code for the clouds of the future.</li>
<li><a href="http://blog.leafe.com/the-core-deficiency/" target="_blank">The core deficiency</a>: Getting core code reviewers is still a challenge.</li>
<li><a href="http://www.siliconloons.com/posts/2015-04-06-neutron-kilo-retrospective-and-a-look-toward-liberty/" target="_blank">Neutron Kilo retrospective and a look toward Liberty</a>: Where is OpenStack networking headed as we look to future releases?</li>
</ul><h3>OpenStack discussions</h3>
<p>Here's a sample of some of the most active discussions this week on the OpenStack developers' listserv. For a more in-depth look, why not <a href="http://lists.openstack.org/pipermail/openstack-dev/" target="_blank" title="OpenStack listserv">dive in</a> yourself?</p>
<ul><li><a href="http://lists.openstack.org/pipermail/openstack-dev/2015-April/061071.html" target="_blank">The specs process, effective operators feedback and product management</a>: How features get merged in Neutron, and how the process might improve.</li>
<li><a href="http://lists.openstack.org/pipermail/openstack-dev/2015-April/061218.html" target="_blank">PTL voting is now open</a>: It's time to select the next leaders of the individual OpenStack projects.</li>
<li><a href="http://lists.openstack.org/pipermail/openstack-dev/2015-April/060932.html" target="_blank">Kilo stable branches for "other" libraries</a>: Versioning the other important parts of OpenStack.</li>
<li><a href="http://lists.openstack.org/pipermail/openstack-dev/2015-April/060748.html" target="_blank">How to send messages (and events) to our users</a>: Establishing communications mechanisms for conveying information to users.</li>
<li><a href="http://lists.openstack.org/pipermail/openstack-dev/2015-April/061002.html" target="_blank">Things to tackle in Liberty</a>: What's next for OpenStack Nova?</li>
</ul><h3>OpenStack events</h3>
<p>Here's what is happening this week. Something missing? <a href="mailto:osdc-admin@redhat.com">Email</a> us and let us know, or add it to our <a href="https://opensource.com/resources/conferences-and-events-monthly" target="_blank" title="Events Calendar">events calendar</a>.</p>
<ul><li><a href="http://www.percona.com/live/openstack-live-2015/" target="_blank" title="OpenStack Live" style="font-size: 13.0080003738403px; line-height: 1.538em; background-color: transparent;">OpenStack Live</a>: April 13-14, 2015; Santa Clara, CA.</li>
<li><span style="font-size: 13.0080003738403px; line-height: 1.538em; background-color: transparent;"><a href="http://www.meetup.com/Santiago-Devops-and-Continuous-Delivery-Meetup/events/221467200/" target="_blank">Santiago devops and continuous delivery meetup</a>: April 14, 2015; Santiago, Chile.</span></li>
<li><span style="font-size: 13.0080003738403px; line-height: 1.538em; background-color: transparent;"><a href="http://www.meetup.com/espacio_RES/events/221651979/" target="_blank">OpenStack Sevilla meetup</a>: April 15, 2015; Sevilla, Spain.</span></li>
<li><span style="font-size: 13.0080003738403px; line-height: 1.538em; background-color: transparent;"><a href="http://www.meetup.com/Openstack-Brasil/events/221384769/" target="_blank">OpenStack Brasil hangout</a>: April 15, 2015; Porto Alegre, Brazil.</span></li>
</ul><p><span style="font-size: 13.0080003738403px; line-height: 1.538em; background-color: transparent;">And don't forget to mark your calendar for the big event!</span></p>
<ul><li><a href="https://www.openstack.org/summit/vancouver-2015/" target="_blank" style="font-size: 13.0080003738403px; line-height: 1.538em; background-color: transparent;">OpenStack Summit</a>: May 18-22, 2015; Vancouver, BC.</li>
</ul><p>Interested in the OpenStack project development meetings? A complete list of these meetings' regular schedules is available <a href="https://wiki.openstack.org/wiki/Meetings" target="_blank" title="OpenStack Meetings">here</a>.</p>
<p>Thanks to Stefano Maffulli and the <a href="http://www.openstack.org/blog/2015/04/openstack-community-weekly-newsletter-apr-3-10/" target="_blank">OpenStack Community Weekly Newsletter</a> for linking us to several of the stories in this article.</p>
