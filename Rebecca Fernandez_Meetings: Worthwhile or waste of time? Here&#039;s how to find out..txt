<p>The pointless, endless office meeting. We've all been stuck in one. And if your department is like most, there's no transparent means to determine which conference room gatherings are highly productive and which are taking valuable time away from the business.</p>
<p>So here's a simple technique to hold your co-workers and management accountable for the meetings they call.</p>
<!--break-->
<p>Get management to agree to place a box and a basket of small cards in every conference room. At the close of each meeting, pass out the cards and ask everyone to rate the meeting using the following scale:</p>
<p style="margin-bottom: 0in; padding-left: 30px;"><strong>5</strong> – Crucial. Have new, essential information, was assigned a set of important action items, or made key decisions.</p>
<p style="margin-bottom: 0in; padding-left: 30px;"><strong>4</strong> – Important. Took away valuable information, made tentative or useful plans, or have a set of action items that really need to be completed.</p>
<p style="margin-bottom: 0in; padding-left: 30px;"><strong>3 </strong>– Should have ended sooner. Important material was covered along with less valuable information.</p>
<p style="margin-bottom: 0in; padding-left: 30px;"><strong>2 </strong>– Next time just send an email.</p>
<p style="margin-bottom: 0in; padding-left: 30px;"><strong>1 </strong>– A complete waste of time and resources.</p>
<p>Pass around the box to place the anonymous cards into.</p>
<p>But don't just give the information to the meeting leader. Here's where transparency comes in. Place it on a chart in a common area or spreadsheet that is regularly emailed to the department.</p>
<p>Alternately, you could calculate the cost of every meeting, using <a href="http://www.payscale.com/meeting-miser">Meeting Miser</a>.</p>
<p>Rather than using the information in a divisive manner, work as a department to adjust your meeting habits and make the numbers more agreeable.</p>
<p>And if you're really brave, clear your throat the next time a meeting begins to spiral downward, and ask, "Shall we go ahead and rate this meeting, then?"</p>
