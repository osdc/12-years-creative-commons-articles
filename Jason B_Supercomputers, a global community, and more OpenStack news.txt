<p>Are you interested in keeping track of what is happening in the open source cloud? Opensource.com is your source for news in <a href="https://opensource.com/resources/what-is-openstack" target="_blank" title="What is OpenStack?">OpenStack</a>, the open source cloud infrastructure project.</p>
<hr /><h3>OpenStack around the web</h3>
<p>There is a lot of interesting stuff being written about OpenStack. Here's a sampling from some of our favorites:</p>
<ul><li><a href="https://osic.org/blogs/big-ideas-contest-showcases" target="_blank">Cray’s latest supercomputer runs OpenStack and open source big data tools</a>: Supercomputer manufacturer turns to OpenStack for newest solution.</li>
<li><a href="http://www.geektime.com/2016/05/26/from-austin-to-tel-aviv-the-openstack-community-goes-from-local-to-global/" target="_blank">From Austin to Tel Aviv</a>: The OpenStack community goes from local to global.</li>
<li><a href="http://getcloudify.org/2016/05/18/developing-openstack-api-experience-giving-first-summit-talk-cloud-orchestration-tosca.html" target="_blank">Developing for OpenStack APIs</a>: One developer's experience giving his first summit talk.</li>
<li><a href="https://osic.org/blogs/big-ideas-contest-showcases" target="_blank">Big ideas contest showcases OpenStack innovation</a>: Demonstrating scalability, interoperability, and performance at scale.</li>
<li><a href="http://superuser.openstack.org/articles/what-s-in-the-cards-for-openstack" target="_blank">What's in the cards for OpenStack</a>: Three community members talk bigger, badder startups, fewer "experimental" deployments, and more.</li>
<li><a href="http://superuser.openstack.org/articles/how-ancestry-com-s-open-source-strategy-combines-kubernetes-and-openstack" target="_blank">How Ancestry.com's open source strategy combines Kubernetes and OpenStack</a>: Scaling storage for more than 2 million users searching for their roots.</li>
<li><a href="https://major.io/2016/05/24/test-fedora-24-beta-openstack-cloud/" target="_blank">Test Fedora 24 Beta in an OpenStack cloud</a>: Get a sneak peek at upcoming features.</li>
<li><a href="http://superuser.openstack.org/articles/how-burton-snowboards-is-carving-down-the-openstack-trail" target="_blank">How Burton Snowboards is carving down the OpenStack trail</a>: Agile on the slopes and OpenStack Swift in the data center.</li>
<li><a href="https://robhirschfeld.com/2016/05/24/open-source-as-reality-tv-and-burning-data-centers-gcondemand-podcast-notes/" target="_blank">Open source as reality TV and burning data centers</a>: Notes from an OpenStack podcast.</li>
</ul><h3>OpenStack discussions</h3>
<p>Here's a sample of some of the most active discussions this week on the OpenStack developers' listserv. For a more in-depth look, why not <a href="http://lists.openstack.org/pipermail/openstack-dev/" target="_blank" title="OpenStack listserv">dive in</a> yourself?</p>
<ul><li><a href="http://lists.openstack.org/pipermail/openstack-dev/2016-May/095654.html" target="_blank">I'm going to expire open bug reports older than 18 months</a>: Retiring old bugs in Nova.</li>
<li><a href="http://lists.openstack.org/pipermail/openstack-dev/2016-May/095667.html" target="_blank">Mentors needed in specific technical areas</a>: Growing the size of the OpenStack mentoring community.</li>
<li><a href="http://lists.openstack.org/pipermail/openstack-dev/2016-May/095749.html" target="_blank">Log spool in the context</a>: Two options for adding a spooling logger to Oslo.</li>
<li><a href="http://lists.openstack.org/pipermail/openstack-dev/2016-May/096071.html" target="_blank">Leadership training</a>: Ideas to include those not in the room.</li>
</ul><h3>Cloud &amp; OpenStack events</h3>
<p>Here's what is happening this week. Something missing? <a href="mailto:osdc-admin@redhat.com">Email</a> us and let us know, or add it to our <a href="https://opensource.com/resources/conferences-and-events-monthly" target="_blank" title="Events Calendar">events calendar</a>.</p>
<ul><li><a href="http://www.meetup.com/Australian-OpenStack-User-Group/events/229278942/" target="_blank">Australian OpenStack user group meetup</a>: Monday, May 30; Brisbane, Australia.</li>
<li><a href="http://www.meetup.com/Turkey-OpenStack-Meetup/events/231072196/" target="_blank">2016 OpenStack Days</a>: Tuesday, May 31; Istanbul, Turkey.</li>
<li><a href="http://www.meetup.com/OpenStack-MeetUp-Frankfurt/events/230922309/" target="_blank">Austin Summit recap</a>: Tuesday, May 31; Frankfurt, Germany.</li>
<li><a href="http://www.meetup.com/OpenStack-DFW/events/230292911/" target="_blank">Mitaka overview</a>: Wednesday, June 1; Richardson, TX.</li>
<li><a href="http://www.meetup.com/OpenStack-Israel/events/229279741/" target="_blank">OpenStack Day Israel 2016</a>: Thursday, June 2; Tel Aviv, Israel.</li>
<li><a href="http://www.meetup.com/Openstack-Netherlands/events/226986961/" target="_blank">OpenStack hardware integration and service modeling</a>: Thursday, June 2; Amersfoort, Netherlands.</li>
<li><a href="http://www.meetup.com/OpenStack-Barcelona/events/231358802/" target="_blank">OpenStack and containers</a>: Thursday, June 2; Barcelona, Spain.</li>
</ul><p><em>Interested in the OpenStack project development meetings? A complete list of these meetings' regular schedules is available <a href="https://wiki.openstack.org/wiki/Meetings" target="_blank" title="OpenStack Meetings">here</a>.</em></p>
