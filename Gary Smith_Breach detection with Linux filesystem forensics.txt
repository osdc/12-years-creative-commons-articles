<p>Forensic analysis of a Linux disk image is often part of incident response to determine if a breach has occurred. Linux forensics is a different and fascinating world compared to Microsoft Windows forensics. In this article, I will analyze a disk image from a potentially compromised Linux system in order to determine the who, what, when, where, why, and how of the incident and create event and filesystem timelines. Finally, I will extract artifacts of interest from the disk image.</p>
<p>In this tutorial, we will use some new tools and some old tools in creative, new ways to perform a forensic analysis of a disk image.</p>
<h2>The scenario</h2>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Linux resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux commands cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Free online course: RHEL Technical Overview</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux networking cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">SELinux cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux common commands cheat sheet</a></li>
<li><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">What are Linux containers?</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Our latest Linux articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>Premiere Fabrication Engineering (PFE) suspects there has been an incident or compromise involving the company's main server named pfe1. They believe the server may have been involved in an incident and may have been compromised sometime between the first of March and the last of March. They have engaged my services as a forensic examiner to investigate if the server was compromised and involved in an incident. The investigation will determine the who, what, when, where, why, and how behind the possible compromise. Additionally, PFE has requested my recommendations for further security measures for their servers.</p>
<h2>The disk image</h2>
<p>To conduct the forensic analysis of the server, I ask PFE to send me a forensic disk image of pfe1 on a USB drive. They agree and say, "the USB is in the mail." The USB drive arrives, and I start to examine its contents. To conduct the forensic analysis, I use a virtual machine (VM) running the SANS SIFT distribution. The <a href="https://digital-forensics.sans.org/community/downloads" target="_blank">SIFT Workstation</a> is a group of free and open source incident response and forensic tools designed to perform detailed digital forensic examinations in a variety of settings. SIFT has a wide array of forensic tools, and if it doesn't have a tool I want, I can install one without much difficulty since it is an Ubuntu-based distribution.</p>
<p>Upon examination, I find the USB doesn't contain a disk image, rather copies of the VMware ESX host files, which are VMDK files from PFE's hybrid cloud. This was not what I was expecting. I have several options:</p>
<ol><li>I can contact PFE and be more explicit about what I am expecting from them. Early in an engagement like this, it might not be the best thing to do.</li>
<li>I can load the VMDK files into a virtualization tool such as VMPlayer and run it as a live VM using its native Linux programs to perform forensic analysis. There are at least three reasons not to do this. First, timestamps on files and file contents will be altered when running the VMDK files as a live system. Second, since the server is thought to be compromised, every file and program of the VMDK filesystems must be considered compromised. Third, using the native programs on a compromised system to do a forensic analysis may have unforeseen consequences.</li>
<li>To analyze the VMDK files, I could use the libvmdk-utils package that contains tools to access data stored in VMDK files.</li>
<li>However, a better approach is to convert the VMDK file format into RAW format. This will make it easier to run the different tools in the SIFT distribution on the files in the disk image.</li>
</ol><p>To convert from VMDK to RAW format, I use the <a href="http://manpages.ubuntu.com/manpages/trusty/man1/qemu-img.1.html" target="_blank">qemu-img</a> utility, which allows creating, converting, and modifying images offline. The following figure shows the command to convert the VMDK format into a RAW format.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Converting a VMDK file to RAW format"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/linuxfilesystemforensics_1.png" width="700" height="70" alt="Converting a VMDK file to RAW format" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>Next, I need to list the partition table from the disk image and obtain information about where each partition starts (sectors) using the <a href="http://manpages.ubuntu.com/manpages/trusty/man1/mmls.1.html" target="_blank">mmls</a> utility. This utility displays the layout of the partitions in a volume system, including partition tables and disk labels. Then I use the starting sector and query the details associated with the filesystem using the <a href="http://manpages.ubuntu.com/manpages/artful/en/man1/fsstat.1.html" target="_blank">fsstat</a> utility, which displays the details associated with a filesystem. The figures below show the <code>mmls</code> and <code>fsstat</code> commands in operation.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="mmls command output"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/linuxfilesystemforensics_2.png" width="700" height="215" alt="mmls command output" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>I learn several interesting things from the <code>mmls</code> output: A Linux primary partition starts at sector 2048 and is approximately 8 gigabytes in size. A DOS partition, probably the boot partition, is approximately 8 megabytes in size. Finally, there is a swap partition of approximately 8 gigabytes.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="fsstat command output"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/linuxfilesystemforensics_3.png" width="700" height="322" alt="fsstat command output" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>Running <code>fsstat</code> tells me many useful things about the partition: the type of filesystem, the last time data was written to the filesystem, whether the filesystem was cleanly unmounted, and where the filesystem was mounted.</p>
<p>I'm ready to mount the partition and start the analysis. To do this, I need to read the partition tables on the raw image specified and create device maps over partition segments detected. I could do this by hand with the information from <code>mmls</code> and <code>fsstat</code>—or I could use <a href="http://manpages.ubuntu.com/manpages/trusty/man8/kpartx.8.html" target="_blank">kpartx</a> to do it for me.</p>
<p><article class="media media--type-image media--view-mode-full" title="Using kpartx to create loopback devices"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/linuxfilesystemforensics_4.png" width="700" height="100" alt="Using kpartx to create loopback devices" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>I use options to create read-only mapping (<code>-r</code>), add partition mapping (<code>-a</code>), and give verbose output (<code>-v</code>). The <code>loop0p1</code> is the name of a device file under <code>/dev/mapper</code> I can use to access the partition. To mount it, I run:</p>
<pre>
<code class="language-text">$ mount -o ro -o loop=/dev/mapper/loop0p1 pf1.raw /mnt</code></pre><p>Note that I'm mounting the partition as read-only (<code>-o ro</code>) to prevent accidental contamination.</p>
<p>After mounting the disk, I start my forensic analysis and investigation by creating a timeline. Some forensic examiners don't believe in creating a timeline. Instead, once they have a mounted partition, they creep through the filesystem looking for artifacts that might be relevant to the investigation. I label these forensic examiners "creepers." While this is one way to forensically investigate, it is far from repeatable, is prone to error, and may miss valuable evidence.</p>
<p>I believe creating a timeline is a crucial step because it includes useful information about files that were modified, accessed, changed, and created in a human-readable format, known as MAC (modified, accessed, changed) time evidence. This activity helps identify the specific time and order an event took place.</p>
<h2>Notes about Linux filesystems</h2>
<p>Linux filesystems like ext2 and ext3 don't have timestamps for a file's creation/birthtime. The creation timestamp was introduced in ext4. The book<em> <a href="https://www.amazon.com/Forensic-Discovery-paperback-Dan-Farmer/dp/0321703251" target="_blank">Forensic Discovery</a></em> (1st edition) by Dan Farmer and Wietse Venema outlines the different timestamps.</p>
<ul><li><strong>Last modification time:</strong> For directories, this is the last time an entry was added, renamed, or removed. For other file types, it's the last time the file was written to.</li>
<li><strong>Last access (read) time:</strong> For directories, this is the last time it was searched. For other file types, it's the last time the file was read.</li>
<li><strong>Last status change:</strong> Examples of status changes are change of owner, change of access permission, change of hard link count, or an explicit change of any of the MAC times.</li>
<li><strong>Deletion time:</strong> ext2 and ext3 record the time a file was deleted in the <code>dtime</code> timestamp, but not all tools support it.</li>
<li><strong>Creation time:</strong> ext4fs records the time the file was created in the <code>crtime</code> timestamp, but not all tools support it.</li>
</ul><p>The different timestamps are stored in the metadata contained in the inodes. Inodes are similar to the MFT entry number in the Windows world. One way to read the file metadata on a Linux system is to first get the inode number using the command <code>ls -i file</code> then use <code>istat</code> against the partition device and specify the inode number. This will show you the different metadata attributes, including the timestamps, the file size, owner's group and user id, permissions, and the blocks that contain the actual data.</p>
<h2>Creating the super timeline</h2>
<p>My next step is to create a super timeline using log2timeline/plaso. <a href="https://github.com/log2timeline/plaso" target="_blank">Plaso</a> is a Python-based rewrite of the Perl-based log2timeline tool initially created by Kristinn Gudjonsson and enhanced by others. It's easy to make a super timeline with log2timeline, but interpretation is difficult. The latest version of the plaso engine can parse the ext4 as well as different type of artifacts, such as syslog messages, audit, utmp, and others.</p>
<p>To create the super timeline, I launch log2timeline against the mounted disk folder and use the Linux parsers. This process takes some time; when it finishes I have a timeline with the different artifacts in plaso database format, then I can use <code>psort.py</code> to convert the plaso database into any number of different output formats. To see the output formats that <code>psort.py</code> supports, enter <code>psort -o list</code>. I used <code>psort.py</code> to create an Excel-formatted super timeline. The figure below outlines the steps to perform this operation.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Creating a super timeline in. xslx format"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/linuxfilesystemforensics_5.png" width="700" height="66" alt="Creating a super timeline in. xslx format" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p><em>(Note: extraneous lines removed from images)</em></p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Creating a super timeline in. xslx format"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/linuxfilesystemforensics_5a_0.png" width="700" height="26" alt="Creating a super timeline in. xslx format" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>I import the super timeline into a spreadsheet program to make viewing, sorting, and searching easier. While you can view a super timeline in a spreadsheet program, it's easier to work with it in a real database such as MySQL or Elasticsearch. I create a second super timeline and dispatch it directly to an Elasticsearch instance from <code>psort.py</code>. Once the super timeline has been indexed by Elasticsearch, I can visualize and analyze the data with <a href="https://www.elastic.co/products/kibana" target="_blank">Kibana</a>.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Creating a super timeline and ingesting it into Elasticsearch"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/linuxfilesystemforensics_6.png" width="700" height="27" alt="Creating a super timeline and ingesting it into Elasticsearch" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<h2>Investigating with Elasticsearch/Kibana</h2>
<p>As <a href="http://allyouneediskill.wikia.com/wiki/Master_Sergeant_Farell" target="_blank">Master Sergeant Farrell</a> said, "Through readiness and discipline, we are masters of our fate." During the analysis, it pays to be patient and meticulous and avoid being a creeper. One thing that helps a super timeline analysis is to have an idea of when the incident may have happened. In this case (pun intended), the client says the incident may have happened in March. I still consider the possibility the client is incorrect about the timeframe. Armed with this information, I start reducing the super timeline's timeframe and narrowing it down. I'm looking for artifacts of interest that have a "temporal proximity" with the supposed date of the incident. The goal is to recreate what happened based on different artifacts.</p>
<p>To narrow the scope of the super timeline, I use the Elasticsearch/Kibana instance I set up. With Kibana, I can set up any number of intricate dashboards to display and correlate forensic events of interest, but I want to avoid this level of complexity. Instead, I select indexes of interest for display and create a bar graph of activity by date:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Activity on pfe1 over time"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/linuxfilesystemforensics_7.png" width="700" height="175" alt="Activity on pfe1 over time" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>The next step is to expand the large bar at the end of the chart:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Activity on pfe1 during March"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/linuxfilesystemforensics_8.png" width="700" height="117" alt="Activity on pfe1 during March" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>There is a large bar on 05-Mar. I expand that bar out to see the activity on that particular date:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Activity on pfe1 on 05-Mar"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/linuxfilesystemforensics_9.png" width="700" height="117" alt="Activity on pfe1 on 05-Mar" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>Looking at the logfile activity from the super timeline, I see this activity was from a software install/upgrade. There is very little to be found in this area of activity.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Log listing from pfe1 on 05-Mar"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/linuxfilesystemforensics_10.png" width="700" height="248" alt="Log listing from pfe1 on 05-Mar" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>I go back to Kibana to see the last set of activities on the system and find this in the logs:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Last activity on pfe1 before shutdown"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/linuxfilesystemforensics_11.png" width="700" height="195" alt="Last activity on pfe1 before shutdown" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>One of the last activities on the system was user john installed a program from a directory named xingyiquan. Xing Yi Quan is a style of Chinese martial arts similar to Kung Fu and Tai Chi Quan. It seems odd that user john would install a martial arts program on a company server from his own user account. I use Kibana's search capability to find other instances of xingyiquan in the logfiles. I found three periods of activity surrounding the string xingyiquan on 05-Mar, 09-Mar, and 12-Mar.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="xingyiquan&amp;nbsp;activity on pfe1"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/linuxfilesystemforensics_12.png" width="700" height="111" alt="xingyiquan&amp;nbsp;activity on pfe1" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>Next, I look at the log entries for these days. I start with 05-Mar and find evidence of an internet search using the Firefox browser and the Google search engine for a rootkit named xingyiquan. The Google search found the existence of such a rootkit on packetstormsecurity.com. Then, the browser went to packetstormsecurity.com and downloaded a file named <code>xingyiquan.tar.gz</code> from that site into user john's download directory.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Search and download of xingyiquan.tar.gz"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/linuxfilesystemforensics_13.png" width="700" height="171" alt="Search and download of xingyiquan.tar.gz" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>Although it appears user john went to google.com to search for the rootkit and then to packetstormsecurity.com to download the rootkit, these log entries do not indicate the user behind the search and download. I need to look further into this.</p>
<p>The Firefox browser keeps its history information in an SQLite database under the <code>.mozilla</code> directory in a user's home directory (i.e., user john) in a file named <code>places.sqlite</code>. To view the information in the database, I use a program called <a href="http://sqlitebrowser.org/" target="_blank">sqlitebrowser</a>. It's a GUI application that allows a user to drill down into an SQLite database and view the records stored there. I launched sqlitebrowser and imported <code>places.sqlite</code> from the <code>.mozilla</code> directory under user john's home directory. The results are shown below.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Search and download history of user john"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/linuxfilesystemforensics_14.png" width="700" height="96" alt="Search and download history of user john" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>The number in the far-right column is the timestamp for the activity on the left. As a test of congruence, I converted the timestamp <code>1425614413880000</code> to human time and got March 5, 2015, 8:00:13.880 PM. This matches closely with the time March 5th, 2015, 20:00:00.000 from Kibana. We can say with reasonable certainty that user john searched for a rootkit named xingyiquan and downloaded a file from packetstormsecurity.com named <code>xingyiquan.tar.gz</code> to user john's download directory.</p>
<h2>Investigating with MySQL</h2>
<p>At this point, I decide to import the super timeline into a MySQL database to gain greater flexibility in searching and manipulating data than Elasticsearch/Kibana alone allows.</p>
<h2>Building the xingyiquan rootkit</h2>
<p>I load the super timeline I created from the plaso database into a MySQL database. From working with Elasticsearch/Kibana, I know that user john downloaded the rootkit <code>xingyiquan.tar.gz</code> from packetstormsecurity.com to the download directory. Here is evidence of the download activity from the MySQL timeline database:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Downloading the xingyiquan.tar.gz rootkit"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/linuxfilesystemforensics_15.png" width="700" height="183" alt="Downloading the xingyiquan.tar.gz rootkit" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>Shortly after the rootkit was downloaded, the source from the <code>tar.gz</code> archive was extracted.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Extracting the rootkit source from the tar.gz archive"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/linuxfilesystemforensics_16.png" width="700" height="283" alt="Extracting the rootkit source from the tar.gz archive" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>Nothing was done with the rootkit until 09-Mar, when the bad actor read the README file for the rootkit with the More program, then compiled and installed the rootkit.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Building the xingyiquan rootkit"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/linuxfilesystemforensics_17.png" width="700" height="235" alt="Building the xingyiquan rootkit" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<h2>Command histories</h2>
<p>I load histories of all the users on pfe1 that have <code>bash</code> command histories into a table in the MySQL database. Once the histories are loaded, I can easily display them using a query like:</p>
<pre>
<code class="language-text">select * from histories order by recno;</code></pre><p>To get a history for a specific user, I use a query like:</p>
<pre>
<code class="language-text">select historyCommand from histories where historyFilename like '%&lt;username&gt;%' order by recno;</code></pre><p>I find several interesting commands from user john's <code>bash</code> history. Namely, user john created the johnn account, deleted it, created it again, copied <code>/bin/true</code> to <code>/bin/false</code>, gave passwords to the whoopsie and lightdm accounts, copied <code>/bin/bash</code> to <code>/bin/false</code>, edited the password and group files, moved the user johnn's home directory from <code>johnn</code> to <code>.johnn</code>, (making it a hidden directory), changed the password file using <code>sed</code> after looking up how to use <sed>sed</sed>, and finally installed the xingyiquan rootkit.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="User john's activity"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/linuxfilesystemforensics_18.png" width="445" height="443" alt="User john's activity" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>Next, I look at the <code>bash</code> command history for user johnn. It showed no unusual activity.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="User johnn's activity"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/linuxfilesystemforensics_19.png" width="160" height="81" alt="User johnn's activity" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>Noting that user john copied <code>/bin/bash</code> to <code>/bin/false</code>, I test whether this was true by checking the sizes of these files and getting an MD5 hash of the files. As shown below, the file sizes and the MD5 hashes are the same. Thus, the files are the same.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Checking /bin/bash and /bin/false"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/linuxfilesystemforensics_20.png" width="505" height="125" alt="Checking /bin/bash and /bin/false" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<h2>Investigating successful and failed logins</h2>
<p>To answer part of the "when" question, I load the logfiles containing data on logins, logouts, system startups, and shutdowns into a table in the MySQL database. Using a simple query like: </p>
<pre>
<code class="language-text">select * from logins order by start</code></pre><p>I find the following activity:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Successful logins to pfe1"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/linuxfilesystemforensics_21.png" width="700" height="117" alt="Successful logins to pfe1" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>From this figure, I see that user john logged into pfe1 from IP address <code>192.168.56.1</code>. Five minutes later, user johnn logged into pfe1 from the same IP address. Two logins by user lightdm followed four minutes later and another one minute later, then user johnn logged in less than one minute later. Then pfe1 was rebooted.</p>
<p>Looking at unsuccessful logins, I find this activity:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Unsuccessful logins to pfe1"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/linuxfilesystemforensics_22.png" width="700" height="34" alt="Unsuccessful logins to pfe1" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>Again, user lightdm attempted to log into pfe1 from IP address <code>192.168.56.1</code>. In light of bogus accounts logging into pfe1, one of my recommendations to PFE will be to check the system with IP address <code>192.168.56.1</code> for evidence of compromise.</p>
<h2>Investigating logfiles</h2>
<p>This analysis of successful and failed logins provides valuable information about when events occurred. I turn my attention to investigating the logfiles on pfe1, particularly the authentication and authorization activity in <code>/var/log/auth*</code>. I load all the logfiles on pfe1 into a MySQL database table and use a query like:</p>
<pre>
<code class="language-text">select logentry from logs where logfilename like '%auth%' order by recno;</code></pre><p>and save that to a file. I open that file with my favorite editor and search for <code>192.168.56.1</code>. Following is a section of the activity:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Account activity on pfe1"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/u128651/linuxfilesystemforensics_23.png" width="700" height="349" alt="Account activity on pfe1" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p>This section shows that user john logged in from IP address <code>192.168.56.1</code> and created the johnn account, removed the johnn account, and created it again. Then, user johnn logged into pfe1 from IP address <code>192.168.56.1</code>. Next, user johnn attempted to become user whoopsie with an <code>su</code> command, which failed. Then, the password for user whoopsie was changed. User johnn next attempted to become user lightdm with an <code>su</code> command, which also failed. This correlates with the activity shown in Figures 21 and 22.</p>
<h2>Conclusions from my investigation</h2>
<ul><li>User john searched for, downloaded, compiled, and installed a rootkit named xingyiquan onto the server pfe1. The xingyiquan rootkit hides processes, files, directories, processes, and network connections; adds backdoors; and more.</li>
<li>User john created, deleted, and recreated another account on pfe1 named johnn. User john made the home directory of user johnn a hidden file to obscure the existence of this user account.</li>
<li>User john copied the file <code>/bin/true</code> over <code>/bin/false</code> and then <code>/bin/bash</code> over <code>/bin/false</code> to facilitate the logins of system accounts not normally used for interactive logins.</li>
<li>User john created passwords for the system accounts whoopsie and lightdm. These accounts normally do not have passwords.</li>
<li>The user account johnn was successfully logged into and user johnn unsuccessfully attempted to become users whoopsie and lightdm.</li>
<li>Server pfe1 has been seriously compromised.</li>
</ul><h2>My recommendations to PFE</h2>
<ul><li>Rebuild server pfe1 from the original distribution and apply all relevant patches to the system before returning it to service.</li>
<li>Set up a centralized syslog server and have all systems in the PFE hybrid cloud log to the centralized syslog server <em>and</em> to local logs to consolidate log data and prevent tampering with system logs. Use a security information and event monitoring (SIEM) product to facilitate security event review and correlation.</li>
<li>Implement <code>bash</code> command timestamps on all company servers.</li>
<li>Enable audit logging of the root account on all PFE servers and direct the audit logs to the centralized syslog server where they can be correlated with other log information.</li>
<li>Investigate the system with IP address <code>192.168.56.1</code> for breaches and compromises, as it was used as a pivot point in the compromise of pfe1.</li>
</ul><p>If you have used forensics to analyze your Linux filesystem for compromises, please share your tips and recommendations in the comments.</p>
<hr /><p><em>Gary Smith will be speaking at LinuxFest Northwest this year. See <a href="https://www.linuxfestnorthwest.org/conferences/lfnw18" target="_blank">program highlights</a> or <a href="https://www.linuxfestnorthwest.org/conferences/lfnw18/register/new" target="_blank">register to attend</a>.</em></p>
