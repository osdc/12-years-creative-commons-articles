<p>We live in a busy world and can save time and effort by using the ls command when we need to look for files and data. But without a lot of tweaking, the default ls output isn't quite soothing to the eyes. Why spend your time squinting at black and white text when you have an alternative in exa?</p>
<p><a href="https://the.exa.website/docs" target="_blank">Exa</a> is a modern-day replacement for the regular ls command, and it makes life easier. The tool is written in <a href="https://opensource.com/tags/rust">Rust</a>, which is known for its parallelism and safety.</p>
<h2 id="install-exa">Install exa</h2>
<p>To install exa, run:</p>
<pre><code class="language-bash">$ dnf install exa</code></pre><h2 id="explore-exa's-features">Explore exa's features</h2>
<p>Exa improves upon the ls file list with more features and better defaults. It uses colors to distinguish file types and metadata. It knows about symlinks, extended attributes, and Git. And it's small, fast, and has just a single binary.</p>
<h3 id="track-files">Track files</h3>
<p>You can use exa to track a new file added in a given Git repo.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Tracking Git files with exa"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/exa_trackingfiles.png" width="675" height="237" alt="Tracking Git files with exa" title="Tracking Git files with exa" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Sudeshna Sur, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<h3 id="tree-structure">Tree structure</h3>
<p>This is exa's basic tree structure. The level determines the depth of the listing; this is set to two. If you want to list more subdirectories and files, increase the level's value.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="exa's default tree structure"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/exa_treestructure.png" width="660" height="551" alt="exa's default tree structure" title="exa's default tree structure" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Sudeshna Sur, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>This tree includes a lot of metadata about each file.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Metadata in exa's tree structure"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/exa_metadata.png" width="675" height="390" alt="Metadata in exa's tree structure" title="Metadata in exa's tree structure" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Sudeshna Sur, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<h3 id="color-schemes">Color schemes</h3>
<p>By default, exa segregates different file types according to <a href="https://the.exa.website/features/colours" target="_blank">built-in color schemes</a>. It not only color-codes files and directories, but also Cargo.toml, CMakeLists.txt, Gruntfile.coffee, Gruntfile.js, Makefile, and many other file types.</p>
<h3 id="extended-file-attributes">Extended file attributes</h3>
<p>When you're exploring xattrs (extended file attributes) in exa, <code>--extended</code> will show up in all the xattrs.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="xattrs in exa"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/exa_xattrs.png" width="675" height="130" alt="xattrs in exa" title="xattrs in exa" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Sudeshna Sur, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<h3 id="symlinks">Symlinks</h3>
<p>Exa understands symlinks and also points out the actual file.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="symlinks in exa"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/exa_symlinks.png" width="675" height="162" alt="symlinks in exa" title="symlinks in exa" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Sudeshna Sur, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<h3 id="recurse">Recurse</h3>
<p>When you want to loop listings for all directories under the current directory, exa brings in recurse.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="recurse in exa"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/exa_recurse.png" width="675" height="359" alt="recurse in exa" title="recurse in exa" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Sudeshna Sur, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<h2 id="conclusion">Conclusion</h2>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Linux resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux commands cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Free online course: RHEL Technical Overview</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux networking cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">SELinux cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux common commands cheat sheet</a></li>
<li><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">What are Linux containers?</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Our latest Linux articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>I believe exa is one of the easiest, most adaptable tools. It helps me track a lot of Git and Maven files. Its color-coding makes it easier for me to search through multiple subdirectories, and it helps me to understand the current xattrs.</p>
<p>Have you replaced ls with exa? Please share your feedback in the comments.</p>
