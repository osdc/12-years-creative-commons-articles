<p>Last month, we put the Raspberry Pi, a tiny $25 Linux computer, in our <a href="https://opensource.com/life/11/11/open-source-gift-guide-and-giveaway-our-answer-cyber-monday">open source gift guide</a>. It led overwhelmingly as your favorite on the list. But other similar options have been popping up, like the Allwinner A10 ($15) and the CuBox (quite a bit more).</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on Raspberry Pi</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/resources/what-raspberry-pi?src=raspberry_pi_resource_menu1&amp;intcmp=701f2000000h4RcAAI">What is Raspberry Pi?</a></li>
<li><a href="https://opensource.com/downloads/raspberry-pi-guide?src=raspberry_pi_resource_menu2&amp;intcmp=701f2000000h4RcAAI">eBook: Guide to Raspberry Pi</a></li>
<li><a href="https://opensource.com/downloads/getting-started-raspberry-pi-cheat-sheet?src=raspberry_pi_resource_menu3&amp;intcmp=701f2000000h4RcAAI">Getting started with Raspberry Pi cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/kubernetes-raspberry-pi?src=raspberry_pi_resource_menu6&amp;intcmp=701f2000000h4RcAAI">eBook: Running Kubernetes on your Raspberry Pi</a></li>
<li><a href="https://www.redhat.com/en/resources/data-intensive-applications-hybrid-cloud-blueprint-detail?src=raspberry_pi_resource_menu7&amp;intcmp=701f2000000h4RcAAI">Whitepaper: Data-intensive intelligent applications in a hybrid cloud blueprint</a></li>
<li><a href="https://www.redhat.com/en/topics/edge-computing?src=raspberry_pi_resource_menu5&amp;intcmp=701f2000000h4RcAAI">Understanding edge computing</a></li>
<li><a href="https://opensource.com/tags/raspberry-pi?src=raspberry_pi_resource_menu4&amp;intcmp=701f2000000h4RcAAI">Our latest on Raspberry Pi </a></li>
</ul></div>
</div>
</div>
</div>

<ul></ul><p>The <a href="http://www.raspberrypi.org/">Raspberry Pi</a> is a tiny computer that uses your TV as a monitor. The UK team developing it hopes kids will be able to take advantage of the low price and use it to learn programming. Although it was expected in December, this week the Raspberry Pi finally began manufacturing.</p>
<p>The <a href="http://rhombus-tech.net/allwinner_a10/">Allwinner A10</a> is being developed in China by Rhombus Tech to give the free software community--hobbyists and entrepreneurs--ineepensive, open-source hardware.</p>
<!-- p { margin-bottom: 0.08in; } --><p>In the category of tiny-but-not-as-cheap, there’s the much more polished <a href="http://www.solid-run.com/products/cubox">CuBox</a>. Unlike the others, the CuBox is already in a case. The intended applications are anything from digital signage (as it’s small enough to hide almost anywhere) to a media box or Android TV to a developer’s machine. In fact, for the latter, the CuBox is “unbrickable,” in that it comes with recovery features that work even if the bootloader was completely erased.</p>
<p>A few things separate the CuBox from the Raspberry Pi and Allwinner machines. The first is that it’s more than $100 more expensive. But also unlike the other two, the CuBox comes in a box with a power supply and a 2GB microSD card (preloaded with Ubuntu), and perhaps above all, they’re already shipping.</p>
<p>The first two are roughly 85mm x 55mm (3.5" x 2.2"), or about the size of a credit card. The CuBox is a 2" x 2" x 2" cube. Here's how the rest of their specs compare:</p>
<table border="1" cellpadding="4" cellspacing="0" width="100%"><tbody><tr valign="TOP"><td width="25%">
<p> </p>
</td>
<td width="25%">
<p><strong>Raspberry Pi Model A</strong></p>
</td>
<td width="25%">
<p><strong>Allwinner A10/ARM Cortex A8</strong></p>
</td>
<td width="25%">
<p><strong>CuBox</strong></p>
</td>
</tr><tr valign="TOP"><td width="25%">
<p><strong>Price</strong></p>
</td>
<td width="25%">
<p>$25 ($35 for Model B)</p>
</td>
<td width="25%">
<p><span style="text-decoration: line-through;">$15</span> (*see correction below) </p>
</td>
<td width="25%">
<p>$129</p>
</td>
</tr><tr valign="TOP"><td width="25%">
<p><strong>CPU</strong></p>
</td>
<td width="25%">
<p>700 MHz ARM core (ARM 11)</p>
</td>
<td width="25%">
<p>1.5 GHz Cortex A8 ARM core</p>
</td>
<td width="25%">
<p>800Mhz Marvell Armada 510 processor (ARM7)</p>
</td>
</tr><tr valign="TOP"><td width="25%">
<p><strong>Video</strong></p>
</td>
<td width="25%">
<p>1080p hardware-accelerated video playback; HDMI or composite RCA</p>
</td>
<td width="25%">
<p>2160p hardware-accelerated video playback; 2 24-pin RGB/TTL interfaces, HDMI out</p>
</td>
<td width="25%">
<p>1080p video playback; HDMI</p>
</td>
</tr><tr valign="TOP"><td width="25%">
<p><strong>GPU</strong></p>
</td>
<td width="25%">
<p>Broadcom VideoCore IV, OpenGL ES 2.0, 1080p30 h.264/MPEG-4 AVC high-profile decode</p>
<p> </p>
</td>
<td width="25%">
<p>MALI400MP OpenGL ES 2.0 GPU</p>
</td>
<td width="25%">
<p>OpenGL Embedded Standard 2.0 Graphics Engine</p>
</td>
</tr><tr valign="TOP"><td width="25%">
<p><strong>RAM</strong></p>
</td>
<td width="25%">
<p>128 MB RAM (256 MB on Model B)</p>
</td>
<td width="25%">
<p>up to 1gb RAM</p>
</td>
<td width="25%">
<p>1 GB</p>
</td>
</tr><tr valign="TOP"><td width="25%">
<p><strong>NAND Flash controller?</strong></p>
</td>
<td width="25%">
<p>No</p>
</td>
<td width="25%">
<p>Yes</p>
</td>
<td width="25%">
<p>MicroSD slot supports up to 64GB microSDXC NAND flash memory</p>
</td>
</tr><tr valign="TOP"><td width="25%">
<p><strong>SDIO</strong></p>
</td>
<td width="25%">
<p>1</p>
<p> </p>
</td>
<td width="25%">
<p>4</p>
</td>
<td width="25%">
<p>No—microSD (for OS flash card; comes with 2 GB card)</p>
</td>
</tr><tr valign="TOP"><td width="25%">
<p><strong>USB ports</strong></p>
</td>
<td width="25%">
<p>1 (2 on Model B)</p>
</td>
<td width="25%">
<p>1 USB port and 1 USB-OTG</p>
</td>
<td width="25%">
<p>2 and 1 microUSB</p>
</td>
</tr><tr valign="TOP"><td width="25%">
<p><strong>SATA</strong></p>
</td>
<td width="25%">
<p>No</p>
</td>
<td width="25%">
<p>1 SATA-II</p>
</td>
<td width="25%">
<p>3 SATA-II</p>
</td>
</tr><tr valign="TOP"><td width="25%">
<p><strong>10/100 Ethernet</strong></p>
</td>
<td width="25%">
<p>Model B only</p>
</td>
<td width="25%">
<p>Yes</p>
</td>
<td width="25%">
<p>Yes</p>
</td>
</tr><tr valign="TOP"><td width="25%">
<p><strong>Other</strong></p>
</td>
<td width="25%">
<p>2×13 header pins for GPIO, SPI, I²C, UART, +3.3 V, +5 V</p>
</td>
<td width="25%">
<p>GPIO, I2C, PWM, Keyboard Matrix (8x8), built-in resistive touchscreen controller</p>
</td>
<td width="25%">
<p>Infrared receiver, microUSB for debug console, “unbrickable”</p>
</td>
</tr></tbody></table><p><em>* From Rhombus Tech's Luke Leighton, March 7:  "The price is NOT $15. the *COST* - the Bill of Materials - is*APPROACHING* $15 when you order 100,000 units. *NO* decision has beenmade on the *SALE* price because we are still evaluating the cost of the parts, and are still seeking reliable long-term suppliers of the connectors." He also adds that when the CPU cards are available, they would like to invite people to pay the profit that they would </em>like<em> to pay, with the profits going towards goal-orientated, free software-related bounties, bonus payments, future products, and sponsorship developer kits for free software developers." </em></p>
<p><strong>How do they compare on openness?</strong></p>
<ul><li>
<p>Raspberry Pi “[hasn’t] made a decision on open hardware yet. We have SPI, I2C, UART and a fair bit of GPIO at 3.3V if you want to do interfacing.”</p>
</li>
<li>
<p>Allwinner is GPL-compliant <a href="http://rhombus-tech.net/allwinner_a10/pcb/">open hardware</a>. <a href="http://rhombus-tech.net/faq/#index11h2">Read the details.</a></p>
</li>
<li>
<p>CuBox is intended for open source software but does not appear to be or have a statement on open hardware.</p>
</li>
</ul><p>While the Raspberry Pi has gotten a lot of the attention (and does have the best name), the Allwinner really is all winner on <span style="text-decoration: line-through;">price</span>, openness, and many of its features. The CuBox is the winner primarily on polish and the fact that you can order one right now, although the others are soon to catch up.</p>
<p> </p>
