<p>Pandora Flexible Monitoring Solution (FMS) is all-purpose monitoring software, which means it can control network equipment, servers (Linux and Windows), virtual environments, applications, databases, and a lot more. It can do both remote monitoring and monitoring based on agents installed on the servers. You can get collected data in reports and graphs and raise alerts if something goes wrong.</p>
<p>Pandora FMS is offered in two versions: the <a href="https://pandorafms.org/" target="_blank">open source community edition</a> is aimed at private users and organizations of any size and is fully functional and totally free, while the <a href="https://pandorafms.com/" target="_blank">enterprise version</a> is designed to facilitate the work of companies, as it has support services and special features for large environments. Both versions are updated every month and accessible directly from the console.</p>
<h2 id="installing-pandora-fms">Installing Pandora FMS</h2>
<h3 id="getting-started">Getting started</h3>
<p>Linux is the Pandora FMS's preferred operating system, but it also works perfectly under Windows. CentOS 7 is the recommended distribution, and there are installation packages in Debian/Ubuntu and SUSE Linux. If you feel brave, you can install it from source on other distros or FreeBSD or Solaris, but professional support is available only in Linux.</p>
<p>For a small test, you will need a server with at least 4GB of RAM and about 20GB of free disk space. With this environment, you can monitor 200 to 300 servers easily. Pandora FMS has different ways to scale, and it can monitor several thousand servers in a single instance. By combining several instances, clients with even 100,000 devices can be monitored.</p>
<h3 id="iso-installation-easy-peasy">ISO installation</h3>
<p>The easiest way to install Pandora FMS is to use the ISO image, which contains a CentOS 7 version with all the dependencies. The following steps will get Pandora FMS ready to use in just five minutes.</p>
<ol><li><a href="http://pandorafms.org/features/free-download-monitoring-software/" target="_blank">Download</a> the ISO from Pandora FMS's website.</li>
<li>Burn it onto a DVD or USB stick, or boot it from your virtual infrastructure manager (e.g., VMware, Xen, VirtualBox).</li>
<li>Boot the image and proceed to the guided setup (a standard CentOS setup process). Set a unique password for the root user.</li>
<li>Identify the IP address of your new system.</li>
<li>Access the Pandora FMS console, using the IP address of the system where you installed Pandora FMS. Open a web browser and enter <strong>http://&lt;pandora_ip_address&gt;/pandora_console</strong> and log in as <strong>admin</strong> using the default password <strong>pandora</strong>.</li>
</ol><p>Congratulations, you're in! You can skip the other installation methods and <a href="#Monitoring">jump ahead</a> to start monitoring something real.</p>
<h3 id="docker-installation">Docker installation</h3>
<ol><li>First, launch Pandora FMS with this command:<br /><pre><code class="language-bash">curl -sSL http://pandorafms.org/getpandora | sh</code></pre><p>	You can also run Pandora FMS as a container by executing:</p>
<pre><code class="language-bash">docker run --rm -ti -p 80:80 -p 443:443 \
  --name pandorafms pandorafms/pandorafms:latest</code></pre></li>
<li>Once Pandora FMS is running, open your browser and enter <br /><br /><strong>http://&lt;ip address&gt;/pandora_console</strong>. Log in as <strong>admin</strong> with the default password <strong>pandora</strong>.</li>
</ol><p>The Docker container is at <a href="https://hub.docker.com/r/pandorafms/pandorafms" target="_blank">hub.docker.com/r/pandorafms/pandorafms</a>.</p>
<h3 id="yum-installation-a-fast-way-for-centos">Yum installation</h3>
<p>You can install Pandora FMS for Red Hat Enterprise Linux or CentOS 7 in just five steps.</p>
<ol><li>Activate CentOS Updates, CentOS Extras, and EPEL in <a href="https://pandorafms.com/docs/index.php?title=Pandora:Documentation_en:Installing#Installation_in_Red_Hat_Enterprise_Linux_.2F_Fedora_.2F_CentOS" target="_blank">your repository's library</a>.</li>
<li>Add the official Pandora FMS repo to your system:<br /><pre><code class="language-bash">[artica_pandorafms]

name=CentOS7 - PandoraFMS official repo
baseurl=http://firefly.artica.es/centos7
gpgcheck=0
enabled=1
</code></pre></li>
<li>Install the packages from the repo and solve all dependencies:<br /><pre><code class="language-bash">yum install pandorafms_console pandorafms_server mariadb-server</code></pre></li>
<li>Reload services if you need to install Apache or MySQL/MariaDB:<br /><pre><code class="language-bash">service httpd reload (or equivalent)

service mysqld reload (or equivalent)</code></pre></li>
<li>Open your browser and enter <strong>http://&lt;ip address&gt;/pandora_console</strong>. Proceed with the setup process. After accepting the license and doing a few pre-checks, you should see something like this:</li>
</ol><p><article class="align-center media media--type-image media--view-mode-full" title="Pandora FMS environment and database setup"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/installing-pandora-fms-1.png" width="500" height="587" alt="Pandora FMS environment and database setup" title="Pandora FMS environment and database setup" /></div>
      
  </article></p>
<p>This screen is only needed when you install using the RPM, DEB, or source code (Git, tarball, etc.). This step of the console configuration uses MySQL credentials (which you need to know) to create a database and a username and password for Pandora FMS console and server. You need to set up the server password manually (yep! Vim or Nano?) by editing the <strong>/etc/pandora/pandora_server.conf</strong> file (follow the <a href="https://pandorafms.com/docs/index.php?title=Pandora:Documentation_en:Installing#Server_Initialization_and_Basic_Configuration" target="_blank">instructions in the documentation</a>).</p>
<p>Restart the Pandora FMS server, and everything should be ready.</p>
<h3 id="other-ways-to-install-pandora-fms">Other ways to install Pandora FMS</h3>
<p>If none of these installation methods work with your setup, other options include a Git checkout, tarball with sources, DEB package (with the .deb online repo), and SUSE RPM. You can learn more about these installation methods in the <a href="https://pandorafms.com/docs/index.php?title=Pandora:Documentation_en:Installing" target="_blank">installing wiki</a>.</p>
<p>Grabbing the code is pretty easy with Git:</p>
<pre><code class="language-bash">git clone https://github.com/pandorafms/pandorafms.git</code></pre><h2 id="taking-the-first-steps-with-pandora-fms"><a id="Monitoring" name="Monitoring"></a>Monitoring with Pandora FMS</h2>
<p>When you log into the console, you will see a welcome screen.</p>
<p><article class="media media--type-image media--view-mode-full" title="Pandora FMS welcome screen"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/installing-pandora-fms-2.png" width="650" height="276" alt="Pandora FMS welcome screen" title="Pandora FMS welcome screen" /></div>
      
  </article></p>
<h3 id="monitoring-something-connected-to-the-network">Monitoring something connected to the network</h3>
<p>Let's begin with the most simple thing to do: ping to a host. First, create an agent by selecting <strong>Resources</strong> then <strong>Manage Agents</strong> from the menu.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Locating the Manage Agents menu"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/installing-pandora-fms-3.png" width="650" height="332" alt="Locating the Manage Agents menu" title="Locating the Manage Agents menu" /></div>
      
  </article></p>
<p>Click on <strong>Create</strong> at the bottom of the page, and fill the basic information (don't go crazy, just add your IP address and name).</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Enter basic data in the Agent Manager"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/installing-pandora-fms-4.png" width="650" height="225" alt="Enter basic data in the Agent Manager" title="Enter basic data in the Agent Manager" /></div>
      
  </article></p>
<p>Go to the <strong>Modules</strong> tab and create a network module.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Create a network module"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/installing-pandora-fms-5.png" width="650" height="92" alt="Create a network module" title="Create a network module" /></div>
      
  </article></p>
<p>Use the Module component (which comes from an internal library pre-defined in Pandora FMS) to choose the ping by selecting <strong>Network Management</strong> and entering <strong>Host Alive</strong>.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Choosing Host Alive ping"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/installing-pandora-fms-6.png" width="650" height="124" alt="Choosing Host Alive ping" title="Choosing Host Alive ping" /></div>
      
  </article></p>
<p>Click on <strong>Save</strong> and go back to the "view" interface by clicking the "eye" icon on the right.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Menu bar with &quot;eye&quot; icon"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/installing-pandora-fms-7.png" width="650" height="28" alt="Menu bar with &quot;eye&quot; icon" title="Menu bar with &quot;eye&quot; icon" /></div>
      
  </article></p>
<p>Congratulations! Your ping is running (you know it because it's green).</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Console showing ping is running"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/installing-pandora-fms-8.png" width="650" height="307" alt="Console showing ping is running" title="Console showing ping is running" /></div>
      
  </article></p>
<p>This is the manual way; you can also use the wizard to grab an entire Simple Network Management Protocol (SNMP) device to show interfaces, or you can use a bulk operation to copy a configuration from one device to another, or you can use the command-line interface (CLI) API to do configurations automatically. Review the <a href="https://pandorafms.com/docs/index.php?title=Main_Page" target="_blank">online wiki</a>, with over 1200 articles of documentation, to learn more.</p>
<p>The following shows an old Sonicwall NSA 250M Firewall monitored with the SNMP wizard. It shows data on status interfaces, active connections, CPU usage, active VPN, and a lot more.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Console showing firewall monitoring"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/installing-pandora-fms-9.png" width="650" height="337" alt="Console showing firewall monitoring" title="Console showing firewall monitoring" /></div>
      
  </article></p>
<p>Remote monitoring supports SNMP v.1, 2, and 3; Windows Management Instrumentation (WMI); remote SSH calls; SNMP trap capturing; and NetFlow monitoring.</p>
<h3 id="monitoring-a-server-with-an-agent">Monitoring a server with an agent</h3>
<p>Installing a Linux agent in Red Hat/CentOS is simple. Enter:</p>
<pre><code class="language-bash">yum install pandorafms_agent_unix</code></pre><p>Edit <strong>/etc/pandora/pandora_agent.conf</strong> and set up the IP address of your Pandora FMS server:</p>
<pre><code class="language-text">server_ip   &lt;x.x.x.x&gt;</code></pre><p>Restart the agent and wait a few seconds for the console to show the data.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Console monitoring a Linux agent"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/installing-pandora-fms-10.png" width="650" height="343" alt="Console monitoring a Linux agent" title="Console monitoring a Linux agent" /></div>
      
  </article></p>
<p>In the main agent view, you can see events, data, and history; define the threshold for status change; and set up alerts to warn you when something is wrong. Months worth of data is available for graphs, reports, and service-level agreement (SLA) compliance.</p>
<p>Installing a Windows agent is even easier because the installer supports automation for unattended setups. Start by downloading the agent and doing some usual routines. At some point, it will ask you for your server IP and the name for the agent, but that's all.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Pandora FMS Windows setup screen"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/installing-pandora-fms-11.png" width="450" height="351" alt="Pandora FMS Windows setup screen" title="Pandora FMS Windows setup screen" /></div>
      
  </article></p>
<p>Windows agents support grabbing service status and processes, executing local commands to get information, getting Windows events, native WMI calls, obtaining performance counters directly from the system, and providing a lot more information than the basic CPU/RAM/disk stuff. It uses the same configuration file as the Linux version (pandora_agent.conf), which you can edit with a text editor like Notepad. Editing is very easy; you should be able to add your own checks in less than a minute.</p>
<h2 id="graphs-reports-and-slas">Creating graphs, reports, and SLA checks</h2>
<p>Pandora FMS has lots of options for graphs and reports, including on SLA compliance, in both the open source and enterprise versions.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Pandora FMS SLA compliance report"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/installing-pandora-fms-12.png" width="650" height="184" alt="Pandora FMS SLA compliance report" title="Pandora FMS SLA compliance report" /></div>
      
  </article></p>
<p>Pandora FMS's Visual Map feature allows you to create a map of information that combines status, data, graphs, icons, and more. You can edit it using an online editor. Pandora FMS is 100% operable from the console; no desktop application or Java is needed, nor do you need to execute commands from the console.</p>
<p>Here are three examples.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Pandora FMS support ticket graph"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/pandora-fms-visual-console-1.jpg" width="650" height="366" alt="Pandora FMS support ticket graph" title="Pandora FMS support ticket graph" /></div>
      
  </article></p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Pandora FMS network status graph"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/pandora-fms-visual-console-2.jpg" width="650" height="366" alt="Pandora FMS network status graph" title="Pandora FMS network status graph" /></div>
      
  </article></p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Pandora FMS server status graph"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/pandora-fms-visual-console-3.png" width="650" height="365" alt="Pandora FMS server status graph" title="Pandora FMS server status graph" /></div>
      
  </article></p>
<p>If you would like to learn more about Pandora FMS, visit the <a href="https://pandorafms.org/" target="_blank">website</a> or ask questions in the <a href="https://pandorafms.org/forum/" target="_blank">forum</a>.</p>
