<p>Hello, open gaming fans! In this week's edition, we take a look at the latest Steam controller update, a <em>Company of Heroes</em> Linux update, and more.</p>
<h2>Open gaming roundup for December 12-18, 2015</h2>
<p><!--break--></p>
<h3>Steam controller update</h3>
<p>Not being able to carry your configurations across was an issue for several Steam gaming enthusiasts, but not any more. The latest update of Steam controller lets you take your custom configurations with you. You can also customize the on/off sounds and brightness of the Steam button. It also lets you share non-Steam-game configs. Read more <a href="https://www.gamingonlinux.com/articles/steam-controller-updated-you-can-name-it-take-configs-with-you-adjust-the-light-bleep-and-more.6382" target="_blank">here</a>.</p>
<h3><em>Company of Heroes</em> updated for Linux</h3>
<p>A long overdue Linux update came to <em>Company of Heroes </em>this week. The update is welcome news for the community. You can find the game <a href="http://store.steampowered.com/agecheck/app/231430/" target="_blank">here</a>.</p>
<h3>Pick of the week: <em>FlightGear</em></h3>
<p><em>FlightGear</em> is an open source flight simulator. It runs on all the major platforms, i.e. Linux, Mac, and Windows. The primary goal of the <em>FlightGear</em> project is to create a sophisticated and open flight simulator framework for use in research or academic environments, pilot training, and engineering, but gaming enthusiasts can certainly take it for a spin for fun. You can download <em>FlightGear</em> <a href="http://www.flightgear.org/download/" target="_blank">here</a>.</p>
<h2>New games out for Linux</h2>
<h3><em>War Thunder Royal Armour</em> update</h3>
<p><em>War Thunder</em> introduces British tanks and a host of new vehicles in its latest <em>Royal Armour</em> update. Here's what <a href="http://linuxgamenews.com/post/135339643347/battlefleet-gothic-armada-looks-epic#.VnQZWfkrJBx" target="_blank">Linux game news</a> says:</p>
<blockquote><p>Thirty British tanks including variants of Valentine’s, Matildas, Cromwells and Churchills will be introduced over the coming weeks with five regular and four premium tanks starting for the Brits. Over the next few weeks the British ranks will steadily grow and encompass self-propelled guns and the very first armoured cars in War Thunder in the form of the T172E2 and Mk.II AA.</p>
</blockquote>
<p><iframe allowfullscreen="" frameborder="0" height="293" src="https://www.youtube.com/embed/p5Fm7-7qtPI" width="520"></iframe></p>
<h3><em>Street Fighter V</em> is coming to Linux and SteamOS in 2016</h3>
<p><em>Street Fighter V</em> is a fighting action game powered by Unreal Engine 4. Here's what <a href="http://www.capcom-unity.com/street_fighter/blog/2015/12/17/announcing-steam-os-support-for-sfv" target="_blank">Capcom blog</a> announced a while back:</p>
<blockquote><p>I’m excited to announce that Street Fighter V will be officially supporting Steam OS! We are working closely with Valve on this endeavor, and will have more details to announce in the near future, so stay tuned for updates. Street Fighter V will also support the Steam controller, and you’ll be able to try that out right away during the upcoming beta test that takes place from December 18~20.</p>
</blockquote>
<p><iframe allowfullscreen="" frameborder="0" height="293" src="https://www.youtube.com/embed/rCuMHpeDNmY" width="520"></iframe></p>
