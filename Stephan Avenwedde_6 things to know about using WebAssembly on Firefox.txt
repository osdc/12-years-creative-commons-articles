<p>WebAssembly is a portable execution format that has drawn a lot of interest due to its ability to execute applications in the browser at near-native speed. By its nature, WebAssembly has some special properties and limitations. However, by combining it with other technologies, completely new possibilities arise, especially related to gaming in the browser.</p>
<p>This article describes the concepts, possibilities, and limitations of running WebAssembly on Firefox.</p>
<h2 id="the-sandbox">The sandbox</h2>
<p>WebAssembly has a <a href="https://webassembly.org/docs/security/" target="_blank">strict security policy</a>. A program or functional unit in WebAssembly is called a <em>module</em>. Each module instance runs its own isolated memory space. Therefore, one module cannot access another module's virtual address space, even if they are loaded on the same web page. By design, WebAssembly also considers memory safety and control-flow integrity, which enables an (almost-) deterministic execution.</p>
<h2 id="web-apis">Web APIs</h2>
<p>Access to many kinds of input and output devices is granted via JavaScript <a href="https://developer.mozilla.org/en-US/docs/Web/API" target="_blank">Web APIs</a>. In the future, access to Web APIs will be available without the detour over to JavaScript, according to this <a href="https://github.com/WebAssembly/gc/blob/master/README.md" target="_blank">proposal</a>. C++ programmers can find information about accessing the Web APIs on <a href="https://emscripten.org/docs/porting/connecting_cpp_and_javascript/Interacting-with-code.html" target="_blank">Emscripten.org</a>. Rust programmers can use the <a href="https://github.com/rustwasm/wasm-bindgen" target="_blank">wasm-bindgen</a> library that is documented on <a href="https://rustwasm.github.io/wasm-bindgen/" target="_blank">rustwasm.github.io</a>.</p>
<h2 id="file-inputoutput">File input/output</h2>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Programming and development</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ&amp;src=programming_resource_menu4">Red Hat Developers Blog</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Programming cheat sheets</a></li>
<li><a href="https://www.redhat.com/en/services/training/learning-subscription?intcmp=7016000000127cYAAQ">Try for free: Red Hat Learning Subscription</a></li>
<li><a href="https://opensource.com/downloads/bash-programming-guide?intcmp=7016000000127cYAAQ">eBook: An introduction to programming with Bash</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/bash-shell-cheat-sheet?intcmp=7016000000127cYAAQ">Bash Shell Scripting Cheat Sheet</a></li>
<li><a href="https://developers.redhat.com/e-books/modernizing-enterprise-java?intcmp=7016000000127cYAAQ">eBook: Modernizing Enterprise Java</a></li>
</ul></div>
</div>
</div>
</div>
<p>Because WebAssembly is executed in a sandboxed environment, it cannot access the host's filesystem when it is executed in a browser. However, Emscripten offers a solution in the form of a virtual filesystem.</p>
<p>Emscripten makes it possible to preload files to the memory filesystem at compile time. Those files can then be read from within the WebAssembly application, just as you would on an ordinary filesystem. This <a href="https://emscripten.org/docs/api_reference/Filesystem-API.html" target="_blank">tutorial</a> offers more information.</p>
<h2 id="persistent-data">Persistent data</h2>
<p>If you need to store persistent data on the client-side, it must be done over a JavaScript Web API. Refer to Mozilla Developer Network's documentation on <a href="https://developer.mozilla.org/en-US/docs/Web/API/IndexedDB_API/Browser_storage_limits_and_eviction_criteria" target="_blank">browser storage limits and eviction criteria</a> for more detailed information about the different approaches.</p>
<h2 id="memory-management">Memory management</h2>
<p>WebAssembly modules operate on linear memory as a <a href="https://en.wikipedia.org/wiki/Stack_machine" target="_blank">stack machine</a>. This means that concepts like heap memory allocations are not available. However, if you are using <code>new</code> in C++ or <code>Box::new</code> in Rust, you would expect it to result in a heap memory allocation. The way heap memory allocation requests are translated into WebAssembly relies heavily upon the toolchain. You can find a detailed analysis of how different toolchains deal with heap memory allocations in Frank Rehberger's post about <a href="https://frehberg.wordpress.com/webassembly-and-dynamic-memory/" target="_blank"><em>WebAssembly and dynamic memory</em></a>.</p>
<h2 id="games">Games!</h2>
<p>In combination with <a href="https://en.wikipedia.org/wiki/WebGL" target="_blank">WebGL</a>, WebAssembly enables native gaming in the browser due to its high execution speed. The big proprietary game engines <a href="https://beta.unity3d.com/jonas/AngryBots/" target="_blank">Unity</a> and <a href="https://www.youtube.com/watch?v=TwuIRcpeUWE" target="_blank">Unreal Engine 4</a> show what is possible with WebGL. There are also open source game engines that use WebAssembly and the WebGL interface. Here are some examples:</p>
<ul><li>Since November 2011, the <a href="https://en.wikipedia.org/wiki/Id_Tech_4" target="_blank">id Tech 4</a> engine (better known as the Doom 3 engine) is available under the GPL license on <a href="https://github.com/id-Software/DOOM-3" target="_blank">GitHub</a>. There is also a <a href="https://wasm.continuation-labs.com/d3demo/" target="_blank">WebAssembly port of Doom 3</a>.</li>
<li>The Urho3D engine provides some <a href="https://urho3d.github.io/samples/" target="_blank">impressive examples</a> that can run in the browser.</li>
<li>If you like retro games, try this <a href="https://vaporboy.net/" target="_blank">Game Boy emulator</a>.</li>
<li>The <a href="https://docs.godotengine.org/en/stable/development/compiling/compiling_for_web.html" target="_blank">Godot engine is also capable of producing WebAssembly</a>. I couldn't find a demo, but the <a href="https://godotengine.org/editor/latest/godot.tools.html" target="_blank">Godot editor</a> has been ported to WebAssembly.</li>
</ul><h2 id="more-about-webassembly">More about WebAssembly</h2>
<p>WebAssembly is a promising technology that I believe we will see more frequently in the future. In addition to executing in the browser, WebAssembly can also be used as a portable execution format. The <a href="https://github.com/wasmerio/wasmer" target="_blank">Wasmer</a> container host enables you to execute WebAssembly code on various platforms.</p>
<p>If you want more demos, examples, and tutorials, take a look at this <a href="https://github.com/mbasso/awesome-wasm" target="_blank">extensive collection of WebAssembly topics</a>. Not exclusive to WebAssembly but still worth a look are Mozilla's <a href="https://developer.mozilla.org/en-US/docs/Games/Examples" target="_blank">collection of games and demos</a>.</p>
