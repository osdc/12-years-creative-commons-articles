<p>Jen is out this week, but she'll be back June 3 with her usual top 5 video. In the meantime, here's a look at some of this week's best articles.</p>
<h2>Top 5 articles of the week</h2>
<h3><a href="https://opensource.com/life/16/5/sparkfun-inventors-kit-giveaway" target="_blank">We're giving away an Arduino starter kit!</a></h3>
<p>We kicked off our open hardware series in style by announcing a SparkFun Inventor's Kit giveaway. Enter now for a chance to win!</p>
<h3><a href="https://opensource.com/business/16/5/containers-unikernels-learn-arduino-raspberry-pi" target="_blank">What containers and unikernels can learn from Arduino and Raspberry Pi</a></h3>
<p>Containers and unikernels share many similarities with the general purpose open hardware boards.</p>
<h3><a href="https://opensource.com/business/16/5/open-source-cad-programs" target="_blank">3 open source alternatives to AutoCAD</a></h3>
<p>Are open source alternatives to AutoCAD a good fit for your needs? Read about several options and decide for yourself.</p>
<h3><a href="https://opensource.com/life/16/5/19-years-later-cathedral-and-bazaar-still-moves-us" target="_blank"><em>The Cathedral and the Bazaar</em> turns 19</a></h3>
<p>Did Eric Raymond's <a href="https://en.wikipedia.org/wiki/The_Cathedral_and_the_Bazaar" target="_blank"><em>The Cathedral and the Bazaar</em></a> accurately predict the future of software development? That's the wrong question, writes Bryan Behrenshausen.</p>
<h3><a href="https://opensource.com/business/16/5/interview-alison-chaiken-steven-crumb" target="_blank">Driving cars into the future with Linux</a></h3>
<p>Don Watkins and Deb Nicholson take a look at how Linux and open source software are changing in-vehicle infotainment through GENIVI and the Automotive Grade Linux (AGL) workgroup.</p>
