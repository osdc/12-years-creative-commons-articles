<p>In the <a href="https://opensource.com/article/21/2/what-does-it-mean-be-technical">first article in this series</a>, I explained how the tech industry divides people and roles into "technical" or "non-technical" categories and the problems associated with this. The tech industry makes it difficult for people interested in tech—but not coding—to figure out where they fit in and what they can do.</p>
<p>If you're interested in technology or open source but aren't interested in coding, there are roles available for you. Any of these positions at a tech company likely require somebody who is tech-savvy but does not necessarily write code. You do, however, need to know the terminology and understand the product.</p>
<p>I've recently noticed the addition of the word "technical" onto job titles such as technical account manager, technical product manager, technical community manager, etc. This mirrors the trend a few years ago where the word "engineer" was tacked onto titles to indicate the role's technical needs. After a while, everybody has the word "engineer" in their title, and the classification loses some of its allure.</p>
<p>As I sat down to write these articles, this tweet from Tim Banks appeared in my timeline:</p>
<blockquote class="twitter-tweet"><p dir="ltr" lang="en" xml:lang="en">Women who've made career changes into tech, but aren't developers (think like infosec, data science/analysts, infra engineers, etc), what are some things you'd wished you'd known, resources that were valuable, or advice you'd have for someone looking to make a similar change?</p>
<p>— Tim Banks is a buttery biscuit (@elchefe) <a href="https://twitter.com/elchefe/status/1338933320147750915?ref_src=twsrc%5Etfw">December 15, 2020</a></p></blockquote>
<script async="" src="https://platform.twitter.com/widgets.js" charset="utf-8"></script><p>This follows the advice in my first article: Tim does not simply ask about "non-technical roles"; he provides more significant context. On a medium like Twitter, where every character counts, those extra characters make a difference. These are <em>technical</em> roles. Calling them non-technical to save characters in a tweet would have changed the impact and meaning.</p>
<p>Here's a sampling of non-engineering roles in tech that require technical knowledge.</p>
<h2 id="technical-writer">Technical writer</h2>
<p>A <a href="https://opensource.com/article/17/5/technical-writing-job-interview-tips">technical writer's job</a> is to transfer factual information between two or more parties. Traditionally, a technical writer provides instructions or documentation on how to use a technology product. Recently, I've seen the term "technical writer" refer to people who write other forms of content. Tech companies want a person to write blog posts for their developer audience, and this skill is different from copywriting or content marketing.</p>
<p><strong>Technical skills required:</strong></p>
<ul><li>Writing</li>
<li>User knowledge or experience with a specific technology</li>
<li>The ability to quickly come up to speed on a new product or feature</li>
<li>Skill in various authoring environments</li>
</ul><p><strong>Good for people who:</strong></p>
<ul><li>Can plainly provide step-by-step instructions</li>
<li>Enjoy collaborating</li>
<li>Have a passion for the active voice and Oxford comma</li>
<li>Enjoy describing the what and how</li>
</ul><h2 id="product-manager">Product manager</h2>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More open source career advice</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7013a000002DRawAAG">Open source cheat sheets</a></li>
<li><a href="https://developers.redhat.com/learn/linux-starter-kit-for-developers/?intcmp=7013a000002DRawAAG">Linux starter kit for developers</a></li>
<li><a href="https://www.redhat.com/sysadmin/questions-for-employer?intcmp=7013a000002DRawAAG">7 questions sysadmins should ask a potential employer before taking a job</a></li>
<li><a href="https://www.redhat.com/architect/?intcmp=7013a000002DRawAAG">Resources for IT artchitects</a></li>
<li><a href="https://enterprisersproject.com/article/2019/3/article-cheat-sheet?intcmp=7013a000002DRawAAG">Cheat sheet: IT job interviews</a></li>
</ul></div>
</div>
</div>
</div>
<p>A <a href="https://opensource.com/article/20/2/product-management-open-source-company">product manager</a> is responsible for leading a product's strategy. Responsibilities may include gathering and prioritizing customers' requirements, writing business cases, and training the sales force. Product managers work cross-functionally to successfully launch a product using a combination of creative and technical skills. Product managers require deep product expertise.</p>
<p><strong>Technical skills required:</strong></p>
<ul><li>Hands-on product knowledge and the ability to configure or run a demo</li>
<li>Knowledge of the technological ecosystem related to the product</li>
<li>Analytical and research skills</li>
</ul><p><strong>Good for people who:</strong></p>
<ul><li>Enjoy strategizing and planning what comes next</li>
<li>Can see a common thread in different people's needs</li>
<li>Can articulate the business needs and requirements</li>
<li>Enjoy describing the why</li>
</ul><h2 id="data-analyst">Data analyst</h2>
<p>Data analysts are responsible for collecting and interpreting data to help drive business decisions such as whether to enter a new market, what customers to target, or where to invest. The role requires knowing how to use all of the potential data available to make decisions. We tend to oversimplify things, and data analysis is often over-simplified. Getting the right information isn't as simple as writing a query to "select all limit 10" to get the top 10 rows. You need to know what tables to join. You need to know how to sort. You need to know whether the data needs to be cleaned up in some way before or after running the query.</p>
<p><strong>Technical skills required:</strong></p>
<ul><li>Knowledge of SQL, Python, and R</li>
<li>Ability to see and extract patterns in data</li>
<li>Understanding how things function end to end</li>
<li>Critical thinking</li>
<li>Machine learning</li>
</ul><p><strong>Good for people who:</strong></p>
<ul><li>Enjoy problem-solving</li>
<li>Desire to learn and ask questions</li>
</ul><h2 id="developer-relations">Developer relations</h2>
<p><a href="https://www.marythengvall.com/blog/2019/5/22/what-is-developer-relations-and-why-should-you-care" target="_blank">Developer relations</a> is a relatively new discipline in technology. It encompasses the roles of <a href="https://opensource.com/article/20/10/open-source-developer-advocates">developer advocate</a>, developer evangelist, and developer marketing, among others. These roles require you to communicate with developers, build relationships with them, and help them be more productive. You advocate for the developers' needs to the company and represent the company to the developer. Developer relations can include writing articles, creating tutorials, recording podcasts, speaking at conferences, and creating integrations and demos. Some say you need to have worked as a developer to move into developer relations. I did not take that path, and I know many others who haven't.</p>
<p><strong>Technical skills required:</strong></p>
<p>These will be highly dependent on the company and the role. You will need some (not all) depending on your focus.</p>
<ul><li>Understanding technical concepts related to the product</li>
<li>Writing</li>
<li>Video and audio editing for tutorials and podcasts</li>
<li>Speaking</li>
</ul><p><strong>Good for people who:</strong></p>
<ul><li>Have empathy and want to teach and empower others</li>
<li>Can advocate for others</li>
<li>Are creative</li>
</ul><h2 id="endless-possibilities">Endless possibilities</h2>
<p>This is not a comprehensive list of all the non-engineering roles available in tech, but a sampling of some of the roles available for people who don't enjoy writing code daily. If you're interested in a tech career, look at your skills and what roles would be a good fit. The possibilities are endless. To help you in your journey, in the final article in this series, I'll share some advice from people who are in these roles.</p>
