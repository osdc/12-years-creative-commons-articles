<p>I've heard the term "impostor syndrome" tossed around a lot lately, and while I understand what it means in broad strokes, it wasn't until Christina Keelan's keynote at the Community Leadership Summit (CLS) this year that I began to gain a deeper understanding.</p>
<blockquote><p>"Impostor syndrome can be defined as a collection of feelings of inadequacy that persist even in face of information that indicates that the opposite is true. It is experienced internally as chronic self-doubt, and feelings of intellectual fraudulence."—from the <a href="https://counseling.caltech.edu/general/InfoandResources/Impostor" target="_blank">Caltech Counseling Center</a></p>
</blockquote>
<p>The term, although heard more often today, was coined in 1978. Early on, it was thought to mostly affect women, but it has since been proven that it is found equally in both men and women. So, if we find that we're struggling with impostor syndrome, what can we do?</p>
<ul><li>Recognize it! If it's not addressed victims can develop anxiety, stress, and depression.</li>
<li>Embrace positive feedback and don't downplay your accomplishments.</li>
<li>Seek help or find a mentor (other people feel this way too).</li>
<li>Accept that we can't do it all. Do what you do and do it well. Don't try to be something you're not. (If you're a community manager be a community manager, you don't have to be a developer too.)</li>
</ul><p>Impostor syndrome is also a problem when we strive to prove our worth to our managers. Management wants to see metrics or deliverables—and metrics can be scary. Before panicking about keeping track of your accomplishments:</p>
<ul><li>Find out what your leadership team expects from you.</li>
<li>Determine what metrics are necessary for your team.</li>
<li>Start to track early on so you don't get overwhelmed as your community grows.</li>
<li>Maintain open and honest communication with your leaders.</li>
</ul><p>Remember that <a href="https://opensource.com/business/15/12/avoid-burnout-live-happy" target="_blank">burnout happens</a>. (We had an extension of this discussion in a session led by Jono Bacon). He talked to us about the 12-Stage Burnout Cycle by Herbert Freudenberger and Gail North. In this cycle the stages of burnout are:</p>
<ol><li>You want to prove yourself (connected very closely to imposter syndrome).</li>
<li>Working harder and longer hours, may find it harder to switch off.</li>
<li>Start ignoring your needs (erratic sleeping patterns, reducing social interaction).</li>
<li>Panic and conflict: you look to other people as the cause.</li>
<li>Values often change, hobbies start to seem irrelevant.</li>
<li>Become cynical and intolerant, become a bit more aggressive.</li>
<li>Withdrawal: you have almost a non existent social life.</li>
<li>Behavioral changes become evident to other people.</li>
<li>Depersonalization: you feel worthless and don't think you can have an impact.</li>
<li>Inner emptiness: lots of overeating, drug use, and alcoholism.</li>
<li>Depression: you feel completely lost.</li>
<li>You're looking for a way out.</li>
</ol><p>Christina shared these tips:</p>
<ul><li>Don't wait until you've hit rock bottom (both physically and mentally).</li>
<li>Make sure you balance personal life and work schedule. Find time to disconnect and remember it's okay to say "no."</li>
<li>Become more self aware by looking into the principles of <a href="https://en.wikipedia.org/wiki/Stoicism" target="_blank">Stoicism</a>.</li>
<li>Practice mindfulness (and maybe, yoga).</li>
</ul><p>In a talk by Josh Simmons at CLS 2016, he said not be ashamed to seek out help. Therapists and counselors can be a great help in teaching you how to become more self-aware and how to take more care of yourself. We are all wonderful people and need to be aware of that and take care of ourselves.</p>
