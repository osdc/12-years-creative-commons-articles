<p>OpenSSH is widely used, but there isn't a well-known connection manager, so I developed the ncurses SSH connection manager (<code>nccm</code>) to fill that significant gap in the process. <code>nccm</code> is a simple SSH connection manager with an ultra-portable terminal interface (written in ncurses, as the project name suggests). And best of all, it's straightforward to use. With <code>nccm</code>, you can connect to an SSH session of your choice with minimum distraction and minimal keystrokes.</p>
<h2>Install nccm</h2>
<p>The quickest way to get going is to clone the project from its <a href="https://github.com/flyingrhinonz/nccm" target="_blank">Git repository</a>:</p>
<pre><code class="language-bash">$ git clone https://github.com/flyingrhinonz/nccm nccm.git</code></pre><p>In the <code>nccm.git/nccm</code> directory, there are two files—<code>nccm</code> itself and an <code>nccm.yml</code> configuration file.</p>
<p>First, copy the nccm script to <code>/usr/local/bin/</code> and grant it executable permissions. You can do this in one step with the <code>install</code> command:</p>
<pre><code class="language-bash">$ sudo install -m755 nccm
–target-directory /usr/local/bin</code></pre><p>The <code>nccm.yml</code> file can be copied to any one of these locations, and is loaded from the first location found:</p>
<ul><li><code>~/.config/nccm/nccm.yml</code></li>
<li><code>~/.nccm.yml</code></li>
<li><code>~/nccm.yml</code></li>
<li><code>/etc/nccm.yml</code></li>
</ul><p>The <code>nccm</code> command requires Python 3 to be installed on your machine, which shouldn't be a problem on most Linux boxes. Most Python library dependencies are already present as part of Python 3; however, there are some YAML dependencies and utilities you must install.</p>
<p>If you don't have <code>pip</code> installed, you can install it with your package manager. And while you're at it, install the <code>yamllint</code> application to help you validate the <code>nccm.yml</code> file.</p>
<p>On Debian or similar, use <code>apt</code>:</p>
<pre><code class="language-bash">$ sudo apt install python3-pip yamllint</code></pre><p>On Fedora or similar, use <code>dnf</code>:</p>
<pre><code class="language-bash">$ sudo dnf install python3-pip yamllint</code></pre><p>You also need PyYAML, which you can install with the <code>pip</code> command:</p>
<pre><code class="language-bash">$ pip3 install --user PyYAML</code></pre><h2>Using nccm</h2>
<p>Before starting, edit the <code>nccm.yml</code> file and add your SSH configuration. Formatting YAML is easy, and there are examples provided in the file. Just follow the structure—provide the connection name at the beginning of the line, with config items indented two spaces. Don't forget the colons—these are part of the YAML language.</p>
<p>Don't worry about ordering your SSH session blocks in any specific way, because <code>nccm</code> gives you "sort by" options within the program.</p>
<p>Once you've finished editing, check your work with <code>yamllint</code>:</p>
<pre><code class="language-bash">$ yamllint ~/.config/nccm/nccm.yml</code></pre><p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Linux resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux commands cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Free online course: RHEL Technical Overview</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux networking cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">SELinux cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux common commands cheat sheet</a></li>
<li><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">What are Linux containers?</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Our latest Linux articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>If no errors are returned, then you've formatted your file correctly, and it's safe to continue.</p>
<p>If <code>nccm</code> is accessible <a href="https://opensource.com/article/17/6/set-path-linux" target="_self">from your path</a> and is executable, then typing <code>nccm</code> is all that's required to launch the TUI (terminal user interface). If you see Python 3 exceptions, check whether you have satisfied the dependencies. Any exceptions should mention any package that's missing.</p>
<p>As long as you're using the YAML config file without changing <code>nccm_config_control mode</code>, then you can use these keyboard controls:</p>
<ul><li>Up/Down arrows - Move the marker the traditional way</li>
<li>Home/End - Jump marker to list first/last entry</li>
<li>PgUp/PgDn - Page up/down in the list</li>
<li>Left/Right arrows - Scroll the list horizontally</li>
<li>TAB - Moves the cursor between text boxes</li>
<li>Enter - Connect to the selected entry</li>
<li>Ctrl-h - Display this help menu</li>
<li>Ctrl-q or Ctrl-c - Quit the program</li>
<li>F1-F5 or !@#$% - Sort by respective column (1-5)</li>
</ul><p>Use keys F1 through F5 to sort by columns 1 through 5. If your desktop captures F-key input, you can instead sort by pressing <strong>!@#$%</strong> in the "Conn" text box. The display shows 4 visible columms but we treat username and server address as separate columns for sorting purposes giving us 5 controls for sorting. You can reverse the order by pressing the same "sort" key a second time. A connection can be made by pressing <strong>Enter</strong> on the highlighted line.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="nccm screenshot terminal view"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/nccm_screenshot.png" width="675" height="357" alt="nccm screenshot terminal view" title="nccm screenshot terminal view" /></div>
      
  </article></p>
<p>Typing text into the "Filter" text box filters the output with an "and" function between everything entered. This is case-insensitive, and a blank space delimits entries. The same is true for the "Conn" text box, but pressing <strong>Enter</strong> here connects to that specific entry number.</p>
<p>There are a few more interesting features to discover, such as focus mode, but I'll leave it up to you to explore the details. See the project page or built-in help for more details.</p>
<p>The config YAML file is well-documented, so you'll know how to edit the settings to make <code>nccm</code> work best for you. The <code>nccm</code> program is highly commented, too, so you may wish to fork or mod it to add more features. Pull requests are welcome!</p>
<h2>Relax into SSH with nccm</h2>
<p>I hope this program serves you well and is as useful to you as it is to me. Thanks for being part of the open source community, and please accept <code>nccm</code> as my contribution to the ongoing efforts toward seamless, painless, and efficient computing experiences.</p>
