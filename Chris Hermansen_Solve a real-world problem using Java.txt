<p>As I wrote in the first two articles in this series, I enjoy solving small problems by writing small programs in different languages, so I can compare the different ways they approach the solution. The example I'm using in this series is dividing bulk supplies into hampers of similar value to distribute to struggling neighbors in your community, which you can <a href="https://opensource.com/article/20/8/solving-problem-groovy">read about</a> in the first article in this series.</p>
<p>In the first article, I solved this problem <a href="https://opensource.com/article/20/8/solving-problem-groovy">using the Groovy programming language</a>, which is like Python in many ways, but syntactically it's more like C and Java. In the second article, I <a href="https://opensource.com/article/20/8/solving-problem-python">solved it in Python</a> with a very similar design and effort, which demonstrates the resemblance between the languages.</p>
<p>Now I'll try it in <a href="https://www.java.com/en/" target="_blank">Java</a>.</p>
<h2 id="the-java-solution">The Java solution</h2>
<p>When working in Java, I find myself declaring utility classes to hold tuples of data (the new record feature is going to be great for that), rather than using the language support for maps offered in Groovy and Python. This is because Java encourages creating maps that map one specific type to another specific type, but in Groovy or Python, it's cool to have a map with mixed-type keys and mixed-type values.</p>
<p>The first task is to define these utility classes, and the first is the <code>Unit</code> class:</p>
<pre><code class="language-java">class Unit {
    private String item, brand;
    private int price;

    public Unit(String item, String brand, int price) {
        this.item = item;
        this.brand = brand;
        this.price = price;
    }
    public String getItem() { return this.item; }
    public String getBrand() { return this.brand; }
    public int getPrice() { return this.price; }

    @Override
    public String toString() { return String.format("item: %s brand: %s price: %d",item,brand,price); }
}</code></pre><p>There's nothing too startling here. I effectively created a class whose instances are immutable since there are no setters for fields <code>item</code>, <code>brand</code>, or <code>price</code> and they are declared <code>private</code>. As a general rule, I don't see value in creating a mutable data structure unless I'm going to mutate it; and in this application, I don't see any value in mutating the <code>Unit</code> class.</p>
<p>While more effort is required to create these utility classes, creating them encourages a bit more design effort than just using a map, which can be a good thing. In this case, I realized that a bulk package is composed of a number of individual units, so I created the <code>Pack</code> class:</p>
<pre><code class="language-java">class Pack {
    private Unit unit;
    private int count;

    public Pack(String item, String brand, int unitCount, int packPrice) {
        this.unit = new Unit(item, brand, unitCount &gt; 0 ? packPrice / unitCount : 0);
        this.count = unitCount;
    }

    public String getItem() { return unit.getItem(); }
    public String getBrand() { return unit.getBrand(); }
    public int getUnitPrice() { return unit.getPrice(); }
    public int getUnitCount() { return count; }
    public List&lt;Unit&gt; unpack() { return Collections.nCopies(count, unit); }


    @Override
    public String toString() { return String.format("item: %s brand: %s unitCount: %d unitPrice: %d",unit.getItem(),unit.getBrand(),count,unit.getPrice()); }
}</code></pre><p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on Java</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/topics/enterprise-java/?intcmp=7013a000002Cxq6AAC">What is enterprise Java programming?</a></li>
<li><a href="https://developers.redhat.com/products/openjdk/overview?intcmp=7013a000002Cxq6AAC">Red Hat build of OpenJDK</a></li>
<li><a href="https://opensource.com/downloads/java-cheat-sheet?intcmp=7013a000002Cxq6AAC">Java cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/do092-developing-cloud-native-applications-microservices-architectures?intcmp=7013a000002Cxq6AAC">Free online course: Developing cloud-native applications with microservices architectures</a></li>
<li><a href="https://opensource.com/tags/java?intcmp=7013a000002Cxq6AAC">Fresh Java articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>Similar to the <code>Unit</code> class, the <code>Pack</code> class is immutable. A couple of things worth mentioning here:</p>
<ol><li>I could have passed a <code>Unit</code> instance into the <code>Pack</code> constructor. I chose not to because the bundled, physical nature of a bulk package encouraged me to think of the "unit-ness" as an internal thing not visible from the outside but that requires unpacking to expose the units. Is this an important decision in this case? Probably not, but to me, at least, it's always good to think through this kind of consideration.</li>
<li>Which leads to the <code>unpack()</code> method. The <code>Pack</code> class creates the list of <code>Unit</code> instances only when you call this method—that is, the class is <em>lazy</em>. As a general design principle, I've found it's worthwhile to decide whether a class' behavior should be eager or lazy, and when it doesn't seem to matter, I go with lazy. Is this an important decision in this case? Maybe—this lazy design enables a new list of <code>Unit</code> instances to be generated on every call of <code>unpack()</code>, which could prove to be a good thing down the road. In any case, getting in the habit of always thinking about eager vs. lazy behavior is a good habit.</li>
</ol><p>The sharp-eyed reader will note that, unlike in the Groovy and Python examples where I was mostly focused on compact code and spent way less time thinking about design decisions, here, I separated the definition of a <code>Pack</code> from the number of <code>Pack</code> instances purchased. Again, from a design perspective, this seemed like a good idea as the <code>Pack</code> is conceptually quite independent of the number of <code>Pack</code> instances acquired.</p>
<p>Given this, I need one more utility class: the <code>Bought</code> class:</p>
<pre><code class="language-java">class Bought {
    private Pack pack;
    private int count;

    public Bought(Pack pack, int packCount) {
        this.pack = pack;
        this.count = packCount;
    }

    public String getItem() { return pack.getItem(); }
    public String getBrand() { return pack.getBrand(); }
    public int getUnitPrice() { return pack.getUnitPrice(); }
    public int getUnitCount() { return pack.getUnitCount() * count; }
    public List&lt;Unit&gt; unpack() { return Collections.nCopies(count, pack.unpack()).stream().flatMap(List::stream).collect(Collectors.toList()); }

    @Override
    public String toString() { return String.format("item: %s brand: %s bought: %d pack(s) totalUnitCount: %d unitPrice: %d",pack.getItem(),pack.getBrand(),count,pack.getUnitCount() * count,pack.getUnitPrice()); }
}</code></pre><p>Notably:</p>
<ol><li>I decided to pass a <code>Pack</code> into the constructor. Why? Because to my way of thinking, the physical structure of the purchased bulk packages is external, not internal, as in the case of the individual bulk packages. Once again, it may not be important in this application, but I believe it's always good to think about these things. If nothing else, note that I am not married to symmetry!</li>
<li>Once again the <code>unpack()</code> method demonstrates the lazy design principle. This goes to more effort to generate a list of <code>Unit</code> instances (rather than a <em>list of lists</em> of <code>Unit</code> instances, which would be easier but require flattening further out in the code).</li>
</ol><p>OK! Time to move on and solve the problem. First, declare the purchased packs:</p>
<pre><code class="language-java">        var packs = new Bought[] {
            new Bought(new Pack("Rice","Best Family",10,5650),1),
            new Bought(new Pack("Spaghetti","Best Family",1,327),10),
            new Bought(new Pack("Sardines","Fresh Caught",3,2727),3),
            new Bought(new Pack("Chickpeas","Southern Style",2,2600),5),
            new Bought(new Pack("Lentils","Southern Style",2,2378),5),
            new Bought(new Pack("Vegetable oil","Crafco",12,10020),1),
            new Bought(new Pack("UHT milk","Atlantic",6,4560),2),
            new Bought(new Pack("Flour","Neighbor Mills",10,5200),1),
            new Bought(new Pack("Tomato sauce","Best Family",1,190),10),
            new Bought(new Pack("Sugar","Good Price",1,565),10),
            new Bought(new Pack("Tea","Superior",5,2720),2),
            new Bought(new Pack("Coffee","Colombia Select",2,4180),5),
            new Bought(new Pack("Tofu","Gourmet Choice",1,1580),10),
            new Bought(new Pack("Bleach","Blanchite",5,3550),2),
            new Bought(new Pack("Soap","Sunny Day",6,1794),2)
        };</code></pre><p>This is pretty nice from a readability perspective: there is one pack of Best Family Rice containing 10 units that cost 5,650 (using those crazy monetary units, like in the other examples). It's straightforward to see that in addition to the one bulk pack of 10 bags of rice, the organization acquired 10 bulk packs of one bag each of spaghetti. The utility classes are doing some work under the covers, but that's not important at this point because of the great design job!</p>
<p>Note the <code>var</code> keyword is used here; it's one of the nice features in recent versions of Java that help make the language a bit less verbose (the principle is called <em>DRY</em>—don't repeat yourself) by letting the compiler infer the variable's data type from the right-side expression's type. This looks kind of similar to the Groovy <code>def</code> keyword, but since Groovy by default is dynamically typed and Java is statically typed, the typing information inferred in Java by <code>var</code> persists throughout the lifetime of that variable.</p>
<p>Finally, it's worth mentioning that <code>packs</code> here is an array and not a <code>List</code> instance. If you were reading this data from a separate file, you would probably prefer to create it as a list.</p>
<p>Next, unpack the bulk packages. Because the unpacking of <code>Pack</code> instances is delegated into lists of <code>Unit</code> instances, you can use that like this:</p>
<pre><code class="language-java">        var units = Stream.of(packs)
            .flatMap(bought -&gt; {
                return bought.unpack().stream(); })
            .collect(Collectors.toList());</code></pre><p>This uses some of the nice functional programming features introduced in later Java versions. Convert the array <code>packs</code> declared previously to a Java stream, use <code>flatmap()</code> with a lambda to flatten the sublists of units generated by the <code>unpack()</code> method of the <code>Bought</code> class, and collect the resulting stream elements back into a list.</p>
<p>As in the Groovy and Java solutions, the final step is repacking the units into the hampers for distribution. Here's the code—it's not much wordier than the Groovy version (tiresome semicolons aside) nor really all that different:</p>
<pre><code class="language-java">        var valueIdeal = 5000;
        var valueMax = Math.round(valueIdeal * 1.1);
        var rnd = new Random();
        var hamperNumber = 0;                         // [1]

        while (units.size() &gt; 0) {                    // [2]
            hamperNumber++;
            var hamper = new ArrayList&lt;Unit&gt;();
            var value = 0;                            // [2.1]
            for (boolean canAdd = true; canAdd; ) {   // [2.2]
                var u = rnd.nextInt(units.size());            // [2.2.1]
                canAdd = false;                               // [2.2.2]
                for (int o = 0; o &lt; units.size(); o++) {      // [2.2.3]
                    var uo = (u + o) % units.size();
                    var unit = units.get(uo);                      // [2.2.3.1]
                    if (units.size() &lt; 3 ||
                            !hamper.contains(unit) &amp;&amp;
                            (value + unit.getPrice()) &lt; valueMax) { // [2.2.3.2]
                        hamper.add(unit);
                        value += unit.getPrice();
                        units.remove(uo);                           // [2.2.3.3]
                        canAdd = units.size() &gt; 0;
                        break;                                      // [2.2.3.4]
                    }
                }
            }                                                // [2.2.4]
            System.out.println();
            System.out.printf("Hamper %d value %d:\n",hamperNumber,value);
            hamper.forEach(unit -&gt; {
                System.out.printf("%-25s%-25s%7d\n", unit.getItem(), unit.getBrand(),
                       unit.getPrice());
            });                                                      // [2.3]
            System.out.printf("Remaining units %d\n",units.size());  // [2.4]</code></pre><p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on Java</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/topics/enterprise-java/?intcmp=7013a000002Cxq6AAC">What is enterprise Java programming?</a></li>
<li><a href="https://developers.redhat.com/products/openjdk/overview?intcmp=7013a000002Cxq6AAC">Red Hat build of OpenJDK</a></li>
<li><a href="https://opensource.com/downloads/java-cheat-sheet?intcmp=7013a000002Cxq6AAC">Java cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/do092-developing-cloud-native-applications-microservices-architectures?intcmp=7013a000002Cxq6AAC">Free online course: Developing cloud-native applications with microservices architectures</a></li>
<li><a href="https://opensource.com/tags/java?intcmp=7013a000002Cxq6AAC">Fresh Java articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>Some clarification, with numbers in brackets in the comments above (e.g., <em>[1]</em>) corresponding to the clarifications below:</p>
<ul style="list-style-type:none;"><li>1. Set up the ideal and maximum values to be loaded into any given hamper, initialize Java's random number generator and the hamper number.</li>
<li>2. This <code>while {}</code> loop will redistribute units into hampers as long as there are more available:<br /><ul style="list-style-type:none;"><li>2.1 Increment the hamper number, get a new empty hamper (a list of <code>Unit</code> instances), and set its value to 0.</li>
<li>2.2 This <code>for {}</code> loop will add as many units to the hamper as possible:<br /><ul style="list-style-type:none;"><li>2.2.1 Get a random number between zero and the number of remaining units minus 1.</li>
<li>2.2.2 Assume you can't find more units to add.</li>
<li>2.2.3 This <code>for {}</code> loop, starting at the randomly chosen index, will try to find a unit that can be added to the hamper.<br /><ul style="list-style-type:none;"><li>2.2.3.1 Figure out which unit to look at.</li>
<li>2.2.3.2 Add this unit to the hamper if there are only a few left or if the value of the hamper isn't too high once the unit is added <em>and</em> that unit isn't already in the hamper.</li>
<li>2.2.3.3 Add the unit to the hamper, increment the hamper value by the unit price, and remove the unit from the available units list.</li>
<li>2.2.3.4 As long as there are units left, you can add more, so break out of this loop to keep looking.</li>
</ul></li>
<li>2.2.4 On exit from this <code>for {}</code> loop, if you inspected every remaining unit and could not find one to add to the hamper, the hamper is complete; otherwise, you found one and can continue looking for more.</li>
</ul></li>
<li>2.3 Print out the contents of the hamper.</li>
<li>2.4 Print out the remaining units info.</li>
</ul></li>
</ul><p>When you run this code, the output looks quite similar to the output from the Groovy and Python programs:</p>
<pre><code class="language-text">Hamper 1 value 5465:
Tofu                     Gourmet Choice              1580
Bleach                   Blanchite                    710
Coffee                   Colombia Select             2090
Flour                    Neighbor Mills               520
Sugar                    Good Price                   565
Remaining units 150

Hamper 2 value 5482:
Sardines                 Fresh Caught                 909
Tomato sauce             Best Family                  190
Vegetable oil            Crafco                       835
UHT milk                 Atlantic                     760
Chickpeas                Southern Style              1300
Lentils                  Southern Style              1189
Soap                     Sunny Day                    299
Remaining units 143

Hamper 3 value 5353:
Soap                     Sunny Day                    299
Rice                     Best Family                  565
UHT milk                 Atlantic                     760
Flour                    Neighbor Mills               520
Vegetable oil            Crafco                       835
Bleach                   Blanchite                    710
Tomato sauce             Best Family                  190
Sardines                 Fresh Caught                 909
Sugar                    Good Price                   565
Remaining units 134

…

Hamper 23 value 5125:
Sardines                 Fresh Caught                 909
Rice                     Best Family                  565
Spaghetti                Best Family                  327
Lentils                  Southern Style              1189
Chickpeas                Southern Style              1300
Vegetable oil            Crafco                       835
Remaining units 4

Hamper 24 value 2466:
UHT milk                 Atlantic                     760
Spaghetti                Best Family                  327
Vegetable oil            Crafco                       835
Tea                      Superior                     544
Remaining units 0</code></pre><p>The last hamper is abbreviated in contents and value.</p>
<h2 id="closing-thoughts">Closing thoughts</h2>
<p>The similarities in the "working code" with the Groovy original are obvious—the close relationship between Groovy and Java is evident. Groovy and Java diverged in a few ways in things that were added to Java after Groovy was released, such as the <code>var</code> vs. <code>def</code> keywords and the superficial similarities and differences between Groovy closures and Java lambdas. Moreover, the whole Java streams framework adds a great deal of power and expressiveness to the Java platform (full disclosure, in case it's not obvious—I am but a babe in the Java streams woods).</p>
<p>Java's intent to use maps for mapping instances of a single type to instances of another single type pushes you to use utility classes, or tuples, instead of the more inherently flexible intents in Groovy maps (which are basically just <code>Map&lt;Object,Object&gt;</code> plus a lot of syntactic sugar to vanish the kinds of casting and <code>instanceof</code> hassles that you would create in Java) or in Python. The bonus from this is the opportunity to apply some real design effort to these utility classes, which pays off at least insofar as it instills good habits in the programmer.</p>
<p>Aside from the utility classes, there isn't a lot of additional ceremony nor boilerplate in the Java code compared to the Groovy code. Well, except that you need to add a bunch of imports and wrap the "working code" in a class definition, which might look like this:</p>
<pre><code class="language-java">import java.lang.*;
import java.util.*;
import java.util.Collections.*;
import java.util.stream.*;
import java.util.stream.Collectors.*;
import java.util.Random.*;

public class Distribute {

    static public void main(String[] args) {
        // the working code shown above
    }
}
class Unit { … }
class Pack { … }
class Bought { … }</code></pre><p>The same fiddly bits are necessary in Java as they are in Groovy and Python when it comes to grabbing stuff out of the list of <code>Unit</code> instances for the hampers, involving random numbers, loops through remaining units, etc.</p>
<p>Another issue worth mentioning—this isn't a particularly efficient approach. Removing elements from <code>ArrayLists</code>, being careless about repeated expressions, and a few other things make this less suitable for a huge redistribution problem. I've been a bit more careful here to stick with integer data. But at least it's quite quick to execute.</p>
<p>Yes, I'm still using the dreaded <code>while { … }</code> and <code>for { … }</code>. I still haven't thought of a way to use map and reduce style stream processing in conjunction with a random selection of units for repackaging. Can you?</p>
<p>Stay tuned for the next articles in this series, with versions in <a href="https://julialang.org/" target="_blank">Julia</a> and <a href="https://golang.org/" target="_blank">Go</a>.</p>
