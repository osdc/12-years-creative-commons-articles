<p>Automated tests are required to ensure your program's quality and that it works as expected. Unit tests examine only certain parts of your algorithm, but don't look at how each component fits together. That's where functional testing, sometimes referred as integration testing, comes in.</p>
<p>A functional test basically interacts with your user interface, whether through a website or a desktop application. To show you how that works, let's look at how to test a Gtk+ application. For simplicity, in this tutorial let's use the <a href="https://developer.gnome.org/gtk-tutorial/stable/" target="_blank">Tictactoe</a> example from the Gtk+ 2.0 tutorial.</p>
<h2>Basic setup</h2>
<p>For every functional test, you usually define some global variables, such as "user interaction delay" or "timeout until a failure is indicated" (i.e., when an event doesn't occur until the specified time and the application is doomed).</p>
<pre>
<small><code class="language-c">#define TTT_FUNCTIONAL_TEST_UTIL_IDLE_CONDITION(f) ((TttFunctionalTestUtilIdleCondition)(f))
#define TTT_FUNCTIONAL_TEST_UTIL_REACTION_TIME (125000)
#define TTT_FUNCTIONAL_TEST_UTIL_REACTION_TIME_LONG (500000)
typedef gboolean (*TttFunctionalTestUtilIdleCondition)(gpointer data);
struct timespec ttt_functional_test_util_default_timeout = {
  20,
  0,
};</code></small></pre><p>Now we can implement our dead-time functions. Here, we'll use the <b>usleep</b> function in order to get the desired delay.</p>
<pre>
<small><code class="language-c">void
ttt_functional_test_util_reaction_time()
{
  usleep(TTT_FUNCTIONAL_TEST_UTIL_REACTION_TIME);
}

void
ttt_functional_test_util_reaction_time_long()
{
  usleep(TTT_FUNCTIONAL_TEST_UTIL_REACTION_TIME_LONG);
}
</code></small></pre><p>The timeout function delays execution until a state of a control is applied. It is useful for actions that are applied asynchronously, and that is why it delays for a longer period of time.</p>
<pre>
<small><code class="language-c">void
ttt_functional_test_util_idle_condition_and_timeout(
     TttFunctionalTestUtilIdleCondition idle_condition,
     struct timespec *timeout,
     pointer data)
{
  struct timespec start_time, current_time;

  clock_gettime(CLOCK_MONOTONIC,
                &amp;start_time);

  while(TTT_FUNCTIONAL_TEST_UTIL_IDLE_CONDITION(idle_condition)(data)){
    ttt_functional_test_util_reaction_time();

    clock_gettime(CLOCK_MONOTONIC,
                  &amp;current_time);

    if(start_time.tv_sec + timeout-&gt;tv_sec &lt; current_time.tv_sec){
      break;
    }
  }

  ttt_functional_test_util_reaction_time();
}</code></small></pre><h2>Interacting with the graphical user interface</h2>
<p>In order to simulate user interaction, the <a href="https://developer.gnome.org/gdk3/stable/" target="_blank"><b>Gdk library</b></a> provides the functions we need. To do our work here, we need only these three functions:</p>
<ul><li>gdk_display_warp_pointer()</li>
<li>gdk_test_simulate_button()</li>
<li>gdk_test_simulate_key()</li>
</ul><p>For instance, to test a button click, we do the following:</p>
<pre>
<small><code class="language-c">gboolean
ttt_functional_test_util_button_click(GtkButton *button)
{
  GtkWidget *widget;

  GdkWindow *window;

  gint x, y;
  gint origin_x, origin_y;

  if(button == NULL ||
     !GTK_IS_BUTTON(button)){
    return(FALSE);
  }

  widget = button;

  if(!GTK_WIDGET_REALIZED(widget)){
    ttt_functional_test_util_reaction_time_long();
  }

  /* retrieve window and pointer position */
  gdk_threads_enter();

  window = gtk_widget_get_window(widget);

  x = widget-&gt;allocation.x + widget-&gt;allocation.width / 2.0;
  y = widget-&gt;allocation.y + widget-&gt;allocation.height / 2.0;

  gdk_window_get_origin(window, &amp;origin_x, &amp;origin_y);

  gdk_display_warp_pointer(gtk_widget_get_display(widget),
                           gtk_widget_get_screen(widget),
                           origin_x + x, origin_y + y);

  gdk_threads_leave();

  /* click the button */
  ttt_functional_test_util_reaction_time();

  gdk_test_simulate_button(window,
                           x,
                           y,
                           1,
                           GDK_BUTTON1_MASK,
                           GDK_BUTTON_PRESS);

  ttt_functional_test_util_reaction_time();

  gdk_test_simulate_button(window,
                           x,
                           y,
                           1,
                           GDK_BUTTON1_MASK,
                           GDK_BUTTON_RELEASE);

  ttt_functional_test_util_reaction_time();

  ttt_functional_test_util_reaction_time_long();

  return(TRUE);
}</code></small></pre><p>We want to ensure the button has an active state, so we provide an idle-condition function:</p>
<pre>
<small><code class="language-c">gboolean
ttt_functional_test_util_idle_test_toggle_active(
     GtkToggleButton **toggle_button)
{
  gboolean do_idle;

  do_idle = TRUE;

  gdk_threads_enter();

  if(*toggle_button != NULL &amp;&amp;
     GTK_IS_TOGGLE_BUTTON(*toggle_button) &amp;&amp;
     gtk_toggle_button_get_active(*toggle_button)){
    do_idle = FALSE;
  }

  gdk_threads_leave();

  return(do_idle);
}</code></small></pre><h2>The test scenario</h2>
<p>Since the Tictactoe program is very simple, we just need to ensure that a <a href="https://developer.gnome.org/gtk3/stable/GtkToggleButton.html" target="_blank"><b>GtkToggleButton</b></a> was clicked. The functional test can proceed once it asserts the button entered the active state. To click the buttons, we use the handy <b>util</b> function provided above.</p>
<p>For illustration, let's assume player A wins immediately by filling the very first row, because player B is not paying attention and just filled the second row:</p>
<pre>
<small><code class="language-c">GtkWindow *window;
Tictactoe *ttt;

void*
ttt_functional_test_gtk_main(void *)
{
  gtk_main();

  pthread_exit(NULL);
}

void
ttt_functional_test_dumb_player_b()
{
  GtkButton *buttons[3][3];

  guint i;
  
  /* to avoid race-conditions copy the buttons */
  gdk_threads_enter();
  
  memcpy(buttons, ttt-&gt;buttons, 9 * sizeof(GtkButton *));

  gdk_threads_leave();

  /* TEST 1 - the dumb player B */
  for(i = 0; i &lt; 3; i++){
    /* assert player A clicks the button successfully */
    if(!ttt_functional_test_util_button_click(buttons[0][i])){
      exit(-1);
    }

    functional_test_util_idle_condition_and_timeout(
         ttt_functional_test_util_idle_test_toggle_active,
         ttt_functional_test_util_default_timeout,
         &amp;buttons[0][i]);


    /* assert player B clicks the button successfully */
    if(!ttt_functional_test_util_button_click(buttons[1][i])){
      exit(-1);
    }

    functional_test_util_idle_condition_and_timeout(
         ttt_functional_test_util_idle_test_toggle_active,
         ttt_functional_test_util_default_timeout,
         &amp;buttons[1][i]);
  }
}

int
main(int argc, char **argv)
{
  pthread_t thread;

  gtk_init(&amp;argc, &amp;argv);
		 
  /* start the tictactoe application */
  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

  ttt = tictactoe_new();
  gtk_container_add(window, ttt);

  gtk_widget_show_all(window);

  /* start the Gtk+ dispatcher */
  pthread_create(&amp;thread, NULL,
                 ttt_functional_test_gtk_main, NULL);

  /* launch test routines */
  ttt_functional_test_dumb_player_b();

  /* terminate the application */
  gdk_threads_enter();

  gtk_main_quit();
  
  gdk_threads_leave();

  return(0);
}</code></small></pre>