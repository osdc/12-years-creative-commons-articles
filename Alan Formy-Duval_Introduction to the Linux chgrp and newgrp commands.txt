<p>In a recent article, I introduced the <a href="https://opensource.com/article/19/8/linux-chown-command"><strong>chown</strong> command</a>, which is used for modifying ownership of files on systems. Recall that ownership is the combination of the user and group assigned to an object. The <strong>chgrp</strong> and <strong>newgrp</strong> commands provide additional help for managing files that need to maintain group ownership.</p>
<h2 id="using-chgrp">Using chgrp</h2>
<p>The <strong>chgrp</strong> command simply changes the group ownership of a file. It is the same as the <strong>chown :&lt;group&gt;</strong> command. You can use:</p>
<pre><code class="language-bash">$chown :alan mynotes</code></pre><p>or:</p>
<pre><code class="language-bash">$chgrp alan mynotes</code></pre><h3 id="recursive">Recursive</h3>
<p>A few additional arguments to chgrp can be useful at both the command line and in a script. Just like many other Linux commands, chgrp has a recursive argument, <strong>-R</strong>. You will need this to operate on a directory and its contents recursively, as I'll demonstrate below. I added the <strong>-v</strong> (<strong>verbose</strong>) argument so chgrp tells me what it is doing:</p>
<pre><code class="language-bash">$ ls -l . conf
.:
drwxrwxr-x 2 alan alan 4096 Aug  5 15:33 conf

conf:
-rw-rw-r-- 1 alan alan 0 Aug  5 15:33 conf.xml
# chgrp -vR delta conf 
changed group of 'conf/conf.xml' from alan to delta
changed group of 'conf' from alan to delta</code></pre><h3 id="reference">Reference</h3>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Linux resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux commands cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Free online course: RHEL Technical Overview</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux networking cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">SELinux cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux common commands cheat sheet</a></li>
<li><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">What are Linux containers?</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Our latest Linux articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>A reference file (<strong>--reference=RFILE</strong>) can be used when changing the group on files to match a certain configuration or when you don't know the group, as might be the case when running a script. You can duplicate another file's group (<strong>RFILE</strong>), referred to as a reference file. For example, to undo the changes made above (recall that a dot [<strong>.</strong>] refers to the present working directory):</p>
<pre><code class="language-bash">$ chgrp -vR --reference=. conf</code></pre><h3 id="report-changes">Report changes</h3>
<p>Most commands have arguments for controlling their output. The most common is <strong>-v</strong> to enable verbose, and the chgrp command has a verbose mode. It also has a <strong>-c</strong> (<strong>--changes</strong>) argument, which instructs chgrp to report only when a change is made. Chgrp will still report other things, such as if an operation is not permitted.</p>
<p>The argument <strong>-f</strong> (<strong>--silent</strong>, <strong>--quiet</strong>) is used to suppress most error messages. I will use this argument and <strong>-c</strong> in the next section so it will show only actual changes.</p>
<h3 id="preserve-root">Preserve root</h3>
<p>The root (<strong>/</strong>) of the Linux filesystem should be treated with great respect. If a command mistake is made at this level, the consequences can be terrible and leave a system completely useless. Particularly when you are running a recursive command that will make any kind of change—or worse, deletions. The chgrp command has an argument that can be used to protect and preserve the root. The argument is <strong>--preserve-root</strong>. If this argument is used with a recursive chgrp command on the root, nothing will happen and a message will appear instead:</p>
<pre><code class="language-bash">[root@localhost /]# chgrp -cfR --preserve-root a+w /
chgrp: it is dangerous to operate recursively on '/'
chgrp: use --no-preserve-root to override this failsafe</code></pre><p>The option has no effect when it's not used in conjunction with recursive. However, if the command is run by the root user, the permissions of <strong>/</strong> will change, but not those of other files or directories within it:</p>
<pre><code class="language-bash">[alan@localhost /]$ chgrp -c --preserve-root alan /
chgrp: changing group of '/': Operation not permitted
[root@localhost /]# chgrp -c --preserve-root alan /
changed group of '/' from root to alan</code></pre><p>Surprisingly, it seems, this is not the default argument. The option <strong>--no-preserve-root</strong> is the default. If you run the command above without the "preserve" option, it will default to "no preserve" mode and possibly change permissions on files that shouldn't be changed:</p>
<pre><code class="language-bash">[alan@localhost /]$ chgrp -cfR alan /
changed group of '/dev/pts/0' from tty to alan
changed group of '/dev/tty2' from tty to alan
changed group of '/var/spool/mail/alan' from mail to alan</code></pre><h2 id="about-newgrp">About newgrp</h2>
<p>The <strong>newgrp</strong> command allows a user to override the current primary group. newgrp can be handy when you are working in a directory where all files must have the same group ownership. Suppose you have a directory called <em>share</em> on your intranet server where different teams store marketing photos. The group is <strong>share</strong>. As different users place files into the directory, the files' primary groups might become mixed up. Whenever new files are added, you can run <strong>chgrp</strong> to correct any mix-ups by setting the group to <strong>share</strong>:</p>
<pre><code class="language-bash">$ cd share
ls -l
-rw-r--r--. 1 alan share 0 Aug  7 15:35 pic13
-rw-r--r--. 1 alan alan 0 Aug  7 15:35 pic1
-rw-r--r--. 1 susan delta 0 Aug  7 15:35 pic2
-rw-r--r--. 1 james gamma 0 Aug  7 15:35 pic3
-rw-rw-r--. 1 bill contract  0 Aug  7 15:36 pic4</code></pre><p>I covered <strong>setgid</strong> mode in my article on the <a href="https://opensource.com/article/19/8/linux-chmod-command"><strong>chmod</strong> command</a>. This would be one way to solve this problem. But, suppose the setgid bit was not set for some reason. The newgrp command is useful in this situation. Before any users put files into the <em>share</em> directory, they can run the command <strong>newgrp share</strong>. This switches their primary group to <strong>share</strong> so all files they put into the directory will automatically have the group <strong>share</strong>, rather than the user's primary group. Once they are finished, users can switch back to their regular primary group with (for example):</p>
<pre><code class="language-bash">newgrp alan</code></pre><h2 id="conclusion">Conclusion</h2>
<p>It is important to understand how to manage users, groups, and permissions. It is also good to know a few alternative ways to work around problems you might encounter since not all environments are set up the same way.</p>
