<p>Adam Drew makes open source music. He writes, records, and produces all his own sounds using Linux and all FOSS tools. <a href="https://opensource.com/life/11/4/webcast-preview-free-and-open-source-software-music-production">Read more in his post from last week.</a></p>
<!--break-->
<p>Wednesday, April 20, he'll be joining us for an <a href="https://engage.redhat.com/forms/20110420MakingMusic">Open Your World webcast</a> to explain it all. He'll get in-depth on the pieces and how to make them work together so you can get started making your own open source music. Watch Adam for a preview of what you can expect:</p>
<p><iframe width="560" height="315" src="https://www.youtube.com/embed/eU8wlgwTe50" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe></p>
<p><a href="https://engage.redhat.com/forms/20110420MakingMusic">Register for Adam's webcast</a></p>
