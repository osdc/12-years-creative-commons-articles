<p>Krita is best known as a digital painting application, but in my experience, it's kind of a digital imaging powerhouse. Recently, a fork of GIMP called GLIMPSE had to pause its development, and because I like alternatives, it occurred to me that Krita could be a reasonable photo editor for at least some use cases. It isn't easy to measure the suitability of an application for a group of tasks because different people require or prefer different things. What's very common and indispensable to one person is an edge case for someone else. However, photo editing applications are broadly known for a primarily universal set of functions, so I decided to look at those likely to be in a top 5 must-have list and test Krita against each one.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Great Content</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=7016000000127cYAAQ">Free online course: RHEL technical overview</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=7016000000127cYAAQ">Learn Advanced Linux Commands</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Download Cheat Sheets</a></li>
<li><a href="https://opensource.com/alternatives?intcmp=7016000000127cYAAQ">Find an Open Source Alternative</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=7016000000127cYAAQ">Read Top Linux Content</a></li>
<li><a href="https://opensource.com/resources?intcmp=7016000000127cYAAQ">Check out open source resources</a></li>
</ul></div>
</div>
</div>
</div>

<h2>1. Layers</h2>
<p>Photo editors have layers. That was one of the first game-changing features of digital photo editors, and it remains one of the most basic requirements. Krita, of course, has layers, and in fact, so many paint and photo applications do these days that I probably could have glossed over it. There's a reason I mention this as a requirement, though. Krita doesn't just have layers. It's got twelve different kinds of layers. Of course, <em>more</em> doesn't always mean <em>better</em>. What matters is what Krita does with all those layers.</p>
<ol><li><strong>Paint:</strong> The default kind of layer containing raster data.</li>
<li><strong>Vector:</strong> It's no Inkscape, but Krita does support vector and has several good vector drawing tools. This layer stores vector data, including text.</li>
<li><strong>Group:</strong> A folder for layers.</li>
<li><strong>Filter:</strong> A layer containing nothing but an effect, which gets applied to all layers below it, as if you were looking at your image through a special lens. Combined with a group layer, you can apply this lens to only the layers you want.</li>
<li><strong>Filter mask:</strong> Like a filter layer, a filter mask is a layer that gets attached to just one other layer. Because it is a layer and not just a jumble of settings, it's easy to duplicate on other layers as needed, and it can stack with other filters.</li>
<li><strong>Colorize mask:</strong> Colorize regions of a picture with just one stroke of a paintbrush. This is very much an illustrator's tool, and I've found little to no use for this tool in photography. It could have interesting results for some photos, though.</li>
<li><strong>Clone:</strong> An effect layer that updates based on its "parent." It's a little like an alias or symlink for layers and can be helpful when you need one layer to be in more than one place. For example, you can group an effect layer and a clone layer to isolate a special effect without actually moving your source layer from its group.</li>
<li><strong>Fill:</strong> A layer containing a fill, which can be anything from a solid color to gradient to patterns or noise. Combined with compositing modes, there are great effects you can achieve with this, and because it's a layer type, it's easy to modify and change on a whim.</li>
<li><strong>File:</strong> Dynamically loads a file as a layer. When the file changes, so does the layer. Never open a dozen files to change a company logo, watermark, or iconography again.</li>
<li><strong>Selection:</strong> Store selections as layers attached to a paint layer, so you only have to select an important region once.</li>
<li><strong>Transparency:</strong> Have you ever wanted to paint with Alpha? Now you can. Black paint produces 100% transparency, white paint produces 0%, and all shades of gray are available in between.</li>
<li><strong>Transform:</strong> Apply transformation to a layer. Because it's a layer type, it's easy to modify and change on a whim.</li>
</ol><p>If the capabilities of these layers somehow haven't convinced you to use Krita as your photo editor, don't worry: Krita has much more to offer.</p>
<h2>2. Selections</h2>
<p>Photo editing applications need the ability to select sometimes complex and uneven parts of a photograph. Sometimes you want to remove an element from the frame. Other times you want to emphasize it, or duplicate it, or adjust it in some way. Krita has all the usual selection tools: rectangle, elliptical, freehand (also sometimes known as a "lasso"), polygonal, and contiguous color.</p>
<p>It also has a <em>similar color</em> selection tool, which is essentially a contiguous color selection for the entire image, contiguous or not.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Similar color selection in Krita"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/krita-similar-colour-selection.jpg" width="1042" height="658" alt="Similar color selection in Krita" title="Similar color selection in Krita" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Seth Kenlon, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>There's also a bezier selection tool and a magnetic selection tool that finds edges as you draw your selection. It's sort of an assisted freehand tool.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Magnetic selection in Krita"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/krita-magnetic-selection.jpg" width="1058" height="609" alt="Magnetic selection in Krita" title="Magnetic selection in Krita" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Seth Kenlon, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>This is a great assortment of selection tools, and I do find myself using them for quick and easy selections. However, once I'd discovered Selection Layers and the Global Mask layer, it was hard to use anything else.</p>
<p>To activate the global mask layer, go to the <strong>Select</strong> menu and choose <strong>Show Global Selection Mask</strong>. A (temporary) red overlay gets added to your image. Using an assortment of paintbrushes (and Krita has no shortage of variety in that department), you can poke through the mask with white paint or add to the mask with black paint.</p>
<p>Everything you paint white becomes a selection when you click back onto your image layer.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Selection mask"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/krita-mask-selection.jpg" width="1033" height="394" alt="Selection mask" title="Selection mask" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Seth Kenlon, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>It's a very precise and endlessly editable selection method, and its only fault is that it's so powerful. It's easy to lose hours of your time in pursuit of that mythical perfect mask, so pace yourself.</p>
<h2>3. Filters</h2>
<p>The industry moves in cycles. Once upon a time, drop shadows were the sign of advanced digital artistry, then 3D bevels, glows, gloss, reflection, and of course, lens flare. It's true that filters get over-used sometimes, but when used judiciously, they really are one of the primary reasons photo editors are so useful. Commonplace tasks like color balance, dodging and burning used to be impossible outside of a darkroom.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Color balance in Krita"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/krita-color-balance.jpg" width="1920" height="1080" alt="Color balance in Krita" title="Color balance in Krita" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Seth Kenlon, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>Open source has pioneered much of the most advanced imaging software (did you know that content-aware selection was created for GIMP?), and that includes effects. Krita benefits from this and takes full advantage of it. Not only does Krita offer the usual array of effects, like color balance, contrast, levels, threshold, saturation, and so on, but with the wildly popular <a href="https://gmic.eu" target="_blank">G'MIC</a> plugin, it offers hundreds of additional filters and effects.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="G'MIC plugin for Krita"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/krita-gmic.jpg" width="1920" height="1080" alt="G'MIC plugin for Krita" title="G'MIC plugin for Krita" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Seth Kenlon, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>The only things I noticed Krita lacking were spot dodge and burn tools. I rarely use these tools in a photo editor, and you can emulate them with a mask and a filter layer, so it's probably not an issue.</p>
<h2>4. CMYK and ICC profiles</h2>
<p>I've had plenty of work professionally printed, and I've never had any issue with it, but some people need the CMYK colorspace for their workflow. Krita has both the RGB and CMYK mode and full support for ICC colorspaces, so you can match modes and profiles to other applications in your pipeline. </p>
<h2>5. Retouching</h2>
<p>If you need to smooth over wrinkles, color in grays, redden lips, brighten eyes, remove a rubbish bin from the background, or otherwise retouch a photo, you're in luck. Digital painting is exactly what Krita was <em>actually</em> built to do. This is what Krita does best, and there are plenty of composite modes to help you integrate your paint with your photo.</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Smart patching with Krita"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/krita-smart-patch.jpg" width="1920" height="1080" alt="Smart patching with Krita" title="Smart patching with Krita" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Seth Kenlon, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<h2>Krita isn't just for painting anymore</h2>
<p>The Krita development team makes no claims about Krita being a photo editor. Still, after years of enjoying it for digital paint and materials emulation, I've found that Krita's development has, probably accidentally, converged with what a photographer needs. The tools provided by Krita for editing digital paint work just as well for pixels representing a photograph. I've enjoyed opening photos in Krita, and I've been pleased with the workflow and the results. Make no mistake: Krita is its own application, so you do have to re-train your muscles and sometimes approach a task from a different angle, but once you get familiar with the tools Krita offers, you may not want to give them up.</p>
<p>Krita is <a href="https://krita.org/en/download/krita-desktop/" target="_blank">available for Linux, Windows, and macOS</a>.</p>
<p>On Fedora, Mageia, and similar distributions, you can install it with your package manager:</p>
<pre><code class="language-bash">$ sudo dnf install krita</code></pre><p>On Elementary, Mint, and other Debian-based distributions:</p>
<pre><code class="language-bash">$ sudo apt install krita</code></pre><p>I use Krita as a <a href="https://opensource.com/article/21/11/install-flatpak-linux">Flatpak</a>.</p>
<p>If you want to support the continued development of Krita, you can also buy Krita for Linux and Windows on Steam.</p>
