<p>When I am checking out Linux systems (or even troubleshooting computers running other operating systems), I frequently use the <code>top</code> command to check out the system's RAM and CPU utilization. It provides me with information to assess the computer's overall health. I learned about the <code>top</code> command early in my Linux journey and have relied on it to give me a quick overview of what is happening on servers or other Linux systems, including Raspberry Pi. According to its man page, the <em>top program provides a dynamic real-time view of a running system. It can display system summary information as well as a list of processes or threads currently being managed by the Linux kernel</em>.</p>
<p>A quick overview is often all I need to determine what is going on with the system in question. But there is so much more to the <code>top</code> command than meets the eye. Specific features of your <code>top</code> command may vary depending on whose version (procps-ng, Busybox, BSD) you run, so consult the man page for details.</p>
<p>To launch <code>top</code>, type it into your terminal:</p>
<pre>
<code class="language-bash">$ top</code></pre><p>Running processes are displayed below the table heading on the <code>top</code> screen, and system statistics are shown above it.</p>
<pre>
<code class="language-bash">Top 05:31:09 up 55 min,3 users,load average: 0.54, 0.38, 0.46
Tasks: 469 total, 1 running, 468 sleeping,  0 stopped, 0 zombie
%Cpu(s): 1.0 us, 0.4 sy, 0.0 ni, 98.6 id, 0.1 wa, 0.0 hi,0.0 si,0.0 st
MiB Mem : 32116.1 total,  20256.5 free, 6376.3 used, 5483.3 buff/cache
MiB Swap: 0.0 total,  0.0 free,      0.0 used.  25111.4 avail Mem  

 PID USER  PR NI   VIRT    RES   SHR S %CPU %MEM    TIME+ COMMAND                                                 
2566 don   20  0  11.9g 701300 78848 S  3.3  2.1  2:03.80 firefox-bin
1606 don   20  0  24.2g  88084  4512 S  2.0  0.3  0:39.59 elisa
1989 don   20  0 894236 201580 23536 S  2.0  0.6  0:46.12 stopgo-java
5483 don   20  0  24.5g 239200 20868 S  1.3  0.7  0:26.54 Isolated Web Co
5726 don   20  0 977252 228012 44472 S  1.3  0.7  0:41.25 pulseaudio</code></pre><p>Press the <strong>Z</strong> key to change the color of the output. I find this makes the output a little easier on the eyes.</p>
<p>Press the <strong>1</strong> key to see a graphical representation of each CPU core on the system. Press <strong>1</strong> repeatedly to assess core statistics for your CPU cores.</p>
<p>You can display memory usage graphically by invoking the <code>top</code> command and then pressing the <strong>m</strong> key.</p>
<p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">More Linux resources</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux commands cheat sheet">Linux commands cheat sheet</a></div>
              <div class="field__item"><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Advanced Linux commands cheat sheet">Advanced Linux commands cheat sheet</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Free online course: RHEL technical overview">Free online course: RHEL technical overview</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux networking cheat sheet">Linux networking cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="SELinux cheat sheet">SELinux cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux common commands cheat sheet">Linux common commands cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="What are Linux containers?">What are Linux containers?</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Register for your free Red Hat account">Register for your free Red Hat account</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Our latest Linux articles">Our latest Linux articles</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<h2>Useful top options</h2>
<p>If you're looking only for the processes started by a specific user, you can get that information with the <code>-u</code> option:</p>
<pre>
<code class="language-bash">$ top -u 'username'</code></pre><p>To get a list of idle processes on your system, use the <code>-i</code> option:</p>
<pre>
<code class="language-bash">$ top -i</code></pre><p>You can set the update interval to an arbitrary value in seconds. The default value is three seconds. Change it to five like this:</p>
<pre>
<code class="language-bash">$ top -d 5</code></pre><p>You can also run <code>top</code> on a timer. For instance, the following command sets the number of iterations to two and then exits:</p>
<pre>
<code class="language-bash">$ top -n 2</code></pre><h2>Locate a process with top</h2>
<p>Press <strong>Shift+L</strong> to locate a process by name. This creates a prompt just above the bold table header line. Type in the name of the process you're looking for and then press <strong>Enter</strong> or <strong>Return</strong> to see the instances of that process highlighted in the newly sorted process list.</p>
<h2>Stopping a process with top</h2>
<p>You can stop or "kill" a running process with <code>top</code>, too. First, find the process you want to stop using either <strong>Shift+L</strong> or <code>pgrep</code>. Next, press <strong>K</strong> and enter the process ID you want to stop. The default value is whatever is at the top of the list, so be sure to enter the PID you want to stop before pressing <strong>Enter</strong>, or you may stop a process you didn't intend to.</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-03/top-results_0.png" width="780" height="210" alt="top results" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Don Watkins, CC BY-SA 4.0)</p>
</div>
      
  </article><h2>Top top</h2>
<p>There are many iterations of the <code>top</code> command, including <code>htop</code>, <code>atop</code>, <code>btop</code>, and <code>ttop</code>. There are specialized <code>top</code> commands, too, like <code>powertop</code> for power usage and <code>ntop</code> for networks. What's your favorite <code>top</code>?</p>
