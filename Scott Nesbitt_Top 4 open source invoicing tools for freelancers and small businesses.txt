<p>Small business owners and freelancers put a lot of work into their businesses. They do that not only because they’re passionate about what they do, but they also have the goal of getting paid.</p>
<!--break-->
<p>That’s no small part of the job, either.</p>
<p>Getting paid usually means sending a client an invoice. It’s easy enough to whip up an invoice using a word processor or a spreadsheet, but sometimes you need a bit more. A more professional look. A way of keeping track of your invoices. Reminders about when to follow up on the invoices that you’ve sent.</p>
<p>There’s a wide range of commercial and closed-source invoicing tools out there. But the offerings on the open source side of the fence are just as good, and maybe even more flexible than their closed source counterparts.</p>
<p>Let’s take a look at four open source invoicing tools that are great choices for freelancers and small businesses on a tight budget.</p>
<h2 id="what-to-look-for-in-an-invoicing-tool">What to look for in an invoicing tool</h2>
<p>Not all invoicing tools are created equally. Some, depending on their developers and their maturity, may have more or fewer features than most. But at its core, a good invoicing application has all or several of these features:</p>
<p>It should be accessible on the web. That can be on your own website or, if you lack the technical chops to install and maintain the tool, then it should have a hosted version.</p>
<p>It should also let you easily customize invoice templates to get the look and feel that you want. On top of that, the tool should have a either a reporting feature or the ability to save your records to a spreadsheet.</p>
<p>The tool should support multiple currencies and allow you to configure your local tax rates. Supporting multiple languages is a bonus.</p>
<p>Finally, the tool should be able to accept payments via PayPal or a payment service like Stripe. If it doesn’t so that out of the box, it should be easy to add that integration.</p>
<h2 id="simple-invoices">Simple Invoices</h2>
<p>While it’s name contains the word <em>simple</em>, <a href="http://www.simpleinvoices.org/">Simple Invoices</a> isn’t a barebones application. Far from it. Simple Invoices is easy to set up and use, but packs a number of useful features. Not only does it accept payment from PayPal but you can also hook Simple Invoices into a payment gateway called <a href="http://www.eway.com.au/">eWAY</a>.</p>
<p>Simple Invoices goes beyond invoicing. It can also track your inventory, create receipts, estimates, and quotes. You can save your invoices as PDF, Word, Excel, or text files.</p>
<p>Before you download and install Simple Invoices, you can <a href="http://demo.simpleinvoices.org/">give it a try</a>. You can also use one of the <a href="http://www.simpleinvoices.org/market_place">hosted versions</a> from $4 a month and up.</p>
<h2 id="invoice-ninja">Invoice Ninja</h2>
<p><a href="https://www.invoiceninja.com/">Invoice Ninja</a> melds a simple interface with a powerful set of features that lets you create, manage, and send invoices to clients and customers. You can easily configure multiple clients, track payments and outstanding invoices, generate quotes, and email invoices.</p>
<p>What sets Invoice Ninja part from its competitors is that it supports over 20 popular Internet payment processors, including PayPal and Google Wallet.</p>
<p><a href="https://github.com/hillelcoren/invoice-ninja">Download a version</a> that you can install on your own server, or <a href="https://www.invoiceninja.com/pricing/">get an account</a> with the hosted version of the tool. There’s a free version and one that will set you back $50 a year.</p>
<h2 id="siwapp">siwapp</h2>
<p>It’s not the slickest looking tool, but <a href="http://www.siwapp.org/">siwapp</a> compensates for that by making it easy to create professional-looking invoices. Just enter information into a few fields and, when you’re done, click a button to email the invoice.</p>
<p>siwapp also comes with a trio of modules that let you create and manage customer records, estimates, and products. On the other hand, out of the box siwapp doesn’t come with integration with a payment processor. If you’re a dab hand with PHP, you might be able to hack siwapp and include a payment link, as <a href="https://groups.google.com/forum/#!searchin/siwapp-users/payment/siwapp-users/mOiIMzeSdoo/fAX5XlCxphUJ">one user did for PayPal</a>.</p>
<p>You can <a href="http://demo.siwapp.org/">test drive</a> siwapp before you decide to install it.</p>
<h2 id="open-source-billing">Open Source Billing</h2>
<p>Described by its developer as "beautifully simple billing software," <a href="http://demo.opensourcebilling.org/">Open Source Billing</a> does live up to the description. It has one of the cleanest interfaces you’ve seen, which makes configuring and using the tool a breeze.</p>
<p>Open Source Billing stands out because of its dashboard, which tracks your current and past invoices, as well as any outstanding amounts. That information is broken up in to graphs and tables, which makes it easy to follow.</p>
<p>You do much of the configuration on the invoice for itself. You can add items, tax rates, clients, and even payment terms with a click and a few keystrokes. Open Source Billing saves that information across all of your invoices, both new and old.</p>
<p>As with some of the other tools we’ve looked at, Open Source Billing has a <a href="http://demo.opensourcebilling.org/">demo that you can try</a>.</p>
<hr /><p><strong>Share your favorite open source invoicing tool with us in the comments below.</strong></p>
