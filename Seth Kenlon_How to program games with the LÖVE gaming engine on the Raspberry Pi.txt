<p>The <a href="https://opensource.com/tags/raspberry-pi" target="_blank">Raspberry Pi</a> is famous for introducing kids to open source software and programming. The Pi is an affordable, practical introduction to professional-grade computing, disguised as hackable fun. An application that's done the most to get young children started in programming has been Mitch Resnick's <a href="https://opensource.com/tags/scratch" target="_blank">Scratch</a> (which fortunately was <a href="http://github.com/raspberrypi/scratch" target="_blank">forked by the Pi Foundation</a> when Scratch 2 switched to the non-open Adobe Air), but an inevitable question is what someone should graduate to after they've outgrown drag-and-drop programming.</p>
<p>After a drag-and-drop intro like Scratch, there are many candidates for the next level of programming. There's the excellent <a href="http://pygame.org" target="_blank">PyGame</a>, there's a Java subset called <a href="http://processing.org" target="_blank">Processing</a>, the powerful <a href="http://godotengine.org" target="_blank">Godot</a> engine, and many others. The trick is to find a framework that is easy enough to ease the transition from the instant gratification of drag-and-drop, but complex enough to accurately represent what professional programmers actually do all day.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Programming and development</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ&amp;src=programming_resource_menu4">Red Hat Developers Blog</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Programming cheat sheets</a></li>
<li><a href="https://www.redhat.com/en/services/training/learning-subscription?intcmp=7016000000127cYAAQ">Try for free: Red Hat Learning Subscription</a></li>
<li><a href="https://opensource.com/downloads/bash-programming-guide?intcmp=7016000000127cYAAQ">eBook: An introduction to programming with Bash</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/bash-shell-cheat-sheet?intcmp=7016000000127cYAAQ">Bash Shell Scripting Cheat Sheet</a></li>
<li><a href="https://developers.redhat.com/e-books/modernizing-enterprise-java?intcmp=7016000000127cYAAQ">eBook: Modernizing Enterprise Java</a></li>
</ul></div>
</div>
</div>
</div>

<p>A particularly robust game engine is called <a href="http://love2d.org" target="_blank">LÖVE</a>. LÖVE uses the scripting language <a href="http://lua.org" target="_blank">Lua</a>, which doesn't get as much attention as Python, but is <a href="https://developer.valvesoftware.com/wiki/Dota_2_Workshop_Tools/Lua_Abilities_and_Modifiers" target="_blank">heavily embedded</a> (both literally and figuratively) in the modern video game industry. Lua is listed as a necessary skill by almost all the major game studios, it's an option in the proprietary Unity and Unreal game engines, and it generally pops up in all kinds of unexpected places in the real world.</p>
<p>To make a long story short, Lua is a language worth learning, especially if you're planning on going into game development. As far as the LÖVE engine is concerned, it's about as good as the PyGame framework in terms of features, and because it has no IDE, it is very lightweight and, in a way, less complex to learn than something like Godot.</p>
<p>Better yet, LÖVE runs on the Pi natively, but its projects can be open and run on any platform that Lua does. That includes Linux, Windows, and Mac, but also Android and even the oh-so-closed iOS. In other words, LÖVE is not a bad platform to get started in mobile development, too.</p>
<p class="rtecenter"><img alt="Mr. Rescue, an open source game available on itch.io" class="media-image attr__typeof__foaf:Image img__fid__347171 img__view_mode__default attr__format__default attr__field_file_image_alt_text[und][0][value]__Mr. Rescue, an open source game available on itch.io attr__field_file_image_title_text[und][0][value]__Mr. attr__field_file_image_caption[und][0][value]__&#10;&lt;p&gt;opensource.com&lt;/p&gt;&#10;&lt;p&gt; attr__field_file_image_caption[und][0][format]__panopoly_wysiwyg_text attr__field_folder[und]__9402" src="https://opensource.com/%3Ca%20href%3D" https:="" />https://opensource.com/sites/default/files/love_rescue.png" title="Mr. Rescue, an open source game available on itch.io" typeof="foaf:Image" width="520" height="400"&gt;</p>
<p class="rtecenter"><sup>Mr. Rescue, an open source game available on itch.io</sup></p>
<p>Now that I've sold you on LÖVE as the next step up from Scratch on the Pi, let's dig in and see how it works.</p>
<h2>Installing LÖVE</h2>
<p>As usual, installing LÖVE is just one easy command on the Raspberry Pi:</p>
<pre><code>
$ sudo apt install love2d
</code></pre><p>If you're running <a href="https://opensource.com/article/17/3/how-install-fedora-on-raspberry-pi" target="_blank">Fedora on your Pi</a>, use <b>dnf</b> instead:</p>
<pre><code class="language-bash">
$ sudo dnf install love2d
</code></pre><p>Either way, the package management system pulls in Lua, SDL, and other dependencies that LÖVE needs to run.</p>
<h2>Hello, World!</h2>
<p>There's not much to see in the LÖVE engine. It's really a framework, meaning you can use whatever text editor you prefer. First thing to try is a "hello world" program, to make sure it launches, and to introduce you to the basic syntax of both the Lua language and the LÖVE framework. Open a text editor and enter this text:</p>
<pre><code class="language-ruby">
cwide = 520
chigh = 333

love.window.setTitle(' Hello Wörld ')
love.window.setMode(cwide, chigh)

function love.load()
	love.graphics.setBackgroundColor(177, 106, 248)
	-- font = love.graphics.setNewFont("SkirtGirl.ttf", 72)
end

function love.draw()
love.graphics.setColor(255, 33, 78)
	love.graphics.print("Hello World", cwide/4, chigh/3.33)
end
</code></pre><p>Save that as <b>main.lua</b>.</p>
<p>A distributable LÖVE package is just a standard zip file, with the <b>.love</b> extension. The main file, which must always be named <b>main.lua</b>, must be at the top level of the zip file. Create it like this:</p>
<pre><code>
$ zip hello.love main.lua
</code></pre><p>And launch it:</p>
<pre><code>
$ love ./hello.love
</code></pre><p class="rtecenter"><img alt="Hello World" class="media-image attr__typeof__foaf:Image img__fid__347176 img__view_mode__default attr__format__default attr__field_file_image_alt_text[und][0][value]__Hello World attr__field_file_image_title_text[und][0][value]__Hello attr__field_file_image_caption[und][0][value]__&#10;&lt;p&gt;opensource.com&lt;/p&gt;&#10;&lt;p&gt; attr__field_file_image_caption[und][0][format]__panopoly_wysiwyg_text attr__field_folder[und]__9402" src="https://opensource.com/%3Ca%20href%3D" https:="" />https://opensource.com/sites/default/files/love_hello.png" title="Hello World" typeof="foaf:Image" width="544" height="390"&gt;</p>
<p>The code is pretty easy to understand. The <b>cwide</b> and <b>chigh</b> variables are global, available to the whole script, and they set the width and height of the game world. In the first block of code (called a <b>function</b>), the background color is set, and in the second function the "hello world" gets printed to the screen.</p>
<p>The line preceded by two dashes (<b>--</b>) is a comment. Including a <b>.ttf</b> in the <b>.love</b> package and uncommenting the <b>setNewFont</b> line renders text in that font face. You don't have to use the Skirt Girl font, of course; just grab a font from <a href="https://fontlibrary.org" target="_blank">FontLibrary.org</a> and adjust the code accordingly.</p>
<pre><code class="language-bash">$ zip hello.love main.lua SkirtGirl.ttf
</code></pre><p>And launch it:</p>
<pre><code class="language-bash">
$ love ./hello.love
</code></pre><h2>The basics</h2>
<p>One big difference between Scratch and a professional programming language is that with Scratch, you can learn a few basic principles and then randomly explore until you've discovered all the other features.</p>
<p>With lower-level programming languages like Lua, the things you learn first aren't things you can copy and paste to produce a playable game. You learn how to use the language, and then you look up functions that accomplish actions that you want your game to feature. Until you learn how the language works, stumbling your way into a working game world is still difficult.</p>
<p>Luckily, you can learn the language as you build the start of a game. Along the way, you pick up tricks that you can reuse later to make your game work the way you want it to work.</p>
<p>To begin, you need to know the three main functions in the core LÖVE engine:</p>
<ul><li><b>function love.load()</b> is the function that gets triggered when you launch a LÖVE game (it's called the <b>init</b> or <b>void setup()</b> function in other languages). It's used to set up the groundwork for your game world. <b>function love.load()</b> only gets executed once, so it only contains code that persists throughout your game. It's, more or less, the equivalent of the <b>When Green Flag is Clicked</b> block and the <b>Stage</b> script area in Scratch.</li>
<li><b>function love.update(dt)</b> is the function that gets constantly updated during game play. Anything in a love.update(dt) function is refreshed as the game is played. If you've played any game, whether it's Five Nights at Freddy's or Skyrim or anything else, and you've monitored your frame rate (fps), everything happening as the frames tick would be in an update loop (not literally, because those games weren't made with LÖVE, but something like an update function).</li>
<li><b>function love.draw()</b> is a function that causes the engine to instantiate the graphical components that you created in the <b>love.load()</b> function of your game. You can load a sprite or create a mountain, but if you don't draw it, then it never shows up in your game.</li>
</ul><p>Those are the three functions that serve as the foundation for any element you create in LÖVE. You can create new functions of your own, too. First, let's explore the LÖVE framework.</p>
<h2>Sprites</h2>
<p>The basics of the "Hello, World!" is a good starting point. The same basic three functions still serve as the basic skeleton of the application. Unlike rendering simple text on the screen, though, this time we create a sprite using a graphic from <a href="http://freesvg.org" target="_blank">freesvg.org</a>.</p>
<p>The code examples and assets for this example are available in a <a href="https://notabug.org/seth/lessons_love2d" target="_blank">git repository</a>. Clone it for yourself if you want to follow along:</p>
<pre><code class="language-bash">
$ git clone https://notabug.org/seth/lessons_love2d.git
</code></pre><p>Most objects in LÖVE are stored in arrays. An array is a little like a list in Scratch; it's a bunch of characteristics placed into a common container. Generally, when you create a sprite in LÖVE, you create an array to hold attributes about the sprite, and then list the attributes in <b>love.load</b> that you want the object to have.</p>
<p>In the <b>love.draw</b> function, the sprite gets drawn to the screen.</p>
<pre><code class="language-ruby">
fish  = {}
cwide = 520
chigh = 333
    
love.window.setMode(cwide, chigh)
love.window.setTitle(' Collide ')
    
function love.load()
	fish.x    = 0
	fish.y    = 0
	fish.img  = love.graphics.newImage('images/fish.png')
end
    
function love.update(dt)
        -- this is a comment
end
    
function love.draw()
    	love.graphics.draw(fish.img, fish.x, fish.y, 0, 1, 1, 0, 0)
end
</code></pre><p>The fish's natural predator is one of the most vicious creatures of the antarctic: the penguin. Create a penguin sprite in the same way as the fish sprite was created, with the addition of a <b>player.speed</b>, which is how many pixels the penguin will move once we get around to setting up player controls:</p>
<pre><code class="language-ruby">
fish  = {}
player= {}
cwide = 520
chigh = 333
    
love.window.setMode(cwide, chigh)
love.window.setTitle(' Collide ')
    
function love.load()
  fish.x    = 0
  fish.y    = 0
  fish.img  = love.graphics.newImage('images/fish.png')
  player.x     = 100
  player.y     = 100
  player.img   = love.graphics.newImage('images/tux.png')
  player.speed = 10
end
    
function love.update(dt)
        -- this is a comment
end
    
function love.draw()
    	love.graphics.draw(fish.img,  fish.x,  fish.y,  0,1,1,0, 0)
        love.graphics.draw(player.img,player.x,player.y,0,1,1,0, 0)
end
</code></pre><p>To keep things tidy, place the <b>png</b> files in an <b>images</b> directory. To test the game as it is so far, zip up the <b>main.lua</b> and <b>png</b> files:</p>
<pre><code class="language-bash">
$ zip game.zip main.lua -r images
$ mv game.zip game.love
$ love ./game.love
</code></pre><p class="rtecenter"><img alt="Our current cast of characters." class="media-image attr__typeof__foaf:Image img__fid__347181 img__view_mode__default attr__format__default attr__field_file_image_alt_text[und][0][value]__Our current cast of characters. attr__field_file_image_title_text[und][0][value]__Our attr__field_file_image_caption[und][0][value]__&#10;&lt;p&gt;opensource.com&lt;/p&gt;&#10;&lt;p&gt; attr__field_file_image_caption[und][0][format]__panopoly_wysiwyg_text attr__field_folder[und]__9402" src="https://opensource.com/%3Ca%20href%3D" https:="" />https://opensource.com/sites/default/files/love_sprites.png" title="Our current cast of characters." typeof="foaf:Image" width="375" height="256"&gt;</p>
<p class="rtecenter"><sup>Our current cast of characters</sup></p>
<h2>Movement</h2>
<p>There are several ways to implement movement in LÖVE, including functions for mouse, joystick, and keyboard. We've already established variables for the player's <b>x</b> and <b>y</b> positions, and for how many pixels the player moves, so use an if/then statement to detect key presses, and then redefine the variable holding the position of the player sprite using simple maths:</p>
<pre><code class="language-ruby">
player     = {}
fish       = {}
cwide      = 520
chigh      = 333
    
love.window.setMode(cwide, chigh)
love.window.setTitle(' Collide ')

function love.load()
	fish.x    = 0
	fish.y    = 0
	fish.img  = love.graphics.newImage( 'images/fish.png' )
	player.x     = 100
	player.y     = 100
	player.img   = love.graphics.newImage('images/tux.png')
	player.speed = 10
end

function love.update(dt)
	if love.keyboard.isDown("right") then
	    player.x = player.x+player.speed

	elseif love.keyboard.isDown("left") then
	    player.x = player.x-player.speed

	elseif love.keyboard.isDown("up")  then
	    player.y = player.y-player.speed    

	elseif love.keyboard.isDown("down") then
	    player.y = player.y+player.speed
        end
end

function love.draw()
	love.graphics.draw(player.img,player.x,player.y,0,1,1,0, 0)
	love.graphics.draw(fish.img, fish.x, fish.y, 0, 1, 1, 0, 0)    
end
</code></pre><p>Test the code to confirm that movement works as expected. Remember to re-zip all the files before testing so that you don't accidentally test the old version of the game.</p>
<p>To make the movement make a little more sense, we can create functions to change the direction a sprite is facing depending on what key is pressed. This is the equivalent of these kinds of Scratch code blocks:</p>
<p class="rtecenter"><img alt="Looking and moving in Scratch" class="media-image attr__typeof__foaf:Image img__fid__347186 img__view_mode__default attr__format__default attr__field_file_image_alt_text[und][0][value]__Looking and moving in Scratch attr__field_file_image_title_text[und][0][value]__Looking attr__field_file_image_caption[und][0][value]__&#10;&lt;p&gt;opensource.com&lt;/p&gt;&#10;&lt;p&gt; attr__field_file_image_caption[und][0][format]__panopoly_wysiwyg_text attr__field_folder[und]__9402" src="https://opensource.com/%3Ca%20href%3D" https:="" />https://opensource.com/sites/default/files/scratch-right.png" title="Looking and moving in Scratch" typeof="foaf:Image" width="314" height="192"&gt;</p>
<p class="rtecenter"><sup>Looking and moving in Scratch</sup></p>
<p>Generate a flipped version of the player sprite using ImageMagick:</p>
<pre><code class="language-bash">
$ convert tux.png -flop tuxleft.png
</code></pre><p>And then write a function to swap out the image. You need two functions: one to swap out the image, and another to restore it to the original:</p>
<pre><code class="language-ruby">
function rotate_left()
	player.img = love.graphics.newImage('images/tuxleft.png')
end

function rotate_right()
	player.img = love.graphics.newImage('images/tux.png' )
end
</code></pre><p>Use these functions where appropriate:</p>
<pre><code class="language-ruby">
function love.update(dt)
	if love.keyboard.isDown("right") then
	    player.x = player.x+player.speed
            rotate_right()

	elseif love.keyboard.isDown("left") then
	    player.x = player.x-player.speed
            rotate_left()
	    
	elseif love.keyboard.isDown("up")  then
	    player.y = player.y-player.speed    

	elseif love.keyboard.isDown("down") then
	    player.y = player.y+player.speed
        end
end
</code></pre><h2>Automated movement</h2>
<p>Using the same convention of the sprite's <b>x</b> value, and a little mathematics, you can make the fish automatically move across the screen from edge to edge. This is the equivalent of doing this in Scratch:</p>
<p class="rtecenter"><img alt="Automated movement in Scratch" class="media-image attr__typeof__foaf:Image img__fid__347191 img__view_mode__default attr__format__default attr__field_file_image_alt_text[und][0][value]__Automated movement in Scratch attr__field_file_image_title_text[und][0][value]__Automated attr__field_file_image_caption[und][0][value]__&#10;&lt;p&gt;opensource.com&lt;/p&gt;&#10;&lt;p&gt; attr__field_file_image_caption[und][0][format]__panopoly_wysiwyg_text attr__field_folder[und]__9402" src="https://opensource.com/%3Ca%20href%3D" https:="" />https://opensource.com/sites/default/files/scratch-edge.png" title="Automated movement in Scratch" typeof="foaf:Image" width="314" height="192"&gt;</p>
<p class="rtecenter"><sup>Automated movement in Scratch</sup></p>
<p>There is no <b>if on edge, bounce</b> in LÖVE, but by checking whether the fish's <b>x</b> value has reached the far left of the screen, which is <b>0</b> pixels, or the far right, which is the same as the width of the canvas (the <b>cwide</b> variable), you can determine whether the sprite has gone off the edge or not.</p>
<p>If the fish is at the far left, then it has reached the edge of the screen, so you increment the fish's position forcing it to move right. If the fish is at the far right, then decrement the fish's position, making it move left.</p>
<pre><code class="language-ruby">
function automove(obj,x,y,ox,oy)
        if obj.x == cwide then
	    local edgeright = 0
	elseif obj.x == 0 then
	    local edgeright = 1
end

if edgeright == 1 then
	    obj.x = obj.x + x
	else
	    obj.x = obj.x - x
	end
    end
</code></pre><p>There's a sequence of events that happens to be important in this case. In the first <b>if</b> block, you check for the value of <b>x</b> and assign a temporary (local) variable to indicate where the fish needs to go next. Then in a second, separate <b>if</b> block, you move the fish. For bonus points, try doing it all in one <b>if</b> statement and see whether you can understand why it fails. For more bonus points, see whether you can figure out a different way of moving the fish.</p>
<p>To implement the fish's movement function, call the function at the bottom of the <b>love.update</b> loop:</p>
<pre><code>
automove(fish,1,0,fish.img:getWidth(),fish.img:getHeight() )
</code></pre><p>If you test the script, you'll notice that the fish goes all the way off the screen when it hits the right edge. It's doing this because a sprite's <b>x</b> value is based on its upper left pixel. I'll leave it as an exercise for you to figure out what variable you should subtract from <b>cwide</b> when checking for the fish's position.</p>
<h2>Collision detection</h2>
<p>Video games are all about collisions. It's when things bump into each other, whether those things are an unfortunate hero taking a dive into a lava pit, or a bad guy getting blasted with a mana bolt, stuff is supposed to happen.</p>
<p>Before detecting a collision, let's decide what we want to have happen when a collision occurs. Because you already know how to change a sprite's appearance, we'll create two functions: one to change the fish into the bones left over after the penguin has caught it, and the other to change the fish back to life any time the penguin isn't around.</p>
<pre><code class="language-ruby">
function falive()
        fish.img = love.graphics.newImage('images/fish.png')
end

function fdead()
        fish.img = love.graphics.newImage('images/fishbones.png')
end
</code></pre><p>With these functions set up, it's time to calculate collisions.</p>
<p>In Scratch, there are code blocks to check whether two sprites are touching.</p>
<p class="rtecenter"><img alt="Scratch collision" class="media-image attr__typeof__foaf:Image img__fid__347196 img__view_mode__default attr__format__default attr__field_file_image_alt_text[und][0][value]__Scratch collision attr__field_file_image_title_text[und][0][value]__Scratch attr__field_file_image_caption[und][0][value]__&#10;&lt;p&gt;opensource.com&lt;/p&gt;&#10;&lt;p&gt; attr__field_file_image_caption[und][0][format]__panopoly_wysiwyg_text attr__field_folder[und]__9402" src="https://opensource.com/%3Ca%20href%3D" https:="" />https://opensource.com/sites/default/files/scratch-collision.png" title="Scratch collision" typeof="foaf:Image" width="338" height="148"&gt;</p>
<p class="rtecenter"><sup>Scratch collision</sup></p>
<p>The same concept, in principle, applies in LÖVE. There are several different ways to detect collisions, including external libraries like <a href="https://github.com/vrld/HC" target="_blank">HC</a> and <b>love.physics</b>, but a good compromise between the two is a custom function to detect an overlap in sprite boundaries.</p>
<pre><code class="language-ruby">
function CheckCollision(x1,y1,w1,h1, x2,y2,w2,h2)
        return x1 &lt; x2+w2 and
	    x2 &lt; x1+w1 and
	    y1 &lt; y2+h2 and
	    y2 &lt; y1+h1
end
</code></pre><p>The math is complex, but it makes sense if you take a moment to think about it. The goal is detect whether two images are overlapping. If the images are overlapping, they can be said to have collided. Here's an illustration of two boxes <i>not</i>* overlapping, with sample <b>y</b> values to keep things simple:</p>
<p class="rtecenter"><img alt="Two boxes" class="media-image attr__typeof__foaf:Image img__fid__347201 img__view_mode__default attr__format__default attr__field_file_image_alt_text[und][0][value]__Two boxes attr__field_file_image_title_text[und][0][value]__Two attr__field_file_image_caption[und][0][value]__&#10;&lt;p&gt;opensource.com&lt;/p&gt;&#10;&lt;p&gt; attr__field_file_image_caption[und][0][format]__panopoly_wysiwyg_text attr__field_folder[und]__9402" src="https://opensource.com/%3Ca%20href%3D" https:="" />https://opensource.com/sites/default/files/overlap_no.png" title="Two boxes" typeof="foaf:Image" width="507" height="649"&gt;</p>
<p class="rtecenter"><sup>Two boxes</sup></p>
<p>Take a line from the function and crunch the numbers:</p>
<pre><code class="language-ruby">
y2  &lt; y1 + h1
110 &lt; 0  + 100
</code></pre><p>That's obviously a false statement, so the function must return <b>false</b>. In other words, the boxes have not collided.</p>
<p>Now look at the same logic with two overlapping boxes:</p>
<p class="rtecenter"><img alt="Overlapping boxes" class="media-image attr__typeof__foaf:Image img__fid__347206 img__view_mode__default attr__format__default attr__field_file_image_alt_text[und][0][value]__Overlapping boxes attr__field_file_image_title_text[und][0][value]__Overlapping attr__field_file_image_caption[und][0][value]__&#10;&lt;p&gt;opensource.com&lt;/p&gt;&#10;&lt;p&gt; attr__field_file_image_caption[und][0][format]__panopoly_wysiwyg_text attr__field_folder[und]__9402" src="https://opensource.com/%3Ca%20href%3D" https:="" />https://opensource.com/sites/default/files/overlap.png" title="Overlapping boxes" typeof="foaf:Image" width="507" height="460"&gt;</p>
<pre><code class="language-ruby">
y2  &lt; y1 + h1
50  &lt; 0  + 100
</code></pre><p>This one is clearly true. Assuming all statements in the function are also true (and they would be, had I bothered adding <b>x</b> values), then the function returns <b>true</b>.</p>
<p>To leverage the collision check, evaluate it with an <b>if</b> statement. You know now that if the <b>CheckCollision</b> function returns <b>true</b>, then there is a collision. The <b>CheckCollision</b> function has been written generically, so when calling it, you need to feed it the appropriate values so it knows which object is which.</p>
<p>Most of the values are intuitive. You need LÖVE to use the <b>x</b> and <b>y</b> positions of each object being checked for collision, and the size of the object. The only values that are special in this case is the one that gets swapped out depending on the collision state. For those, hard code the size of the dead fish rather than the live fish, or else the state of the collision will get changed in the middle of the collision detection. In fact, if you want to see it glitch, you can do it the wrong way first:</p>
<pre><code class="language-ruby">
if CheckCollision(fish.x,fish.y,fish.img:getWidth(),fish.img:getHeight(), player.x,player.y,player.img:getWidth(),player.img:getHeight() ) then
        fdead()
    else
	falive()
    end
</code></pre><p>The right way is to get the size of the smaller sprite image. You can do this with ImageMagick:</p>
<pre><code class="language-bash">
$ identify images/fishbones.png
images/fishbones.png PNG 150x61 [...]
</code></pre><p>Then hard code the "hot spot" with the appropriate dimensions:</p>
<pre><code class="language-ruby">
if CheckCollision(fish.x,fish.y,150,61, player.x,player.y,player.img:getWidth(),player.img:getHeight() ) then
        fdead()
    else
	falive()
end
</code></pre><p>The side effect of hard coding the collision detection area is that when the penguin touches the edge of the fish, the fish doesn't get gobbled up. For more precise collision detection, explore the <a href="https://github.com/vrld/HC" target="_blank">HC</a> library or <b>love.physics</b>.</p>
<h2>Final code</h2>
<p>It's not much of a game, but it demonstrates the important elements of video games:</p>
<pre><code class="language-ruby">
player     = {}
fish       = {}
cwide      = 520
chigh      = 333

love.window.setTitle(' Hello Game Wörld ')
love.window.setMode(cwide, chigh)

function love.load()
	fish.x    = 0
	fish.y    = 0
	fish.img  = love.graphics.newImage( 'images/fish.png' )
	player.x     = 150
	player.y     = 150
	player.img   = love.graphics.newImage('images/tux.png')
	player.speed = 10
    end

function love.update(dt)
	if love.keyboard.isDown("right") then
	    player.x = player.x+player.speed

	elseif love.keyboard.isDown("left") then
	    player.x = player.x-player.speed

	elseif love.keyboard.isDown("up")  then
	    player.y = player.y-player.speed    

	elseif love.keyboard.isDown("down") then
	    player.y = player.y+player.speed
        end

if CheckCollision(fish.x,fish.y,151,61, player.x,player.y,player.img:getWidth(),player.img:getHeight() ) then
	    fdead()
	else
	    falive()
end

automove(fish,1,0,fish.wide,fish.high)
end

function love.draw()
	love.graphics.draw(player.img,player.x,player.y,0,1,1,0, 0)
	love.graphics.draw(fish.img, fish.x, fish.y, 0, 1, 1, 0, 0)    
end

function automove(obj,x,y,ox,oy)
	if obj.x == cwide-fish.img:getWidth() then
	    edgeright = 0
	    elseif obj.x == 0 then
	    edgeright = 1
end

if edgeright == 1 then
	    obj.x = obj.x + x
	else
	    obj.x = obj.x - x
	end
   end

function CheckCollision(x1,y1,w1,h1, x2,y2,w2,h2)
	return x1 &lt; x2+w2 and
	    x2 &lt; x1+w1 and
	    y1 &lt; y2+h2 and
	    y2 &lt; y1+h1
	end

function rotate_left()
	player.img = love.graphics.newImage('images/tuxleft.png')
    end

function rotate_right()
	player.img = love.graphics.newImage('images/tux.png' )
    end

function falive()
	fish.img = love.graphics.newImage('images/fish.png')
    end

function fdead()
	fish.img = love.graphics.newImage('images/fishbones.png')
    end
</code></pre><p>From here you can use the principles you've learned to create more exciting work. Collisions are the basis for most interactions in video games, whether it's to trigger a conversation with an NPC, manage combat, pick up items, set off traps, or almost anything else, so if you master that, the rest is repetition and elbow grease.</p>
<p>So go and make a video game! Share it with friends, play it on mobile, and, as always, keep leveling up.</p>
