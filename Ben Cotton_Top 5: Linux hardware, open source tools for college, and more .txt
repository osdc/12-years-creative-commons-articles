<p>In this week's Top 5, we highlight education, Linux-friendly hardware, and more.</p>
<p></p><center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/TMzit64QtDM" frameborder="0" allowfullscreen=""></iframe><p></p></center>
<h2>Top 5 articles of the week</h2>
<p><strong>5. <a href="https://opensource.com/article/17/6/kde-education-software">5 great KDE apps to help you study</a> </strong></p>
<p>Zsolt Szakács shares tools for people who want to learn new skills or practice existing ones. With typing, geography, music, and more, there's something in here for everyone.</p>
<p><strong>4. <a href="https://opensource.com/article/17/6/gimp-10-ways">10 ways the GIMP image editor changed my life</a></strong></p>
<p>The GIMP is old enough to drink in Canada, so to celebrate Carl Hermansen shares 10 ways the iconic software has changed his life. He's used it to create logos, DJ press kits, retail packaging, and more all while developing his professional skills and becoming a part of the community.</p>
<p><strong>3. <a href="https://opensource.com/article/17/6/3-alternatives-libreoffice-writer">3 alternatives to LibreOffice Writer</a></strong></p>
<p>LibreOffice Writer is the most widely-used word processor, but there are other choices out there. Community Moderator Scott Nesbitt takes a look at three of them, and several more are mentioned in the comments.</p>
<p><strong>2. <a href="https://opensource.com/article/17/6/open-source-tools-university-student">A list of open source tools for college</a></strong></p>
<p>Proprietary software has a strong foothold in higher ed, but it doesn't have to. Aaron Cocker shares some open source tools he uses as he pursues a bachelor's degree in Computing.</p>
<p><strong>1. <a href="https://opensource.com/article/17/6/oem-linux-hardware">3 off-the-shelf Linux computers compared</a></strong></p>
<p>Jim Salter takes a look at computers from three different manufacturers that ship with Linux off-the-shelf. It's a thorough review with lots of pictures, so you'll want to take a look.</p>
