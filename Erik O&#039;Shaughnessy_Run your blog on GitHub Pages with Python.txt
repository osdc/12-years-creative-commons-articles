<p><a href="https://github.com/" target="_blank">GitHub</a> is a hugely popular web service for source code control that uses <a href="https://git-scm.com" target="_blank">Git</a> to synchronize local files with copies kept on GitHub's servers so you can easily share and back up your work.</p>
<p>In addition to providing a user interface for code repositories, GitHub also enables users to <a href="https://help.github.com/en/categories/github-pages-basics" target="_blank">publish web pages</a> directly from a repository. The website generation package GitHub recommends is <a href="https://jekyllrb.com" target="_blank">Jekyll</a>, written in Ruby. Since I'm a bigger fan of <a href="https://python.org" target="_blank">Python</a>, I prefer <a href="https://blog.getpelican.com" target="_blank">Pelican</a>, a Python-based blogging platform that works well with GitHub.</p>
<p>Pelican and Jekyll both transform content written in <a href="https://guides.github.com/features/mastering-markdown" target="_blank">Markdown</a> or <a href="http://docutils.sourceforge.net/docs/user/rst/quickref.html" target="_blank">reStructuredText</a> into HTML to generate static websites, and both generators support themes that allow unlimited customization.</p>
<p>In this article, I'll describe how to install Pelican, set up your GitHub repository, run a quickstart helper, write some Markdown files, and publish your first page. I'll assume that you have a <a href="https://github.com/join?source=header-home" target="_blank">GitHub account</a>, are comfortable with <a href="https://git-scm.com/docs" target="_blank">basic Git commands</a>, and want to publish a blog using Pelican.</p>
<h2 id="install-pelican-and-create-the-repo">Install Pelican and create the repo</h2>
<p>First things first, Pelican (and <strong>ghp-import</strong>) must be installed on your local machine. This is super easy with <a href="https://pip.pypa.io/en/stable/" target="_blank">pip</a>, the Python package installation tool (you have pip right?):</p>
<pre><code class="language-text">$ pip install pelican ghp-import Markdown</code></pre><p>Next, open a browser and create a new repository on GitHub for your sweet new blog. Name it as follows (substituting your GitHub username for &lt;username&gt; here and throughout this tutorial):</p>
<pre><code class="language-text">https://GitHub.com/username/username.github.io</code></pre><p>Leave it empty; we will fill it with compelling blog content in a moment.</p>
<p>Using a command line (you command line right?), clone your empty Git repository to your local machine:</p>
<pre><code class="language-bash">$ git clone https://GitHub.com/username/username.github.io blog
$ cd blog</code></pre><h2 id="that-one-weird-trick...">That one weird trick…</h2>
<p>Here's a not-super-obvious trick about publishing web content on GitHub. For user pages (pages hosted in repos named <em>username.github.io</em>), the content is served from the <strong>master</strong> branch.</p>
<p>I strongly prefer not to keep all the Pelican configuration files and raw Markdown files in <strong>master</strong>, rather just the web content. So I keep the Pelican configuration and the raw content in a separate branch I like to call <strong>content</strong>. (You can call it whatever you want, but the following instructions will call it <strong>content</strong>.) I like this structure since I can throw away all the files in <strong>master</strong> and re-populate it with the <strong>content</strong> branch.</p>
<pre><code class="language-bash">$ git checkout -b content
Switched to a new branch 'content'</code></pre><h2 id="configure-pelican">Configure Pelican</h2>
<p>Now it's time for content configuration. Pelican provides a great initialization tool called <strong>pelican-quickstart</strong> that will ask you a series of questions about your blog.</p>
<pre><code class="language-bash">$ pelican-quickstart
Welcome to pelican-quickstart v3.7.1.

This script will help you create a new Pelican-based website.

Please answer the following questions so this script can generate the files
needed by Pelican.

&gt; Where do you want to create your new web site? [.]  
&gt; What will be the title of this web site? Super blog
&gt; Who will be the author of this web site? username
&gt; What will be the default language of this web site? [en] 
&gt; Do you want to specify a URL prefix? e.g., http://example.com   (Y/n) n
&gt; Do you want to enable article pagination? (Y/n) 
&gt; How many articles per page do you want? [10] 
&gt; What is your time zone? [Europe/Paris] US/Central
&gt; Do you want to generate a Fabfile/Makefile to automate generation and publishing? (Y/n) y
&gt; Do you want an auto-reload &amp; simpleHTTP script to assist with theme and site development? (Y/n) y
&gt; Do you want to upload your website using FTP? (y/N) n
&gt; Do you want to upload your website using SSH? (y/N) n
&gt; Do you want to upload your website using Dropbox? (y/N) n
&gt; Do you want to upload your website using S3? (y/N) n
&gt; Do you want to upload your website using Rackspace Cloud Files? (y/N) n
&gt; Do you want to upload your website using GitHub Pages? (y/N) y
&gt; Is this your personal page (username.github.io)? (y/N) y
Done. Your new project is available at /Users/username/blog</code></pre><p>You can take the defaults on every question except:</p>
<ul><li>Website title, which should be unique and special</li>
<li>Website author, which can be a personal username or your full name</li>
<li>Time zone, which may not be in Paris</li>
<li>Upload to GitHub Pages, which is a "y" in our case</li>
</ul><p>After answering all the questions, Pelican leaves the following in the current directory:</p>
<pre><code class="language-bash">$ ls
Makefile		content/	develop_server.sh*
fabfile.py		output/		pelicanconf.py
publishconf.py</code></pre><p>You can check out the <a href="https://docs.getpelican.com" target="_blank">Pelican docs</a> to find out how to use those files, but we're all about getting things done <em>right now</em>. No, I haven't read the docs yet either.</p>
<h2 id="forge-on">Forge on</h2>
<p>Add all the Pelican-generated files to the <strong>content</strong> branch of the local Git repo, commit the changes, and push the local changes to the remote repo hosted on GitHub by entering:</p>
<pre><code class="language-bash">$ git add .
$ git commit -m 'initial pelican commit to content'
$ git push origin content</code></pre><p>This isn't super exciting, but it will be handy if we need to revert edits to one of these files.</p>
<h2 id="finally-getting-somewhere">Finally getting somewhere</h2>
<p>OK, now you can get bloggy! All of your blog posts, photos, images, PDFs, etc., will live in the <strong>content</strong> directory, which is initially empty. To begin creating a first post and an About page with a photo, enter:</p>
<pre><code class="language-bash">$ cd content
$ mkdir pages images
$ cp /Users/username/SecretStash/HotPhotoOfMe.jpg images
$ touch first-post.md
$ touch pages/about.md
</code></pre><p>Next, open the empty file <strong>first-post.md</strong> in your favorite text editor and add the following:</p>
<pre><code class="language-text">title: First Post on My Sweet New Blog
date: &lt;today's date&gt;
author: Your Name Here

# I am On My Way To Internet Fame and Fortune!

This is my first post on my new blog. While not super informative it
should convey my sense of excitement and eagerness to engage with you,
the reader!</code></pre><p>The first three lines contain metadata that Pelican uses to organize things. There are lots of different metadata you can put there; again, the docs are your best bet for learning more about the options.</p>
<p>Now, open the empty file <strong>pages/about.md</strong> and add this text:</p>
<pre><code class="language-text">title: About
date: &lt;today's date&gt;

![So Schmexy][my_sweet_photo]

Hi, I am &lt;username&gt; and I wrote this epic collection of Interweb
wisdom. In days of yore, much of this would have been deemed sorcery
and I would probably have been burned at the stake.

?

[my_sweet_photo]: {static}/images/HotPhotoOfMe.jpg</code></pre><p>You now have three new pieces of web content in your content directory. Of the content branch. That's a lot of content.</p>
<h2 id="publish">Publish</h2>
<p>Don't worry; the payoff is coming!</p>
<p>All that's left to do is:</p>
<ul><li>Run Pelican to generate the static HTML files in <strong>output</strong>:<br /><pre><code class="language-bash">$ pelican content -o output -s publishconf.py</code></pre></li>
<li>Use <strong>ghp-import</strong> to add the contents of the <strong>output</strong> directory to the <strong>master</strong> branch:<br /><pre><code class="language-bash">$ ghp-import -m "Generate Pelican site" --no-jekyll -b master output</code></pre></li>
<li>Push the local master branch to the remote repo:<br /><pre><code class="language-bash">$ git push origin master</code></pre></li>
<li>Commit and push the new content to the <strong>content</strong> branch:<br /><pre><code class="language-bash">$ git add content
$ git commit -m 'added a first post, a photo and an about page'
$ git push origin content</code></pre></li>
</ul><h2 id="omg-i-did-it">OMG, I did it!</h2>
<p>Now the exciting part is here when you get to view what you've published for everyone to see! Open your browser and enter:</p>
<pre><code class="language-text">https://username.github.io</code></pre><p>Congratulations on your new blog, self-published on GitHub! You can follow this pattern whenever you want to add more pages or articles. Happy blogging.</p>
