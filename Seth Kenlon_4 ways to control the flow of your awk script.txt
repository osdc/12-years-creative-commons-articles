<p>There are many ways to control the flow of an awk script, including <a href="https://opensource.com/article/19/11/loops-awk">loops</a>, <strong>switch</strong> statements and the <strong>break</strong>, <strong>continue</strong>, and <strong>next</strong> commands.</p>
<h2 id="sample-data">Sample data</h2>
<p>Create a sample data set called <strong>colours.txt</strong> and copy this content into it:</p>
<pre><code class="language-text">name       color  amount
apple      red    4
banana     yellow 6
strawberry red    3
raspberry  red    99
grape      purple 10
apple      green  8
plum       purple 2
kiwi       brown  4
potato     brown  9
pineapple  yellow 5</code></pre><h2 id="switch-statements">Switch statements</h2>
<p>The <strong>switch</strong> statement is a feature specific to GNU awk, so you can only use it with <strong>gawk</strong>. If your system or your target system doesn't have <strong>gawk</strong>, then you should not use a switch statement.</p>
<p>The <strong>switch</strong> statement in <strong>gawk</strong> is similar to the one in C and many other languages. The syntax is:</p>
<pre><code class="language-bash">switch (expression) {
	case VALUE:
		&lt;do something here&gt;
	[...]
	default:
		&lt;do something here&gt;
}</code></pre><p>The <strong>expression</strong> part can be any awk expression that returns a numeric or string result. The <strong>VALUE</strong> part (after the word <strong>case</strong>) is a numeric or string constant or a regular expression.</p>
<p>When a <strong>switch</strong> statement runs, the <em>expression</em> is evaluated, and the result is matched against each case value. If there's a match, then the code contained within a case definition is executed. If there's no match in any case definition, then the default statement is executed.</p>
<p>The keyword <strong>break</strong> is at the end of the code in each case definition to break the loop. Without <strong>break</strong>, awk would continue to search for matching case values.</p>
<p>Here's an example <strong>switch</strong> statement:</p>
<pre><code class="language-bash">#!/usr/bin/awk -f
#
# Example of the use of 'switch' in GNU Awk.

NR &gt; 1 {
    printf "The %s is classified as: ",$1

    switch ($1) {
        case "apple":
            print "a fruit, pome"
            break
        case "banana":
        case "grape":
        case "kiwi":
            print "a fruit, berry"
            break
		case "raspberry":
			print "a computer, pi"
			break
        case "plum":
            print "a fruit, drupe"
            break
        case "pineapple":
            print "a fruit, fused berries (syncarp)"
            break
        case "potato":
            print "a vegetable, tuber"
            break
        default:
            print "[unclassified]"
    }
}</code></pre><p>This script notably ignores the first line of the file, which in the case of the sample data is just a header. It does this by operating only on records with an index number greater than 1. On all other records, this script compares the contents of the first field (<strong>$1</strong>, as you know from previous articles) to the value of each <strong>case</strong> definition. If there's a match, the <strong>print</strong> function is used to print the botanical classification of the entry. If there are no matches, then the <strong>default</strong> instance prints <strong>"[unclassified]"</strong>.</p>
<p>The banana, grape, and kiwi are all botanically classified as a berry, so there are three <strong>case</strong> definitions associated with one <strong>print</strong> result.</p>
<p>Run the script on the <strong>colours.txt</strong> sample file, and you should get this:</p>
<pre><code class="language-text">The apple is classified as: a fruit, pome
The banana is classified as: a fruit, berry
The strawberry is classified as: [unclassified]
The raspberry is classified as: a computer, pi
The grape is classified as: a fruit, berry
The apple is classified as: a fruit, pome
The plum is classified as: a fruit, drupe
The kiwi is classified as: a fruit, berry
The potato is classified as: a vegetable, tuber
The pineapple is classified as: a fruit, fused berries (syncarp)</code></pre><h2 id="break">Break</h2>
<p>The <strong>break</strong> statement is mainly used for the early termination of a <strong>for</strong>, <strong>while</strong>, or <strong>do-while</strong> loop or a <strong>switch</strong> statement. In a loop, <strong>break</strong> is often used where it's not possible to determine the number of iterations of the loop beforehand. Invoking <strong>break</strong> terminates the enclosing loop (which is relevant when there are nested loops or loops within loops).</p>
<p>This example, straight out of the <a href="https://www.gnu.org/software/gawk/manual/" target="_blank">GNU awk manual</a>, shows a method of finding the smallest divisor. Read the additional comments for a clear understanding of how the code works:</p>
<pre><code class="language-bash">#!/usr/bin/awk -f

{
    num = $1

    # Make an infinite FOR loop
    for (divisor = 2; ; divisor++) {

        # If num is divisible by divisor, then break
        if (num % divisor == 0) {
            printf "Smallest divisor of %d is %d\n", num, divisor
            break
        }

        # If divisor has gotten too large, the number has no
        # divisor, so is a prime 
        if (divisor * divisor &gt; num) {
            printf "%d is prime\n", num
            break
        }
    }
}</code></pre><p>Try running the script to see its results:</p>
<pre><code class="language-bash">    $ echo 67 | ./divisor.awk
    67 is prime
    $ echo 69 | ./divisor.awk
    Smallest divisor of 69 is 3</code></pre><p>As you can see, even though the script starts out with an explicit <em>infinite</em> loop with no end condition, the <strong>break</strong> function ensures that the script eventually terminates.</p>
<h2 id="continue">Continue</h2>
<p>The <strong>continue</strong> function is similar to <strong>break</strong>. It can be used in a <strong>for</strong>, <strong>while</strong>, or <strong>do-while</strong> loop (it's not relevant to a <strong>switch</strong> statements, though). Invoking <strong>continue</strong> skips the rest of the enclosing loop and begins the next cycle.</p>
<p>Here's another good example from the GNU awk manual to demonstrate a possible use of <strong>continue</strong>:</p>
<pre><code class="language-bash">#!/usr/bin/awk -f

# Loop, printing numbers 0-20, except 5

BEGIN {
    for (x = 0; x &lt;= 20; x++) {
        if (x == 5)
            continue
        printf "%d ", x
    }
    print ""
}</code></pre><p>This script analyzes the value of <strong>x</strong> before printing anything. If the value is exactly 5, then <strong>continue</strong> is invoked, causing the <strong>printf</strong> line to be skipped, but leaves the loop unbroken. Try the same code but with <strong>break</strong> instead to see the difference.</p>
<h2 id="next">Next</h2>
<p>This statement is not related to loops like <strong>break</strong> and <strong>continue</strong> are. Instead, <strong>next</strong> applies to the main record processing cycle of awk: the functions you place between the BEGIN and END functions. The <strong>next</strong> statement causes awk to stop processing the <em>current input record</em> and to move to the next one.</p>
<p>As you know from the earlier articles in this series, awk reads records from its input stream and applies rules to them. The <strong>next</strong> statement stops the execution of rules for the current record and moves to the next one.</p>
<p>Here's an example of <strong>next</strong> being used to "hold" information upon a specific condition:</p>
<pre><code class="language-bash">#!/usr/bin/awk -f

# Ignore the header
NR == 1 { next }

# If field 2 (colour) is less than 6
# characters, then save it with its
#  line number and skip it

length($2) &lt; 6 {
    skip[NR] = $0
    next
}

# It's not the header and
# the colour name is &gt; 6 characters, 
# so print the line
{
    print
}

# At the end, show what was skipped
END {
    printf "\nSkipped:\n"
    for (n in skip)
        print n": "skip[n]
}</code></pre><p>This sample uses <strong>next</strong> in the first rule to avoid the first line of the file, which is a header row. The second rule skips lines when the color name is less than six characters long, but it also saves that line in an array called <strong>skip</strong>, using the line number as the key (also known as the <em>index</em>).</p>
<p>The third rule prints anything it sees, but it is not invoked if either rule 1 or rule 2 causes it to be skipped.</p>
<p>Finally, at the end of all the processing, the <strong>END</strong> rule prints the contents of the array.</p>
<p>Run the sample script on the <strong>colours.txt</strong> file from above (and previous articles):</p>
<pre><code class="language-bash">$ ./next.awk colours.txt
banana     yellow 6
grape      purple 10
plum       purple 2
pineapple  yellow 5

Skipped:
2: apple      red    4
4: strawberry red    3
6: apple      green  8
8: kiwi       brown  4
9: potato     brown  9</code></pre><h2 id="control-freak">Control freak</h2>
<p>In summary, <strong>switch</strong>, <strong>continue</strong>, <strong>next</strong>, and <strong>break</strong> are important preemptive exceptions to awk rules that provide greater control of your script. You don't have to use them directly; often, you can gain the same logic through other means, but they're great convenience functions that make the coder's life a lot easier. The next article in this series covers the <strong>printf</strong> statement.</p>
<hr /><p>Would you rather listen to this article? It was adapted from an episode of <a href="http://hackerpublicradio.org/eps.php?id=2438" target="_blank">Hacker Public Radio</a>, a community technology podcast by hackers, for hackers.</p>
