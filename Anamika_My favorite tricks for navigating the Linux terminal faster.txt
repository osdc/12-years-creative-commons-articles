<p>One of the advantages of working in a terminal is that it's faster than most other interfaces. Thanks to the <a href="https://tiswww.case.edu/php/chet/readline/rltop.html" target="_blank">GNU Readline</a> library and the built-in syntax of shells like <a href="https://opensource.com/downloads/bash-cheat-sheet" target="_blank">Bash</a> and <a href="http://www.opensource.com/article/19/9/getting-started-zsh" target="_blank">Zsh</a>, there are several ways to make your interactions with the command line even faster. Here are five ways to make the most of your time in the terminal.</p>
<h2>1. Navigate without the arrow keys</h2>
<p>While executing commands on the command line, sometimes you miss a part at the beginning or forget to add certain tags or arguments toward the end. It's common for users to use the <em>Left </em>and <em>Right arrow</em> keys on the keyboard to move through a command to make edits.</p>
<p>There's a better way to get around the command line. You can move the cursor to the beginning of the line with <strong>CTRL+A</strong>. Similarly, use <strong>CTRL+E</strong> to move the cursor to the end of the line. <strong>Alt+F</strong> moves one word forward, and <strong>Alt+B</strong> moves one word back.</p>
<p><strong>Shortcuts:</strong></p>
<ul><li>Instead of <em>Left arrow, left, left, left</em>, use <strong>CTRL+A</strong> to go to the start of the line or <strong>Alt+B</strong> to move back one word.</li>
<li>Instead of <em>Right arrow, right, right, right</em>, use <strong>CTRL+E</strong> to move to the end of the line, or <strong>Alt+F</strong> to move forward a word.</li>
</ul><p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">More Linux resources</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux commands cheat sheet">Linux commands cheat sheet</a></div>
              <div class="field__item"><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Advanced Linux commands cheat sheet">Advanced Linux commands cheat sheet</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Free online course: RHEL technical overview">Free online course: RHEL technical overview</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux networking cheat sheet">Linux networking cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="SELinux cheat sheet">SELinux cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Linux common commands cheat sheet">Linux common commands cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="What are Linux containers?">What are Linux containers?</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Register for your free Red Hat account">Register for your free Red Hat account</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ" data-analytics-category="resource list" data-analytics-text="Our latest Linux articles">Our latest Linux articles</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<h2>2. Don't use the backspace or delete keys</h2>
<p>It's not uncommon to misspell commands. You might be used to using the <em>Backspace </em>key on the keyboard to delete characters in the backward direction and the <em>Delete </em>button to delete them in the forward direction. You can also do this task more efficiently and easily with some helpful keyboard shortcuts.</p>
<p>Instead of deleting commands character by character, you can delete everything from the current cursor position to the beginning of the line or the end.</p>
<p>Use <strong>CTRL+U</strong> to erase everything from the current cursor position to the beginning of the line. Similarly, <strong>CTRL+K</strong> erases everything from the current cursor position to the end of the line.</p>
<p><strong>Shortcuts:</strong></p>
<ul><li>Instead of <em>Backspace</em>, use <strong>CTRL+U</strong>.</li>
<li>Instead of <em>Delete</em>, use <strong>CTRL+K</strong>.</li>
</ul><p><drupal-entity data-embed-button="image_cta" data-entity-embed-display="view_mode:cta.embed" data-entity-type="cta" data-entity-uuid="4cdb23e5-0af4-4121-8bc7-1f19bfb2ecc6" data-langcode="en" class="align-right"></drupal-entity></p>
<h2>3. Execute multiple commands in a single line</h2>
<p>Sometimes it's convenient to execute multiple commands in one go, letting a series of commands run while you step away from your computer or turn your attention to something else.</p>
<p>For example, I love contributing to open source, which means working with <a href="https://opensource.com/downloads/cheat-sheet-git" target="_blank">Git repositories</a>. I find myself running these three commands frequently:</p>
<pre>
<code class="language-shell">$ git add
$ git commit -m "message"
$ git push origin main</code></pre><p>Instead of running these commands in three different lines, I use a semi-colon (<code>;</code>) to concatenate them onto a single line and then execute them in sequence.</p>
<p><strong>Shortcuts:</strong></p>
<ul><li>Instead of:<br /><pre>
<code class="language-shell">$ git add .
$ git commit -m "message"
$ git push origin main</code></pre><p>	Use:</p>
<pre>
<code class="language-shell">$ git add .;git commit -m "message";git push origin main</code></pre></li>
</ul><ul><li>Use the <code>;</code> symbol to concatenate and execute any number of commands in a single line. To stop the sequence of commands when one fails, use <code>&amp;&amp;</code> instead:<br /><pre>
<code class="language-shell">$ git add . &amp;&amp; git commit -m "message" &amp;&amp; git push origin main</code></pre></li>
</ul><h2>4. Alias frequently used commands</h2>
<p>You probably run some commands often. Sometimes, these may be lengthy commands or a combination of different commands with the same arguments.</p>
<p>To save myself from retyping these types of commands, I create an alias for the commands I use most frequently. For example, I often contribute to projects stored in a Git repository. Since I use the <code>git push origin main</code> command numerous times daily, I created an alias for it.</p>
<p>To create an alias, open your <code>.bashrc</code> file in your favorite editor and add an alias:</p>
<pre>
<code class="language-plaintext">alias gpom= "git push origin main"</code></pre><p>Try creating an alias for anything you run regularly.</p>
<p>Note: The <code>.bashrc</code> file is for users using the Bash shell. If your system runs a different shell, you probably need to adjust the configuration file you use and possibly the syntax of the alias command. You can check the name of the default shell in your system with the <code>echo $SHELL</code> command.</p>
<p>After creating the alias, reload your configuration:</p>
<pre>
<code class="language-shell">$ . ~/.bashrc</code></pre><p>And then try your new command:</p>
<pre>
<code class="language-shell">$ gpom</code></pre><p><strong>Shortcut:</strong></p>
<p>Instead of typing the original command, such as:</p>
<pre>
<code class="language-shell">$ git push origin main</code></pre><p>Create an alias with the <code>alias</code> declaration in <code>.bashrc</code> or your shell's configuration file.</p>
<h2>5. Search and run a previous command without using the arrow keys</h2>
<p>Most terminal users tend to reuse previously executed commands. You might have learned to use the <em>Up arrow</em> button on your keyboard to navigate your shell's history. But when the command you want to reuse is several lines in the past, you must press the <em>Up arrow</em> repeatedly until you find the command you are looking for.</p>
<p>Typically the situation goes like this: <em>Up arrow, up, up, up. Oh, I found it!</em> <em>Enter</em>.</p>
<p>There is an easier way: You can search your history one step at a time using the <code>history</code> command.</p>
<p>When you use the <code>history</code> command, the list of commands appears with a number beside each. These numbers are known as the <code>history-number</code> of the command. You can type <code>!{history-number}</code> on your terminal to run the command of the corresponding number.</p>
<p><strong>Shortcuts:</strong></p>
<ul><li>Instead of <em>Up arrow, up, up, up, Enter</em>, type <code>history</code>, and then look for the <code>history-number</code> of the command you want to run:<br /><pre>
<code class="language-shell">$ !{history-number}</code></pre></li>
</ul><ul><li>You can also perform this task a different way: Instead of: <em>Up arrow, up, up, up, Enter</em>, use <strong>CTRL+R</strong> and type the first few letters of the command you want to repeat.</li>
</ul><h2>Command-line shortcuts</h2>
<p>Shortcut keys provide an easier and quicker method of navigating and executing commands in the shell. Knowing the little tips and tricks can make your day less hectic and speed up your work on the command line.</p>
<hr /><p><em>This article originally appeared on <a href="https://www.redhat.com/sysadmin/shortcuts-command-line-navigation?intcmp=7013a000002qLH8AAM" target="_blank">Red Hat Enable Sysadmin</a> and has been republished with the author's permission.</em></p>
