<p>Late last year, <a href="http://sexysexypenguins.com/posts/introducing-linch-pin/" target="_blank">my team announced</a> <a href="http://linch-pin.readthedocs.io/en/develop/" target="_blank">LinchPin</a>, a hybrid <a href="https://opensource.com/resources/cloud">cloud</a> orchestration tool using Ansible. Provisioning cloud resources has never been easier or faster. With the power of Ansible behind LinchPin, and a focus on simplicity, many cloud resources are available at users' fingertips. In this article, I'll introduce LinchPin and look at how the project has matured in the past 10 months.</p>
<p>Back when LinchPin was introduced, using the <b>ansible-playbook</b> command to run LinchPin was complex. Although that can still be accomplished, LinchPin now has a new front-end command-line user interface (CLI), which is written in <a href="http://click.pocoo.org/" target="_blank">Click</a> and makes LinchPin even simpler than it was before.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on Ansible</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/downloads/ansible-quickstart?intcmp=701f2000000h4RcAAI">A quickstart guide to Ansible</a></li>
<li><a href="https://opensource.com/downloads/ansible-cheat-sheet?intcmp=701f2000000h4RcAAI">Ansible cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/do007-ansible-essentials-simplicity-automation-technical-overview?intcmp=701f2000000h4RcAAI">Free online course: Ansible essentials</a></li>
<li><a href="https://docs.ansible.com/ansible/latest/intro_installation.html?intcmp=701f2000000h4RcAAI">Download and install Ansible</a></li>
<li><a href="https://go.redhat.com/automated-enterprise-ebook-20180920?intcmp=701f2000000h4RcAAI">eBook: The automated enterprise</a></li>
<li><a href="https://www.ansible.com/resources/ebooks/ansible-for-devops?intcmp=701f2000000h4RcAAI">eBook: Ansible for DevOps</a></li>
<li><a href="https://www.ansible.com/resources/ebooks?intcmp=701f2000000h4RcAAI">Free Ansible eBooks</a></li>
<li><a href="https://opensource.com/tags/ansible?intcmp=701f2000000h4RcAAI">Latest Ansible articles</a></li>
</ul></div>
</div>
</div>
</div>

<p>Not to be outdone by the CLI, LinchPin now also has a <a href="https://opensource.com/resources/python">Python</a> API, which can be used to manage resources, such as Amazon EC2 and OpenStack instances, networks, storage, security groups, and more. The API <a href="http://linchpin.readthedocs.io/en/develop/libdocs.html">documentation</a> can be helpful when you're trying out LinchPin's Python API.</p>
<h2>Playbooks as a library</h2>
<p>Because the core bits of LinchPin are <a href="http://docs.ansible.com/ansible/playbooks.html" target="_blank">Ansible playbooks</a>, the roles, modules, filters, and anything else to do with calling Ansible modules has been moved into the LinchPin library. This means that although one can still call the playbooks directly, it's not the preferred mechanism for managing resources. The <b>linchpin</b> executable has become the de facto front end for the command-line.</p>
<h2>Command-line in depth</h2>
<p>Let's have a look at the <b>linchpin</b> command in depth:</p>
<pre>
<code class="language-bash">$ linchpin
Usage: linchpin [OPTIONS] COMMAND [ARGS]...

  linchpin: hybrid cloud orchestration

Options:
  -c, --config PATH       Path to config file
  -w, --workspace PATH    Use the specified workspace if the familiar Jenkins
                          $WORKSPACE environment variable is not set
  -v, --verbose           Enable verbose output
  --version               Prints the version and exits
  --creds-path PATH       Use the specified credentials path if WORKSPACE
                          environment variable is not set
  -h, --help              Show this message and exit.

Commands:
  init     Initializes a linchpin project.
  up       Provisions nodes from the given target(s) in...
  destroy  Destroys nodes from the given target(s) in...
</code></pre><p>What can be seen immediately is a simple description, along with options and arguments that can be passed to the command. The three commands found near the bottom of this help are where the focus will be for this document.</p>
<h3>Configuration</h3>
<p>In the past, there was <b>linchpin_config.yml</b>. This file is no longer, and has been replaced with an ini-style configuration file, called <b>linchpin.conf</b>. Although this file can be modified or placed elsewhere, its placement in the library path allows easy lookup of configurations. In most cases, the <b>linchpin.conf</b> file should not need to be modified.</p>
<h3>Workspace</h3>
<p>The workspace is a defined filesystem path, which allows grouping of resources in a logical way. A workspace can be considered a single point for a particular environment, set of services, or other logical grouping. It can also be one big storage bin of all managed resources.</p>
<p>The workspace can be specified on the command-line with the <b>--workspace (-w)</b> option, followed by the workspace path. It can also be specified with an environment variable (e.g., <b>$WORKSPACE</b> in bash). The default workspace is the current directory.</p>
<h3>Initialization (init)</h3>
<p>Running <b>linchpin init</b> will generate the directory structure needed, along with an example <b>PinFile</b>, <b>topology</b>, and <b>layout</b> files:</p>
<pre>
<code class="language-bash">$ export WORKSPACE=/tmp/workspace
$ linchpin init
PinFile and file structure created at /tmp/workspace
$ cd /tmp/workspace/
$ tree
.
├── credentials
├── hooks
├── inventories
├── layouts
│   └── example-layout.yml
├── PinFile
├── resources
└── topologies
    └── example-topology.yml
</code></pre><p>At this point, one could execute <b>linchpin up</b> and provision a single libvirt virtual machine, with a network named <b>linchpin-centos71</b>. An inventory would be generated and placed in <b>inventories/libvirt.inventory</b>. This can be known by reading the <b>topologies/example-topology.yml</b> and gleaning out the <b>topology_name</b> value.</p>
<h3>Provisioning (linchpin up)</h3>
<p>Once a PinFile, topology, and optionally a layout are in place, provisioning can happen.</p>
<p>We use the dummy tooling because it is much simpler to configure; it doesn't require anything extra (authentication, network, etc.). The dummy provider creates a temporary file, which represents provisioned hosts. If the temporary file does not have any data, hosts have not been provisioned, or they have been recently destroyed.</p>
<p>The tree for the dummy provider is simple:</p>
<pre>
<code class="language-bash">$ tree
.
├── hooks
├── inventories
├── layouts
│   └── dummy-layout.yml
├── PinFile
├── resources
└── topologies
    └── dummy-cluster.yml
</code></pre><p>The PinFile is also simple; it specifies which topology, and optional layout to use for the <b>dummy1</b> target:</p>
<pre>
<code class="language-yaml">---
dummy1:
  topology: dummy-cluster.yml
  layout: dummy-layout.yml
</code></pre><p>The <b>dummy-cluster.yml</b> topology file is a reference to provision three (3) resources of type <b>dummy_node</b>:</p>
<pre>
<code class="language-yaml">---
topology_name: "dummy_cluster" # topology name
resource_groups:
  -
    resource_group_name: "dummy"
    resource_group_type: "dummy"
    resource_definitions:
      -
        name: "web"
        type: "dummy_node"
        count: 3
</code></pre><p>Performing the command <b>linchpin up</b> should generate <b>resources</b> and <b>inventory</b> files based upon the <b>topology_name</b> (in this case, <b>dummy_cluster</b>):</p>
<pre>
<code class="language-bash">$ linchpin up
target: dummy1, action: up

$ ls {resources,inventories}/dummy*
inventories/dummy_cluster.inventory  resources/dummy_cluster.output
</code></pre><p>To verify resources with the dummy cluster, check <b>/tmp/dummy.hosts</b>:</p>
<pre>
<code class="language-bash">$ cat /tmp/dummy.hosts
web-0.example.net
web-1.example.net
web-2.example.net
</code></pre><p>The Dummy module provides a basic tooling for pretend (or dummy) provisioning. Check out the details for OpenStack, AWS EC2, Google Cloud, and more in the LinchPin <a href="https://github.com/CentOS-PaaS-SIG/linchpin/tree/develop/linchpin/examples/topologies">examples</a>.</p>
<h3>Inventory Generation</h3>
<p>As part of the PinFile mentioned above, a <b>layout</b> can be specified. If this file is specified and exists in the correct location, an Ansible static inventory file will be generated automatically for the resources provisioned:</p>
<pre>
<code class="language-yaml">---
inventory_layout:
  vars:
    hostname: __IP__
  hosts:
    example-node:
      count: 3
      host_groups:
        - example
</code></pre><p>When the <b>linchpin up</b> execution is complete, the resources file provides useful details. Specifically, the IP address(es) or host name(s) are interpolated into the static inventory:</p>
<pre>
<code class="language-bash">[example]
web-2.example.net hostname=web-2.example.net
web-1.example.net hostname=web-1.example.net
web-0.example.net hostname=web-0.example.net

[all]
web-2.example.net hostname=web-2.example.net
web-1.example.net hostname=web-1.example.net
web-0.example.net hostname=web-0.example.net
</code></pre><h3>Teardown (linchpin destroy)</h3>
<p>LinchPin also can perform a teardown of resources. A teardown action generally expects that resources have been provisioned; however, because Ansible is idempotent, <b>linchpin destroy</b> will only check to make sure the resources are up. Only if the resources are already up will the teardown happen.</p>
<p>The command <b>linchpin destroy</b> will either use resources and/or topology files to determine the proper teardown procedure.</p>
<p>The <b>dummy</b> Ansible role does not use the resources, only the topology during teardown:</p>
<pre>
<code class="language-bash">$ linchpin destroy
target: dummy1, action: destroy

$ cat /tmp/dummy.hosts
-- EMPTY FILE --
</code></pre><p>The teardown functionality is slightly more limited around ephemeral resources, like networking, storage, etc. It is possible that a network resource could be used with multiple cloud instances. In this way, performing a <b>linchpin destroy</b> does not teardown certain resources. This is dependent on each provider's implementation. See specific implementations for each of the <a href="https://github.com/CentOS-PaaS-SIG/linch-pin/tree/develop/linchpin/provision/roles">providers</a>.</p>
<h2>The LinchPin Python API</h2>
<p>Much of what is implemented in the <b>linchpin</b> command-line tool has been written using the Python API. The API, although not complete, has become a vital component of the LinchPin tooling.</p>
<p>The API consists of three packages:</p>
<ul><li><b>linchpin</b></li>
<li><b>linchpin.cli</b></li>
<li><b>linchpin.api</b></li>
</ul><p>The command-line tool is managed at the base <b>linchpin</b> package; it imports the <b>linchpin.cli</b> modules and classes, which is a subclassing of <b>linchpin.api</b>. The purpose for this is to allow for other possible implementations of LinchPin using the <b>linchpin.api</b>, like a planned RESTful API.</p>
<p>For more information, see the <a href="http://linchpin.readthedocs.io/en/develop/libdocs.html">Python API library documentation on Read the Docs</a>.</p>
<h2>Hooks</h2>
<p>One of the big improvements in LinchPin 1.0 going forward is hooks. The goal with hooks is to allow additional configuration using external resources in certain specific states during <b>linchpin</b> execution. The states currently are as follows:</p>
<ul><li><b>preup</b>: Executed before provisioning the topology resources</li>
<li><b>postup</b>: Executed after provisioning the topology resources, and generating the optional inventory</li>
<li><b>predestroy</b>: Executed before teardown of the topology resources</li>
<li><b>postdestroy</b>: Executed after teardown of the topology resources</li>
</ul><p>In each case, these hooks allow external scripts to run. Several types of hooks exist, including custom ones called <i>Action Managers</i>. Here's a list of built-in Action Managers:</p>
<ul><li><b>shell</b>: Allows either inline shell commands, or an executable shell script</li>
<li><b>python</b>: Executes a Python script</li>
<li><b>ansible</b>: Executes an Ansible playbook, allowing passing of a <b>vars_file</b> and <b>extra_vars</b> represented as a Python dict</li>
<li><b>nodejs</b>: Executes a Node.js script</li>
<li><b>ruby</b>: Executes a Ruby script</li>
</ul><p>A hook is bound to a specific target and must be restated for each target used. In the future, hooks will be able to be global, and then named in the <b>hooks</b> section for each target more simply.</p>
<h2>Using hooks</h2>
<p>Describing hooks is simple enough, understanding their power might not be so simple. This feature exists to provide flexible power to the user for things that the LinchPin developers might not consider. This concept could lead to a simple way to ping a set of systems, for instance, before running another hook.</p>
<p>Looking into the <i>workspace</i> more closely, one might have noticed the <b>hooks</b> directory. Let's have a look inside this directory to see the structure:</p>
<pre>
<code class="language-bash">$ tree hooks/
hooks/
├── ansible
│   ├── ping
│   │   └── dummy_ping.yaml
└── shell
    └── database
        ├── init_db.sh
        └── setup_db.sh
</code></pre><p>In every case, hooks can be used in the <b>PinFile</b>, shown here:</p>
<pre>
<code class="language-yaml">---
dummy1:
  topology: dummy-cluster.yml
  layout: dummy-layout.yml
  hooks:
    postup:
      - name: ping
        type: ansible
        actions:
          - dummy_ping.yaml
      - name: database
        type: shell
        actions:
          - setup_db.sh
          - init_db.sh
</code></pre><p>The basic concept is that there are three postup actions to complete. Hooks are executed in top-down order. Thus, the Ansible <b>ping</b> task would run first, followed by the two shell tasks, <b>setup_db.sh</b>, followed by <b>init_db.sh</b>. Assuming the hooks execute successfully, a ping of the systems would occur, then a database would be set up and initialized.</p>
<h2>Authentication Driver</h2>
<p>In the initial design of LinchPin, developers decided to have authentication be managed within the Ansible playbooks; however, moving to a more API and command-line driven tool meant that authentication should be outside of the library where the playbooks now reside, and still pass authentication values along as needed.</p>
<h2>Configuration</h2>
<p>Letting users use the authentication method provided by the driver used accomplished this task. For instance, if the topology called for OpenStack, the standard method is to use either a yaml file, or similar <b>OS_</b> prefixed environment variables. A clouds.yaml file consists of a profile, with an <b>auth</b> section:</p>
<pre>
<code class="language-yaml">clouds:
  default:
    auth:
      auth_url: http://stack.example.com:5000/v2.0/
      project_name: factory2
      username: factory-user
      password: password-is-not-a-good-password
</code></pre><p>More detail is in the <a href="https://docs.openstack.org/developer/python-openstackclient/configuration.html">OpenStack documentation</a>.</p>
<p>This clouds.yaml—or any other authentication file—is located in the <b>default_credentials_path</b> (e.g., ~/.config/linchpin) and referenced in the topology:</p>
<pre>
<code class="language-yaml">---
topology_name: openstack-test
resource_groups:
  -
    resource_group_name: linchpin
    resource_group_type: openstack
    resource_definitions:
      - name: resource
        type: os_server
        flavor: m1.small
        image: rhel-7.2-server-x86_64-released
        count: 1
        keypair: test-key
        networks:
          - test-net2
        fip_pool: 10.0.72.0/24
    credentials:
      filename: clouds.yaml
      profile: default
</code></pre><p>The <b>default_credentials_path</b> can be changed by modifying the <b>linchpin.conf</b>.</p>
<p>The topology includes a new <b>credentials</b> section at the bottom. With <b>openstack</b>, <b>ec2</b>, and <b>gcloud</b> modules, the credentials can be specified similarly. The Authentication driver will then look in the given <i>filename</i> <b>clouds.yaml</b>, and search for the <i>profile</i> named <b>default</b>.</p>
<p>Assuming authentication is found and loaded, provisioning will continue as normal.</p>
<h2>Simplicity</h2>
<p>Although LinchPin can be complex around topologies, inventory layouts, hooks, and authentication management, the ultimate goal is simplicity. By simplifying with a command-line interface, along with goals to improve the developer experience coming post-1.0, LinchPin continues to show that complex configurations can be managed with simplicity.</p>
<h2>Community Growth</h2>
<p>Over the past year, LinchPin's community has grown to the point that we now have a <a href="https://www.redhat.com/mailman/listinfo/linchpin">mailing list</a>, an IRC channel (#linchpin on chat.freenode.net), and even manage our sprints in the open with <a href="https://github.com/CentOS-PaaS-SIG/linch-pin/projects/4">GitHub</a>.</p>
<p>The community membership has grown immensely—from 2 core developers to about 10 contributors over the past year. More people continue to work with the project. If you've got an interest in LinchPin, drop us a line, file an issue on GitHub, join up on IRC, or send us an email.</p>
<p><i>This article is based on Clint Savage's OpenWest talk, <a href="https://www.openwest.org/custom/description.php?id=166" target="_blank">Introducing LinchPin: Hybrid cloud provisioning using Ansible</a>. <a href="https://www.openwest.org/" target="_blank">OpenWest</a> will be held July 12-15, 2017 in Salt Lake City, Utah.</i></p>
