<p><a href="https://www.getapp.com/it-management-software/a/qbox-dot-io-hosted-elasticsearch/" target="_blank">Elasticsearch</a> is an open source, full-text search engine developed in Java. Users upload datasets as JSON files. Then, Elasticsearch stores the original document before adding a searchable reference to the document in the cluster’s index.</p>
<p>Less than nine years after its creation, Elasticsearch is the most popular enterprise search engine. Elastic released its latest update—version 7.2.0 —on June 25, 2019.</p>
<p><a href="https://www.elastic.co/products/kibana" target="_blank">Kibana</a> is an open source data visualizer for Elasticsearch. This tool helps users create visualizations on top of content indexed in an Elasticsearch cluster.</p>
<p><a href="https://en.wikipedia.org/wiki/Pie_chart#Ring" target="_blank">Sunbursts</a>, <a href="https://en.wikipedia.org/wiki/Spatial_analysis" target="_blank">geospatial data maps</a>, <a href="https://en.wikipedia.org/wiki/Correlation_and_dependence" target="_blank">relationship analyses</a>, and dashboards with live data are just a few options. And thanks to Elasticsearch’s machine learning prowess, you can learn which properties might influence your data (like servers or IP addresses) and find abnormal patterns.</p>
<p>At <a href="https://www.devfestdc.org/" target="_blank">DevFest DC</a> last month, <a href="https://www.summerrankin.com/about" target="_blank">Dr. Summer Rankin</a>—lead data scientist at Booz Allen Hamilton—uploaded a dataset of content from TED Talks to Elasticsearch, then used Kibana to quickly build a dashboard. Intrigued, I went to an Elasticsearch meetup days later.</p>
<p>Since this course was for newbies, we started at Square One: Installing Elastic and Kibana on our laptops. Without both packages installed, we couldn’t create our own visualizations from the dataset of Shakespeare texts we were using as a dummy JSON file.</p>
<p>Next, I will share step-by-step instructions for downloading, installing, and running Elasticsearch Version 7.1.1 on MacOS. This was the latest version when I attended the Elasticsearch meetup in mid-June 2019.</p>
<h2 id="downloading-elasticsearch-for-macos">Downloading Elasticsearch for MacOS</h2>
<ol><li>Go to <a href="https://www.elastic.co/downloads/elasticsearch" target="_blank">https://www.elastic.co/downloads/elasticsearch</a>, which takes you to the webpage below:</li>
</ol><p><article class="align-center media media--type-image media--view-mode-full" title="The Elasticsearch download page."><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/wwa1f3_600px_0.png" width="600" height="335" alt="The Elasticsearch download page." title="The Elasticsearch download page." /></div>
      
  </article></p>
<ol start="2"><li>In the <strong>Downloads</strong> section, click <strong>MacOS</strong>, which downloads the Elasticsearch TAR file (for example, <strong>elasticsearch-7.1.1-darwin-x86_64.tar</strong>) into your <strong>Downloads</strong> folder.</li>
<li>Double-click this file to unpack it into its own folder (for example, <strong>elasticsearch-7.1.1</strong>), which contains all of the files that were in the TAR.</li>
</ol><p class="rteindent1"><strong>Tip</strong>: If you want Elasticsearch to live in another folder, now is the time to move this folder.</p>
<h2 id="running-elasticsearch-from-the-macos-command-line">Running Elasticsearch from the MacOS command line</h2>
<p>You can run Elasticsearch only using the command line if you prefer. Just follow this process:</p>
<ol><li><a href="https://support.apple.com/en-ca/guide/terminal/welcome/mac" target="_blank">Open a <strong>Terminal</strong> window</a>.</li>
<li>In the terminal window, enter your Elasticsearch folder. For example (if you moved the program, change <strong>Downloads</strong> to the correct path):</li>
</ol><p class="rteindent1"><strong>$ cd ~Downloads/elasticsearch-1.1.0</strong></p>
<ol start="3"><li>Change to the Elasticsearch <strong>bin</strong> subfolder, and start the program. For example:</li>
</ol><p class="rteindent1"><strong>$ cd bin $ ./elasticsearch</strong></p>
<p>Here’s some of the output that my command line terminal displayed when I launched Elasticsearch 1.1.0:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Terminal output when running Elasticsearch."><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/io6t1a_600px.png" width="600" height="513" alt="Terminal output when running Elasticsearch." title="Terminal output when running Elasticsearch." /></div>
      
  </article></p>
<p class="rteindent1"><strong>NOTE</strong>: Elasticsearch runs in the foreground by default, which can cause your computer to slow down. Press <strong>Ctrl-C to</strong> stop Elasticsearch from running.</p>
<h2 id="running-elasticsearch-using-the-gui">Running Elasticsearch using the GUI</h2>
<p>If you prefer your point-and-click environment, you can run Elasticsearch like so:</p>
<ol><li>Open a new <strong>Finder</strong> window.</li>
<li>Select <strong>Downloads</strong> in the left Finder sidebar (or, if you moved Elasticsearch to another folder, navigate to there).</li>
<li>Open the folder called (for the sake of this example) <strong>elasticsearch-7.1.1</strong>. A selection of eight subfolders appears.</li>
</ol><p><article class="align-center media media--type-image media--view-mode-full" title="The elasticsearch/bin menu."><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/o43yku_600px.png" width="600" height="338" alt="The elasticsearch/bin menu." title="The elasticsearch/bin menu." /></div>
      
  </article></p>
<ol start="4"><li>Open the <strong>bin</strong> subfolder. As the screenshot above shows, this subfolder yields 20 assets.</li>
<li>Click the first option, which is <strong>elasticsearch</strong>.</li>
</ol><p>Note that you may get a security warning, as shown below:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="The security warning dialog box."><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/elasticsearch_security_warning_500px.jpg" width="500" height="217" alt="The security warning dialog box." title="The security warning dialog box." /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>opensource.com</p>
</div>
      
  </article></p>
<p> </p>
<p>In order to open the program in this case:</p>
<ol><li>Click <strong>OK</strong> in the warning dialog box.</li>
<li>Open <strong>System Preferences</strong>.</li>
<li>Click <strong>Security &amp; Privacy</strong>, which opens the window shown below:</li>
</ol><p><article class="align-center media media--type-image media--view-mode-full" title="Where you can allow your computer to open the downloaded file."><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/the_general_tab_of_the_system_preferences_security_and_privacy_window.jpg" width="600" height="527" alt="Where you can allow your computer to open the downloaded file." title="Where you can allow your computer to open the downloaded file." /></div>
      
  </article></p>
<ol start="4"><li>Click <strong>Open Anyway</strong>, which opens the confirmation dialog box shown below:</li>
</ol><p><article class="align-center media media--type-image media--view-mode-full" title="Security confirmation dialog box."><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/confirmation_dialog_box.jpg" width="500" height="278" alt="Security confirmation dialog box." title="Security confirmation dialog box." /></div>
      
  </article></p>
<ol start="5"><li>Click <strong>Open</strong>. A terminal window opens and launches Elasticsearch.</li>
</ol><p>The launch process can take a while, so let it run. Eventually, it will finish, and you will see output similar to this at the end:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Launching Elasticsearch in MacOS."><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/y5dvtu_600px.png" width="600" height="406" alt="Launching Elasticsearch in MacOS." title="Launching Elasticsearch in MacOS." /></div>
      
  </article></p>
<h2 id="learning-more">Learning more</h2>
<p>Once you’ve installed Elasticsearch, it’s time to start exploring!</p>
<p>The tool’s <a href="https://www.elastic.co/webinars/getting-started-elasticsearch?ultron=%5BB%5D-Elastic-US+CA-Exact&amp;blade=adwords-s&amp;Device=c&amp;thor=elasticsearch&amp;gclid=EAIaIQobChMImdbvlqOP4wIVjI-zCh3P_Q9mEAAYASABEgJuAvD_BwE" target="_blank">Elasticsearch: Getting Started</a> guide directs you based on your goals. Its introductory video walks through steps to launch a hosted cluster on <a href="https://info.elastic.co/elasticsearch-service-gaw-v10-nav.html?ultron=%5BB%5D-Elastic-US+CA-Exact&amp;blade=adwords-s&amp;Device=c&amp;thor=elasticsearch%20service&amp;gclid=EAIaIQobChMI_MXHt-SZ4wIVJBh9Ch3wsQfPEAAYASAAEgJo9fD_BwE" target="_blank">Elasticsearch Service</a>, perform basic search queries, play with data through create, read, update, and delete (CRUD) REST APIs, and more.</p>
<p>This guide also offers links to documentation, dev console commands, training subscriptions, and a free trial of Elasticsearch Service. This trial lets you deploy Elastic and Kibana on AWS and GCP to support your Elastic clusters in the cloud.</p>
<p>In the follow-up to this article, we’ll walk through the steps you’ll take to install Kibana on MacOS. This process will take your Elasticsearch queries to the next level via diverse data visualizations. Stay tuned!</p>
