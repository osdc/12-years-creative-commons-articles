<p>If you're in the US, you've still got a week left to do your taxes--or at least file an extension. (They're not due until April 17 this year.) For all the procrastinators out there (and next year's exceptionally early planners), there are a few open source tools for helping you get that chore out of the way:</p>
<!--break--><p><br /><br /></p>
<ul><li>The current version of <a href="http://opentaxsolver.sourceforge.net/">Open-source Tax Solver (OTS)</a> contains the updated US 1040 with Schedules A, B, C, D, form 8829 and the new 8849 forms, as well as several updated state tax forms. It also includes improvements on previous versions if you've used the software before.</li>
<li>OTS also offers a <a href="http://opentaxsolver.sourceforge.net/payroll_deduction.html">payroll deduction calculator</a> to help you get closer to the right number the first time around. </li>
<li><a href="https://github.com/codehero/OpenTaxFormFiller">OpenTaxFormFiller</a> is "an open source utility aiming to eliminate the pain of using a PDF viewer to fill in tax forms." It will also import the output from the aforementioned OTS.</li>
</ul><p> </p>
