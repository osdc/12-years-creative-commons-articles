<p>Welcome to the business channel on opensource.com. In this channel we plan to explore how the principles of open source are influencing the world of business.</p>
<p>I have been working at open source software companies for the past 10 years and have experienced first hand how the open source model creates better software faster. I hadn't spent as much time thinking about open source beyond software.</p>
<p>This all changed for me last year.</p>
<!--break-->
<p>I had the privilege of hearing <a title="Gary Hamel" href="http://www.garyhamel.com/" target="_blank">Gary Hamel</a>  speak on the future of management. Gary told the audience that if they wanted to see a model of the business leader of the future  they should study Linus Torvalds.</p>
<p>From Gary’s book <em>The Future of Management</em></p>
<p><em>“Torvalds understands that in a community of peers, people bow to competence, commitment, and foresight, rather than to power. The fact that Torvalds has retained his position at the center of the Linux galaxy for more than a decade, without any formal mandate, makes him a worthy role model for aspiring 21st-century leaders.“</em></p>
<p>Wow. I was very familiar with Linus and his work on the Linux kernel. I had not thought of Linus as the model for 21st-century leaders.</p>
<p>This is one of many examples of how the <a title="open source way" href="https://opensource.com/open-source-way" target="_self">basic principles of open source</a> (open exchange, participation, rapid prototyping, meritocracy, community) are influencing the world of business. This channel will be a place where we continue to learn from each other how open source is effecting the business world.</p>
<p>Jump in and join the conversation. <a title="register" href="https://opensource.com/user/register" target="_self">Create an account</a> to make it easier to  comment on articles  and participate in polls. If you have ideas on what should be discussed in this channel, add them in the comments.</p>
<p>Welcome to the business channel,</p>
<p>Jeff Mackanic</p>
<p>mackanic(at)redhatdotcom</p>
<p> </p>
