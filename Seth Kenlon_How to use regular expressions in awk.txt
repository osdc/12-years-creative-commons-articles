<p>In awk, regular expressions (regex) allow for dynamic and complex pattern definitions. You're not limited to searching for simple strings but also patterns within patterns.</p>
<p>The syntax for using regular expressions to match lines in awk is:</p>
<pre><code class="language-bash">word ~ /match/</code></pre><p>The inverse of that is <em>not</em> matching a pattern:</p>
<pre><code class="language-bash">word !~ /match/</code></pre><p>If you haven't already, create the sample file from our <a href="https://opensource.com/article/19/10/intro-awk">previous article</a>:</p>
<pre><code class="language-text">name       color  amount
apple      red    4
banana     yellow 6
strawberry red    3
raspberry  red    99
grape      purple 10
apple      green  8
plum       purple 2
kiwi       brown  4
potato     brown  9
pineapple  yellow 5</code></pre><p>Save the file as <strong>colours.txt</strong> and run:</p>
<pre><code class="language-bash">$ awk -e '$1 ~ /p[el]/ {print $0}' colours.txt
apple      red    4
grape      purple 10
apple      green  8
plum       purple 2
pineapple  yellow 5</code></pre><p>You have selected all records containing the letter <strong>p</strong> followed by <em>either</em> an <strong>e</strong> or an <b>l</b>.</p>
<p>Adding an <strong>o</strong> inside the square brackets creates a new pattern to match:</p>
<pre><code class="language-bash">$ awk -e '$1 ~ /p[o]/ {print $0}' colours.txt
apple      red    4
grape      purple 10
apple      green  8
plum       purple 2
pineapple  yellow 5
potato     brown  9</code></pre><h2 id="regular-expression-basics">Regular expression basics</h2>
<p>Certain characters have special meanings when they're used in regular expressions.</p>
<h3>Anchors</h3>
<table border="1"><thead><tr><th>Anchor</th>
<th>Function</th>
</tr></thead><tbody><tr><td><strong>^</strong></td>
<td>Indicates the beginning of the line</td>
</tr><tr><td><strong>$</strong></td>
<td>Indicates the end of a line</td>
</tr><tr><td><strong>\A</strong></td>
<td>Denotes the beginning of a string</td>
</tr><tr><td><strong>\z</strong></td>
<td>Denotes the end of a string</td>
</tr><tr><td><strong>\b</strong></td>
<td>Marks a word boundary</td>
</tr></tbody></table><p>For example, this awk command prints any record containing an <strong>r</strong> character:</p>
<pre><code class="language-bash">$ awk -e '$1 ~ /r/ {print $0}' colours.txt
strawberry red    3
raspberry  red    99
grape      purple 10</code></pre><p>Add a <strong>^</strong> symbol to select only records where <strong>r</strong> occurs at the beginning of the line:</p>
<pre><code class="language-bash">$ awk -e '$1 ~ /^r/ {print $0}' colours.txt
raspberry  red    99</code></pre><h3 id="characters">Characters</h3>
<table border="1"><thead><tr><th>Character</th>
<th>Function</th>
</tr></thead><tbody><tr><td><strong>[ad]</strong></td>
<td>Selects <strong>a</strong> or <strong>d</strong></td>
</tr><tr><td><strong>[a-d]</strong></td>
<td>Selects any character <strong>a</strong> through <strong>d</strong> (a, b, c, or d)</td>
</tr><tr><td><strong>[^a-d]</strong></td>
<td>Selects any character <em>except</em> <strong>a</strong> through <strong>d</strong> (e, f, g, h…)</td>
</tr><tr><td><strong>\w</strong></td>
<td>Selects any word</td>
</tr><tr><td><strong>\s</strong></td>
<td>Selects any whitespace character</td>
</tr><tr><td><strong>\d</strong></td>
<td>Selects any digit</td>
</tr></tbody></table><p>The capital versions of w, s, and d are negations; for example, <strong>\D</strong> <em>does not</em> select any digit.</p>
<p><a href="https://opensource.com/article/19/7/what-posix-richard-stallman-explains">POSIX</a> regex offers easy mnemonics for character classes:</p>
<table border="1"><thead><tr><th>POSIX mnemonic</th>
<th>Function</th>
</tr></thead><tbody><tr><td><strong>[:alnum:]</strong></td>
<td>Alphanumeric characters</td>
</tr><tr><td><strong>[:alpha:]</strong></td>
<td>Alphabetic characters</td>
</tr><tr><td><strong>[:space:]</strong></td>
<td>Space characters (such as space, tab, and formfeed)</td>
</tr><tr><td><strong>[:blank:]</strong></td>
<td>Space and tab characters</td>
</tr><tr><td><strong>[:upper:]</strong></td>
<td>Uppercase alphabetic characters</td>
</tr><tr><td><strong>[:lower:]</strong></td>
<td>Lowercase alphabetic characters</td>
</tr><tr><td><strong>[:digit:]</strong></td>
<td>Numeric characters</td>
</tr><tr><td><strong>[:xdigit:]</strong></td>
<td>Characters that are hexadecimal digits</td>
</tr><tr><td><strong>[:punct:]</strong></td>
<td>Punctuation characters (i.e., characters that are not letters, digits, control characters, or space characters)</td>
</tr><tr><td><strong>[:cntrl:]</strong></td>
<td>Control characters <span style="color:#e74c3c;"></span></td>
</tr><tr><td><strong>[:graph:]</strong></td>
<td>Characters that are both printable and visible (e.g., a space is printable but not visible, whereas an <strong>a</strong> is both)</td>
</tr><tr><td><strong>[:print:]</strong></td>
<td>Printable characters (i.e., characters that are not control characters)</td>
</tr></tbody></table><h2 id="quantifiers">Quantifiers</h2>
<table border="1"><thead><tr><th>Quantifier</th>
<th>Function</th>
</tr></thead><tbody><tr><td><strong>.</strong></td>
<td>Matches any character</td>
</tr><tr><td><strong>+</strong></td>
<td>Modifies the preceding set to mean <em>one or more times</em></td>
</tr><tr><td><strong>*</strong></td>
<td>Modifies the preceding set to mean <em>zero or more times</em></td>
</tr><tr><td><strong>?</strong></td>
<td>Modifies the preceding set to mean <em>zero or one time</em></td>
</tr><tr><td><strong>{n}</strong></td>
<td>Modifies the preceding set to mean <em>exactly n times</em></td>
</tr><tr><td><strong>{n,}</strong></td>
<td>Modifies the preceding set to mean <em>n or more times</em></td>
</tr><tr><td><strong>{n,m}</strong></td>
<td>Modifies the preceding set to mean <em>between n and m times</em></td>
</tr></tbody></table><p>Many quantifiers modify the character sets that precede them. For example, <strong>.</strong> means any character that appears exactly once, but <strong>.*</strong> means <em>any or no</em> character. Here's an example; look at the regex pattern carefully:</p>
<pre><code class="language-bash">$ printf "red\nrd\n"
red
rd
$ printf "red\nrd\n" | awk -e '$0 ~ /^r.d/ {print}'
red
$ printf "red\nrd\n" | awk -e '$0 ~ /^r.*d/ {print}'
red
rd</code></pre><p>Similarly, numbers in braces specify the number of times something occurs. To find records in which an <strong>e</strong> character occurs exactly twice:</p>
<pre><code class="language-bash">$ awk -e '$2 ~ /e{2}/ {print $0}' colours.txt
apple      green  8</code></pre><h2 id="grouped-matches">Grouped matches</h2>
<table border="1"><thead><tr><th>Quantifier</th>
<th>Function</th>
</tr></thead><tbody><tr><td><strong>(red)</strong></td>
<td>Parentheses indicate that the enclosed letters must appear contiguously</td>
</tr><tr><td><strong>|</strong></td>
<td>Means <em>or</em> in the context of a grouped match</td>
</tr></tbody></table><p>For instance, the pattern <strong>(red)</strong> matches the word <strong>red</strong> and <strong>ordered</strong> but not any word that contains all three of those letters in another order (such as the word <strong>order</strong>).</p>
<h2 id="awk-like-sed-with-sub-and-gsub">Awk like sed with sub() and gsub()</h2>
<p>Awk features several functions that perform find-and-replace actions, much like the Unix command <strong>sed</strong>. These are functions, just like <strong>print</strong> and <strong>printf</strong>, and can be used in awk rules to replace strings with a new string, whether the new string is a string or a variable.</p>
<p>The <strong>sub</strong> function substitutes the <em>first</em> matched entity (in a record) with a replacement string. For example, if you have this rule in an awk script:</p>
<pre><code class="language-bash">{ sub(/apple/, "nut", $1);
    print $1 }</code></pre><p>running it on the example file <strong>colours.txt</strong> produces this output:</p>
<pre><code class="language-bash">name
nut
banana
raspberry
strawberry
grape
nut
plum
kiwi
potato
pinenut</code></pre><p>The reason both <strong>apple</strong> and <strong>pineapple</strong> were replaced with <strong>nut</strong> is that both are the first match of their records. If the records were different, then the results could differ:</p>
<pre><code class="language-bash">$ printf "apple apple\npineapple apple\n" | \
awk -e 'sub(/apple/, "nut")'
nut apple
pinenut apple</code></pre><p>The <strong>gsub</strong> command substitutes <em>all</em> matching items:</p>
<pre><code class="language-bash">$ printf "apple apple\npineapple apple\n" | \
awk -e 'gsub(/apple/, "nut")'
nut nut
pinenut nut</code></pre><h3 id="gensub">Gensub</h3>
<p>An even more complex version of these functions, called <strong>gensub()</strong>, is also available.</p>
<p>The <strong>gensub</strong> function allows you to use the <strong>&amp;</strong> character to recall the matched text. For example, if you have a file with the word <strong>Awk</strong> and you want to change it to <strong>GNU Awk</strong>, you could use this rule:</p>
<pre><code class="language-bash">{ print gensub(/(Awk)/, "GNU &amp;", 1) }</code></pre><p>This searches for the group of characters <strong>Awk</strong> and stores it in memory, represented by the special character <strong>&amp;</strong>. Then it substitutes the string for <strong>GNU &amp;</strong>, meaning <strong>GNU Awk</strong>. The <strong>1</strong> character at the end tells <strong>gensub()</strong> to replace the first occurrence.</p>
<pre><code class="language-bash">$ printf "Awk\nAwk is not Awkward" \
| awk -e ' { print gensub(/(Awk)/, "GNU &amp;",1) }'
GNU Awk
GNU Awk is not Awkward</code></pre><h2 id="theres-a-time-and-a-place">There's a time and a place</h2>
<p>Awk is a powerful tool, and regex are complex. You might think awk is so very powerful that it could easily replace <strong>grep</strong> and <strong>sed</strong> and <strong>tr</strong> and <a href="https://opensource.com/article/19/10/get-sorted-sort"><strong>sort</strong></a> and many more, and in a sense, you'd be right. However, awk is just one tool in a toolbox that's overflowing with great options. You have a choice about what you use and when you use it, so don't feel that you have to use one tool for every job great and small.</p>
<p>With that said, awk really <em>is</em> a powerful tool with lots of great functions. The more you use it, the better you get to know it. Remember its capabilities, and fall back on it occasionally so can you get comfortable with it.</p>
<p>Our next article will cover looping in Awk, so come back soon!</p>
<hr /><p><em>This article is adapted from an episode of <a href="http://hackerpublicradio.org/eps.php?id=2129" target="_blank">Hacker Public Radio</a>, a community technology podcast.</em></p>
