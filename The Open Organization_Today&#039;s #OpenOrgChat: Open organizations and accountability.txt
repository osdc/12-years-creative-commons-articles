<p>Join us later today for an #OpenOrgChat about "Open Organizations and Accountability"! As usual, we'll gather around the #OpenOrgChat hashtag at 2 p.m. Eastern (14:00 ET/18:00 UTC).</p>
<p>Follow <a href="https://twitter.com/openorgbook" target="_blank">OpenOrgBook</a> and the chat's <a href="https://www.hashtracking.com/streams/openorgbook/openorgchat" target="_blank">live stream</a> for updates!</p>
<h3>This week's special guests</h3>
<ul><li>Amy Gaskins (<a href="https://twitter.com/amyvgaskins" target="_blank">@AmyVGaskins</a>)</li>
<li>Sandra McCann (<a href="https://twitter.com/sc_mccann" target="_blank">@sc_mccann</a>)</li>
<li>Jen Kelchner (<a href="https://twitter.com/jenkelchner" target="_blank">@JenKelchner</a>)</li>
</ul><h3>Some questions we'll explore</h3>
<ul><li>What is accountability? How do open organizations specifically embrace the concept?</li>
<li>How can we enhance accountability without creating cultures of excessive blame?</li>
<li>What is the relationship between accountability and transparency in open organizations?</li>
<li>How do you ensure accountability across geographical locations or on remote teams?</li>
</ul><h3>#OpenOrgChat Twitter chat</h3>
<p><a class="twitter-timeline" href="https://twitter.com/search?q=openorgchat" data-widget-id="636969512437444608">Tweets about openorgchat</a></p>
<script>
<!--//--><![CDATA[// ><!--
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
//--><!]]>
</script>