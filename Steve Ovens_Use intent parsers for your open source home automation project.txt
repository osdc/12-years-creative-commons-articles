<p>In <a href="https://opensource.com/article/20/6/open-source-voice-assistant">part 1</a> and <a href="https://opensource.com/article/20/6/mycroft">part 2</a> of this series on the <a href="https://mycroft.ai/" target="_blank">Mycroft</a> open source voice assistant, I laid the groundwork for learning how to create a skill. In <a href="https://opensource.com/article/20/6/outline-mycroft-voice-assistant-skill">part 3</a>, I walked through creating an outline for a skill and recommended creating the skill in pure Python first to ensure the methods work as intended. That way, when things go wrong, you know it is related to how your Mycroft skill is constructed and not the code itself.</p>
<p>In this article, you will enhance the outline from part 3 by adding:</p>
<ul><li>Mycroft entities</li>
<li>Padatious intents</li>
<li>Adapt intents</li>
<li>Dialogs</li>
<li>Conversational contexts</li>
</ul><p>The code for this project can be found in <a href="https://gitlab.com/stratus-ss/mycroft-ourgroceries-skill" target="_blank">my GitLab</a> repository.</p>
<p>Let's get to it!</p>
<h1 id="improve-your-skill">Improve your skill</h1>
<p>As a reminder, the purpose of this project is to use Mycroft to add items to shopping lists in the <a href="https://www.ourgroceries.com/overview" target="_blank">OurGroceries</a> app. However, this tutorial can be applied to a variety of home automation applications like turning on your lights, getting the morning weather report, or controlling your entertainment systems.</p>
<p>Here's what the skill's outline looks like so far:</p>
<pre><code class="language-python">from mycroft import intent_file_handler, MycroftSkill, intent_handler
from mycroft.skills.context import adds_context, removes_context

class OurGroceriesSkill(MycroftSkill):
    def __init__(self):
        MycroftSkill.__init__(self)

    # Mycroft should call this function directly when the user
    # asks to create a new item
    def create_item_on_list(self, message):
        pass

    # Mycroft should also call this function directly
    def create_shopping_list(self, message):
        pass

    # This is not called directly, but instead should be triggered
    # as part of context aware decisions
    def handle_dont_create_anyways_context(self):
        pass

    # This function is also part of the context aware decision tree
    def handle_create_anyways_context(self):
        pass


    def stop(self):
        pass

def create_skill():
    return OurGroceriesSkill()</code></pre><p>As it stands, Mycroft will load the skill successfully, but it won't do anything because all the methods have the command <code>pass</code> in them. For now, ignore the <code>__init__(self)</code> method and start working with the <code>create_item_on_list</code> method. From the comments, you can see the intent is for Mycroft to call this method directly. That means that you need to declare an <em>intent</em>. How do you do that?</p>
<h1 id="working-with-intents">Working with intents</h1>
<p>You may have noticed that the <a href="https://opensource.com/article/20/6/outline-mycroft-voice-assistant-skill#decorator">outline</a> <code>mycroft-msk</code> created (in the third article) has what looks like a weird function, <code>@intent_file_handler('ourgroceries.intent')</code>, above the <code>handle_test</code> method. These are special notations in Python called decorators (if you want, head over to Real Python for a <a href="https://realpython.com/primer-on-python-decorators/" target="_blank">primer on Python decorators</a>). For this tutorial, it is sufficient to know that a decorator is a way to pass your function into a prebuilt function developed by Mycroft. This saves a lot of work and boilerplate code.</p>
<p>Recall from part three of this series that this project uses two intent parsers: Padacious and Adapt, which I described in the second article.</p>
<h2 id="padatious-intents">Padatious intents</h2>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Python Resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/topics/middleware/what-is-ide?intcmp=7016000000127cYAAQ">What is an IDE?</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-python-37-beginners?intcmp=7016000000127cYAAQ">Cheat sheet: Python 3.7 for beginners</a></li>
<li><a href="https://opensource.com/resources/python/gui-frameworks?intcmp=7016000000127cYAAQ">Top Python GUI frameworks</a></li>
<li><a href="https://opensource.com/downloads/7-essential-pypi-libraries?intcmp=7016000000127cYAAQ">Download: 7 essential PyPI libraries</a></li>
<li><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ">Red Hat Developers</a></li>
<li><a href="https://opensource.com/tags/python?intcmp=7016000000127cYAAQ">Latest Python content</a></li>
</ul></div>
</div>
</div>
</div>
<p>So how do you know which decorator to use and where to use it? Great questions! I'll start with Padatious, which is the easier of the two to understand. If you recall from the second article, <a href="https://mycroft-ai.gitbook.io/docs/skill-development/user-interaction/intents/padatious-intents" target="_blank">Padatious</a> skills are <em>trained</em> based on phrases that the skill's developer decides are relevant for the skill. As Mycroft may install many skills using the Padatious intent engine, each intent is given a score by the neural network module that Mycroft employs. Mycroft then chooses the highest-scoring intent and executes its functions.</p>
<p>The phrases which Mycroft will use to train the intent are placed inside a file with a <code>.intent</code> file extension. You can have multiple <code>.intent</code> files, but you must reference each file explicitly. That means if you have <code>create.item.intent</code> and <code>create.category.intent</code>, there will be no confusion about which file your variables are populated from, as you must call them by file name. As you can see in the output from <code>mycroft-msk</code>, the decorator is intuitively named <code>@intent_file_handler()</code>. Simply use the name of the file as the argument for the decorator, such as <code>@intent_file_handler("create.item.intent")</code>.</p>
<p>Think about what phrases someone might use to add an item to a shopping list. Since the motivating factor for this skill was using Mycroft to create a grocery list, the example code uses food-related terms, but you could use generic terms. With that said, here are some phrases you might say to add an item to your grocery list:</p>
<ul><li>Add tomatoes to my shopping list</li>
<li>Add tomatoes to the grocery list</li>
<li>Add tomatoes to Costco list</li>
</ul><p>You may choose to have some grammatically incorrect phrases as well, to account for Mycroft misunderstanding the user's voice. From the list above, what pieces of information are programmatically relevant? <code>tomatoes</code>, <code>shopping list</code>, <code>grocery list</code>, and <code>Costco list</code>. The official documentation refers to this type of object as an <em>entity</em>. You can think of an entity as a variable, if that makes more sense to you. This will become clearer later when you create an intent file. While the <code>mycroft-msk</code> command will put intents in <code>locale/en-us</code> by default, I put mine under <code>vocab/en-us/</code>. Why? Well, that's because the Adapt intent parser stores its files in <code>vocab</code>, and I prefer to keep all my intent files in the same location. My file <code>vocab/en-us/create.item.intent</code> starts with:</p>
<pre><code class="language-python">add {Food} to my {ShoppingList}</code></pre><p>This defines the <em>entities</em> <code>Food</code> and <code>ShoppingList</code>.</p>
<p><strong>IMPORTANT NOTE</strong>:  Padatious entities are <strong>not</strong> case-sensitive, and Padatious interprets everything in lower-case. For example, <code>ShoppingList</code> will be <code>shoppinglist</code>.</p>
<p>Now that you have an intent, have Mycroft say a phrase containing your <em>entities</em>. Don't forget to add the intent decorator! Your new function will look like this:</p>
<pre><code class="language-python">    @intent_file_handler("create.item.intent")
    def create_item_on_list(self, message):
        """
        This function adds an item to the specified list

        :param message:
        :return: Nothing
        """
        item_to_add = message.data.get('food')
        list_name = message.data.get('shoppinglist')
        self.speak("Adding %s to %s" % (item_to_add, list_name))</code></pre><p>The graphic below uses three phrases:</p>
<ul><li>Add tomatoes to my shopping list</li>
<li>Add nails to my hardware list</li>
<li>Add buns to groceries list</li>
</ul><p>Mycroft will not be able to figure out the intent behind one of these phrases. Can you guess which one and why?</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Mycroft processing intent"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/mycroft-padatious-first-intent.gif" width="1000" height="559" alt="Mycroft processing intent" title="Mycroft processing intent" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Steve Ovens, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>In case the video was a little too fast for you, here's the answer: Mycroft can't process the phrase <code>add buns to groceries list</code> because it is missing the keyword <code>my</code>. The intent explicitly says <code>add {Food} to my {ShoppingList}</code>. Without the word <code>my</code> as input from the user, the Padatious intent for the skill scores low and, therefore, Mycroft doesn't choose this skill to handle the request. The simplest solution is to add a new line to your intent file, like this:</p>
<pre><code class="language-python">add {Food} to {ShoppingList}</code></pre><p>Mycroft can often reload skills when it detects a change, but I prefer to restart Mycroft's skills section to make sure. I also clear the logs a lot during testing, so I run the following command to do everything in one line:</p>
<pre><code class="language-python"> ./stop-mycroft.sh skills;sudo rm -f /var/log/mycroft/skills.log; ./start-mycroft.sh skills; mycroft-cli-client</code></pre><p>Testing the skill after Mycroft restarts yields the following results:</p>
<pre><code class="language-python">add buns to groceries list                                                                 
 &gt;&gt; Adding buns to groceries list </code></pre><p>In case it is not clear, any response Mycroft makes in the <code>mycroft-cli-client</code> is prefixed by <code>&gt;&gt;</code> to indicate its response. Now that you have a basic intent, go back and review the objectives for this skill from part 3 of this series:</p>
<ol><li>Login/authenticate</li>
<li>Get a list of the current grocery lists</li>
<li>Add item to a specific grocery list</li>
<li>Add item to a category under a specific list</li>
<li>Be able to add a category (since OurGroceries allows items to be placed in categories)</li>
</ol><p>Ignore the first two items for now—those deal with the online portion of the project, and you need to complete the other objectives first. For the third item, you have a basic intent that, in theory, should be able to take the <em>entities</em> Mycroft detects and turn them into variables in the Python code. For the fourth item on the list, add two new lines to your intent:</p>
<pre><code class="language-python">add {Food} to my {ShoppingList} under {Category}
add {Food} to {ShoppingList} under {Category}</code></pre><p>You also need to alter your function slightly. When using the Padatious intent parser, <em>entities</em> are returned via the <code>message.data.get()</code> function. This function will return <code>None</code> if the entity is undefined. In other words, if Mycroft cannot parse <code>{Category}</code> from an <code>utterance</code> the user makes, <code>message.data.get()</code> will return <code>None</code>. With this in mind, here is some quick test code:</p>
<pre><code class="language-python">    @intent_file_handler("create.item.intent")
    def create_item_on_list(self, message):
        """
        This function adds an item to the specified list

        :param message:
        :return: Nothing
        """
        item_to_add = message.data.get('food')
        list_name = message.data.get('shoppinglist')
        category_name = message.data.get('category')
        if category_name is None:
            self.speak("Adding %s to %s" % (item_to_add, list_name))
        else:
            self.speak("Adding %s to %s under the category %s" % (item_to_add, list_name, category_name))</code></pre><p>Here is an example that tests these code changes:</p>
<p><article class="align-center media media--type-image media--view-mode-full" title="Mycroft testing code changes"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/mycroft-padatious-category1.gif" width="1000" height="562" alt="Mycroft testing code changes" title="Mycroft testing code changes" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p class="rtecenter"><sup>(Steve Ovens, <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="ugc">CC BY-SA 4.0</a>)</sup></p>
</div>
      
  </article></p>
<p>In the example, Mycroft responds with <code>&gt;&gt; Adding nails to my hardware list under</code>, yet the only time you told Mycroft to say the word <code>under</code> is when <code>category_name</code> has a value other than <code>None</code>. This is because the intent parser is interpreting the word <code>under</code> as a part of the entity <code>ShoppingList</code>. Because the utterance had the word <code>my</code> in it, the sentences that matched the utterances could have been either:</p>
<ol><li><code>add {Food} to my {ShoppingList}</code><br /><br />
	or</li>
<li><code>add {Food} to my {ShoppingList} under {Category}</code></li>
</ol><p>Since the user did not state the <code>{Category}</code>, Mycroft selected the first statement as the most correct. This means that anything <em>after</em> the word <code>my</code> would be cast into the entity <code>{ShoppingList}</code>. Since, therefore, <code>{Category}</code> is <code>None</code>, Mycroft speaks, "Adding nails to my hardware list under" instead of "Adding nails to my hardware list under None."</p>
<p>Padatious may seem a bit simplistic at first. For each phrase you need Mycroft to match, simply add a line to the intent file. However, with a complex intent, you could have several dozen lines attempting to cover all the different utterances you want to handle.</p>
<p>There is another option that may be worth considering. Padatious intents support <em>parentheses expansion</em>. This means you can use a form of <em>OR</em> statements to cut down on the number for lines in an intent. Going back, the example is trying to account for three cases:</p>
<pre><code class="language-python">add {Food} to my {ShoppingList}
add {Food} to my {ShoppingList} under {Category}
add {Food} to the {ShoppingList}
add {Food} to the {ShoppingList} under {Category}
add {Food} to {ShoppingList}
add {Food} to {ShoppingList} under {Category}</code></pre><p>If you want to rewrite this using an <em>OR</em> statement to combine the <code>my</code> and <code>the</code> keywords, you could write:</p>
<pre><code class="language-python">add {Food} to ( my | the ) {ShoppingList}
add {Food} to ( my | the ) {ShoppingList} under {Category}
add {Food} to {ShoppingList}
add {Food} to {ShoppingList} under {Category}</code></pre><p>This removes two lines from the intent. Parentheses expansion also supports making something optional. So, if you want to make <code>the</code> and <code>my</code> optional and thus allow for the phrase <code>add {Food} to {ShoppingList}</code>, it would look like:</p>
<pre><code class="language-python">add {Food} to ( | my | the ) {ShoppingList}
add {Food} to ( | my | the ) {ShoppingList} under {Category}</code></pre><p>This simple change covers all three scenarios (once you restart the Mycroft skills subsystem). You can go a step further and condense this into a single line if you wish:</p>
<pre><code class="language-python">add {Food} to ( | my | the ) {ShoppingList} ( | under {Category})</code></pre><p><strong>Note:</strong> For easier readability, use white spaces in your intent parentheses expansion.</p>
<p>To summarize the important points about Padatious intent parsing:</p>
<ul><li>You have to give several examples of phrases for Mycroft to come up with the correct match.</li>
<li>Padatious intents use entities such as <code>{Food}</code> to identify object values that can be retrieved from your Python code.</li>
<li>Entities are <em>always lower-case</em> regardless of how you declare them in the intent file.</li>
<li>If an entity cannot be parsed from an utterance, its value is <code>None</code>.</li>
<li>The decorator for Padatious intents is <code>@intent_file_handler('my.intent.file.intent')</code>.</li>
</ul><h2 id="adapt-intents">Adapt intents</h2>
<p>Unlike Padatious intents, where you specify entities in the intent file, the Adapt intent parser works with a series of keywords that work in combination with regular expression (regex) files to attempt to capture an entity. You would use Adapt over Padatious when you:</p>
<ol><li>Expect the utterance to be complex and the more robust parsing of regex is required</li>
<li>Want or need Mycroft to be context-aware</li>
<li>Need intents to be as lightweight as possible</li>
</ol><p>That said, the <code>voc</code> files Adapt uses are quite flexible. They can include a single word (as shown in the <a href="https://mycroft-ai.gitbook.io/docs/skill-development/user-interaction/intents/adapt-intents" target="_blank">official documentation</a>), or they can include the start of a sentence you want to react to.</p>
<p>As one goal of this project is to have Mycroft create a new shopping list in the OurGroceries app, I wanted to add in some rudimentary checking so that the user is informed if a list with a similar name exists and asked if they still want to create a new list. This should cut down on list duplication and misplacement of items.</p>
<p>Mock up some code, and then you can deal with the vocab and regex files. While you could use Pytest or similar unit tests to assert specific values, for the sake of simplicity, you will create a list called "shopping list." The Python mock function will look like this:</p>
<pre><code class="language-python">    def create_shopping_list(self, message):
        fake_list = ["shopping list"]
        self.new_shopping_list_name = message.data['ListName'].lower()
        for current_shopping_list in fake_list:
            try:
                if self.new_shopping_list_name in current_shopping_list:
                    if self.new_shopping_list_name == current_shopping_list:
                        self.speak("The shopping list %s already exists" % self.new_shopping_list_name )
                        break
                    else:
                        self.speak("I found a similar naming list called %s" % current_shopping_list)
                        # This hands off to either handle_dont_create_anyways_context or handle_create_anyways_context
                        # to make a context aware decision
                        self.speak("Would you like me to add your new list anyways?", expect_response=True)
                        break
                else:
                    self.speak("Ok creating a new list called %s" % self.new_shopping_list_name)
            except Exception as ex:
    		print(ex)
                pass</code></pre><p>Notice I am using a <code>forloop</code> to iterate over <code>fake_list</code>. That is because, in theory, multiple lists will be returned from the OurGroceries app. Also note the <code>try/except</code> block; I have given a general pass on the exception because, right now, I don't know what kind of exceptions I may run into. As you use and debug your code, you can tighten this up a bit.</p>
<p>Another line to note is:</p>
<pre><code class="language-python">self.speak("Would you like me to add your new list anyways?", expect_response=True) </code></pre><p>This bit of code will have Mycroft prompt the user for a response and store the result. I'll talk more about this bit of code in the conversational contexts section.</p>
<h2 id="regular-expressions-entities-and-adapt-intents">Regular expressions, entities, and Adapt intents</h2>
<p>Now you have some pseudo code, but you need to add the decorator for Mycroft to action your code. You need to create three files for this to work: two vocab files and one regex file. The regex file, which I will name <code>add.shopping.list.rx</code>, looks like this:</p>
<pre><code class="language-python">start a new list called (?P&lt;ListName&gt;.*)
create a new list called (?P&lt;ListName&gt;.*)
add a new list called (?P&lt;ListName&gt;.*)</code></pre><p>You could make this a one-liner, but for simplicity's sake, keep it as three lines. Note this strange-looking notation: <code>(?P&lt;ListName&gt;.*)</code>. This is the part of the code that captures and creates the entity. The entity, in this case, is called <code>ListName</code>. For checking your syntax, I recommend <a href="https://pythex.org/" target="_blank">Pythex</a>. It is very helpful when I am debugging my regex (I'm pretty terrible at regex).</p>
<p><strong>IMPORTANT NOTE: </strong>Adapt intents are case sensitive.</p>
<h2 id="adapt-and-vocab-files">Adapt and vocab files</h2>
<p>Now that your regex includes the full sentences you expect, create your two vocab files. The first file is called <code>CreateKeyword.voc</code>. As you can surmise from the file name, all the words you want to associate with the <code>create</code> action should reside here. This file is very simple:</p>
<pre><code class="language-python">start a new
create a new
add a new</code></pre><p>In the documentation, you will often see only a single word per line. However, due to some Mycroft default skills using <code>start</code> and <code>create</code>, I need to add words so that Mycroft will pick my skill appropriately.</p>
<p>The second file is even easier. It's called <code>ListKeyword.voc</code> and has a single word in it:</p>
<pre><code class="language-python">list</code></pre><p>With these files defined, you can now construct your decorator:</p>
<pre><code class="language-python">@intent_handler(IntentBuilder('CreateShoppingIntent').require('CreateKeyword').require('ListKeyword').require("ListName"))</code></pre><p>The first argument in the <code>IntentBuilder</code> is <code>'CreateShoppingIntent'</code>; this is the name of the intent and is completely optional. If you want to leave this blank, you can. The <code>require</code> section is a bit confusing. When it comes to keywords, the argument for <code>require</code> is the name of the file without the file extension. In this case, one of the files is called <code>ListKeyword.voc</code>, so the argument being passed into <code>require</code> is just <code>'ListKeyword'</code>.</p>
<p>While you can name your vocab files anything you want, I highly recommend using the word <code>Keyword</code> in the file so that when you are building your <code>intent_handler</code> decorator, it is clear what you are requiring.</p>
<p>If <code>require</code> is actually an entity from a regex file, the argument for <code>require</code> is the name of the entity as you defined it in the regex. If your regex was <code>start a new list called (?P&lt;NewList&gt;.*)</code>, then you would write <code>require('NewList')</code>.</p>
<p>Restart the Mycroft skills subsection and try it out. You should see this in the Mycroft command-line interface:</p>
<pre><code class="language-python"> add a new list called hardware
 &gt;&gt; Ok creating a new list called hardware
 
 create a new list called hardware
 &gt;&gt; Ok creating a new list called hardware
 
 start a new list called hardware
 &gt;&gt; Ok creating a new list called hardware</code></pre><h2 id="conversational-contexts">Conversational contexts</h2>
<p>Great, it works! Now add the following decorator to your function:</p>
<pre><code class="language-python">@adds_context("CreateAnywaysContext")</code></pre><p>This decorator is tied to the <a href="https://mycroft-ai.gitbook.io/docs/skill-development/user-interaction/conversational-context" target="_blank">conversational context</a> that Mycroft supports. Conversational contexts are essentially where you can speak normally to Mycroft and it will understand your meaning. For example, you could ask: "Who was John Quincy Adams?" After Mycroft responds, saying something like "John Quincy Adams was the sixth president of the United States," you could ask: "How old was he when he became president?" If you ask the second question first, Mycroft has no way to know who the pronoun <em>he</em> refers to. However, in the context of this conversation, Mycroft understands that <em>he</em> refers to John Quincy Adams.</p>
<p>Getting back to creating a conversational context, the argument for its decorator is the name of the context. This example calls the context <code>CreateAnywaysContext</code> and hence, the full decorator is <code>@adds_context("CreateAnywaysContext")</code>. This mock method is now complete. However, you now need to add two simple methods to handle the user's feedback. You can simplify the grocery list skill by requiring either a yes or a no answer. Create a <code>YesKeyword.voc</code> and a <code>NoKeyword.voc</code>, and place the words <code>yes</code> and <code>no</code> in them, respectively.</p>
<p>Now create two more methods in your Python:</p>
<pre><code class="language-python">@intent_handler(IntentBuilder('DoNotAddIntent').require("NoKeyword").require('CreateAnywaysContext').build())
@removes_context("CreateAnywayscontext")
def handle_dont_create_anyways_context(self):
    """
    Does nothing but acknowledges the user does not wish to proceed
    Uses dont.add.response.dialog
    :return:
    """
    self.speak_dialog('dont.add.response')

    @intent_handler(IntentBuilder('AddAnywaysIntent').require("YesKeyword").require('CreateAnywaysContext').build())
@removes_context("CreateAnywayscontext")
def handle_create_anyways_context(self):
    """
    If the user wants to create a similarly named list, it is handled here
    Uses do.add.response.dialog
    :return:
    """
    self.speak_dialog('do.add.response')</code></pre><p>There are two things here you have not seen so far:</p>
<ol><li><code>@remove_context</code></li>
<li><code>self.speak_dialog</code></li>
</ol><p>If a method that requires <code>CreateAnywaysContext</code> is called, the decorator <code>@remove_context</code> gets rid of the context so that Mycroft does not accidentally action a context more than once. While multiple contexts can be applied to a method, this project will not use them.</p>
<h2 id="dialogs">Dialogs</h2>
<p>Dialogs are files that have several prebuilt responses that Mycroft can pick from. These dialogs are stored in <code>dialog/{language tag}/</code>, and the language tag is based on the IETF standard. Examples can be found on <a href="https://www.venea.net/web/culture_code" target="_blank">Venea.net</a> in the IETF LanguageTag column.</p>
<p>Mycroft picks randomly from the list of sentences in a specified dialog file. Why would you use a dialog file instead of implementing <code>self.speak</code> in Python? The answer is simple: When you create and use a dialog file, you do not have to change the Python code to support other languages.</p>
<p>For example, if the dialog file called <code>dont.add.response.dialog</code> exists under <code>en-us</code> with the following content:</p>
<pre><code class="language-python">Ok... exiting
Gotcha I won't add it
Ok I'll disregard it
Make up your mind!</code></pre><p>You could also create <code>de-de/dont.add.response.dialog</code> with the following content:</p>
<pre><code class="language-python">Ok... Beenden
Erwischt Ich werde es nicht hinzufügen
Ok, ich werde es ignorieren.
Entscheiden Sie sich!</code></pre><p>In your Python code, you would use <code>self.speak_dialog('dont.add.response')</code> to randomly select one of the answers for Mycroft to use. If a user's Mycroft language is set to German, Mycroft will automatically select the correct dialog and play the dialog in German instead of English.</p>
<p>To wrap up this section, create two files under <code>dialog/en-us</code>. For <code>dont.add.response.dialog</code>, use the same content as in the above example. For <code>do.add.response.dialog</code>, use:</p>
<pre><code class="language-python">Ok adding it now
Sure thing
Yup yup yup</code></pre><p>At this point in this project, your tree should look something like this:</p>
<pre><code class="language-sql">├── dialog
│   └── en-us
│       ├── do.add.response.dialog
│       └── dont.add.response.dialog
├── __init__.py
├── regex
│   └── en-us
│       └── add.shopping.list.rx
└── vocab
    └── en-us
        ├── create.item.intent
        ├── CreateKeyword.voc
        └── ListKeyword.voc</code></pre><p>Note that I created the files by hand. If you used the <code>mycroft-msk create</code> method, you might have <code>locale</code> directories, <code>settingsmeta.yaml,</code> or other artifacts.</p>
<h2 id="wrapping-up">Wrapping up</h2>
<p>We've covered a lot so far. You have implemented the Padatious intent parser to, theoretically, add a new item to a list, whether or not you put it under a category. You have also used the Adapt intent parser to add a new category. You used conversational context to prompt the user for confirmation if a similar list already exists. Finally, you learned the concept of dialogs as a way for Mycroft to provide varied confirmation responses to the user.</p>
<p>Currently, the code looks like:</p>
<pre><code class="language-python">from mycroft import intent_file_handler, MycroftSkill, intent_handler
from mycroft.skills.context import adds_context, removes_context
from adapt.intent import IntentBuilder

class OurGroceriesSkill(MycroftSkill):
    def __init__(self):
        MycroftSkill.__init__(self)

    # Mycroft should call this function directly when the user
    # asks to create a new item
    @intent_file_handler("create.item.intent")
    def create_item_on_list(self, message):
        """
        This function adds an item to the specified list

        :param message:
        :return: Nothing
        """
        item_to_add = message.data.get('food')
        list_name = message.data.get('shoppinglist')
        category_name = message.data.get('category')
        if category_name is None:
            self.speak("Adding %s to %s" % (item_to_add, list_name))
        else:
            self.speak("Adding %s to %s under the category %s" % (item_to_add, list_name, category_name))

    # Mycroft should also call this function directly
    @intent_handler(IntentBuilder('CreateShoppingIntent').require('CreateKeyword').require('ListKeyword').require("ListName"))
    def create_shopping_list(self, message):
        fake_list = ["shopping list"]
        self.new_shopping_list_name = message.data['ListName'].lower()
        for current_shopping_list in fake_list:
            try:
                if self.new_shopping_list_name in current_shopping_list:
                    if self.new_shopping_list_name == current_shopping_list:
                        self.speak("The shopping list %s already exists" % self.new_shopping_list_name )
                        break
                    else:
                        self.speak("I found a similar naming list called %s" % current_shopping_list)
                        # This hands off to either handle_dont_create_anyways_context or handle_create_anyways_context
                        # to make a context aware decision
                        self.speak("Would you like me to add your new list anyways?", expect_response=True)
                        break
                else:
                    self.speak("Ok creating a new list called %s" % self.new_shopping_list_name)
            except AttributeError:
                pass
    # This is not called directly, but instead should be triggered
    # as part of context aware decisions
    @intent_handler(IntentBuilder('DoNotAddIntent').require("NoKeyword").require('CreateAnywaysContext').build())
    @removes_context("CreateAnywayscontext")
    def handle_dont_create_anyways_context(self):
        """
        Does nothing but acknowledges the user does not wish to proceed
        Uses dont.add.response.dialog
        :return:
        """
        self.speak_dialog('dont.add.response')

    # This function is also part of the context aware decision tree
    @intent_handler(IntentBuilder('AddAnywaysIntent').require("YesKeyword").require('CreateAnywaysContext').build())
    @removes_context("CreateAnywayscontext")
    def handle_create_anyways_context(self):
        """
        If the user wants to create a similarly named list, it is handled here
        Uses do.add.response.dialog
        :return:
        """
        self.speak_dialog('do.add.response')

    def stop(self):
        pass

def create_skill():
    return OurGroceriesSkill()</code></pre><p> </p>
<p>In the next article, I will go into logging, getting settings from the web UI, and continuing to fill out the skill into something more useful.</p>
