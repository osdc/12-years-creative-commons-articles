<p>When you're the CEO and president of a year-old web startup, having systems experience that dates back to Unix makes for a peerless foundation for growth. <a href="http://www.twitter.com/salimlakhani" target="_blank">Salim Lakhani</a> of <a href="http://www.devpanel.com" target="_blank">devPanel</a> is that luminary.</p>
<p>Lakhani's current role involves promoting the use of applications like Drupal, WordPress, Magento, and Redline through free tools and services. But, this Denver-based executive's experience shows most in forming the global, distributed team of developers and support staff inherent to success.</p>
<!--break-->
<p>At <a href="https://events.drupal.org/losangeles2015" target="_blank">DrupalCon Los Angeles</a> this year, I talked with Lakhani about Drupal, devPanel, and more.</p>
<p><em>Read more interviews and articles from <a href="https://opensource.com/tags/drupalcon-los-angeles-2015" target="_blank">DrupalCon Los Angeles 2015</a>.</em></p>
<p><img src="https://opensource.com/sites/default/files/images/life/Interview%20banner%20Q%26A.png" width="520" height="88" /></p>
<h3>Has Drupal begun to change?</h3>
<p>Drupal started off as a great CMS for building small to large websites. Now, it's more of a platform for developing robust web applications.</p>
<h3>How do developers use your project within the Drupal ecosystem?</h3>
<p>devPanel provides point-and-click Drupal DevOps across hosting companies and platforms. It's a free service that developers can use for setting up and managing dev, test, stage, and production environments. With devPanel, developers have access to all the functionality of Pantheon and Acquia for free on their own servers.</p>
<h3>What's your preferred solution for team communication?</h3>
<p><a href="http://www.redmine.org/" target="_blank">Redmine</a>. It's an open source ticket management system with lots of functionality.</p>
<h3>Have you had any growth challenges?</h3>
<p>Hiring the right people is always a challenge. Early on we realized that everyone looks good on paper, so now we hire all our folks as contractors first and have them go through a suite of dummy projects where we already know what the outcome should look like. They're like case studies—the information given to contractors is incomplete, giving us a chance to observe what questions they ask, how they interact and communicate with others, and what decisions they make.</p>
<h3>What role has DrupalCon played in your profession?</h3>
<p>DrupalCon is the great annual gathering for everything Drupal. It's the time we get to meet with friends, clients, and partners. It's also where we learn about new technologies and techniques for doing things better. DrupalCon helps our team learn, network, and grow.</p>
<h3>Are we done using the term "disruptive," or is something big around the corner?</h3>
<p>There's always something bigger, better, and cooler around the corner!</p>
<hr /><p><em style="font-size: 13.0080003738403px; line-height: 1.538em; background-color: transparent;">Read more interviews and articles from <a href="https://opensource.com/tags/drupalcon-los-angeles-2015" target="_blank">DrupalCon Los Angeles 2015</a>.</em></p>
