<p>Behavior-driven development is a great way to write tests for code because it uses language that real humans can understand. Once you <a href="https://opensource.com/article/19/2/behavior-driven-development-tools">learn about BDD and its benefits</a>, you may want to implement it in your next project. Let's see how to implement BDD in Drupal using <a href="http://behat.org/en/latest/" target="_blank">Behat</a> with the <a href="http://mink.behat.org/en/latest/index.html" target="_blank">Mink</a> extension.</p>
<h2 id="install-and-configure-the-tools">Install and configure the tools</h2>
<p>Since it is good practice to use <a href="https://www.drupal.org/project/composer" target="_blank">Composer</a> to manage a Drupal site's dependencies, use it to install the tools for BDD tests: Behat, Mink, and the Behat Drupal Extension. The Behat Drupal Extension lists Behat and Mink among its dependencies, so you can get all of the tools by installing the Behat Drupal Extension package:</p>
<pre>
<code class="language-text">composer require drupal/drupal-extension --dev</code></pre><p>Mink allows you to write tests in a human-readable format. For example:</p>
<blockquote><p>Given I am registered user,<br />
When I visit the homepage,<br />
Then I should see a personalized news feed</p>
</blockquote>
<div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More DevOps resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/resources/devops?intcmp=7013a00000263HlAAI">What is DevOps?</a></li>
<li><a href="https://opensource.com/downloads/devops-hiring-guide?intcmp=7013a00000263HlAAI">The ultimate DevOps hiring guide</a></li>
<li><a href="https://opensource.com/downloads/devops-monitoring-guide?intcmp=7013a00000263HlAAI">DevOps monitoring tools guide</a></li>
<li><a href="https://opensource.com/downloads/devsecops?intcmp=7013a00000263HlAAI">Getting started with DevSecOps</a></li>
<li><a href="https://enterprisersproject.com/cheat-sheet-devops-glossary?intcmp=7013a00000263HlAAI">Download the DevOps glossary</a></li>
<li><a href="https://www.ansible.com/resources/ebooks/ansible-for-devops?intcmp=7013a00000263HlAAI">eBook: Ansible for DevOps</a></li>
<li><a href="https://opensource.com/tags/devops?src=devops_resource_menu2">Latest DevOps articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>Because these tests are supposed to emulate user interaction, you can assume they will be executed within a web browser. That is where Mink comes into play. There are various browser emulators, such as Goutte and Selenium, and they all behave differently and have very different APIs. Mink allows you to write a test once and execute it in different browser emulators. In layman's terms, Mink allows you to control a browser programmatically to emulate a user's action.</p>
<p>Now that you have the tools installed, you should have a <strong>behat</strong> command available. If you run it:</p>
<pre>
<code class="language-text">./vendor/bin/behat</code></pre><p>you should get an error, like:</p>
<pre>
<code class="language-text">FeatureContext context class not found and can not be used</code></pre><p>Start by initializing Behat:</p>
<pre>
<code class="language-text">./vendor/bin/behat --init</code></pre><p>This will create two folders and one file, which we will revisit later; for now, running <strong>behat</strong> without the extra parameters should not yield an error. Instead, you should see an output similar to this:</p>
<pre>
<code class="language-bash">No scenarios
No steps
0m0.00s (7.70Mb)</code></pre><p>Now you are ready to write your first test, for example, to verify that website visitors can leave a message using the site-wide contact form.</p>
<h2 id="writing-test-scenarios">Writing test scenarios</h2>
<p>By default, Behat will look for files in the <strong>features</strong> folder that's created when the project is initialized. The file inside that folder should have the <strong>.feature</strong> extension. Let's tests the site-wide contact form. Creata a file <strong>contact-form.feature</strong> in the features folder with the following content:</p>
<pre>
<code class="language-yaml">
Feature: Contact form
  In order to send a message to the site administrators
  As a visitor
  I should be able to use the site-wide contact form

  Scenario: A visitor can use the site-wide contact form
    Given I am at "contact/feedback"
    When I fill in "name" with "John Doe"
    And I fill in "mail" with "john@doe.com"
    And I fill in "subject" with "Hello world"
    And I fill in "message" with "Lorem Ipsum"
    And I press "Send message"
    Then I should see the text "Your message has been sent."</code></pre><p>Behat tests are written in Gherkin, a human-readable format that follows the Context–Action–Outcome pattern. It consists of several special keywords that, when parsed, will execute commands to emulate a user's interaction with the website.</p>
<p>The sentences that start with the keywords <strong>Given</strong>, <strong>When</strong>, and <strong>Then</strong> indicate the Context, Action, and Outcome, respectively. They are called <strong>Steps</strong> and they should be written from the perspective of the user performing the action. Behat will read them and execute the corresponding <strong>Step Definitions</strong>. (More on this later.)</p>
<p>This example instructs the browser to visit a page under the "contact/feedback" link, fill in some field values, press a button, and check whether a message is present on the page to verify that the action worked. Run the test; your output should look similar to this:</p>
<pre>
<code class="language-bash">1 scenario (1 undefined)
7 steps (7 undefined)
0m0.01s (8.01Mb)

 &gt;&gt; default suite has undefined steps. Please choose the context to generate snippets:

  [0] None
  [1] FeatureContext
 &gt;</code></pre><p>Type 0 at the prompt to select the None option. This verifies that Behat found the test and tried to execute it, but it is complaining about undefined steps. These are the <strong>Step Definitions</strong>, PHP code that will execute the tasks required to fulfill the step. You can check which steps definitions are available by running:</p>
<pre>
<code class="language-text">./vendor/bin/behat -dl</code></pre><p>Currently there are no step definitions, so you shouldn't see any output. You could write your own, but for now, you can use some provided by the Mink extension and the Behat Drupal Extension. Create a <strong>behat.yml</strong> file at the same level as the Features folder—not inside it—with the following contents:</p>
<pre>
<code class="language-yaml">default:
  suites:
    default:
      contexts:
        - FeatureContext
        - Drupal\DrupalExtension\Context\DrupalContext
        - Drupal\DrupalExtension\Context\MinkContext
        - Drupal\DrupalExtension\Context\MessageContext
        - Drupal\DrupalExtension\Context\DrushContext
  extensions:
    Behat\MinkExtension:
      goutte: ~</code></pre><p>Steps definitions are provided through <strong>Contexts</strong>. When you initialized Behat, it created a FeatureContext without any step definitions. In the example above, we are updating the configuration file to include this empty context along with others provided by the Drupal Behat Extension. Running <strong>./vendor/bin/behat -dl</strong> again produces a list of 120+ steps you can use; here is a trimmed version of the output:</p>
<pre>
<code class="language-bash">default | Given I am an anonymous user
default | When I visit :path
default | When I click :link
default | Then I (should )see the text :text</code></pre><p>Now you can perform lots of actions. Run the tests again with <strong>./vendor/bin/behat</strong> .The test should fail with an error similar to:</p>
<pre>
<code class="language-bash">  Scenario: A visitor can use the site-wide contact form 	# features/contact-form.feature:8
	And I am at "contact/feedback"                       	# Drupal\DrupalExtension\Context\MinkContext::assertAtPath()
	When I fill in "name" with "John Doe"                	# Drupal\DrupalExtension\Context\MinkContext::fillField()
	And I fill in "mail" with "john@doe.com"             	# Drupal\DrupalExtension\Context\MinkContext::fillField()
	And I fill in "subject" with "Hello world"           	# Drupal\DrupalExtension\Context\MinkContext::fillField()
  	Form field with id|name|label|value|placeholder "subject" not found. (Behat\Mink\Exception\ElementNotFoundException)
	And I fill in "message" with "Lorem Ipsum"           	# Drupal\DrupalExtension\Context\MinkContext::fillField()
	And I press "Send message"                           	# Drupal\DrupalExtension\Context\MinkContext::pressButton()
	Then I should see the text "Your message has been sent." # Drupal\DrupalExtension\Context\MinkContext::assertTextVisible()

--- Failed scenarios:

	features/contact-form.feature:8

1 scenario (1 failed)
7 steps (3 passed, 1 failed, 3 skipped)
0m0.10s (12.84Mb)</code></pre><p>The output shows that the first three steps—visiting the contact page and filling in the name and subject fields—worked. But the test fails when the user tries to enter the subject, then it skips the rest of the steps. These steps require you to use the name attribute of the HTML tag that renders the form field.</p>
<p>When I created the test, I purposely used the proper values for the name and address fields so they would pass. When in doubt, use your browser's developer tools to inspect the source code and find the proper values you should use. By doing this, I found I should use <strong>subject[0][value]</strong> for the subject and <strong>message[0][value]</strong> for the message. When I update my test to use those values and run it again, it should pass with flying colors and produce an output similar to:</p>
<pre>
<code class="language-text">1 scenario (1 passed)
7 steps (7 passed)
0m0.29s (12.88Mb)</code></pre><p>Success! The test passes! In case you are wondering, I'm using the Goutte browser. It is a command line browser, and the driver to use it with Behat is installed as a dependency of the Behat Drupal Extension package.</p>
<h2 id="other-things-to-note">Other things to note</h2>
<p>As mentioned above, BDD tests should be written from the perspective of the user performing the action. Users don't think in terms of HTML name attributes. That is why writing tests using <strong>subject[0][value]</strong> and <strong>message[0][value]</strong> is both cryptic and not very user friendly. You can improve this by creating custom steps at <strong>features/bootstrap/FeatureContext.php</strong>, which was generated when Behat initialized.</p>
<p>Also, if you run the test several times, you will find that it starts failing. This is because Drupal, by default, imposes a limit of five submissions per hour. Each time you run the test, it's like a real user is performing the action. Once the limit is reached, you'll get an error on the Drupal interface. The test fails because the expected success message is missing.</p>
<p>This illustrates the importance of debugging your tests. There are some steps that can help with this, like <strong>Then print last drush output</strong> and <strong>Then I break</strong>. Better yet is using a real debugger, like <a href="https://xdebug.org/" target="_blank">Xdebug</a>. You can also install other packages that provide more step definitions specifically for debugging purposes, like <a href="https://github.com/Behatch/contexts">Behatch</a> and <a href="https://github.com/nuvoleweb/drupal-behat">Nuvole's</a> extension,. For example, you can configure Behat to take a screenshot of the state of the browser when a test fails (if this capability is provided by the driver you're using).</p>
<p>Regarding drivers and browser emulators, Goutte doesn't support JavaScript. If a feature depends on JavaScript, you can test it by using the <a href="http://mink.behat.org/en/latest/drivers/selenium2.html" target="_blank">Selenium2Driver</a> in combination with <a href="https://github.com/mozilla/geckodriver" target="_blank">Geckodriver</a><a href="https://github.com/mozilla/geckodriver" target="_blank"> </a>and Firefox. Every driver and browser has different features and capabilities. For example, the Goutte driver provides access to the response's HTTP status code, but the Selenium2Driver doesn't. (You can read more about <a href="http://mink.behat.org/en/latest/guides/drivers.html" target="_blank">drivers in Mink and Behat</a>.) For Behat to pickup a javascript enabled driver/browser you need to annotate the scenario using the @javascript tag. Example:</p>
<pre>
<code class="language-yaml">Feature:
  (feature description)

  @javascript
  Scenario: An editor can select the author of a node from an autocomplete field
    (list of steps)</code></pre><p>Another tag that is useful for Drupal sites is <strong>@api</strong>. This instructs the Behat Drupal Extension to use a driver that can perform operations specific to Drupal; for example, creating users and nodes for your tests. Although you could follow the registration process to create a user and assign roles, it is easier to simply use a step like <strong>Given I am logged in as a user with the "Authenticated user" role</strong>. For this to work, you need to <a href="https://behat-drupal-extension.readthedocs.io/en/3.1/drivers.html" target="_blank">specify whether you want to use the Drupal or Drush driver</a>. Make sure to update your <strong>behat.yml</strong> file accordingly. For example, to use the Drupal driver:</p>
<pre>
<code class="language-yaml">default:
  extensions:
    Drupal\DrupalExtension:
      blackbox: ~
      api_driver: drupal
      drupal:
        drupal_root: ./relative/path/to/drupal</code></pre><p>I hope this introduction to BDD testing in Drupal serves you well. If you have questions, feel free to add a comment below, send me an email to {my first name}@{my last name}.me  or a tweet at <a href="https://twitter.com/dinarcon?lang=en" target="_blank">@dinarcon</a>.</p>
<hr /><p><em>Mauricio Dinarte will present <a href="https://events.drupal.org/seattle2019/sessions/behavior-driven-development-drupal-8-behat" target="_blank">Behavior-Driven Development in Drupal 8 with Behat</a> at <a href="https://events.drupal.org/seattle2019" target="_blank">DrupalCon</a> in Seattle, April 8-12, 2019.</em></p>
<hr />