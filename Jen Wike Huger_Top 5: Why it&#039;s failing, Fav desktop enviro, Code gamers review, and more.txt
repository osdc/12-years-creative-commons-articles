<p>This week, our best articles include a review of two code gamer platforms, 6 continous integration tools, why eat your own open source dog food, your favorite desktop environment, and why your open source project is failing.</p>
<p><iframe src="https://www.youtube.com/embed/clTa5HuwC90" allowfullscreen="" frameborder="0" height="315" width="520"></iframe></p>
<h2>Top 5 articles of the week</h2>
<p><strong>5. Teach coding with games: a review of Codewars and CodeCombat</strong></p>
<p><a href="https://opensource.com/education/15/7/codewars-codecombat-review">https://opensource.com/education/15/7/codewars-codecombat-review</a></p>
<p>CodeCombat and Codewars are two excellent platforms for learning how to code through gaming. CodeCombat is more of a role-playing game for any age, and with Codewars the gamer solves more difficult problems to advance to the next level.</p>
<p><strong>4. 6 top continuous integration tools</strong></p>
<p><a href="https://opensource.com/business/15/7/six-continuous-integration-tools">https://opensource.com/business/15/7/six-continuous-integration-tools</a></p>
<p>This is the TLDR for six open source continuous integration tools: Jenkins, Buildbot, Travis CI, Strider, Go, amd Integrity. This review includes each one's project page, source code, and license.</p>
<p><strong>3. Eating our own dog food in open source</strong></p>
<p><a href="https://opensource.com/business/15/7/eating-our-own-dog-food">https://opensource.com/business/15/7/eating-our-own-dog-food</a></p>
<p>Jason van Gumster, a Blender expert and enthusiast, writes another installment of his Open Art column for us about how we can all do a better job using open source tools for everything from document creation to image manipulation, and more. What was most interesting to me was his bit about <em>all</em> software being inadequate. So, why not default to open?</p>
<p><strong>2. What is your favorite desktop environment?</strong></p>
<p><a href="https://opensource.com/life/15/7/poll-preferred-desktop-environment">https://opensource.com/life/15/7/poll-preferred-desktop-environment</a></p>
<p>This week's poll asks: Which desktop environment is your favorite? We listed a handful of options, then asked you to write in if you didn't see yours on the list. At almost 3K votes as of this writing / recording, KDE and Gnome are neck and neck with 22% and 23% of the votes respectively. We also had many comments on the article and on social media that we forgot to include the popular Unity! We hope to include it when we ask this poll again in a few months.</p>
<p><strong>1. This is why your open source project is failing</strong></p>
<p><a href="https://opensource.com/life/15/7/why-your-open-source-project-failing">https://opensource.com/life/15/7/why-your-open-source-project-failing</a></p>
<p>Nicole Engard deservedly ranks in at #1 this week for her report from OSCON last week on Tom Callaway's talk <em>This is Why You Fail: The Avoidable Mistakes Open Source Projects STILL Make</em>. Tom identifies 7 points of failure, and 11 reasons why your open source projects fails. Check it out! You may just save your project.</p>
