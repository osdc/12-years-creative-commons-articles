<p>Over the years, several friends have asked me for tips on managing their home networks. In most cases, they are setting up home automation and want to access their services from the outside.</p>
<p>Every time I helped them, each made the same comment: "Are you kidding? It cannot be so complicated to publish one simple application!"</p>
<p>Publishing applications that don't put your network or cluster at risk can indeed be quite complicated. When we started working on <a href="https://traefik.io/traefik-hub/" target="_blank">Traefik Hub</a>—the latest product by <a href="https://traefik.io/" target="_blank">Traefik Labs</a>—I knew it would be a game-changer for publishing applications.</p>
<p>This article demonstrates the complexity of publishing services and how Traefik Hub makes your life a lot easier. I use the example of setting up a server to control your home automation remotely with Traefik Hub running on a Raspberry Pi.</p>
<h2>The challenge</h2>
<p>Setting up a server to manage your home automation is nice, but being able to control it remotely from anywhere in the world using only your mobile phone—that's even nicer!</p>
<p>However, with great power comes great responsibility. If you want access to your local network from the outside, you'd better ensure it's resilient and that you are the only one with access.</p>
<p>First, I'll look at the steps you would normally take to achieve that.</p>
<h2>Reach your Home Assistant remotely any time</h2>
<p><a href="https://www.home-assistant.io/" target="_blank">Home Assistant</a> is a well-known solution to manage home automation devices. It's an open source project written in Python. It allows you to have Home Automation with a local installation: No data on the cloud and everything is kept private. I recommend <a href="https://opensource.com/article/20/12/home-assistant" target="_blank">this excellent article</a> to help you install Home Assistant on your Raspberry Pi using Docker.</p>
<p>To reach your Home Assistant from the outside, you must expose your Raspberry Pi to the internet. To do so, you have to:</p>
<ul><li>Know the <a href="https://opensource.com/article/18/5/how-find-ip-address-linux" target="_blank">public IP of your router</a>.</li>
<li>Redirect the traffic <a href="https://opensource.com/article/21/9/what-port-forwarding" target="_blank">from your router to your Raspberry Pi</a>.</li>
</ul><p><strong>Note:</strong> Most internet providers assign dynamic public IPs—each time your router restarts, your IP will probably change. To build a resilient system, you would also need a dynamic domain.</p>
<p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">More on Raspberry Pi</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://opensource.com/resources/what-raspberry-pi?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="What is Raspberry Pi?">What is Raspberry Pi?</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/raspberry-pi-guide?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="eBook: Guide to Raspberry Pi">eBook: Guide to Raspberry Pi</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/getting-started-raspberry-pi-cheat-sheet?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Getting started with Raspberry Pi cheat sheet">Getting started with Raspberry Pi cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/kubernetes-raspberry-pi?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="eBook: Running Kubernetes on your Raspberry Pi">eBook: Running Kubernetes on your Raspberry Pi</a></div>
              <div class="field__item"><a href="https://www.redhat.com/en/topics/edge-computing?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Understanding edge computing">Understanding edge computing</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Register for your free Red Hat account">Register for your free Red Hat account</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/raspberry-pi?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Our latest on Raspberry Pi">Our latest on Raspberry Pi</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<h3>Encryption matters</h3>
<p>When you communicate with your server, you send sensitive data, such as your username and password. You must verify and encrypt communication using a <a href="https://traefik.io/glossary/https-encryption-tls-ssl-letsencrypt/" target="_blank">TLS certificate</a> to avoid this data being stolen.</p>
<p>This requires:</p>
<ul><li>A <a href="https://traefik.io/glossary/reverse-proxy/" target="_blank">reverse proxy</a> to do the TLS termination.</li>
<li>A piece of infrastructure to generate and renew your <a href="https://traefik.io/glossary/https-encryption-tls-ssl-letsencrypt/" target="_blank">Let's Encrypt certificates</a>.</li>
</ul><h3>TL;DR</h3>
<p>To sum it up, after installing Home Assistant on your Raspberry Pi, you need to:</p>
<ul><li>Get your router public IP.</li>
<li>Create a port forward to your Raspberry Pi.</li>
<li>Buy a domain name.</li>
<li>Create a dynamic domain.</li>
<li>Install a reverse proxy and configure it for encrypted access using a TLS certificate.</li>
</ul><p>Now, imagine if you could skip all of the steps above and publish your services in a few clicks!</p>
<h2>Traefik Hub to the rescue</h2>
<p><a href="https://traefik.io/traefik-hub/" target="_blank">Traefik Hub</a> is a cloud-native networking SaaS platform that allows users to publish their services at the edge quickly. Using Traefik Hub, you can publish your Home Assistant application in a few clicks.</p>
<p>Remember the challenges I mentioned earlier? Scratch that. Once you have Home Assistant installed on your Raspberry Pi, all you have to do is connect your Raspberry Pi to Traefik Hub. Traefik Hub handles everything for you, including:</p>
<ul><li>Making your service reachable from the internet.</li>
<li>Providing a dynamic domain (for free).</li>
<li>Encrypting communication with a TLS certificate and an Access Control Policy.</li>
</ul><p>And now that I have introduced Traefik Hub, I'll get down to the business of configuring it.</p>
<h3>Step 1: Connect your Raspberry Pi to Traefik Hub</h3>
<p>First, head over to Traefik Hub and <a href="http://hub.traefik.io/" target="_blank">sign up</a> for a free account. You can sign up via Google or GitHub.</p>
<p>You need to add a new agent to connect your Raspberry Pi to Traefik Hub.</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-08/1addagent.png" width="936" height="716" alt="add an agent" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Nicolas Mengin, CC BY-SA 4.0)</p>
</div>
      
  </article><p>Traefik Hub provides several snippets that allow you to start from scratch.</p>
<p>Since the Home Assistant setup is a bit complex, you can get your token from the Hub UI and use the script below for this example. The token allows you to connect your agent to Traefik Hub. Traefik Hub then attaches this agent to your account, and you can start publishing your services.</p>
<p>Here's the script:</p>
<pre>
<code class="language-text">version: '3'

networks:
  traefik: {}

services:
  homeassistant:
    container_name: homeassistant
    image: "ghcr.io/home-assistant/home-assistant:stable"
    volumes:
		  # /!\\ Mount the custom configuration file described below /!\\
      - ./configuration.yaml:/config/configuration.yaml
      - /etc/localtime:/etc/localtime:ro
    restart: unless-stopped
    privileged: true
		networks:
      - traefik
		ports:
			- 8123
 
	# Start the agent with the latest version
  hub-agent:
    image: ghcr.io/traefik/hub-agent-traefik:v0.7.2
    restart: "on-failure"
    container_name: hub-agent
    networks:
      - traefik
		command:
      - run
      - --hub.token=&lt;YOUR_TOKEN&gt; # Set your token here
      - --auth-server.advertise-url=http://hub-agent
      - --traefik.host=traefik
      - --traefik.tls.insecure=true
      - --hub.url=https://platform.hub.traefik.io/agent
      - --hub.ui.url=https://hub.traefik.io
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    depends_on:
      - traefik

  # Start Traefik with the latest version
  traefik:
    image: traefik:v2.8
    container_name: traefik
    networks:
      - traefik
    command:
      # Enable Hub communication (open the port 9900 and 9901 by default)
      - --experimental.hub=true
      - --hub.tls.insecure=true
      - --metrics.prometheus.addrouterslabels=true</code></pre><pre>
<code class="language-text"># ./configuration.yaml to mount on your home assistant container
# in /config/configuration.yaml

# These modifications are required by home assistant to be exposed using
# a third party software such as the Traefik Hub agent

# Loads default set of integrations. Do not remove.
default_config:

http:
  ip_ban_enabled: true
  login_attempts_threshold: 5
  use_x_forwarded_for: true
  trusted_proxies:
    - 192.168.1.0/24
    - 172.18.0.0/24
    - 127.0.0.1
    - ::1
    - fe80::/64
    - fe00::/64
    - fd00::/64

# Text to speech
tts:
  - platform: google_translate</code></pre><h3>Step 2: Publish your service</h3>
<p>Once you have installed the agent on your Raspberry Pi, Traefik Hub discovers every service running on your cluster so you can publish them without digging into your configuration files.</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-08/2homeasst.png" width="936" height="738" alt="Configure Home Assistant" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Nicolas Mengin, CC BY-SA 4.0)</p>
</div>
      
  </article><p>Select your Home Assistant service, and click the <strong>Save and Publish</strong> button to publish it.</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-08/3publishhomeasst.png" width="936" height="740" alt="Publish Home Assistant service" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Nicolas Mengin, CC BY-SA 4.0)</p>
</div>
      
  </article><p>And now let the magic happen!</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-08/4successpubd.png" width="936" height="996" alt="Success message for published service" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Nicolas Mengin, CC BY-SA 4.0)</p>
</div>
      
  </article><p>Once Hub notifies you that your service has been published, you can reach it from the internet using the domain Traefik Hub has generated. The connection is verifiable and encrypted, and your Home Assistant remains reachable even if your public IP changes.</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-08/5secureconnect.png" width="936" height="442" alt="Secure connection confirmed" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Nicolas Mengin, CC BY-SA 4.0)</p>
</div>
      
  </article><h2>Behind the scenes</h2>
<p>Your application is published. Next, I'll discuss a few things Traefik Hub takes care of behind the scenes to offer a seamless experience and some handy configuration options.</p>
<h3>Traefik instance</h3>
<p>When you installed the Traefik Hub Agent, you certainly noticed that it comes with a Traefik Proxy instance.</p>
<p>Traefik Hub creates a tunnel between its platform and the agent you installed on your Raspberry Pi to publish your service on the internet. The agent passes through the requests to open source <a href="https://traefik.io/traefik/" target="_blank">Traefik Proxy</a>, which is used as an <a href="https://traefik.io/glossary/kubernetes-ingress-and-ingress-controller-101/" target="_blank">Ingress Controller</a>. Traefik Hub manages both the domain and the TLS certificate, and it shares the certificate with your Traefik instance to allow it to do the TLS termination.</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-08/6topology.jpg" width="468" height="303" alt="Traefik connection topology" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Nicolas Mengin, CC BY-SA 4.0)</p>
</div>
      
  </article><h3>Access Control Policy</h3>
<p>Another point to remember is that a deployed Home Assistant application comes with its own login system. However, when you publish a service using Traefik Hub, you can restrict access further by using an <strong>Access Control Policy</strong> such as JWT and Basic Auth.</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-08/7newACP.png" width="936" height="642" alt="New access control policy" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Nicolas Mengin, CC BY-SA 4.0)</p>
</div>
      
  </article><h3>Kubernetes</h3>
<p>If you are a Kubernetes user, you can also publish your Kubernetes Services. Traefik Hub can manage Kubernetes Services through the UI or a dedicated CRD.</p>
<h3>Manage and monitor</h3>
<p>Traefik Hub also provides a web UI that allows you to manage and monitor the exposition of services.</p>
<article class="align-center media media--type-image media--view-mode-default"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/2022-08/8perf.png" width="936" height="986" alt="Performance dashboard" /></div>
      
            <div class="clearfix text-formatted field field--name-field-caption field--type-text-long field--label-hidden field__item"><p>(Nicolas Mengin, CC BY-SA 4.0)</p>
</div>
      
  </article><h2>Wrap up</h2>
<p>This article started by going through a long and complicated list of tasks that come with publishing an application over an encrypted and verifiable connection. Setting up home automation is an excellent example of that level of complexity. But when things seem impossibly hard, there's always an easier alternative! Traefik Hub makes your life simpler by taking over most of the mundane operations tasks, saving time, and allowing developers to focus on building applications.</p>
<p>Now you can turn the lights on in your house, even if you're on the other side of the world!</p>
<p>If you're interested in learning more about Traefik Hub, check out this <a href="https://traefik.io/blog/publish-and-secure-applications-with-traefik-hub/" target="_blank">getting started article</a>. Traefik Hub is currently in Beta, so please don't hesitate to give it a try and provide feedback—you can do so directly in the UI.</p>
<p>I hope you found this article helpful, and thank you for reading!</p>
