<p>Over the past several years, Rust has gained a passionate following among programmers. Tech trends come and go, so it can be difficult to separate excitement just because something is new versus excitement over the merits of a technology, but <a href="https://opensource.com/article/21/7/rt-thread-smart">RT-Thread</a> community developer Liu Kang believes that Rust is a truly well-designed language. Kang says that Rust aims to help developers build reliable and efficient software, and it was designed for that purpose from the ground up. There are key features you'll hear about Rust, and in this article, Kang demonstrates that many of these features are exactly why Rust also happens to be great for embedded systems. Here are some examples:</p>
<ul><li>High performance: It's fast, with high memory utilization</li>
<li>Reliability: Memory errors can be eliminated during compilation</li>
<li>Productivity: Great documentation, a friendly compiler with useful error messages, and top-notch tooling. There's an integrated package manager and build tool, smart multi-editor support with auto-completion and type inspections, an auto-formatter, and more.</li>
</ul><p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Great Content</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=7016000000127cYAAQ">Free online course: RHEL technical overview</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=7016000000127cYAAQ">Learn Advanced Linux Commands</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Download Cheat Sheets</a></li>
<li><a href="https://opensource.com/alternatives?intcmp=7016000000127cYAAQ">Find an Open Source Alternative</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=7016000000127cYAAQ">Read Top Linux Content</a></li>
<li><a href="https://opensource.com/resources?intcmp=7016000000127cYAAQ">Check out open source resources</a></li>
</ul></div>
</div>
</div>
</div>

<h2>Why use Rust for embedded development?</h2>
<p>Rust is designed to guarantee both security and high performance. Embedded software can have problems, mostly due to memory. Rust is, in a way, a compiler-oriented language, so you can be sure that you're using memory safely while compiling. Here are some of the benefits of using Rust to develop on embedded devices:</p>
<ul><li>Powerful static analysis</li>
<li>Flexible memory</li>
<li>Fearless concurrency</li>
<li>Interoperability</li>
<li>Portability</li>
<li>Community-driven</li>
</ul><p>In this article, I use the open source <a href="https://github.com/RT-Thread/rt-thread" target="_blank">RT-Thread operating system</a> to demonstrate how to use Rust for embedded development.</p>
<h2>How to call Rust in C</h2>
<p>When calling Rust code in C code, you must package the Rust source code as a static library file. When the C code compiles, link it in.</p>
<h3>Creating a static library with Rust</h3>
<p>There are two steps in this process.</p>
<p>1. Use <code>cargo init --lib rust_to_c </code>to build a lib library in Clion. Add the following code to the <code>lib.rs</code>. The following function evaluates the sum of two values of type <strong>i32</strong> and returns the result:</p>
<pre><code class="language-rust">#![no_std]
use core::panic::PanicInfo;

#[no_mangle]
pub extern "C" fn sum(a: i32, b: i32) -&gt; i32 {
    a + b
}

#[panic_handler]
fn panic(_info:&amp;PanicInfo) -&gt; !{
    loop{}
}</code></pre><p>2. Add the following code to your <code>Cargo.toml</code> file to tell Rustc what type of library to generate:</p>
<pre><code class="language-text">[lib]
name = "sum"
crate-type = ["staticlib"]
path = "src/lib.rs"</code></pre><h3>Cross-compilation</h3>
<p>You can cross-compile for your target. Assuming your embedded system is Arm-based, the steps are simple:</p>
<pre><code class="language-bash">$ rustup target add armv7a-none-eabi</code></pre><p>2. Generate the static library file:</p>
<pre><code class="language-bash">$ cargo build --target=armv7a-none-eabi --release --verbose
Fresh rust_to_c v0.1.0
Finished release [optimized] target(s) in 0.01s</code></pre><h3>Generate Header File</h3>
<p>You need header files, too.</p>
<p>1. Install <a href="https://github.com/eqrion/cbindgen" target="_blank">cbindgen</a>. The <code>cbindgen</code> tool generates a C or C++11 header file from the Rust library:</p>
<pre><code class="language-bash">$ cargo install --force cbindgen</code></pre><p>2. Create a new <code>cbindgen.toml</code> file under your project folder.</p>
<p>3. Generate a header file:</p>
<pre><code class="language-bash">$ cbindgen --config cbindgen.toml --crate rust_to_c --output sum.h</code></pre><h3>Call the Rust library file</h3>
<p>Now you can make calls to your Rust libraries.</p>
<p>1. Put the generated <code>sum.h</code> and <code>sum.a</code> files into the <code>rt-thread/bsp/qemu-vexpress-a9/applications</code> directory.</p>
<p>2. Modify the <code>SConscript</code> file and add a static library:</p>
<pre><code class="language-rust">   from building import *
   
   cwd     = GetCurrentDir()
   src     = Glob('*.c') + Glob('*.cpp')
   CPPPATH = [cwd]
   
   LIBS = ["libsum.a"]
   LIBPATH = [GetCurrentDir()]
   
   group = DefineGroup('Applications', src, depend = [''], CPPPATH = CPPPATH, LIBS = LIBS, LIBPATH = LIBPATH)
   
   Return('group')</code></pre><p>3. Call the <strong>sum</strong> function in the main function, get the return value, and <code>printf</code> the value.</p>
<pre><code class="language-rust">   #include &lt;stdint.h&gt;
   #include &lt;stdio.h&gt;
   #include &lt;stdlib.h&gt;
   #include &lt;rtthread.h&gt;
   #include "sum.h"
   
   int main(void)
   {
       int32_t tmp;
   
       tmp = sum(1, 2);
       printf("call rust sum(1, 2) = %d\n", tmp);
   
       return 0;
   }</code></pre><p>4. In the RT-Thread <a href="https://www.rt-thread.io/download.html?download=Env" target="_blank">Env</a> environment, use <code>scons</code> to compile the project and run:</p>
<pre><code class="language-bash">$ scons -j6
scons: Reading SConscript files ...
scons: done reading SConscript files.
scons: Building targets ...
[...]
scons: done building targets.

$ qemu.sh
 \ | /
- RT -     Thread Operating System
 / | \     4.0.4 build Jul 28 2021
2006 - 2021 Copyright by rt-thread team
lwIP-2.1.2 initialized!
[...]
call rust sum(1, 2) = 3</code></pre><h2>Add, subtract, multiply, and divide</h2>
<p>You can implement some complicated math in Rust. In the <code>lib.rs</code> file, use the Rust language to implement add, subtract, multiply, and divide:</p>
<pre><code class="language-rust">#![no_std]
use core::panic::PanicInfo;

#[no_mangle]
pub extern "C" fn add(a: i32, b: i32) -&gt; i32 {
    a + b
}

#[no_mangle]
pub extern "C" fn subtract(a: i32, b: i32) -&gt; i32 {
    a - b
}

#[no_mangle]
pub extern "C" fn multiply(a: i32, b: i32) -&gt; i32 {
    a * b
}

#[no_mangle]
pub extern "C" fn divide(a: i32, b: i32) -&gt; i32 {
    a / b
}

#[panic_handler]
fn panic(_info:&amp;PanicInfo) -&gt; !{
    loop{}
}</code></pre><p>Build your library files and header files and place them in the application directory. Use <code>scons</code> to compile. If errors appear during linking, find the solution on the official <a href="https://github.com/rust-lang/compiler-builtins/issues/353" target="_blank">Github page</a>.</p>
<p>Modify the <code>rtconfig.py</code> file, and add the link parameter <code>--allow-multiple-definition</code>:</p>
<pre><code class="language-rust">       DEVICE = ' -march=armv7-a -marm -msoft-float'
       CFLAGS = DEVICE + ' -Wall'
       AFLAGS = ' -c' + DEVICE + ' -x assembler-with-cpp -D__ASSEMBLY__ -I.'
       LINK_SCRIPT = 'link.lds'
       LFLAGS = DEVICE + ' -nostartfiles -Wl,--gc-sections,-Map=rtthread.map,-cref,-u,system_vectors,--allow-multiple-definition'+\
                         ' -T %s' % LINK_SCRIPT
   
       CPATH = ''
       LPATH = ''</code></pre><p>Compile and run QEMU to see your work.</p>
<h2>Call C in Rust</h2>
<p>Rust can be called in C code, but what about calling C in your Rust code? The following is an example of calling the <code>rt_kprintf</code> C function in Rust code.</p>
<p>First, modify the <code>lib.rs</code> file:</p>
<pre><code class="language-rust">    // The imported rt-thread functions list
    extern "C" {
        pub fn rt_kprintf(format: *const u8, ...);
    }
    
    #[no_mangle]
    pub extern "C" fn add(a: i32, b: i32) -&gt; i32 {
        unsafe {
            rt_kprintf(b"this is from rust\n" as *const u8);
        }
        a + b
    }</code></pre><p>Next, generate the library file:</p>
<pre><code class="language-bash">$ cargo build --target=armv7a-none-eabi --release --verbose
Compiling rust_to_c v0.1.0
Running `rustc --crate-name sum --edition=2018 src/lib.rs --error-format=json --json=diagnostic-rendered-ansi --crate-type staticlib --emit=dep-info,link -C opt-level=3 -C embed-bitcode=no -C metadata=a
Finished release [optimized] target(s) in 0.11s</code></pre><p>And now, to run the code, copy the library files generated by Rust into the application directory and rebuild:</p>
<pre><code class="language-bash">$ scons -j6 scons: Reading SConscript files ... scons: done reading SConscript files. [...]
scons: Building targets ... scons: done building targets.</code></pre><p>Run QEMU again to see the results in your embedded image.</p>
<h2>You can have it all</h2>
<p>Using Rust for your embedded development gives you all the features of Rust without the need to sacrifice flexibility or stability. Try Rust on your embedded system today. For more information about the process of embedded Rust (and about RT-Thread itself), check out the RT-Thread project's <a href="https://www.youtube.com/channel/UCdDHtIfSYPq4002r27ffqP" target="_blank">YouTube channel</a>. And remember, embedded can be open, too.</p>
<hr /><p>Special thanks to Liu Kang for providing this article, and for his tireless work on making embedded programming easy for everyone!</p>
