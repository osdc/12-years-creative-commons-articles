<p>Depending on whom you ask, <i>functional programming</i> (FP) is either an enlightened approach to programming that should be spread far and wide, or an overly academic approach to programming with few real-world benefits. In this article, I will explain what functional programming is, explore its benefits, and recommend resources for learning functional programming.</p>
<h2>Syntax primer</h2>
<p>Code examples in this article are in the <a href="https://wiki.haskell.org/Introduction" target="_blank">Haskell</a> programming language. All that you need to understand for this article is the basic function syntax:</p>
<pre>
<code class="language-haskell">  even :: Int -&gt; Bool
  even = ...    -- implementation goes here</code></pre><p>This defines a one-argument function named <b>even</b>. The first line is the <i>type declaration</i>, which says that <b>even</b> takes an <b>Int</b> and returns a <b>Bool</b>. The implementation follows and consists of one or more <i>equations</i>. We'll ignore the implementation (the name and type tell us enough):</p>
<pre>
<code class="language-haskell">  map :: (a -&gt; b) -&gt; [a] -&gt; [b]
  map = ...</code></pre><p>In this example, <b>map</b> is a function that takes two arguments:</p>
<ol><li><b>(a -&gt; b)</b>: a functions that turns an <b>a</b> into a <b>b</b></li>
<li><b>[a]</b>: a list of <b>a</b></li>
</ol><p>and returns a list of <b>b</b>. Again, we don't care about the definition—the type is more interesting! <b>a</b> and <b>b</b> are <i>type variables</i> that could stand for any type. In the expression below, <b>a</b> is <b>Int</b> and <b>b</b> is <b>Bool</b>:</p>
<pre>
<code class="language-haskell">  map even [1,2,3]</code></pre><p>It evaluates to a <b>[Bool]</b>:</p>
<pre>
<code class="language-haskell">  [False,True,False]</code></pre><p>If you see other syntax that you do not understand, don't panic; full comprehension of the syntax is not essential.</p>
<h2>Myths about functional programming</h2>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Programming and development</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/?intcmp=7016000000127cYAAQ&amp;src=programming_resource_menu4">Red Hat Developers Blog</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7016000000127cYAAQ">Programming cheat sheets</a></li>
<li><a href="https://www.redhat.com/en/services/training/learning-subscription?intcmp=7016000000127cYAAQ">Try for free: Red Hat Learning Subscription</a></li>
<li><a href="https://opensource.com/downloads/bash-programming-guide?intcmp=7016000000127cYAAQ">eBook: An introduction to programming with Bash</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/bash-shell-cheat-sheet?intcmp=7016000000127cYAAQ">Bash Shell Scripting Cheat Sheet</a></li>
<li><a href="https://developers.redhat.com/e-books/modernizing-enterprise-java?intcmp=7016000000127cYAAQ">eBook: Modernizing Enterprise Java</a></li>
</ul></div>
</div>
</div>
</div>

<p>Let's begin by dispelling common misconceptions:</p>
<ul><li>Functional programming is not the rival or antithesis of imperative or object-oriented programming. This is a false dichotomy.</li>
<li>Functional programming is not just the domain of academics. It is true that the history of functional programming is steeped in academia, and languages such as like Haskell and OCaml are popular research languages. But today many companies use functional programming for large-scale systems, small specialized programs, and everything in between. There's even an annual conference for <a href="http://cufp.org/" target="_blank">Commercial Users of Functional Programming</a>; past programs give an insight into how functional programming is being used in industry, and by whom.</li>
<li>Functional programming has nothing to do with <a href="https://www.haskell.org/tutorial/monads.html" target="_blank">monads</a>, nor any other particular abstraction. For all the hand-wringing around this topic, monad is just an abstraction with laws. Some things are monads, others are not.</li>
<li>Functional programming is not especially hard to learn. Some languages may have different syntax or evaluation semantics from those you already know, but these differences are superficial. There are dense concepts in functional programming, but this is also true of other approaches.</li>
</ul><h2>What is functional programming?</h2>
<p>At its core, functional programming is just programming with functions—<i>pure</i> mathematical functions. The result of a function depends only on the arguments, and there are no side effects, such as I/O or mutation of state. Programs are built by combining functions together. One way of combining functions is <i>function composition</i>:</p>
<pre>
<code class="language-haskell">  (.) :: (b -&gt; c) -&gt; (a -&gt; b) -&gt; (a -&gt; c)
  (g . f) x = g (f x)</code></pre><p>This <i>infix</i> function combines two functions into one, applying <b>g</b> to the output of <b>f</b>. We'll see it used in an upcoming example. For comparison, the same function in Python looks like:</p>
<pre>
<code class="language-python">  def compose(g, f):
    return lambda x: g(f(x))</code></pre><p>The beauty of functional programming is that because functions are deterministic and have no side effects, you can always replace a function application with the result of the application. This substitution of equals for equals enables <i>equational reasoning</i>. Every programmer has to reason about their code and others', and equational reasoning is a great tool for doing that. Let's look at an example. You encounter the expression:</p>
<pre>
<code class="language-haskell">  map even . map (+1)</code></pre><p>What does this program do? Can it be simplified? Equational reasoning lets you analyze the code through a series of substitutions:</p>
<pre>
<code class="language-haskell">  map even . map (+1)
  map (even . (+1))         -- from definition of 'map'
  map (\x -&gt; even (x + 1))  -- lambda abstraction
  map odd                   -- from definition of 'even'</code></pre><p>We can use equational reasoning to understand programs and optimize for readability. The Haskell compiler uses equational reasoning to perform many kinds of program optimizations. Without pure functions, equational reasoning either isn't possible, or requires an inordinate effort from the programmer.</p>
<h2>Functional programming languages</h2>
<p>What do you need from a programming language to be able to do functional programming?</p>
<p>Doing functional programming meaningfully in a language without <i>higher-order functions</i> (the ability to pass functions as arguments and return functions), <i>lambdas</i> (anonymous functions), and <i>generics</i> is difficult. Most modern languages have these, but there are differences in <i>how well</i> different languages support functional programming. The languages with the best support are called <i>functional programming languages</i>. These include <em>Haskell</em>, <em>OCaml</em>, <em>F#</em>, and <em>Scala</em>, which are statically typed, and the dynamically typed <i>Erlang</i> and <i>Clojure</i>.</p>
<p>Even among functional languages there are big differences in how far you can exploit functional programming. Having a type system helps a lot, especially if it supports <i>type inference</i> (so you don't always have to type the types). There isn't room in this article to go into detail, but suffice it to say, not all type systems are created equal.</p>
<p>As with all languages, different functional languages emphasize different concepts, techniques, or use cases. When choosing a language, considering how well it supports functional programming and whether it fits your use case is important. If you're stuck using some non-FP language, you will still benefit from applying functional programming to the extent the language supports it.</p>
<h2>Don't open that trap door!</h2>
<p>Recall that the result of a function depends only on its inputs. Alas, almost all programming languages have "features" that break this assumption. Null values, type case (<b>instanceof</b>), type casting, exceptions, side-effects, and the possibility of infinite recursion are trap doors that break equational reasoning and impair a programmer's ability to reason about the behavior or correctness of a program. (<i>Total languages</i>, which do not have any trap doors, include Agda, Idris, and Coq.)</p>
<p>Fortunately, as programmers, we can choose to avoid these traps, and if we are disciplined, we can pretend that the trap doors do not exist. This idea is called <i>fast and loose reasoning</i>. It costs nothing—almost any program can be written without using the trap doors—and by avoiding them you win back equational reasoning, composability and reuse.</p>
<p>Let's discuss exceptions in detail. This trap door breaks equational reasoning because the possibility of abnormal termination is not reflected in the type. (Count yourself lucky if the documentation even mentions the exceptions that could be thrown.) But there is no reason why we can't have a return type that encompasses all the failure modes.</p>
<p>Avoiding trap doors is an area in which language features can make a big difference. For avoiding exceptions, <i>algebraic data types</i> can be used to model error conditions, like so:</p>
<pre>
<span style="font-size:small;"><code class="language-haskell">  -- new data type for results of computations that can fail
  --
  data Result e a = Error e | Success a

  -- new data type for three kinds of arithmetic errors
  --
  data ArithError = DivByZero | Overflow | Underflow

  -- integer division, accounting for divide-by-zero
  --
  safeDiv :: Int -&gt; Int -&gt; Result ArithError Int
  safeDiv x y =
    if y == 0
      then Error DivByZero
      else Success (div x y)</code></span></pre><p>The trade-off in this example is that you must now work with values of type <b>Result ArithError Int</b> instead of plain old <b>Int</b>, but there are abstractions for dealing with this. You no longer need to handle exceptions and can use fast and loose reasoning, so overall it's a win.</p>
<h2>Theorems for free</h2>
<p>Most modern statically typed languages have <i>generics</i> (also called <i>parametric polymorphism</i>), where functions are defined over one or more abstract types. For example, consider a function over lists:</p>
<pre>
<code class="language-haskell">  f :: [a] -&gt; [a]
  f = ...</code></pre><p>The same function in Java looks like:</p>
<pre>
<code class="language-java">  static &lt;A&gt; List&lt;A&gt; f(List&lt;A&gt; xs) { ... }</code></pre><p>The compiled program is a proof that this function will work with <i>any</i> choice for the type <b>a</b>. With that in mind, and employing fast and loose reasoning, can you work out what the function does? Does knowing the type help?</p>
<p>In this case, the type doesn't tell us exactly what the function does (it could reverse the list, drop the first element, or many other things), but it does tell us a lot. Just from the type, we can derive theorems about the function:</p>
<ul><li><b>Theorem 1</b>: Every element in the output appears in the input; it couldn't possibly add an <b>a</b> to the list because it has no knowledge of what <b>a</b> is or how to construct one.</li>
<li><b>Theorem 2</b>: If you map any function over the list then apply <b>f</b>, the result is the same as applying <b>f</b> then mapping.</li>
</ul><p>Theorem 1 helps us understand what the code is doing, and Theorem 2 is useful for program optimization. We learned all this just from the type! This result—the ability to derive useful theorems from types—is called <i>parametricity</i>. It follows that a type is a partial (sometimes complete) specification of a function's behavior, and a kind of machine-checked documentation.</p>
<p>Now it's your turn to exploit parametricity. What can you conclude from the types of <b>map</b> and <b>(.)</b>, or the following functions?</p>
<ul><li><b>foo :: a -&gt; (a, a)</b></li>
<li><b>bar :: a -&gt; a -&gt; a</b></li>
<li><b>baz :: b -&gt; a -&gt; a</b></li>
</ul><h2>Resources for learning functional programming</h2>
<p>Perhaps you have been convinced that functional programming is a better way to write software, and you are wondering how to get started? There are several approaches to learning functional programming; here are some I recommend (with, I admit, a strong bias toward Haskell):</p>
<ul><li>UPenn's <a href="https://www.cis.upenn.edu/~cis194/fall16/" target="_blank">CIS 194: Introduction to Haskell</a> is a solid introduction to functional programming concepts and real-world Haskell development. The course material is available, but the lectures are not (you could view Brisbane Functional Programming Group's <a href="https://github.com/bfpg/cis194-yorgey-lectures" target="_blank">series of talks covering CIS 194</a> from a few years ago instead).</li>
<li>Good introductory books include <i><a href="https://www.manning.com/books/functional-programming-in-scala" target="_blank">Functional Programming in Scala</a></i>, <i><a href="http://www.cambridge.org/gb/academic/subjects/computer-science/programming-languages-and-applied-logic/thinking-functionally-haskell" target="_blank">Thinking Functionally with Haskell</a></i>, and <i><a href="http://haskellbook.com/" target="_blank">Haskell Programming from first principles</a></i>.</li>
<li>The <a href="https://github.com/data61/fp-course" target="_blank">Data61 FP course</a> (f.k.a., <i>NICTA</i> course) teaches foundational abstractions and data structures through <i>type-driven development</i>. The payoff is huge, but it is <i>difficult by design</i>, having its origins in training workshops, so only attempt it if you know a functional programmer who is willing to mentor you.</li>
<li>Start practicing functional programming in whatever code you're working on. Write pure functions (avoid non-determinism and mutation), use higher-order functions and recursion instead of loops, exploit parametricity for improved readability and reuse. Many people start out in functional programming by experimenting and experiencing the benefits in all kinds of languages.</li>
<li>Join a functional programming user group or study group in your area—or start one—and look out for functional programming conferences (new ones are popping up all the time).</li>
</ul><h2>Conclusion</h2>
<p>In this article, I discussed what functional programming is and is not, and looked at advantages of functional programming, including equational reasoning and parametricity. We learned that you can do <i>some</i> functional programming in most programming languages, but the choice of language affects how much you can benefit, with <i>functional programming languages</i>, such as Haskell, having the most to offer. I also recommended resources for learning functional programming.</p>
<p>Functional programming is a rich field and there are many deeper (and denser) topics awaiting exploration. I would be remiss not to mention a few that have practical implications, such as:</p>
<ul><li>lenses and prisms (first-class, composable getters and setters; great for working with nested data);</li>
<li>theorem proving (why test your code when you could <i>prove it correct</i> instead?);</li>
<li>lazy evaluation (lets you work with potentially infinite data structures);</li>
<li>and category theory (the origin of many beautiful and practical abstractions in functional programming).</li>
</ul><p>I hope that you have enjoyed this introduction to functional programming and are inspired to dive into this fun and practical approach to software development.</p>
<p><em>This article is published under the <a href="https://creativecommons.org/licenses/by/4.0/" target="_blank">CC BY 4.0</a> license.</em></p>
