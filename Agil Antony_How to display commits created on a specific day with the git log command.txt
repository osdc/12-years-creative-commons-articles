<p>The <code>git log</code> command offers many opportunities to learn more about the commits made by contributors. One way you might consume such information is by date. To view commits in a Git repository created on a specific date or range of dates, use the <code>git log</code> command with the options <code>--since</code> or <code>--until</code>, or both.</p>
<p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">More on Git</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://opensource.com/resources/what-is-git?intcmp=7013a000002CxqLAAS" data-analytics-category="resource list" data-analytics-text="What is Git?">What is Git?</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-git?intcmp=7013a000002CxqLAAS" data-analytics-category="resource list" data-analytics-text="Git cheat sheet">Git cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheet-markdown?intcmp=7013a000002CxqLAAS" data-analytics-category="resource list" data-analytics-text="Markdown cheat sheet">Markdown cheat sheet</a></div>
              <div class="field__item"><a href="https://opensource.com/downloads/git-tricks-tips?intcmp=7013a000002CxqLAAS" data-analytics-category="resource list" data-analytics-text="eBook: Git tips and tricks">eBook: Git tips and tricks</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Register for your free Red Hat account">Register for your free Red Hat account</a></div>
              <div class="field__item"><a href="https://opensource.com/tags/git?intcmp=7013a000002CxqLAAS" data-analytics-category="resource list" data-analytics-text="New Git articles">New Git articles</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<p>First, checkout the branch you want to inspect (for example, <code>main</code>):</p>
<pre>
<code class="language-bash">$ git checkout main</code></pre><p>Next, display the commits for the current date (today):</p>
<pre>
<code class="language-bash">$ git log --oneline --since="yesterday"</code></pre><p>Display commits for the current date by a specific author only (for example, <code>Agil</code>):</p>
<pre>
<code class="language-bash">$ git log --oneline --since="yesterday" --author="Agil"</code></pre><p>You can also display results for a range of dates. Display commits between any two dates (for example, 22 April 2022 and 24 April 2022):</p>
<pre>
<code class="language-bash">$ git log --oneline --since="2022-04-22" --until="2022-04-24"</code></pre><p>In this example, the output displays all the commits between 22 April 2022 and 24 April 2022, which excludes the commits done on 22 April 2022. If you want to include the commits done on 22 April 2022, replace <code>2022-04-22</code> with <code>2022-04-21</code>.</p>
<p>Run the following command to display commits between any two dates by a specific author only (for example, <code>Agil</code>):</p>
<pre>
<code class="language-bash">$ git log --oneline --since="2022-04-22" \
--until="2022-04-24" --author="Agil"</code></pre><h2>Reporting</h2>
<p>Git has many advantages, and one of them is the way it enables you to gather data about your project. The <code>git log</code> command is an important reporting tool and yet another reason to use Git!</p>
