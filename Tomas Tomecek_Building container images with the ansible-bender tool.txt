<p>Containers and <a href="https://www.ansible.com/" target="_blank">Ansible</a> blend together so nicely—from management and orchestration to provisioning and building. In this article, we'll focus on the building part.</p>
<p>If you are familiar with Ansible, you know that you can write a series of tasks, and the <strong>ansible-playbook</strong> command will execute them for you. Did you know that you can also execute such commands in a container environment and get the same result as if you'd written a Dockerfile and run <strong>podman build</strong>.</p>
<p>Here is an example:</p>
<pre><code class="language-yaml">- name: Serve our file using httpd
  hosts: all
  tasks:
  - name: Install httpd
    package:
      name: httpd
      state: installed
  - name: Copy our file to httpd’s webroot
    copy:
      src: our-file.txt
      dest: /var/www/html/</code></pre><p>You could execute this playbook locally on your web server or in a container, and it would work—as long as you remember to create the <strong>our-file.txt</strong> file first.</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on Ansible</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://opensource.com/downloads/ansible-quickstart?intcmp=701f2000000h4RcAAI">A quickstart guide to Ansible</a></li>
<li><a href="https://opensource.com/downloads/ansible-cheat-sheet?intcmp=701f2000000h4RcAAI">Ansible cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/do007-ansible-essentials-simplicity-automation-technical-overview?intcmp=701f2000000h4RcAAI">Free online course: Ansible essentials</a></li>
<li><a href="https://docs.ansible.com/ansible/latest/intro_installation.html?intcmp=701f2000000h4RcAAI">Download and install Ansible</a></li>
<li><a href="https://go.redhat.com/automated-enterprise-ebook-20180920?intcmp=701f2000000h4RcAAI">eBook: The automated enterprise</a></li>
<li><a href="https://www.ansible.com/resources/ebooks/ansible-for-devops?intcmp=701f2000000h4RcAAI">eBook: Ansible for DevOps</a></li>
<li><a href="https://www.ansible.com/resources/ebooks?intcmp=701f2000000h4RcAAI">Free Ansible eBooks</a></li>
<li><a href="https://opensource.com/tags/ansible?intcmp=701f2000000h4RcAAI">Latest Ansible articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>But something is missing. You need to start (and configure) httpd in order for your file to be served. This is a difference between container builds and infrastructure provisioning: When building an image, you just prepare the content; running the container is a different task. On the other hand, you can attach metadata to the container image that tells the command to run by default.</p>
<p>Here's where a tool would help. How about trying <strong>ansible-bender</strong>?</p>
<pre><code class="language-bash">$ ansible-bender build the-playbook.yaml fedora:30 our-httpd</code></pre><p>This script uses the ansible-bender tool to execute the playbook against a Fedora 30 container image and names the resulting container image <strong>our-httpd</strong>.</p>
<p>But when you run that container, it won't start httpd because it doesn't know how to do it. You can fix this by adding some metadata to the playbook:</p>
<pre><code class="language-yaml">- name: Serve our file using httpd
  hosts: all
  vars:
    ansible_bender:
      base_image: fedora:30
      target_image:
        name: our-httpd
        cmd: httpd -DFOREGROUND
  tasks:
  - name: Install httpd
    package:
      name: httpd
      state: installed
  - name: Listen on all network interfaces. 
    lineinfile:      
      path: /etc/httpd/conf/httpd.conf  
      regexp: '^Listen ' 
      line: Listen 0.0.0.0:80   
  - name: Copy our file to httpd’s webroot
    copy:
      src: our-file.txt
      dest: /var/www/html</code></pre><p>Now you can build the image (from here on, please run all the commands as root—currently, Buildah and Podman won't create dedicated networks for rootless containers):</p>
<pre><code class="language-yaml"># ansible-bender build the-playbook.yaml
PLAY [Serve our file using httpd] ****************************************************
                                                                                                                                                                             
TASK [Gathering Facts] ***************************************************************    
ok: [our-httpd-20191004-131941266141-cont]

TASK [Install httpd] *****************************************************************
loaded from cache: 'f053578ed2d47581307e9ba3f64f4b4da945579a082c6f99bd797635e62befd0'
skipping: [our-httpd-20191004-131941266141-cont]

TASK [Listen on all network interfaces.] *********************************************
changed: [our-httpd-20191004-131941266141-cont]

TASK [Copy our file to httpd’s webroot] **********************************************
changed: [our-httpd-20191004-131941266141-cont]

PLAY RECAP ***************************************************************************
our-httpd-20191004-131941266141-cont : ok=3    changed=2    unreachable=0    failed=0    skipped=1    rescued=0    ignored=0 

Getting image source signatures
Copying blob sha256:4650c04b851c62897e9c02c6041a0e3127f8253fafa3a09642552a8e77c044c8
Copying blob sha256:87b740bba596291af8e9d6d91e30a01d5eba9dd815b55895b8705a2acc3a825e
Copying blob sha256:82c21252bd87532e93e77498e3767ac2617aa9e578e32e4de09e87156b9189a0
Copying config sha256:44c6dc6dda1afe28892400c825de1c987c4641fd44fa5919a44cf0a94f58949f
Writing manifest to image destination
Storing signatures 
44c6dc6dda1afe28892400c825de1c987c4641fd44fa5919a44cf0a94f58949f
Image 'our-httpd' was built successfully \o/</code></pre><p>The image is built, and it's time to run the container:</p>
<pre><code class="language-yaml"># podman run our-httpd
AH00558: httpd: Could not reliably determine the server's fully qualified domain name, using 10.88.2.106. Set the 'ServerName' directive globally to suppress this message</code></pre><p>Is your file being served? First, find out the IP of your container:</p>
<pre><code class="language-yaml"># podman inspect -f '{{ .NetworkSettings.IPAddress }}' 7418570ba5a0
10.88.2.106</code></pre><p>And now you can check:</p>
<pre><code class="language-yaml">$ curl http://10.88.2.106/our-file.txt
Ansible is ❤</code></pre><p>What were the contents of your file?</p>
<p>This was just an introduction to building container images with Ansible. If you want to learn more about what ansible-bender can do, please check it out on <a href="https://github.com/ansible-community/ansible-bender" target="_blank">GitHub</a>. Happy building!</p>
