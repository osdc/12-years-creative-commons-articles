<p>Open source is one of the most important technology trends of our time. It’s the lifeblood of the digital economy and the preeminent way that software-based innovation happens today. In fact, it’s estimated that over 90% of software released today contains open source libraries.</p>
<p>There's no doubt the open source model is effective and impactful. But is there still room for improvement? When comparing the broader software industry’s processes to that of open source communities, one big gap stands out: productivity management.</p>
<p>By and large, open source project leads and maintainers have been slow to adopt modern productivity and project management practices and tools commonly embraced by startups and enterprises to drive the efficiency and predictability of software development processes. It’s time we examine how the application of these approaches and capabilities can improve the management of open source projects for the better.</p>
<h2>Understanding productivity in open source software development</h2>
<p>The open source model, at its heart, is community-driven. There is no single definition of success for different communities, so a one-size-fits-all approach to measuring success does not exist. And what we have traditionally thought of as productivity measures for software development, like commit velocity, the number of pull requests approved and merged, and even the lines of code delivered, only tell part of the story.</p>
<p>Open source projects are people-powered. We need to take a holistic and humanistic approach to measuring productivity that goes beyond traditional measures. I think this new approach should focus on the fact that great open source is about communication and coordination among a diverse community of contributors. The level of inclusivity, openness, and transparency within communities impacts how people feel about their participation, resulting in more productive teams.</p>
<p>These and other dimensions of what contributes to productivity on open source teams can be understood and measured with the SPACE framework, which was developed based on learnings from the proprietary world and research conducted by GitHub, the University of Victoria in Canada, and Microsoft. I believe that the SPACE framework has the potential to provide a balanced view of what is happening in open source projects, which would help to drive and optimize collaboration and participation among project team members.</p>
<p></p><div class="embedded-resource-list callout-float-right"> 
                                <a href="#bottom-list" class="visually-hidden focusable skip-link">Skip to bottom of list</a>    <div class="callout-float-right embedded-resource-list" data-analytics-region="sidebar">
            <div class="field field--name-title field--type-string field--label-hidden field__item">More open source career advice</div>
      
      <div class="field field--name-links field--type-link field--label-hidden field__items">
              <div class="field__item"><a href="https://opensource.com/downloads/cheat-sheets?intcmp=7013a000002DRawAAG" data-analytics-category="resource list" data-analytics-text="Open source cheat sheets">Open source cheat sheets</a></div>
              <div class="field__item"><a href="https://developers.redhat.com/learn/linux-starter-kit-for-developers/?intcmp=7013a000002DRawAAG" data-analytics-category="resource list" data-analytics-text="Linux starter kit for developers">Linux starter kit for developers</a></div>
              <div class="field__item"><a href="https://www.redhat.com/sysadmin/questions-for-employer?intcmp=7013a000002DRawAAG" data-analytics-category="resource list" data-analytics-text="7 questions sysadmins should ask a potential employer before taking a job">7 questions sysadmins should ask a potential employer before taking a job</a></div>
              <div class="field__item"><a href="https://www.redhat.com/architect/?intcmp=7013a000002DRawAAG" data-analytics-category="resource list" data-analytics-text="Resources for IT architects">Resources for IT architects</a></div>
              <div class="field__item"><a href="https://enterprisersproject.com/article/2019/3/article-cheat-sheet?intcmp=7013a000002DRawAAG" data-analytics-category="resource list" data-analytics-text="Cheat sheet: IT job interviews">Cheat sheet: IT job interviews</a></div>
              <div class="field__item"><a href="https://console.redhat.com?intcmp=7016000000127cYAAQ" data-analytics-category="resource list" data-analytics-text="Register for your free Red Hat account">Register for your free Red Hat account</a></div>
          </div>
  </div>
</div>
                                <a id="bottom-list" tabindex="-1"></a>
<h2>A more accurate productivity framework</h2>
<p>The SPACE framework acronym stands for:</p>
<ul><li><strong>S</strong>atisfaction and well-being</li>
<li><strong>P</strong>erformance</li>
<li><strong>A</strong>ctivity</li>
<li><strong>C</strong>ommunication and collaboration</li>
<li><strong>E</strong>fficiency and flow</li>
</ul><p>Satisfaction and well-being refer to how fulfilled developers feel with the team, their tools, and the environment, as well as how healthy and happy they are. Happiness is somewhat underrated as a factor in the success of teams. Still, there is strong evidence of a direct correlation between the way people feel and their productivity. In the open source industry, surveying contributors, committers, and maintainers about their attitudes, preferences, and priorities about what is being done and how is essential to understanding attitudes and opinions.</p>
<p>Performance in this context is about evaluating productivity in terms of the outcomes of processes instead of output. Team-level examples are code-review velocity (which captures the speed of reviews) and story points shipped. More holistic measures focus on quality and reliability. For example, was the code written in a way that ensures it will reliably do what it is supposed to do? Are there a lot of bugs in the software? Is industry adoption of the software growing?</p>
<p>Open source activity focuses on measuring design and development and CI/CD metrics, like build, test, deployments, releases, and infrastructure utilization. Example metrics for open source projects are the number of pull requests, commits, code reviews completed, build releases, and project documents created.</p>
<p>Communication and collaboration capture how people and teams work together, communicate, and coordinate efforts with high transparency and awareness within and between teams. Metrics in this area focus on the vibrancy of forums, as measured by the number of posts, messages, questions asked and answered, and project meetings held.</p>
<p>Finally, efficiency and flow refer to the ability to complete work and progress towards it with minimal interruptions and delays. At the individual developer level, this is all about getting into a flow that allows complex tasks to be completed with minimal distractions, interruptions, or context switching. At the project team level, this is about optimizing flow to minimize the delays and handoffs that take place in the steps needed to take software from an idea or feature request to being written into code. Metrics are built around process delays, handoffs, time on task, and the ease of project contributions and integrations.</p>
<h2>Applying the SPACE framework to open source teams</h2>
<p>Here are some sample metrics to illustrate how the SPACE framework could be used for an open source project.</p>
<h3>Satisfaction and well-being</h3>
<ul><li>Contributor satisfaction</li>
<li>Community sentiment</li>
<li>Community growth &amp; diversity</li>
</ul><h3>Performance</h3>
<ul><li>Code review velocity</li>
<li>Story points shipped</li>
<li>Absence of bugs</li>
<li>Industry adoption</li>
</ul><h3>Activity</h3>
<ul><li>number of pull requests</li>
<li>number of commits</li>
<li>number of code reviews</li>
<li>number of builds</li>
<li>number of releases</li>
<li>number of docs created</li>
</ul><h3>Communication and collaboration</h3>
<ul><li>Forum posts</li>
<li>Messages</li>
<li>Questions asked &amp; answered</li>
<li>Meetings</li>
</ul><h3>Efficiency and flow</h3>
<ul><li>Code review timing</li>
<li>Process delays &amp; handoffs</li>
<li>Ease of contributions/integration</li>
</ul><h2>Tools for managing open source projects must be fit for purpose</h2>
<p>There is an opportunity to leverage the tools and approaches startups and high-growth organizations use to understand and improve open source development efficiency. All while putting open source’s core tenets, like openness and transparency, into practice.</p>
<p>Tools used by open source teams should enable maintainers and contributors to be productive and successful, while allowing the projects to be open and welcoming to everyone, including developers who may work in multiple organizations and even competing companies. It is also critical to provide an excellent onboarding experience for new contributors and accelerate their time-to-understanding and time-to-contribution.</p>
<p>Tools for managing open source projects should transparently manage data and accurately reflect project progress based on where the collaboration happens: in the codebase and repositories. Open source teams should be able to see real-time updates based on updates to issues and pull requests. And, project leads and maintainers should have the flexibility to decide whether access to the project should be completely public or if it should be limited to trusted individuals for issues or information of a more sensitive nature.</p>
<p>Ideally, tools should allow self-governed project teams to streamline coordination, processes, and workflows and eliminate repetitive tasks through automation. This reduces human friction and empowers maintainers and contributors to focus on what really matters: contributing to the ecosystem or community and delivering releases faster and more reliably.</p>
<p>The tools teams use should also support collaboration from people wherever they are. Since open source teams work in a remote and asynchronous world, tools should be able to integrate everyone’s contributions wherever and whenever they occur. These efforts should be enabled by great documentation stored in a central and easily accessible place. And finally, the tools should enable continuous improvement based on the types of frameworks and measures of productivity outlined above.</p>
<p>Features that allow for increased transparency are especially important for open source projects. Tools should help keep community members aligned and working towards a common goal with a project roadmap that shows work is in flight, progress updates, and predicted end dates.</p>
<h2>Conclusion</h2>
<p>Open source projects are a benefit to us all, and as such, it benefits everyone to make the processes that exist within these projects as productive as possible.</p>
<p>By leveraging concepts like the SPACE framework and modern tools, we can ditch the spreadsheets and manual ways of tracking, measuring, and improving productivity. We can adapt approaches that power software development in the proprietary world and leverage modern tools that can help increase the quality, reliability, and predictability of open source software releases. Open source is far too important to leave to anything less.</p>
