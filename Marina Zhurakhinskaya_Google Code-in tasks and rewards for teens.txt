<p><a href="http://www.google-melange.com/gci/homepage/google/gci2014">Google Code-in</a> is an initiative for 13-17 year olds to get involved in open source software projects.</p>
<p>From December 1, 2014 to January 19, 2015, participating open source organizations identify small tasks for beginners that can take a few hours or a few days to complete, and they identify mentors for participants to work with. Tasks include coding, documentation, research, design, and testing. One doesn't need to know how to code to participate, though it's a great opportunity to learn to code too.</p>
<!--break-->
<p><img src="https://opensource.com/sites/default/files/resize/images/life-uploads/gci_2014_logo-350x169.jpg" alt="Google Code-in for teens" title="Google Code-in for teens" style="margin: 8px auto; vertical-align: middle; display: block;" class="media-element file-default" data-file_info="%7B%22fid%22:%22173659%22,%22view_mode%22:%22default%22,%22fields%22:%7B%22format%22:%22default%22,%22field_file_image_alt_text%5Bund%5D%5B0%5D%5Bvalue%5D%22:%22Google%20Code-in%20for%20teens%22,%22field_file_image_title_text%5Bund%5D%5B0%5D%5Bvalue%5D%22:%22Google%20Code-in%20for%20teens%22,%22field_file_image_caption%5Bund%5D%5B0%5D%5Bvalue%5D%22:%22%3Cp%3EPhoto%20courtesy%20of%20Google%20Code-in%3C/p%3E%22,%22field_file_image_caption%5Bund%5D%5B0%5D%5Bformat%5D%22:%22panopoly_wysiwyg_text%22,%22field_folder%5Bund%5D%22:%229402%22%7D,%22type%22:%22media%22%7D" width="350" height="169" /></p>
<p>How it works:</p>
<ol><li>Pick a task</li>
<li>Complete the task</li>
<li>Your task is approved</li>
<li>Repeat!</li>
</ol><p>The <a href="http://www.google-melange.com/gci/org/list/public/google/gci2014">participating organizations</a> are:</p>
<ul><li><a href="http://www.apertium.org/">Apertium</a> - platform for rule-based machine translations</li>
<li><a href="http://brlcad.org/">BRL-CAD</a> - 3D solid modelling computer-aided design</li>
<li><a href="http://www.copyleftgames.org/">Copyleft Games Group</a> - visual effects, physics, and other libraries for games</li>
<li><a href="https://drupal.org/">Drupal</a> - popular content management system used to build websites</li>
<li><a href="http://fossasia.org/">FOSSASIA</a> - technology for social change with a focus on Asian users</li>
<li><a href="https://www.haiku-os.org/">Haiku</a> - fast and simple operating system</li>
<li><a href="http://www.kde.org/">KDE</a> - powerful desktop and applications for Linux and Unix</li>
<li><a href="http://mifos.org/mifos-initiative/">Mifos Initiative</a> - financial services to the poor, such as microfinancing</li>
<li><a href="http://openmrs.org/">OpenMRS</a> - medical records system used in many developing countries</li>
<li><a href="http://sahanafoundation.org/">Sahana Software Foundation</a> - disaster relief management software</li>
<li><a href="http://www.sugarlabs.org/">Sugar Labs</a> - educational software for children, such as Scratch</li>
<li><a href="http://wikimediafoundation.org/wiki/Home">Wikimedia Foundation</a> - software behind Wikipedia</li>
</ul><p>Each organization will select its top five participants as finalists and its top two to win the grand prize of a trip to Google headquarters in California, USA with a parent or a guardian. Everyone who completes at least three tasks will get a T-shirt.</p>
<p>Google Code-in is an excellent opportunity for teenagers to gain experience working on real-world software projects and connect with an international community. <a href="http://google-opensource.blogspot.com/2014/02/google-code-in-2013-numbers-numbers.html">Last year</a>, 337 participants from 46 countries completed 2,113 tasks in a span of seven weeks!<br /><br />If you are a teenager reading this, jump right in and tell your friends! Alternatively, encourage a teenager in your life to participate or spead the word to parents and educators you know. You can bring the <a href="https://developers.google.com/open-source/gci/resources/flyers">available flyer</a> to your classroom or your coding club.</p>
