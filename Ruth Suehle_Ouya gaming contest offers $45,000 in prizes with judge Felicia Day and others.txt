<p>Last fall, 63,416 people donated $8.5 million to help the creation of an Android-based, open source gaming console. Ouya's Kickstarter project had donations of $95 or more from 58,211 people who wanted the first shot at this console that didn't yet exist. (If you weren't one of them, you can pre-order them now at <a href="http://www.ouya.tv/">ouya.tv</a>.)</p>
<!--break-->
<p>To celebrate Ouya's official release (dev kits began shipping in December), <a title="Kill Screen Daily" href="http://www.killscreendaily.com" target="_blank">Kill Screen</a> is sponsoring a 10-day game jam called <a href="http://www.killscreendaily.com/create/">CREATE</a> to get developers to create some great games very quickly.</p>
<p>They've got a killer slate of judges ready and waiting to award $45,000 in prizes:</p>
<ul><li>Felicia Day (<em>The Guild</em>, Geek and Sundry, other geek greatness)</li>
<li>Adam Saltsman (creator of Canabalt and Hundreds)</li>
<li>Phil Fish (creator of Fez)</li>
<li>Alexis Madrigal<em> (Atlantic Monthly</em>)</li>
<li>Austin Wintory (Grammy-nominated and Sundance Audience Award winner, composer of the scores for 300 games)</li>
<li>Ed Fries (co-creator, Xbox and OUYA advisor)</li>
<li>Joanne McNeil (former editor, Rhizome.org and writer)</li>
<li>Zach Gage (creator of SpellTower and artist)</li>
</ul><p>Developers anywhere in the wolrd are welcome to submit playable prototypes (not necessarily complete games, but playable) from January 14 - 23. You'll also need to submit a YouTube video showing it off for those who don't get to play it.</p>
<p>The grand prize is $20,000, but every finalist gets an Ouya dev console and an invite to an Ouya unveiling attempt. Finalists in each category will be announced February 11 and winners on  February 18.</p>
<p>Ready to get started? You do need to pre-order an Ouya first or have backed the Kickstarter. Then head over to the <a href="https://devs.ouya.tv/developers">Ouya developer portal</a> and start creating. (You can get started now, but full contest details won't be posted until the competition begins.)</p>
<p>Good luck!</p>
