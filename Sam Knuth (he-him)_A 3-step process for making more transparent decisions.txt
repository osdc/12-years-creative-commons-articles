<p>One of the most powerful ways to make your work as a leader more transparent is to take an existing process, open it up for feedback from your team, and then change the process to account for this feedback. The following exercise makes transparency more tangible, and it helps develop the "muscle memory" needed for continually evaluating and adjusting your work with transparency in mind.</p>
<p>I would argue that you can undertake this activity this with any process—even processes that might seem "off limits," like the promotion or salary adjustment processes. But if that's too big for a first bite, then you might consider beginning with a less sensitive process, such as the travel approval process or your system for searching for candidates to fill open positions on your team. (I've done this with our hiring process and promotion processes, for example.)</p>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>Learn about open organizations</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://theopenorganization.org">Download resources</a></li>
<li><a href="https://theopenorganization.community">Join the community</a></li>
<li><a href="https://opensource.com/open-organization/resources/open-org-definition?src=too_resource_menu3a">What is an open organization?</a></li>
<li><a href="https://opensource.com/open-organization/resources/open-org-maturity-model?src=too_resource_menu4a">How open is your organization?</a></li>
</ul></div>
</div>
</div>
</div>

<p>Opening up processes and making them more transparent builds your credibility and enhances trust with team members. It forces you to "walk the transparency walk" in ways that might challenge your assumptions or comfort level. Working this way does create additional work, particularly at the beginning of the process—but, ultimately, this works well for holding managers (like me) accountable to team members, and it creates more consistency.</p>
<h2>Phase 1: Pick a process</h2>
<p><strong>Step 1.</strong> Think of a common or routine process your team uses, but one that is not generally open for scrutiny. Some examples might include:</p>
<ul><li>Hiring: How are job descriptions created, interview teams selected, candidates screened and final hiring decisions made?</li>
<li>Planning: How are your team or organizational goals determined for the year or quarter?</li>
<li>Promotions: How do you select candidates for promotion, consider them, and decide who gets promoted?</li>
<li>Manager performance appraisals: Who receives the opportunity to provide feedback on manager performance, and how are they able to do it?</li>
<li>Travel: How is the travel budget apportioned, and how do you make decisions about whether to approval travel (or whether to nominate someone for travel)?</li>
</ul><p>One of the above examples may resonate with you, or you may identify something else that you feel is more appropriate. Perhaps you've received questions about a particular process, or you find yourself explaining the rationale for a particular kind of decision frequently. Choose something that you are able to control or influence—and something you believe your constituents care about.</p>
<p><strong>Step 2.</strong> Now answer the following questions about the process:</p>
<ul><li>Is the process currently documented in a place that all constituents know about and can access? If not, go ahead and create that documentation now (it doesn't have to be too detailed; just explain the different steps of the process and how it works). You may find that the process isn't clear or consistent enough to document. In that case, document it the way you <em>think</em> it should work in the ideal case.</li>
<li>Does the completed process documentation explain how decisions are made at various points? For example, in a travel approval process, does it explain how a decision to approve or deny a request is made?</li>
<li>What are the <em>inputs</em> of the process? For example, when determining departmental goals for the year, what data is used for key performance indicators? Whose feedback is sought and incorporated? Who has the opportunity to review or "sign off"?</li>
<li>What <em>assumptions</em> does this process make? For example, in promotion decisions, do you assume that all candidates for promotion will be put forward by their managers at the appropriate time?</li>
<li>What are the <em>outputs </em>of the process? For example, in assessing the performance of the managers, is the result shared with the manager being evaluated? Are any aspects of the review shared more broadly with the manager's direct reports (areas for improvement, for example)?</li>
</ul><p>Avoid making judgements when answering the above questions. If the process doesn't clearly explain how a decision is made, that might be fine. The questions are simply an opportunity to assess the current state.</p>
<p>Next, revise the documentation of the process until you are satisfied that it adequately explains the process and anticipates the potential questions.</p>
<h2>Phase 2: Gather feedback</h2>
<p>The next phase involves sharing the process with your constituents and asking for feedback. Sharing is easier said than done.</p>
<p><strong>Step 1.</strong> Encourage people to provide feedback. Consider a variety of mechanisms for doing this:</p>
<ul><li>Post the process somewhere people can find it internally and note where they can make comments or provide feedback. A Google document works great with the ability to comment on specific text or suggest changes directly in the text.</li>
<li>Share the process document via email, inviting feedback</li>
<li>Mention the process document and ask for feedback during team meetings or one-on-one conversations</li>
<li>Give people a time window within which to provide feedback, and send periodic reminders during that window.</li>
</ul><p>If you don't get much feedback, don't assume that silence is equal to endorsement. Try asking people directly if they have any idea why feedback is not coming in. Are people too busy? Is the process not as important to people as you thought? Have you effectively articulated what you're asking for?</p>
<p><strong>Step 2.</strong> Iterate. As you get feedback about the process, engage the team in revising and iterating on the process. Incorporate ideas and suggestions for improvement, and ask for confirmation that the intended feedback has been applied. If you don't agree with a suggestion, be open to the discussion and ask yourself why you don't agree and what the merits are of one method versus another.</p>
<p>Setting a timebox for collecting feedback and iterating is helpful to move things forward. Once feedback has been collected and reviewed, discussed and applied, post the final process for the team to review.</p>
<h2>Phase 3: Implement</h2>
<p>Implementing a process is often the hardest phase of the initiative. But if you've taken account of feedback when revising your process, people should already been anticipating it and will likely be more supportive. The documentation you have from the iterative process above is a great tool to keep you accountable on the implementation.</p>
<p><strong>Step 1.</strong> Review requirements for implementation. Many processes that can benefit from increased transparency simply require doing things a little differently, but you do want to review whether you need any other support (tooling, for example).</p>
<p><strong>Step 2.</strong> Set a timeline for implementation. Review the timeline with constituents so they know what to expect. If the new process requires a process change for others, be sure to provide enough time for people to adapt to the new behavior, and provide communication and reminders.</p>
<p><strong>Step 3.</strong> Follow up. After using the process for 3-6 months, check in with your constituents to see how it's going. Is the new process more transparent? More effective? More predictable? Do you have any lessons learned that could be used to improve the process further?</p>
