<p>Of all the <a href="https://opensource.com/resources/linux">Linux</a> commands out there (and there are many), the three most quintessential seem to be <code>sed</code>, <code>awk</code>, and <code>grep</code>. Maybe it's the arcane sound of their names, or the breadth of their potential use, or just their age, but when someone's giving an example of a "Linuxy" command, it's usually one of those three. And while <code>sed</code> and <code>grep</code> have several simple one-line standards, the less prestigious <code>awk</code> remains persistently prominent for being particularly puzzling.</p>
<p>You're likely to use <code>sed</code> for a quick string replacement or <code>grep</code> to filter for a pattern on a daily basis. You're far less likely to compose an <code>awk</code> command. I often wonder why this is, and I attribute it to a few things. First of all, many of us barely use <code>sed</code> and <code>grep</code> for anything but some variation upon these two commands:</p>
<pre><code class="language-bash">$ sed -e 's/foo/bar/g' file.txt
$ grep foo file.txt</code></pre><p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Linux resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux commands cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Free online course: RHEL Technical Overview</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux networking cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">SELinux cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux common commands cheat sheet</a></li>
<li><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">What are Linux containers?</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Our latest Linux articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>So, even though you might feel more comfortable with <code>sed</code> and <code>grep</code>, you may not use their full potential. Of course, there's no obligation to learn more about <code>sed</code> or <code>grep</code>, but I sometimes wonder about the way I "learn" commands. Instead of learning <em>how</em> a command works, I often learn a specific incantation that includes a command. As a result, I often feel a false familiarity with the command. I think I know a command because I can name three or four options off the top of my head, even though I don't know what the options do and can't quite put my finger on the syntax.</p>
<p>And that's the problem, I believe, that many people face when confronted with the power and flexibility of <code>awk</code>.</p>
<h2 id="learning-awk-to-use-awk">Learning awk to use awk</h2>
<p>The basics of <code>awk</code> are surprisingly simple. It's often noted that <code>awk</code> is a programming language, and although it's a relatively basic one, it's true. This means you can learn <code>awk</code> the same way you learn a new coding language: learn its syntax using some basic commands, learn its vocabulary so you can build up to complex actions, and then practice, practice, practice.</p>
<h2 id="how-awk-parses-input">How awk parses input</h2>
<p><code>Awk</code> sees input, essentially, as an array. When <code>awk</code> scans over a text file, it treats each line, individually and in succession, as a <em>record</em>. Each record is broken into <em>fields</em>. Of course, <code>awk</code> must keep track of this information, and you can see that data using the <code>NR</code> (number of records) and <code>NF</code> (number of fields) built-in variables. For example, this gives you the line count of a file:</p>
<pre><code class="language-bash">$ awk 'END { print NR;}' example.txt
36</code></pre><p>This also reveals something about <code>awk</code> syntax. Whether you're writing <code>awk</code> as a one-liner or as a self-contained script, the structure of an <code>awk</code> instruction is:</p>
<pre><code class="language-text">pattern or keyword { actions }</code></pre><p>In this example, the word <code>END</code> is a special, reserved keyword rather than a pattern. A similar keyword is <code>BEGIN</code>. With both of these keywords, <code>awk</code> just executes the action in braces at the start or end of parsing data.</p>
<p>You can use a <em>pattern</em> as a filter or qualifier so that <code>awk</code> only executes a given action when it is able to match your pattern to the current record. For instance, suppose you want to use <code>awk</code>, much as you would <code>grep</code>, to find the word <em>Linux</em> in a file of text:</p>
<pre><code class="language-bash">$ awk '/Linux/ { print $0; }' os.txt
OS: CentOS Linux (10.1.1.8)
OS: CentOS Linux (10.1.1.9)
OS: Red Hat Enterprise Linux (RHEL) (10.1.1.11)
OS: Elementary Linux (10.1.2.4)
OS: Elementary Linux (10.1.2.5)
OS: Elementary Linux (10.1.2.6)</code></pre><p>For <code>awk</code>, each line in the file is a record, and each word in a record is a field. By default, fields are separated by a space. You can change that with the <code>--field-separator</code> option, which sets the <code>FS</code> (field separator) variable to whatever you want it to be:</p>
<pre><code class="language-bash">$ awk --field-separator ':' '/Linux/ { print $2; }' os.txt 
 CentOS Linux (10.1.1.8)
 CentOS Linux (10.1.1.9)
 Red Hat Enterprise Linux (RHEL) (10.1.1.11)
 Elementary Linux (10.1.2.4)
 Elementary Linux (10.1.2.5)
 Elementary Linux (10.1.2.6)</code></pre><p>In this sample, there's an empty space before each listing because there's a blank space after each colon (<code>:</code>) in the source text. This isn't <code>cut</code>, though, so the field separator needn't be limited to one character:</p>
<pre><code class="language-bash">$ awk --field-separator ': ' '/Linux/ { print $2; }' os.txt 
CentOS Linux (10.1.1.8)
CentOS Linux (10.1.1.9)
Red Hat Enterprise Linux (RHEL) (10.1.1.11)
Elementary Linux (10.1.2.4)
Elementary Linux (10.1.2.5)
Elementary Linux (10.1.2.6)</code></pre><h2 id="functions-in-awk">Functions in awk</h2>
<p>You can build your own functions in <code>awk</code> using this syntax:</p>
<pre><code class="language-bash">name(parameters) { actions }</code></pre><p>Functions are important because they allow you to write code once and reuse it throughout your work. When constructing one-liners, custom functions are a little less useful than they are in scripts, but <code>awk</code> defines many functions for you already. They work basically the same as any function in any other language or spreadsheet: You learn the order that the function needs information from you, and you can feed it whatever you want to get the results.</p>
<p>There are functions to perform mathematical operations and string processing. The math ones are often fairly straightforward. You provide a number, and it crunches it:</p>
<pre><code class="language-bash">$ awk 'BEGIN { print sqrt(1764); }'
42</code></pre><p>String functions can be more complex but are well documented in the <a href="https://www.gnu.org/software/gawk/manual/gawk.html" target="_blank">GNU awk manual</a>. For example, the <code>split</code> function takes an entity that <code>awk</code> views as a single field and splits it into different parts. It requires a field, a variable to use as an array containing each part of the split, and the character you want to use as the delimiter.</p>
<p>Using the output of the previous examples, I know that there's an IP address at the very end of each record. In this case, I can send just the last field of a record to the <code>split</code> function by referencing the variable <code>NF</code> because it contains the number of fields (and the final field must be the highest number):</p>
<pre><code class="language-bash">$ awk --field-separator ': ' '/Linux/ { split($NF, IP, "."); print "subnet: " IP[3]; }' os.txt
subnet: 1
subnet: 1
subnet: 1
subnet: 2
subnet: 2
subnet: 2</code></pre><p>There are many more functions, and there's no reason to limit yourself to one per block of <code>awk</code> code. You can construct complex pipelines with <code>awk</code> in your terminal, or you can write <code>awk</code> scripts to define and utilize your own functions.</p>
<h2 id="download-the-cheat-sheet">Download the eBook</h2>
<p>Learning <code>awk</code> is mostly a matter of using <code>awk</code>. Use it even if it means duplicating functionality you already have with <code>sed</code> or <code>grep</code> or <code>cut</code> or <code>tr</code> or any other perfectly valid commands. Once you get comfortable with it, you can write Bash functions that invoke your custom <code>awk</code> commands for easier use. And eventually, you'll be able to write scripts to parse complex datasets.</p>
<p><strong><a href="https://opensource.com/downloads/awk-ebook">Download our </a></strong><span style="color:#e74c3c;"><strong><a href="https://opensource.com/downloads/awk-ebook">eBook</a> </strong></span>to learn everything you need to know about <code>awk</code>, and start using it today.</p>
