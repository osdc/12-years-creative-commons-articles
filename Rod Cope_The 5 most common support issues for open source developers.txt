<p>What is the number one factor that software developers consider when choosing which open source software packages to use? A <a href="http://www.roguewave.com/programs/open-source-support-report" target="_blank">recent report</a> by Rogue Wave Software says <strong>support</strong>. What is the second most important factor? Who will carry the burden of providing that support. Additionally, an unsurprising 67% of developers say they are expected to be responsible for support.</p>
<p><em>This report cites a survey of 350 people conducted by Open System Media as well as an internal analysis of 1,000 support requests. Open System Media conducted the survey through their website, and our internal analysis was of internal data collected through support requests submitted to our system. Respondents include: software developers, database administrators, security engineers, CIOs/IT managers, and solution architects.</em></p>
<p>There are no stupid questions when it comes to OSS use, and these are their most common issues:</p>
<ol><li>How do I set up or use a particular function within this package?</li>
<li>How does this security update affect me?</li>
<li>How do I isolate this bug between the proprietary and open source code in this stack?</li>
<li>Which package or version is the best choice for what I want to do?</li>
<li>What are the differences between these two versions that apply to me?</li>
</ol><p>Additional support challenges arise when an organization elects to use OSS because they are new to open source, or the best practices for use aren't defined:</p>
<ul><li>Lack of knowledge or experience for a specific package or version.</li>
<li>Application performance issues and tuning.</li>
<li>Mandated use of packages by the customer but little in-house expertise.</li>
<li>Need for fast bug turnarounds in production to meet uptime requirements.</li>
<li>Support redundancy; what happens when key expertise goes on vacation?</li>
<li>Desire to combine OSS and proprietary code to deliver custom stacks.</li>
<li>Need for workarounds for older versions that are no longer officially supported.</li>
<li>Consolidating multiple different versions and OSS processes across the organization.</li>
</ul><p>Though this isn't surprising it can be troubling, and developers shouldn't have to carry the entire support burden, especially for complex software stacks that encompass databases, build tools, operating systems, middleware, mixed proprietary and open source software, service-level agreements, and multiple, and often conflicting, software licenses.</p>
<p>When deciding who owns support in your organization, you want to choose a team that can provide support for your entire software ecosystem and is easy to get in touch with when you need help. They should provide the necessary expertise and tools, including:</p>
<ul><li>Tier 4 architects, not junior-level engineers</li>
<li>Help along your development journey from build, to continuous integration (CI), to continuous deployment (CD), to monitoring production environments</li>
<li>One number to call for all your questions</li>
</ul><p>The open source software ecosystem is different from the proprietary software world, and organizations need to learn new ways of managing it. Some issues to consider are the openness and diffuse nature of OSS development, how well the proprietary software works with the open source software, and licensing.</p>
