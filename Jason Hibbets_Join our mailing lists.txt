<p><strong>Get email updates</strong></p>
<p><a href="http://bit.ly/dlEmt2" title="https://inquiries.redhat.com/go/redhat/opensourcedotcom-subscription">Sign up here</a> to get Opensource.com highlights and updates delivered to your inbox.</p>
<p>We're asking for only your email address—we like to keep these things simple. The newsletter is sent through Red Hat's email subscription services, which is why it has a redhat.com URL.</p>
<p>After signing up, you may want to add <em>highlights at opensource dot com</em> to your email filters.</p>
<p><strong>Community discussion list</strong></p>
<p>Do you want to help out with Opensource.com but aren't sure how? Interested in seeing some of the behind-the-scenes work that's going on? We've created a community mailing list that is open to anyone and everyone.</p>
<p>You can subscribe to the list at:</p>
<!--break--><p> <a href="https://www.redhat.com/mailman/listinfo/osdc-list" target="_blank" title="https://www.redhat.com/mailman/listinfo/osdc-list">https://www.redhat.com/mailman/listinfo/osdc-list</a></p>
<blockquote><p>This list is for the Opensource.com community, which includes the people who write, edit, moderate, maintain, publish, design, think about, make plans, sets policy and governance, and otherwise do the work that makes this a community practicing the open source way.</p>
<p>This list is not meant to substitute or supplement the discussions that occur on opensource.com. It is meant to be the location where administrivia and such topics are discussed. The list and archives are open to the general community, as is the open source way.</p>
</blockquote>
<p>It's important to reiterate that conversations that could happen on Opensource.com should continue to happen there—it's a more open forum than a mailing list. Appropriate discussions for the mailing list have more to do with execution of the site than with the stories we're telling on it.</p>
<p>We welcome you to subscribe and start participating, share your ideas, and help improve our community.</p>
