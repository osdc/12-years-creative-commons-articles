<p>It's been a great year for the opensource.com Life channel. We've seen tremendous growth of the open source community, and it's been a pleasure to help record and publicize all the exciting projects all of you are working on. Here are some articles that represent the gamut of topics and stories that came through the Life channel in 2012:</p>
<!--break-->
<h2>Open space</h2>
<p class="title"><a title="NASA Mars rover" href="https://opensource.com/life/12/10/NASA-achieves-data-goals-Mars-rover-open-source-software" target="_blank">NASA achieves data goals for Mars rover with open source software</a></p>
<p class="title"><a title="code.NASA" href="https://opensource.com/life/12/1/nasa-launches-codenasagov-share-and-collaborate-further-open-source-community" target="_blank">NASA launches code.nasa.gov to share and collaborate further with the open source community</a></p>
<h2 class="title">Open music</h2>
<p class="title"><a title="Open source music " href="https://opensource.com/life/12/9/open-source-music-making-software-helps-artists-stay-creative" target="_blank">Open source software helps artists create music</a></p>
<p class="title"><a title="DRM history of music" href="https://opensource.com/life/11/11/drm-graveyard-brief-history-digital-rights-management-music" target="_blank">The DRM graveyard: A brief history of digital rights management in music</a></p>
<p class="title"><a title="open design in music" href="https://opensource.com/life/12/11/open-source-design-music-ecology" target="_blank">Open source design in music and ecology</a></p>
<p class="title"><a title="open beats" href="https://opensource.com/life/12/6/open-beats-rock-brazil" target="_blank">Open Beats rock Brazil</a></p>
<h2 class="title">Open beverages</h2>
<p class="title"><a title="open source beer" href="https://opensource.com/life/12/11/beer-open-source-beverage-choice" target="_blank">Beer: The open source beverage of choice</a></p>
<p class="title"><a title="open source pumpkin spice latte" href="https://opensource.com/life/12/12/open-source-pumpkin-spice-latte" target="_blank">Homeroaster crafts a pumpkin spice latte the open source way</a></p>
<p class="title"><a title="open source beer" href="https://opensource.com/life/12/8/open-source-beer" target="_blank">Beer brings people together: Obama, homebrewers, and online communities</a></p>
<h2 class="title">Open gaming</h2>
<div id="content-header">
<p class="title"><a title="STEAM gets 3 new games" href="https://opensource.com/life/12/11/steam-linux-gets-three-new-games" target="_blank">Steam for Linux gets three new games</a></p>
<p class="title"><a title="Five new RPG games for Linux" href="https://opensource.com/life/12/10/5-new-rpg-games-linux" target="_blank">Five new RPG games for Linux</a></p>
<p class="title"><a title="better art for games" href="https://opensource.com/life/12/9/creating-better-art-open-source-games" target="_blank">Creating better art for open source games</a></p>
<h2 class="title">Open maps</h2>
<p class="title"><a title="walk your city" href="https://opensource.com/life/12/4/open-source-wayfinding-walk-your-city" target="_blank">Open source wayfinding with Walk [Your City]</a></p>
<p class="title"><a title="OpenStreetMap" href="https://opensource.com/life/12/10/openstreetmap-first-open-map-world" target="_blank">OpenStreetMap makes first open map of the world</a></p>
<h2 class="title">Open source analogies</h2>
<p class="title"><a title="open source analogy" href="https://opensource.com/life/12/6/open-source-like-sharing-recipe" target="_blank">An open source analogy: Open source is like sharing a recipe</a></p>
<p class="title"><a title="open source is like falling in love" href="https://opensource.com/life/12/6/open-source-falling-love" target="_blank">Open source is like falling in love</a></p>
<p class="title"><a title="mind open sourced" href="https://opensource.com/life/12/4/day-my-mind-became-open-sourced" target="_blank">The day my mind became open sourced</a></p>
<h2 class="title">Open source for beginners</h2>
<p class="title"><a title="join open source project" href="https://opensource.com/life/12/3/its-scary-join-open-source-project" target="_blank">It’s scary to join an open source project</a></p>
<p class="title"><a title="contribute to open source" href="https://opensource.com/life/12/11/open-source-contributions-come-all-shapes-and-sizes" target="_blank">Contribute to an open source project no matter your experience level</a></p>
</div>
<p>I also want to add a list of the top 10 articles from the Life channel based on page views. Let us know in the comments which stories were your favorites. And it doesn't have to be one of the articles mentioned here!</p>
<h2>Top 10 open*life posts in 2012:</h2>
<ol><li><a title="rasperry pi, allwinner, cubox" href="https://opensource.com/life/12/1/linux-hardware-race-tiniest-and-cheapest-15-cheap" target="_blank">Raspberry Pi, Allwinner, and CuBox in the Linux hardware race to tiniest and cheapest</a></li>
<li><a title="open source creative tools" href="https://opensource.com/life/12/9/tour-through-open-source-creative-tools" target="_blank">A tour through open source creative tools</a></li>
<li><a title="NASA Mars rover" href="https://opensource.com/life/12/10/NASA-achieves-data-goals-Mars-rover-open-source-software" target="_blank">NASA achieves data goals for Mars rover with open source software</a></li>
<li><a title="top open source gifts" href="https://opensource.com/life/12/11/top-ten-open-source-gifts-holidays" target="_blank">Top ten open source gifts for the holidays</a></li>
<li><a title="hacker highschool" href="https://opensource.com/life/12/8/hacker-highschool-students-learn-redesign-future" target="_blank">In Hacker Highschool, students learn to redesign the future</a></li>
<li><a title="SOPA gallery" href="https://opensource.com/life/12/1/january-18-captured-sopa-blackout-gallery" target="_blank">January 18 captured: A SOPA blackout gallery</a></li>
<li><a title="open beats" href="https://opensource.com/life/12/6/open-beats-rock-brazil" target="_blank">Open Beats rock Brazil</a></li>
<li><a title="debunking the oatmeal" href="https://opensource.com/life/12/2/debunking-oatmeal-linux" target="_blank">Debunking The Oatmeal and the perception of Linux as difficult to use</a></li>
<li><a title="open source diagrams" href="https://opensource.com/life/12/7/open-source-visio-tips" target="_blank">Three tips for working with open source diagrams</a></li>
<li><a title="experiment with Linux" href="https://opensource.com/life/12/11/why-experiment-linux" target="_blank">Why experiment with Linux?</a></li>
</ol><p class="title">Thanks to everyone who has contributed to the Life channel—it's been quite a year!</p>
