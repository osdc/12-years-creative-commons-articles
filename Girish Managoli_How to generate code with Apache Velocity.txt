<p>Apache <a href="https://velocity.apache.org/" target="_blank">Velocity</a> is an open source, Java-based template engine and code generator that converts templates into source code. Because it is implemented in Java, it is capable of interpreting varied templates and generating code for any language (web, service, SQL, scripts, etc.), although it seems to be oriented mostly toward web development.</p>
<h2 id="velocitys-structure">Velocity's structure</h2>
<p>Velocity's structure is comprised of an engine and tools. Its core is the Velocity Engine, which uses the defined template, interprets the template language, and generates the code.</p>
<p>Templates are defined with Velocity Template Language (<a href="https://velocity.apache.org/engine/devel/vtl-reference.html" target="_blank">VTL</a>), a simple language with effective directives. VTL statements are directives or variables, and variables can be standalone or class methods.</p>
<p>Examples of VTL expressions include:</p>
<table border="1" cellpadding="1" cellspacing="1" style="width:700px;"><tbody><tr><td style="vertical-align: top;">
<pre><code class="language-java">package ${packagename};</code></pre></td>
<td style="vertical-align: top;">Inserts a package statement in Java where the package name is defined as <strong>packagename</strong></td>
</tr><tr><td>
<pre><code class="language-java">public ${classname} implements Serializable {</code></pre></td>
<td style="vertical-align: top;">Adds a class with name <strong>classname</strong></td>
</tr><tr><td>
<pre><code class="language-java">#foreach( $property in $properties )
 public ${property.fieldType} get${property.getField()}() {
     return this.${property.fieldName};
 }
#end</code></pre></td>
<td style="vertical-align: top;">Creates getter methods for all defined properties</td>
</tr></tbody></table><p>Velocity tools are collections of basic user-friendly capabilities. There are <a href="http://velocity.apache.org/tools/devel/generic.html" target="_blank">GenericTools</a>, a "set of classes that provide basic infrastructure for using tools in standard Java SE Velocity projects, as well as a set of tools for use in generic Velocity templates." They include DateTool, MathTool, NumberTool, SortTool, and XmlTool. There are also <a href="http://velocity.apache.org/tools/devel/view.html" target="_blank">VelocityView</a> tools, which include "all of the GenericTools and adds infrastructure and specialized tools for using Velocity in the view layer of web applications (Java EE projects)." VelocityView tools include BrowserTool, CookieTool, and ImportTool</p>
<h2 id="velocity-advantages-and-disadvantages">Velocity advantages and disadvantages</h2>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More on Java</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/topics/enterprise-java/?intcmp=7013a000002Cxq6AAC">What is enterprise Java programming?</a></li>
<li><a href="https://developers.redhat.com/products/openjdk/overview?intcmp=7013a000002Cxq6AAC">Red Hat build of OpenJDK</a></li>
<li><a href="https://opensource.com/downloads/java-cheat-sheet?intcmp=7013a000002Cxq6AAC">Java cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/do092-developing-cloud-native-applications-microservices-architectures?intcmp=7013a000002Cxq6AAC">Free online course: Developing cloud-native applications with microservices architectures</a></li>
<li><a href="https://opensource.com/tags/java?intcmp=7013a000002Cxq6AAC">Fresh Java articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>Velocity is easy to use and has the capability to generate any language. On the downside, there is a learning curve to understand and apply its template language. Velocity is morphology- and ontology-free. It has no knowledge of the design capability of the module it generates. As a practical example, Velocity may use a template for a controller (e.g., Model-View-Controller or an architecture style) and generate the code, but it has no awareness of the concept of a controller. This is both an advantage and disadvantage, with a generator being simple and easy to use but with no awareness of the design's aptitude.</p>
<h2 id="using-velocity">Using Velocity</h2>
<p>Velocity's Java library is available on the <a href="https://mvnrepository.com/artifact/org.apache.velocity/velocity" target="_blank">Maven repository</a>. To use the .jar file, define Velocity's latest version in your Maven build config. (Velocity 1.7 is the latest version, as of this writing.) For example, enter the following in your Maven Project Object Model (POM):</p>
<pre><code class="language-java">&lt;dependency&gt;
	&lt;groupId&gt;org.apache.velocity&lt;/groupId&gt;
	&lt;artifactId&gt;velocity&lt;/artifactId&gt;
	&lt;version&gt;1.7&lt;/version&gt;
&lt;/dependency&gt;</code></pre><h3 id="a-java-hello-world-example">Java Hello World example</h3>
<p>To generate code, you need two things:</p>
<ol><li>The <strong>Velocity template</strong> to be used for generation, e.g., java_example.vm:<br /><pre><code class="language-java">public class ${className} {

    public static void main(String[] args) {
        System.out.println("${message}");
    }

}</code></pre></li>
</ol><ol start="2"><li>The <strong>Velocity generator</strong> that uses the template to generate code, e.g., VelocityStartGenerator.java:<br /><pre><code class="language-java">public class VelocityStartGenerator {
 
    static String inputTemplate = "java_example.vm";
    static String className = "VelocityExample";
    static String message = "Hello World!";
    static String outputFile = className + ".java";
	
    public static void main(String[] args) throws IOException {
    	
        VelocityEngine velocityEngine = new VelocityEngine();
        velocityEngine.init();
      
        VelocityContext context = new VelocityContext();
        context.put("className", className);
        context.put("message", message);

        Writer writer = new FileWriter(new File(outputFile));
        Velocity.mergeTemplate(inputTemplate, "UTF-8", context, writer);
        writer.flush();
        writer.close();
        
        System.out.println("Generated " + outputFile);
    }
}</code></pre></li>
</ol><p>Build the Velocity generator and run it. With Maven:</p>
<pre><code class="language-java">$ mvn clean install
$ mvn exec:java -Dexec.mainClass=velocity_generator.VelocityStartGenerator</code></pre><p>This generates the file <strong>VelocityExample.java</strong>, which looks like this:</p>
<pre><code class="language-java">public class VelocityExample {

    public static void main(String[] args) {
        System.out.println("Hello World!");
    }

}</code></pre><p>Build and run it, and it will say: "Hello World!"</p>
<pre><code class="language-java">$ javac VelocityExample.java
$ java VelocityExample
Hello World!</code></pre><p>Play around with the generator. Can you make the generated file say "Howdy"?</p>
<p>Next, check out the following HTML and shell script generation examples.</p>
<h3 id="html-hello-world">HTML Hello World example</h3>
<table border="1" cellpadding="1" cellspacing="1" style="width:700px;"><tbody><tr><td style="vertical-align: top;"><strong>Velocity template<br /><br />
			(html_example.vm)</strong></td>
<td>
<pre><code class="language-text">&lt;!DOCTYPE html&gt;
&lt;html&gt;
&lt;body&gt;

&lt;h1&gt;$message&lt;/h1&gt;

&lt;/body&gt;
&lt;/html&gt;</code></pre></td>
</tr><tr><td style="vertical-align: top;"><strong>Velocity generator<br /><br />
			(VelocityStartGenerator.java)</strong></td>
<td>
<pre><code class="language-java">public class VelocityStartGenerator {
 
    static String inputTemplate = "http_example.vm";
    static String message = "Hello World!";
    static String outputFile = "helloworld.html";
	
    public static void main(String[] args) throws IOException {
    	
        VelocityEngine velocityEngine = new VelocityEngine();
        velocityEngine.init();
      
        VelocityContext context = new VelocityContext();
        context.put("message", message);

        Writer writer = new FileWriter(new File(outputFile));
        Velocity.mergeTemplate(inputTemplate
        		, "UTF-8", context, writer);
        writer.flush();
        writer.close();
        
        System.out.println("Generated " + outputFile);
    }
}</code></pre></td>
</tr><tr><td style="vertical-align: top;"><strong>Generated file<br /><br />
			(helloworld.html)</strong></td>
<td>
<pre><code class="language-text">&lt;!DOCTYPE html&gt;
&lt;html&gt;
&lt;body&gt;

&lt;h1&gt;Hello World!&lt;/h1&gt;

&lt;/body&gt;
&lt;/html&gt;</code></pre></td>
</tr><tr><td style="vertical-align: top;"><strong>Generated output</strong></td>
<td>
<article class="align-left media media--type-image media--view-mode-full" title="Velocity HTML output"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/velocity_html_output.png" width="400" height="210" alt="Velocity HTML output" title="Velocity HTML output" /></div>
      
  </article></td>
</tr></tbody></table><h3 id="shell-script-hello-world">Shell script Hello World example</h3>
<table border="1" cellpadding="1" cellspacing="1" style="width:700px;"><tbody><tr><td style="vertical-align: top;"><strong>Velocity template<br /><br />
			(sh_example.vm)</strong></td>
<td>
<pre><code class="language-bash">echo $message</code></pre></td>
</tr><tr><td style="vertical-align: top;"><strong>Velocity generator<br /><br />
			(VelocityStartGenerator.java)</strong></td>
<td>
<pre><code class="language-java">public class VelocityStartGenerator {
 
    static String inputTemplate = "sh_example.vm";
    static String message = "Hello World!";
    static String outputFile = "helloworld.sh";
	
    public static void main(String[] args) throws IOException {
    	
        VelocityEngine velocityEngine = new VelocityEngine();
        velocityEngine.init();
      
        VelocityContext context = new VelocityContext();
        context.put("message", message);

        Writer writer = new FileWriter(new File(outputFile));
        Velocity.mergeTemplate(inputTemplate, "UTF-8", context, writer);
        writer.flush();
        writer.close();
        
        System.out.println("Generated " + outputFile);
    }
}</code></pre></td>
</tr><tr><td style="vertical-align: top;"><strong>Generated file<br /><br />
			(helloworld.sh)</strong></td>
<td>
<pre><code class="language-bash">echo Hello World!</code></pre></td>
</tr><tr><td style="vertical-align: top;"><strong>Generated output</strong></td>
<td>
<article class="align-left media media--type-image media--view-mode-full" title="Velocity shell output"><div class="field field--name-field-media-image field--type-image field--label-hidden field__item">  <img loading="lazy" src="https://opensource.com/sites/default/files/uploads/velocity_shell_output.png" width="400" height="227" alt="Velocity shell output" title="Velocity shell output" /></div>
      
  </article></td>
</tr></tbody></table><h2 id="conclusion">Conclusion</h2>
<p>You can find Velocity in Apache's <a href="https://github.com/apache/velocity-engine" target="_blank">GitHub</a> repo, and you can find examples of its use on <a href="https://gitlab.com/gammay/apache-velocity-example" target="_blank">GitLab</a> and <a href="https://github.com/gammay/apache-velocity-example" target="_blank">GitHub</a>.</p>
<p>This was a brief and quick rundown on Apache Velocity. Have you used it? What do you think? Please share your thoughts in the comments.</p>
