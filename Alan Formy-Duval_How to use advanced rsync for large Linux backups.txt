<p>It seems clear that backups are always a hot topic in the Linux world. Back in 2017, David Both offered <a href="http://Opensource.com">Opensource.com</a> readers tips on "<a href="https://opensource.com/article/17/1/rsync-backup-linux">Using rsync to back up your Linux system</a>," and earlier this year, he published a poll asking us, "<a href="https://opensource.com/poll/19/4/backup-strategy-home-directory-linux">What's your primary backup strategy for the /home directory in Linux?</a>" In another poll this year, Don Watkins asked, "<a href="https://opensource.com/article/19/2/linux-backup-solutions">Which open source backup solution do you use?</a>"</p>
<p>My response is <a href="https://en.wikipedia.org/wiki/Rsync" target="_blank">rsync</a>. I really like rsync! There are plenty of large and complex tools on the market that may be necessary for managing tape drives or storage library devices, but a simple open source command line tool may be all you need.</p>
<h2 id="basic-rsync">Basic rsync</h2>
<p>I managed the binary repository system for a global organization that had roughly 35,000 developers with multiple terabytes of files. I regularly moved or archived hundreds of gigabytes of data at a time. Rsync was used. This experience gave me confidence in this simple tool. (So, yes, I use it at home to back up my Linux systems.)</p>
<p>The basic rsync command is simple.</p>
<pre><code class="language-text">rsync -av SRC DST</code></pre><p>Indeed, the rsync commands taught in any tutorial will work fine for most general situations. However, suppose we need to back up a very large amount of data. Something like a directory with 2,000 sub-directories, each holding anywhere from 50GB to 700GB of data. Running rsync on this directory could take a tremendous amount of time, particularly if you're using the checksum option, which I prefer.</p>
<p>Performance is likely to suffer if we try to sync large amounts of data or sync across slow network connections. Let me show you some methods I use to ensure good performance and reliability.</p>
<h2 id="advanced-rsync">Advanced rsync</h2>
<p>
</p><div class="embedded-callout-menu callout-float-right">
<div class="view view-related-content-callout view-id-related_content_callout view-display-id-default">
<div class="view-header">
<p>More Linux resources</p>
</div>
<div class="view-content">
<div class="item-list">
<ul><li><a href="https://developers.redhat.com/cheat-sheets/linux-commands-cheat-sheet/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux commands cheat sheet</a></li>
<li><a href="https://developers.redhat.com/cheat-sheets/advanced-linux-commands/?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Advanced Linux commands cheat sheet</a></li>
<li><a href="https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Free online course: RHEL Technical Overview</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-networking?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux networking cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/cheat-sheet-selinux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">SELinux cheat sheet</a></li>
<li><a href="https://opensource.com/downloads/linux-common-commands-cheat-sheet?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Linux common commands cheat sheet</a></li>
<li><a href="https://opensource.com/resources/what-are-linux-containers?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">What are Linux containers?</a></li>
<li><a href="https://opensource.com/tags/linux?intcmp=70160000000h1jYAAQ&amp;utm_source=intcallout&amp;utm_campaign=linuxcontent">Our latest Linux articles</a></li>
</ul></div>
</div>
</div>
</div>
<p>One of the first lines that appears when rsync runs is: "sending incremental file list." If you do a search for this line, you'll see many questions asking things like: why is it taking forever? or why does it seem to hang up?</p>
<p>Here's an example based on this scenario. Let's say we have a directory called <strong>/storage</strong> that we want to back up to an external USB device mounted at <strong>/media/WDPassport</strong>.</p>
<p>If we want to back up <strong>/storage</strong> to a USB external drive, we could use this command:</p>
<pre><code class="language-text">rsync -cav /storage /media/WDPassport</code></pre><p>The <strong>c</strong> option tells rsync to use file checksums instead of timestamps to determine changed files, and this usually takes longer. In order to break down the <strong>/storage</strong> directory, I sync by subdirectory, using the <strong>find</strong> command. Here's an example:</p>
<pre><code class="language-text">find /storage -type d -exec rsync -cav {} /media/WDPassport \;</code></pre><p>This looks OK, but if there are any files in the <strong>/storage</strong> directory, they will not be copied. So, how can we sync the files in <strong>/storage</strong>? There is also a small nuance where certain options will cause rsync to sync the <strong>.</strong> directory, which is the root of the source directory; this means it will sync the subdirectories twice, and we don't want that.</p>
<p>Long story short, the solution I settled on is a "double-incremental" script. This allows me to break down a directory, for example, breaking <strong>/home</strong> into the individual users' home directories or in cases when you have multiple large directories, such as music or family photos.</p>
<p>Here is an example of my script:</p>
<pre><code class="language-text">HOMES="alan"
DRIVE="/media/WDPassport"

for HOME in $HOMES; do
     cd /home/$HOME
     rsync -cdlptgov --delete . /$DRIVE/$HOME
     find . -maxdepth 1 -type d -not -name "." -exec rsync -crlptgov --delete {} /$DRIVE/$HOME \;
done</code></pre><p>The first rsync command copies the files and directories that it finds in the source directory. However, it leaves the directories empty so we can iterate through them using the <strong>find</strong> command. This is done by passing the <strong>d</strong> argument, which tells rsync not to recurse the directory.</p>
<pre><code class="language-text">-d, --dirs                  transfer directories without recursing</code></pre><p>The <strong>find</strong> command then passes each directory to rsync individually. Rsync then copies the directories' contents. This is done by passing the <strong>r</strong> argument, which tells rsync to recurse the directory.</p>
<pre><code class="language-text">-r, --recursive             recurse into directories</code></pre><p>This keeps the increment file that rsync uses to a manageable size.</p>
<p>Most rsync tutorials use the <strong>a</strong> (or <strong>archive</strong>) argument for convenience. This is actually a compound argument.</p>
<pre><code class="language-text">-a, --archive               archive mode; equals -rlptgoD (no -H,-A,-X)</code></pre><p>The other arguments that I pass would have been included in the <strong>a</strong>; those are <strong>l</strong>, <strong>p</strong>, <strong>t</strong>, <strong>g</strong>, and <strong>o</strong>.</p>
<pre><code class="language-text">-l, --links                 copy symlinks as symlinks
-p, --perms                 preserve permissions
-t, --times                 preserve modification times
-g, --group                 preserve group
-o, --owner                 preserve owner (super-user only)</code></pre><p>The <strong>--delete</strong> option tells rsync to remove any files on the destination that no longer exist on the source. This way, the result is an exact duplication. You can also add an exclude for the <strong>.Trash</strong> directories or perhaps the <strong>.DS_Store</strong> files created by MacOS.</p>
<pre><code class="language-text">-not -name ".Trash*" -not -name ".DS_Store"</code></pre><h2 id="be-careful">Be careful</h2>
<p>One final recommendation: rsync can be a destructive command. Luckily, its thoughtful creators provided the ability to do "dry runs." If we include the <strong>n</strong> option, rsync will display the expected output without writing any data.</p>
<pre><code class="language-text">rsync -cdlptgovn --delete . /$DRIVE/$HOME</code></pre><p>This script is scalable to very large storage sizes and large latency or slow link situations. I'm sure there is still room for improvement, as there always is. If you have suggestions, please share them in the comments.</p>
